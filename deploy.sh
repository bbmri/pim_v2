#!/usr/bin/env bash

echo "Deploying PIM to pim-" $1 " server"

TS_1=$SECONDS

# Attempt to shut down running PIM
if [ -f "/home/ubuntu/pim/docker-compose.yml" ]; then
   cd ~/pim/server/docker/production/site && sudo docker-compose stop
fi

# Ensure the PIM containers are stopped
sudo docker stop pim_flask
sudo docker stop pim_db

# Docker system cleanup (remove dangling containers images)
sudo docker system prune -f

# Docker volumes can clog the vm, so remove any dangling or orphaned volumes
sudo docker volume ls -qf dangling=true | xargs -r sudo docker volume rm

TS_2=$SECONDS

echo "Stage 1 took $(($TS_2 - $TS_1)) seconds"

# Find large directories
#find -maxdepth 2 -type d -exec du -sh {} \;

# Add Gitlab server to known hosts
ssh-keyscan -t rsa gitlab.com > ~/.ssh/known_hosts

if [ -d "/home/ubuntu/pim" ]; then
    cd ~/pim && git pull
else
    cd ~ && git clone git@gitlab.com:bbmri/pim.git
fi

# Checkout develop branch in case of production deployment
if [ $1 = "develop" ]; then
    cd ~/pim && git checkout develop
fi

# Checkout master branch in case of production deployment
if [ $1 = "production" ]; then
    cd ~/pim && git checkout master
fi

# Run script that writes deployment info (datetime and git revision hash) to file
cd ~/pim && sh deploy_info.sh

TS_3=$SECONDS

echo "Stage 2 took $(($TS_3 - $TS_2)) seconds"

cd ~/pim/app && npm install

TS_4=$SECONDS

echo "NPM install took $(($TS_4 - $TS_3)) seconds"

# Run production bundling tools
cd ~/pim/app && npm run "build"

TS_5=$SECONDS

echo "NPM build took $(($TS_5 - $TS_4)) seconds"

# Build images if necessary
cd ~/pim/server/docker/production/site && sudo docker-compose build

# Start docker compose up in the background
cd ~/pim/server/docker/production/site && sudo PIM_ROOT=/home/ubuntu/pim PIM_DATA=/home/ubuntu/pim-data docker-compose up -d

TS_6=$SECONDS

echo "Docker compose build an up took $(($TS_6 - $TS_5)) seconds"

# Wait for PIM to be up
sleep 5;

# Verify that the service has started with curl
wget -qO- "$2/api/runs" &> /dev/null