## What is PIM?
PIM is a web service that visualizes the state of an (image) processing pipeline and provides interaction modalities to inspect the data that flows in the pipeline.

#### Requirements
* Docker CE (developed and tested with 17.12.0)
* Docker Compose (developed and tested with version 1.16.1) 

### Running PIM for development and demonstration purposes
The most convenient way to develop PIM is to use the Docker Compose environment located under ```/pim/develop```. This environment has all the necessary services to interactively implement, test and debug PIM:
* PostgreSQL database
* Flask REST API
* WebPack development server

Launch the environment with:
```
cd /pim/develop
sudo docker-compose up
```
PIM will be available at ```localhost:8080```

The PIM database is populated with example runs for demonstration purposes.

#### Backend development
The files for backend development are located in ```/pim/server```. The backend server is based on a Flask application with debug turned on, so changing the content of *.py files will trigger a re-load of the Flask application. 

#### Frontend development
The files for backend development are located in ```/pim/app```. Changing the content of JavaScript files triggers a re-load of the WebPack development server. 



