import os

from adapter.link import Link
from adapter.run import Run
from adapter.port import Port


def create_run(**kwargs):
    run = Run()
    run.name = 'epi_fat'

    os.environ['RUN_NAME'] = run.name

    nodes = dict()

    nodes['root'] = run.create_node(name='root', parent=None)

    run.root = nodes['root']

    
    nodes['step_Extract_Fat_Tissue'] = run.create_node(name='step_Extract_Fat_Tissue', description='Leaf', parent=nodes['root'])
    nodes['step_statistics'] = run.create_node(name='step_statistics', description='Leaf', parent=nodes['root'])
    nodes['step_Sinks'] = run.create_node(name='step_Sinks', description='Leaf', parent=nodes['root'])
    nodes['step_majority_voting'] = run.create_node(name='step_majority_voting', description='Leaf', parent=nodes['root'])
    nodes['step_Atlas_Registration'] = run.create_node(name='step_Atlas_Registration', description='Leaf', parent=nodes['root'])
    nodes['step_PreProcessing'] = run.create_node(name='step_PreProcessing', description='Leaf', parent=nodes['root'])

    
    nodes['register_distance_map'] = run.create_node(name='register_distance_map', type='node', parent=nodes['step_Atlas_Registration'], no_in_ports=0, no_out_ports=0)

    
    nodes['register_distance_map'].in_ports['in_fixed_image'] = Port(node=nodes['register_distance_map'], name='in_fixed_image', description='')
    nodes['register_distance_map'].in_ports['in_moving_image'] = Port(node=nodes['register_distance_map'], name='in_moving_image', description='')
    nodes['register_distance_map'].in_ports['in_parameters'] = Port(node=nodes['register_distance_map'], name='in_parameters', description='')
    nodes['register_distance_map'].in_ports['in_fixed_mask'] = Port(node=nodes['register_distance_map'], name='in_fixed_mask', description='')
    nodes['register_distance_map'].in_ports['in_moving_mask'] = Port(node=nodes['register_distance_map'], name='in_moving_mask', description='')
    nodes['register_distance_map'].in_ports['in_initial_transform'] = Port(node=nodes['register_distance_map'], name='in_initial_transform', description='')
    nodes['register_distance_map'].in_ports['in_priority'] = Port(node=nodes['register_distance_map'], name='in_priority', description='')
    nodes['register_distance_map'].in_ports['in_threads'] = Port(node=nodes['register_distance_map'], name='in_threads', description='')

    
    nodes['register_distance_map'].out_ports['out_directory'] = Port(node=nodes['register_distance_map'], name='out_directory', description='')
    nodes['register_distance_map'].out_ports['out_transform'] = Port(node=nodes['register_distance_map'], name='out_transform', description='')
    nodes['register_distance_map'].out_ports['out_log_file'] = Port(node=nodes['register_distance_map'], name='out_log_file', description='')
    
    nodes['parameter_files'] = run.create_node(name='parameter_files', type='source', parent=nodes['step_Atlas_Registration'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['parameter_files'].out_ports['out_output'] = Port(node=nodes['parameter_files'], name='out_output', description='')
    
    nodes['const_ThresholdImage_UpperThreshold_0'] = run.create_node(name='const_ThresholdImage_UpperThreshold_0', type='constant', parent=nodes['step_Extract_Fat_Tissue'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_ThresholdImage_UpperThreshold_0'].out_ports['out_output'] = Port(node=nodes['const_ThresholdImage_UpperThreshold_0'], name='out_output', description='')
    
    nodes['calculate_fat_volume'] = run.create_node(name='calculate_fat_volume', type='node', parent=nodes['step_statistics'], no_in_ports=0, no_out_ports=0)

    
    nodes['calculate_fat_volume'].in_ports['in_label'] = Port(node=nodes['calculate_fat_volume'], name='in_label', description='')
    nodes['calculate_fat_volume'].in_ports['in_image'] = Port(node=nodes['calculate_fat_volume'], name='in_image', description='')
    nodes['calculate_fat_volume'].in_ports['in_mask'] = Port(node=nodes['calculate_fat_volume'], name='in_mask', description='')
    nodes['calculate_fat_volume'].in_ports['in_volume'] = Port(node=nodes['calculate_fat_volume'], name='in_volume', description='')
    nodes['calculate_fat_volume'].in_ports['in_stddev'] = Port(node=nodes['calculate_fat_volume'], name='in_stddev', description='')
    nodes['calculate_fat_volume'].in_ports['in_mean'] = Port(node=nodes['calculate_fat_volume'], name='in_mean', description='')
    nodes['calculate_fat_volume'].in_ports['in_selection'] = Port(node=nodes['calculate_fat_volume'], name='in_selection', description='')
    nodes['calculate_fat_volume'].in_ports['in_ddof'] = Port(node=nodes['calculate_fat_volume'], name='in_ddof', description='')
    nodes['calculate_fat_volume'].in_ports['in_flatten'] = Port(node=nodes['calculate_fat_volume'], name='in_flatten', description='')

    
    nodes['calculate_fat_volume'].out_ports['out_statistics'] = Port(node=nodes['calculate_fat_volume'], name='out_statistics', description='')
    
    nodes['atlas_heartmasks'] = run.create_node(name='atlas_heartmasks', type='source', parent=nodes['step_Atlas_Registration'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['atlas_heartmasks'].out_ports['out_output'] = Port(node=nodes['atlas_heartmasks'], name='out_output', description='')
    
    nodes['const_threshold_input_lower_threshold_0'] = run.create_node(name='const_threshold_input_lower_threshold_0', type='constant', parent=nodes['step_PreProcessing'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_threshold_input_lower_threshold_0'].out_ports['out_output'] = Port(node=nodes['const_threshold_input_lower_threshold_0'], name='out_output', description='')
    
    nodes['const_threshold_input_upper_threshold_0'] = run.create_node(name='const_threshold_input_upper_threshold_0', type='constant', parent=nodes['step_PreProcessing'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_threshold_input_upper_threshold_0'].out_ports['out_output'] = Port(node=nodes['const_threshold_input_upper_threshold_0'], name='out_output', description='')
    
    nodes['const_register_distance_map_parameters_0'] = run.create_node(name='const_register_distance_map_parameters_0', type='constant', parent=nodes['step_Atlas_Registration'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_register_distance_map_parameters_0'].out_ports['out_output'] = Port(node=nodes['const_register_distance_map_parameters_0'], name='out_output', description='')
    
    nodes['MaskImage'] = run.create_node(name='MaskImage', type='node', parent=nodes['step_Extract_Fat_Tissue'], no_in_ports=0, no_out_ports=0)

    
    nodes['MaskImage'].in_ports['in_InputImage'] = Port(node=nodes['MaskImage'], name='in_InputImage', description='')
    nodes['MaskImage'].in_ports['in_InputMask'] = Port(node=nodes['MaskImage'], name='in_InputMask', description='')
    nodes['MaskImage'].in_ports['in_Options'] = Port(node=nodes['MaskImage'], name='in_Options', description='')

    
    nodes['MaskImage'].out_ports['out_OutputImage'] = Port(node=nodes['MaskImage'], name='out_OutputImage', description='')
    
    nodes['atlas_distance_maps'] = run.create_node(name='atlas_distance_maps', type='source', parent=nodes['step_Atlas_Registration'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['atlas_distance_maps'].out_ports['out_output'] = Port(node=nodes['atlas_distance_maps'], name='out_output', description='')
    
    nodes['convert_from_dicom'] = run.create_node(name='convert_from_dicom', type='node', parent=nodes['step_PreProcessing'], no_in_ports=0, no_out_ports=0)

    
    nodes['convert_from_dicom'].in_ports['in_dicom_image'] = Port(node=nodes['convert_from_dicom'], name='in_dicom_image', description='')
    nodes['convert_from_dicom'].in_ports['in_component_type'] = Port(node=nodes['convert_from_dicom'], name='in_component_type', description='')
    nodes['convert_from_dicom'].in_ports['in_compression_flag'] = Port(node=nodes['convert_from_dicom'], name='in_compression_flag', description='')
    nodes['convert_from_dicom'].in_ports['in_series_uid'] = Port(node=nodes['convert_from_dicom'], name='in_series_uid', description='')
    nodes['convert_from_dicom'].in_ports['in_restrictions'] = Port(node=nodes['convert_from_dicom'], name='in_restrictions', description='')

    
    nodes['convert_from_dicom'].out_ports['out_image'] = Port(node=nodes['convert_from_dicom'], name='out_image', description='')
    
    nodes['const_ThresholdImage_LowerThreshold_0'] = run.create_node(name='const_ThresholdImage_LowerThreshold_0', type='constant', parent=nodes['step_Extract_Fat_Tissue'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_ThresholdImage_LowerThreshold_0'].out_ports['out_output'] = Port(node=nodes['const_ThresholdImage_LowerThreshold_0'], name='out_output', description='')
    
    nodes['const_MajorityVoting_method_0'] = run.create_node(name='const_MajorityVoting_method_0', type='constant', parent=nodes['step_majority_voting'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_MajorityVoting_method_0'].out_ports['out_output'] = Port(node=nodes['const_MajorityVoting_method_0'], name='out_output', description='')
    
    nodes['const_ThresholdImage_OutsideValue_0'] = run.create_node(name='const_ThresholdImage_OutsideValue_0', type='constant', parent=nodes['step_Extract_Fat_Tissue'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_ThresholdImage_OutsideValue_0'].out_ports['out_output'] = Port(node=nodes['const_ThresholdImage_OutsideValue_0'], name='out_output', description='')
    
    nodes['MajorityVoting'] = run.create_node(name='MajorityVoting', type='node', parent=nodes['step_majority_voting'], no_in_ports=0, no_out_ports=0)

    
    nodes['MajorityVoting'].in_ports['in_images'] = Port(node=nodes['MajorityVoting'], name='in_images', description='')
    nodes['MajorityVoting'].in_ports['in_method'] = Port(node=nodes['MajorityVoting'], name='in_method', description='')
    nodes['MajorityVoting'].in_ports['in_number_of_classes'] = Port(node=nodes['MajorityVoting'], name='in_number_of_classes', description='')
    nodes['MajorityVoting'].in_ports['in_original_labels'] = Port(node=nodes['MajorityVoting'], name='in_original_labels', description='')
    nodes['MajorityVoting'].in_ports['in_substitute_labels'] = Port(node=nodes['MajorityVoting'], name='in_substitute_labels', description='')

    
    nodes['MajorityVoting'].out_ports['out_hard_segment'] = Port(node=nodes['MajorityVoting'], name='out_hard_segment', description='')
    nodes['MajorityVoting'].out_ports['out_soft_segment'] = Port(node=nodes['MajorityVoting'], name='out_soft_segment', description='')
    
    nodes['fat_tissue_output_image'] = run.create_node(name='fat_tissue_output_image', type='sink', parent=nodes['step_Sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['fat_tissue_output_image'].in_ports['in_input'] = Port(node=nodes['fat_tissue_output_image'], name='in_input', description='')

    
    
    nodes['subject_images'] = run.create_node(name='subject_images', type='source', parent=nodes['root'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['subject_images'].out_ports['out_output'] = Port(node=nodes['subject_images'], name='out_output', description='')
    
    nodes['const_ConnectedComponentImageFilter_MinSize_0'] = run.create_node(name='const_ConnectedComponentImageFilter_MinSize_0', type='constant', parent=nodes['step_Extract_Fat_Tissue'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_ConnectedComponentImageFilter_MinSize_0'].out_ports['out_output'] = Port(node=nodes['const_ConnectedComponentImageFilter_MinSize_0'], name='out_output', description='')
    
    nodes['elastix_node'] = run.create_node(name='elastix_node', type='node', parent=nodes['step_Atlas_Registration'], no_in_ports=0, no_out_ports=0)

    
    nodes['elastix_node'].in_ports['in_fixed_image'] = Port(node=nodes['elastix_node'], name='in_fixed_image', description='')
    nodes['elastix_node'].in_ports['in_moving_image'] = Port(node=nodes['elastix_node'], name='in_moving_image', description='')
    nodes['elastix_node'].in_ports['in_parameters'] = Port(node=nodes['elastix_node'], name='in_parameters', description='')
    nodes['elastix_node'].in_ports['in_fixed_mask'] = Port(node=nodes['elastix_node'], name='in_fixed_mask', description='')
    nodes['elastix_node'].in_ports['in_moving_mask'] = Port(node=nodes['elastix_node'], name='in_moving_mask', description='')
    nodes['elastix_node'].in_ports['in_initial_transform'] = Port(node=nodes['elastix_node'], name='in_initial_transform', description='')
    nodes['elastix_node'].in_ports['in_priority'] = Port(node=nodes['elastix_node'], name='in_priority', description='')
    nodes['elastix_node'].in_ports['in_threads'] = Port(node=nodes['elastix_node'], name='in_threads', description='')

    
    nodes['elastix_node'].out_ports['out_directory'] = Port(node=nodes['elastix_node'], name='out_directory', description='')
    nodes['elastix_node'].out_ports['out_transform'] = Port(node=nodes['elastix_node'], name='out_transform', description='')
    nodes['elastix_node'].out_ports['out_log_file'] = Port(node=nodes['elastix_node'], name='out_log_file', description='')
    
    nodes['const_cast_image_to_ushort_component_type_0'] = run.create_node(name='const_cast_image_to_ushort_component_type_0', type='constant', parent=nodes['step_PreProcessing'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_cast_image_to_ushort_component_type_0'].out_ports['out_output'] = Port(node=nodes['const_cast_image_to_ushort_component_type_0'], name='out_output', description='')
    
    nodes['const_morph_open_dimensions_0'] = run.create_node(name='const_morph_open_dimensions_0', type='constant', parent=nodes['step_PreProcessing'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_morph_open_dimensions_0'].out_ports['out_output'] = Port(node=nodes['const_morph_open_dimensions_0'], name='out_output', description='')
    
    nodes['const_elastix_node_threads_0'] = run.create_node(name='const_elastix_node_threads_0', type='constant', parent=nodes['step_Atlas_Registration'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_elastix_node_threads_0'].out_ports['out_output'] = Port(node=nodes['const_elastix_node_threads_0'], name='out_output', description='')
    
    nodes['distance_transform'] = run.create_node(name='distance_transform', type='node', parent=nodes['step_PreProcessing'], no_in_ports=0, no_out_ports=0)

    
    nodes['distance_transform'].in_ports['in_image'] = Port(node=nodes['distance_transform'], name='in_image', description='')
    nodes['distance_transform'].in_ports['in_squared_flag'] = Port(node=nodes['distance_transform'], name='in_squared_flag', description='')
    nodes['distance_transform'].in_ports['in_method'] = Port(node=nodes['distance_transform'], name='in_method', description='')

    
    nodes['distance_transform'].out_ports['out_image'] = Port(node=nodes['distance_transform'], name='out_image', description='')
    
    nodes['atlas_images'] = run.create_node(name='atlas_images', type='source', parent=nodes['step_Atlas_Registration'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['atlas_images'].out_ports['out_output'] = Port(node=nodes['atlas_images'], name='out_output', description='')
    
    nodes['pericardium_output_image'] = run.create_node(name='pericardium_output_image', type='sink', parent=nodes['root'], no_in_ports=0, no_out_ports=0)

    
    nodes['pericardium_output_image'].in_ports['in_input'] = Port(node=nodes['pericardium_output_image'], name='in_input', description='')

    
    
    nodes['ConnectedComponentImageFilter'] = run.create_node(name='ConnectedComponentImageFilter', type='node', parent=nodes['step_Extract_Fat_Tissue'], no_in_ports=0, no_out_ports=0)

    
    nodes['ConnectedComponentImageFilter'].in_ports['in_InputImage'] = Port(node=nodes['ConnectedComponentImageFilter'], name='in_InputImage', description='')
    nodes['ConnectedComponentImageFilter'].in_ports['in_MinSize'] = Port(node=nodes['ConnectedComponentImageFilter'], name='in_MinSize', description='')

    
    nodes['ConnectedComponentImageFilter'].out_ports['out_OutputImage'] = Port(node=nodes['ConnectedComponentImageFilter'], name='out_OutputImage', description='')
    
    nodes['const_register_distance_map_threads_0'] = run.create_node(name='const_register_distance_map_threads_0', type='constant', parent=nodes['step_Atlas_Registration'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_register_distance_map_threads_0'].out_ports['out_output'] = Port(node=nodes['const_register_distance_map_threads_0'], name='out_output', description='')
    
    nodes['threshold_input'] = run.create_node(name='threshold_input', type='node', parent=nodes['step_PreProcessing'], no_in_ports=0, no_out_ports=0)

    
    nodes['threshold_input'].in_ports['in_image'] = Port(node=nodes['threshold_input'], name='in_image', description='')
    nodes['threshold_input'].in_ports['in_method'] = Port(node=nodes['threshold_input'], name='in_method', description='')
    nodes['threshold_input'].in_ports['in_mask'] = Port(node=nodes['threshold_input'], name='in_mask', description='')
    nodes['threshold_input'].in_ports['in_lower_threshold'] = Port(node=nodes['threshold_input'], name='in_lower_threshold', description='')
    nodes['threshold_input'].in_ports['in_upper_threshold'] = Port(node=nodes['threshold_input'], name='in_upper_threshold', description='')
    nodes['threshold_input'].in_ports['in_inside_value'] = Port(node=nodes['threshold_input'], name='in_inside_value', description='')
    nodes['threshold_input'].in_ports['in_outside_value'] = Port(node=nodes['threshold_input'], name='in_outside_value', description='')
    nodes['threshold_input'].in_ports['in_number_of_thresholds'] = Port(node=nodes['threshold_input'], name='in_number_of_thresholds', description='')
    nodes['threshold_input'].in_ports['in_number_of_histbins'] = Port(node=nodes['threshold_input'], name='in_number_of_histbins', description='')
    nodes['threshold_input'].in_ports['in_radius'] = Port(node=nodes['threshold_input'], name='in_radius', description='')
    nodes['threshold_input'].in_ports['in_number_of_controlpoints'] = Port(node=nodes['threshold_input'], name='in_number_of_controlpoints', description='')
    nodes['threshold_input'].in_ports['in_number_of_levels'] = Port(node=nodes['threshold_input'], name='in_number_of_levels', description='')
    nodes['threshold_input'].in_ports['in_number_of_samples'] = Port(node=nodes['threshold_input'], name='in_number_of_samples', description='')
    nodes['threshold_input'].in_ports['in_spline_order'] = Port(node=nodes['threshold_input'], name='in_spline_order', description='')
    nodes['threshold_input'].in_ports['in_power'] = Port(node=nodes['threshold_input'], name='in_power', description='')
    nodes['threshold_input'].in_ports['in_sigma'] = Port(node=nodes['threshold_input'], name='in_sigma', description='')
    nodes['threshold_input'].in_ports['in_number_of_iterations'] = Port(node=nodes['threshold_input'], name='in_number_of_iterations', description='')
    nodes['threshold_input'].in_ports['in_mask_value'] = Port(node=nodes['threshold_input'], name='in_mask_value', description='')
    nodes['threshold_input'].in_ports['in_mixture_type'] = Port(node=nodes['threshold_input'], name='in_mixture_type', description='')
    nodes['threshold_input'].in_ports['in_compression_flag'] = Port(node=nodes['threshold_input'], name='in_compression_flag', description='')

    
    nodes['threshold_input'].out_ports['out_image'] = Port(node=nodes['threshold_input'], name='out_image', description='')
    
    nodes['const_morph_open_kernel_size_0'] = run.create_node(name='const_morph_open_kernel_size_0', type='constant', parent=nodes['step_PreProcessing'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_morph_open_kernel_size_0'].out_ports['out_output'] = Port(node=nodes['const_morph_open_kernel_size_0'], name='out_output', description='')
    
    nodes['transformix_node'] = run.create_node(name='transformix_node', type='node', parent=nodes['step_Atlas_Registration'], no_in_ports=0, no_out_ports=0)

    
    nodes['transformix_node'].in_ports['in_transform'] = Port(node=nodes['transformix_node'], name='in_transform', description='')
    nodes['transformix_node'].in_ports['in_image'] = Port(node=nodes['transformix_node'], name='in_image', description='')
    nodes['transformix_node'].in_ports['in_points'] = Port(node=nodes['transformix_node'], name='in_points', description='')
    nodes['transformix_node'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['transformix_node'], name='in_determinant_of_jacobian_flag', description='')
    nodes['transformix_node'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['transformix_node'], name='in_jacobian_matrix_flag', description='')
    nodes['transformix_node'].in_ports['in_priority'] = Port(node=nodes['transformix_node'], name='in_priority', description='')
    nodes['transformix_node'].in_ports['in_threads'] = Port(node=nodes['transformix_node'], name='in_threads', description='')

    
    nodes['transformix_node'].out_ports['out_directory'] = Port(node=nodes['transformix_node'], name='out_directory', description='')
    nodes['transformix_node'].out_ports['out_image'] = Port(node=nodes['transformix_node'], name='out_image', description='')
    nodes['transformix_node'].out_ports['out_points'] = Port(node=nodes['transformix_node'], name='out_points', description='')
    nodes['transformix_node'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['transformix_node'], name='out_determinant_of_jacobian', description='')
    nodes['transformix_node'].out_ports['out_jacobian_matrix'] = Port(node=nodes['transformix_node'], name='out_jacobian_matrix', description='')
    nodes['transformix_node'].out_ports['out_log_file'] = Port(node=nodes['transformix_node'], name='out_log_file', description='')
    
    nodes['const_MaskImage_Options_0'] = run.create_node(name='const_MaskImage_Options_0', type='constant', parent=nodes['step_Extract_Fat_Tissue'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_MaskImage_Options_0'].out_ports['out_output'] = Port(node=nodes['const_MaskImage_Options_0'], name='out_output', description='')
    
    nodes['ConvertOFFtoMask'] = run.create_node(name='ConvertOFFtoMask', type='node', parent=nodes['step_majority_voting'], no_in_ports=0, no_out_ports=0)

    
    nodes['ConvertOFFtoMask'].in_ports['in_InputSurface'] = Port(node=nodes['ConvertOFFtoMask'], name='in_InputSurface', description='')
    nodes['ConvertOFFtoMask'].in_ports['in_InputImage'] = Port(node=nodes['ConvertOFFtoMask'], name='in_InputImage', description='')
    nodes['ConvertOFFtoMask'].in_ports['in_Intersections'] = Port(node=nodes['ConvertOFFtoMask'], name='in_Intersections', description='')

    
    nodes['ConvertOFFtoMask'].out_ports['out_OutputImage'] = Port(node=nodes['ConvertOFFtoMask'], name='out_OutputImage', description='')
    
    nodes['ThresholdImage'] = run.create_node(name='ThresholdImage', type='node', parent=nodes['step_Extract_Fat_Tissue'], no_in_ports=0, no_out_ports=0)

    
    nodes['ThresholdImage'].in_ports['in_InputImage'] = Port(node=nodes['ThresholdImage'], name='in_InputImage', description='')
    nodes['ThresholdImage'].in_ports['in_LowerThreshold'] = Port(node=nodes['ThresholdImage'], name='in_LowerThreshold', description='')
    nodes['ThresholdImage'].in_ports['in_UpperThreshold'] = Port(node=nodes['ThresholdImage'], name='in_UpperThreshold', description='')
    nodes['ThresholdImage'].in_ports['in_OutsideValue'] = Port(node=nodes['ThresholdImage'], name='in_OutsideValue', description='')
    nodes['ThresholdImage'].in_ports['in_InsideValue'] = Port(node=nodes['ThresholdImage'], name='in_InsideValue', description='')

    
    nodes['ThresholdImage'].out_ports['out_OutputImage'] = Port(node=nodes['ThresholdImage'], name='out_OutputImage', description='')
    
    nodes['morph_open'] = run.create_node(name='morph_open', type='node', parent=nodes['step_PreProcessing'], no_in_ports=0, no_out_ports=0)

    
    nodes['morph_open'].in_ports['in_image'] = Port(node=nodes['morph_open'], name='in_image', description='')
    nodes['morph_open'].in_ports['in_dimensions'] = Port(node=nodes['morph_open'], name='in_dimensions', description='')
    nodes['morph_open'].in_ports['in_kernel_size'] = Port(node=nodes['morph_open'], name='in_kernel_size', description='')

    
    nodes['morph_open'].out_ports['out_image'] = Port(node=nodes['morph_open'], name='out_image', description='')
    
    nodes['const_distance_transform_method_0'] = run.create_node(name='const_distance_transform_method_0', type='constant', parent=nodes['step_PreProcessing'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_distance_transform_method_0'].out_ports['out_output'] = Port(node=nodes['const_distance_transform_method_0'], name='out_output', description='')
    
    nodes['ground_truth'] = run.create_node(name='ground_truth', type='source', parent=nodes['root'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['ground_truth'].out_ports['out_output'] = Port(node=nodes['ground_truth'], name='out_output', description='')
    
    nodes['const_threshold_input_outside_value_0'] = run.create_node(name='const_threshold_input_outside_value_0', type='constant', parent=nodes['step_PreProcessing'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_threshold_input_outside_value_0'].out_ports['out_output'] = Port(node=nodes['const_threshold_input_outside_value_0'], name='out_output', description='')
    
    nodes['atlas_surfaces'] = run.create_node(name='atlas_surfaces', type='source', parent=nodes['step_Atlas_Registration'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['atlas_surfaces'].out_ports['out_output'] = Port(node=nodes['atlas_surfaces'], name='out_output', description='')
    
    nodes['const_transformix_node_threads_0'] = run.create_node(name='const_transformix_node_threads_0', type='constant', parent=nodes['step_Atlas_Registration'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_transformix_node_threads_0'].out_ports['out_output'] = Port(node=nodes['const_transformix_node_threads_0'], name='out_output', description='')
    
    nodes['const_threshold_input_method_0'] = run.create_node(name='const_threshold_input_method_0', type='constant', parent=nodes['step_PreProcessing'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_threshold_input_method_0'].out_ports['out_output'] = Port(node=nodes['const_threshold_input_method_0'], name='out_output', description='')
    
    nodes['const_threshold_input_inside_value_0'] = run.create_node(name='const_threshold_input_inside_value_0', type='constant', parent=nodes['step_PreProcessing'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_threshold_input_inside_value_0'].out_ports['out_output'] = Port(node=nodes['const_threshold_input_inside_value_0'], name='out_output', description='')
    
    nodes['const_ThresholdImage_InsideValue_0'] = run.create_node(name='const_ThresholdImage_InsideValue_0', type='constant', parent=nodes['step_Extract_Fat_Tissue'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_ThresholdImage_InsideValue_0'].out_ports['out_output'] = Port(node=nodes['const_ThresholdImage_InsideValue_0'], name='out_output', description='')
    
    nodes['const_ConvertOFFtoMask_Intersections_0'] = run.create_node(name='const_ConvertOFFtoMask_Intersections_0', type='constant', parent=nodes['step_majority_voting'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_ConvertOFFtoMask_Intersections_0'].out_ports['out_output'] = Port(node=nodes['const_ConvertOFFtoMask_Intersections_0'], name='out_output', description='')
    
    nodes['cast_image_to_ushort'] = run.create_node(name='cast_image_to_ushort', type='node', parent=nodes['step_PreProcessing'], no_in_ports=0, no_out_ports=0)

    
    nodes['cast_image_to_ushort'].in_ports['in_image'] = Port(node=nodes['cast_image_to_ushort'], name='in_image', description='')
    nodes['cast_image_to_ushort'].in_ports['in_component_type'] = Port(node=nodes['cast_image_to_ushort'], name='in_component_type', description='')
    nodes['cast_image_to_ushort'].in_ports['in_compression_flag'] = Port(node=nodes['cast_image_to_ushort'], name='in_compression_flag', description='')

    
    nodes['cast_image_to_ushort'].out_ports['out_image'] = Port(node=nodes['cast_image_to_ushort'], name='out_image', description='')
    
    nodes['ConvertVTKtoOFF'] = run.create_node(name='ConvertVTKtoOFF', type='node', parent=nodes['step_majority_voting'], no_in_ports=0, no_out_ports=0)

    
    nodes['ConvertVTKtoOFF'].in_ports['in_Input'] = Port(node=nodes['ConvertVTKtoOFF'], name='in_Input', description='')

    
    nodes['ConvertVTKtoOFF'].out_ports['out_Output'] = Port(node=nodes['ConvertVTKtoOFF'], name='out_Output', description='')
    
    nodes['volume_measures'] = run.create_node(name='volume_measures', type='sink', parent=nodes['root'], no_in_ports=0, no_out_ports=0)

    
    nodes['volume_measures'].in_ports['in_input'] = Port(node=nodes['volume_measures'], name='in_input', description='')

    
    
    nodes['const_MajorityVoting_number_of_classes_0'] = run.create_node(name='const_MajorityVoting_number_of_classes_0', type='constant', parent=nodes['step_majority_voting'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_MajorityVoting_number_of_classes_0'].out_ports['out_output'] = Port(node=nodes['const_MajorityVoting_number_of_classes_0'], name='out_output', description='')
    
    nodes['heartmask'] = run.create_node(name='heartmask', type='sink', parent=nodes['step_Sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['heartmask'].in_ports['in_input'] = Port(node=nodes['heartmask'], name='in_input', description='')

    
    

    # Links
    run.links.append(Link(from_node=nodes['ConvertVTKtoOFF'], to_node=nodes['ConvertOFFtoMask'], data_type='OFFObjectFile'))
    run.links.append(Link(from_node=nodes['convert_from_dicom'], to_node=nodes['ConvertOFFtoMask'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_elastix_node_threads_0'], to_node=nodes['elastix_node'], data_type='Int'))
    run.links.append(Link(from_node=nodes['register_distance_map'], to_node=nodes['elastix_node'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['atlas_heartmasks'], to_node=nodes['elastix_node'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['parameter_files'], to_node=nodes['elastix_node'], data_type='ElastixParameterFile'))
    run.links.append(Link(from_node=nodes['const_transformix_node_threads_0'], to_node=nodes['transformix_node'], data_type='Int'))
    run.links.append(Link(from_node=nodes['transformix_node'], to_node=nodes['ConvertVTKtoOFF'], data_type='VTKDataFile'))
    run.links.append(Link(from_node=nodes['elastix_node'], to_node=nodes['transformix_node'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['atlas_surfaces'], to_node=nodes['transformix_node'], data_type='VTKDataFile'))
    run.links.append(Link(from_node=nodes['ConnectedComponentImageFilter'], to_node=nodes['fat_tissue_output_image'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['ConnectedComponentImageFilter'], to_node=nodes['calculate_fat_volume'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_ConnectedComponentImageFilter_MinSize_0'], to_node=nodes['ConnectedComponentImageFilter'], data_type='Float'))
    run.links.append(Link(from_node=nodes['const_ThresholdImage_LowerThreshold_0'], to_node=nodes['ThresholdImage'], data_type='Float'))
    run.links.append(Link(from_node=nodes['calculate_fat_volume'], to_node=nodes['volume_measures'], data_type='CsvFile'))
    run.links.append(Link(from_node=nodes['MajorityVoting'], to_node=nodes['heartmask'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_ThresholdImage_UpperThreshold_0'], to_node=nodes['ThresholdImage'], data_type='Float'))
    run.links.append(Link(from_node=nodes['const_ThresholdImage_OutsideValue_0'], to_node=nodes['ThresholdImage'], data_type='Float'))
    run.links.append(Link(from_node=nodes['const_ThresholdImage_InsideValue_0'], to_node=nodes['ThresholdImage'], data_type='Float'))
    run.links.append(Link(from_node=nodes['MaskImage'], to_node=nodes['ThresholdImage'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['ThresholdImage'], to_node=nodes['ConnectedComponentImageFilter'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_MaskImage_Options_0'], to_node=nodes['MaskImage'], data_type='String'))
    run.links.append(Link(from_node=nodes['const_distance_transform_method_0'], to_node=nodes['distance_transform'], data_type='__PxDistanceTransform_0.3.2_interface__method__Enum__'))
    run.links.append(Link(from_node=nodes['morph_open'], to_node=nodes['distance_transform'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_morph_open_kernel_size_0'], to_node=nodes['morph_open'], data_type='Int'))
    run.links.append(Link(from_node=nodes['const_morph_open_dimensions_0'], to_node=nodes['morph_open'], data_type='Int'))
    run.links.append(Link(from_node=nodes['const_register_distance_map_threads_0'], to_node=nodes['register_distance_map'], data_type='Int'))
    run.links.append(Link(from_node=nodes['const_register_distance_map_parameters_0'], to_node=nodes['register_distance_map'], data_type='ElastixParameterFile'))
    run.links.append(Link(from_node=nodes['distance_transform'], to_node=nodes['register_distance_map'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['atlas_distance_maps'], to_node=nodes['register_distance_map'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['convert_from_dicom'], to_node=nodes['MaskImage'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['MajorityVoting'], to_node=nodes['pericardium_output_image'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['convert_from_dicom'], to_node=nodes['elastix_node'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['atlas_images'], to_node=nodes['elastix_node'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_MajorityVoting_method_0'], to_node=nodes['MajorityVoting'], data_type='__PxCombineSegmentations_0.3.0_interface__method__Enum__'))
    run.links.append(Link(from_node=nodes['const_ConvertOFFtoMask_Intersections_0'], to_node=nodes['ConvertOFFtoMask'], data_type='Int'))
    run.links.append(Link(from_node=nodes['ConvertOFFtoMask'], to_node=nodes['MajorityVoting'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_MajorityVoting_number_of_classes_0'], to_node=nodes['MajorityVoting'], data_type='Int'))
    run.links.append(Link(from_node=nodes['MajorityVoting'], to_node=nodes['MaskImage'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['threshold_input'], to_node=nodes['morph_open'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_threshold_input_outside_value_0'], to_node=nodes['threshold_input'], data_type='Float'))
    run.links.append(Link(from_node=nodes['cast_image_to_ushort'], to_node=nodes['threshold_input'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_cast_image_to_ushort_component_type_0'], to_node=nodes['cast_image_to_ushort'], data_type='__PxCastConvert_0.3.2_interface__component_type__Enum__'))
    run.links.append(Link(from_node=nodes['convert_from_dicom'], to_node=nodes['cast_image_to_ushort'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['subject_images'], to_node=nodes['convert_from_dicom'], data_type='DicomImageFile'))
    run.links.append(Link(from_node=nodes['const_threshold_input_inside_value_0'], to_node=nodes['threshold_input'], data_type='Float'))
    run.links.append(Link(from_node=nodes['const_threshold_input_upper_threshold_0'], to_node=nodes['threshold_input'], data_type='Float'))
    run.links.append(Link(from_node=nodes['const_threshold_input_lower_threshold_0'], to_node=nodes['threshold_input'], data_type='Float'))
    run.links.append(Link(from_node=nodes['const_threshold_input_method_0'], to_node=nodes['threshold_input'], data_type='__PxThresholdImage_0.3.0_interface__method__Enum__'))

    return run

