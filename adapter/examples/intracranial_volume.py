import os

from adapter.link import Link
from adapter.run import Run
from adapter.port import Port


def create_run(**kwargs):
    run = Run()
    run.name = 'intracranial_volume'

    os.environ['RUN_NAME'] = run.name

    nodes = dict()

    nodes['root'] = run.create_node(name='root', parent=None)

    run.root = nodes['root']

    
    nodes['0_Program_Basic_Arguments'] = run.create_node(name='0_Program_Basic_Arguments', description='Leaf', parent=nodes['root'])
    nodes['1_Program_Linear_Registration_Arguments'] = run.create_node(name='1_Program_Linear_Registration_Arguments', description='Leaf', parent=nodes['root'])
    nodes['2_Subject_Name'] = run.create_node(name='2_Subject_Name', description='Leaf', parent=nodes['root'])
    nodes['3_Program_Extended_Arguments'] = run.create_node(name='3_Program_Extended_Arguments', description='Leaf', parent=nodes['root'])
    nodes['4_ICV_Files_Outputs'] = run.create_node(name='4_ICV_Files_Outputs', description='Leaf', parent=nodes['root'])
    nodes['5_Main_Operation'] = run.create_node(name='5_Main_Operation', description='Leaf', parent=nodes['root'])
    nodes['6_Operation_Type'] = run.create_node(name='6_Operation_Type', description='Leaf', parent=nodes['root'])
    nodes['7_Program_File_Arguments'] = run.create_node(name='7_Program_File_Arguments', description='Leaf', parent=nodes['root'])
    nodes['8_Model_Name'] = run.create_node(name='8_Model_Name', description='Leaf', parent=nodes['root'])

    
    nodes['9_Input_To_Process'] = run.create_node(name='9_Input_To_Process', type='source', parent=nodes['7_Program_File_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['9_Input_To_Process'].out_ports['out_output'] = Port(node=nodes['9_Input_To_Process'], name='out_output', description='')
    
    nodes['10_Linear_Estimate'] = run.create_node(name='10_Linear_Estimate', type='source', parent=nodes['1_Program_Linear_Registration_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['10_Linear_Estimate'].out_ports['out_output'] = Port(node=nodes['10_Linear_Estimate'], name='out_output', description='')
    
    nodes['11_ICV_Original_Subject'] = run.create_node(name='11_ICV_Original_Subject', type='sink', parent=nodes['4_ICV_Files_Outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['11_ICV_Original_Subject'].in_ports['in_input'] = Port(node=nodes['11_ICV_Original_Subject'], name='in_input', description='')

    
    
    nodes['12_Num_Of_Registrations'] = run.create_node(name='12_Num_Of_Registrations', type='source', parent=nodes['1_Program_Linear_Registration_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['12_Num_Of_Registrations'].out_ports['out_output'] = Port(node=nodes['12_Num_Of_Registrations'], name='out_output', description='')
    
    nodes['13_Sub_lattice'] = run.create_node(name='13_Sub_lattice', type='source', parent=nodes['3_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['13_Sub_lattice'].out_ports['out_output'] = Port(node=nodes['13_Sub_lattice'], name='out_output', description='')
    
    nodes['14_ICV_Output_Log'] = run.create_node(name='14_ICV_Output_Log', type='sink', parent=nodes['4_ICV_Files_Outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['14_ICV_Output_Log'].in_ports['in_input'] = Port(node=nodes['14_ICV_Output_Log'], name='in_input', description='')

    
    
    nodes['15_Mask_To_Binary_Min'] = run.create_node(name='15_Mask_To_Binary_Min', type='source', parent=nodes['3_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['15_Mask_To_Binary_Min'].out_ports['out_output'] = Port(node=nodes['15_Mask_To_Binary_Min'], name='out_output', description='')
    
    nodes['16_ICV_Output_ICV_Mask'] = run.create_node(name='16_ICV_Output_ICV_Mask', type='sink', parent=nodes['4_ICV_Files_Outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['16_ICV_Output_ICV_Mask'].in_ports['in_input'] = Port(node=nodes['16_ICV_Output_ICV_Mask'], name='in_input', description='')

    
    
    nodes['17_ICV_Output_ICV_JPG'] = run.create_node(name='17_ICV_Output_ICV_JPG', type='sink', parent=nodes['4_ICV_Files_Outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['17_ICV_Output_ICV_JPG'].in_ports['in_input'] = Port(node=nodes['17_ICV_Output_ICV_JPG'], name='in_input', description='')

    
    
    nodes['18_Blur_FWHM'] = run.create_node(name='18_Blur_FWHM', type='source', parent=nodes['0_Program_Basic_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['18_Blur_FWHM'].out_ports['out_output'] = Port(node=nodes['18_Blur_FWHM'], name='out_output', description='')
    
    nodes['19_Calc_Crude'] = run.create_node(name='19_Calc_Crude', type='source', parent=nodes['6_Operation_Type'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['19_Calc_Crude'].out_ports['out_output'] = Port(node=nodes['19_Calc_Crude'], name='out_output', description='')
    
    nodes['20_Non_linear'] = run.create_node(name='20_Non_linear', type='source', parent=nodes['3_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['20_Non_linear'].out_ports['out_output'] = Port(node=nodes['20_Non_linear'], name='out_output', description='')
    
    nodes['21_Clobber'] = run.create_node(name='21_Clobber', type='source', parent=nodes['3_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['21_Clobber'].out_ports['out_output'] = Port(node=nodes['21_Clobber'], name='out_output', description='')
    
    nodes['22_ICV_Output_ICV_Parameters'] = run.create_node(name='22_ICV_Output_ICV_Parameters', type='sink', parent=nodes['4_ICV_Files_Outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['22_ICV_Output_ICV_Parameters'].in_ports['in_input'] = Port(node=nodes['22_ICV_Output_ICV_Parameters'], name='in_input', description='')

    
    
    nodes['23_Stiffness'] = run.create_node(name='23_Stiffness', type='source', parent=nodes['3_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['23_Stiffness'].out_ports['out_output'] = Port(node=nodes['23_Stiffness'], name='out_output', description='')
    
    nodes['24_Debug'] = run.create_node(name='24_Debug', type='source', parent=nodes['3_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['24_Debug'].out_ports['out_output'] = Port(node=nodes['24_Debug'], name='out_output', description='')
    
    nodes['25_Model'] = run.create_node(name='25_Model', type='source', parent=nodes['8_Model_Name'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['25_Model'].out_ports['out_output'] = Port(node=nodes['25_Model'], name='out_output', description='')
    
    nodes['26_IntracranialVolume'] = run.create_node(name='26_IntracranialVolume', type='node', parent=nodes['5_Main_Operation'], no_in_ports=0, no_out_ports=0)

    
    nodes['26_IntracranialVolume'].in_ports['in_Help_Argument'] = Port(node=nodes['26_IntracranialVolume'], name='in_Help_Argument', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Model'] = Port(node=nodes['26_IntracranialVolume'], name='in_Model', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Parameters_File'] = Port(node=nodes['26_IntracranialVolume'], name='in_Parameters_File', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Processed_File'] = Port(node=nodes['26_IntracranialVolume'], name='in_Processed_File', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Processed_Folder'] = Port(node=nodes['26_IntracranialVolume'], name='in_Processed_Folder', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Processed_XNAT_DICOM'] = Port(node=nodes['26_IntracranialVolume'], name='in_Processed_XNAT_DICOM', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Linear_Estimate'] = Port(node=nodes['26_IntracranialVolume'], name='in_Linear_Estimate', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Num_Of_Registrations'] = Port(node=nodes['26_IntracranialVolume'], name='in_Num_Of_Registrations', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Registration_Blur_FWHM'] = Port(node=nodes['26_IntracranialVolume'], name='in_Registration_Blur_FWHM', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Num_Of_Iterations'] = Port(node=nodes['26_IntracranialVolume'], name='in_Num_Of_Iterations', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Step_Size'] = Port(node=nodes['26_IntracranialVolume'], name='in_Step_Size', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Blur_FWHM'] = Port(node=nodes['26_IntracranialVolume'], name='in_Blur_FWHM', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Weight'] = Port(node=nodes['26_IntracranialVolume'], name='in_Weight', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Stiffness'] = Port(node=nodes['26_IntracranialVolume'], name='in_Stiffness', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Similarity'] = Port(node=nodes['26_IntracranialVolume'], name='in_Similarity', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Sub_lattice'] = Port(node=nodes['26_IntracranialVolume'], name='in_Sub_lattice', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Debug'] = Port(node=nodes['26_IntracranialVolume'], name='in_Debug', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Clobber'] = Port(node=nodes['26_IntracranialVolume'], name='in_Clobber', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Non_linear'] = Port(node=nodes['26_IntracranialVolume'], name='in_Non_linear', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Mask_To_Binary_Min'] = Port(node=nodes['26_IntracranialVolume'], name='in_Mask_To_Binary_Min', description='')
    nodes['26_IntracranialVolume'].in_ports['in_RUN_GUI'] = Port(node=nodes['26_IntracranialVolume'], name='in_RUN_GUI', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Calc_Crude'] = Port(node=nodes['26_IntracranialVolume'], name='in_Calc_Crude', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Calc_Fine'] = Port(node=nodes['26_IntracranialVolume'], name='in_Calc_Fine', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Clac_Ave_Brain'] = Port(node=nodes['26_IntracranialVolume'], name='in_Clac_Ave_Brain', description='')
    nodes['26_IntracranialVolume'].in_ports['in_Subject_Name'] = Port(node=nodes['26_IntracranialVolume'], name='in_Subject_Name', description='')

    
    nodes['26_IntracranialVolume'].out_ports['out_icv_directory'] = Port(node=nodes['26_IntracranialVolume'], name='out_icv_directory', description='')
    nodes['26_IntracranialVolume'].out_ports['out_icv_output_nii_mask'] = Port(node=nodes['26_IntracranialVolume'], name='out_icv_output_nii_mask', description='')
    nodes['26_IntracranialVolume'].out_ports['out_icv_output_mnc_mask'] = Port(node=nodes['26_IntracranialVolume'], name='out_icv_output_mnc_mask', description='')
    nodes['26_IntracranialVolume'].out_ports['out_icv_volume'] = Port(node=nodes['26_IntracranialVolume'], name='out_icv_volume', description='')
    nodes['26_IntracranialVolume'].out_ports['out_icv_parameters_used'] = Port(node=nodes['26_IntracranialVolume'], name='out_icv_parameters_used', description='')
    nodes['26_IntracranialVolume'].out_ports['out_icv_log'] = Port(node=nodes['26_IntracranialVolume'], name='out_icv_log', description='')
    nodes['26_IntracranialVolume'].out_ports['out_icv_jpg'] = Port(node=nodes['26_IntracranialVolume'], name='out_icv_jpg', description='')
    nodes['26_IntracranialVolume'].out_ports['out_icv_original_file_In_nii'] = Port(node=nodes['26_IntracranialVolume'], name='out_icv_original_file_In_nii', description='')
    
    nodes['27_Registration_Blur_FWHM'] = run.create_node(name='27_Registration_Blur_FWHM', type='source', parent=nodes['1_Program_Linear_Registration_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['27_Registration_Blur_FWHM'].out_ports['out_output'] = Port(node=nodes['27_Registration_Blur_FWHM'], name='out_output', description='')
    
    nodes['28_Num_Of_Iterations'] = run.create_node(name='28_Num_Of_Iterations', type='source', parent=nodes['0_Program_Basic_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['28_Num_Of_Iterations'].out_ports['out_output'] = Port(node=nodes['28_Num_Of_Iterations'], name='out_output', description='')
    
    nodes['29_Weight'] = run.create_node(name='29_Weight', type='source', parent=nodes['3_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['29_Weight'].out_ports['out_output'] = Port(node=nodes['29_Weight'], name='out_output', description='')
    
    nodes['30_Similarity'] = run.create_node(name='30_Similarity', type='source', parent=nodes['3_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['30_Similarity'].out_ports['out_output'] = Port(node=nodes['30_Similarity'], name='out_output', description='')
    
    nodes['31_ICV_Output_ICV_Volume'] = run.create_node(name='31_ICV_Output_ICV_Volume', type='sink', parent=nodes['4_ICV_Files_Outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['31_ICV_Output_ICV_Volume'].in_ports['in_input'] = Port(node=nodes['31_ICV_Output_ICV_Volume'], name='in_input', description='')

    
    
    nodes['32_ICV_Subject_Name'] = run.create_node(name='32_ICV_Subject_Name', type='source', parent=nodes['2_Subject_Name'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['32_ICV_Subject_Name'].out_ports['out_output'] = Port(node=nodes['32_ICV_Subject_Name'], name='out_output', description='')
    
    nodes['33_Step_Size'] = run.create_node(name='33_Step_Size', type='source', parent=nodes['0_Program_Basic_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['33_Step_Size'].out_ports['out_output'] = Port(node=nodes['33_Step_Size'], name='out_output', description='')
    

    # Links
    run.links.append(Link(from_node=nodes['26_IntracranialVolume'], to_node=nodes['17_ICV_Output_ICV_JPG'], data_type='JPGFile'))
    run.links.append(Link(from_node=nodes['26_IntracranialVolume'], to_node=nodes['11_ICV_Original_Subject'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['26_IntracranialVolume'], to_node=nodes['31_ICV_Output_ICV_Volume'], data_type='TxtFile'))
    run.links.append(Link(from_node=nodes['26_IntracranialVolume'], to_node=nodes['14_ICV_Output_Log'], data_type='TxtFile'))
    run.links.append(Link(from_node=nodes['21_Clobber'], to_node=nodes['26_IntracranialVolume'], data_type='Boolean'))
    run.links.append(Link(from_node=nodes['24_Debug'], to_node=nodes['26_IntracranialVolume'], data_type='Boolean'))
    run.links.append(Link(from_node=nodes['13_Sub_lattice'], to_node=nodes['26_IntracranialVolume'], data_type='Int'))
    run.links.append(Link(from_node=nodes['30_Similarity'], to_node=nodes['26_IntracranialVolume'], data_type='Float'))
    run.links.append(Link(from_node=nodes['32_ICV_Subject_Name'], to_node=nodes['26_IntracranialVolume'], data_type='String'))
    run.links.append(Link(from_node=nodes['19_Calc_Crude'], to_node=nodes['26_IntracranialVolume'], data_type='Boolean'))
    run.links.append(Link(from_node=nodes['15_Mask_To_Binary_Min'], to_node=nodes['26_IntracranialVolume'], data_type='Float'))
    run.links.append(Link(from_node=nodes['20_Non_linear'], to_node=nodes['26_IntracranialVolume'], data_type='String'))
    run.links.append(Link(from_node=nodes['26_IntracranialVolume'], to_node=nodes['22_ICV_Output_ICV_Parameters'], data_type='TxtFile'))
    run.links.append(Link(from_node=nodes['26_IntracranialVolume'], to_node=nodes['16_ICV_Output_ICV_Mask'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['23_Stiffness'], to_node=nodes['26_IntracranialVolume'], data_type='Float'))
    run.links.append(Link(from_node=nodes['29_Weight'], to_node=nodes['26_IntracranialVolume'], data_type='Float'))
    run.links.append(Link(from_node=nodes['12_Num_Of_Registrations'], to_node=nodes['26_IntracranialVolume'], data_type='Int'))
    run.links.append(Link(from_node=nodes['10_Linear_Estimate'], to_node=nodes['26_IntracranialVolume'], data_type='String'))
    run.links.append(Link(from_node=nodes['9_Input_To_Process'], to_node=nodes['26_IntracranialVolume'], data_type='DicomImageFile'))
    run.links.append(Link(from_node=nodes['25_Model'], to_node=nodes['26_IntracranialVolume'], data_type='String'))
    run.links.append(Link(from_node=nodes['18_Blur_FWHM'], to_node=nodes['26_IntracranialVolume'], data_type='String'))
    run.links.append(Link(from_node=nodes['33_Step_Size'], to_node=nodes['26_IntracranialVolume'], data_type='String'))
    run.links.append(Link(from_node=nodes['28_Num_Of_Iterations'], to_node=nodes['26_IntracranialVolume'], data_type='String'))
    run.links.append(Link(from_node=nodes['27_Registration_Blur_FWHM'], to_node=nodes['26_IntracranialVolume'], data_type='Int'))

    return run

