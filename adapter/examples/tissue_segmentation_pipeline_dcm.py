import os

from adapter.link import Link
from adapter.run import Run
from adapter.port import Port


def create_run(**kwargs):
    run = Run()
    run.name = 'tissue_segmentation_pipeline_dcm'

    os.environ['RUN_NAME'] = run.name

    nodes = dict()

    nodes['root'] = run.create_node(name='root', parent=None)

    run.root = nodes['root']

    
    nodes['0_knnseg'] = run.create_node(name='0_knnseg', description='Leaf', parent=nodes['root'])
    nodes['1_elastix_mask'] = run.create_node(name='1_elastix_mask', description='Leaf', parent=nodes['root'])
    nodes['2_convert'] = run.create_node(name='2_convert', description='Leaf', parent=nodes['root'])
    nodes['3_multi_atlas_reg'] = run.create_node(name='3_multi_atlas_reg', description='Leaf', parent=nodes['root'])
    nodes['4_sinks'] = run.create_node(name='4_sinks', description='Leaf', parent=nodes['root'])
    nodes['5_nii2ana_flip'] = run.create_node(name='5_nii2ana_flip', description='Leaf', parent=nodes['root'])
    nodes['6_finalconvert'] = run.create_node(name='6_finalconvert', description='Leaf', parent=nodes['root'])
    nodes['7_NUC'] = run.create_node(name='7_NUC', description='Leaf', parent=nodes['root'])
    nodes['8_wmlsegment'] = run.create_node(name='8_wmlsegment', description='Leaf', parent=nodes['root'])
    nodes['9_coreg'] = run.create_node(name='9_coreg', description='Leaf', parent=nodes['root'])
    nodes['10_mask2ana'] = run.create_node(name='10_mask2ana', description='Leaf', parent=nodes['root'])
    nodes['11_sources'] = run.create_node(name='11_sources', description='Leaf', parent=nodes['root'])
    nodes['12_brainmaskimprove'] = run.create_node(name='12_brainmaskimprove', description='Leaf', parent=nodes['root'])
    nodes['13_mnc2ana'] = run.create_node(name='13_mnc2ana', description='Leaf', parent=nodes['root'])
    nodes['14_rama'] = run.create_node(name='14_rama', description='Leaf', parent=nodes['root'])
    nodes['15_mnc2ana_flip'] = run.create_node(name='15_mnc2ana_flip', description='Leaf', parent=nodes['root'])

    
    nodes['16_sink_flair'] = run.create_node(name='16_sink_flair', type='sink', parent=nodes['4_sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['16_sink_flair'].in_ports['in_input'] = Port(node=nodes['16_sink_flair'], name='in_input', description='')

    
    
    nodes['17_const_elastix_mask_moving_image_0'] = run.create_node(name='17_const_elastix_mask_moving_image_0', type='constant', parent=nodes['1_elastix_mask'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['17_const_elastix_mask_moving_image_0'].out_ports['out_output'] = Port(node=nodes['17_const_elastix_mask_moving_image_0'], name='out_output', description='')
    
    nodes['18_thresholds'] = run.create_node(name='18_thresholds', type='constant', parent=nodes['9_coreg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['18_thresholds'].out_ports['out_output'] = Port(node=nodes['18_thresholds'], name='out_output', description='')
    
    nodes['19_N3_T1'] = run.create_node(name='19_N3_T1', type='node', parent=nodes['7_NUC'], no_in_ports=0, no_out_ports=0)

    
    nodes['19_N3_T1'].in_ports['in_image'] = Port(node=nodes['19_N3_T1'], name='in_image', description='')
    nodes['19_N3_T1'].in_ports['in_clobber_flag'] = Port(node=nodes['19_N3_T1'], name='in_clobber_flag', description='')
    nodes['19_N3_T1'].in_ports['in_quiet_flag'] = Port(node=nodes['19_N3_T1'], name='in_quiet_flag', description='')
    nodes['19_N3_T1'].in_ports['in_normalize_field_flag'] = Port(node=nodes['19_N3_T1'], name='in_normalize_field_flag', description='')
    nodes['19_N3_T1'].in_ports['in_iterations'] = Port(node=nodes['19_N3_T1'], name='in_iterations', description='')
    nodes['19_N3_T1'].in_ports['in_stop'] = Port(node=nodes['19_N3_T1'], name='in_stop', description='')
    nodes['19_N3_T1'].in_ports['in_mask'] = Port(node=nodes['19_N3_T1'], name='in_mask', description='')

    
    nodes['19_N3_T1'].out_ports['out_image'] = Port(node=nodes['19_N3_T1'], name='out_image', description='')
    
    nodes['20_const_N3_PD_stop_0'] = run.create_node(name='20_const_N3_PD_stop_0', type='constant', parent=nodes['7_NUC'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['20_const_N3_PD_stop_0'].out_ports['out_output'] = Port(node=nodes['20_const_N3_PD_stop_0'], name='out_output', description='')
    
    nodes['21_minc2ana_knnseg_flip'] = run.create_node(name='21_minc2ana_knnseg_flip', type='node', parent=nodes['15_mnc2ana_flip'], no_in_ports=0, no_out_ports=0)

    
    nodes['21_minc2ana_knnseg_flip'].in_ports['in_image'] = Port(node=nodes['21_minc2ana_knnseg_flip'], name='in_image', description='')
    nodes['21_minc2ana_knnseg_flip'].in_ports['in_neuro_flag'] = Port(node=nodes['21_minc2ana_knnseg_flip'], name='in_neuro_flag', description='')

    
    nodes['21_minc2ana_knnseg_flip'].out_ports['out_image'] = Port(node=nodes['21_minc2ana_knnseg_flip'], name='out_image', description='')
    
    nodes['22_const_transformix_mask_image_0'] = run.create_node(name='22_const_transformix_mask_image_0', type='constant', parent=nodes['1_elastix_mask'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['22_const_transformix_mask_image_0'].out_ports['out_output'] = Port(node=nodes['22_const_transformix_mask_image_0'], name='out_output', description='')
    
    nodes['23_sink_knnseg'] = run.create_node(name='23_sink_knnseg', type='sink', parent=nodes['4_sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['23_sink_knnseg'].in_ports['in_input'] = Port(node=nodes['23_sink_knnseg'], name='in_input', description='')

    
    
    nodes['24_ana2nii_lobes'] = run.create_node(name='24_ana2nii_lobes', type='node', parent=nodes['6_finalconvert'], no_in_ports=0, no_out_ports=0)

    
    nodes['24_ana2nii_lobes'].in_ports['in_image'] = Port(node=nodes['24_ana2nii_lobes'], name='in_image', description='')
    nodes['24_ana2nii_lobes'].in_ports['in_reference_image'] = Port(node=nodes['24_ana2nii_lobes'], name='in_reference_image', description='')

    
    nodes['24_ana2nii_lobes'].out_ports['out_image'] = Port(node=nodes['24_ana2nii_lobes'], name='out_image', description='')
    
    nodes['25_multi_atlas_trans_masks_t1'] = run.create_node(name='25_multi_atlas_trans_masks_t1', type='node', parent=nodes['3_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    
    nodes['25_multi_atlas_trans_masks_t1'].in_ports['in_transform'] = Port(node=nodes['25_multi_atlas_trans_masks_t1'], name='in_transform', description='')
    nodes['25_multi_atlas_trans_masks_t1'].in_ports['in_image'] = Port(node=nodes['25_multi_atlas_trans_masks_t1'], name='in_image', description='')
    nodes['25_multi_atlas_trans_masks_t1'].in_ports['in_points'] = Port(node=nodes['25_multi_atlas_trans_masks_t1'], name='in_points', description='')
    nodes['25_multi_atlas_trans_masks_t1'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['25_multi_atlas_trans_masks_t1'], name='in_determinant_of_jacobian_flag', description='')
    nodes['25_multi_atlas_trans_masks_t1'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['25_multi_atlas_trans_masks_t1'], name='in_jacobian_matrix_flag', description='')
    nodes['25_multi_atlas_trans_masks_t1'].in_ports['in_priority'] = Port(node=nodes['25_multi_atlas_trans_masks_t1'], name='in_priority', description='')
    nodes['25_multi_atlas_trans_masks_t1'].in_ports['in_threads'] = Port(node=nodes['25_multi_atlas_trans_masks_t1'], name='in_threads', description='')

    
    nodes['25_multi_atlas_trans_masks_t1'].out_ports['out_directory'] = Port(node=nodes['25_multi_atlas_trans_masks_t1'], name='out_directory', description='')
    nodes['25_multi_atlas_trans_masks_t1'].out_ports['out_image'] = Port(node=nodes['25_multi_atlas_trans_masks_t1'], name='out_image', description='')
    nodes['25_multi_atlas_trans_masks_t1'].out_ports['out_points'] = Port(node=nodes['25_multi_atlas_trans_masks_t1'], name='out_points', description='')
    nodes['25_multi_atlas_trans_masks_t1'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['25_multi_atlas_trans_masks_t1'], name='out_determinant_of_jacobian', description='')
    nodes['25_multi_atlas_trans_masks_t1'].out_ports['out_jacobian_matrix'] = Port(node=nodes['25_multi_atlas_trans_masks_t1'], name='out_jacobian_matrix', description='')
    nodes['25_multi_atlas_trans_masks_t1'].out_ports['out_log_file'] = Port(node=nodes['25_multi_atlas_trans_masks_t1'], name='out_log_file', description='')
    
    nodes['26_const_N3_swi_stop_0'] = run.create_node(name='26_const_N3_swi_stop_0', type='constant', parent=nodes['7_NUC'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['26_const_N3_swi_stop_0'].out_ports['out_output'] = Port(node=nodes['26_const_N3_swi_stop_0'], name='out_output', description='')
    
    nodes['27_const_N3_flair_iterations_0'] = run.create_node(name='27_const_N3_flair_iterations_0', type='constant', parent=nodes['7_NUC'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['27_const_N3_flair_iterations_0'].out_ports['out_output'] = Port(node=nodes['27_const_N3_flair_iterations_0'], name='out_output', description='')
    
    nodes['28_const_N3_T1_iterations_0'] = run.create_node(name='28_const_N3_T1_iterations_0', type='constant', parent=nodes['7_NUC'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['28_const_N3_T1_iterations_0'].out_ports['out_output'] = Port(node=nodes['28_const_N3_T1_iterations_0'], name='out_output', description='')
    
    nodes['29_const_N3_swi_iterations_0'] = run.create_node(name='29_const_N3_swi_iterations_0', type='constant', parent=nodes['7_NUC'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['29_const_N3_swi_iterations_0'].out_ports['out_output'] = Port(node=nodes['29_const_N3_swi_iterations_0'], name='out_output', description='')
    
    nodes['30_minc2ana_pd_flip'] = run.create_node(name='30_minc2ana_pd_flip', type='node', parent=nodes['15_mnc2ana_flip'], no_in_ports=0, no_out_ports=0)

    
    nodes['30_minc2ana_pd_flip'].in_ports['in_image'] = Port(node=nodes['30_minc2ana_pd_flip'], name='in_image', description='')
    nodes['30_minc2ana_pd_flip'].in_ports['in_neuro_flag'] = Port(node=nodes['30_minc2ana_pd_flip'], name='in_neuro_flag', description='')

    
    nodes['30_minc2ana_pd_flip'].out_ports['out_image'] = Port(node=nodes['30_minc2ana_pd_flip'], name='out_image', description='')
    
    nodes['31_sink_knnwml'] = run.create_node(name='31_sink_knnwml', type='sink', parent=nodes['4_sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['31_sink_knnwml'].in_ports['in_input'] = Port(node=nodes['31_sink_knnwml'], name='in_input', description='')

    
    
    nodes['32_ana2nii_pd'] = run.create_node(name='32_ana2nii_pd', type='node', parent=nodes['6_finalconvert'], no_in_ports=0, no_out_ports=0)

    
    nodes['32_ana2nii_pd'].in_ports['in_image'] = Port(node=nodes['32_ana2nii_pd'], name='in_image', description='')
    nodes['32_ana2nii_pd'].in_ports['in_reference_image'] = Port(node=nodes['32_ana2nii_pd'], name='in_reference_image', description='')

    
    nodes['32_ana2nii_pd'].out_ports['out_image'] = Port(node=nodes['32_ana2nii_pd'], name='out_image', description='')
    
    nodes['33_mask2mnc_modify_header'] = run.create_node(name='33_mask2mnc_modify_header', type='node', parent=nodes['10_mask2ana'], no_in_ports=0, no_out_ports=0)

    
    nodes['33_mask2mnc_modify_header'].in_ports['in_image'] = Port(node=nodes['33_mask2mnc_modify_header'], name='in_image', description='')
    nodes['33_mask2mnc_modify_header'].in_ports['in_mask'] = Port(node=nodes['33_mask2mnc_modify_header'], name='in_mask', description='')

    
    nodes['33_mask2mnc_modify_header'].out_ports['out_image'] = Port(node=nodes['33_mask2mnc_modify_header'], name='out_image', description='')
    
    nodes['34_const_rama_flair_const2_0'] = run.create_node(name='34_const_rama_flair_const2_0', type='constant', parent=nodes['14_rama'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['34_const_rama_flair_const2_0'].out_ports['out_output'] = Port(node=nodes['34_const_rama_flair_const2_0'], name='out_output', description='')
    
    nodes['35_sink_mask'] = run.create_node(name='35_sink_mask', type='sink', parent=nodes['4_sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['35_sink_mask'].in_ports['in_input'] = Port(node=nodes['35_sink_mask'], name='in_input', description='')

    
    
    nodes['36_sink_swi'] = run.create_node(name='36_sink_swi', type='sink', parent=nodes['4_sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['36_sink_swi'].in_ports['in_input'] = Port(node=nodes['36_sink_swi'], name='in_input', description='')

    
    
    nodes['37_transformix_mask'] = run.create_node(name='37_transformix_mask', type='node', parent=nodes['1_elastix_mask'], no_in_ports=0, no_out_ports=0)

    
    nodes['37_transformix_mask'].in_ports['in_transform'] = Port(node=nodes['37_transformix_mask'], name='in_transform', description='')
    nodes['37_transformix_mask'].in_ports['in_image'] = Port(node=nodes['37_transformix_mask'], name='in_image', description='')
    nodes['37_transformix_mask'].in_ports['in_points'] = Port(node=nodes['37_transformix_mask'], name='in_points', description='')
    nodes['37_transformix_mask'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['37_transformix_mask'], name='in_determinant_of_jacobian_flag', description='')
    nodes['37_transformix_mask'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['37_transformix_mask'], name='in_jacobian_matrix_flag', description='')
    nodes['37_transformix_mask'].in_ports['in_priority'] = Port(node=nodes['37_transformix_mask'], name='in_priority', description='')
    nodes['37_transformix_mask'].in_ports['in_threads'] = Port(node=nodes['37_transformix_mask'], name='in_threads', description='')

    
    nodes['37_transformix_mask'].out_ports['out_directory'] = Port(node=nodes['37_transformix_mask'], name='out_directory', description='')
    nodes['37_transformix_mask'].out_ports['out_image'] = Port(node=nodes['37_transformix_mask'], name='out_image', description='')
    nodes['37_transformix_mask'].out_ports['out_points'] = Port(node=nodes['37_transformix_mask'], name='out_points', description='')
    nodes['37_transformix_mask'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['37_transformix_mask'], name='out_determinant_of_jacobian', description='')
    nodes['37_transformix_mask'].out_ports['out_jacobian_matrix'] = Port(node=nodes['37_transformix_mask'], name='out_jacobian_matrix', description='')
    nodes['37_transformix_mask'].out_ports['out_log_file'] = Port(node=nodes['37_transformix_mask'], name='out_log_file', description='')
    
    nodes['38_N3_swi'] = run.create_node(name='38_N3_swi', type='node', parent=nodes['7_NUC'], no_in_ports=0, no_out_ports=0)

    
    nodes['38_N3_swi'].in_ports['in_image'] = Port(node=nodes['38_N3_swi'], name='in_image', description='')
    nodes['38_N3_swi'].in_ports['in_clobber_flag'] = Port(node=nodes['38_N3_swi'], name='in_clobber_flag', description='')
    nodes['38_N3_swi'].in_ports['in_quiet_flag'] = Port(node=nodes['38_N3_swi'], name='in_quiet_flag', description='')
    nodes['38_N3_swi'].in_ports['in_normalize_field_flag'] = Port(node=nodes['38_N3_swi'], name='in_normalize_field_flag', description='')
    nodes['38_N3_swi'].in_ports['in_iterations'] = Port(node=nodes['38_N3_swi'], name='in_iterations', description='')
    nodes['38_N3_swi'].in_ports['in_stop'] = Port(node=nodes['38_N3_swi'], name='in_stop', description='')
    nodes['38_N3_swi'].in_ports['in_mask'] = Port(node=nodes['38_N3_swi'], name='in_mask', description='')

    
    nodes['38_N3_swi'].out_ports['out_image'] = Port(node=nodes['38_N3_swi'], name='out_image', description='')
    
    nodes['39_const_knnseg_trained_features_0'] = run.create_node(name='39_const_knnseg_trained_features_0', type='constant', parent=nodes['0_knnseg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['39_const_knnseg_trained_features_0'].out_ports['out_output'] = Port(node=nodes['39_const_knnseg_trained_features_0'], name='out_output', description='')
    
    nodes['40_mask2mnc_ana2mnc'] = run.create_node(name='40_mask2mnc_ana2mnc', type='node', parent=nodes['10_mask2ana'], no_in_ports=0, no_out_ports=0)

    
    nodes['40_mask2mnc_ana2mnc'].in_ports['in_image'] = Port(node=nodes['40_mask2mnc_ana2mnc'], name='in_image', description='')

    
    nodes['40_mask2mnc_ana2mnc'].out_ports['out_image'] = Port(node=nodes['40_mask2mnc_ana2mnc'], name='out_image', description='')
    
    nodes['41_rama_PD'] = run.create_node(name='41_rama_PD', type='node', parent=nodes['14_rama'], no_in_ports=0, no_out_ports=0)

    
    nodes['41_rama_PD'].in_ports['in_image'] = Port(node=nodes['41_rama_PD'], name='in_image', description='')
    nodes['41_rama_PD'].in_ports['in_clobber_flag'] = Port(node=nodes['41_rama_PD'], name='in_clobber_flag', description='')
    nodes['41_rama_PD'].in_ports['in_nocache_flag'] = Port(node=nodes['41_rama_PD'], name='in_nocache_flag', description='')
    nodes['41_rama_PD'].in_ports['in_mask'] = Port(node=nodes['41_rama_PD'], name='in_mask', description='')
    nodes['41_rama_PD'].in_ports['in_range'] = Port(node=nodes['41_rama_PD'], name='in_range', description='')
    nodes['41_rama_PD'].in_ports['in_constants'] = Port(node=nodes['41_rama_PD'], name='in_constants', description='')

    
    nodes['41_rama_PD'].out_ports['out_image'] = Port(node=nodes['41_rama_PD'], name='out_image', description='')
    
    nodes['42_dcm2mnc_t1'] = run.create_node(name='42_dcm2mnc_t1', type='node', parent=nodes['2_convert'], no_in_ports=0, no_out_ports=0)

    
    nodes['42_dcm2mnc_t1'].in_ports['in_dicom_images'] = Port(node=nodes['42_dcm2mnc_t1'], name='in_dicom_images', description='')

    
    nodes['42_dcm2mnc_t1'].out_ports['out_image'] = Port(node=nodes['42_dcm2mnc_t1'], name='out_image', description='')
    
    nodes['43_const_knnseg_parameters_0'] = run.create_node(name='43_const_knnseg_parameters_0', type='constant', parent=nodes['0_knnseg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['43_const_knnseg_parameters_0'].out_ports['out_output'] = Port(node=nodes['43_const_knnseg_parameters_0'], name='out_output', description='')
    
    nodes['44_minc2ana_swi_flip'] = run.create_node(name='44_minc2ana_swi_flip', type='node', parent=nodes['15_mnc2ana_flip'], no_in_ports=0, no_out_ports=0)

    
    nodes['44_minc2ana_swi_flip'].in_ports['in_image'] = Port(node=nodes['44_minc2ana_swi_flip'], name='in_image', description='')
    nodes['44_minc2ana_swi_flip'].in_ports['in_neuro_flag'] = Port(node=nodes['44_minc2ana_swi_flip'], name='in_neuro_flag', description='')

    
    nodes['44_minc2ana_swi_flip'].out_ports['out_image'] = Port(node=nodes['44_minc2ana_swi_flip'], name='out_image', description='')
    
    nodes['45_const_rama_PD_constants_0'] = run.create_node(name='45_const_rama_PD_constants_0', type='constant', parent=nodes['14_rama'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['45_const_rama_PD_constants_0'].out_ports['out_output'] = Port(node=nodes['45_const_rama_PD_constants_0'], name='out_output', description='')
    
    nodes['46_sink_pd'] = run.create_node(name='46_sink_pd', type='sink', parent=nodes['4_sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['46_sink_pd'].in_ports['in_input'] = Port(node=nodes['46_sink_pd'], name='in_input', description='')

    
    
    nodes['47_multi_atlas_combine_lobes_intensity'] = run.create_node(name='47_multi_atlas_combine_lobes_intensity', type='node', parent=nodes['3_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    
    nodes['47_multi_atlas_combine_lobes_intensity'].in_ports['in_image'] = Port(node=nodes['47_multi_atlas_combine_lobes_intensity'], name='in_image', description='')
    nodes['47_multi_atlas_combine_lobes_intensity'].in_ports['in_original_values'] = Port(node=nodes['47_multi_atlas_combine_lobes_intensity'], name='in_original_values', description='')
    nodes['47_multi_atlas_combine_lobes_intensity'].in_ports['in_substitute_values'] = Port(node=nodes['47_multi_atlas_combine_lobes_intensity'], name='in_substitute_values', description='')
    nodes['47_multi_atlas_combine_lobes_intensity'].in_ports['in_component_type'] = Port(node=nodes['47_multi_atlas_combine_lobes_intensity'], name='in_component_type', description='')

    
    nodes['47_multi_atlas_combine_lobes_intensity'].out_ports['out_image'] = Port(node=nodes['47_multi_atlas_combine_lobes_intensity'], name='out_image', description='')
    
    nodes['48_const_multi_atlas_combine_masks_number_of_classes_0'] = run.create_node(name='48_const_multi_atlas_combine_masks_number_of_classes_0', type='constant', parent=nodes['3_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['48_const_multi_atlas_combine_masks_number_of_classes_0'].out_ports['out_output'] = Port(node=nodes['48_const_multi_atlas_combine_masks_number_of_classes_0'], name='out_output', description='')
    
    nodes['49_N3_flair'] = run.create_node(name='49_N3_flair', type='node', parent=nodes['7_NUC'], no_in_ports=0, no_out_ports=0)

    
    nodes['49_N3_flair'].in_ports['in_image'] = Port(node=nodes['49_N3_flair'], name='in_image', description='')
    nodes['49_N3_flair'].in_ports['in_clobber_flag'] = Port(node=nodes['49_N3_flair'], name='in_clobber_flag', description='')
    nodes['49_N3_flair'].in_ports['in_quiet_flag'] = Port(node=nodes['49_N3_flair'], name='in_quiet_flag', description='')
    nodes['49_N3_flair'].in_ports['in_normalize_field_flag'] = Port(node=nodes['49_N3_flair'], name='in_normalize_field_flag', description='')
    nodes['49_N3_flair'].in_ports['in_iterations'] = Port(node=nodes['49_N3_flair'], name='in_iterations', description='')
    nodes['49_N3_flair'].in_ports['in_stop'] = Port(node=nodes['49_N3_flair'], name='in_stop', description='')
    nodes['49_N3_flair'].in_ports['in_mask'] = Port(node=nodes['49_N3_flair'], name='in_mask', description='')

    
    nodes['49_N3_flair'].out_ports['out_image'] = Port(node=nodes['49_N3_flair'], name='out_image', description='')
    
    nodes['50_mritoself_swi'] = run.create_node(name='50_mritoself_swi', type='node', parent=nodes['9_coreg'], no_in_ports=0, no_out_ports=0)

    
    nodes['50_mritoself_swi'].in_ports['in_source'] = Port(node=nodes['50_mritoself_swi'], name='in_source', description='')
    nodes['50_mritoself_swi'].in_ports['in_target'] = Port(node=nodes['50_mritoself_swi'], name='in_target', description='')
    nodes['50_mritoself_swi'].in_ports['in_clobber_flag'] = Port(node=nodes['50_mritoself_swi'], name='in_clobber_flag', description='')
    nodes['50_mritoself_swi'].in_ports['in_threshold'] = Port(node=nodes['50_mritoself_swi'], name='in_threshold', description='')

    
    nodes['50_mritoself_swi'].out_ports['out_transform'] = Port(node=nodes['50_mritoself_swi'], name='out_transform', description='')
    
    nodes['51_knnseg'] = run.create_node(name='51_knnseg', type='node', parent=nodes['0_knnseg'], no_in_ports=0, no_out_ports=0)

    
    nodes['51_knnseg'].in_ports['in_t1'] = Port(node=nodes['51_knnseg'], name='in_t1', description='')
    nodes['51_knnseg'].in_ports['in_pd'] = Port(node=nodes['51_knnseg'], name='in_pd', description='')
    nodes['51_knnseg'].in_ports['in_parameters'] = Port(node=nodes['51_knnseg'], name='in_parameters', description='')
    nodes['51_knnseg'].in_ports['in_trained_features'] = Port(node=nodes['51_knnseg'], name='in_trained_features', description='')
    nodes['51_knnseg'].in_ports['in_fknn_flag'] = Port(node=nodes['51_knnseg'], name='in_fknn_flag', description='')
    nodes['51_knnseg'].in_ports['in_clobber_flag'] = Port(node=nodes['51_knnseg'], name='in_clobber_flag', description='')
    nodes['51_knnseg'].in_ports['in_verbose_flag'] = Port(node=nodes['51_knnseg'], name='in_verbose_flag', description='')

    
    nodes['51_knnseg'].out_ports['out_label_image'] = Port(node=nodes['51_knnseg'], name='out_label_image', description='')
    
    nodes['52_minc2ana_flair_flip'] = run.create_node(name='52_minc2ana_flair_flip', type='node', parent=nodes['15_mnc2ana_flip'], no_in_ports=0, no_out_ports=0)

    
    nodes['52_minc2ana_flair_flip'].in_ports['in_image'] = Port(node=nodes['52_minc2ana_flair_flip'], name='in_image', description='')
    nodes['52_minc2ana_flair_flip'].in_ports['in_neuro_flag'] = Port(node=nodes['52_minc2ana_flair_flip'], name='in_neuro_flag', description='')

    
    nodes['52_minc2ana_flair_flip'].out_ports['out_image'] = Port(node=nodes['52_minc2ana_flair_flip'], name='out_image', description='')
    
    nodes['53_const_minc2ana_t1_neuro_flag_0'] = run.create_node(name='53_const_minc2ana_t1_neuro_flag_0', type='constant', parent=nodes['13_mnc2ana'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['53_const_minc2ana_t1_neuro_flag_0'].out_ports['out_output'] = Port(node=nodes['53_const_minc2ana_t1_neuro_flag_0'], name='out_output', description='')
    
    nodes['54_multi_atlas_reg_t1'] = run.create_node(name='54_multi_atlas_reg_t1', type='node', parent=nodes['3_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    
    nodes['54_multi_atlas_reg_t1'].in_ports['in_fixed_image'] = Port(node=nodes['54_multi_atlas_reg_t1'], name='in_fixed_image', description='')
    nodes['54_multi_atlas_reg_t1'].in_ports['in_moving_image'] = Port(node=nodes['54_multi_atlas_reg_t1'], name='in_moving_image', description='')
    nodes['54_multi_atlas_reg_t1'].in_ports['in_parameters'] = Port(node=nodes['54_multi_atlas_reg_t1'], name='in_parameters', description='')
    nodes['54_multi_atlas_reg_t1'].in_ports['in_fixed_mask'] = Port(node=nodes['54_multi_atlas_reg_t1'], name='in_fixed_mask', description='')
    nodes['54_multi_atlas_reg_t1'].in_ports['in_moving_mask'] = Port(node=nodes['54_multi_atlas_reg_t1'], name='in_moving_mask', description='')
    nodes['54_multi_atlas_reg_t1'].in_ports['in_initial_transform'] = Port(node=nodes['54_multi_atlas_reg_t1'], name='in_initial_transform', description='')
    nodes['54_multi_atlas_reg_t1'].in_ports['in_priority'] = Port(node=nodes['54_multi_atlas_reg_t1'], name='in_priority', description='')
    nodes['54_multi_atlas_reg_t1'].in_ports['in_threads'] = Port(node=nodes['54_multi_atlas_reg_t1'], name='in_threads', description='')

    
    nodes['54_multi_atlas_reg_t1'].out_ports['out_directory'] = Port(node=nodes['54_multi_atlas_reg_t1'], name='out_directory', description='')
    nodes['54_multi_atlas_reg_t1'].out_ports['out_transform'] = Port(node=nodes['54_multi_atlas_reg_t1'], name='out_transform', description='')
    nodes['54_multi_atlas_reg_t1'].out_ports['out_log_file'] = Port(node=nodes['54_multi_atlas_reg_t1'], name='out_log_file', description='')
    
    nodes['55_const_N3_PD_iterations_0'] = run.create_node(name='55_const_N3_PD_iterations_0', type='constant', parent=nodes['7_NUC'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['55_const_N3_PD_iterations_0'].out_ports['out_output'] = Port(node=nodes['55_const_N3_PD_iterations_0'], name='out_output', description='')
    
    nodes['56_PD'] = run.create_node(name='56_PD', type='source', parent=nodes['11_sources'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['56_PD'].out_ports['out_output'] = Port(node=nodes['56_PD'], name='out_output', description='')
    
    nodes['57_mask_nii2ana_flip'] = run.create_node(name='57_mask_nii2ana_flip', type='node', parent=nodes['5_nii2ana_flip'], no_in_ports=0, no_out_ports=0)

    
    nodes['57_mask_nii2ana_flip'].in_ports['in_image'] = Port(node=nodes['57_mask_nii2ana_flip'], name='in_image', description='')

    
    nodes['57_mask_nii2ana_flip'].out_ports['out_image'] = Port(node=nodes['57_mask_nii2ana_flip'], name='out_image', description='')
    
    nodes['58_multi_atlas_combine_masks'] = run.create_node(name='58_multi_atlas_combine_masks', type='node', parent=nodes['3_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    
    nodes['58_multi_atlas_combine_masks'].in_ports['in_images'] = Port(node=nodes['58_multi_atlas_combine_masks'], name='in_images', description='')
    nodes['58_multi_atlas_combine_masks'].in_ports['in_method'] = Port(node=nodes['58_multi_atlas_combine_masks'], name='in_method', description='')
    nodes['58_multi_atlas_combine_masks'].in_ports['in_number_of_classes'] = Port(node=nodes['58_multi_atlas_combine_masks'], name='in_number_of_classes', description='')
    nodes['58_multi_atlas_combine_masks'].in_ports['in_original_labels'] = Port(node=nodes['58_multi_atlas_combine_masks'], name='in_original_labels', description='')
    nodes['58_multi_atlas_combine_masks'].in_ports['in_substitute_labels'] = Port(node=nodes['58_multi_atlas_combine_masks'], name='in_substitute_labels', description='')

    
    nodes['58_multi_atlas_combine_masks'].out_ports['out_hard_segment'] = Port(node=nodes['58_multi_atlas_combine_masks'], name='out_hard_segment', description='')
    
    nodes['59_multi_atlases_lobes_templabels'] = run.create_node(name='59_multi_atlases_lobes_templabels', type='constant', parent=nodes['3_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['59_multi_atlases_lobes_templabels'].out_ports['out_output'] = Port(node=nodes['59_multi_atlases_lobes_templabels'], name='out_output', description='')
    
    nodes['60_FLAIR'] = run.create_node(name='60_FLAIR', type='source', parent=nodes['11_sources'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['60_FLAIR'].out_ports['out_output'] = Port(node=nodes['60_FLAIR'], name='out_output', description='')
    
    nodes['61_const_multi_atlas_combine_lobes_number_of_classes_0'] = run.create_node(name='61_const_multi_atlas_combine_lobes_number_of_classes_0', type='constant', parent=nodes['3_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['61_const_multi_atlas_combine_lobes_number_of_classes_0'].out_ports['out_output'] = Port(node=nodes['61_const_multi_atlas_combine_lobes_number_of_classes_0'], name='out_output', description='')
    
    nodes['62_mincresample_pd'] = run.create_node(name='62_mincresample_pd', type='node', parent=nodes['9_coreg'], no_in_ports=0, no_out_ports=0)

    
    nodes['62_mincresample_pd'].in_ports['in_image'] = Port(node=nodes['62_mincresample_pd'], name='in_image', description='')
    nodes['62_mincresample_pd'].in_ports['in_clobber_flag'] = Port(node=nodes['62_mincresample_pd'], name='in_clobber_flag', description='')
    nodes['62_mincresample_pd'].in_ports['in_trilinear_flag'] = Port(node=nodes['62_mincresample_pd'], name='in_trilinear_flag', description='')
    nodes['62_mincresample_pd'].in_ports['in_like'] = Port(node=nodes['62_mincresample_pd'], name='in_like', description='')
    nodes['62_mincresample_pd'].in_ports['in_transform'] = Port(node=nodes['62_mincresample_pd'], name='in_transform', description='')
    nodes['62_mincresample_pd'].in_ports['in_invert_transformation'] = Port(node=nodes['62_mincresample_pd'], name='in_invert_transformation', description='')

    
    nodes['62_mincresample_pd'].out_ports['out_image'] = Port(node=nodes['62_mincresample_pd'], name='out_image', description='')
    
    nodes['63_ana2nii_mask'] = run.create_node(name='63_ana2nii_mask', type='node', parent=nodes['6_finalconvert'], no_in_ports=0, no_out_ports=0)

    
    nodes['63_ana2nii_mask'].in_ports['in_image'] = Port(node=nodes['63_ana2nii_mask'], name='in_image', description='')
    nodes['63_ana2nii_mask'].in_ports['in_reference_image'] = Port(node=nodes['63_ana2nii_mask'], name='in_reference_image', description='')

    
    nodes['63_ana2nii_mask'].out_ports['out_image'] = Port(node=nodes['63_ana2nii_mask'], name='out_image', description='')
    
    nodes['64_wmlsegment'] = run.create_node(name='64_wmlsegment', type='node', parent=nodes['8_wmlsegment'], no_in_ports=0, no_out_ports=0)

    
    nodes['64_wmlsegment'].in_ports['in_tissue_labels'] = Port(node=nodes['64_wmlsegment'], name='in_tissue_labels', description='')
    nodes['64_wmlsegment'].in_ports['in_flair'] = Port(node=nodes['64_wmlsegment'], name='in_flair', description='')
    nodes['64_wmlsegment'].in_ports['in_brain_mask'] = Port(node=nodes['64_wmlsegment'], name='in_brain_mask', description='')
    nodes['64_wmlsegment'].in_ports['in_alpha'] = Port(node=nodes['64_wmlsegment'], name='in_alpha', description='')
    nodes['64_wmlsegment'].in_ports['in_beta'] = Port(node=nodes['64_wmlsegment'], name='in_beta', description='')

    
    nodes['64_wmlsegment'].out_ports['out_white_matter_lesion_mask'] = Port(node=nodes['64_wmlsegment'], name='out_white_matter_lesion_mask', description='')
    
    nodes['65_ana2nii_knnseg'] = run.create_node(name='65_ana2nii_knnseg', type='node', parent=nodes['6_finalconvert'], no_in_ports=0, no_out_ports=0)

    
    nodes['65_ana2nii_knnseg'].in_ports['in_image'] = Port(node=nodes['65_ana2nii_knnseg'], name='in_image', description='')
    nodes['65_ana2nii_knnseg'].in_ports['in_reference_image'] = Port(node=nodes['65_ana2nii_knnseg'], name='in_reference_image', description='')

    
    nodes['65_ana2nii_knnseg'].out_ports['out_image'] = Port(node=nodes['65_ana2nii_knnseg'], name='out_image', description='')
    
    nodes['66_minc2ana_t1_flip'] = run.create_node(name='66_minc2ana_t1_flip', type='node', parent=nodes['15_mnc2ana_flip'], no_in_ports=0, no_out_ports=0)

    
    nodes['66_minc2ana_t1_flip'].in_ports['in_image'] = Port(node=nodes['66_minc2ana_t1_flip'], name='in_image', description='')
    nodes['66_minc2ana_t1_flip'].in_ports['in_neuro_flag'] = Port(node=nodes['66_minc2ana_t1_flip'], name='in_neuro_flag', description='')

    
    nodes['66_minc2ana_t1_flip'].out_ports['out_image'] = Port(node=nodes['66_minc2ana_t1_flip'], name='out_image', description='')
    
    nodes['67_N3_PD'] = run.create_node(name='67_N3_PD', type='node', parent=nodes['7_NUC'], no_in_ports=0, no_out_ports=0)

    
    nodes['67_N3_PD'].in_ports['in_image'] = Port(node=nodes['67_N3_PD'], name='in_image', description='')
    nodes['67_N3_PD'].in_ports['in_clobber_flag'] = Port(node=nodes['67_N3_PD'], name='in_clobber_flag', description='')
    nodes['67_N3_PD'].in_ports['in_quiet_flag'] = Port(node=nodes['67_N3_PD'], name='in_quiet_flag', description='')
    nodes['67_N3_PD'].in_ports['in_normalize_field_flag'] = Port(node=nodes['67_N3_PD'], name='in_normalize_field_flag', description='')
    nodes['67_N3_PD'].in_ports['in_iterations'] = Port(node=nodes['67_N3_PD'], name='in_iterations', description='')
    nodes['67_N3_PD'].in_ports['in_stop'] = Port(node=nodes['67_N3_PD'], name='in_stop', description='')
    nodes['67_N3_PD'].in_ports['in_mask'] = Port(node=nodes['67_N3_PD'], name='in_mask', description='')

    
    nodes['67_N3_PD'].out_ports['out_image'] = Port(node=nodes['67_N3_PD'], name='out_image', description='')
    
    nodes['68_dcm2mnc_swi'] = run.create_node(name='68_dcm2mnc_swi', type='node', parent=nodes['2_convert'], no_in_ports=0, no_out_ports=0)

    
    nodes['68_dcm2mnc_swi'].in_ports['in_dicom_images'] = Port(node=nodes['68_dcm2mnc_swi'], name='in_dicom_images', description='')

    
    nodes['68_dcm2mnc_swi'].out_ports['out_image'] = Port(node=nodes['68_dcm2mnc_swi'], name='out_image', description='')
    
    nodes['69_mritoself_pd'] = run.create_node(name='69_mritoself_pd', type='node', parent=nodes['9_coreg'], no_in_ports=0, no_out_ports=0)

    
    nodes['69_mritoself_pd'].in_ports['in_source'] = Port(node=nodes['69_mritoself_pd'], name='in_source', description='')
    nodes['69_mritoself_pd'].in_ports['in_target'] = Port(node=nodes['69_mritoself_pd'], name='in_target', description='')
    nodes['69_mritoself_pd'].in_ports['in_clobber_flag'] = Port(node=nodes['69_mritoself_pd'], name='in_clobber_flag', description='')
    nodes['69_mritoself_pd'].in_ports['in_threshold'] = Port(node=nodes['69_mritoself_pd'], name='in_threshold', description='')

    
    nodes['69_mritoself_pd'].out_ports['out_transform'] = Port(node=nodes['69_mritoself_pd'], name='out_transform', description='')
    
    nodes['70_multi_atlases_masks'] = run.create_node(name='70_multi_atlases_masks', type='constant', parent=nodes['3_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['70_multi_atlases_masks'].out_ports['out_output'] = Port(node=nodes['70_multi_atlases_masks'], name='out_output', description='')
    
    nodes['71_multi_atlases_lobes'] = run.create_node(name='71_multi_atlases_lobes', type='constant', parent=nodes['3_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['71_multi_atlases_lobes'].out_ports['out_output'] = Port(node=nodes['71_multi_atlases_lobes'], name='out_output', description='')
    
    nodes['72_multi_atlas_trans_lobes_t1'] = run.create_node(name='72_multi_atlas_trans_lobes_t1', type='node', parent=nodes['3_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    
    nodes['72_multi_atlas_trans_lobes_t1'].in_ports['in_transform'] = Port(node=nodes['72_multi_atlas_trans_lobes_t1'], name='in_transform', description='')
    nodes['72_multi_atlas_trans_lobes_t1'].in_ports['in_image'] = Port(node=nodes['72_multi_atlas_trans_lobes_t1'], name='in_image', description='')
    nodes['72_multi_atlas_trans_lobes_t1'].in_ports['in_points'] = Port(node=nodes['72_multi_atlas_trans_lobes_t1'], name='in_points', description='')
    nodes['72_multi_atlas_trans_lobes_t1'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['72_multi_atlas_trans_lobes_t1'], name='in_determinant_of_jacobian_flag', description='')
    nodes['72_multi_atlas_trans_lobes_t1'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['72_multi_atlas_trans_lobes_t1'], name='in_jacobian_matrix_flag', description='')
    nodes['72_multi_atlas_trans_lobes_t1'].in_ports['in_priority'] = Port(node=nodes['72_multi_atlas_trans_lobes_t1'], name='in_priority', description='')
    nodes['72_multi_atlas_trans_lobes_t1'].in_ports['in_threads'] = Port(node=nodes['72_multi_atlas_trans_lobes_t1'], name='in_threads', description='')

    
    nodes['72_multi_atlas_trans_lobes_t1'].out_ports['out_directory'] = Port(node=nodes['72_multi_atlas_trans_lobes_t1'], name='out_directory', description='')
    nodes['72_multi_atlas_trans_lobes_t1'].out_ports['out_image'] = Port(node=nodes['72_multi_atlas_trans_lobes_t1'], name='out_image', description='')
    nodes['72_multi_atlas_trans_lobes_t1'].out_ports['out_points'] = Port(node=nodes['72_multi_atlas_trans_lobes_t1'], name='out_points', description='')
    nodes['72_multi_atlas_trans_lobes_t1'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['72_multi_atlas_trans_lobes_t1'], name='out_determinant_of_jacobian', description='')
    nodes['72_multi_atlas_trans_lobes_t1'].out_ports['out_jacobian_matrix'] = Port(node=nodes['72_multi_atlas_trans_lobes_t1'], name='out_jacobian_matrix', description='')
    nodes['72_multi_atlas_trans_lobes_t1'].out_ports['out_log_file'] = Port(node=nodes['72_multi_atlas_trans_lobes_t1'], name='out_log_file', description='')
    
    nodes['73_sink_t1'] = run.create_node(name='73_sink_t1', type='sink', parent=nodes['4_sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['73_sink_t1'].in_ports['in_input'] = Port(node=nodes['73_sink_t1'], name='in_input', description='')

    
    
    nodes['74_rama_T1'] = run.create_node(name='74_rama_T1', type='node', parent=nodes['14_rama'], no_in_ports=0, no_out_ports=0)

    
    nodes['74_rama_T1'].in_ports['in_image'] = Port(node=nodes['74_rama_T1'], name='in_image', description='')
    nodes['74_rama_T1'].in_ports['in_clobber_flag'] = Port(node=nodes['74_rama_T1'], name='in_clobber_flag', description='')
    nodes['74_rama_T1'].in_ports['in_nocache_flag'] = Port(node=nodes['74_rama_T1'], name='in_nocache_flag', description='')
    nodes['74_rama_T1'].in_ports['in_mask'] = Port(node=nodes['74_rama_T1'], name='in_mask', description='')
    nodes['74_rama_T1'].in_ports['in_range'] = Port(node=nodes['74_rama_T1'], name='in_range', description='')
    nodes['74_rama_T1'].in_ports['in_constants'] = Port(node=nodes['74_rama_T1'], name='in_constants', description='')

    
    nodes['74_rama_T1'].out_ports['out_image'] = Port(node=nodes['74_rama_T1'], name='out_image', description='')
    
    nodes['75_dcm2mnc_pd'] = run.create_node(name='75_dcm2mnc_pd', type='node', parent=nodes['2_convert'], no_in_ports=0, no_out_ports=0)

    
    nodes['75_dcm2mnc_pd'].in_ports['in_dicom_images'] = Port(node=nodes['75_dcm2mnc_pd'], name='in_dicom_images', description='')

    
    nodes['75_dcm2mnc_pd'].out_ports['out_image'] = Port(node=nodes['75_dcm2mnc_pd'], name='out_image', description='')
    
    nodes['76_mincresample_flair'] = run.create_node(name='76_mincresample_flair', type='node', parent=nodes['9_coreg'], no_in_ports=0, no_out_ports=0)

    
    nodes['76_mincresample_flair'].in_ports['in_image'] = Port(node=nodes['76_mincresample_flair'], name='in_image', description='')
    nodes['76_mincresample_flair'].in_ports['in_clobber_flag'] = Port(node=nodes['76_mincresample_flair'], name='in_clobber_flag', description='')
    nodes['76_mincresample_flair'].in_ports['in_trilinear_flag'] = Port(node=nodes['76_mincresample_flair'], name='in_trilinear_flag', description='')
    nodes['76_mincresample_flair'].in_ports['in_like'] = Port(node=nodes['76_mincresample_flair'], name='in_like', description='')
    nodes['76_mincresample_flair'].in_ports['in_transform'] = Port(node=nodes['76_mincresample_flair'], name='in_transform', description='')
    nodes['76_mincresample_flair'].in_ports['in_invert_transformation'] = Port(node=nodes['76_mincresample_flair'], name='in_invert_transformation', description='')

    
    nodes['76_mincresample_flair'].out_ports['out_image'] = Port(node=nodes['76_mincresample_flair'], name='out_image', description='')
    
    nodes['77_ana2nii_knnwml'] = run.create_node(name='77_ana2nii_knnwml', type='node', parent=nodes['6_finalconvert'], no_in_ports=0, no_out_ports=0)

    
    nodes['77_ana2nii_knnwml'].in_ports['in_image'] = Port(node=nodes['77_ana2nii_knnwml'], name='in_image', description='')
    nodes['77_ana2nii_knnwml'].in_ports['in_reference_image'] = Port(node=nodes['77_ana2nii_knnwml'], name='in_reference_image', description='')

    
    nodes['77_ana2nii_knnwml'].out_ports['out_image'] = Port(node=nodes['77_ana2nii_knnwml'], name='out_image', description='')
    
    nodes['78_sink_lobes'] = run.create_node(name='78_sink_lobes', type='sink', parent=nodes['4_sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['78_sink_lobes'].in_ports['in_input'] = Port(node=nodes['78_sink_lobes'], name='in_input', description='')

    
    
    nodes['79_multi_atlases_scans_t1'] = run.create_node(name='79_multi_atlases_scans_t1', type='constant', parent=nodes['3_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['79_multi_atlases_scans_t1'].out_ports['out_output'] = Port(node=nodes['79_multi_atlases_scans_t1'], name='out_output', description='')
    
    nodes['80_minc2ana_t1'] = run.create_node(name='80_minc2ana_t1', type='node', parent=nodes['13_mnc2ana'], no_in_ports=0, no_out_ports=0)

    
    nodes['80_minc2ana_t1'].in_ports['in_image'] = Port(node=nodes['80_minc2ana_t1'], name='in_image', description='')
    nodes['80_minc2ana_t1'].in_ports['in_neuro_flag'] = Port(node=nodes['80_minc2ana_t1'], name='in_neuro_flag', description='')

    
    nodes['80_minc2ana_t1'].out_ports['out_image'] = Port(node=nodes['80_minc2ana_t1'], name='out_image', description='')
    
    nodes['81_const_rama_T1_constants_0'] = run.create_node(name='81_const_rama_T1_constants_0', type='constant', parent=nodes['14_rama'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['81_const_rama_T1_constants_0'].out_ports['out_output'] = Port(node=nodes['81_const_rama_T1_constants_0'], name='out_output', description='')
    
    nodes['82_rama_flair'] = run.create_node(name='82_rama_flair', type='node', parent=nodes['14_rama'], no_in_ports=0, no_out_ports=0)

    
    nodes['82_rama_flair'].in_ports['in_image'] = Port(node=nodes['82_rama_flair'], name='in_image', description='')
    nodes['82_rama_flair'].in_ports['in_mask'] = Port(node=nodes['82_rama_flair'], name='in_mask', description='')
    nodes['82_rama_flair'].in_ports['in_const1'] = Port(node=nodes['82_rama_flair'], name='in_const1', description='')
    nodes['82_rama_flair'].in_ports['in_const2'] = Port(node=nodes['82_rama_flair'], name='in_const2', description='')
    nodes['82_rama_flair'].in_ports['in_const3'] = Port(node=nodes['82_rama_flair'], name='in_const3', description='')

    
    nodes['82_rama_flair'].out_ports['out_image'] = Port(node=nodes['82_rama_flair'], name='out_image', description='')
    
    nodes['83_multi_atlases_lobes_finallabels'] = run.create_node(name='83_multi_atlases_lobes_finallabels', type='constant', parent=nodes['3_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['83_multi_atlases_lobes_finallabels'].out_ports['out_output'] = Port(node=nodes['83_multi_atlases_lobes_finallabels'], name='out_output', description='')
    
    nodes['84_const_rama_PD_range_0'] = run.create_node(name='84_const_rama_PD_range_0', type='constant', parent=nodes['14_rama'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['84_const_rama_PD_range_0'].out_ports['out_output'] = Port(node=nodes['84_const_rama_PD_range_0'], name='out_output', description='')
    
    nodes['85_SWI'] = run.create_node(name='85_SWI', type='source', parent=nodes['11_sources'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['85_SWI'].out_ports['out_output'] = Port(node=nodes['85_SWI'], name='out_output', description='')
    
    nodes['86_ana2nii_T1'] = run.create_node(name='86_ana2nii_T1', type='node', parent=nodes['6_finalconvert'], no_in_ports=0, no_out_ports=0)

    
    nodes['86_ana2nii_T1'].in_ports['in_image'] = Port(node=nodes['86_ana2nii_T1'], name='in_image', description='')
    nodes['86_ana2nii_T1'].in_ports['in_reference_image'] = Port(node=nodes['86_ana2nii_T1'], name='in_reference_image', description='')

    
    nodes['86_ana2nii_T1'].out_ports['out_image'] = Port(node=nodes['86_ana2nii_T1'], name='out_image', description='')
    
    nodes['87_multi_atlas_combine_lobes'] = run.create_node(name='87_multi_atlas_combine_lobes', type='node', parent=nodes['3_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    
    nodes['87_multi_atlas_combine_lobes'].in_ports['in_images'] = Port(node=nodes['87_multi_atlas_combine_lobes'], name='in_images', description='')
    nodes['87_multi_atlas_combine_lobes'].in_ports['in_method'] = Port(node=nodes['87_multi_atlas_combine_lobes'], name='in_method', description='')
    nodes['87_multi_atlas_combine_lobes'].in_ports['in_number_of_classes'] = Port(node=nodes['87_multi_atlas_combine_lobes'], name='in_number_of_classes', description='')
    nodes['87_multi_atlas_combine_lobes'].in_ports['in_original_labels'] = Port(node=nodes['87_multi_atlas_combine_lobes'], name='in_original_labels', description='')
    nodes['87_multi_atlas_combine_lobes'].in_ports['in_substitute_labels'] = Port(node=nodes['87_multi_atlas_combine_lobes'], name='in_substitute_labels', description='')

    
    nodes['87_multi_atlas_combine_lobes'].out_ports['out_hard_segment'] = Port(node=nodes['87_multi_atlas_combine_lobes'], name='out_hard_segment', description='')
    
    nodes['88_const_multi_atlas_combine_lobes_method_0'] = run.create_node(name='88_const_multi_atlas_combine_lobes_method_0', type='constant', parent=nodes['3_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['88_const_multi_atlas_combine_lobes_method_0'].out_ports['out_output'] = Port(node=nodes['88_const_multi_atlas_combine_lobes_method_0'], name='out_output', description='')
    
    nodes['89_mincresample_swi'] = run.create_node(name='89_mincresample_swi', type='node', parent=nodes['9_coreg'], no_in_ports=0, no_out_ports=0)

    
    nodes['89_mincresample_swi'].in_ports['in_image'] = Port(node=nodes['89_mincresample_swi'], name='in_image', description='')
    nodes['89_mincresample_swi'].in_ports['in_clobber_flag'] = Port(node=nodes['89_mincresample_swi'], name='in_clobber_flag', description='')
    nodes['89_mincresample_swi'].in_ports['in_trilinear_flag'] = Port(node=nodes['89_mincresample_swi'], name='in_trilinear_flag', description='')
    nodes['89_mincresample_swi'].in_ports['in_like'] = Port(node=nodes['89_mincresample_swi'], name='in_like', description='')
    nodes['89_mincresample_swi'].in_ports['in_transform'] = Port(node=nodes['89_mincresample_swi'], name='in_transform', description='')
    nodes['89_mincresample_swi'].in_ports['in_invert_transformation'] = Port(node=nodes['89_mincresample_swi'], name='in_invert_transformation', description='')

    
    nodes['89_mincresample_swi'].out_ports['out_image'] = Port(node=nodes['89_mincresample_swi'], name='out_image', description='')
    
    nodes['90_const_multi_atlas_combine_masks_method_0'] = run.create_node(name='90_const_multi_atlas_combine_masks_method_0', type='constant', parent=nodes['3_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['90_const_multi_atlas_combine_masks_method_0'].out_ports['out_output'] = Port(node=nodes['90_const_multi_atlas_combine_masks_method_0'], name='out_output', description='')
    
    nodes['91_BrainMaskImprove'] = run.create_node(name='91_BrainMaskImprove', type='node', parent=nodes['12_brainmaskimprove'], no_in_ports=0, no_out_ports=0)

    
    nodes['91_BrainMaskImprove'].in_ports['in_segmentation'] = Port(node=nodes['91_BrainMaskImprove'], name='in_segmentation', description='')
    nodes['91_BrainMaskImprove'].in_ports['in_fraction'] = Port(node=nodes['91_BrainMaskImprove'], name='in_fraction', description='')

    
    nodes['91_BrainMaskImprove'].out_ports['out_segmentation'] = Port(node=nodes['91_BrainMaskImprove'], name='out_segmentation', description='')
    
    nodes['92_ana2nii_flair'] = run.create_node(name='92_ana2nii_flair', type='node', parent=nodes['6_finalconvert'], no_in_ports=0, no_out_ports=0)

    
    nodes['92_ana2nii_flair'].in_ports['in_image'] = Port(node=nodes['92_ana2nii_flair'], name='in_image', description='')
    nodes['92_ana2nii_flair'].in_ports['in_reference_image'] = Port(node=nodes['92_ana2nii_flair'], name='in_reference_image', description='')

    
    nodes['92_ana2nii_flair'].out_ports['out_image'] = Port(node=nodes['92_ana2nii_flair'], name='out_image', description='')
    
    nodes['93_elastix_mask'] = run.create_node(name='93_elastix_mask', type='node', parent=nodes['1_elastix_mask'], no_in_ports=0, no_out_ports=0)

    
    nodes['93_elastix_mask'].in_ports['in_fixed_image'] = Port(node=nodes['93_elastix_mask'], name='in_fixed_image', description='')
    nodes['93_elastix_mask'].in_ports['in_moving_image'] = Port(node=nodes['93_elastix_mask'], name='in_moving_image', description='')
    nodes['93_elastix_mask'].in_ports['in_parameters'] = Port(node=nodes['93_elastix_mask'], name='in_parameters', description='')
    nodes['93_elastix_mask'].in_ports['in_fixed_mask'] = Port(node=nodes['93_elastix_mask'], name='in_fixed_mask', description='')
    nodes['93_elastix_mask'].in_ports['in_moving_mask'] = Port(node=nodes['93_elastix_mask'], name='in_moving_mask', description='')
    nodes['93_elastix_mask'].in_ports['in_initial_transform'] = Port(node=nodes['93_elastix_mask'], name='in_initial_transform', description='')
    nodes['93_elastix_mask'].in_ports['in_priority'] = Port(node=nodes['93_elastix_mask'], name='in_priority', description='')
    nodes['93_elastix_mask'].in_ports['in_threads'] = Port(node=nodes['93_elastix_mask'], name='in_threads', description='')

    
    nodes['93_elastix_mask'].out_ports['out_directory'] = Port(node=nodes['93_elastix_mask'], name='out_directory', description='')
    nodes['93_elastix_mask'].out_ports['out_transform'] = Port(node=nodes['93_elastix_mask'], name='out_transform', description='')
    nodes['93_elastix_mask'].out_ports['out_log_file'] = Port(node=nodes['93_elastix_mask'], name='out_log_file', description='')
    
    nodes['94_const_rama_T1_range_0'] = run.create_node(name='94_const_rama_T1_range_0', type='constant', parent=nodes['14_rama'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['94_const_rama_T1_range_0'].out_ports['out_output'] = Port(node=nodes['94_const_rama_T1_range_0'], name='out_output', description='')
    
    nodes['95_T1'] = run.create_node(name='95_T1', type='source', parent=nodes['11_sources'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['95_T1'].out_ports['out_output'] = Port(node=nodes['95_T1'], name='out_output', description='')
    
    nodes['96_dcm2mnc_flair'] = run.create_node(name='96_dcm2mnc_flair', type='node', parent=nodes['2_convert'], no_in_ports=0, no_out_ports=0)

    
    nodes['96_dcm2mnc_flair'].in_ports['in_dicom_images'] = Port(node=nodes['96_dcm2mnc_flair'], name='in_dicom_images', description='')

    
    nodes['96_dcm2mnc_flair'].out_ports['out_image'] = Port(node=nodes['96_dcm2mnc_flair'], name='out_image', description='')
    
    nodes['97_mritoself_flair'] = run.create_node(name='97_mritoself_flair', type='node', parent=nodes['9_coreg'], no_in_ports=0, no_out_ports=0)

    
    nodes['97_mritoself_flair'].in_ports['in_source'] = Port(node=nodes['97_mritoself_flair'], name='in_source', description='')
    nodes['97_mritoself_flair'].in_ports['in_target'] = Port(node=nodes['97_mritoself_flair'], name='in_target', description='')
    nodes['97_mritoself_flair'].in_ports['in_clobber_flag'] = Port(node=nodes['97_mritoself_flair'], name='in_clobber_flag', description='')
    nodes['97_mritoself_flair'].in_ports['in_threshold'] = Port(node=nodes['97_mritoself_flair'], name='in_threshold', description='')

    
    nodes['97_mritoself_flair'].out_ports['out_transform'] = Port(node=nodes['97_mritoself_flair'], name='out_transform', description='')
    
    nodes['98_const_N3_T1_stop_0'] = run.create_node(name='98_const_N3_T1_stop_0', type='constant', parent=nodes['7_NUC'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['98_const_N3_T1_stop_0'].out_ports['out_output'] = Port(node=nodes['98_const_N3_T1_stop_0'], name='out_output', description='')
    
    nodes['99_const_rama_flair_const3_0'] = run.create_node(name='99_const_rama_flair_const3_0', type='constant', parent=nodes['14_rama'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['99_const_rama_flair_const3_0'].out_ports['out_output'] = Port(node=nodes['99_const_rama_flair_const3_0'], name='out_output', description='')
    
    nodes['100_const_N3_flair_stop_0'] = run.create_node(name='100_const_N3_flair_stop_0', type='constant', parent=nodes['7_NUC'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['100_const_N3_flair_stop_0'].out_ports['out_output'] = Port(node=nodes['100_const_N3_flair_stop_0'], name='out_output', description='')
    
    nodes['101_const_registration_parameters'] = run.create_node(name='101_const_registration_parameters', type='constant', parent=nodes['root'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['101_const_registration_parameters'].out_ports['out_output'] = Port(node=nodes['101_const_registration_parameters'], name='out_output', description='')
    
    nodes['102_lobes_nii2ana_flip'] = run.create_node(name='102_lobes_nii2ana_flip', type='node', parent=nodes['5_nii2ana_flip'], no_in_ports=0, no_out_ports=0)

    
    nodes['102_lobes_nii2ana_flip'].in_ports['in_image'] = Port(node=nodes['102_lobes_nii2ana_flip'], name='in_image', description='')

    
    nodes['102_lobes_nii2ana_flip'].out_ports['out_image'] = Port(node=nodes['102_lobes_nii2ana_flip'], name='out_image', description='')
    
    nodes['103_ana2nii_swi'] = run.create_node(name='103_ana2nii_swi', type='node', parent=nodes['6_finalconvert'], no_in_ports=0, no_out_ports=0)

    
    nodes['103_ana2nii_swi'].in_ports['in_image'] = Port(node=nodes['103_ana2nii_swi'], name='in_image', description='')
    nodes['103_ana2nii_swi'].in_ports['in_reference_image'] = Port(node=nodes['103_ana2nii_swi'], name='in_reference_image', description='')

    
    nodes['103_ana2nii_swi'].out_ports['out_image'] = Port(node=nodes['103_ana2nii_swi'], name='out_image', description='')
    
    nodes['104_const_rama_flair_const1_0'] = run.create_node(name='104_const_rama_flair_const1_0', type='constant', parent=nodes['14_rama'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['104_const_rama_flair_const1_0'].out_ports['out_output'] = Port(node=nodes['104_const_rama_flair_const1_0'], name='out_output', description='')
    

    # Links
    run.links.append(Link(from_node=nodes['58_multi_atlas_combine_masks'], to_node=nodes['57_mask_nii2ana_flip'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['47_multi_atlas_combine_lobes_intensity'], to_node=nodes['102_lobes_nii2ana_flip'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['72_multi_atlas_trans_lobes_t1'], to_node=nodes['87_multi_atlas_combine_lobes'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['88_const_multi_atlas_combine_lobes_method_0'], to_node=nodes['87_multi_atlas_combine_lobes'], data_type='__PxCombineSegmentations_0.3.2_interface__method__Enum__'))
    run.links.append(Link(from_node=nodes['61_const_multi_atlas_combine_lobes_number_of_classes_0'], to_node=nodes['87_multi_atlas_combine_lobes'], data_type='Int'))
    run.links.append(Link(from_node=nodes['87_multi_atlas_combine_lobes'], to_node=nodes['47_multi_atlas_combine_lobes_intensity'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['83_multi_atlases_lobes_finallabels'], to_node=nodes['87_multi_atlas_combine_lobes'], data_type='Int'))
    run.links.append(Link(from_node=nodes['59_multi_atlases_lobes_templabels'], to_node=nodes['87_multi_atlas_combine_lobes'], data_type='Int'))
    run.links.append(Link(from_node=nodes['59_multi_atlases_lobes_templabels'], to_node=nodes['47_multi_atlas_combine_lobes_intensity'], data_type='Int'))
    run.links.append(Link(from_node=nodes['83_multi_atlases_lobes_finallabels'], to_node=nodes['47_multi_atlas_combine_lobes_intensity'], data_type='Int'))
    run.links.append(Link(from_node=nodes['62_mincresample_pd'], to_node=nodes['67_N3_PD'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['33_mask2mnc_modify_header'], to_node=nodes['67_N3_PD'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['42_dcm2mnc_t1'], to_node=nodes['19_N3_T1'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['33_mask2mnc_modify_header'], to_node=nodes['19_N3_T1'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['20_const_N3_PD_stop_0'], to_node=nodes['67_N3_PD'], data_type='Float'))
    run.links.append(Link(from_node=nodes['55_const_N3_PD_iterations_0'], to_node=nodes['67_N3_PD'], data_type='Int'))
    run.links.append(Link(from_node=nodes['40_mask2mnc_ana2mnc'], to_node=nodes['33_mask2mnc_modify_header'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['42_dcm2mnc_t1'], to_node=nodes['33_mask2mnc_modify_header'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['98_const_N3_T1_stop_0'], to_node=nodes['19_N3_T1'], data_type='Float'))
    run.links.append(Link(from_node=nodes['28_const_N3_T1_iterations_0'], to_node=nodes['19_N3_T1'], data_type='Int'))
    run.links.append(Link(from_node=nodes['91_BrainMaskImprove'], to_node=nodes['77_ana2nii_knnwml'], data_type='AnalyzeImageFile'))
    run.links.append(Link(from_node=nodes['86_ana2nii_T1'], to_node=nodes['77_ana2nii_knnwml'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['86_ana2nii_T1'], to_node=nodes['73_sink_t1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['32_ana2nii_pd'], to_node=nodes['46_sink_pd'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['57_mask_nii2ana_flip'], to_node=nodes['63_ana2nii_mask'], data_type='AnalyzeImageFile'))
    run.links.append(Link(from_node=nodes['86_ana2nii_T1'], to_node=nodes['63_ana2nii_mask'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['102_lobes_nii2ana_flip'], to_node=nodes['24_ana2nii_lobes'], data_type='AnalyzeImageFile'))
    run.links.append(Link(from_node=nodes['86_ana2nii_T1'], to_node=nodes['24_ana2nii_lobes'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['92_ana2nii_flair'], to_node=nodes['16_sink_flair'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['103_ana2nii_swi'], to_node=nodes['36_sink_swi'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['93_elastix_mask'], to_node=nodes['37_transformix_mask'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['37_transformix_mask'], to_node=nodes['40_mask2mnc_ana2mnc'], data_type='AnalyzeImageFile'))
    run.links.append(Link(from_node=nodes['42_dcm2mnc_t1'], to_node=nodes['80_minc2ana_t1'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['53_const_minc2ana_t1_neuro_flag_0'], to_node=nodes['80_minc2ana_t1'], data_type='Boolean'))
    run.links.append(Link(from_node=nodes['68_dcm2mnc_swi'], to_node=nodes['89_mincresample_swi'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['42_dcm2mnc_t1'], to_node=nodes['89_mincresample_swi'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['101_const_registration_parameters'], to_node=nodes['93_elastix_mask'], data_type='ElastixParameterFile'))
    run.links.append(Link(from_node=nodes['22_const_transformix_mask_image_0'], to_node=nodes['37_transformix_mask'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['80_minc2ana_t1'], to_node=nodes['93_elastix_mask'], data_type='AnalyzeImageFile'))
    run.links.append(Link(from_node=nodes['17_const_elastix_mask_moving_image_0'], to_node=nodes['93_elastix_mask'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['77_ana2nii_knnwml'], to_node=nodes['31_sink_knnwml'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['24_ana2nii_lobes'], to_node=nodes['78_sink_lobes'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['63_ana2nii_mask'], to_node=nodes['35_sink_mask'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['65_ana2nii_knnseg'], to_node=nodes['23_sink_knnseg'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['34_const_rama_flair_const2_0'], to_node=nodes['82_rama_flair'], data_type='Float'))
    run.links.append(Link(from_node=nodes['104_const_rama_flair_const1_0'], to_node=nodes['82_rama_flair'], data_type='Float'))
    run.links.append(Link(from_node=nodes['33_mask2mnc_modify_header'], to_node=nodes['82_rama_flair'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['49_N3_flair'], to_node=nodes['82_rama_flair'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['45_const_rama_PD_constants_0'], to_node=nodes['41_rama_PD'], data_type='Float'))
    run.links.append(Link(from_node=nodes['84_const_rama_PD_range_0'], to_node=nodes['41_rama_PD'], data_type='Float'))
    run.links.append(Link(from_node=nodes['33_mask2mnc_modify_header'], to_node=nodes['41_rama_PD'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['67_N3_PD'], to_node=nodes['41_rama_PD'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['81_const_rama_T1_constants_0'], to_node=nodes['74_rama_T1'], data_type='Float'))
    run.links.append(Link(from_node=nodes['94_const_rama_T1_range_0'], to_node=nodes['74_rama_T1'], data_type='Float'))
    run.links.append(Link(from_node=nodes['18_thresholds'], to_node=nodes['97_mritoself_flair'], data_type='Int'))
    run.links.append(Link(from_node=nodes['42_dcm2mnc_t1'], to_node=nodes['97_mritoself_flair'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['85_SWI'], to_node=nodes['68_dcm2mnc_swi'], data_type='DicomImageFile'))
    run.links.append(Link(from_node=nodes['60_FLAIR'], to_node=nodes['96_dcm2mnc_flair'], data_type='DicomImageFile'))
    run.links.append(Link(from_node=nodes['56_PD'], to_node=nodes['75_dcm2mnc_pd'], data_type='DicomImageFile'))
    run.links.append(Link(from_node=nodes['95_T1'], to_node=nodes['42_dcm2mnc_t1'], data_type='DicomImageFile'))
    run.links.append(Link(from_node=nodes['96_dcm2mnc_flair'], to_node=nodes['97_mritoself_flair'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['18_thresholds'], to_node=nodes['69_mritoself_pd'], data_type='Int'))
    run.links.append(Link(from_node=nodes['42_dcm2mnc_t1'], to_node=nodes['69_mritoself_pd'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['75_dcm2mnc_pd'], to_node=nodes['69_mritoself_pd'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['19_N3_T1'], to_node=nodes['74_rama_T1'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['33_mask2mnc_modify_header'], to_node=nodes['74_rama_T1'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['29_const_N3_swi_iterations_0'], to_node=nodes['38_N3_swi'], data_type='Int'))
    run.links.append(Link(from_node=nodes['26_const_N3_swi_stop_0'], to_node=nodes['38_N3_swi'], data_type='Float'))
    run.links.append(Link(from_node=nodes['33_mask2mnc_modify_header'], to_node=nodes['38_N3_swi'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['89_mincresample_swi'], to_node=nodes['38_N3_swi'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['27_const_N3_flair_iterations_0'], to_node=nodes['49_N3_flair'], data_type='Int'))
    run.links.append(Link(from_node=nodes['100_const_N3_flair_stop_0'], to_node=nodes['49_N3_flair'], data_type='Float'))
    run.links.append(Link(from_node=nodes['33_mask2mnc_modify_header'], to_node=nodes['49_N3_flair'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['76_mincresample_flair'], to_node=nodes['49_N3_flair'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['48_const_multi_atlas_combine_masks_number_of_classes_0'], to_node=nodes['58_multi_atlas_combine_masks'], data_type='Int'))
    run.links.append(Link(from_node=nodes['90_const_multi_atlas_combine_masks_method_0'], to_node=nodes['58_multi_atlas_combine_masks'], data_type='__PxCombineSegmentations_0.3.2_interface__method__Enum__'))
    run.links.append(Link(from_node=nodes['79_multi_atlases_scans_t1'], to_node=nodes['54_multi_atlas_reg_t1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['66_minc2ana_t1_flip'], to_node=nodes['54_multi_atlas_reg_t1'], data_type='AnalyzeImageFile'))
    run.links.append(Link(from_node=nodes['70_multi_atlases_masks'], to_node=nodes['25_multi_atlas_trans_masks_t1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['101_const_registration_parameters'], to_node=nodes['54_multi_atlas_reg_t1'], data_type='ElastixParameterFile'))
    run.links.append(Link(from_node=nodes['71_multi_atlases_lobes'], to_node=nodes['72_multi_atlas_trans_lobes_t1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['54_multi_atlas_reg_t1'], to_node=nodes['25_multi_atlas_trans_masks_t1'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['25_multi_atlas_trans_masks_t1'], to_node=nodes['58_multi_atlas_combine_masks'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['54_multi_atlas_reg_t1'], to_node=nodes['72_multi_atlas_trans_lobes_t1'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['67_N3_PD'], to_node=nodes['30_minc2ana_pd_flip'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['49_N3_flair'], to_node=nodes['52_minc2ana_flair_flip'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['41_rama_PD'], to_node=nodes['51_knnseg'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['19_N3_T1'], to_node=nodes['66_minc2ana_t1_flip'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['39_const_knnseg_trained_features_0'], to_node=nodes['51_knnseg'], data_type='MincClassifyFeatureFile'))
    run.links.append(Link(from_node=nodes['74_rama_T1'], to_node=nodes['51_knnseg'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['99_const_rama_flair_const3_0'], to_node=nodes['82_rama_flair'], data_type='Float'))
    run.links.append(Link(from_node=nodes['43_const_knnseg_parameters_0'], to_node=nodes['51_knnseg'], data_type='MincClassifyParameterFile'))
    run.links.append(Link(from_node=nodes['38_N3_swi'], to_node=nodes['44_minc2ana_swi_flip'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['51_knnseg'], to_node=nodes['21_minc2ana_knnseg_flip'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['69_mritoself_pd'], to_node=nodes['62_mincresample_pd'], data_type='MNIAffineTransformFile'))
    run.links.append(Link(from_node=nodes['18_thresholds'], to_node=nodes['50_mritoself_swi'], data_type='Int'))
    run.links.append(Link(from_node=nodes['42_dcm2mnc_t1'], to_node=nodes['50_mritoself_swi'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['68_dcm2mnc_swi'], to_node=nodes['50_mritoself_swi'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['96_dcm2mnc_flair'], to_node=nodes['76_mincresample_flair'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['97_mritoself_flair'], to_node=nodes['76_mincresample_flair'], data_type='MNIAffineTransformFile'))
    run.links.append(Link(from_node=nodes['42_dcm2mnc_t1'], to_node=nodes['62_mincresample_pd'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['75_dcm2mnc_pd'], to_node=nodes['62_mincresample_pd'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['50_mritoself_swi'], to_node=nodes['89_mincresample_swi'], data_type='MNIAffineTransformFile'))
    run.links.append(Link(from_node=nodes['42_dcm2mnc_t1'], to_node=nodes['76_mincresample_flair'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['86_ana2nii_T1'], to_node=nodes['65_ana2nii_knnseg'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['21_minc2ana_knnseg_flip'], to_node=nodes['65_ana2nii_knnseg'], data_type='AnalyzeImageFile'))
    run.links.append(Link(from_node=nodes['64_wmlsegment'], to_node=nodes['91_BrainMaskImprove'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['57_mask_nii2ana_flip'], to_node=nodes['64_wmlsegment'], data_type='AnalyzeImageFile'))
    run.links.append(Link(from_node=nodes['52_minc2ana_flair_flip'], to_node=nodes['64_wmlsegment'], data_type='AnalyzeImageFile'))
    run.links.append(Link(from_node=nodes['21_minc2ana_knnseg_flip'], to_node=nodes['64_wmlsegment'], data_type='AnalyzeImageFile'))
    run.links.append(Link(from_node=nodes['44_minc2ana_swi_flip'], to_node=nodes['103_ana2nii_swi'], data_type='AnalyzeImageFile'))
    run.links.append(Link(from_node=nodes['52_minc2ana_flair_flip'], to_node=nodes['92_ana2nii_flair'], data_type='AnalyzeImageFile'))
    run.links.append(Link(from_node=nodes['30_minc2ana_pd_flip'], to_node=nodes['32_ana2nii_pd'], data_type='AnalyzeImageFile'))
    run.links.append(Link(from_node=nodes['66_minc2ana_t1_flip'], to_node=nodes['86_ana2nii_T1'], data_type='AnalyzeImageFile'))

    return run

