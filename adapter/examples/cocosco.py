import os

from adapter.link import Link
from adapter.run import Run
from adapter.port import Port


def create_run(**kwargs):
    run = Run()
    run.name = 'cocosco'

    os.environ['RUN_NAME'] = run.name

    nodes = dict()

    nodes['root'] = run.create_node(name='root', parent=None)

    run.root = nodes['root']

    
    nodes['step_elastix_mask'] = run.create_node(name='step_elastix_mask', description='Leaf', parent=nodes['root'])
    nodes['step_convert'] = run.create_node(name='step_convert', description='Leaf', parent=nodes['root'])
    nodes['step_multi_atlas_reg'] = run.create_node(name='step_multi_atlas_reg', description='Leaf', parent=nodes['root'])
    nodes['step_sinks'] = run.create_node(name='step_sinks', description='Leaf', parent=nodes['root'])
    nodes['step_wmlsegment'] = run.create_node(name='step_wmlsegment', description='Leaf', parent=nodes['root'])
    nodes['step_classifier'] = run.create_node(name='step_classifier', description='Leaf', parent=nodes['root'])
    nodes['step_sources'] = run.create_node(name='step_sources', description='Leaf', parent=nodes['root'])
    nodes['step_N4'] = run.create_node(name='step_N4', description='Leaf', parent=nodes['root'])
    nodes['step_rama'] = run.create_node(name='step_rama', description='Leaf', parent=nodes['root'])
    nodes['step_co-registration'] = run.create_node(name='step_co-registration', description='Leaf', parent=nodes['root'])

    
    nodes['sink_flair'] = run.create_node(name='sink_flair', type='sink', parent=nodes['step_sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['sink_flair'].in_ports['in_input'] = Port(node=nodes['sink_flair'], name='in_input', description='')

    
    
    nodes['const_elastix_mask_moving_image_0'] = run.create_node(name='const_elastix_mask_moving_image_0', type='constant', parent=nodes['step_elastix_mask'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_elastix_mask_moving_image_0'].out_ports['out_output'] = Port(node=nodes['const_elastix_mask_moving_image_0'], name='out_output', description='')
    
    nodes['const_transformix_mask_image_0'] = run.create_node(name='const_transformix_mask_image_0', type='constant', parent=nodes['step_elastix_mask'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_transformix_mask_image_0'].out_ports['out_output'] = Port(node=nodes['const_transformix_mask_image_0'], name='out_output', description='')
    
    nodes['sink_knnseg'] = run.create_node(name='sink_knnseg', type='sink', parent=nodes['step_sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['sink_knnseg'].in_ports['in_input'] = Port(node=nodes['sink_knnseg'], name='in_input', description='')

    
    
    nodes['multi_atlas_trans_masks_t1'] = run.create_node(name='multi_atlas_trans_masks_t1', type='node', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    
    nodes['multi_atlas_trans_masks_t1'].in_ports['in_transform'] = Port(node=nodes['multi_atlas_trans_masks_t1'], name='in_transform', description='')
    nodes['multi_atlas_trans_masks_t1'].in_ports['in_image'] = Port(node=nodes['multi_atlas_trans_masks_t1'], name='in_image', description='')
    nodes['multi_atlas_trans_masks_t1'].in_ports['in_points'] = Port(node=nodes['multi_atlas_trans_masks_t1'], name='in_points', description='')
    nodes['multi_atlas_trans_masks_t1'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['multi_atlas_trans_masks_t1'], name='in_determinant_of_jacobian_flag', description='')
    nodes['multi_atlas_trans_masks_t1'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['multi_atlas_trans_masks_t1'], name='in_jacobian_matrix_flag', description='')
    nodes['multi_atlas_trans_masks_t1'].in_ports['in_priority'] = Port(node=nodes['multi_atlas_trans_masks_t1'], name='in_priority', description='')
    nodes['multi_atlas_trans_masks_t1'].in_ports['in_threads'] = Port(node=nodes['multi_atlas_trans_masks_t1'], name='in_threads', description='')

    
    nodes['multi_atlas_trans_masks_t1'].out_ports['out_directory'] = Port(node=nodes['multi_atlas_trans_masks_t1'], name='out_directory', description='')
    nodes['multi_atlas_trans_masks_t1'].out_ports['out_image'] = Port(node=nodes['multi_atlas_trans_masks_t1'], name='out_image', description='')
    nodes['multi_atlas_trans_masks_t1'].out_ports['out_points'] = Port(node=nodes['multi_atlas_trans_masks_t1'], name='out_points', description='')
    nodes['multi_atlas_trans_masks_t1'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['multi_atlas_trans_masks_t1'], name='out_determinant_of_jacobian', description='')
    nodes['multi_atlas_trans_masks_t1'].out_ports['out_jacobian_matrix'] = Port(node=nodes['multi_atlas_trans_masks_t1'], name='out_jacobian_matrix', description='')
    nodes['multi_atlas_trans_masks_t1'].out_ports['out_log_file'] = Port(node=nodes['multi_atlas_trans_masks_t1'], name='out_log_file', description='')
    
    nodes['const_remove_neck_mask_FOV_0'] = run.create_node(name='const_remove_neck_mask_FOV_0', type='constant', parent=nodes['step_co-registration'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_remove_neck_mask_FOV_0'].out_ports['out_output'] = Port(node=nodes['const_remove_neck_mask_FOV_0'], name='out_output', description='')
    
    nodes['const_combine_prob_masks_operator_0'] = run.create_node(name='const_combine_prob_masks_operator_0', type='constant', parent=nodes['step_classifier'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_combine_prob_masks_operator_0'].out_ports['out_output'] = Port(node=nodes['const_combine_prob_masks_operator_0'], name='out_output', description='')
    
    nodes['const_n4_T1_bspline_fitting_0'] = run.create_node(name='const_n4_T1_bspline_fitting_0', type='constant', parent=nodes['step_N4'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_n4_T1_bspline_fitting_0'].out_ports['out_output'] = Port(node=nodes['const_n4_T1_bspline_fitting_0'], name='out_output', description='')
    
    nodes['n4_pd'] = run.create_node(name='n4_pd', type='node', parent=nodes['step_N4'], no_in_ports=0, no_out_ports=0)

    
    nodes['n4_pd'].in_ports['in_number_of_dimensions'] = Port(node=nodes['n4_pd'], name='in_number_of_dimensions', description='')
    nodes['n4_pd'].in_ports['in_image'] = Port(node=nodes['n4_pd'], name='in_image', description='')
    nodes['n4_pd'].in_ports['in_mask'] = Port(node=nodes['n4_pd'], name='in_mask', description='')
    nodes['n4_pd'].in_ports['in_weight_image'] = Port(node=nodes['n4_pd'], name='in_weight_image', description='')
    nodes['n4_pd'].in_ports['in_shrink_factor'] = Port(node=nodes['n4_pd'], name='in_shrink_factor', description='')
    nodes['n4_pd'].in_ports['in_converge'] = Port(node=nodes['n4_pd'], name='in_converge', description='')
    nodes['n4_pd'].in_ports['in_bspline_fitting'] = Port(node=nodes['n4_pd'], name='in_bspline_fitting', description='')

    
    nodes['n4_pd'].out_ports['out_image'] = Port(node=nodes['n4_pd'], name='out_image', description='')
    
    nodes['sink_knnwml'] = run.create_node(name='sink_knnwml', type='sink', parent=nodes['step_sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['sink_knnwml'].in_ports['in_input'] = Port(node=nodes['sink_knnwml'], name='in_input', description='')

    
    
    nodes['const_sample_image_seed_int_0'] = run.create_node(name='const_sample_image_seed_int_0', type='constant', parent=nodes['step_classifier'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_sample_image_seed_int_0'].out_ports['out_output'] = Port(node=nodes['const_sample_image_seed_int_0'], name='out_output', description='')
    
    nodes['sink_mask'] = run.create_node(name='sink_mask', type='sink', parent=nodes['step_sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['sink_mask'].in_ports['in_input'] = Port(node=nodes['sink_mask'], name='in_input', description='')

    
    
    nodes['transformix_mask'] = run.create_node(name='transformix_mask', type='node', parent=nodes['step_elastix_mask'], no_in_ports=0, no_out_ports=0)

    
    nodes['transformix_mask'].in_ports['in_transform'] = Port(node=nodes['transformix_mask'], name='in_transform', description='')
    nodes['transformix_mask'].in_ports['in_image'] = Port(node=nodes['transformix_mask'], name='in_image', description='')
    nodes['transformix_mask'].in_ports['in_points'] = Port(node=nodes['transformix_mask'], name='in_points', description='')
    nodes['transformix_mask'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['transformix_mask'], name='in_determinant_of_jacobian_flag', description='')
    nodes['transformix_mask'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['transformix_mask'], name='in_jacobian_matrix_flag', description='')
    nodes['transformix_mask'].in_ports['in_priority'] = Port(node=nodes['transformix_mask'], name='in_priority', description='')
    nodes['transformix_mask'].in_ports['in_threads'] = Port(node=nodes['transformix_mask'], name='in_threads', description='')

    
    nodes['transformix_mask'].out_ports['out_directory'] = Port(node=nodes['transformix_mask'], name='out_directory', description='')
    nodes['transformix_mask'].out_ports['out_image'] = Port(node=nodes['transformix_mask'], name='out_image', description='')
    nodes['transformix_mask'].out_ports['out_points'] = Port(node=nodes['transformix_mask'], name='out_points', description='')
    nodes['transformix_mask'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['transformix_mask'], name='out_determinant_of_jacobian', description='')
    nodes['transformix_mask'].out_ports['out_jacobian_matrix'] = Port(node=nodes['transformix_mask'], name='out_jacobian_matrix', description='')
    nodes['transformix_mask'].out_ports['out_log_file'] = Port(node=nodes['transformix_mask'], name='out_log_file', description='')
    
    nodes['const_rama_T1_lower_percentile_0'] = run.create_node(name='const_rama_T1_lower_percentile_0', type='constant', parent=nodes['step_rama'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_rama_T1_lower_percentile_0'].out_ports['out_output'] = Port(node=nodes['const_rama_T1_lower_percentile_0'], name='out_output', description='')
    
    nodes['rama_PD'] = run.create_node(name='rama_PD', type='node', parent=nodes['step_rama'], no_in_ports=0, no_out_ports=0)

    
    nodes['rama_PD'].in_ports['in_image'] = Port(node=nodes['rama_PD'], name='in_image', description='')
    nodes['rama_PD'].in_ports['in_mask'] = Port(node=nodes['rama_PD'], name='in_mask', description='')
    nodes['rama_PD'].in_ports['in_upper_percentile'] = Port(node=nodes['rama_PD'], name='in_upper_percentile', description='')
    nodes['rama_PD'].in_ports['in_lower_percentile'] = Port(node=nodes['rama_PD'], name='in_lower_percentile', description='')

    
    nodes['rama_PD'].out_ports['out_image'] = Port(node=nodes['rama_PD'], name='out_image', description='')
    
    nodes['const_BrainMaskImprove_fraction_0'] = run.create_node(name='const_BrainMaskImprove_fraction_0', type='constant', parent=nodes['step_wmlsegment'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_BrainMaskImprove_fraction_0'].out_ports['out_output'] = Port(node=nodes['const_BrainMaskImprove_fraction_0'], name='out_output', description='')
    
    nodes['multi_atlas_combine_masks'] = run.create_node(name='multi_atlas_combine_masks', type='node', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    
    nodes['multi_atlas_combine_masks'].in_ports['in_images'] = Port(node=nodes['multi_atlas_combine_masks'], name='in_images', description='')
    nodes['multi_atlas_combine_masks'].in_ports['in_method'] = Port(node=nodes['multi_atlas_combine_masks'], name='in_method', description='')
    nodes['multi_atlas_combine_masks'].in_ports['in_number_of_classes'] = Port(node=nodes['multi_atlas_combine_masks'], name='in_number_of_classes', description='')
    nodes['multi_atlas_combine_masks'].in_ports['in_original_labels'] = Port(node=nodes['multi_atlas_combine_masks'], name='in_original_labels', description='')
    nodes['multi_atlas_combine_masks'].in_ports['in_substitute_labels'] = Port(node=nodes['multi_atlas_combine_masks'], name='in_substitute_labels', description='')

    
    nodes['multi_atlas_combine_masks'].out_ports['out_hard_segment'] = Port(node=nodes['multi_atlas_combine_masks'], name='out_hard_segment', description='')
    nodes['multi_atlas_combine_masks'].out_ports['out_soft_segment'] = Port(node=nodes['multi_atlas_combine_masks'], name='out_soft_segment', description='')
    
    nodes['const_apply_classifier_number_of_classes_0'] = run.create_node(name='const_apply_classifier_number_of_classes_0', type='constant', parent=nodes['step_classifier'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_apply_classifier_number_of_classes_0'].out_ports['out_output'] = Port(node=nodes['const_apply_classifier_number_of_classes_0'], name='out_output', description='')
    
    nodes['sink_pd'] = run.create_node(name='sink_pd', type='sink', parent=nodes['step_sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['sink_pd'].in_ports['in_input'] = Port(node=nodes['sink_pd'], name='in_input', description='')

    
    
    nodes['const_n4_pd_converge_0'] = run.create_node(name='const_n4_pd_converge_0', type='constant', parent=nodes['step_N4'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_n4_pd_converge_0'].out_ports['out_output'] = Port(node=nodes['const_n4_pd_converge_0'], name='out_output', description='')
    
    nodes['multi_atlas_combine_lobes_intensity'] = run.create_node(name='multi_atlas_combine_lobes_intensity', type='node', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    
    nodes['multi_atlas_combine_lobes_intensity'].in_ports['in_image'] = Port(node=nodes['multi_atlas_combine_lobes_intensity'], name='in_image', description='')
    nodes['multi_atlas_combine_lobes_intensity'].in_ports['in_original_values'] = Port(node=nodes['multi_atlas_combine_lobes_intensity'], name='in_original_values', description='')
    nodes['multi_atlas_combine_lobes_intensity'].in_ports['in_substitute_values'] = Port(node=nodes['multi_atlas_combine_lobes_intensity'], name='in_substitute_values', description='')
    nodes['multi_atlas_combine_lobes_intensity'].in_ports['in_component_type'] = Port(node=nodes['multi_atlas_combine_lobes_intensity'], name='in_component_type', description='')

    
    nodes['multi_atlas_combine_lobes_intensity'].out_ports['out_image'] = Port(node=nodes['multi_atlas_combine_lobes_intensity'], name='out_image', description='')
    
    nodes['threshold_prob'] = run.create_node(name='threshold_prob', type='node', parent=nodes['step_classifier'], no_in_ports=0, no_out_ports=0)

    
    nodes['threshold_prob'].in_ports['in_image'] = Port(node=nodes['threshold_prob'], name='in_image', description='')
    nodes['threshold_prob'].in_ports['in_threshold'] = Port(node=nodes['threshold_prob'], name='in_threshold', description='')

    
    nodes['threshold_prob'].out_ports['out_image'] = Port(node=nodes['threshold_prob'], name='out_image', description='')
    
    nodes['const_multi_atlas_combine_masks_number_of_classes_0'] = run.create_node(name='const_multi_atlas_combine_masks_number_of_classes_0', type='constant', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_multi_atlas_combine_masks_number_of_classes_0'].out_ports['out_output'] = Port(node=nodes['const_multi_atlas_combine_masks_number_of_classes_0'], name='out_output', description='')
    
    nodes['const_rama_flair_upper_percentile_0'] = run.create_node(name='const_rama_flair_upper_percentile_0', type='constant', parent=nodes['step_rama'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_rama_flair_upper_percentile_0'].out_ports['out_output'] = Port(node=nodes['const_rama_flair_upper_percentile_0'], name='out_output', description='')
    
    nodes['const_n4_T1_shrink_factor_0'] = run.create_node(name='const_n4_T1_shrink_factor_0', type='constant', parent=nodes['step_N4'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_n4_T1_shrink_factor_0'].out_ports['out_output'] = Port(node=nodes['const_n4_T1_shrink_factor_0'], name='out_output', description='')
    
    nodes['const_wmlsegment_beta_0'] = run.create_node(name='const_wmlsegment_beta_0', type='constant', parent=nodes['step_wmlsegment'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_wmlsegment_beta_0'].out_ports['out_output'] = Port(node=nodes['const_wmlsegment_beta_0'], name='out_output', description='')
    
    nodes['const_rigid_paramter'] = run.create_node(name='const_rigid_paramter', type='constant', parent=nodes['step_co-registration'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_rigid_paramter'].out_ports['out_output'] = Port(node=nodes['const_rigid_paramter'], name='out_output', description='')
    
    nodes['const_n4_T1_converge_0'] = run.create_node(name='const_n4_T1_converge_0', type='constant', parent=nodes['step_N4'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_n4_T1_converge_0'].out_ports['out_output'] = Port(node=nodes['const_n4_T1_converge_0'], name='out_output', description='')
    
    nodes['multi_atlas_reg_t1'] = run.create_node(name='multi_atlas_reg_t1', type='node', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    
    nodes['multi_atlas_reg_t1'].in_ports['in_fixed_image'] = Port(node=nodes['multi_atlas_reg_t1'], name='in_fixed_image', description='')
    nodes['multi_atlas_reg_t1'].in_ports['in_moving_image'] = Port(node=nodes['multi_atlas_reg_t1'], name='in_moving_image', description='')
    nodes['multi_atlas_reg_t1'].in_ports['in_parameters'] = Port(node=nodes['multi_atlas_reg_t1'], name='in_parameters', description='')
    nodes['multi_atlas_reg_t1'].in_ports['in_fixed_mask'] = Port(node=nodes['multi_atlas_reg_t1'], name='in_fixed_mask', description='')
    nodes['multi_atlas_reg_t1'].in_ports['in_moving_mask'] = Port(node=nodes['multi_atlas_reg_t1'], name='in_moving_mask', description='')
    nodes['multi_atlas_reg_t1'].in_ports['in_initial_transform'] = Port(node=nodes['multi_atlas_reg_t1'], name='in_initial_transform', description='')
    nodes['multi_atlas_reg_t1'].in_ports['in_priority'] = Port(node=nodes['multi_atlas_reg_t1'], name='in_priority', description='')
    nodes['multi_atlas_reg_t1'].in_ports['in_threads'] = Port(node=nodes['multi_atlas_reg_t1'], name='in_threads', description='')

    
    nodes['multi_atlas_reg_t1'].out_ports['out_directory'] = Port(node=nodes['multi_atlas_reg_t1'], name='out_directory', description='')
    nodes['multi_atlas_reg_t1'].out_ports['out_transform'] = Port(node=nodes['multi_atlas_reg_t1'], name='out_transform', description='')
    nodes['multi_atlas_reg_t1'].out_ports['out_log_file'] = Port(node=nodes['multi_atlas_reg_t1'], name='out_log_file', description='')
    
    nodes['PD'] = run.create_node(name='PD', type='source', parent=nodes['step_sources'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['PD'].out_ports['out_output'] = Port(node=nodes['PD'], name='out_output', description='')
    
    nodes['const_train_classifier_parameters_0'] = run.create_node(name='const_train_classifier_parameters_0', type='constant', parent=nodes['step_classifier'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_train_classifier_parameters_0'].out_ports['out_output'] = Port(node=nodes['const_train_classifier_parameters_0'], name='out_output', description='')
    
    nodes['co_register_flair'] = run.create_node(name='co_register_flair', type='node', parent=nodes['step_co-registration'], no_in_ports=0, no_out_ports=0)

    
    nodes['co_register_flair'].in_ports['in_fixed_image'] = Port(node=nodes['co_register_flair'], name='in_fixed_image', description='')
    nodes['co_register_flair'].in_ports['in_moving_image'] = Port(node=nodes['co_register_flair'], name='in_moving_image', description='')
    nodes['co_register_flair'].in_ports['in_parameters'] = Port(node=nodes['co_register_flair'], name='in_parameters', description='')
    nodes['co_register_flair'].in_ports['in_fixed_mask'] = Port(node=nodes['co_register_flair'], name='in_fixed_mask', description='')
    nodes['co_register_flair'].in_ports['in_moving_mask'] = Port(node=nodes['co_register_flair'], name='in_moving_mask', description='')
    nodes['co_register_flair'].in_ports['in_initial_transform'] = Port(node=nodes['co_register_flair'], name='in_initial_transform', description='')
    nodes['co_register_flair'].in_ports['in_priority'] = Port(node=nodes['co_register_flair'], name='in_priority', description='')
    nodes['co_register_flair'].in_ports['in_threads'] = Port(node=nodes['co_register_flair'], name='in_threads', description='')

    
    nodes['co_register_flair'].out_ports['out_directory'] = Port(node=nodes['co_register_flair'], name='out_directory', description='')
    nodes['co_register_flair'].out_ports['out_transform'] = Port(node=nodes['co_register_flair'], name='out_transform', description='')
    nodes['co_register_flair'].out_ports['out_log_file'] = Port(node=nodes['co_register_flair'], name='out_log_file', description='')
    
    nodes['dcm2nii_t1'] = run.create_node(name='dcm2nii_t1', type='node', parent=nodes['step_convert'], no_in_ports=0, no_out_ports=0)

    
    nodes['dcm2nii_t1'].in_ports['in_dicom_image'] = Port(node=nodes['dcm2nii_t1'], name='in_dicom_image', description='')

    
    nodes['dcm2nii_t1'].out_ports['out_directory'] = Port(node=nodes['dcm2nii_t1'], name='out_directory', description='')
    nodes['dcm2nii_t1'].out_ports['out_image'] = Port(node=nodes['dcm2nii_t1'], name='out_image', description='')
    nodes['dcm2nii_t1'].out_ports['out_bval'] = Port(node=nodes['dcm2nii_t1'], name='out_bval', description='')
    nodes['dcm2nii_t1'].out_ports['out_bvec'] = Port(node=nodes['dcm2nii_t1'], name='out_bvec', description='')
    
    nodes['sample_image'] = run.create_node(name='sample_image', type='node', parent=nodes['step_classifier'], no_in_ports=0, no_out_ports=0)

    
    nodes['sample_image'].in_ports['in_image'] = Port(node=nodes['sample_image'], name='in_image', description='')
    nodes['sample_image'].in_ports['in_labels'] = Port(node=nodes['sample_image'], name='in_labels', description='')
    nodes['sample_image'].in_ports['in_mask'] = Port(node=nodes['sample_image'], name='in_mask', description='')
    nodes['sample_image'].in_ports['in_nsamples'] = Port(node=nodes['sample_image'], name='in_nsamples', description='')
    nodes['sample_image'].in_ports['in_seed_int'] = Port(node=nodes['sample_image'], name='in_seed_int', description='')
    nodes['sample_image'].in_ports['in_use_negative_flag'] = Port(node=nodes['sample_image'], name='in_use_negative_flag', description='')

    
    nodes['sample_image'].out_ports['out_sample_file'] = Port(node=nodes['sample_image'], name='out_sample_file', description='')
    
    nodes['multi_atlases_lobes_templabels'] = run.create_node(name='multi_atlases_lobes_templabels', type='constant', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['multi_atlases_lobes_templabels'].out_ports['out_output'] = Port(node=nodes['multi_atlases_lobes_templabels'], name='out_output', description='')
    
    nodes['combine_prob_masks'] = run.create_node(name='combine_prob_masks', type='node', parent=nodes['step_classifier'], no_in_ports=0, no_out_ports=0)

    
    nodes['combine_prob_masks'].in_ports['in_image'] = Port(node=nodes['combine_prob_masks'], name='in_image', description='')
    nodes['combine_prob_masks'].in_ports['in_operator'] = Port(node=nodes['combine_prob_masks'], name='in_operator', description='')
    nodes['combine_prob_masks'].in_ports['in_compression_flag'] = Port(node=nodes['combine_prob_masks'], name='in_compression_flag', description='')
    nodes['combine_prob_masks'].in_ports['in_component_type'] = Port(node=nodes['combine_prob_masks'], name='in_component_type', description='')

    
    nodes['combine_prob_masks'].out_ports['out_image'] = Port(node=nodes['combine_prob_masks'], name='out_image', description='')
    
    nodes['n4_flair'] = run.create_node(name='n4_flair', type='node', parent=nodes['step_N4'], no_in_ports=0, no_out_ports=0)

    
    nodes['n4_flair'].in_ports['in_number_of_dimensions'] = Port(node=nodes['n4_flair'], name='in_number_of_dimensions', description='')
    nodes['n4_flair'].in_ports['in_image'] = Port(node=nodes['n4_flair'], name='in_image', description='')
    nodes['n4_flair'].in_ports['in_mask'] = Port(node=nodes['n4_flair'], name='in_mask', description='')
    nodes['n4_flair'].in_ports['in_weight_image'] = Port(node=nodes['n4_flair'], name='in_weight_image', description='')
    nodes['n4_flair'].in_ports['in_shrink_factor'] = Port(node=nodes['n4_flair'], name='in_shrink_factor', description='')
    nodes['n4_flair'].in_ports['in_converge'] = Port(node=nodes['n4_flair'], name='in_converge', description='')
    nodes['n4_flair'].in_ports['in_bspline_fitting'] = Port(node=nodes['n4_flair'], name='in_bspline_fitting', description='')

    
    nodes['n4_flair'].out_ports['out_image'] = Port(node=nodes['n4_flair'], name='out_image', description='')
    
    nodes['FLAIR'] = run.create_node(name='FLAIR', type='source', parent=nodes['step_sources'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['FLAIR'].out_ports['out_output'] = Port(node=nodes['FLAIR'], name='out_output', description='')
    
    nodes['const_multi_atlas_combine_lobes_number_of_classes_0'] = run.create_node(name='const_multi_atlas_combine_lobes_number_of_classes_0', type='constant', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_multi_atlas_combine_lobes_number_of_classes_0'].out_ports['out_output'] = Port(node=nodes['const_multi_atlas_combine_lobes_number_of_classes_0'], name='out_output', description='')
    
    nodes['wmlsegment'] = run.create_node(name='wmlsegment', type='node', parent=nodes['step_wmlsegment'], no_in_ports=0, no_out_ports=0)

    
    nodes['wmlsegment'].in_ports['in_tissue_labels'] = Port(node=nodes['wmlsegment'], name='in_tissue_labels', description='')
    nodes['wmlsegment'].in_ports['in_flair'] = Port(node=nodes['wmlsegment'], name='in_flair', description='')
    nodes['wmlsegment'].in_ports['in_brain_mask'] = Port(node=nodes['wmlsegment'], name='in_brain_mask', description='')
    nodes['wmlsegment'].in_ports['in_alpha'] = Port(node=nodes['wmlsegment'], name='in_alpha', description='')
    nodes['wmlsegment'].in_ports['in_beta'] = Port(node=nodes['wmlsegment'], name='in_beta', description='')

    
    nodes['wmlsegment'].out_ports['out_white_matter_lesion_mask'] = Port(node=nodes['wmlsegment'], name='out_white_matter_lesion_mask', description='')
    
    nodes['const_wmlsegment_alpha_0'] = run.create_node(name='const_wmlsegment_alpha_0', type='constant', parent=nodes['step_wmlsegment'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_wmlsegment_alpha_0'].out_ports['out_output'] = Port(node=nodes['const_wmlsegment_alpha_0'], name='out_output', description='')
    
    nodes['const_multi_atlas_combine_tissue_method_0'] = run.create_node(name='const_multi_atlas_combine_tissue_method_0', type='constant', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_multi_atlas_combine_tissue_method_0'].out_ports['out_output'] = Port(node=nodes['const_multi_atlas_combine_tissue_method_0'], name='out_output', description='')
    
    nodes['const_threshold_prob_threshold_0'] = run.create_node(name='const_threshold_prob_threshold_0', type='constant', parent=nodes['step_classifier'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_threshold_prob_threshold_0'].out_ports['out_output'] = Port(node=nodes['const_threshold_prob_threshold_0'], name='out_output', description='')
    
    nodes['multi_atlases_tissue'] = run.create_node(name='multi_atlases_tissue', type='constant', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['multi_atlases_tissue'].out_ports['out_output'] = Port(node=nodes['multi_atlases_tissue'], name='out_output', description='')
    
    nodes['const_multi_atlas_combine_tissue_original_labels_0'] = run.create_node(name='const_multi_atlas_combine_tissue_original_labels_0', type='constant', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_multi_atlas_combine_tissue_original_labels_0'].out_ports['out_output'] = Port(node=nodes['const_multi_atlas_combine_tissue_original_labels_0'], name='out_output', description='')
    
    nodes['multi_atlases_masks'] = run.create_node(name='multi_atlases_masks', type='constant', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['multi_atlases_masks'].out_ports['out_output'] = Port(node=nodes['multi_atlases_masks'], name='out_output', description='')
    
    nodes['multi_atlases_lobes'] = run.create_node(name='multi_atlases_lobes', type='constant', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['multi_atlases_lobes'].out_ports['out_output'] = Port(node=nodes['multi_atlases_lobes'], name='out_output', description='')
    
    nodes['dcm2nii_pd'] = run.create_node(name='dcm2nii_pd', type='node', parent=nodes['step_convert'], no_in_ports=0, no_out_ports=0)

    
    nodes['dcm2nii_pd'].in_ports['in_dicom_image'] = Port(node=nodes['dcm2nii_pd'], name='in_dicom_image', description='')

    
    nodes['dcm2nii_pd'].out_ports['out_directory'] = Port(node=nodes['dcm2nii_pd'], name='out_directory', description='')
    nodes['dcm2nii_pd'].out_ports['out_image'] = Port(node=nodes['dcm2nii_pd'], name='out_image', description='')
    nodes['dcm2nii_pd'].out_ports['out_bval'] = Port(node=nodes['dcm2nii_pd'], name='out_bval', description='')
    nodes['dcm2nii_pd'].out_ports['out_bvec'] = Port(node=nodes['dcm2nii_pd'], name='out_bvec', description='')
    
    nodes['multi_atlas_trans_lobes_t1'] = run.create_node(name='multi_atlas_trans_lobes_t1', type='node', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    
    nodes['multi_atlas_trans_lobes_t1'].in_ports['in_transform'] = Port(node=nodes['multi_atlas_trans_lobes_t1'], name='in_transform', description='')
    nodes['multi_atlas_trans_lobes_t1'].in_ports['in_image'] = Port(node=nodes['multi_atlas_trans_lobes_t1'], name='in_image', description='')
    nodes['multi_atlas_trans_lobes_t1'].in_ports['in_points'] = Port(node=nodes['multi_atlas_trans_lobes_t1'], name='in_points', description='')
    nodes['multi_atlas_trans_lobes_t1'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['multi_atlas_trans_lobes_t1'], name='in_determinant_of_jacobian_flag', description='')
    nodes['multi_atlas_trans_lobes_t1'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['multi_atlas_trans_lobes_t1'], name='in_jacobian_matrix_flag', description='')
    nodes['multi_atlas_trans_lobes_t1'].in_ports['in_priority'] = Port(node=nodes['multi_atlas_trans_lobes_t1'], name='in_priority', description='')
    nodes['multi_atlas_trans_lobes_t1'].in_ports['in_threads'] = Port(node=nodes['multi_atlas_trans_lobes_t1'], name='in_threads', description='')

    
    nodes['multi_atlas_trans_lobes_t1'].out_ports['out_directory'] = Port(node=nodes['multi_atlas_trans_lobes_t1'], name='out_directory', description='')
    nodes['multi_atlas_trans_lobes_t1'].out_ports['out_image'] = Port(node=nodes['multi_atlas_trans_lobes_t1'], name='out_image', description='')
    nodes['multi_atlas_trans_lobes_t1'].out_ports['out_points'] = Port(node=nodes['multi_atlas_trans_lobes_t1'], name='out_points', description='')
    nodes['multi_atlas_trans_lobes_t1'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['multi_atlas_trans_lobes_t1'], name='out_determinant_of_jacobian', description='')
    nodes['multi_atlas_trans_lobes_t1'].out_ports['out_jacobian_matrix'] = Port(node=nodes['multi_atlas_trans_lobes_t1'], name='out_jacobian_matrix', description='')
    nodes['multi_atlas_trans_lobes_t1'].out_ports['out_log_file'] = Port(node=nodes['multi_atlas_trans_lobes_t1'], name='out_log_file', description='')
    
    nodes['const_n4_pd_bspline_fitting_0'] = run.create_node(name='const_n4_pd_bspline_fitting_0', type='constant', parent=nodes['step_N4'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_n4_pd_bspline_fitting_0'].out_ports['out_output'] = Port(node=nodes['const_n4_pd_bspline_fitting_0'], name='out_output', description='')
    
    nodes['transform_flair'] = run.create_node(name='transform_flair', type='node', parent=nodes['step_co-registration'], no_in_ports=0, no_out_ports=0)

    
    nodes['transform_flair'].in_ports['in_transform'] = Port(node=nodes['transform_flair'], name='in_transform', description='')
    nodes['transform_flair'].in_ports['in_image'] = Port(node=nodes['transform_flair'], name='in_image', description='')
    nodes['transform_flair'].in_ports['in_points'] = Port(node=nodes['transform_flair'], name='in_points', description='')
    nodes['transform_flair'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['transform_flair'], name='in_determinant_of_jacobian_flag', description='')
    nodes['transform_flair'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['transform_flair'], name='in_jacobian_matrix_flag', description='')
    nodes['transform_flair'].in_ports['in_priority'] = Port(node=nodes['transform_flair'], name='in_priority', description='')
    nodes['transform_flair'].in_ports['in_threads'] = Port(node=nodes['transform_flair'], name='in_threads', description='')

    
    nodes['transform_flair'].out_ports['out_directory'] = Port(node=nodes['transform_flair'], name='out_directory', description='')
    nodes['transform_flair'].out_ports['out_image'] = Port(node=nodes['transform_flair'], name='out_image', description='')
    nodes['transform_flair'].out_ports['out_points'] = Port(node=nodes['transform_flair'], name='out_points', description='')
    nodes['transform_flair'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['transform_flair'], name='out_determinant_of_jacobian', description='')
    nodes['transform_flair'].out_ports['out_jacobian_matrix'] = Port(node=nodes['transform_flair'], name='out_jacobian_matrix', description='')
    nodes['transform_flair'].out_ports['out_log_file'] = Port(node=nodes['transform_flair'], name='out_log_file', description='')
    
    nodes['const_n4_flair_shrink_factor_0'] = run.create_node(name='const_n4_flair_shrink_factor_0', type='constant', parent=nodes['step_N4'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_n4_flair_shrink_factor_0'].out_ports['out_output'] = Port(node=nodes['const_n4_flair_shrink_factor_0'], name='out_output', description='')
    
    nodes['sink_t1'] = run.create_node(name='sink_t1', type='sink', parent=nodes['step_sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['sink_t1'].in_ports['in_input'] = Port(node=nodes['sink_t1'], name='in_input', description='')

    
    
    nodes['rama_T1'] = run.create_node(name='rama_T1', type='node', parent=nodes['step_rama'], no_in_ports=0, no_out_ports=0)

    
    nodes['rama_T1'].in_ports['in_image'] = Port(node=nodes['rama_T1'], name='in_image', description='')
    nodes['rama_T1'].in_ports['in_mask'] = Port(node=nodes['rama_T1'], name='in_mask', description='')
    nodes['rama_T1'].in_ports['in_upper_percentile'] = Port(node=nodes['rama_T1'], name='in_upper_percentile', description='')
    nodes['rama_T1'].in_ports['in_lower_percentile'] = Port(node=nodes['rama_T1'], name='in_lower_percentile', description='')

    
    nodes['rama_T1'].out_ports['out_image'] = Port(node=nodes['rama_T1'], name='out_image', description='')
    
    nodes['const_rama_PD_upper_percentile_0'] = run.create_node(name='const_rama_PD_upper_percentile_0', type='constant', parent=nodes['step_rama'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_rama_PD_upper_percentile_0'].out_ports['out_output'] = Port(node=nodes['const_rama_PD_upper_percentile_0'], name='out_output', description='')
    
    nodes['const_multi_atlas_combine_masks_method_0'] = run.create_node(name='const_multi_atlas_combine_masks_method_0', type='constant', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_multi_atlas_combine_masks_method_0'].out_ports['out_output'] = Port(node=nodes['const_multi_atlas_combine_masks_method_0'], name='out_output', description='')
    
    nodes['sink_lobes'] = run.create_node(name='sink_lobes', type='sink', parent=nodes['step_sinks'], no_in_ports=0, no_out_ports=0)

    
    nodes['sink_lobes'].in_ports['in_input'] = Port(node=nodes['sink_lobes'], name='in_input', description='')

    
    
    nodes['const_apply_classifier_number_of_cores_0'] = run.create_node(name='const_apply_classifier_number_of_cores_0', type='constant', parent=nodes['step_classifier'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_apply_classifier_number_of_cores_0'].out_ports['out_output'] = Port(node=nodes['const_apply_classifier_number_of_cores_0'], name='out_output', description='')
    
    nodes['multi_atlases_scans_t1'] = run.create_node(name='multi_atlases_scans_t1', type='constant', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['multi_atlases_scans_t1'].out_ports['out_output'] = Port(node=nodes['multi_atlases_scans_t1'], name='out_output', description='')
    
    nodes['co_register_pd'] = run.create_node(name='co_register_pd', type='node', parent=nodes['step_co-registration'], no_in_ports=0, no_out_ports=0)

    
    nodes['co_register_pd'].in_ports['in_fixed_image'] = Port(node=nodes['co_register_pd'], name='in_fixed_image', description='')
    nodes['co_register_pd'].in_ports['in_moving_image'] = Port(node=nodes['co_register_pd'], name='in_moving_image', description='')
    nodes['co_register_pd'].in_ports['in_parameters'] = Port(node=nodes['co_register_pd'], name='in_parameters', description='')
    nodes['co_register_pd'].in_ports['in_fixed_mask'] = Port(node=nodes['co_register_pd'], name='in_fixed_mask', description='')
    nodes['co_register_pd'].in_ports['in_moving_mask'] = Port(node=nodes['co_register_pd'], name='in_moving_mask', description='')
    nodes['co_register_pd'].in_ports['in_initial_transform'] = Port(node=nodes['co_register_pd'], name='in_initial_transform', description='')
    nodes['co_register_pd'].in_ports['in_priority'] = Port(node=nodes['co_register_pd'], name='in_priority', description='')
    nodes['co_register_pd'].in_ports['in_threads'] = Port(node=nodes['co_register_pd'], name='in_threads', description='')

    
    nodes['co_register_pd'].out_ports['out_directory'] = Port(node=nodes['co_register_pd'], name='out_directory', description='')
    nodes['co_register_pd'].out_ports['out_transform'] = Port(node=nodes['co_register_pd'], name='out_transform', description='')
    nodes['co_register_pd'].out_ports['out_log_file'] = Port(node=nodes['co_register_pd'], name='out_log_file', description='')
    
    nodes['rama_flair'] = run.create_node(name='rama_flair', type='node', parent=nodes['step_rama'], no_in_ports=0, no_out_ports=0)

    
    nodes['rama_flair'].in_ports['in_image'] = Port(node=nodes['rama_flair'], name='in_image', description='')
    nodes['rama_flair'].in_ports['in_mask'] = Port(node=nodes['rama_flair'], name='in_mask', description='')
    nodes['rama_flair'].in_ports['in_upper_percentile'] = Port(node=nodes['rama_flair'], name='in_upper_percentile', description='')
    nodes['rama_flair'].in_ports['in_lower_percentile'] = Port(node=nodes['rama_flair'], name='in_lower_percentile', description='')

    
    nodes['rama_flair'].out_ports['out_image'] = Port(node=nodes['rama_flair'], name='out_image', description='')
    
    nodes['const_multi_atlas_combine_tissue_substitute_labels_0'] = run.create_node(name='const_multi_atlas_combine_tissue_substitute_labels_0', type='constant', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_multi_atlas_combine_tissue_substitute_labels_0'].out_ports['out_output'] = Port(node=nodes['const_multi_atlas_combine_tissue_substitute_labels_0'], name='out_output', description='')
    
    nodes['const_rama_flair_lower_percentile_0'] = run.create_node(name='const_rama_flair_lower_percentile_0', type='constant', parent=nodes['step_rama'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_rama_flair_lower_percentile_0'].out_ports['out_output'] = Port(node=nodes['const_rama_flair_lower_percentile_0'], name='out_output', description='')
    
    nodes['multi_atlases_lobes_finallabels'] = run.create_node(name='multi_atlases_lobes_finallabels', type='constant', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['multi_atlases_lobes_finallabels'].out_ports['out_output'] = Port(node=nodes['multi_atlases_lobes_finallabels'], name='out_output', description='')
    
    nodes['multi_atlas_combine_tissue'] = run.create_node(name='multi_atlas_combine_tissue', type='node', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    
    nodes['multi_atlas_combine_tissue'].in_ports['in_images'] = Port(node=nodes['multi_atlas_combine_tissue'], name='in_images', description='')
    nodes['multi_atlas_combine_tissue'].in_ports['in_method'] = Port(node=nodes['multi_atlas_combine_tissue'], name='in_method', description='')
    nodes['multi_atlas_combine_tissue'].in_ports['in_number_of_classes'] = Port(node=nodes['multi_atlas_combine_tissue'], name='in_number_of_classes', description='')
    nodes['multi_atlas_combine_tissue'].in_ports['in_original_labels'] = Port(node=nodes['multi_atlas_combine_tissue'], name='in_original_labels', description='')
    nodes['multi_atlas_combine_tissue'].in_ports['in_substitute_labels'] = Port(node=nodes['multi_atlas_combine_tissue'], name='in_substitute_labels', description='')

    
    nodes['multi_atlas_combine_tissue'].out_ports['out_hard_segment'] = Port(node=nodes['multi_atlas_combine_tissue'], name='out_hard_segment', description='')
    nodes['multi_atlas_combine_tissue'].out_ports['out_soft_segment'] = Port(node=nodes['multi_atlas_combine_tissue'], name='out_soft_segment', description='')
    
    nodes['const_rama_T1_upper_percentile_0'] = run.create_node(name='const_rama_T1_upper_percentile_0', type='constant', parent=nodes['step_rama'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_rama_T1_upper_percentile_0'].out_ports['out_output'] = Port(node=nodes['const_rama_T1_upper_percentile_0'], name='out_output', description='')
    
    nodes['const_sample_image_nsamples_0'] = run.create_node(name='const_sample_image_nsamples_0', type='constant', parent=nodes['step_classifier'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_sample_image_nsamples_0'].out_ports['out_output'] = Port(node=nodes['const_sample_image_nsamples_0'], name='out_output', description='')
    
    nodes['const_rama_PD_lower_percentile_0'] = run.create_node(name='const_rama_PD_lower_percentile_0', type='constant', parent=nodes['step_rama'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_rama_PD_lower_percentile_0'].out_ports['out_output'] = Port(node=nodes['const_rama_PD_lower_percentile_0'], name='out_output', description='')
    
    nodes['const_n4_flair_bspline_fitting_0'] = run.create_node(name='const_n4_flair_bspline_fitting_0', type='constant', parent=nodes['step_N4'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_n4_flair_bspline_fitting_0'].out_ports['out_output'] = Port(node=nodes['const_n4_flair_bspline_fitting_0'], name='out_output', description='')
    
    nodes['n4_T1'] = run.create_node(name='n4_T1', type='node', parent=nodes['step_N4'], no_in_ports=0, no_out_ports=0)

    
    nodes['n4_T1'].in_ports['in_number_of_dimensions'] = Port(node=nodes['n4_T1'], name='in_number_of_dimensions', description='')
    nodes['n4_T1'].in_ports['in_image'] = Port(node=nodes['n4_T1'], name='in_image', description='')
    nodes['n4_T1'].in_ports['in_mask'] = Port(node=nodes['n4_T1'], name='in_mask', description='')
    nodes['n4_T1'].in_ports['in_weight_image'] = Port(node=nodes['n4_T1'], name='in_weight_image', description='')
    nodes['n4_T1'].in_ports['in_shrink_factor'] = Port(node=nodes['n4_T1'], name='in_shrink_factor', description='')
    nodes['n4_T1'].in_ports['in_converge'] = Port(node=nodes['n4_T1'], name='in_converge', description='')
    nodes['n4_T1'].in_ports['in_bspline_fitting'] = Port(node=nodes['n4_T1'], name='in_bspline_fitting', description='')

    
    nodes['n4_T1'].out_ports['out_image'] = Port(node=nodes['n4_T1'], name='out_image', description='')
    
    nodes['const_train_classifier_classifier_0'] = run.create_node(name='const_train_classifier_classifier_0', type='constant', parent=nodes['step_classifier'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_train_classifier_classifier_0'].out_ports['out_output'] = Port(node=nodes['const_train_classifier_classifier_0'], name='out_output', description='')
    
    nodes['multi_atlas_combine_lobes'] = run.create_node(name='multi_atlas_combine_lobes', type='node', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    
    nodes['multi_atlas_combine_lobes'].in_ports['in_images'] = Port(node=nodes['multi_atlas_combine_lobes'], name='in_images', description='')
    nodes['multi_atlas_combine_lobes'].in_ports['in_method'] = Port(node=nodes['multi_atlas_combine_lobes'], name='in_method', description='')
    nodes['multi_atlas_combine_lobes'].in_ports['in_number_of_classes'] = Port(node=nodes['multi_atlas_combine_lobes'], name='in_number_of_classes', description='')
    nodes['multi_atlas_combine_lobes'].in_ports['in_original_labels'] = Port(node=nodes['multi_atlas_combine_lobes'], name='in_original_labels', description='')
    nodes['multi_atlas_combine_lobes'].in_ports['in_substitute_labels'] = Port(node=nodes['multi_atlas_combine_lobes'], name='in_substitute_labels', description='')

    
    nodes['multi_atlas_combine_lobes'].out_ports['out_hard_segment'] = Port(node=nodes['multi_atlas_combine_lobes'], name='out_hard_segment', description='')
    nodes['multi_atlas_combine_lobes'].out_ports['out_soft_segment'] = Port(node=nodes['multi_atlas_combine_lobes'], name='out_soft_segment', description='')
    
    nodes['const_multi_atlas_combine_lobes_method_0'] = run.create_node(name='const_multi_atlas_combine_lobes_method_0', type='constant', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_multi_atlas_combine_lobes_method_0'].out_ports['out_output'] = Port(node=nodes['const_multi_atlas_combine_lobes_method_0'], name='out_output', description='')
    
    nodes['remove_neck'] = run.create_node(name='remove_neck', type='node', parent=nodes['step_co-registration'], no_in_ports=0, no_out_ports=0)

    
    nodes['remove_neck'].in_ports['in_image'] = Port(node=nodes['remove_neck'], name='in_image', description='')
    nodes['remove_neck'].in_ports['in_mask_FOV'] = Port(node=nodes['remove_neck'], name='in_mask_FOV', description='')
    nodes['remove_neck'].in_ports['in_mask_NONE'] = Port(node=nodes['remove_neck'], name='in_mask_NONE', description='')
    nodes['remove_neck'].in_ports['in_roi_FOV'] = Port(node=nodes['remove_neck'], name='in_roi_FOV', description='')
    nodes['remove_neck'].in_ports['in_roi_NONE'] = Port(node=nodes['remove_neck'], name='in_roi_NONE', description='')

    
    nodes['remove_neck'].out_ports['out_image'] = Port(node=nodes['remove_neck'], name='out_image', description='')
    
    nodes['const_n4_flair_converge_0'] = run.create_node(name='const_n4_flair_converge_0', type='constant', parent=nodes['step_N4'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_n4_flair_converge_0'].out_ports['out_output'] = Port(node=nodes['const_n4_flair_converge_0'], name='out_output', description='')
    
    nodes['dcm2nii_flair'] = run.create_node(name='dcm2nii_flair', type='node', parent=nodes['step_convert'], no_in_ports=0, no_out_ports=0)

    
    nodes['dcm2nii_flair'].in_ports['in_dicom_image'] = Port(node=nodes['dcm2nii_flair'], name='in_dicom_image', description='')

    
    nodes['dcm2nii_flair'].out_ports['out_directory'] = Port(node=nodes['dcm2nii_flair'], name='out_directory', description='')
    nodes['dcm2nii_flair'].out_ports['out_image'] = Port(node=nodes['dcm2nii_flair'], name='out_image', description='')
    nodes['dcm2nii_flair'].out_ports['out_bval'] = Port(node=nodes['dcm2nii_flair'], name='out_bval', description='')
    nodes['dcm2nii_flair'].out_ports['out_bvec'] = Port(node=nodes['dcm2nii_flair'], name='out_bvec', description='')
    
    nodes['const_remove_neck_roi_NONE_0'] = run.create_node(name='const_remove_neck_roi_NONE_0', type='constant', parent=nodes['step_co-registration'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_remove_neck_roi_NONE_0'].out_ports['out_output'] = Port(node=nodes['const_remove_neck_roi_NONE_0'], name='out_output', description='')
    
    nodes['BrainMaskImprove'] = run.create_node(name='BrainMaskImprove', type='node', parent=nodes['step_wmlsegment'], no_in_ports=0, no_out_ports=0)

    
    nodes['BrainMaskImprove'].in_ports['in_segmentation'] = Port(node=nodes['BrainMaskImprove'], name='in_segmentation', description='')
    nodes['BrainMaskImprove'].in_ports['in_fraction'] = Port(node=nodes['BrainMaskImprove'], name='in_fraction', description='')

    
    nodes['BrainMaskImprove'].out_ports['out_segmentation'] = Port(node=nodes['BrainMaskImprove'], name='out_segmentation', description='')
    
    nodes['multi_atlas_trans_tissue_t1'] = run.create_node(name='multi_atlas_trans_tissue_t1', type='node', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    
    nodes['multi_atlas_trans_tissue_t1'].in_ports['in_transform'] = Port(node=nodes['multi_atlas_trans_tissue_t1'], name='in_transform', description='')
    nodes['multi_atlas_trans_tissue_t1'].in_ports['in_image'] = Port(node=nodes['multi_atlas_trans_tissue_t1'], name='in_image', description='')
    nodes['multi_atlas_trans_tissue_t1'].in_ports['in_points'] = Port(node=nodes['multi_atlas_trans_tissue_t1'], name='in_points', description='')
    nodes['multi_atlas_trans_tissue_t1'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['multi_atlas_trans_tissue_t1'], name='in_determinant_of_jacobian_flag', description='')
    nodes['multi_atlas_trans_tissue_t1'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['multi_atlas_trans_tissue_t1'], name='in_jacobian_matrix_flag', description='')
    nodes['multi_atlas_trans_tissue_t1'].in_ports['in_priority'] = Port(node=nodes['multi_atlas_trans_tissue_t1'], name='in_priority', description='')
    nodes['multi_atlas_trans_tissue_t1'].in_ports['in_threads'] = Port(node=nodes['multi_atlas_trans_tissue_t1'], name='in_threads', description='')

    
    nodes['multi_atlas_trans_tissue_t1'].out_ports['out_directory'] = Port(node=nodes['multi_atlas_trans_tissue_t1'], name='out_directory', description='')
    nodes['multi_atlas_trans_tissue_t1'].out_ports['out_image'] = Port(node=nodes['multi_atlas_trans_tissue_t1'], name='out_image', description='')
    nodes['multi_atlas_trans_tissue_t1'].out_ports['out_points'] = Port(node=nodes['multi_atlas_trans_tissue_t1'], name='out_points', description='')
    nodes['multi_atlas_trans_tissue_t1'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['multi_atlas_trans_tissue_t1'], name='out_determinant_of_jacobian', description='')
    nodes['multi_atlas_trans_tissue_t1'].out_ports['out_jacobian_matrix'] = Port(node=nodes['multi_atlas_trans_tissue_t1'], name='out_jacobian_matrix', description='')
    nodes['multi_atlas_trans_tissue_t1'].out_ports['out_log_file'] = Port(node=nodes['multi_atlas_trans_tissue_t1'], name='out_log_file', description='')
    
    nodes['elastix_mask'] = run.create_node(name='elastix_mask', type='node', parent=nodes['step_elastix_mask'], no_in_ports=0, no_out_ports=0)

    
    nodes['elastix_mask'].in_ports['in_fixed_image'] = Port(node=nodes['elastix_mask'], name='in_fixed_image', description='')
    nodes['elastix_mask'].in_ports['in_moving_image'] = Port(node=nodes['elastix_mask'], name='in_moving_image', description='')
    nodes['elastix_mask'].in_ports['in_parameters'] = Port(node=nodes['elastix_mask'], name='in_parameters', description='')
    nodes['elastix_mask'].in_ports['in_fixed_mask'] = Port(node=nodes['elastix_mask'], name='in_fixed_mask', description='')
    nodes['elastix_mask'].in_ports['in_moving_mask'] = Port(node=nodes['elastix_mask'], name='in_moving_mask', description='')
    nodes['elastix_mask'].in_ports['in_initial_transform'] = Port(node=nodes['elastix_mask'], name='in_initial_transform', description='')
    nodes['elastix_mask'].in_ports['in_priority'] = Port(node=nodes['elastix_mask'], name='in_priority', description='')
    nodes['elastix_mask'].in_ports['in_threads'] = Port(node=nodes['elastix_mask'], name='in_threads', description='')

    
    nodes['elastix_mask'].out_ports['out_directory'] = Port(node=nodes['elastix_mask'], name='out_directory', description='')
    nodes['elastix_mask'].out_ports['out_transform'] = Port(node=nodes['elastix_mask'], name='out_transform', description='')
    nodes['elastix_mask'].out_ports['out_log_file'] = Port(node=nodes['elastix_mask'], name='out_log_file', description='')
    
    nodes['train_classifier'] = run.create_node(name='train_classifier', type='node', parent=nodes['step_classifier'], no_in_ports=0, no_out_ports=0)

    
    nodes['train_classifier'].in_ports['in_classifier'] = Port(node=nodes['train_classifier'], name='in_classifier', description='')
    nodes['train_classifier'].in_ports['in_samples'] = Port(node=nodes['train_classifier'], name='in_samples', description='')
    nodes['train_classifier'].in_ports['in_parameters'] = Port(node=nodes['train_classifier'], name='in_parameters', description='')
    nodes['train_classifier'].in_ports['in_number_of_train_samples'] = Port(node=nodes['train_classifier'], name='in_number_of_train_samples', description='')
    nodes['train_classifier'].in_ports['in_number_of_folds'] = Port(node=nodes['train_classifier'], name='in_number_of_folds', description='')
    nodes['train_classifier'].in_ports['in_number_of_cv_samples'] = Port(node=nodes['train_classifier'], name='in_number_of_cv_samples', description='')

    
    nodes['train_classifier'].out_ports['out_classifier'] = Port(node=nodes['train_classifier'], name='out_classifier', description='')
    
    nodes['T1'] = run.create_node(name='T1', type='source', parent=nodes['step_sources'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['T1'].out_ports['out_output'] = Port(node=nodes['T1'], name='out_output', description='')
    
    nodes['transform_pd'] = run.create_node(name='transform_pd', type='node', parent=nodes['step_co-registration'], no_in_ports=0, no_out_ports=0)

    
    nodes['transform_pd'].in_ports['in_transform'] = Port(node=nodes['transform_pd'], name='in_transform', description='')
    nodes['transform_pd'].in_ports['in_image'] = Port(node=nodes['transform_pd'], name='in_image', description='')
    nodes['transform_pd'].in_ports['in_points'] = Port(node=nodes['transform_pd'], name='in_points', description='')
    nodes['transform_pd'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['transform_pd'], name='in_determinant_of_jacobian_flag', description='')
    nodes['transform_pd'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['transform_pd'], name='in_jacobian_matrix_flag', description='')
    nodes['transform_pd'].in_ports['in_priority'] = Port(node=nodes['transform_pd'], name='in_priority', description='')
    nodes['transform_pd'].in_ports['in_threads'] = Port(node=nodes['transform_pd'], name='in_threads', description='')

    
    nodes['transform_pd'].out_ports['out_directory'] = Port(node=nodes['transform_pd'], name='out_directory', description='')
    nodes['transform_pd'].out_ports['out_image'] = Port(node=nodes['transform_pd'], name='out_image', description='')
    nodes['transform_pd'].out_ports['out_points'] = Port(node=nodes['transform_pd'], name='out_points', description='')
    nodes['transform_pd'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['transform_pd'], name='out_determinant_of_jacobian', description='')
    nodes['transform_pd'].out_ports['out_jacobian_matrix'] = Port(node=nodes['transform_pd'], name='out_jacobian_matrix', description='')
    nodes['transform_pd'].out_ports['out_log_file'] = Port(node=nodes['transform_pd'], name='out_log_file', description='')
    
    nodes['apply_classifier'] = run.create_node(name='apply_classifier', type='node', parent=nodes['step_classifier'], no_in_ports=0, no_out_ports=0)

    
    nodes['apply_classifier'].in_ports['in_image'] = Port(node=nodes['apply_classifier'], name='in_image', description='')
    nodes['apply_classifier'].in_ports['in_mask'] = Port(node=nodes['apply_classifier'], name='in_mask', description='')
    nodes['apply_classifier'].in_ports['in_classifier'] = Port(node=nodes['apply_classifier'], name='in_classifier', description='')
    nodes['apply_classifier'].in_ports['in_number_of_classes'] = Port(node=nodes['apply_classifier'], name='in_number_of_classes', description='')
    nodes['apply_classifier'].in_ports['in_number_of_cores'] = Port(node=nodes['apply_classifier'], name='in_number_of_cores', description='')

    
    nodes['apply_classifier'].out_ports['out_label_image'] = Port(node=nodes['apply_classifier'], name='out_label_image', description='')
    nodes['apply_classifier'].out_ports['out_probability_image'] = Port(node=nodes['apply_classifier'], name='out_probability_image', description='')
    
    nodes['const_multi_atlas_combine_tissue_number_of_classes_0'] = run.create_node(name='const_multi_atlas_combine_tissue_number_of_classes_0', type='constant', parent=nodes['step_multi_atlas_reg'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_multi_atlas_combine_tissue_number_of_classes_0'].out_ports['out_output'] = Port(node=nodes['const_multi_atlas_combine_tissue_number_of_classes_0'], name='out_output', description='')
    
    nodes['const_n4_pd_shrink_factor_0'] = run.create_node(name='const_n4_pd_shrink_factor_0', type='constant', parent=nodes['step_N4'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_n4_pd_shrink_factor_0'].out_ports['out_output'] = Port(node=nodes['const_n4_pd_shrink_factor_0'], name='out_output', description='')
    
    nodes['const_registration_parameters'] = run.create_node(name='const_registration_parameters', type='constant', parent=nodes['root'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['const_registration_parameters'].out_ports['out_output'] = Port(node=nodes['const_registration_parameters'], name='out_output', description='')
    

    # Links
    run.links.append(Link(from_node=nodes['rama_PD'], to_node=nodes['apply_classifier'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['rama_flair'], to_node=nodes['apply_classifier'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['multi_atlas_combine_tissue'], to_node=nodes['sample_image'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['threshold_prob'], to_node=nodes['sample_image'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_sample_image_nsamples_0'], to_node=nodes['sample_image'], data_type='Float'))
    run.links.append(Link(from_node=nodes['const_sample_image_seed_int_0'], to_node=nodes['sample_image'], data_type='Int'))
    run.links.append(Link(from_node=nodes['sample_image'], to_node=nodes['train_classifier'], data_type='NumpyArraysFile'))
    run.links.append(Link(from_node=nodes['const_train_classifier_classifier_0'], to_node=nodes['train_classifier'], data_type='__TrainClassifier_1.0_interface__classifier__Enum__'))
    run.links.append(Link(from_node=nodes['const_train_classifier_parameters_0'], to_node=nodes['train_classifier'], data_type='KeyValueFile'))
    run.links.append(Link(from_node=nodes['rama_T1'], to_node=nodes['apply_classifier'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['multi_atlases_masks'], to_node=nodes['multi_atlas_trans_masks_t1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_registration_parameters'], to_node=nodes['multi_atlas_reg_t1'], data_type='ElastixParameterFile'))
    run.links.append(Link(from_node=nodes['const_n4_flair_bspline_fitting_0'], to_node=nodes['n4_flair'], data_type='String'))
    run.links.append(Link(from_node=nodes['const_n4_flair_converge_0'], to_node=nodes['n4_flair'], data_type='String'))
    run.links.append(Link(from_node=nodes['multi_atlases_scans_t1'], to_node=nodes['multi_atlas_reg_t1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['n4_T1'], to_node=nodes['multi_atlas_reg_t1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['transform_flair'], to_node=nodes['n4_flair'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_n4_pd_bspline_fitting_0'], to_node=nodes['n4_pd'], data_type='String'))
    run.links.append(Link(from_node=nodes['const_n4_flair_shrink_factor_0'], to_node=nodes['n4_flair'], data_type='UnsignedInt'))
    run.links.append(Link(from_node=nodes['transformix_mask'], to_node=nodes['n4_flair'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['apply_classifier'], to_node=nodes['sink_knnseg'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['multi_atlas_combine_masks'], to_node=nodes['sink_mask'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['multi_atlas_combine_lobes_intensity'], to_node=nodes['sink_lobes'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['BrainMaskImprove'], to_node=nodes['sink_knnwml'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_BrainMaskImprove_fraction_0'], to_node=nodes['BrainMaskImprove'], data_type='Float'))
    run.links.append(Link(from_node=nodes['n4_T1'], to_node=nodes['sink_t1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['n4_pd'], to_node=nodes['sink_pd'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['n4_flair'], to_node=nodes['sink_flair'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_n4_pd_shrink_factor_0'], to_node=nodes['n4_pd'], data_type='UnsignedInt'))
    run.links.append(Link(from_node=nodes['const_n4_pd_converge_0'], to_node=nodes['n4_pd'], data_type='String'))
    run.links.append(Link(from_node=nodes['transformix_mask'], to_node=nodes['n4_T1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_n4_T1_shrink_factor_0'], to_node=nodes['n4_T1'], data_type='UnsignedInt'))
    run.links.append(Link(from_node=nodes['elastix_mask'], to_node=nodes['transformix_mask'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['dcm2nii_t1'], to_node=nodes['n4_T1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['transform_pd'], to_node=nodes['n4_pd'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['transformix_mask'], to_node=nodes['n4_pd'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_n4_T1_converge_0'], to_node=nodes['n4_T1'], data_type='String'))
    run.links.append(Link(from_node=nodes['const_n4_T1_bspline_fitting_0'], to_node=nodes['n4_T1'], data_type='String'))
    run.links.append(Link(from_node=nodes['const_multi_atlas_combine_tissue_original_labels_0'], to_node=nodes['multi_atlas_combine_tissue'], data_type='Int'))
    run.links.append(Link(from_node=nodes['const_multi_atlas_combine_tissue_number_of_classes_0'], to_node=nodes['multi_atlas_combine_tissue'], data_type='Int'))
    run.links.append(Link(from_node=nodes['const_multi_atlas_combine_tissue_method_0'], to_node=nodes['multi_atlas_combine_tissue'], data_type='__PxCombineSegmentations_0.3.0_interface__method__Enum__'))
    run.links.append(Link(from_node=nodes['multi_atlas_trans_tissue_t1'], to_node=nodes['multi_atlas_combine_tissue'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['multi_atlases_lobes_finallabels'], to_node=nodes['multi_atlas_combine_lobes_intensity'], data_type='Int'))
    run.links.append(Link(from_node=nodes['multi_atlases_lobes_templabels'], to_node=nodes['multi_atlas_combine_lobes_intensity'], data_type='Int'))
    run.links.append(Link(from_node=nodes['multi_atlases_lobes_templabels'], to_node=nodes['multi_atlas_combine_lobes'], data_type='Int'))
    run.links.append(Link(from_node=nodes['multi_atlases_lobes_finallabels'], to_node=nodes['multi_atlas_combine_lobes'], data_type='Int'))
    run.links.append(Link(from_node=nodes['multi_atlas_combine_lobes'], to_node=nodes['multi_atlas_combine_lobes_intensity'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_multi_atlas_combine_lobes_number_of_classes_0'], to_node=nodes['multi_atlas_combine_lobes'], data_type='Int'))
    run.links.append(Link(from_node=nodes['remove_neck'], to_node=nodes['co_register_flair'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_rigid_paramter'], to_node=nodes['co_register_pd'], data_type='ElastixParameterFile'))
    run.links.append(Link(from_node=nodes['dcm2nii_t1'], to_node=nodes['remove_neck'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['FLAIR'], to_node=nodes['dcm2nii_flair'], data_type='DicomImageFile'))
    run.links.append(Link(from_node=nodes['PD'], to_node=nodes['dcm2nii_pd'], data_type='DicomImageFile'))
    run.links.append(Link(from_node=nodes['T1'], to_node=nodes['dcm2nii_t1'], data_type='DicomImageFile'))
    run.links.append(Link(from_node=nodes['dcm2nii_pd'], to_node=nodes['co_register_pd'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['remove_neck'], to_node=nodes['co_register_pd'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_remove_neck_roi_NONE_0'], to_node=nodes['remove_neck'], data_type='Boolean'))
    run.links.append(Link(from_node=nodes['const_remove_neck_mask_FOV_0'], to_node=nodes['remove_neck'], data_type='Boolean'))
    run.links.append(Link(from_node=nodes['multi_atlas_trans_lobes_t1'], to_node=nodes['multi_atlas_combine_lobes'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_multi_atlas_combine_lobes_method_0'], to_node=nodes['multi_atlas_combine_lobes'], data_type='__PxCombineSegmentations_0.3.0_interface__method__Enum__'))
    run.links.append(Link(from_node=nodes['multi_atlas_reg_t1'], to_node=nodes['multi_atlas_trans_tissue_t1'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['multi_atlas_trans_masks_t1'], to_node=nodes['multi_atlas_combine_masks'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_multi_atlas_combine_masks_method_0'], to_node=nodes['multi_atlas_combine_masks'], data_type='__PxCombineSegmentations_0.3.0_interface__method__Enum__'))
    run.links.append(Link(from_node=nodes['const_multi_atlas_combine_masks_number_of_classes_0'], to_node=nodes['multi_atlas_combine_masks'], data_type='Int'))
    run.links.append(Link(from_node=nodes['multi_atlas_reg_t1'], to_node=nodes['multi_atlas_trans_masks_t1'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['multi_atlases_lobes'], to_node=nodes['multi_atlas_trans_lobes_t1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['multi_atlas_reg_t1'], to_node=nodes['multi_atlas_trans_lobes_t1'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['multi_atlases_tissue'], to_node=nodes['multi_atlas_trans_tissue_t1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['rama_flair'], to_node=nodes['sample_image'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['rama_PD'], to_node=nodes['sample_image'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_rama_flair_upper_percentile_0'], to_node=nodes['rama_flair'], data_type='Float'))
    run.links.append(Link(from_node=nodes['multi_atlas_combine_masks'], to_node=nodes['rama_flair'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['multi_atlas_combine_tissue'], to_node=nodes['combine_prob_masks'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_rama_flair_lower_percentile_0'], to_node=nodes['rama_flair'], data_type='Float'))
    run.links.append(Link(from_node=nodes['combine_prob_masks'], to_node=nodes['threshold_prob'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_combine_prob_masks_operator_0'], to_node=nodes['combine_prob_masks'], data_type='__PxNaryImageOperator_0.3.2_interface__operator__Enum__'))
    run.links.append(Link(from_node=nodes['rama_T1'], to_node=nodes['sample_image'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_threshold_prob_threshold_0'], to_node=nodes['threshold_prob'], data_type='Float'))
    run.links.append(Link(from_node=nodes['multi_atlas_combine_masks'], to_node=nodes['rama_PD'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_rama_PD_upper_percentile_0'], to_node=nodes['rama_PD'], data_type='Float'))
    run.links.append(Link(from_node=nodes['const_rama_T1_lower_percentile_0'], to_node=nodes['rama_T1'], data_type='Float'))
    run.links.append(Link(from_node=nodes['n4_pd'], to_node=nodes['rama_PD'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['multi_atlas_combine_masks'], to_node=nodes['rama_T1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_rama_T1_upper_percentile_0'], to_node=nodes['rama_T1'], data_type='Float'))
    run.links.append(Link(from_node=nodes['const_multi_atlas_combine_tissue_substitute_labels_0'], to_node=nodes['multi_atlas_combine_tissue'], data_type='Int'))
    run.links.append(Link(from_node=nodes['n4_T1'], to_node=nodes['rama_T1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_rama_PD_lower_percentile_0'], to_node=nodes['rama_PD'], data_type='Float'))
    run.links.append(Link(from_node=nodes['n4_flair'], to_node=nodes['rama_flair'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['dcm2nii_pd'], to_node=nodes['transform_pd'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['co_register_pd'], to_node=nodes['transform_pd'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['const_rigid_paramter'], to_node=nodes['co_register_flair'], data_type='ElastixParameterFile'))
    run.links.append(Link(from_node=nodes['dcm2nii_flair'], to_node=nodes['co_register_flair'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_elastix_mask_moving_image_0'], to_node=nodes['elastix_mask'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['remove_neck'], to_node=nodes['elastix_mask'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['dcm2nii_flair'], to_node=nodes['transform_flair'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['co_register_flair'], to_node=nodes['transform_flair'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['const_transformix_mask_image_0'], to_node=nodes['transformix_mask'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_registration_parameters'], to_node=nodes['elastix_mask'], data_type='ElastixParameterFile'))
    run.links.append(Link(from_node=nodes['wmlsegment'], to_node=nodes['BrainMaskImprove'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['const_wmlsegment_beta_0'], to_node=nodes['wmlsegment'], data_type='Float'))
    run.links.append(Link(from_node=nodes['const_apply_classifier_number_of_cores_0'], to_node=nodes['apply_classifier'], data_type='Int'))
    run.links.append(Link(from_node=nodes['const_apply_classifier_number_of_classes_0'], to_node=nodes['apply_classifier'], data_type='Int'))
    run.links.append(Link(from_node=nodes['multi_atlas_combine_masks'], to_node=nodes['apply_classifier'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['train_classifier'], to_node=nodes['apply_classifier'], data_type='SKLearnClassifierFile'))
    run.links.append(Link(from_node=nodes['const_wmlsegment_alpha_0'], to_node=nodes['wmlsegment'], data_type='Float'))
    run.links.append(Link(from_node=nodes['multi_atlas_combine_masks'], to_node=nodes['wmlsegment'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['rama_flair'], to_node=nodes['wmlsegment'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['apply_classifier'], to_node=nodes['wmlsegment'], data_type='NiftiImageFileCompressed'))

    return run

