import os

from adapter.link import Link
from adapter.run import Run
from adapter.port import Port


def create_run(**kwargs):
    run = Run()
    run.name = 'basic'

    os.environ['RUN_NAME'] = run.name

    nodes = dict()

    nodes['root'] = run.create_node(name='root', parent=None)

    run.root = nodes['root']

    nodes['node_a'] = run.create_node(name='node_a', description='node_a', parent=nodes['root'])
    nodes['group'] = run.create_node(name='group', description='group', parent=nodes['root'])
    nodes['node_b'] = run.create_node(name='node_b', description='node_b', parent=nodes['group'])

    # Ports
    nodes['node_a'].out_ports['out'] = Port(node=nodes['node_a'], name='out', description='')
    nodes['node_b'].out_ports['in'] = Port(node=nodes['node_b'], name='in', description='')

    # Links
    run.links.append(Link(from_node=nodes['node_a'], to_node=nodes['node_b'], data_type='undefined'))

    return run