import os

from adapter.link import Link
from adapter.run import Run
from adapter.port import Port


def create_run(**kwargs):
    run = Run()
    run.name = 'average_brain'

    os.environ['RUN_NAME'] = run.name

    nodes = dict()

    nodes['root'] = run.create_node(name='root', parent=None)

    run.root = nodes['root']

    
    nodes['0_Files_Conversions5'] = run.create_node(name='0_Files_Conversions5', description='Leaf', parent=nodes['root'])
    nodes['1_ICV_Model_Name'] = run.create_node(name='1_ICV_Model_Name', description='Leaf', parent=nodes['root'])
    nodes['2_Main_Calculation'] = run.create_node(name='2_Main_Calculation', description='Leaf', parent=nodes['root'])
    nodes['3_Files_Conversions4'] = run.create_node(name='3_Files_Conversions4', description='Leaf', parent=nodes['root'])
    nodes['4_Input_Arguments22'] = run.create_node(name='4_Input_Arguments22', description='Leaf', parent=nodes['root'])
    nodes['5_Input_Arguments23'] = run.create_node(name='5_Input_Arguments23', description='Leaf', parent=nodes['root'])
    nodes['6_Input_Arguments20'] = run.create_node(name='6_Input_Arguments20', description='Leaf', parent=nodes['root'])
    nodes['7_Program_Extended_Arguments'] = run.create_node(name='7_Program_Extended_Arguments', description='Leaf', parent=nodes['root'])
    nodes['8_Input_Arguments26'] = run.create_node(name='8_Input_Arguments26', description='Leaf', parent=nodes['root'])
    nodes['9_Input_Arguments27'] = run.create_node(name='9_Input_Arguments27', description='Leaf', parent=nodes['root'])
    nodes['10_Input_Arguments24'] = run.create_node(name='10_Input_Arguments24', description='Leaf', parent=nodes['root'])
    nodes['11_Input_Arguments25'] = run.create_node(name='11_Input_Arguments25', description='Leaf', parent=nodes['root'])
    nodes['12_ABM_Files_Outputs'] = run.create_node(name='12_ABM_Files_Outputs', description='Leaf', parent=nodes['root'])
    nodes['13_Input_Arguments28'] = run.create_node(name='13_Input_Arguments28', description='Leaf', parent=nodes['root'])
    nodes['14_Input_Arguments29'] = run.create_node(name='14_Input_Arguments29', description='Leaf', parent=nodes['root'])
    nodes['15_Files_Conversions28'] = run.create_node(name='15_Files_Conversions28', description='Leaf', parent=nodes['root'])
    nodes['16_Files_Conversions29'] = run.create_node(name='16_Files_Conversions29', description='Leaf', parent=nodes['root'])
    nodes['17_Collect_Converted_Files'] = run.create_node(name='17_Collect_Converted_Files', description='Leaf', parent=nodes['root'])
    nodes['18_Files_Conversions22'] = run.create_node(name='18_Files_Conversions22', description='Leaf', parent=nodes['root'])
    nodes['19_Program_Basic_Arguments'] = run.create_node(name='19_Program_Basic_Arguments', description='Leaf', parent=nodes['root'])
    nodes['20_ABM_Program_Extended_Arguments'] = run.create_node(name='20_ABM_Program_Extended_Arguments', description='Leaf', parent=nodes['root'])
    nodes['21_Files_Conversions21'] = run.create_node(name='21_Files_Conversions21', description='Leaf', parent=nodes['root'])
    nodes['22_Files_Conversions26'] = run.create_node(name='22_Files_Conversions26', description='Leaf', parent=nodes['root'])
    nodes['23_Files_Conversions27'] = run.create_node(name='23_Files_Conversions27', description='Leaf', parent=nodes['root'])
    nodes['24_Files_Conversions24'] = run.create_node(name='24_Files_Conversions24', description='Leaf', parent=nodes['root'])
    nodes['25_Files_Conversions25'] = run.create_node(name='25_Files_Conversions25', description='Leaf', parent=nodes['root'])
    nodes['26_ABM_Program_Basic_Arguments'] = run.create_node(name='26_ABM_Program_Basic_Arguments', description='Leaf', parent=nodes['root'])
    nodes['27_ABM_Operation_Type'] = run.create_node(name='27_ABM_Operation_Type', description='Leaf', parent=nodes['root'])
    nodes['28_ABM_First_Subject'] = run.create_node(name='28_ABM_First_Subject', description='Leaf', parent=nodes['root'])
    nodes['29_Input_Arguments3'] = run.create_node(name='29_Input_Arguments3', description='Leaf', parent=nodes['root'])
    nodes['30_Input_Arguments2'] = run.create_node(name='30_Input_Arguments2', description='Leaf', parent=nodes['root'])
    nodes['31_Input_Arguments1'] = run.create_node(name='31_Input_Arguments1', description='Leaf', parent=nodes['root'])
    nodes['32_Input_Arguments7'] = run.create_node(name='32_Input_Arguments7', description='Leaf', parent=nodes['root'])
    nodes['33_Input_Arguments6'] = run.create_node(name='33_Input_Arguments6', description='Leaf', parent=nodes['root'])
    nodes['34_Input_Arguments5'] = run.create_node(name='34_Input_Arguments5', description='Leaf', parent=nodes['root'])
    nodes['35_Input_Arguments4'] = run.create_node(name='35_Input_Arguments4', description='Leaf', parent=nodes['root'])
    nodes['36_Input_Arguments9'] = run.create_node(name='36_Input_Arguments9', description='Leaf', parent=nodes['root'])
    nodes['37_Input_Arguments8'] = run.create_node(name='37_Input_Arguments8', description='Leaf', parent=nodes['root'])
    nodes['38_Program_Linear_Registration_Arguments'] = run.create_node(name='38_Program_Linear_Registration_Arguments', description='Leaf', parent=nodes['root'])
    nodes['39_Mask_Calculation'] = run.create_node(name='39_Mask_Calculation', description='Leaf', parent=nodes['root'])
    nodes['40_Files_Conversions3'] = run.create_node(name='40_Files_Conversions3', description='Leaf', parent=nodes['root'])
    nodes['41_Files_Conversions2'] = run.create_node(name='41_Files_Conversions2', description='Leaf', parent=nodes['root'])
    nodes['42_Files_Conversions1'] = run.create_node(name='42_Files_Conversions1', description='Leaf', parent=nodes['root'])
    nodes['43_Input_Arguments14'] = run.create_node(name='43_Input_Arguments14', description='Leaf', parent=nodes['root'])
    nodes['44_ICV_Files_Outputs'] = run.create_node(name='44_ICV_Files_Outputs', description='Leaf', parent=nodes['root'])
    nodes['45_Files_Conversions6'] = run.create_node(name='45_Files_Conversions6', description='Leaf', parent=nodes['root'])
    nodes['46_Input_Arguments11'] = run.create_node(name='46_Input_Arguments11', description='Leaf', parent=nodes['root'])
    nodes['47_Input_Arguments10'] = run.create_node(name='47_Input_Arguments10', description='Leaf', parent=nodes['root'])
    nodes['48_ABM_Subject_Name'] = run.create_node(name='48_ABM_Subject_Name', description='Leaf', parent=nodes['root'])
    nodes['49_Files_Conversions9'] = run.create_node(name='49_Files_Conversions9', description='Leaf', parent=nodes['root'])
    nodes['50_Files_Conversions8'] = run.create_node(name='50_Files_Conversions8', description='Leaf', parent=nodes['root'])
    nodes['51_Files_Conversions7'] = run.create_node(name='51_Files_Conversions7', description='Leaf', parent=nodes['root'])
    nodes['52_Input_Arguments19'] = run.create_node(name='52_Input_Arguments19', description='Leaf', parent=nodes['root'])
    nodes['53_Input_Arguments18'] = run.create_node(name='53_Input_Arguments18', description='Leaf', parent=nodes['root'])
    nodes['54_Files_Conversions13'] = run.create_node(name='54_Files_Conversions13', description='Leaf', parent=nodes['root'])
    nodes['55_Input_Arguments17'] = run.create_node(name='55_Input_Arguments17', description='Leaf', parent=nodes['root'])
    nodes['56_Files_Conversions11'] = run.create_node(name='56_Files_Conversions11', description='Leaf', parent=nodes['root'])
    nodes['57_Files_Conversions10'] = run.create_node(name='57_Files_Conversions10', description='Leaf', parent=nodes['root'])
    nodes['58_Files_Conversions17'] = run.create_node(name='58_Files_Conversions17', description='Leaf', parent=nodes['root'])
    nodes['59_Files_Conversions16'] = run.create_node(name='59_Files_Conversions16', description='Leaf', parent=nodes['root'])
    nodes['60_Files_Conversions15'] = run.create_node(name='60_Files_Conversions15', description='Leaf', parent=nodes['root'])
    nodes['61_Files_Conversions14'] = run.create_node(name='61_Files_Conversions14', description='Leaf', parent=nodes['root'])
    nodes['62_AMB_Input_Folder'] = run.create_node(name='62_AMB_Input_Folder', description='Leaf', parent=nodes['root'])
    nodes['63_Files_Conversions19'] = run.create_node(name='63_Files_Conversions19', description='Leaf', parent=nodes['root'])
    nodes['64_Files_Conversions18'] = run.create_node(name='64_Files_Conversions18', description='Leaf', parent=nodes['root'])
    nodes['65_Input_Arguments15'] = run.create_node(name='65_Input_Arguments15', description='Leaf', parent=nodes['root'])
    nodes['66_Input_Arguments12'] = run.create_node(name='66_Input_Arguments12', description='Leaf', parent=nodes['root'])
    nodes['67_Input_Arguments21'] = run.create_node(name='67_Input_Arguments21', description='Leaf', parent=nodes['root'])
    nodes['68_Input_Arguments16'] = run.create_node(name='68_Input_Arguments16', description='Leaf', parent=nodes['root'])
    nodes['69_Files_Conversions23'] = run.create_node(name='69_Files_Conversions23', description='Leaf', parent=nodes['root'])
    nodes['70_Operation_Type'] = run.create_node(name='70_Operation_Type', description='Leaf', parent=nodes['root'])
    nodes['71_Input_Arguments13'] = run.create_node(name='71_Input_Arguments13', description='Leaf', parent=nodes['root'])
    nodes['72_ABM_Program_Linear_Registration_Arguments'] = run.create_node(name='72_ABM_Program_Linear_Registration_Arguments', description='Leaf', parent=nodes['root'])
    nodes['73_Files_Conversions20'] = run.create_node(name='73_Files_Conversions20', description='Leaf', parent=nodes['root'])
    nodes['74_Files_Conversions12'] = run.create_node(name='74_Files_Conversions12', description='Leaf', parent=nodes['root'])

    
    nodes['75_ICV_Num_Of_Registrations'] = run.create_node(name='75_ICV_Num_Of_Registrations', type='source', parent=nodes['38_Program_Linear_Registration_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['75_ICV_Num_Of_Registrations'].out_ports['out_output'] = Port(node=nodes['75_ICV_Num_Of_Registrations'], name='out_output', description='')
    
    nodes['76_ABM_Output_JPG'] = run.create_node(name='76_ABM_Output_JPG', type='sink', parent=nodes['12_ABM_Files_Outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['76_ABM_Output_JPG'].in_ports['in_input'] = Port(node=nodes['76_ABM_Output_JPG'], name='in_input', description='')

    
    
    nodes['77_ABM_Step_Size'] = run.create_node(name='77_ABM_Step_Size', type='source', parent=nodes['26_ABM_Program_Basic_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['77_ABM_Step_Size'].out_ports['out_output'] = Port(node=nodes['77_ABM_Step_Size'], name='out_output', description='')
    
    nodes['78_Source_7'] = run.create_node(name='78_Source_7', type='source', parent=nodes['32_Input_Arguments7'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['78_Source_7'].out_ports['out_output'] = Port(node=nodes['78_Source_7'], name='out_output', description='')
    
    nodes['79_Source_4'] = run.create_node(name='79_Source_4', type='source', parent=nodes['35_Input_Arguments4'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['79_Source_4'].out_ports['out_output'] = Port(node=nodes['79_Source_4'], name='out_output', description='')
    
    nodes['80_Source_5'] = run.create_node(name='80_Source_5', type='source', parent=nodes['34_Input_Arguments5'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['80_Source_5'].out_ports['out_output'] = Port(node=nodes['80_Source_5'], name='out_output', description='')
    
    nodes['81_Source_2'] = run.create_node(name='81_Source_2', type='source', parent=nodes['30_Input_Arguments2'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['81_Source_2'].out_ports['out_output'] = Port(node=nodes['81_Source_2'], name='out_output', description='')
    
    nodes['82_Source_3'] = run.create_node(name='82_Source_3', type='source', parent=nodes['29_Input_Arguments3'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['82_Source_3'].out_ports['out_output'] = Port(node=nodes['82_Source_3'], name='out_output', description='')
    
    nodes['83_Source_1'] = run.create_node(name='83_Source_1', type='source', parent=nodes['31_Input_Arguments1'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['83_Source_1'].out_ports['out_output'] = Port(node=nodes['83_Source_1'], name='out_output', description='')
    
    nodes['84_ABM_Name'] = run.create_node(name='84_ABM_Name', type='source', parent=nodes['48_ABM_Subject_Name'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['84_ABM_Name'].out_ports['out_output'] = Port(node=nodes['84_ABM_Name'], name='out_output', description='')
    
    nodes['85_Source_8'] = run.create_node(name='85_Source_8', type='source', parent=nodes['37_Input_Arguments8'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['85_Source_8'].out_ports['out_output'] = Port(node=nodes['85_Source_8'], name='out_output', description='')
    
    nodes['86_Source_9'] = run.create_node(name='86_Source_9', type='source', parent=nodes['36_Input_Arguments9'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['86_Source_9'].out_ports['out_output'] = Port(node=nodes['86_Source_9'], name='out_output', description='')
    
    nodes['87_Source_14'] = run.create_node(name='87_Source_14', type='source', parent=nodes['43_Input_Arguments14'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['87_Source_14'].out_ports['out_output'] = Port(node=nodes['87_Source_14'], name='out_output', description='')
    
    nodes['88_Source_15'] = run.create_node(name='88_Source_15', type='source', parent=nodes['65_Input_Arguments15'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['88_Source_15'].out_ports['out_output'] = Port(node=nodes['88_Source_15'], name='out_output', description='')
    
    nodes['89_Source_16'] = run.create_node(name='89_Source_16', type='source', parent=nodes['68_Input_Arguments16'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['89_Source_16'].out_ports['out_output'] = Port(node=nodes['89_Source_16'], name='out_output', description='')
    
    nodes['90_Source_17'] = run.create_node(name='90_Source_17', type='source', parent=nodes['55_Input_Arguments17'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['90_Source_17'].out_ports['out_output'] = Port(node=nodes['90_Source_17'], name='out_output', description='')
    
    nodes['91_Source_10'] = run.create_node(name='91_Source_10', type='source', parent=nodes['47_Input_Arguments10'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['91_Source_10'].out_ports['out_output'] = Port(node=nodes['91_Source_10'], name='out_output', description='')
    
    nodes['92_Source_11'] = run.create_node(name='92_Source_11', type='source', parent=nodes['46_Input_Arguments11'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['92_Source_11'].out_ports['out_output'] = Port(node=nodes['92_Source_11'], name='out_output', description='')
    
    nodes['93_Source_12'] = run.create_node(name='93_Source_12', type='source', parent=nodes['66_Input_Arguments12'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['93_Source_12'].out_ports['out_output'] = Port(node=nodes['93_Source_12'], name='out_output', description='')
    
    nodes['94_Source_13'] = run.create_node(name='94_Source_13', type='source', parent=nodes['71_Input_Arguments13'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['94_Source_13'].out_ports['out_output'] = Port(node=nodes['94_Source_13'], name='out_output', description='')
    
    nodes['95_Calc_Crude'] = run.create_node(name='95_Calc_Crude', type='source', parent=nodes['70_Operation_Type'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['95_Calc_Crude'].out_ports['out_output'] = Port(node=nodes['95_Calc_Crude'], name='out_output', description='')
    
    nodes['96_Source_18'] = run.create_node(name='96_Source_18', type='source', parent=nodes['53_Input_Arguments18'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['96_Source_18'].out_ports['out_output'] = Port(node=nodes['96_Source_18'], name='out_output', description='')
    
    nodes['97_Source_19'] = run.create_node(name='97_Source_19', type='source', parent=nodes['52_Input_Arguments19'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['97_Source_19'].out_ports['out_output'] = Port(node=nodes['97_Source_19'], name='out_output', description='')
    
    nodes['98_ICV_Debug'] = run.create_node(name='98_ICV_Debug', type='source', parent=nodes['7_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['98_ICV_Debug'].out_ports['out_output'] = Port(node=nodes['98_ICV_Debug'], name='out_output', description='')
    
    nodes['99_mnc2mnc11'] = run.create_node(name='99_mnc2mnc11', type='node', parent=nodes['56_Files_Conversions11'], no_in_ports=0, no_out_ports=0)

    
    nodes['99_mnc2mnc11'].in_ports['in_Processed_Minc'] = Port(node=nodes['99_mnc2mnc11'], name='in_Processed_Minc', description='')
    nodes['99_mnc2mnc11'].in_ports['in_Output_Dir'] = Port(node=nodes['99_mnc2mnc11'], name='in_Output_Dir', description='')

    
    nodes['99_mnc2mnc11'].out_ports['out_output_file_mnc'] = Port(node=nodes['99_mnc2mnc11'], name='out_output_file_mnc', description='')
    
    nodes['100_ICV_Mask_To_Binary_Min'] = run.create_node(name='100_ICV_Mask_To_Binary_Min', type='source', parent=nodes['7_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['100_ICV_Mask_To_Binary_Min'].out_ports['out_output'] = Port(node=nodes['100_ICV_Mask_To_Binary_Min'], name='out_output', description='')
    
    nodes['101_ICV_Linear_Estimate'] = run.create_node(name='101_ICV_Linear_Estimate', type='source', parent=nodes['38_Program_Linear_Registration_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['101_ICV_Linear_Estimate'].out_ports['out_output'] = Port(node=nodes['101_ICV_Linear_Estimate'], name='out_output', description='')
    
    nodes['102_Source_Name_19'] = run.create_node(name='102_Source_Name_19', type='source', parent=nodes['52_Input_Arguments19'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['102_Source_Name_19'].out_ports['out_output'] = Port(node=nodes['102_Source_Name_19'], name='out_output', description='')
    
    nodes['103_Source_Name_18'] = run.create_node(name='103_Source_Name_18', type='source', parent=nodes['53_Input_Arguments18'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['103_Source_Name_18'].out_ports['out_output'] = Port(node=nodes['103_Source_Name_18'], name='out_output', description='')
    
    nodes['104_ICV_Model_Name'] = run.create_node(name='104_ICV_Model_Name', type='source', parent=nodes['1_ICV_Model_Name'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['104_ICV_Model_Name'].out_ports['out_output'] = Port(node=nodes['104_ICV_Model_Name'], name='out_output', description='')
    
    nodes['105_Source_Name_12'] = run.create_node(name='105_Source_Name_12', type='source', parent=nodes['66_Input_Arguments12'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['105_Source_Name_12'].out_ports['out_output'] = Port(node=nodes['105_Source_Name_12'], name='out_output', description='')
    
    nodes['106_Source_Name_11'] = run.create_node(name='106_Source_Name_11', type='source', parent=nodes['46_Input_Arguments11'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['106_Source_Name_11'].out_ports['out_output'] = Port(node=nodes['106_Source_Name_11'], name='out_output', description='')
    
    nodes['107_Source_Name_10'] = run.create_node(name='107_Source_Name_10', type='source', parent=nodes['47_Input_Arguments10'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['107_Source_Name_10'].out_ports['out_output'] = Port(node=nodes['107_Source_Name_10'], name='out_output', description='')
    
    nodes['108_Source_Name_17'] = run.create_node(name='108_Source_Name_17', type='source', parent=nodes['55_Input_Arguments17'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['108_Source_Name_17'].out_ports['out_output'] = Port(node=nodes['108_Source_Name_17'], name='out_output', description='')
    
    nodes['109_Source_Name_16'] = run.create_node(name='109_Source_Name_16', type='source', parent=nodes['68_Input_Arguments16'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['109_Source_Name_16'].out_ports['out_output'] = Port(node=nodes['109_Source_Name_16'], name='out_output', description='')
    
    nodes['110_Source_Name_15'] = run.create_node(name='110_Source_Name_15', type='source', parent=nodes['65_Input_Arguments15'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['110_Source_Name_15'].out_ports['out_output'] = Port(node=nodes['110_Source_Name_15'], name='out_output', description='')
    
    nodes['111_Source_Name_14'] = run.create_node(name='111_Source_Name_14', type='source', parent=nodes['43_Input_Arguments14'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['111_Source_Name_14'].out_ports['out_output'] = Port(node=nodes['111_Source_Name_14'], name='out_output', description='')
    
    nodes['112_nii2mnc18'] = run.create_node(name='112_nii2mnc18', type='node', parent=nodes['64_Files_Conversions18'], no_in_ports=0, no_out_ports=0)

    
    nodes['112_nii2mnc18'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['112_nii2mnc18'], name='in_Processed_NIFTII', description='')
    nodes['112_nii2mnc18'].in_ports['in_Output_File_Name'] = Port(node=nodes['112_nii2mnc18'], name='in_Output_File_Name', description='')

    
    nodes['112_nii2mnc18'].out_ports['out_output_file'] = Port(node=nodes['112_nii2mnc18'], name='out_output_file', description='')
    
    nodes['113_nii2mnc19'] = run.create_node(name='113_nii2mnc19', type='node', parent=nodes['63_Files_Conversions19'], no_in_ports=0, no_out_ports=0)

    
    nodes['113_nii2mnc19'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['113_nii2mnc19'], name='in_Processed_NIFTII', description='')
    nodes['113_nii2mnc19'].in_ports['in_Output_File_Name'] = Port(node=nodes['113_nii2mnc19'], name='in_Output_File_Name', description='')

    
    nodes['113_nii2mnc19'].out_ports['out_output_file'] = Port(node=nodes['113_nii2mnc19'], name='out_output_file', description='')
    
    nodes['114_nii2mnc10'] = run.create_node(name='114_nii2mnc10', type='node', parent=nodes['57_Files_Conversions10'], no_in_ports=0, no_out_ports=0)

    
    nodes['114_nii2mnc10'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['114_nii2mnc10'], name='in_Processed_NIFTII', description='')
    nodes['114_nii2mnc10'].in_ports['in_Output_File_Name'] = Port(node=nodes['114_nii2mnc10'], name='in_Output_File_Name', description='')

    
    nodes['114_nii2mnc10'].out_ports['out_output_file'] = Port(node=nodes['114_nii2mnc10'], name='out_output_file', description='')
    
    nodes['115_nii2mnc11'] = run.create_node(name='115_nii2mnc11', type='node', parent=nodes['56_Files_Conversions11'], no_in_ports=0, no_out_ports=0)

    
    nodes['115_nii2mnc11'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['115_nii2mnc11'], name='in_Processed_NIFTII', description='')
    nodes['115_nii2mnc11'].in_ports['in_Output_File_Name'] = Port(node=nodes['115_nii2mnc11'], name='in_Output_File_Name', description='')

    
    nodes['115_nii2mnc11'].out_ports['out_output_file'] = Port(node=nodes['115_nii2mnc11'], name='out_output_file', description='')
    
    nodes['116_nii2mnc12'] = run.create_node(name='116_nii2mnc12', type='node', parent=nodes['74_Files_Conversions12'], no_in_ports=0, no_out_ports=0)

    
    nodes['116_nii2mnc12'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['116_nii2mnc12'], name='in_Processed_NIFTII', description='')
    nodes['116_nii2mnc12'].in_ports['in_Output_File_Name'] = Port(node=nodes['116_nii2mnc12'], name='in_Output_File_Name', description='')

    
    nodes['116_nii2mnc12'].out_ports['out_output_file'] = Port(node=nodes['116_nii2mnc12'], name='out_output_file', description='')
    
    nodes['117_nii2mnc13'] = run.create_node(name='117_nii2mnc13', type='node', parent=nodes['54_Files_Conversions13'], no_in_ports=0, no_out_ports=0)

    
    nodes['117_nii2mnc13'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['117_nii2mnc13'], name='in_Processed_NIFTII', description='')
    nodes['117_nii2mnc13'].in_ports['in_Output_File_Name'] = Port(node=nodes['117_nii2mnc13'], name='in_Output_File_Name', description='')

    
    nodes['117_nii2mnc13'].out_ports['out_output_file'] = Port(node=nodes['117_nii2mnc13'], name='out_output_file', description='')
    
    nodes['118_nii2mnc14'] = run.create_node(name='118_nii2mnc14', type='node', parent=nodes['61_Files_Conversions14'], no_in_ports=0, no_out_ports=0)

    
    nodes['118_nii2mnc14'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['118_nii2mnc14'], name='in_Processed_NIFTII', description='')
    nodes['118_nii2mnc14'].in_ports['in_Output_File_Name'] = Port(node=nodes['118_nii2mnc14'], name='in_Output_File_Name', description='')

    
    nodes['118_nii2mnc14'].out_ports['out_output_file'] = Port(node=nodes['118_nii2mnc14'], name='out_output_file', description='')
    
    nodes['119_nii2mnc15'] = run.create_node(name='119_nii2mnc15', type='node', parent=nodes['60_Files_Conversions15'], no_in_ports=0, no_out_ports=0)

    
    nodes['119_nii2mnc15'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['119_nii2mnc15'], name='in_Processed_NIFTII', description='')
    nodes['119_nii2mnc15'].in_ports['in_Output_File_Name'] = Port(node=nodes['119_nii2mnc15'], name='in_Output_File_Name', description='')

    
    nodes['119_nii2mnc15'].out_ports['out_output_file'] = Port(node=nodes['119_nii2mnc15'], name='out_output_file', description='')
    
    nodes['120_nii2mnc16'] = run.create_node(name='120_nii2mnc16', type='node', parent=nodes['59_Files_Conversions16'], no_in_ports=0, no_out_ports=0)

    
    nodes['120_nii2mnc16'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['120_nii2mnc16'], name='in_Processed_NIFTII', description='')
    nodes['120_nii2mnc16'].in_ports['in_Output_File_Name'] = Port(node=nodes['120_nii2mnc16'], name='in_Output_File_Name', description='')

    
    nodes['120_nii2mnc16'].out_ports['out_output_file'] = Port(node=nodes['120_nii2mnc16'], name='out_output_file', description='')
    
    nodes['121_nii2mnc17'] = run.create_node(name='121_nii2mnc17', type='node', parent=nodes['58_Files_Conversions17'], no_in_ports=0, no_out_ports=0)

    
    nodes['121_nii2mnc17'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['121_nii2mnc17'], name='in_Processed_NIFTII', description='')
    nodes['121_nii2mnc17'].in_ports['in_Output_File_Name'] = Port(node=nodes['121_nii2mnc17'], name='in_Output_File_Name', description='')

    
    nodes['121_nii2mnc17'].out_ports['out_output_file'] = Port(node=nodes['121_nii2mnc17'], name='out_output_file', description='')
    
    nodes['122_ABM_Num_Of_Iterations'] = run.create_node(name='122_ABM_Num_Of_Iterations', type='source', parent=nodes['26_ABM_Program_Basic_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['122_ABM_Num_Of_Iterations'].out_ports['out_output'] = Port(node=nodes['122_ABM_Num_Of_Iterations'], name='out_output', description='')
    
    nodes['123_ICV_Weight'] = run.create_node(name='123_ICV_Weight', type='source', parent=nodes['7_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['123_ICV_Weight'].out_ports['out_output'] = Port(node=nodes['123_ICV_Weight'], name='out_output', description='')
    
    nodes['124_ABM_Sub_lattice'] = run.create_node(name='124_ABM_Sub_lattice', type='source', parent=nodes['20_ABM_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['124_ABM_Sub_lattice'].out_ports['out_output'] = Port(node=nodes['124_ABM_Sub_lattice'], name='out_output', description='')
    
    nodes['125_ICV_Output_ICV_JPG'] = run.create_node(name='125_ICV_Output_ICV_JPG', type='sink', parent=nodes['44_ICV_Files_Outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['125_ICV_Output_ICV_JPG'].in_ports['in_input'] = Port(node=nodes['125_ICV_Output_ICV_JPG'], name='in_input', description='')

    
    
    nodes['126_ICV_Clobber'] = run.create_node(name='126_ICV_Clobber', type='source', parent=nodes['7_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['126_ICV_Clobber'].out_ports['out_output'] = Port(node=nodes['126_ICV_Clobber'], name='out_output', description='')
    
    nodes['127_ABM_Num_Of_Registrations'] = run.create_node(name='127_ABM_Num_Of_Registrations', type='source', parent=nodes['72_ABM_Program_Linear_Registration_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['127_ABM_Num_Of_Registrations'].out_ports['out_output'] = Port(node=nodes['127_ABM_Num_Of_Registrations'], name='out_output', description='')
    
    nodes['128_ABM_Debug'] = run.create_node(name='128_ABM_Debug', type='source', parent=nodes['20_ABM_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['128_ABM_Debug'].out_ports['out_output'] = Port(node=nodes['128_ABM_Debug'], name='out_output', description='')
    
    nodes['129_ICV_Sub_lattice'] = run.create_node(name='129_ICV_Sub_lattice', type='source', parent=nodes['7_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['129_ICV_Sub_lattice'].out_ports['out_output'] = Port(node=nodes['129_ICV_Sub_lattice'], name='out_output', description='')
    
    nodes['130_mnc2mnc13'] = run.create_node(name='130_mnc2mnc13', type='node', parent=nodes['54_Files_Conversions13'], no_in_ports=0, no_out_ports=0)

    
    nodes['130_mnc2mnc13'].in_ports['in_Processed_Minc'] = Port(node=nodes['130_mnc2mnc13'], name='in_Processed_Minc', description='')
    nodes['130_mnc2mnc13'].in_ports['in_Output_Dir'] = Port(node=nodes['130_mnc2mnc13'], name='in_Output_Dir', description='')

    
    nodes['130_mnc2mnc13'].out_ports['out_output_file_mnc'] = Port(node=nodes['130_mnc2mnc13'], name='out_output_file_mnc', description='')
    
    nodes['131_ABM_Output_Parameters'] = run.create_node(name='131_ABM_Output_Parameters', type='sink', parent=nodes['12_ABM_Files_Outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['131_ABM_Output_Parameters'].in_ports['in_input'] = Port(node=nodes['131_ABM_Output_Parameters'], name='in_input', description='')

    
    
    nodes['132_ICV_Output_Log'] = run.create_node(name='132_ICV_Output_Log', type='sink', parent=nodes['44_ICV_Files_Outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['132_ICV_Output_Log'].in_ports['in_input'] = Port(node=nodes['132_ICV_Output_Log'], name='in_input', description='')

    
    
    nodes['133_IntracranialVolume'] = run.create_node(name='133_IntracranialVolume', type='node', parent=nodes['39_Mask_Calculation'], no_in_ports=0, no_out_ports=0)

    
    nodes['133_IntracranialVolume'].in_ports['in_Help_Argument'] = Port(node=nodes['133_IntracranialVolume'], name='in_Help_Argument', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Model'] = Port(node=nodes['133_IntracranialVolume'], name='in_Model', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Parameters_File'] = Port(node=nodes['133_IntracranialVolume'], name='in_Parameters_File', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Processed_File'] = Port(node=nodes['133_IntracranialVolume'], name='in_Processed_File', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Processed_Folder'] = Port(node=nodes['133_IntracranialVolume'], name='in_Processed_Folder', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Processed_XNAT_DICOM'] = Port(node=nodes['133_IntracranialVolume'], name='in_Processed_XNAT_DICOM', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Linear_Estimate'] = Port(node=nodes['133_IntracranialVolume'], name='in_Linear_Estimate', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Num_Of_Registrations'] = Port(node=nodes['133_IntracranialVolume'], name='in_Num_Of_Registrations', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Registration_Blur_FWHM'] = Port(node=nodes['133_IntracranialVolume'], name='in_Registration_Blur_FWHM', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Num_Of_Iterations'] = Port(node=nodes['133_IntracranialVolume'], name='in_Num_Of_Iterations', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Step_Size'] = Port(node=nodes['133_IntracranialVolume'], name='in_Step_Size', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Blur_FWHM'] = Port(node=nodes['133_IntracranialVolume'], name='in_Blur_FWHM', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Weight'] = Port(node=nodes['133_IntracranialVolume'], name='in_Weight', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Stiffness'] = Port(node=nodes['133_IntracranialVolume'], name='in_Stiffness', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Similarity'] = Port(node=nodes['133_IntracranialVolume'], name='in_Similarity', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Sub_lattice'] = Port(node=nodes['133_IntracranialVolume'], name='in_Sub_lattice', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Debug'] = Port(node=nodes['133_IntracranialVolume'], name='in_Debug', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Clobber'] = Port(node=nodes['133_IntracranialVolume'], name='in_Clobber', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Non_linear'] = Port(node=nodes['133_IntracranialVolume'], name='in_Non_linear', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Mask_To_Binary_Min'] = Port(node=nodes['133_IntracranialVolume'], name='in_Mask_To_Binary_Min', description='')
    nodes['133_IntracranialVolume'].in_ports['in_RUN_GUI'] = Port(node=nodes['133_IntracranialVolume'], name='in_RUN_GUI', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Calc_Crude'] = Port(node=nodes['133_IntracranialVolume'], name='in_Calc_Crude', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Calc_Fine'] = Port(node=nodes['133_IntracranialVolume'], name='in_Calc_Fine', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Clac_Ave_Brain'] = Port(node=nodes['133_IntracranialVolume'], name='in_Clac_Ave_Brain', description='')
    nodes['133_IntracranialVolume'].in_ports['in_Subject_Name'] = Port(node=nodes['133_IntracranialVolume'], name='in_Subject_Name', description='')

    
    nodes['133_IntracranialVolume'].out_ports['out_icv_directory'] = Port(node=nodes['133_IntracranialVolume'], name='out_icv_directory', description='')
    nodes['133_IntracranialVolume'].out_ports['out_icv_output_nii_mask'] = Port(node=nodes['133_IntracranialVolume'], name='out_icv_output_nii_mask', description='')
    nodes['133_IntracranialVolume'].out_ports['out_icv_output_mnc_mask'] = Port(node=nodes['133_IntracranialVolume'], name='out_icv_output_mnc_mask', description='')
    nodes['133_IntracranialVolume'].out_ports['out_icv_volume'] = Port(node=nodes['133_IntracranialVolume'], name='out_icv_volume', description='')
    nodes['133_IntracranialVolume'].out_ports['out_icv_parameters_used'] = Port(node=nodes['133_IntracranialVolume'], name='out_icv_parameters_used', description='')
    nodes['133_IntracranialVolume'].out_ports['out_icv_log'] = Port(node=nodes['133_IntracranialVolume'], name='out_icv_log', description='')
    nodes['133_IntracranialVolume'].out_ports['out_icv_jpg'] = Port(node=nodes['133_IntracranialVolume'], name='out_icv_jpg', description='')
    nodes['133_IntracranialVolume'].out_ports['out_icv_original_file_In_nii'] = Port(node=nodes['133_IntracranialVolume'], name='out_icv_original_file_In_nii', description='')
    
    nodes['134_ABM_Stiffness'] = run.create_node(name='134_ABM_Stiffness', type='source', parent=nodes['20_ABM_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['134_ABM_Stiffness'].out_ports['out_output'] = Port(node=nodes['134_ABM_Stiffness'], name='out_output', description='')
    
    nodes['135_Source_6'] = run.create_node(name='135_Source_6', type='source', parent=nodes['33_Input_Arguments6'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['135_Source_6'].out_ports['out_output'] = Port(node=nodes['135_Source_6'], name='out_output', description='')
    
    nodes['136_ICV_Output_ICV_Volume'] = run.create_node(name='136_ICV_Output_ICV_Volume', type='sink', parent=nodes['44_ICV_Files_Outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['136_ICV_Output_ICV_Volume'].in_ports['in_input'] = Port(node=nodes['136_ICV_Output_ICV_Volume'], name='in_input', description='')

    
    
    nodes['137_ICV_Registration_Blur_FWHM'] = run.create_node(name='137_ICV_Registration_Blur_FWHM', type='source', parent=nodes['38_Program_Linear_Registration_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['137_ICV_Registration_Blur_FWHM'].out_ports['out_output'] = Port(node=nodes['137_ICV_Registration_Blur_FWHM'], name='out_output', description='')
    
    nodes['138_ABM_Blur_FWHM'] = run.create_node(name='138_ABM_Blur_FWHM', type='source', parent=nodes['26_ABM_Program_Basic_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['138_ABM_Blur_FWHM'].out_ports['out_output'] = Port(node=nodes['138_ABM_Blur_FWHM'], name='out_output', description='')
    
    nodes['139_ICV_Non_linear'] = run.create_node(name='139_ICV_Non_linear', type='source', parent=nodes['7_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['139_ICV_Non_linear'].out_ports['out_output'] = Port(node=nodes['139_ICV_Non_linear'], name='out_output', description='')
    
    nodes['140_mnc2mnc10'] = run.create_node(name='140_mnc2mnc10', type='node', parent=nodes['57_Files_Conversions10'], no_in_ports=0, no_out_ports=0)

    
    nodes['140_mnc2mnc10'].in_ports['in_Processed_Minc'] = Port(node=nodes['140_mnc2mnc10'], name='in_Processed_Minc', description='')
    nodes['140_mnc2mnc10'].in_ports['in_Output_Dir'] = Port(node=nodes['140_mnc2mnc10'], name='in_Output_Dir', description='')

    
    nodes['140_mnc2mnc10'].out_ports['out_output_file_mnc'] = Port(node=nodes['140_mnc2mnc10'], name='out_output_file_mnc', description='')
    
    nodes['141_Input_Folder'] = run.create_node(name='141_Input_Folder', type='source', parent=nodes['62_AMB_Input_Folder'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['141_Input_Folder'].out_ports['out_output'] = Port(node=nodes['141_Input_Folder'], name='out_output', description='')
    
    nodes['142_ICV_Blur_FWHM'] = run.create_node(name='142_ICV_Blur_FWHM', type='source', parent=nodes['19_Program_Basic_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['142_ICV_Blur_FWHM'].out_ports['out_output'] = Port(node=nodes['142_ICV_Blur_FWHM'], name='out_output', description='')
    
    nodes['143_mnc2mnc29'] = run.create_node(name='143_mnc2mnc29', type='node', parent=nodes['16_Files_Conversions29'], no_in_ports=0, no_out_ports=0)

    
    nodes['143_mnc2mnc29'].in_ports['in_Processed_Minc'] = Port(node=nodes['143_mnc2mnc29'], name='in_Processed_Minc', description='')
    nodes['143_mnc2mnc29'].in_ports['in_Output_Dir'] = Port(node=nodes['143_mnc2mnc29'], name='in_Output_Dir', description='')

    
    nodes['143_mnc2mnc29'].out_ports['out_output_file_mnc'] = Port(node=nodes['143_mnc2mnc29'], name='out_output_file_mnc', description='')
    
    nodes['144_mnc2mnc28'] = run.create_node(name='144_mnc2mnc28', type='node', parent=nodes['15_Files_Conversions28'], no_in_ports=0, no_out_ports=0)

    
    nodes['144_mnc2mnc28'].in_ports['in_Processed_Minc'] = Port(node=nodes['144_mnc2mnc28'], name='in_Processed_Minc', description='')
    nodes['144_mnc2mnc28'].in_ports['in_Output_Dir'] = Port(node=nodes['144_mnc2mnc28'], name='in_Output_Dir', description='')

    
    nodes['144_mnc2mnc28'].out_ports['out_output_file_mnc'] = Port(node=nodes['144_mnc2mnc28'], name='out_output_file_mnc', description='')
    
    nodes['145_ABM_Weight'] = run.create_node(name='145_ABM_Weight', type='source', parent=nodes['20_ABM_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['145_ABM_Weight'].out_ports['out_output'] = Port(node=nodes['145_ABM_Weight'], name='out_output', description='')
    
    nodes['146_mnc2mnc21'] = run.create_node(name='146_mnc2mnc21', type='node', parent=nodes['21_Files_Conversions21'], no_in_ports=0, no_out_ports=0)

    
    nodes['146_mnc2mnc21'].in_ports['in_Processed_Minc'] = Port(node=nodes['146_mnc2mnc21'], name='in_Processed_Minc', description='')
    nodes['146_mnc2mnc21'].in_ports['in_Output_Dir'] = Port(node=nodes['146_mnc2mnc21'], name='in_Output_Dir', description='')

    
    nodes['146_mnc2mnc21'].out_ports['out_output_file_mnc'] = Port(node=nodes['146_mnc2mnc21'], name='out_output_file_mnc', description='')
    
    nodes['147_mnc2mnc20'] = run.create_node(name='147_mnc2mnc20', type='node', parent=nodes['73_Files_Conversions20'], no_in_ports=0, no_out_ports=0)

    
    nodes['147_mnc2mnc20'].in_ports['in_Processed_Minc'] = Port(node=nodes['147_mnc2mnc20'], name='in_Processed_Minc', description='')
    nodes['147_mnc2mnc20'].in_ports['in_Output_Dir'] = Port(node=nodes['147_mnc2mnc20'], name='in_Output_Dir', description='')

    
    nodes['147_mnc2mnc20'].out_ports['out_output_file_mnc'] = Port(node=nodes['147_mnc2mnc20'], name='out_output_file_mnc', description='')
    
    nodes['148_mnc2mnc23'] = run.create_node(name='148_mnc2mnc23', type='node', parent=nodes['69_Files_Conversions23'], no_in_ports=0, no_out_ports=0)

    
    nodes['148_mnc2mnc23'].in_ports['in_Processed_Minc'] = Port(node=nodes['148_mnc2mnc23'], name='in_Processed_Minc', description='')
    nodes['148_mnc2mnc23'].in_ports['in_Output_Dir'] = Port(node=nodes['148_mnc2mnc23'], name='in_Output_Dir', description='')

    
    nodes['148_mnc2mnc23'].out_ports['out_output_file_mnc'] = Port(node=nodes['148_mnc2mnc23'], name='out_output_file_mnc', description='')
    
    nodes['149_mnc2mnc22'] = run.create_node(name='149_mnc2mnc22', type='node', parent=nodes['18_Files_Conversions22'], no_in_ports=0, no_out_ports=0)

    
    nodes['149_mnc2mnc22'].in_ports['in_Processed_Minc'] = Port(node=nodes['149_mnc2mnc22'], name='in_Processed_Minc', description='')
    nodes['149_mnc2mnc22'].in_ports['in_Output_Dir'] = Port(node=nodes['149_mnc2mnc22'], name='in_Output_Dir', description='')

    
    nodes['149_mnc2mnc22'].out_ports['out_output_file_mnc'] = Port(node=nodes['149_mnc2mnc22'], name='out_output_file_mnc', description='')
    
    nodes['150_mnc2mnc25'] = run.create_node(name='150_mnc2mnc25', type='node', parent=nodes['25_Files_Conversions25'], no_in_ports=0, no_out_ports=0)

    
    nodes['150_mnc2mnc25'].in_ports['in_Processed_Minc'] = Port(node=nodes['150_mnc2mnc25'], name='in_Processed_Minc', description='')
    nodes['150_mnc2mnc25'].in_ports['in_Output_Dir'] = Port(node=nodes['150_mnc2mnc25'], name='in_Output_Dir', description='')

    
    nodes['150_mnc2mnc25'].out_ports['out_output_file_mnc'] = Port(node=nodes['150_mnc2mnc25'], name='out_output_file_mnc', description='')
    
    nodes['151_mnc2mnc24'] = run.create_node(name='151_mnc2mnc24', type='node', parent=nodes['24_Files_Conversions24'], no_in_ports=0, no_out_ports=0)

    
    nodes['151_mnc2mnc24'].in_ports['in_Processed_Minc'] = Port(node=nodes['151_mnc2mnc24'], name='in_Processed_Minc', description='')
    nodes['151_mnc2mnc24'].in_ports['in_Output_Dir'] = Port(node=nodes['151_mnc2mnc24'], name='in_Output_Dir', description='')

    
    nodes['151_mnc2mnc24'].out_ports['out_output_file_mnc'] = Port(node=nodes['151_mnc2mnc24'], name='out_output_file_mnc', description='')
    
    nodes['152_mnc2mnc27'] = run.create_node(name='152_mnc2mnc27', type='node', parent=nodes['23_Files_Conversions27'], no_in_ports=0, no_out_ports=0)

    
    nodes['152_mnc2mnc27'].in_ports['in_Processed_Minc'] = Port(node=nodes['152_mnc2mnc27'], name='in_Processed_Minc', description='')
    nodes['152_mnc2mnc27'].in_ports['in_Output_Dir'] = Port(node=nodes['152_mnc2mnc27'], name='in_Output_Dir', description='')

    
    nodes['152_mnc2mnc27'].out_ports['out_output_file_mnc'] = Port(node=nodes['152_mnc2mnc27'], name='out_output_file_mnc', description='')
    
    nodes['153_mnc2mnc26'] = run.create_node(name='153_mnc2mnc26', type='node', parent=nodes['22_Files_Conversions26'], no_in_ports=0, no_out_ports=0)

    
    nodes['153_mnc2mnc26'].in_ports['in_Processed_Minc'] = Port(node=nodes['153_mnc2mnc26'], name='in_Processed_Minc', description='')
    nodes['153_mnc2mnc26'].in_ports['in_Output_Dir'] = Port(node=nodes['153_mnc2mnc26'], name='in_Output_Dir', description='')

    
    nodes['153_mnc2mnc26'].out_ports['out_output_file_mnc'] = Port(node=nodes['153_mnc2mnc26'], name='out_output_file_mnc', description='')
    
    nodes['154_Source_Name_13'] = run.create_node(name='154_Source_Name_13', type='source', parent=nodes['71_Input_Arguments13'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['154_Source_Name_13'].out_ports['out_output'] = Port(node=nodes['154_Source_Name_13'], name='out_output', description='')
    
    nodes['155_ICV_Output_ICV_Mask'] = run.create_node(name='155_ICV_Output_ICV_Mask', type='sink', parent=nodes['44_ICV_Files_Outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['155_ICV_Output_ICV_Mask'].in_ports['in_input'] = Port(node=nodes['155_ICV_Output_ICV_Mask'], name='in_input', description='')

    
    
    nodes['156_ABM_First_Subject'] = run.create_node(name='156_ABM_First_Subject', type='source', parent=nodes['28_ABM_First_Subject'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['156_ABM_First_Subject'].out_ports['out_output'] = Port(node=nodes['156_ABM_First_Subject'], name='out_output', description='')
    
    nodes['157_nii2mnc2'] = run.create_node(name='157_nii2mnc2', type='node', parent=nodes['41_Files_Conversions2'], no_in_ports=0, no_out_ports=0)

    
    nodes['157_nii2mnc2'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['157_nii2mnc2'], name='in_Processed_NIFTII', description='')
    nodes['157_nii2mnc2'].in_ports['in_Output_File_Name'] = Port(node=nodes['157_nii2mnc2'], name='in_Output_File_Name', description='')

    
    nodes['157_nii2mnc2'].out_ports['out_output_file'] = Port(node=nodes['157_nii2mnc2'], name='out_output_file', description='')
    
    nodes['158_nii2mnc3'] = run.create_node(name='158_nii2mnc3', type='node', parent=nodes['40_Files_Conversions3'], no_in_ports=0, no_out_ports=0)

    
    nodes['158_nii2mnc3'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['158_nii2mnc3'], name='in_Processed_NIFTII', description='')
    nodes['158_nii2mnc3'].in_ports['in_Output_File_Name'] = Port(node=nodes['158_nii2mnc3'], name='in_Output_File_Name', description='')

    
    nodes['158_nii2mnc3'].out_ports['out_output_file'] = Port(node=nodes['158_nii2mnc3'], name='out_output_file', description='')
    
    nodes['159_nii2mnc1'] = run.create_node(name='159_nii2mnc1', type='node', parent=nodes['42_Files_Conversions1'], no_in_ports=0, no_out_ports=0)

    
    nodes['159_nii2mnc1'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['159_nii2mnc1'], name='in_Processed_NIFTII', description='')
    nodes['159_nii2mnc1'].in_ports['in_Output_File_Name'] = Port(node=nodes['159_nii2mnc1'], name='in_Output_File_Name', description='')

    
    nodes['159_nii2mnc1'].out_ports['out_output_file'] = Port(node=nodes['159_nii2mnc1'], name='out_output_file', description='')
    
    nodes['160_nii2mnc6'] = run.create_node(name='160_nii2mnc6', type='node', parent=nodes['45_Files_Conversions6'], no_in_ports=0, no_out_ports=0)

    
    nodes['160_nii2mnc6'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['160_nii2mnc6'], name='in_Processed_NIFTII', description='')
    nodes['160_nii2mnc6'].in_ports['in_Output_File_Name'] = Port(node=nodes['160_nii2mnc6'], name='in_Output_File_Name', description='')

    
    nodes['160_nii2mnc6'].out_ports['out_output_file'] = Port(node=nodes['160_nii2mnc6'], name='out_output_file', description='')
    
    nodes['161_nii2mnc7'] = run.create_node(name='161_nii2mnc7', type='node', parent=nodes['51_Files_Conversions7'], no_in_ports=0, no_out_ports=0)

    
    nodes['161_nii2mnc7'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['161_nii2mnc7'], name='in_Processed_NIFTII', description='')
    nodes['161_nii2mnc7'].in_ports['in_Output_File_Name'] = Port(node=nodes['161_nii2mnc7'], name='in_Output_File_Name', description='')

    
    nodes['161_nii2mnc7'].out_ports['out_output_file'] = Port(node=nodes['161_nii2mnc7'], name='out_output_file', description='')
    
    nodes['162_nii2mnc4'] = run.create_node(name='162_nii2mnc4', type='node', parent=nodes['3_Files_Conversions4'], no_in_ports=0, no_out_ports=0)

    
    nodes['162_nii2mnc4'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['162_nii2mnc4'], name='in_Processed_NIFTII', description='')
    nodes['162_nii2mnc4'].in_ports['in_Output_File_Name'] = Port(node=nodes['162_nii2mnc4'], name='in_Output_File_Name', description='')

    
    nodes['162_nii2mnc4'].out_ports['out_output_file'] = Port(node=nodes['162_nii2mnc4'], name='out_output_file', description='')
    
    nodes['163_nii2mnc5'] = run.create_node(name='163_nii2mnc5', type='node', parent=nodes['0_Files_Conversions5'], no_in_ports=0, no_out_ports=0)

    
    nodes['163_nii2mnc5'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['163_nii2mnc5'], name='in_Processed_NIFTII', description='')
    nodes['163_nii2mnc5'].in_ports['in_Output_File_Name'] = Port(node=nodes['163_nii2mnc5'], name='in_Output_File_Name', description='')

    
    nodes['163_nii2mnc5'].out_ports['out_output_file'] = Port(node=nodes['163_nii2mnc5'], name='out_output_file', description='')
    
    nodes['164_nii2mnc8'] = run.create_node(name='164_nii2mnc8', type='node', parent=nodes['50_Files_Conversions8'], no_in_ports=0, no_out_ports=0)

    
    nodes['164_nii2mnc8'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['164_nii2mnc8'], name='in_Processed_NIFTII', description='')
    nodes['164_nii2mnc8'].in_ports['in_Output_File_Name'] = Port(node=nodes['164_nii2mnc8'], name='in_Output_File_Name', description='')

    
    nodes['164_nii2mnc8'].out_ports['out_output_file'] = Port(node=nodes['164_nii2mnc8'], name='out_output_file', description='')
    
    nodes['165_nii2mnc9'] = run.create_node(name='165_nii2mnc9', type='node', parent=nodes['49_Files_Conversions9'], no_in_ports=0, no_out_ports=0)

    
    nodes['165_nii2mnc9'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['165_nii2mnc9'], name='in_Processed_NIFTII', description='')
    nodes['165_nii2mnc9'].in_ports['in_Output_File_Name'] = Port(node=nodes['165_nii2mnc9'], name='in_Output_File_Name', description='')

    
    nodes['165_nii2mnc9'].out_ports['out_output_file'] = Port(node=nodes['165_nii2mnc9'], name='out_output_file', description='')
    
    nodes['166_mnc2mnc12'] = run.create_node(name='166_mnc2mnc12', type='node', parent=nodes['74_Files_Conversions12'], no_in_ports=0, no_out_ports=0)

    
    nodes['166_mnc2mnc12'].in_ports['in_Processed_Minc'] = Port(node=nodes['166_mnc2mnc12'], name='in_Processed_Minc', description='')
    nodes['166_mnc2mnc12'].in_ports['in_Output_Dir'] = Port(node=nodes['166_mnc2mnc12'], name='in_Output_Dir', description='')

    
    nodes['166_mnc2mnc12'].out_ports['out_output_file_mnc'] = Port(node=nodes['166_mnc2mnc12'], name='out_output_file_mnc', description='')
    
    nodes['167_ICV_Output_ICV_Parameters'] = run.create_node(name='167_ICV_Output_ICV_Parameters', type='sink', parent=nodes['44_ICV_Files_Outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['167_ICV_Output_ICV_Parameters'].in_ports['in_input'] = Port(node=nodes['167_ICV_Output_ICV_Parameters'], name='in_input', description='')

    
    
    nodes['168_ABM_Output_Log'] = run.create_node(name='168_ABM_Output_Log', type='sink', parent=nodes['12_ABM_Files_Outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['168_ABM_Output_Log'].in_ports['in_input'] = Port(node=nodes['168_ABM_Output_Log'], name='in_input', description='')

    
    
    nodes['169_ICV_Similarity'] = run.create_node(name='169_ICV_Similarity', type='source', parent=nodes['7_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['169_ICV_Similarity'].out_ports['out_output'] = Port(node=nodes['169_ICV_Similarity'], name='out_output', description='')
    
    nodes['170_nii2mnc25'] = run.create_node(name='170_nii2mnc25', type='node', parent=nodes['25_Files_Conversions25'], no_in_ports=0, no_out_ports=0)

    
    nodes['170_nii2mnc25'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['170_nii2mnc25'], name='in_Processed_NIFTII', description='')
    nodes['170_nii2mnc25'].in_ports['in_Output_File_Name'] = Port(node=nodes['170_nii2mnc25'], name='in_Output_File_Name', description='')

    
    nodes['170_nii2mnc25'].out_ports['out_output_file'] = Port(node=nodes['170_nii2mnc25'], name='out_output_file', description='')
    
    nodes['171_ABM_Output_Brain'] = run.create_node(name='171_ABM_Output_Brain', type='sink', parent=nodes['12_ABM_Files_Outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['171_ABM_Output_Brain'].in_ports['in_input'] = Port(node=nodes['171_ABM_Output_Brain'], name='in_input', description='')

    
    
    nodes['172_mnc2mnc15'] = run.create_node(name='172_mnc2mnc15', type='node', parent=nodes['60_Files_Conversions15'], no_in_ports=0, no_out_ports=0)

    
    nodes['172_mnc2mnc15'].in_ports['in_Processed_Minc'] = Port(node=nodes['172_mnc2mnc15'], name='in_Processed_Minc', description='')
    nodes['172_mnc2mnc15'].in_ports['in_Output_Dir'] = Port(node=nodes['172_mnc2mnc15'], name='in_Output_Dir', description='')

    
    nodes['172_mnc2mnc15'].out_ports['out_output_file_mnc'] = Port(node=nodes['172_mnc2mnc15'], name='out_output_file_mnc', description='')
    
    nodes['173_mnc2mnc16'] = run.create_node(name='173_mnc2mnc16', type='node', parent=nodes['59_Files_Conversions16'], no_in_ports=0, no_out_ports=0)

    
    nodes['173_mnc2mnc16'].in_ports['in_Processed_Minc'] = Port(node=nodes['173_mnc2mnc16'], name='in_Processed_Minc', description='')
    nodes['173_mnc2mnc16'].in_ports['in_Output_Dir'] = Port(node=nodes['173_mnc2mnc16'], name='in_Output_Dir', description='')

    
    nodes['173_mnc2mnc16'].out_ports['out_output_file_mnc'] = Port(node=nodes['173_mnc2mnc16'], name='out_output_file_mnc', description='')
    
    nodes['174_Collapse_Minc_To_Folder'] = run.create_node(name='174_Collapse_Minc_To_Folder', type='node', parent=nodes['17_Collect_Converted_Files'], no_in_ports=0, no_out_ports=0)

    
    nodes['174_Collapse_Minc_To_Folder'].in_ports['in_Image_Name'] = Port(node=nodes['174_Collapse_Minc_To_Folder'], name='in_Image_Name', description='')

    
    nodes['174_Collapse_Minc_To_Folder'].out_ports['out_output_directory'] = Port(node=nodes['174_Collapse_Minc_To_Folder'], name='out_output_directory', description='')
    
    nodes['175_mnc2mnc17'] = run.create_node(name='175_mnc2mnc17', type='node', parent=nodes['58_Files_Conversions17'], no_in_ports=0, no_out_ports=0)

    
    nodes['175_mnc2mnc17'].in_ports['in_Processed_Minc'] = Port(node=nodes['175_mnc2mnc17'], name='in_Processed_Minc', description='')
    nodes['175_mnc2mnc17'].in_ports['in_Output_Dir'] = Port(node=nodes['175_mnc2mnc17'], name='in_Output_Dir', description='')

    
    nodes['175_mnc2mnc17'].out_ports['out_output_file_mnc'] = Port(node=nodes['175_mnc2mnc17'], name='out_output_file_mnc', description='')
    
    nodes['176_mnc2mnc18'] = run.create_node(name='176_mnc2mnc18', type='node', parent=nodes['64_Files_Conversions18'], no_in_ports=0, no_out_ports=0)

    
    nodes['176_mnc2mnc18'].in_ports['in_Processed_Minc'] = Port(node=nodes['176_mnc2mnc18'], name='in_Processed_Minc', description='')
    nodes['176_mnc2mnc18'].in_ports['in_Output_Dir'] = Port(node=nodes['176_mnc2mnc18'], name='in_Output_Dir', description='')

    
    nodes['176_mnc2mnc18'].out_ports['out_output_file_mnc'] = Port(node=nodes['176_mnc2mnc18'], name='out_output_file_mnc', description='')
    
    nodes['177_ABM_Non_linear'] = run.create_node(name='177_ABM_Non_linear', type='source', parent=nodes['20_ABM_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['177_ABM_Non_linear'].out_ports['out_output'] = Port(node=nodes['177_ABM_Non_linear'], name='out_output', description='')
    
    nodes['178_mnc2mnc19'] = run.create_node(name='178_mnc2mnc19', type='node', parent=nodes['63_Files_Conversions19'], no_in_ports=0, no_out_ports=0)

    
    nodes['178_mnc2mnc19'].in_ports['in_Processed_Minc'] = Port(node=nodes['178_mnc2mnc19'], name='in_Processed_Minc', description='')
    nodes['178_mnc2mnc19'].in_ports['in_Output_Dir'] = Port(node=nodes['178_mnc2mnc19'], name='in_Output_Dir', description='')

    
    nodes['178_mnc2mnc19'].out_ports['out_output_file_mnc'] = Port(node=nodes['178_mnc2mnc19'], name='out_output_file_mnc', description='')
    
    nodes['179_ABM_Linear_Estimate'] = run.create_node(name='179_ABM_Linear_Estimate', type='source', parent=nodes['72_ABM_Program_Linear_Registration_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['179_ABM_Linear_Estimate'].out_ports['out_output'] = Port(node=nodes['179_ABM_Linear_Estimate'], name='out_output', description='')
    
    nodes['180_ABM_Similarity'] = run.create_node(name='180_ABM_Similarity', type='source', parent=nodes['20_ABM_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['180_ABM_Similarity'].out_ports['out_output'] = Port(node=nodes['180_ABM_Similarity'], name='out_output', description='')
    
    nodes['181_ABM_Registration_Blur_FWHM'] = run.create_node(name='181_ABM_Registration_Blur_FWHM', type='source', parent=nodes['72_ABM_Program_Linear_Registration_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['181_ABM_Registration_Blur_FWHM'].out_ports['out_output'] = Port(node=nodes['181_ABM_Registration_Blur_FWHM'], name='out_output', description='')
    
    nodes['182_Source_29'] = run.create_node(name='182_Source_29', type='source', parent=nodes['14_Input_Arguments29'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['182_Source_29'].out_ports['out_output'] = Port(node=nodes['182_Source_29'], name='out_output', description='')
    
    nodes['183_Source_28'] = run.create_node(name='183_Source_28', type='source', parent=nodes['13_Input_Arguments28'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['183_Source_28'].out_ports['out_output'] = Port(node=nodes['183_Source_28'], name='out_output', description='')
    
    nodes['184_ICV_Num_Of_Iterations'] = run.create_node(name='184_ICV_Num_Of_Iterations', type='source', parent=nodes['19_Program_Basic_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['184_ICV_Num_Of_Iterations'].out_ports['out_output'] = Port(node=nodes['184_ICV_Num_Of_Iterations'], name='out_output', description='')
    
    nodes['185_Source_25'] = run.create_node(name='185_Source_25', type='source', parent=nodes['11_Input_Arguments25'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['185_Source_25'].out_ports['out_output'] = Port(node=nodes['185_Source_25'], name='out_output', description='')
    
    nodes['186_Source_24'] = run.create_node(name='186_Source_24', type='source', parent=nodes['10_Input_Arguments24'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['186_Source_24'].out_ports['out_output'] = Port(node=nodes['186_Source_24'], name='out_output', description='')
    
    nodes['187_Source_27'] = run.create_node(name='187_Source_27', type='source', parent=nodes['9_Input_Arguments27'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['187_Source_27'].out_ports['out_output'] = Port(node=nodes['187_Source_27'], name='out_output', description='')
    
    nodes['188_Source_26'] = run.create_node(name='188_Source_26', type='source', parent=nodes['8_Input_Arguments26'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['188_Source_26'].out_ports['out_output'] = Port(node=nodes['188_Source_26'], name='out_output', description='')
    
    nodes['189_Source_21'] = run.create_node(name='189_Source_21', type='source', parent=nodes['67_Input_Arguments21'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['189_Source_21'].out_ports['out_output'] = Port(node=nodes['189_Source_21'], name='out_output', description='')
    
    nodes['190_Source_20'] = run.create_node(name='190_Source_20', type='source', parent=nodes['6_Input_Arguments20'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['190_Source_20'].out_ports['out_output'] = Port(node=nodes['190_Source_20'], name='out_output', description='')
    
    nodes['191_Source_23'] = run.create_node(name='191_Source_23', type='source', parent=nodes['5_Input_Arguments23'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['191_Source_23'].out_ports['out_output'] = Port(node=nodes['191_Source_23'], name='out_output', description='')
    
    nodes['192_Source_22'] = run.create_node(name='192_Source_22', type='source', parent=nodes['4_Input_Arguments22'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['192_Source_22'].out_ports['out_output'] = Port(node=nodes['192_Source_22'], name='out_output', description='')
    
    nodes['193_ICV_Stiffness'] = run.create_node(name='193_ICV_Stiffness', type='source', parent=nodes['7_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['193_ICV_Stiffness'].out_ports['out_output'] = Port(node=nodes['193_ICV_Stiffness'], name='out_output', description='')
    
    nodes['194_ICV_Step_Size'] = run.create_node(name='194_ICV_Step_Size', type='source', parent=nodes['19_Program_Basic_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['194_ICV_Step_Size'].out_ports['out_output'] = Port(node=nodes['194_ICV_Step_Size'], name='out_output', description='')
    
    nodes['195_Average_Brain_Model'] = run.create_node(name='195_Average_Brain_Model', type='node', parent=nodes['2_Main_Calculation'], no_in_ports=0, no_out_ports=0)

    
    nodes['195_Average_Brain_Model'].in_ports['in_Processed_Folder'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Processed_Folder', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_Linear_Estimate'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Linear_Estimate', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_Num_Of_Registrations'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Num_Of_Registrations', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_Registration_Blur_FWHM'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Registration_Blur_FWHM', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_Num_Of_Iterations'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Num_Of_Iterations', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_Step_Size'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Step_Size', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_Blur_FWHM'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Blur_FWHM', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_Weight'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Weight', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_Stiffness'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Stiffness', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_Similarity'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Similarity', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_Sub_lattice'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Sub_lattice', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_Debug'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Debug', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_Clobber'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Clobber', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_Non_linear'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Non_linear', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_ABM_First_Subject'] = Port(node=nodes['195_Average_Brain_Model'], name='in_ABM_First_Subject', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_Clac_Ave_Brain'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Clac_Ave_Brain', description='')
    nodes['195_Average_Brain_Model'].in_ports['in_Subject_Name'] = Port(node=nodes['195_Average_Brain_Model'], name='in_Subject_Name', description='')

    
    nodes['195_Average_Brain_Model'].out_ports['out_abm_directory'] = Port(node=nodes['195_Average_Brain_Model'], name='out_abm_directory', description='')
    nodes['195_Average_Brain_Model'].out_ports['out_abm_output_nii'] = Port(node=nodes['195_Average_Brain_Model'], name='out_abm_output_nii', description='')
    nodes['195_Average_Brain_Model'].out_ports['out_abm_output_mnc'] = Port(node=nodes['195_Average_Brain_Model'], name='out_abm_output_mnc', description='')
    nodes['195_Average_Brain_Model'].out_ports['out_abm_parameters_used'] = Port(node=nodes['195_Average_Brain_Model'], name='out_abm_parameters_used', description='')
    nodes['195_Average_Brain_Model'].out_ports['out_abm_log'] = Port(node=nodes['195_Average_Brain_Model'], name='out_abm_log', description='')
    nodes['195_Average_Brain_Model'].out_ports['out_abm_jpg'] = Port(node=nodes['195_Average_Brain_Model'], name='out_abm_jpg', description='')
    
    nodes['196_Source_Name_9'] = run.create_node(name='196_Source_Name_9', type='source', parent=nodes['36_Input_Arguments9'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['196_Source_Name_9'].out_ports['out_output'] = Port(node=nodes['196_Source_Name_9'], name='out_output', description='')
    
    nodes['197_Source_Name_8'] = run.create_node(name='197_Source_Name_8', type='source', parent=nodes['37_Input_Arguments8'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['197_Source_Name_8'].out_ports['out_output'] = Port(node=nodes['197_Source_Name_8'], name='out_output', description='')
    
    nodes['198_Source_Name_3'] = run.create_node(name='198_Source_Name_3', type='source', parent=nodes['29_Input_Arguments3'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['198_Source_Name_3'].out_ports['out_output'] = Port(node=nodes['198_Source_Name_3'], name='out_output', description='')
    
    nodes['199_ABM_Clobber'] = run.create_node(name='199_ABM_Clobber', type='source', parent=nodes['20_ABM_Program_Extended_Arguments'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['199_ABM_Clobber'].out_ports['out_output'] = Port(node=nodes['199_ABM_Clobber'], name='out_output', description='')
    
    nodes['200_Source_Name_1'] = run.create_node(name='200_Source_Name_1', type='source', parent=nodes['31_Input_Arguments1'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['200_Source_Name_1'].out_ports['out_output'] = Port(node=nodes['200_Source_Name_1'], name='out_output', description='')
    
    nodes['201_Source_Name_7'] = run.create_node(name='201_Source_Name_7', type='source', parent=nodes['32_Input_Arguments7'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['201_Source_Name_7'].out_ports['out_output'] = Port(node=nodes['201_Source_Name_7'], name='out_output', description='')
    
    nodes['202_Source_Name_6'] = run.create_node(name='202_Source_Name_6', type='source', parent=nodes['33_Input_Arguments6'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['202_Source_Name_6'].out_ports['out_output'] = Port(node=nodes['202_Source_Name_6'], name='out_output', description='')
    
    nodes['203_Source_Name_5'] = run.create_node(name='203_Source_Name_5', type='source', parent=nodes['34_Input_Arguments5'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['203_Source_Name_5'].out_ports['out_output'] = Port(node=nodes['203_Source_Name_5'], name='out_output', description='')
    
    nodes['204_Source_Name_4'] = run.create_node(name='204_Source_Name_4', type='source', parent=nodes['35_Input_Arguments4'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['204_Source_Name_4'].out_ports['out_output'] = Port(node=nodes['204_Source_Name_4'], name='out_output', description='')
    
    nodes['205_ABM_Clac_Ave_Brain'] = run.create_node(name='205_ABM_Clac_Ave_Brain', type='source', parent=nodes['27_ABM_Operation_Type'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['205_ABM_Clac_Ave_Brain'].out_ports['out_output'] = Port(node=nodes['205_ABM_Clac_Ave_Brain'], name='out_output', description='')
    
    nodes['206_mnc2mnc8'] = run.create_node(name='206_mnc2mnc8', type='node', parent=nodes['50_Files_Conversions8'], no_in_ports=0, no_out_ports=0)

    
    nodes['206_mnc2mnc8'].in_ports['in_Processed_Minc'] = Port(node=nodes['206_mnc2mnc8'], name='in_Processed_Minc', description='')
    nodes['206_mnc2mnc8'].in_ports['in_Output_Dir'] = Port(node=nodes['206_mnc2mnc8'], name='in_Output_Dir', description='')

    
    nodes['206_mnc2mnc8'].out_ports['out_output_file_mnc'] = Port(node=nodes['206_mnc2mnc8'], name='out_output_file_mnc', description='')
    
    nodes['207_mnc2mnc9'] = run.create_node(name='207_mnc2mnc9', type='node', parent=nodes['49_Files_Conversions9'], no_in_ports=0, no_out_ports=0)

    
    nodes['207_mnc2mnc9'].in_ports['in_Processed_Minc'] = Port(node=nodes['207_mnc2mnc9'], name='in_Processed_Minc', description='')
    nodes['207_mnc2mnc9'].in_ports['in_Output_Dir'] = Port(node=nodes['207_mnc2mnc9'], name='in_Output_Dir', description='')

    
    nodes['207_mnc2mnc9'].out_ports['out_output_file_mnc'] = Port(node=nodes['207_mnc2mnc9'], name='out_output_file_mnc', description='')
    
    nodes['208_mnc2mnc2'] = run.create_node(name='208_mnc2mnc2', type='node', parent=nodes['41_Files_Conversions2'], no_in_ports=0, no_out_ports=0)

    
    nodes['208_mnc2mnc2'].in_ports['in_Processed_Minc'] = Port(node=nodes['208_mnc2mnc2'], name='in_Processed_Minc', description='')
    nodes['208_mnc2mnc2'].in_ports['in_Output_Dir'] = Port(node=nodes['208_mnc2mnc2'], name='in_Output_Dir', description='')

    
    nodes['208_mnc2mnc2'].out_ports['out_output_file_mnc'] = Port(node=nodes['208_mnc2mnc2'], name='out_output_file_mnc', description='')
    
    nodes['209_mnc2mnc3'] = run.create_node(name='209_mnc2mnc3', type='node', parent=nodes['40_Files_Conversions3'], no_in_ports=0, no_out_ports=0)

    
    nodes['209_mnc2mnc3'].in_ports['in_Processed_Minc'] = Port(node=nodes['209_mnc2mnc3'], name='in_Processed_Minc', description='')
    nodes['209_mnc2mnc3'].in_ports['in_Output_Dir'] = Port(node=nodes['209_mnc2mnc3'], name='in_Output_Dir', description='')

    
    nodes['209_mnc2mnc3'].out_ports['out_output_file_mnc'] = Port(node=nodes['209_mnc2mnc3'], name='out_output_file_mnc', description='')
    
    nodes['210_mnc2mnc1'] = run.create_node(name='210_mnc2mnc1', type='node', parent=nodes['42_Files_Conversions1'], no_in_ports=0, no_out_ports=0)

    
    nodes['210_mnc2mnc1'].in_ports['in_Processed_Minc'] = Port(node=nodes['210_mnc2mnc1'], name='in_Processed_Minc', description='')
    nodes['210_mnc2mnc1'].in_ports['in_Output_Dir'] = Port(node=nodes['210_mnc2mnc1'], name='in_Output_Dir', description='')

    
    nodes['210_mnc2mnc1'].out_ports['out_output_file_mnc'] = Port(node=nodes['210_mnc2mnc1'], name='out_output_file_mnc', description='')
    
    nodes['211_mnc2mnc6'] = run.create_node(name='211_mnc2mnc6', type='node', parent=nodes['45_Files_Conversions6'], no_in_ports=0, no_out_ports=0)

    
    nodes['211_mnc2mnc6'].in_ports['in_Processed_Minc'] = Port(node=nodes['211_mnc2mnc6'], name='in_Processed_Minc', description='')
    nodes['211_mnc2mnc6'].in_ports['in_Output_Dir'] = Port(node=nodes['211_mnc2mnc6'], name='in_Output_Dir', description='')

    
    nodes['211_mnc2mnc6'].out_ports['out_output_file_mnc'] = Port(node=nodes['211_mnc2mnc6'], name='out_output_file_mnc', description='')
    
    nodes['212_mnc2mnc7'] = run.create_node(name='212_mnc2mnc7', type='node', parent=nodes['51_Files_Conversions7'], no_in_ports=0, no_out_ports=0)

    
    nodes['212_mnc2mnc7'].in_ports['in_Processed_Minc'] = Port(node=nodes['212_mnc2mnc7'], name='in_Processed_Minc', description='')
    nodes['212_mnc2mnc7'].in_ports['in_Output_Dir'] = Port(node=nodes['212_mnc2mnc7'], name='in_Output_Dir', description='')

    
    nodes['212_mnc2mnc7'].out_ports['out_output_file_mnc'] = Port(node=nodes['212_mnc2mnc7'], name='out_output_file_mnc', description='')
    
    nodes['213_mnc2mnc4'] = run.create_node(name='213_mnc2mnc4', type='node', parent=nodes['3_Files_Conversions4'], no_in_ports=0, no_out_ports=0)

    
    nodes['213_mnc2mnc4'].in_ports['in_Processed_Minc'] = Port(node=nodes['213_mnc2mnc4'], name='in_Processed_Minc', description='')
    nodes['213_mnc2mnc4'].in_ports['in_Output_Dir'] = Port(node=nodes['213_mnc2mnc4'], name='in_Output_Dir', description='')

    
    nodes['213_mnc2mnc4'].out_ports['out_output_file_mnc'] = Port(node=nodes['213_mnc2mnc4'], name='out_output_file_mnc', description='')
    
    nodes['214_mnc2mnc5'] = run.create_node(name='214_mnc2mnc5', type='node', parent=nodes['0_Files_Conversions5'], no_in_ports=0, no_out_ports=0)

    
    nodes['214_mnc2mnc5'].in_ports['in_Processed_Minc'] = Port(node=nodes['214_mnc2mnc5'], name='in_Processed_Minc', description='')
    nodes['214_mnc2mnc5'].in_ports['in_Output_Dir'] = Port(node=nodes['214_mnc2mnc5'], name='in_Output_Dir', description='')

    
    nodes['214_mnc2mnc5'].out_ports['out_output_file_mnc'] = Port(node=nodes['214_mnc2mnc5'], name='out_output_file_mnc', description='')
    
    nodes['215_Source_Name_22'] = run.create_node(name='215_Source_Name_22', type='source', parent=nodes['4_Input_Arguments22'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['215_Source_Name_22'].out_ports['out_output'] = Port(node=nodes['215_Source_Name_22'], name='out_output', description='')
    
    nodes['216_Source_Name_23'] = run.create_node(name='216_Source_Name_23', type='source', parent=nodes['5_Input_Arguments23'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['216_Source_Name_23'].out_ports['out_output'] = Port(node=nodes['216_Source_Name_23'], name='out_output', description='')
    
    nodes['217_Source_Name_20'] = run.create_node(name='217_Source_Name_20', type='source', parent=nodes['6_Input_Arguments20'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['217_Source_Name_20'].out_ports['out_output'] = Port(node=nodes['217_Source_Name_20'], name='out_output', description='')
    
    nodes['218_Source_Name_21'] = run.create_node(name='218_Source_Name_21', type='source', parent=nodes['67_Input_Arguments21'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['218_Source_Name_21'].out_ports['out_output'] = Port(node=nodes['218_Source_Name_21'], name='out_output', description='')
    
    nodes['219_Source_Name_26'] = run.create_node(name='219_Source_Name_26', type='source', parent=nodes['8_Input_Arguments26'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['219_Source_Name_26'].out_ports['out_output'] = Port(node=nodes['219_Source_Name_26'], name='out_output', description='')
    
    nodes['220_Source_Name_27'] = run.create_node(name='220_Source_Name_27', type='source', parent=nodes['9_Input_Arguments27'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['220_Source_Name_27'].out_ports['out_output'] = Port(node=nodes['220_Source_Name_27'], name='out_output', description='')
    
    nodes['221_Source_Name_24'] = run.create_node(name='221_Source_Name_24', type='source', parent=nodes['10_Input_Arguments24'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['221_Source_Name_24'].out_ports['out_output'] = Port(node=nodes['221_Source_Name_24'], name='out_output', description='')
    
    nodes['222_Source_Name_25'] = run.create_node(name='222_Source_Name_25', type='source', parent=nodes['11_Input_Arguments25'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['222_Source_Name_25'].out_ports['out_output'] = Port(node=nodes['222_Source_Name_25'], name='out_output', description='')
    
    nodes['223_Source_Name_28'] = run.create_node(name='223_Source_Name_28', type='source', parent=nodes['13_Input_Arguments28'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['223_Source_Name_28'].out_ports['out_output'] = Port(node=nodes['223_Source_Name_28'], name='out_output', description='')
    
    nodes['224_Source_Name_29'] = run.create_node(name='224_Source_Name_29', type='source', parent=nodes['14_Input_Arguments29'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['224_Source_Name_29'].out_ports['out_output'] = Port(node=nodes['224_Source_Name_29'], name='out_output', description='')
    
    nodes['225_nii2mnc21'] = run.create_node(name='225_nii2mnc21', type='node', parent=nodes['21_Files_Conversions21'], no_in_ports=0, no_out_ports=0)

    
    nodes['225_nii2mnc21'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['225_nii2mnc21'], name='in_Processed_NIFTII', description='')
    nodes['225_nii2mnc21'].in_ports['in_Output_File_Name'] = Port(node=nodes['225_nii2mnc21'], name='in_Output_File_Name', description='')

    
    nodes['225_nii2mnc21'].out_ports['out_output_file'] = Port(node=nodes['225_nii2mnc21'], name='out_output_file', description='')
    
    nodes['226_nii2mnc20'] = run.create_node(name='226_nii2mnc20', type='node', parent=nodes['73_Files_Conversions20'], no_in_ports=0, no_out_ports=0)

    
    nodes['226_nii2mnc20'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['226_nii2mnc20'], name='in_Processed_NIFTII', description='')
    nodes['226_nii2mnc20'].in_ports['in_Output_File_Name'] = Port(node=nodes['226_nii2mnc20'], name='in_Output_File_Name', description='')

    
    nodes['226_nii2mnc20'].out_ports['out_output_file'] = Port(node=nodes['226_nii2mnc20'], name='out_output_file', description='')
    
    nodes['227_nii2mnc23'] = run.create_node(name='227_nii2mnc23', type='node', parent=nodes['69_Files_Conversions23'], no_in_ports=0, no_out_ports=0)

    
    nodes['227_nii2mnc23'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['227_nii2mnc23'], name='in_Processed_NIFTII', description='')
    nodes['227_nii2mnc23'].in_ports['in_Output_File_Name'] = Port(node=nodes['227_nii2mnc23'], name='in_Output_File_Name', description='')

    
    nodes['227_nii2mnc23'].out_ports['out_output_file'] = Port(node=nodes['227_nii2mnc23'], name='out_output_file', description='')
    
    nodes['228_nii2mnc22'] = run.create_node(name='228_nii2mnc22', type='node', parent=nodes['18_Files_Conversions22'], no_in_ports=0, no_out_ports=0)

    
    nodes['228_nii2mnc22'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['228_nii2mnc22'], name='in_Processed_NIFTII', description='')
    nodes['228_nii2mnc22'].in_ports['in_Output_File_Name'] = Port(node=nodes['228_nii2mnc22'], name='in_Output_File_Name', description='')

    
    nodes['228_nii2mnc22'].out_ports['out_output_file'] = Port(node=nodes['228_nii2mnc22'], name='out_output_file', description='')
    
    nodes['229_mnc2mnc14'] = run.create_node(name='229_mnc2mnc14', type='node', parent=nodes['61_Files_Conversions14'], no_in_ports=0, no_out_ports=0)

    
    nodes['229_mnc2mnc14'].in_ports['in_Processed_Minc'] = Port(node=nodes['229_mnc2mnc14'], name='in_Processed_Minc', description='')
    nodes['229_mnc2mnc14'].in_ports['in_Output_Dir'] = Port(node=nodes['229_mnc2mnc14'], name='in_Output_Dir', description='')

    
    nodes['229_mnc2mnc14'].out_ports['out_output_file_mnc'] = Port(node=nodes['229_mnc2mnc14'], name='out_output_file_mnc', description='')
    
    nodes['230_nii2mnc24'] = run.create_node(name='230_nii2mnc24', type='node', parent=nodes['24_Files_Conversions24'], no_in_ports=0, no_out_ports=0)

    
    nodes['230_nii2mnc24'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['230_nii2mnc24'], name='in_Processed_NIFTII', description='')
    nodes['230_nii2mnc24'].in_ports['in_Output_File_Name'] = Port(node=nodes['230_nii2mnc24'], name='in_Output_File_Name', description='')

    
    nodes['230_nii2mnc24'].out_ports['out_output_file'] = Port(node=nodes['230_nii2mnc24'], name='out_output_file', description='')
    
    nodes['231_nii2mnc27'] = run.create_node(name='231_nii2mnc27', type='node', parent=nodes['23_Files_Conversions27'], no_in_ports=0, no_out_ports=0)

    
    nodes['231_nii2mnc27'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['231_nii2mnc27'], name='in_Processed_NIFTII', description='')
    nodes['231_nii2mnc27'].in_ports['in_Output_File_Name'] = Port(node=nodes['231_nii2mnc27'], name='in_Output_File_Name', description='')

    
    nodes['231_nii2mnc27'].out_ports['out_output_file'] = Port(node=nodes['231_nii2mnc27'], name='out_output_file', description='')
    
    nodes['232_nii2mnc26'] = run.create_node(name='232_nii2mnc26', type='node', parent=nodes['22_Files_Conversions26'], no_in_ports=0, no_out_ports=0)

    
    nodes['232_nii2mnc26'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['232_nii2mnc26'], name='in_Processed_NIFTII', description='')
    nodes['232_nii2mnc26'].in_ports['in_Output_File_Name'] = Port(node=nodes['232_nii2mnc26'], name='in_Output_File_Name', description='')

    
    nodes['232_nii2mnc26'].out_ports['out_output_file'] = Port(node=nodes['232_nii2mnc26'], name='out_output_file', description='')
    
    nodes['233_nii2mnc29'] = run.create_node(name='233_nii2mnc29', type='node', parent=nodes['16_Files_Conversions29'], no_in_ports=0, no_out_ports=0)

    
    nodes['233_nii2mnc29'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['233_nii2mnc29'], name='in_Processed_NIFTII', description='')
    nodes['233_nii2mnc29'].in_ports['in_Output_File_Name'] = Port(node=nodes['233_nii2mnc29'], name='in_Output_File_Name', description='')

    
    nodes['233_nii2mnc29'].out_ports['out_output_file'] = Port(node=nodes['233_nii2mnc29'], name='out_output_file', description='')
    
    nodes['234_nii2mnc28'] = run.create_node(name='234_nii2mnc28', type='node', parent=nodes['15_Files_Conversions28'], no_in_ports=0, no_out_ports=0)

    
    nodes['234_nii2mnc28'].in_ports['in_Processed_NIFTII'] = Port(node=nodes['234_nii2mnc28'], name='in_Processed_NIFTII', description='')
    nodes['234_nii2mnc28'].in_ports['in_Output_File_Name'] = Port(node=nodes['234_nii2mnc28'], name='in_Output_File_Name', description='')

    
    nodes['234_nii2mnc28'].out_ports['out_output_file'] = Port(node=nodes['234_nii2mnc28'], name='out_output_file', description='')
    
    nodes['235_Source_Name_2'] = run.create_node(name='235_Source_Name_2', type='source', parent=nodes['30_Input_Arguments2'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['235_Source_Name_2'].out_ports['out_output'] = Port(node=nodes['235_Source_Name_2'], name='out_output', description='')
    

    # Links
    run.links.append(Link(from_node=nodes['105_Source_Name_12'], to_node=nodes['116_nii2mnc12'], data_type='String'))
    run.links.append(Link(from_node=nodes['116_nii2mnc12'], to_node=nodes['166_mnc2mnc12'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['107_Source_Name_10'], to_node=nodes['114_nii2mnc10'], data_type='String'))
    run.links.append(Link(from_node=nodes['114_nii2mnc10'], to_node=nodes['140_mnc2mnc10'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['140_mnc2mnc10'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['92_Source_11'], to_node=nodes['115_nii2mnc11'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['106_Source_Name_11'], to_node=nodes['115_nii2mnc11'], data_type='String'))
    run.links.append(Link(from_node=nodes['115_nii2mnc11'], to_node=nodes['99_mnc2mnc11'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['99_mnc2mnc11'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['93_Source_12'], to_node=nodes['116_nii2mnc12'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['130_mnc2mnc13'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['166_mnc2mnc12'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['172_mnc2mnc15'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['229_mnc2mnc14'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['175_mnc2mnc17'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['173_mnc2mnc16'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['178_mnc2mnc19'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['176_mnc2mnc18'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['146_mnc2mnc21'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['147_mnc2mnc20'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['133_IntracranialVolume'], to_node=nodes['167_ICV_Output_ICV_Parameters'], data_type='TxtFile'))
    run.links.append(Link(from_node=nodes['133_IntracranialVolume'], to_node=nodes['155_ICV_Output_ICV_Mask'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['195_Average_Brain_Model'], to_node=nodes['131_ABM_Output_Parameters'], data_type='TxtFile'))
    run.links.append(Link(from_node=nodes['195_Average_Brain_Model'], to_node=nodes['171_ABM_Output_Brain'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['195_Average_Brain_Model'], to_node=nodes['76_ABM_Output_JPG'], data_type='JPGFile'))
    run.links.append(Link(from_node=nodes['195_Average_Brain_Model'], to_node=nodes['168_ABM_Output_Log'], data_type='TxtFile'))
    run.links.append(Link(from_node=nodes['100_ICV_Mask_To_Binary_Min'], to_node=nodes['133_IntracranialVolume'], data_type='Float'))
    run.links.append(Link(from_node=nodes['139_ICV_Non_linear'], to_node=nodes['133_IntracranialVolume'], data_type='String'))
    run.links.append(Link(from_node=nodes['84_ABM_Name'], to_node=nodes['133_IntracranialVolume'], data_type='String'))
    run.links.append(Link(from_node=nodes['95_Calc_Crude'], to_node=nodes['133_IntracranialVolume'], data_type='Boolean'))
    run.links.append(Link(from_node=nodes['174_Collapse_Minc_To_Folder'], to_node=nodes['195_Average_Brain_Model'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['153_mnc2mnc26'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['152_mnc2mnc27'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['144_mnc2mnc28'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['143_mnc2mnc29'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['149_mnc2mnc22'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['148_mnc2mnc23'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['151_mnc2mnc24'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['150_mnc2mnc25'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['109_Source_Name_16'], to_node=nodes['120_nii2mnc16'], data_type='String'))
    run.links.append(Link(from_node=nodes['120_nii2mnc16'], to_node=nodes['173_mnc2mnc16'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['173_mnc2mnc16'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['90_Source_17'], to_node=nodes['121_nii2mnc17'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['110_Source_Name_15'], to_node=nodes['119_nii2mnc15'], data_type='String'))
    run.links.append(Link(from_node=nodes['119_nii2mnc15'], to_node=nodes['172_mnc2mnc15'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['172_mnc2mnc15'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['89_Source_16'], to_node=nodes['120_nii2mnc16'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['108_Source_Name_17'], to_node=nodes['121_nii2mnc17'], data_type='String'))
    run.links.append(Link(from_node=nodes['121_nii2mnc17'], to_node=nodes['175_mnc2mnc17'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['98_ICV_Debug'], to_node=nodes['133_IntracranialVolume'], data_type='Boolean'))
    run.links.append(Link(from_node=nodes['126_ICV_Clobber'], to_node=nodes['133_IntracranialVolume'], data_type='Boolean'))
    run.links.append(Link(from_node=nodes['194_ICV_Step_Size'], to_node=nodes['133_IntracranialVolume'], data_type='String'))
    run.links.append(Link(from_node=nodes['142_ICV_Blur_FWHM'], to_node=nodes['133_IntracranialVolume'], data_type='String'))
    run.links.append(Link(from_node=nodes['137_ICV_Registration_Blur_FWHM'], to_node=nodes['133_IntracranialVolume'], data_type='Int'))
    run.links.append(Link(from_node=nodes['184_ICV_Num_Of_Iterations'], to_node=nodes['133_IntracranialVolume'], data_type='String'))
    run.links.append(Link(from_node=nodes['169_ICV_Similarity'], to_node=nodes['133_IntracranialVolume'], data_type='Float'))
    run.links.append(Link(from_node=nodes['129_ICV_Sub_lattice'], to_node=nodes['133_IntracranialVolume'], data_type='Int'))
    run.links.append(Link(from_node=nodes['123_ICV_Weight'], to_node=nodes['133_IntracranialVolume'], data_type='Float'))
    run.links.append(Link(from_node=nodes['193_ICV_Stiffness'], to_node=nodes['133_IntracranialVolume'], data_type='Float'))
    run.links.append(Link(from_node=nodes['113_nii2mnc19'], to_node=nodes['178_mnc2mnc19'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['102_Source_Name_19'], to_node=nodes['113_nii2mnc19'], data_type='String'))
    run.links.append(Link(from_node=nodes['97_Source_19'], to_node=nodes['113_nii2mnc19'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['176_mnc2mnc18'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['112_nii2mnc18'], to_node=nodes['176_mnc2mnc18'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['103_Source_Name_18'], to_node=nodes['112_nii2mnc18'], data_type='String'))
    run.links.append(Link(from_node=nodes['96_Source_18'], to_node=nodes['112_nii2mnc18'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['175_mnc2mnc17'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['190_Source_20'], to_node=nodes['226_nii2mnc20'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['178_mnc2mnc19'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['80_Source_5'], to_node=nodes['163_nii2mnc5'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['213_mnc2mnc4'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['162_nii2mnc4'], to_node=nodes['213_mnc2mnc4'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['204_Source_Name_4'], to_node=nodes['162_nii2mnc4'], data_type='String'))
    run.links.append(Link(from_node=nodes['79_Source_4'], to_node=nodes['162_nii2mnc4'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['209_mnc2mnc3'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['158_nii2mnc3'], to_node=nodes['209_mnc2mnc3'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['198_Source_Name_3'], to_node=nodes['158_nii2mnc3'], data_type='String'))
    run.links.append(Link(from_node=nodes['82_Source_3'], to_node=nodes['158_nii2mnc3'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['208_mnc2mnc2'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['215_Source_Name_22'], to_node=nodes['228_nii2mnc22'], data_type='String'))
    run.links.append(Link(from_node=nodes['228_nii2mnc22'], to_node=nodes['149_mnc2mnc22'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['147_mnc2mnc20'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['189_Source_21'], to_node=nodes['225_nii2mnc21'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['217_Source_Name_20'], to_node=nodes['226_nii2mnc20'], data_type='String'))
    run.links.append(Link(from_node=nodes['226_nii2mnc20'], to_node=nodes['147_mnc2mnc20'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['146_mnc2mnc21'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['192_Source_22'], to_node=nodes['228_nii2mnc22'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['218_Source_Name_21'], to_node=nodes['225_nii2mnc21'], data_type='String'))
    run.links.append(Link(from_node=nodes['225_nii2mnc21'], to_node=nodes['146_mnc2mnc21'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['124_ABM_Sub_lattice'], to_node=nodes['195_Average_Brain_Model'], data_type='Int'))
    run.links.append(Link(from_node=nodes['180_ABM_Similarity'], to_node=nodes['195_Average_Brain_Model'], data_type='Float'))
    run.links.append(Link(from_node=nodes['122_ABM_Num_Of_Iterations'], to_node=nodes['195_Average_Brain_Model'], data_type='String'))
    run.links.append(Link(from_node=nodes['181_ABM_Registration_Blur_FWHM'], to_node=nodes['195_Average_Brain_Model'], data_type='Int'))
    run.links.append(Link(from_node=nodes['127_ABM_Num_Of_Registrations'], to_node=nodes['195_Average_Brain_Model'], data_type='Int'))
    run.links.append(Link(from_node=nodes['179_ABM_Linear_Estimate'], to_node=nodes['195_Average_Brain_Model'], data_type='String'))
    run.links.append(Link(from_node=nodes['134_ABM_Stiffness'], to_node=nodes['195_Average_Brain_Model'], data_type='Float'))
    run.links.append(Link(from_node=nodes['145_ABM_Weight'], to_node=nodes['195_Average_Brain_Model'], data_type='Float'))
    run.links.append(Link(from_node=nodes['138_ABM_Blur_FWHM'], to_node=nodes['195_Average_Brain_Model'], data_type='String'))
    run.links.append(Link(from_node=nodes['77_ABM_Step_Size'], to_node=nodes['195_Average_Brain_Model'], data_type='String'))
    run.links.append(Link(from_node=nodes['235_Source_Name_2'], to_node=nodes['157_nii2mnc2'], data_type='String'))
    run.links.append(Link(from_node=nodes['157_nii2mnc2'], to_node=nodes['208_mnc2mnc2'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['200_Source_Name_1'], to_node=nodes['159_nii2mnc1'], data_type='String'))
    run.links.append(Link(from_node=nodes['159_nii2mnc1'], to_node=nodes['210_mnc2mnc1'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['210_mnc2mnc1'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['81_Source_2'], to_node=nodes['157_nii2mnc2'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['133_IntracranialVolume'], to_node=nodes['136_ICV_Output_ICV_Volume'], data_type='TxtFile'))
    run.links.append(Link(from_node=nodes['133_IntracranialVolume'], to_node=nodes['132_ICV_Output_Log'], data_type='TxtFile'))
    run.links.append(Link(from_node=nodes['133_IntracranialVolume'], to_node=nodes['125_ICV_Output_ICV_JPG'], data_type='JPGFile'))
    run.links.append(Link(from_node=nodes['83_Source_1'], to_node=nodes['159_nii2mnc1'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['185_Source_25'], to_node=nodes['170_nii2mnc25'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['151_mnc2mnc24'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['186_Source_24'], to_node=nodes['230_nii2mnc24'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['148_mnc2mnc23'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['230_nii2mnc24'], to_node=nodes['151_mnc2mnc24'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['221_Source_Name_24'], to_node=nodes['230_nii2mnc24'], data_type='String'))
    run.links.append(Link(from_node=nodes['191_Source_23'], to_node=nodes['227_nii2mnc23'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['149_mnc2mnc22'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['227_nii2mnc23'], to_node=nodes['148_mnc2mnc23'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['216_Source_Name_23'], to_node=nodes['227_nii2mnc23'], data_type='String'))
    run.links.append(Link(from_node=nodes['91_Source_10'], to_node=nodes['114_nii2mnc10'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['207_mnc2mnc9'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['85_Source_8'], to_node=nodes['164_nii2mnc8'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['212_mnc2mnc7'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['164_nii2mnc8'], to_node=nodes['206_mnc2mnc8'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['197_Source_Name_8'], to_node=nodes['164_nii2mnc8'], data_type='String'))
    run.links.append(Link(from_node=nodes['86_Source_9'], to_node=nodes['165_nii2mnc9'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['206_mnc2mnc8'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['165_nii2mnc9'], to_node=nodes['207_mnc2mnc9'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['196_Source_Name_9'], to_node=nodes['165_nii2mnc9'], data_type='String'))
    run.links.append(Link(from_node=nodes['220_Source_Name_27'], to_node=nodes['231_nii2mnc27'], data_type='String'))
    run.links.append(Link(from_node=nodes['231_nii2mnc27'], to_node=nodes['152_mnc2mnc27'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['222_Source_Name_25'], to_node=nodes['170_nii2mnc25'], data_type='String'))
    run.links.append(Link(from_node=nodes['170_nii2mnc25'], to_node=nodes['150_mnc2mnc25'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['150_mnc2mnc25'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['188_Source_26'], to_node=nodes['232_nii2mnc26'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['219_Source_Name_26'], to_node=nodes['232_nii2mnc26'], data_type='String'))
    run.links.append(Link(from_node=nodes['232_nii2mnc26'], to_node=nodes['153_mnc2mnc26'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['153_mnc2mnc26'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['187_Source_27'], to_node=nodes['231_nii2mnc27'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['211_mnc2mnc6'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['78_Source_7'], to_node=nodes['161_nii2mnc7'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['202_Source_Name_6'], to_node=nodes['160_nii2mnc6'], data_type='String'))
    run.links.append(Link(from_node=nodes['160_nii2mnc6'], to_node=nodes['211_mnc2mnc6'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['214_mnc2mnc5'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['135_Source_6'], to_node=nodes['160_nii2mnc6'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['203_Source_Name_5'], to_node=nodes['163_nii2mnc5'], data_type='String'))
    run.links.append(Link(from_node=nodes['163_nii2mnc5'], to_node=nodes['214_mnc2mnc5'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['201_Source_Name_7'], to_node=nodes['161_nii2mnc7'], data_type='String'))
    run.links.append(Link(from_node=nodes['161_nii2mnc7'], to_node=nodes['212_mnc2mnc7'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['210_mnc2mnc1'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['143_mnc2mnc29'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['234_nii2mnc28'], to_node=nodes['144_mnc2mnc28'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['223_Source_Name_28'], to_node=nodes['234_nii2mnc28'], data_type='String'))
    run.links.append(Link(from_node=nodes['183_Source_28'], to_node=nodes['234_nii2mnc28'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['152_mnc2mnc27'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['233_nii2mnc29'], to_node=nodes['143_mnc2mnc29'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['224_Source_Name_29'], to_node=nodes['233_nii2mnc29'], data_type='String'))
    run.links.append(Link(from_node=nodes['182_Source_29'], to_node=nodes['233_nii2mnc29'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['144_mnc2mnc28'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['84_ABM_Name'], to_node=nodes['195_Average_Brain_Model'], data_type='String'))
    run.links.append(Link(from_node=nodes['177_ABM_Non_linear'], to_node=nodes['195_Average_Brain_Model'], data_type='String'))
    run.links.append(Link(from_node=nodes['199_ABM_Clobber'], to_node=nodes['195_Average_Brain_Model'], data_type='Boolean'))
    run.links.append(Link(from_node=nodes['128_ABM_Debug'], to_node=nodes['195_Average_Brain_Model'], data_type='Boolean'))
    run.links.append(Link(from_node=nodes['195_Average_Brain_Model'], to_node=nodes['133_IntracranialVolume'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['104_ICV_Model_Name'], to_node=nodes['133_IntracranialVolume'], data_type='String'))
    run.links.append(Link(from_node=nodes['156_ABM_First_Subject'], to_node=nodes['195_Average_Brain_Model'], data_type='String'))
    run.links.append(Link(from_node=nodes['205_ABM_Clac_Ave_Brain'], to_node=nodes['195_Average_Brain_Model'], data_type='Boolean'))
    run.links.append(Link(from_node=nodes['75_ICV_Num_Of_Registrations'], to_node=nodes['133_IntracranialVolume'], data_type='Int'))
    run.links.append(Link(from_node=nodes['101_ICV_Linear_Estimate'], to_node=nodes['133_IntracranialVolume'], data_type='String'))
    run.links.append(Link(from_node=nodes['88_Source_15'], to_node=nodes['119_nii2mnc15'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['229_mnc2mnc14'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['117_nii2mnc13'], to_node=nodes['130_mnc2mnc13'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['154_Source_Name_13'], to_node=nodes['117_nii2mnc13'], data_type='String'))
    run.links.append(Link(from_node=nodes['94_Source_13'], to_node=nodes['117_nii2mnc13'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['166_mnc2mnc12'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['118_nii2mnc14'], to_node=nodes['229_mnc2mnc14'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['111_Source_Name_14'], to_node=nodes['118_nii2mnc14'], data_type='String'))
    run.links.append(Link(from_node=nodes['87_Source_14'], to_node=nodes['118_nii2mnc14'], data_type='NiftiImageFileUncompressed'))
    run.links.append(Link(from_node=nodes['141_Input_Folder'], to_node=nodes['130_mnc2mnc13'], data_type='Directory'))
    run.links.append(Link(from_node=nodes['140_mnc2mnc10'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['99_mnc2mnc11'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['206_mnc2mnc8'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['207_mnc2mnc9'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['211_mnc2mnc6'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['212_mnc2mnc7'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['213_mnc2mnc4'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['214_mnc2mnc5'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['208_mnc2mnc2'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))
    run.links.append(Link(from_node=nodes['209_mnc2mnc3'], to_node=nodes['174_Collapse_Minc_To_Folder'], data_type='MincImageFile'))

    return run

