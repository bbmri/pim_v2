import os

from adapter.link import Link
from adapter.run import Run
from adapter.port import Port


def create_run(**kwargs):
    run = Run()
    run.name = 'iris'

    os.environ['RUN_NAME'] = run.name

    nodes = dict()

    nodes['root'] = run.create_node(name='root', parent=None)

    run.root = nodes['root']

    
    nodes['0_statistics'] = run.create_node(name='0_statistics', description='Leaf', parent=nodes['root'])
    nodes['1_dicom_to_nifti'] = run.create_node(name='1_dicom_to_nifti', description='Leaf', parent=nodes['root'])
    nodes['2_hammers_atlas'] = run.create_node(name='2_hammers_atlas', description='Leaf', parent=nodes['root'])
    nodes['3_subjects'] = run.create_node(name='3_subjects', description='Leaf', parent=nodes['root'])
    nodes['4_output'] = run.create_node(name='4_output', description='Leaf', parent=nodes['root'])
    nodes['5_BET_and_REG'] = run.create_node(name='5_BET_and_REG', description='Leaf', parent=nodes['root'])
    nodes['6_qc_overview'] = run.create_node(name='6_qc_overview', description='Leaf', parent=nodes['root'])
    nodes['65_spm_tissue_quantification'] = run.create_node(name='65_spm_tissue_quantification', description='Leaf', parent=nodes['root'])
    nodes['66_InputImages'] = run.create_node(name='66_InputImages', description='Leaf', parent=nodes['65_spm_tissue_quantification'])
    nodes['67_InputImage'] = run.create_node(name='67_InputImage', description='Leaf', parent=nodes['65_spm_tissue_quantification'])
    nodes['68_SPM_Tissue_Segmentation'] = run.create_node(name='68_SPM_Tissue_Segmentation', description='Leaf', parent=nodes['65_spm_tissue_quantification'])
    nodes['69_outputs'] = run.create_node(name='69_outputs', description='Leaf', parent=nodes['65_spm_tissue_quantification'])

    
    nodes['7_threshold_labels'] = run.create_node(name='7_threshold_labels', type='node', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    
    nodes['7_threshold_labels'].in_ports['in_image'] = Port(node=nodes['7_threshold_labels'], name='in_image', description='')
    nodes['7_threshold_labels'].in_ports['in_method'] = Port(node=nodes['7_threshold_labels'], name='in_method', description='')
    nodes['7_threshold_labels'].in_ports['in_mask'] = Port(node=nodes['7_threshold_labels'], name='in_mask', description='')
    nodes['7_threshold_labels'].in_ports['in_lower_threshold'] = Port(node=nodes['7_threshold_labels'], name='in_lower_threshold', description='')
    nodes['7_threshold_labels'].in_ports['in_upper_threshold'] = Port(node=nodes['7_threshold_labels'], name='in_upper_threshold', description='')
    nodes['7_threshold_labels'].in_ports['in_inside_value'] = Port(node=nodes['7_threshold_labels'], name='in_inside_value', description='')
    nodes['7_threshold_labels'].in_ports['in_outside_value'] = Port(node=nodes['7_threshold_labels'], name='in_outside_value', description='')
    nodes['7_threshold_labels'].in_ports['in_number_of_thresholds'] = Port(node=nodes['7_threshold_labels'], name='in_number_of_thresholds', description='')
    nodes['7_threshold_labels'].in_ports['in_number_of_histbins'] = Port(node=nodes['7_threshold_labels'], name='in_number_of_histbins', description='')
    nodes['7_threshold_labels'].in_ports['in_radius'] = Port(node=nodes['7_threshold_labels'], name='in_radius', description='')
    nodes['7_threshold_labels'].in_ports['in_number_of_controlpoints'] = Port(node=nodes['7_threshold_labels'], name='in_number_of_controlpoints', description='')
    nodes['7_threshold_labels'].in_ports['in_number_of_levels'] = Port(node=nodes['7_threshold_labels'], name='in_number_of_levels', description='')
    nodes['7_threshold_labels'].in_ports['in_number_of_samples'] = Port(node=nodes['7_threshold_labels'], name='in_number_of_samples', description='')
    nodes['7_threshold_labels'].in_ports['in_spline_order'] = Port(node=nodes['7_threshold_labels'], name='in_spline_order', description='')
    nodes['7_threshold_labels'].in_ports['in_power'] = Port(node=nodes['7_threshold_labels'], name='in_power', description='')
    nodes['7_threshold_labels'].in_ports['in_sigma'] = Port(node=nodes['7_threshold_labels'], name='in_sigma', description='')
    nodes['7_threshold_labels'].in_ports['in_number_of_iterations'] = Port(node=nodes['7_threshold_labels'], name='in_number_of_iterations', description='')
    nodes['7_threshold_labels'].in_ports['in_mask_value'] = Port(node=nodes['7_threshold_labels'], name='in_mask_value', description='')
    nodes['7_threshold_labels'].in_ports['in_mixture_type'] = Port(node=nodes['7_threshold_labels'], name='in_mixture_type', description='')
    nodes['7_threshold_labels'].in_ports['in_compression_flag'] = Port(node=nodes['7_threshold_labels'], name='in_compression_flag', description='')

    
    nodes['7_threshold_labels'].out_ports['out_image'] = Port(node=nodes['7_threshold_labels'], name='out_image', description='')
    
    nodes['8_hammers_t1w'] = run.create_node(name='8_hammers_t1w', type='source', parent=nodes['2_hammers_atlas'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['8_hammers_t1w'].out_ports['out_output'] = Port(node=nodes['8_hammers_t1w'], name='out_output', description='')
    
    nodes['9_const_get_gm_volume_flatten_0'] = run.create_node(name='9_const_get_gm_volume_flatten_0', type='constant', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['9_const_get_gm_volume_flatten_0'].out_ports['out_output'] = Port(node=nodes['9_const_get_gm_volume_flatten_0'], name='out_output', description='')
    
    nodes['10_const_bet_fraction_threshold_0'] = run.create_node(name='10_const_bet_fraction_threshold_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['10_const_bet_fraction_threshold_0'].out_ports['out_output'] = Port(node=nodes['10_const_bet_fraction_threshold_0'], name='out_output', description='')
    
    nodes['11_get_gm_volume'] = run.create_node(name='11_get_gm_volume', type='node', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    
    nodes['11_get_gm_volume'].in_ports['in_label'] = Port(node=nodes['11_get_gm_volume'], name='in_label', description='')
    nodes['11_get_gm_volume'].in_ports['in_image'] = Port(node=nodes['11_get_gm_volume'], name='in_image', description='')
    nodes['11_get_gm_volume'].in_ports['in_mask'] = Port(node=nodes['11_get_gm_volume'], name='in_mask', description='')
    nodes['11_get_gm_volume'].in_ports['in_volume'] = Port(node=nodes['11_get_gm_volume'], name='in_volume', description='')
    nodes['11_get_gm_volume'].in_ports['in_stddev'] = Port(node=nodes['11_get_gm_volume'], name='in_stddev', description='')
    nodes['11_get_gm_volume'].in_ports['in_mean'] = Port(node=nodes['11_get_gm_volume'], name='in_mean', description='')
    nodes['11_get_gm_volume'].in_ports['in_selection'] = Port(node=nodes['11_get_gm_volume'], name='in_selection', description='')
    nodes['11_get_gm_volume'].in_ports['in_ddof'] = Port(node=nodes['11_get_gm_volume'], name='in_ddof', description='')
    nodes['11_get_gm_volume'].in_ports['in_flatten'] = Port(node=nodes['11_get_gm_volume'], name='in_flatten', description='')

    
    nodes['11_get_gm_volume'].out_ports['out_statistics'] = Port(node=nodes['11_get_gm_volume'], name='out_statistics', description='')
    
    nodes['12_t1w_registration'] = run.create_node(name='12_t1w_registration', type='node', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    
    nodes['12_t1w_registration'].in_ports['in_fixed_image'] = Port(node=nodes['12_t1w_registration'], name='in_fixed_image', description='')
    nodes['12_t1w_registration'].in_ports['in_moving_image'] = Port(node=nodes['12_t1w_registration'], name='in_moving_image', description='')
    nodes['12_t1w_registration'].in_ports['in_parameters'] = Port(node=nodes['12_t1w_registration'], name='in_parameters', description='')
    nodes['12_t1w_registration'].in_ports['in_fixed_mask'] = Port(node=nodes['12_t1w_registration'], name='in_fixed_mask', description='')
    nodes['12_t1w_registration'].in_ports['in_moving_mask'] = Port(node=nodes['12_t1w_registration'], name='in_moving_mask', description='')
    nodes['12_t1w_registration'].in_ports['in_initial_transform'] = Port(node=nodes['12_t1w_registration'], name='in_initial_transform', description='')
    nodes['12_t1w_registration'].in_ports['in_priority'] = Port(node=nodes['12_t1w_registration'], name='in_priority', description='')
    nodes['12_t1w_registration'].in_ports['in_threads'] = Port(node=nodes['12_t1w_registration'], name='in_threads', description='')

    
    nodes['12_t1w_registration'].out_ports['out_directory'] = Port(node=nodes['12_t1w_registration'], name='out_directory', description='')
    nodes['12_t1w_registration'].out_ports['out_transform'] = Port(node=nodes['12_t1w_registration'], name='out_transform', description='')
    nodes['12_t1w_registration'].out_ports['out_log_file'] = Port(node=nodes['12_t1w_registration'], name='out_log_file', description='')
    
    nodes['13_const_threshold_labels_upper_threshold_0'] = run.create_node(name='13_const_threshold_labels_upper_threshold_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['13_const_threshold_labels_upper_threshold_0'].out_ports['out_output'] = Port(node=nodes['13_const_threshold_labels_upper_threshold_0'], name='out_output', description='')
    
    nodes['14_get_brain_volume'] = run.create_node(name='14_get_brain_volume', type='node', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    
    nodes['14_get_brain_volume'].in_ports['in_label'] = Port(node=nodes['14_get_brain_volume'], name='in_label', description='')
    nodes['14_get_brain_volume'].in_ports['in_image'] = Port(node=nodes['14_get_brain_volume'], name='in_image', description='')
    nodes['14_get_brain_volume'].in_ports['in_mask'] = Port(node=nodes['14_get_brain_volume'], name='in_mask', description='')
    nodes['14_get_brain_volume'].in_ports['in_volume'] = Port(node=nodes['14_get_brain_volume'], name='in_volume', description='')
    nodes['14_get_brain_volume'].in_ports['in_stddev'] = Port(node=nodes['14_get_brain_volume'], name='in_stddev', description='')
    nodes['14_get_brain_volume'].in_ports['in_mean'] = Port(node=nodes['14_get_brain_volume'], name='in_mean', description='')
    nodes['14_get_brain_volume'].in_ports['in_selection'] = Port(node=nodes['14_get_brain_volume'], name='in_selection', description='')
    nodes['14_get_brain_volume'].in_ports['in_ddof'] = Port(node=nodes['14_get_brain_volume'], name='in_ddof', description='')
    nodes['14_get_brain_volume'].in_ports['in_flatten'] = Port(node=nodes['14_get_brain_volume'], name='in_flatten', description='')

    
    nodes['14_get_brain_volume'].out_ports['out_statistics'] = Port(node=nodes['14_get_brain_volume'], name='out_statistics', description='')
    
    nodes['15_const_bet_bias_cleanup_0'] = run.create_node(name='15_const_bet_bias_cleanup_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['15_const_bet_bias_cleanup_0'].out_ports['out_output'] = Port(node=nodes['15_const_bet_bias_cleanup_0'], name='out_output', description='')
    
    nodes['16_wm_map_output'] = run.create_node(name='16_wm_map_output', type='sink', parent=nodes['4_output'], no_in_ports=0, no_out_ports=0)

    
    nodes['16_wm_map_output'].in_ports['in_input'] = Port(node=nodes['16_wm_map_output'], name='in_input', description='')

    
    
    nodes['17_hammers_brain_transform'] = run.create_node(name='17_hammers_brain_transform', type='node', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    
    nodes['17_hammers_brain_transform'].in_ports['in_transform'] = Port(node=nodes['17_hammers_brain_transform'], name='in_transform', description='')
    nodes['17_hammers_brain_transform'].in_ports['in_image'] = Port(node=nodes['17_hammers_brain_transform'], name='in_image', description='')
    nodes['17_hammers_brain_transform'].in_ports['in_points'] = Port(node=nodes['17_hammers_brain_transform'], name='in_points', description='')
    nodes['17_hammers_brain_transform'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['17_hammers_brain_transform'], name='in_determinant_of_jacobian_flag', description='')
    nodes['17_hammers_brain_transform'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['17_hammers_brain_transform'], name='in_jacobian_matrix_flag', description='')
    nodes['17_hammers_brain_transform'].in_ports['in_priority'] = Port(node=nodes['17_hammers_brain_transform'], name='in_priority', description='')
    nodes['17_hammers_brain_transform'].in_ports['in_threads'] = Port(node=nodes['17_hammers_brain_transform'], name='in_threads', description='')

    
    nodes['17_hammers_brain_transform'].out_ports['out_directory'] = Port(node=nodes['17_hammers_brain_transform'], name='out_directory', description='')
    nodes['17_hammers_brain_transform'].out_ports['out_image'] = Port(node=nodes['17_hammers_brain_transform'], name='out_image', description='')
    nodes['17_hammers_brain_transform'].out_ports['out_points'] = Port(node=nodes['17_hammers_brain_transform'], name='out_points', description='')
    nodes['17_hammers_brain_transform'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['17_hammers_brain_transform'], name='out_determinant_of_jacobian', description='')
    nodes['17_hammers_brain_transform'].out_ports['out_jacobian_matrix'] = Port(node=nodes['17_hammers_brain_transform'], name='out_jacobian_matrix', description='')
    nodes['17_hammers_brain_transform'].out_ports['out_log_file'] = Port(node=nodes['17_hammers_brain_transform'], name='out_log_file', description='')
    
    nodes['18_const_get_wm_volume_flatten_0'] = run.create_node(name='18_const_get_wm_volume_flatten_0', type='constant', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['18_const_get_wm_volume_flatten_0'].out_ports['out_output'] = Port(node=nodes['18_const_get_wm_volume_flatten_0'], name='out_output', description='')
    
    nodes['19_mask_hammers_labels'] = run.create_node(name='19_mask_hammers_labels', type='node', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    
    nodes['19_mask_hammers_labels'].in_ports['in_left_hand'] = Port(node=nodes['19_mask_hammers_labels'], name='in_left_hand', description='')
    nodes['19_mask_hammers_labels'].in_ports['in_right_hand'] = Port(node=nodes['19_mask_hammers_labels'], name='in_right_hand', description='')
    nodes['19_mask_hammers_labels'].in_ports['in_operator'] = Port(node=nodes['19_mask_hammers_labels'], name='in_operator', description='')

    
    nodes['19_mask_hammers_labels'].out_ports['out_result'] = Port(node=nodes['19_mask_hammers_labels'], name='out_result', description='')
    
    nodes['20_t1_dicom_to_nifti'] = run.create_node(name='20_t1_dicom_to_nifti', type='node', parent=nodes['1_dicom_to_nifti'], no_in_ports=0, no_out_ports=0)

    
    nodes['20_t1_dicom_to_nifti'].in_ports['in_dicom_image'] = Port(node=nodes['20_t1_dicom_to_nifti'], name='in_dicom_image', description='')
    nodes['20_t1_dicom_to_nifti'].in_ports['in_file_order'] = Port(node=nodes['20_t1_dicom_to_nifti'], name='in_file_order', description='')
    nodes['20_t1_dicom_to_nifti'].in_ports['in_threshold'] = Port(node=nodes['20_t1_dicom_to_nifti'], name='in_threshold', description='')

    
    nodes['20_t1_dicom_to_nifti'].out_ports['out_image'] = Port(node=nodes['20_t1_dicom_to_nifti'], name='out_image', description='')
    
    nodes['21_t1_with_hammers'] = run.create_node(name='21_t1_with_hammers', type='node', parent=nodes['6_qc_overview'], no_in_ports=0, no_out_ports=0)

    
    nodes['21_t1_with_hammers'].in_ports['in_image'] = Port(node=nodes['21_t1_with_hammers'], name='in_image', description='')
    nodes['21_t1_with_hammers'].in_ports['in_segmentation'] = Port(node=nodes['21_t1_with_hammers'], name='in_segmentation', description='')

    
    nodes['21_t1_with_hammers'].out_ports['out_png_image'] = Port(node=nodes['21_t1_with_hammers'], name='out_png_image', description='')
    
    nodes['22_const_brain_mask_registation_parameters_0'] = run.create_node(name='22_const_brain_mask_registation_parameters_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['22_const_brain_mask_registation_parameters_0'].out_ports['out_output'] = Port(node=nodes['22_const_brain_mask_registation_parameters_0'], name='out_output', description='')
    
    nodes['23_multi_atlas_segm'] = run.create_node(name='23_multi_atlas_segm', type='sink', parent=nodes['4_output'], no_in_ports=0, no_out_ports=0)

    
    nodes['23_multi_atlas_segm'].in_ports['in_input'] = Port(node=nodes['23_multi_atlas_segm'], name='in_input', description='')

    
    
    nodes['24_const_combine_labels_method_0'] = run.create_node(name='24_const_combine_labels_method_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['24_const_combine_labels_method_0'].out_ports['out_output'] = Port(node=nodes['24_const_combine_labels_method_0'], name='out_output', description='')
    
    nodes['25_wm_volume'] = run.create_node(name='25_wm_volume', type='sink', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    
    nodes['25_wm_volume'].in_ports['in_input'] = Port(node=nodes['25_wm_volume'], name='in_input', description='')

    
    
    nodes['26_const_get_wm_volume_volume_0'] = run.create_node(name='26_const_get_wm_volume_volume_0', type='constant', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['26_const_get_wm_volume_volume_0'].out_ports['out_output'] = Port(node=nodes['26_const_get_wm_volume_volume_0'], name='out_output', description='')
    
    nodes['27_const_hammers_brain_transform_threads_0'] = run.create_node(name='27_const_hammers_brain_transform_threads_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['27_const_hammers_brain_transform_threads_0'].out_ports['out_output'] = Port(node=nodes['27_const_hammers_brain_transform_threads_0'], name='out_output', description='')
    
    nodes['28_n4_bias_correction'] = run.create_node(name='28_n4_bias_correction', type='node', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    
    nodes['28_n4_bias_correction'].in_ports['in_image'] = Port(node=nodes['28_n4_bias_correction'], name='in_image', description='')
    nodes['28_n4_bias_correction'].in_ports['in_mask'] = Port(node=nodes['28_n4_bias_correction'], name='in_mask', description='')
    nodes['28_n4_bias_correction'].in_ports['in_n4_parameters'] = Port(node=nodes['28_n4_bias_correction'], name='in_n4_parameters', description='')

    
    nodes['28_n4_bias_correction'].out_ports['out_corrected_image'] = Port(node=nodes['28_n4_bias_correction'], name='out_corrected_image', description='')
    
    nodes['29_t1_nifti_images'] = run.create_node(name='29_t1_nifti_images', type='sink', parent=nodes['4_output'], no_in_ports=0, no_out_ports=0)

    
    nodes['29_t1_nifti_images'].in_ports['in_input'] = Port(node=nodes['29_t1_nifti_images'], name='in_input', description='')

    
    
    nodes['30_combine_labels'] = run.create_node(name='30_combine_labels', type='node', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    
    nodes['30_combine_labels'].in_ports['in_images'] = Port(node=nodes['30_combine_labels'], name='in_images', description='')
    nodes['30_combine_labels'].in_ports['in_method'] = Port(node=nodes['30_combine_labels'], name='in_method', description='')
    nodes['30_combine_labels'].in_ports['in_number_of_classes'] = Port(node=nodes['30_combine_labels'], name='in_number_of_classes', description='')
    nodes['30_combine_labels'].in_ports['in_original_labels'] = Port(node=nodes['30_combine_labels'], name='in_original_labels', description='')
    nodes['30_combine_labels'].in_ports['in_substitute_labels'] = Port(node=nodes['30_combine_labels'], name='in_substitute_labels', description='')

    
    nodes['30_combine_labels'].out_ports['out_hard_segment'] = Port(node=nodes['30_combine_labels'], name='out_hard_segment', description='')
    
    nodes['31_t1_with_hammers_sink'] = run.create_node(name='31_t1_with_hammers_sink', type='sink', parent=nodes['6_qc_overview'], no_in_ports=0, no_out_ports=0)

    
    nodes['31_t1_with_hammers_sink'].in_ports['in_input'] = Port(node=nodes['31_t1_with_hammers_sink'], name='in_input', description='')

    
    
    nodes['32_const_dilate_brainmask_operation_type_0'] = run.create_node(name='32_const_dilate_brainmask_operation_type_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['32_const_dilate_brainmask_operation_type_0'].out_ports['out_output'] = Port(node=nodes['32_const_dilate_brainmask_operation_type_0'], name='out_output', description='')
    
    nodes['33_label_registration'] = run.create_node(name='33_label_registration', type='node', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    
    nodes['33_label_registration'].in_ports['in_transform'] = Port(node=nodes['33_label_registration'], name='in_transform', description='')
    nodes['33_label_registration'].in_ports['in_image'] = Port(node=nodes['33_label_registration'], name='in_image', description='')
    nodes['33_label_registration'].in_ports['in_points'] = Port(node=nodes['33_label_registration'], name='in_points', description='')
    nodes['33_label_registration'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['33_label_registration'], name='in_determinant_of_jacobian_flag', description='')
    nodes['33_label_registration'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['33_label_registration'], name='in_jacobian_matrix_flag', description='')
    nodes['33_label_registration'].in_ports['in_priority'] = Port(node=nodes['33_label_registration'], name='in_priority', description='')
    nodes['33_label_registration'].in_ports['in_threads'] = Port(node=nodes['33_label_registration'], name='in_threads', description='')

    
    nodes['33_label_registration'].out_ports['out_directory'] = Port(node=nodes['33_label_registration'], name='out_directory', description='')
    nodes['33_label_registration'].out_ports['out_image'] = Port(node=nodes['33_label_registration'], name='out_image', description='')
    nodes['33_label_registration'].out_ports['out_points'] = Port(node=nodes['33_label_registration'], name='out_points', description='')
    nodes['33_label_registration'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['33_label_registration'], name='out_determinant_of_jacobian', description='')
    nodes['33_label_registration'].out_ports['out_jacobian_matrix'] = Port(node=nodes['33_label_registration'], name='out_jacobian_matrix', description='')
    nodes['33_label_registration'].out_ports['out_log_file'] = Port(node=nodes['33_label_registration'], name='out_log_file', description='')
    
    nodes['34_t1w_dicom'] = run.create_node(name='34_t1w_dicom', type='source', parent=nodes['3_subjects'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['34_t1w_dicom'].out_ports['out_output'] = Port(node=nodes['34_t1w_dicom'], name='out_output', description='')
    
    nodes['35_const_dilate_brainmask_radius_0'] = run.create_node(name='35_const_dilate_brainmask_radius_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['35_const_dilate_brainmask_radius_0'].out_ports['out_output'] = Port(node=nodes['35_const_dilate_brainmask_radius_0'], name='out_output', description='')
    
    nodes['36_hammers_labels'] = run.create_node(name='36_hammers_labels', type='source', parent=nodes['2_hammers_atlas'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['36_hammers_labels'].out_ports['out_output'] = Port(node=nodes['36_hammers_labels'], name='out_output', description='')
    
    nodes['37_t1_with_wm_outline_sink'] = run.create_node(name='37_t1_with_wm_outline_sink', type='sink', parent=nodes['6_qc_overview'], no_in_ports=0, no_out_ports=0)

    
    nodes['37_t1_with_wm_outline_sink'].in_ports['in_input'] = Port(node=nodes['37_t1_with_wm_outline_sink'], name='in_input', description='')

    
    
    nodes['38_threshold_brain'] = run.create_node(name='38_threshold_brain', type='node', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    
    nodes['38_threshold_brain'].in_ports['in_image'] = Port(node=nodes['38_threshold_brain'], name='in_image', description='')
    nodes['38_threshold_brain'].in_ports['in_method'] = Port(node=nodes['38_threshold_brain'], name='in_method', description='')
    nodes['38_threshold_brain'].in_ports['in_mask'] = Port(node=nodes['38_threshold_brain'], name='in_mask', description='')
    nodes['38_threshold_brain'].in_ports['in_lower_threshold'] = Port(node=nodes['38_threshold_brain'], name='in_lower_threshold', description='')
    nodes['38_threshold_brain'].in_ports['in_upper_threshold'] = Port(node=nodes['38_threshold_brain'], name='in_upper_threshold', description='')
    nodes['38_threshold_brain'].in_ports['in_inside_value'] = Port(node=nodes['38_threshold_brain'], name='in_inside_value', description='')
    nodes['38_threshold_brain'].in_ports['in_outside_value'] = Port(node=nodes['38_threshold_brain'], name='in_outside_value', description='')
    nodes['38_threshold_brain'].in_ports['in_number_of_thresholds'] = Port(node=nodes['38_threshold_brain'], name='in_number_of_thresholds', description='')
    nodes['38_threshold_brain'].in_ports['in_number_of_histbins'] = Port(node=nodes['38_threshold_brain'], name='in_number_of_histbins', description='')
    nodes['38_threshold_brain'].in_ports['in_radius'] = Port(node=nodes['38_threshold_brain'], name='in_radius', description='')
    nodes['38_threshold_brain'].in_ports['in_number_of_controlpoints'] = Port(node=nodes['38_threshold_brain'], name='in_number_of_controlpoints', description='')
    nodes['38_threshold_brain'].in_ports['in_number_of_levels'] = Port(node=nodes['38_threshold_brain'], name='in_number_of_levels', description='')
    nodes['38_threshold_brain'].in_ports['in_number_of_samples'] = Port(node=nodes['38_threshold_brain'], name='in_number_of_samples', description='')
    nodes['38_threshold_brain'].in_ports['in_spline_order'] = Port(node=nodes['38_threshold_brain'], name='in_spline_order', description='')
    nodes['38_threshold_brain'].in_ports['in_power'] = Port(node=nodes['38_threshold_brain'], name='in_power', description='')
    nodes['38_threshold_brain'].in_ports['in_sigma'] = Port(node=nodes['38_threshold_brain'], name='in_sigma', description='')
    nodes['38_threshold_brain'].in_ports['in_number_of_iterations'] = Port(node=nodes['38_threshold_brain'], name='in_number_of_iterations', description='')
    nodes['38_threshold_brain'].in_ports['in_mask_value'] = Port(node=nodes['38_threshold_brain'], name='in_mask_value', description='')
    nodes['38_threshold_brain'].in_ports['in_mixture_type'] = Port(node=nodes['38_threshold_brain'], name='in_mixture_type', description='')
    nodes['38_threshold_brain'].in_ports['in_compression_flag'] = Port(node=nodes['38_threshold_brain'], name='in_compression_flag', description='')

    
    nodes['38_threshold_brain'].out_ports['out_image'] = Port(node=nodes['38_threshold_brain'], name='out_image', description='')
    
    nodes['39_const_get_gm_volume_volume_0'] = run.create_node(name='39_const_get_gm_volume_volume_0', type='constant', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['39_const_get_gm_volume_volume_0'].out_ports['out_output'] = Port(node=nodes['39_const_get_gm_volume_volume_0'], name='out_output', description='')
    
    nodes['40_gm_map_output'] = run.create_node(name='40_gm_map_output', type='sink', parent=nodes['4_output'], no_in_ports=0, no_out_ports=0)

    
    nodes['40_gm_map_output'].in_ports['in_input'] = Port(node=nodes['40_gm_map_output'], name='in_input', description='')

    
    
    nodes['41_const_mask_hammers_labels_operator_0'] = run.create_node(name='41_const_mask_hammers_labels_operator_0', type='constant', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['41_const_mask_hammers_labels_operator_0'].out_ports['out_output'] = Port(node=nodes['41_const_mask_hammers_labels_operator_0'], name='out_output', description='')
    
    nodes['42_t1_with_gm_outline'] = run.create_node(name='42_t1_with_gm_outline', type='node', parent=nodes['6_qc_overview'], no_in_ports=0, no_out_ports=0)

    
    nodes['42_t1_with_gm_outline'].in_ports['in_image'] = Port(node=nodes['42_t1_with_gm_outline'], name='in_image', description='')
    nodes['42_t1_with_gm_outline'].in_ports['in_segmentation'] = Port(node=nodes['42_t1_with_gm_outline'], name='in_segmentation', description='')

    
    nodes['42_t1_with_gm_outline'].out_ports['out_png_image'] = Port(node=nodes['42_t1_with_gm_outline'], name='out_png_image', description='')
    
    nodes['43_convert_brain'] = run.create_node(name='43_convert_brain', type='node', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    
    nodes['43_convert_brain'].in_ports['in_image'] = Port(node=nodes['43_convert_brain'], name='in_image', description='')
    nodes['43_convert_brain'].in_ports['in_component_type'] = Port(node=nodes['43_convert_brain'], name='in_component_type', description='')
    nodes['43_convert_brain'].in_ports['in_compression_flag'] = Port(node=nodes['43_convert_brain'], name='in_compression_flag', description='')

    
    nodes['43_convert_brain'].out_ports['out_image'] = Port(node=nodes['43_convert_brain'], name='out_image', description='')
    
    nodes['44_reformat_t1'] = run.create_node(name='44_reformat_t1', type='node', parent=nodes['1_dicom_to_nifti'], no_in_ports=0, no_out_ports=0)

    
    nodes['44_reformat_t1'].in_ports['in_image'] = Port(node=nodes['44_reformat_t1'], name='in_image', description='')

    
    nodes['44_reformat_t1'].out_ports['out_reformatted_image'] = Port(node=nodes['44_reformat_t1'], name='out_reformatted_image', description='')
    
    nodes['45_const_get_wm_volume_selection_0'] = run.create_node(name='45_const_get_wm_volume_selection_0', type='constant', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['45_const_get_wm_volume_selection_0'].out_ports['out_output'] = Port(node=nodes['45_const_get_wm_volume_selection_0'], name='out_output', description='')
    
    nodes['46_t1_qc'] = run.create_node(name='46_t1_qc', type='node', parent=nodes['6_qc_overview'], no_in_ports=0, no_out_ports=0)

    
    nodes['46_t1_qc'].in_ports['in_image'] = Port(node=nodes['46_t1_qc'], name='in_image', description='')

    
    nodes['46_t1_qc'].out_ports['out_png_image'] = Port(node=nodes['46_t1_qc'], name='out_png_image', description='')
    
    nodes['47_const_convert_brain_component_type_0'] = run.create_node(name='47_const_convert_brain_component_type_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['47_const_convert_brain_component_type_0'].out_ports['out_output'] = Port(node=nodes['47_const_convert_brain_component_type_0'], name='out_output', description='')
    
    nodes['48_const_get_brain_volume_flatten_0'] = run.create_node(name='48_const_get_brain_volume_flatten_0', type='constant', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['48_const_get_brain_volume_flatten_0'].out_ports['out_output'] = Port(node=nodes['48_const_get_brain_volume_flatten_0'], name='out_output', description='')
    
    nodes['49_const_t1w_registration_parameters_0'] = run.create_node(name='49_const_t1w_registration_parameters_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['49_const_t1w_registration_parameters_0'].out_ports['out_output'] = Port(node=nodes['49_const_t1w_registration_parameters_0'], name='out_output', description='')
    
    nodes['50_hammers_brains'] = run.create_node(name='50_hammers_brains', type='source', parent=nodes['2_hammers_atlas'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['50_hammers_brains'].out_ports['out_output'] = Port(node=nodes['50_hammers_brains'], name='out_output', description='')
    
    nodes['51_const_combine_brains_number_of_classes_0'] = run.create_node(name='51_const_combine_brains_number_of_classes_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['51_const_combine_brains_number_of_classes_0'].out_ports['out_output'] = Port(node=nodes['51_const_combine_brains_number_of_classes_0'], name='out_output', description='')
    
    nodes['52_const_get_brain_volume_selection_0'] = run.create_node(name='52_const_get_brain_volume_selection_0', type='constant', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['52_const_get_brain_volume_selection_0'].out_ports['out_output'] = Port(node=nodes['52_const_get_brain_volume_selection_0'], name='out_output', description='')
    
    nodes['53_const_label_registration_threads_0'] = run.create_node(name='53_const_label_registration_threads_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['53_const_label_registration_threads_0'].out_ports['out_output'] = Port(node=nodes['53_const_label_registration_threads_0'], name='out_output', description='')
    
    nodes['54_t1_qc_sink'] = run.create_node(name='54_t1_qc_sink', type='sink', parent=nodes['6_qc_overview'], no_in_ports=0, no_out_ports=0)

    
    nodes['54_t1_qc_sink'].in_ports['in_input'] = Port(node=nodes['54_t1_qc_sink'], name='in_input', description='')

    
    
    nodes['55_brain_volume'] = run.create_node(name='55_brain_volume', type='sink', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    
    nodes['55_brain_volume'].in_ports['in_input'] = Port(node=nodes['55_brain_volume'], name='in_input', description='')

    
    
    nodes['56_brain_mask_registation'] = run.create_node(name='56_brain_mask_registation', type='node', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    
    nodes['56_brain_mask_registation'].in_ports['in_fixed_image'] = Port(node=nodes['56_brain_mask_registation'], name='in_fixed_image', description='')
    nodes['56_brain_mask_registation'].in_ports['in_moving_image'] = Port(node=nodes['56_brain_mask_registation'], name='in_moving_image', description='')
    nodes['56_brain_mask_registation'].in_ports['in_parameters'] = Port(node=nodes['56_brain_mask_registation'], name='in_parameters', description='')
    nodes['56_brain_mask_registation'].in_ports['in_fixed_mask'] = Port(node=nodes['56_brain_mask_registation'], name='in_fixed_mask', description='')
    nodes['56_brain_mask_registation'].in_ports['in_moving_mask'] = Port(node=nodes['56_brain_mask_registation'], name='in_moving_mask', description='')
    nodes['56_brain_mask_registation'].in_ports['in_initial_transform'] = Port(node=nodes['56_brain_mask_registation'], name='in_initial_transform', description='')
    nodes['56_brain_mask_registation'].in_ports['in_priority'] = Port(node=nodes['56_brain_mask_registation'], name='in_priority', description='')
    nodes['56_brain_mask_registation'].in_ports['in_threads'] = Port(node=nodes['56_brain_mask_registation'], name='in_threads', description='')

    
    nodes['56_brain_mask_registation'].out_ports['out_directory'] = Port(node=nodes['56_brain_mask_registation'], name='out_directory', description='')
    nodes['56_brain_mask_registation'].out_ports['out_transform'] = Port(node=nodes['56_brain_mask_registation'], name='out_transform', description='')
    nodes['56_brain_mask_registation'].out_ports['out_log_file'] = Port(node=nodes['56_brain_mask_registation'], name='out_log_file', description='')
    
    nodes['57_hammer_brain_mask'] = run.create_node(name='57_hammer_brain_mask', type='sink', parent=nodes['4_output'], no_in_ports=0, no_out_ports=0)

    
    nodes['57_hammer_brain_mask'].in_ports['in_input'] = Port(node=nodes['57_hammer_brain_mask'], name='in_input', description='')

    
    
    nodes['58_const_combine_brains_method_0'] = run.create_node(name='58_const_combine_brains_method_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['58_const_combine_brains_method_0'].out_ports['out_output'] = Port(node=nodes['58_const_combine_brains_method_0'], name='out_output', description='')
    
    nodes['59_const_t1w_registration_threads_0'] = run.create_node(name='59_const_t1w_registration_threads_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['59_const_t1w_registration_threads_0'].out_ports['out_output'] = Port(node=nodes['59_const_t1w_registration_threads_0'], name='out_output', description='')
    
    nodes['60_const_get_brain_volume_volume_0'] = run.create_node(name='60_const_get_brain_volume_volume_0', type='constant', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['60_const_get_brain_volume_volume_0'].out_ports['out_output'] = Port(node=nodes['60_const_get_brain_volume_volume_0'], name='out_output', description='')
    
    nodes['61_const_t1_dicom_to_nifti_file_order_0'] = run.create_node(name='61_const_t1_dicom_to_nifti_file_order_0', type='constant', parent=nodes['1_dicom_to_nifti'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['61_const_t1_dicom_to_nifti_file_order_0'].out_ports['out_output'] = Port(node=nodes['61_const_t1_dicom_to_nifti_file_order_0'], name='out_output', description='')
    
    nodes['62_t1_with_gm_outline_sink'] = run.create_node(name='62_t1_with_gm_outline_sink', type='sink', parent=nodes['6_qc_overview'], no_in_ports=0, no_out_ports=0)

    
    nodes['62_t1_with_gm_outline_sink'].in_ports['in_input'] = Port(node=nodes['62_t1_with_gm_outline_sink'], name='in_input', description='')

    
    
    nodes['63_const_brain_mask_registation_threads_0'] = run.create_node(name='63_const_brain_mask_registation_threads_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['63_const_brain_mask_registation_threads_0'].out_ports['out_output'] = Port(node=nodes['63_const_brain_mask_registation_threads_0'], name='out_output', description='')
    
    nodes['64_hammers_brains_r5'] = run.create_node(name='64_hammers_brains_r5', type='source', parent=nodes['2_hammers_atlas'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['64_hammers_brains_r5'].out_ports['out_output'] = Port(node=nodes['64_hammers_brains_r5'], name='out_output', description='')
    
    nodes['70_spm_segmantation'] = run.create_node(name='70_spm_segmantation', type='node', parent=nodes['68_SPM_Tissue_Segmentation'], no_in_ports=0, no_out_ports=0)

    
    nodes['70_spm_segmantation'].in_ports['in_input_image'] = Port(node=nodes['70_spm_segmantation'], name='in_input_image', description='')

    
    nodes['70_spm_segmantation'].out_ports['out_directory'] = Port(node=nodes['70_spm_segmantation'], name='out_directory', description='')
    nodes['70_spm_segmantation'].out_ports['out_gm_image'] = Port(node=nodes['70_spm_segmantation'], name='out_gm_image', description='')
    nodes['70_spm_segmantation'].out_ports['out_wm_image'] = Port(node=nodes['70_spm_segmantation'], name='out_wm_image', description='')
    nodes['70_spm_segmantation'].out_ports['out_csf_image'] = Port(node=nodes['70_spm_segmantation'], name='out_csf_image', description='')
    
    nodes['71_const_edit_transform_file_set_0'] = run.create_node(name='71_const_edit_transform_file_set_0', type='constant', parent=nodes['68_SPM_Tissue_Segmentation'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['71_const_edit_transform_file_set_0'].out_ports['out_output'] = Port(node=nodes['71_const_edit_transform_file_set_0'], name='out_output', description='')
    
    nodes['72_csf_map'] = run.create_node(name='72_csf_map', type='sink', parent=nodes['69_outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['72_csf_map'].in_ports['in_input'] = Port(node=nodes['72_csf_map'], name='in_input', description='')

    
    nodes['72_csf_map'].out_ports['out_sink'] = Port(node=nodes['72_csf_map'], name='out_sink', description='')
    
    nodes['73_wm_map'] = run.create_node(name='73_wm_map', type='sink', parent=nodes['69_outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['73_wm_map'].in_ports['in_input'] = Port(node=nodes['73_wm_map'], name='in_input', description='')

    
    nodes['73_wm_map'].out_ports['out_sink'] = Port(node=nodes['73_wm_map'], name='out_sink', description='')
    
    nodes['74_const_invert_gm_parameters_0'] = run.create_node(name='74_const_invert_gm_parameters_0', type='constant', parent=nodes['68_SPM_Tissue_Segmentation'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['74_const_invert_gm_parameters_0'].out_ports['out_output'] = Port(node=nodes['74_const_invert_gm_parameters_0'], name='out_output', description='')
    
    nodes['75_t1_passthrough'] = run.create_node(name='75_t1_passthrough', type='node', parent=nodes['67_InputImage'], no_in_ports=0, no_out_ports=0)

    
    nodes['75_t1_passthrough'].in_ports['in_left_hand'] = Port(node=nodes['75_t1_passthrough'], name='in_left_hand', description='')
    nodes['75_t1_passthrough'].in_ports['in_operator'] = Port(node=nodes['75_t1_passthrough'], name='in_operator', description='')

    
    nodes['75_t1_passthrough'].out_ports['out_result'] = Port(node=nodes['75_t1_passthrough'], name='out_result', description='')
    
    nodes['76_reg_t1_to_spmtemplate'] = run.create_node(name='76_reg_t1_to_spmtemplate', type='node', parent=nodes['68_SPM_Tissue_Segmentation'], no_in_ports=0, no_out_ports=0)

    
    nodes['76_reg_t1_to_spmtemplate'].in_ports['in_fixed_image'] = Port(node=nodes['76_reg_t1_to_spmtemplate'], name='in_fixed_image', description='')
    nodes['76_reg_t1_to_spmtemplate'].in_ports['in_moving_image'] = Port(node=nodes['76_reg_t1_to_spmtemplate'], name='in_moving_image', description='')
    nodes['76_reg_t1_to_spmtemplate'].in_ports['in_parameters'] = Port(node=nodes['76_reg_t1_to_spmtemplate'], name='in_parameters', description='')
    nodes['76_reg_t1_to_spmtemplate'].in_ports['in_fixed_mask'] = Port(node=nodes['76_reg_t1_to_spmtemplate'], name='in_fixed_mask', description='')
    nodes['76_reg_t1_to_spmtemplate'].in_ports['in_moving_mask'] = Port(node=nodes['76_reg_t1_to_spmtemplate'], name='in_moving_mask', description='')
    nodes['76_reg_t1_to_spmtemplate'].in_ports['in_initial_transform'] = Port(node=nodes['76_reg_t1_to_spmtemplate'], name='in_initial_transform', description='')
    nodes['76_reg_t1_to_spmtemplate'].in_ports['in_priority'] = Port(node=nodes['76_reg_t1_to_spmtemplate'], name='in_priority', description='')
    nodes['76_reg_t1_to_spmtemplate'].in_ports['in_threads'] = Port(node=nodes['76_reg_t1_to_spmtemplate'], name='in_threads', description='')

    
    nodes['76_reg_t1_to_spmtemplate'].out_ports['out_directory'] = Port(node=nodes['76_reg_t1_to_spmtemplate'], name='out_directory', description='')
    nodes['76_reg_t1_to_spmtemplate'].out_ports['out_transform'] = Port(node=nodes['76_reg_t1_to_spmtemplate'], name='out_transform', description='')
    nodes['76_reg_t1_to_spmtemplate'].out_ports['out_log_file'] = Port(node=nodes['76_reg_t1_to_spmtemplate'], name='out_log_file', description='')
    
    nodes['77_const_t1_passthrough_operator_0'] = run.create_node(name='77_const_t1_passthrough_operator_0', type='constant', parent=nodes['67_InputImage'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['77_const_t1_passthrough_operator_0'].out_ports['out_output'] = Port(node=nodes['77_const_t1_passthrough_operator_0'], name='out_output', description='')
    
    nodes['78_const_reg_t1_to_spmtemplate_parameters_0'] = run.create_node(name='78_const_reg_t1_to_spmtemplate_parameters_0', type='constant', parent=nodes['68_SPM_Tissue_Segmentation'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['78_const_reg_t1_to_spmtemplate_parameters_0'].out_ports['out_output'] = Port(node=nodes['78_const_reg_t1_to_spmtemplate_parameters_0'], name='out_output', description='')
    
    nodes['79_transform_csf'] = run.create_node(name='79_transform_csf', type='node', parent=nodes['68_SPM_Tissue_Segmentation'], no_in_ports=0, no_out_ports=0)

    
    nodes['79_transform_csf'].in_ports['in_transform'] = Port(node=nodes['79_transform_csf'], name='in_transform', description='')
    nodes['79_transform_csf'].in_ports['in_image'] = Port(node=nodes['79_transform_csf'], name='in_image', description='')
    nodes['79_transform_csf'].in_ports['in_points'] = Port(node=nodes['79_transform_csf'], name='in_points', description='')
    nodes['79_transform_csf'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['79_transform_csf'], name='in_determinant_of_jacobian_flag', description='')
    nodes['79_transform_csf'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['79_transform_csf'], name='in_jacobian_matrix_flag', description='')
    nodes['79_transform_csf'].in_ports['in_priority'] = Port(node=nodes['79_transform_csf'], name='in_priority', description='')
    nodes['79_transform_csf'].in_ports['in_threads'] = Port(node=nodes['79_transform_csf'], name='in_threads', description='')

    
    nodes['79_transform_csf'].out_ports['out_directory'] = Port(node=nodes['79_transform_csf'], name='out_directory', description='')
    nodes['79_transform_csf'].out_ports['out_image'] = Port(node=nodes['79_transform_csf'], name='out_image', description='')
    nodes['79_transform_csf'].out_ports['out_points'] = Port(node=nodes['79_transform_csf'], name='out_points', description='')
    nodes['79_transform_csf'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['79_transform_csf'], name='out_determinant_of_jacobian', description='')
    nodes['79_transform_csf'].out_ports['out_jacobian_matrix'] = Port(node=nodes['79_transform_csf'], name='out_jacobian_matrix', description='')
    nodes['79_transform_csf'].out_ports['out_log_file'] = Port(node=nodes['79_transform_csf'], name='out_log_file', description='')
    
    nodes['80_gm_map'] = run.create_node(name='80_gm_map', type='sink', parent=nodes['69_outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['80_gm_map'].in_ports['in_input'] = Port(node=nodes['80_gm_map'], name='in_input', description='')

    
    nodes['80_gm_map'].out_ports['out_sink'] = Port(node=nodes['80_gm_map'], name='out_sink', description='')
    
    nodes['81_T1_images'] = run.create_node(name='81_T1_images', type='source', parent=nodes['66_InputImages'], no_in_ports=0, no_out_ports=0)

    
    nodes['81_T1_images'].in_ports['in_source'] = Port(node=nodes['81_T1_images'], name='in_source', description='')

    
    nodes['81_T1_images'].out_ports['out_output'] = Port(node=nodes['81_T1_images'], name='out_output', description='')
    
    nodes['82_invert_gm'] = run.create_node(name='82_invert_gm', type='node', parent=nodes['68_SPM_Tissue_Segmentation'], no_in_ports=0, no_out_ports=0)

    
    nodes['82_invert_gm'].in_ports['in_fixed_image'] = Port(node=nodes['82_invert_gm'], name='in_fixed_image', description='')
    nodes['82_invert_gm'].in_ports['in_moving_image'] = Port(node=nodes['82_invert_gm'], name='in_moving_image', description='')
    nodes['82_invert_gm'].in_ports['in_parameters'] = Port(node=nodes['82_invert_gm'], name='in_parameters', description='')
    nodes['82_invert_gm'].in_ports['in_fixed_mask'] = Port(node=nodes['82_invert_gm'], name='in_fixed_mask', description='')
    nodes['82_invert_gm'].in_ports['in_moving_mask'] = Port(node=nodes['82_invert_gm'], name='in_moving_mask', description='')
    nodes['82_invert_gm'].in_ports['in_initial_transform'] = Port(node=nodes['82_invert_gm'], name='in_initial_transform', description='')
    nodes['82_invert_gm'].in_ports['in_priority'] = Port(node=nodes['82_invert_gm'], name='in_priority', description='')
    nodes['82_invert_gm'].in_ports['in_threads'] = Port(node=nodes['82_invert_gm'], name='in_threads', description='')

    
    nodes['82_invert_gm'].out_ports['out_directory'] = Port(node=nodes['82_invert_gm'], name='out_directory', description='')
    nodes['82_invert_gm'].out_ports['out_transform'] = Port(node=nodes['82_invert_gm'], name='out_transform', description='')
    nodes['82_invert_gm'].out_ports['out_log_file'] = Port(node=nodes['82_invert_gm'], name='out_log_file', description='')
    
    nodes['83_spm_hard_segment'] = run.create_node(name='83_spm_hard_segment', type='node', parent=nodes['68_SPM_Tissue_Segmentation'], no_in_ports=0, no_out_ports=0)

    
    nodes['83_spm_hard_segment'].in_ports['in_gray_matter'] = Port(node=nodes['83_spm_hard_segment'], name='in_gray_matter', description='')
    nodes['83_spm_hard_segment'].in_ports['in_white_matter'] = Port(node=nodes['83_spm_hard_segment'], name='in_white_matter', description='')
    nodes['83_spm_hard_segment'].in_ports['in_cerebral_spinal_fluid'] = Port(node=nodes['83_spm_hard_segment'], name='in_cerebral_spinal_fluid', description='')

    
    nodes['83_spm_hard_segment'].out_ports['out_gm_image'] = Port(node=nodes['83_spm_hard_segment'], name='out_gm_image', description='')
    nodes['83_spm_hard_segment'].out_ports['out_wm_image'] = Port(node=nodes['83_spm_hard_segment'], name='out_wm_image', description='')
    nodes['83_spm_hard_segment'].out_ports['out_csf_image'] = Port(node=nodes['83_spm_hard_segment'], name='out_csf_image', description='')
    
    nodes['84_csf_spm'] = run.create_node(name='84_csf_spm', type='sink', parent=nodes['69_outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['84_csf_spm'].in_ports['in_input'] = Port(node=nodes['84_csf_spm'], name='in_input', description='')

    
    nodes['84_csf_spm'].out_ports['out_sink'] = Port(node=nodes['84_csf_spm'], name='out_sink', description='')
    
    nodes['85_const_reg_t1_to_spmtemplate_fixed_image_0'] = run.create_node(name='85_const_reg_t1_to_spmtemplate_fixed_image_0', type='constant', parent=nodes['68_SPM_Tissue_Segmentation'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['85_const_reg_t1_to_spmtemplate_fixed_image_0'].out_ports['out_output'] = Port(node=nodes['85_const_reg_t1_to_spmtemplate_fixed_image_0'], name='out_output', description='')
    
    nodes['86_edit_transform_file'] = run.create_node(name='86_edit_transform_file', type='node', parent=nodes['68_SPM_Tissue_Segmentation'], no_in_ports=0, no_out_ports=0)

    
    nodes['86_edit_transform_file'].in_ports['in_transform'] = Port(node=nodes['86_edit_transform_file'], name='in_transform', description='')
    nodes['86_edit_transform_file'].in_ports['in_set'] = Port(node=nodes['86_edit_transform_file'], name='in_set', description='')

    
    nodes['86_edit_transform_file'].out_ports['out_transform'] = Port(node=nodes['86_edit_transform_file'], name='out_transform', description='')
    
    nodes['87_transform_gm'] = run.create_node(name='87_transform_gm', type='node', parent=nodes['68_SPM_Tissue_Segmentation'], no_in_ports=0, no_out_ports=0)

    
    nodes['87_transform_gm'].in_ports['in_transform'] = Port(node=nodes['87_transform_gm'], name='in_transform', description='')
    nodes['87_transform_gm'].in_ports['in_image'] = Port(node=nodes['87_transform_gm'], name='in_image', description='')
    nodes['87_transform_gm'].in_ports['in_points'] = Port(node=nodes['87_transform_gm'], name='in_points', description='')
    nodes['87_transform_gm'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['87_transform_gm'], name='in_determinant_of_jacobian_flag', description='')
    nodes['87_transform_gm'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['87_transform_gm'], name='in_jacobian_matrix_flag', description='')
    nodes['87_transform_gm'].in_ports['in_priority'] = Port(node=nodes['87_transform_gm'], name='in_priority', description='')
    nodes['87_transform_gm'].in_ports['in_threads'] = Port(node=nodes['87_transform_gm'], name='in_threads', description='')

    
    nodes['87_transform_gm'].out_ports['out_directory'] = Port(node=nodes['87_transform_gm'], name='out_directory', description='')
    nodes['87_transform_gm'].out_ports['out_image'] = Port(node=nodes['87_transform_gm'], name='out_image', description='')
    nodes['87_transform_gm'].out_ports['out_points'] = Port(node=nodes['87_transform_gm'], name='out_points', description='')
    nodes['87_transform_gm'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['87_transform_gm'], name='out_determinant_of_jacobian', description='')
    nodes['87_transform_gm'].out_ports['out_jacobian_matrix'] = Port(node=nodes['87_transform_gm'], name='out_jacobian_matrix', description='')
    nodes['87_transform_gm'].out_ports['out_log_file'] = Port(node=nodes['87_transform_gm'], name='out_log_file', description='')
    
    nodes['88_gm_spm'] = run.create_node(name='88_gm_spm', type='sink', parent=nodes['69_outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['88_gm_spm'].in_ports['in_input'] = Port(node=nodes['88_gm_spm'], name='in_input', description='')

    
    nodes['88_gm_spm'].out_ports['out_sink'] = Port(node=nodes['88_gm_spm'], name='out_sink', description='')
    
    nodes['89_transform_wm'] = run.create_node(name='89_transform_wm', type='node', parent=nodes['68_SPM_Tissue_Segmentation'], no_in_ports=0, no_out_ports=0)

    
    nodes['89_transform_wm'].in_ports['in_transform'] = Port(node=nodes['89_transform_wm'], name='in_transform', description='')
    nodes['89_transform_wm'].in_ports['in_image'] = Port(node=nodes['89_transform_wm'], name='in_image', description='')
    nodes['89_transform_wm'].in_ports['in_points'] = Port(node=nodes['89_transform_wm'], name='in_points', description='')
    nodes['89_transform_wm'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['89_transform_wm'], name='in_determinant_of_jacobian_flag', description='')
    nodes['89_transform_wm'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['89_transform_wm'], name='in_jacobian_matrix_flag', description='')
    nodes['89_transform_wm'].in_ports['in_priority'] = Port(node=nodes['89_transform_wm'], name='in_priority', description='')
    nodes['89_transform_wm'].in_ports['in_threads'] = Port(node=nodes['89_transform_wm'], name='in_threads', description='')

    
    nodes['89_transform_wm'].out_ports['out_directory'] = Port(node=nodes['89_transform_wm'], name='out_directory', description='')
    nodes['89_transform_wm'].out_ports['out_image'] = Port(node=nodes['89_transform_wm'], name='out_image', description='')
    nodes['89_transform_wm'].out_ports['out_points'] = Port(node=nodes['89_transform_wm'], name='out_points', description='')
    nodes['89_transform_wm'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['89_transform_wm'], name='out_determinant_of_jacobian', description='')
    nodes['89_transform_wm'].out_ports['out_jacobian_matrix'] = Port(node=nodes['89_transform_wm'], name='out_jacobian_matrix', description='')
    nodes['89_transform_wm'].out_ports['out_log_file'] = Port(node=nodes['89_transform_wm'], name='out_log_file', description='')
    
    nodes['90_wm_spm'] = run.create_node(name='90_wm_spm', type='sink', parent=nodes['69_outputs'], no_in_ports=0, no_out_ports=0)

    
    nodes['90_wm_spm'].in_ports['in_input'] = Port(node=nodes['90_wm_spm'], name='in_input', description='')

    
    nodes['90_wm_spm'].out_ports['out_sink'] = Port(node=nodes['90_wm_spm'], name='out_sink', description='')
    
    nodes['91_transform_t1'] = run.create_node(name='91_transform_t1', type='node', parent=nodes['68_SPM_Tissue_Segmentation'], no_in_ports=0, no_out_ports=0)

    
    nodes['91_transform_t1'].in_ports['in_transform'] = Port(node=nodes['91_transform_t1'], name='in_transform', description='')
    nodes['91_transform_t1'].in_ports['in_image'] = Port(node=nodes['91_transform_t1'], name='in_image', description='')
    nodes['91_transform_t1'].in_ports['in_points'] = Port(node=nodes['91_transform_t1'], name='in_points', description='')
    nodes['91_transform_t1'].in_ports['in_determinant_of_jacobian_flag'] = Port(node=nodes['91_transform_t1'], name='in_determinant_of_jacobian_flag', description='')
    nodes['91_transform_t1'].in_ports['in_jacobian_matrix_flag'] = Port(node=nodes['91_transform_t1'], name='in_jacobian_matrix_flag', description='')
    nodes['91_transform_t1'].in_ports['in_priority'] = Port(node=nodes['91_transform_t1'], name='in_priority', description='')
    nodes['91_transform_t1'].in_ports['in_threads'] = Port(node=nodes['91_transform_t1'], name='in_threads', description='')

    
    nodes['91_transform_t1'].out_ports['out_directory'] = Port(node=nodes['91_transform_t1'], name='out_directory', description='')
    nodes['91_transform_t1'].out_ports['out_image'] = Port(node=nodes['91_transform_t1'], name='out_image', description='')
    nodes['91_transform_t1'].out_ports['out_points'] = Port(node=nodes['91_transform_t1'], name='out_points', description='')
    nodes['91_transform_t1'].out_ports['out_determinant_of_jacobian'] = Port(node=nodes['91_transform_t1'], name='out_determinant_of_jacobian', description='')
    nodes['91_transform_t1'].out_ports['out_jacobian_matrix'] = Port(node=nodes['91_transform_t1'], name='out_jacobian_matrix', description='')
    nodes['91_transform_t1'].out_ports['out_log_file'] = Port(node=nodes['91_transform_t1'], name='out_log_file', description='')
    
    nodes['121_combine_brains'] = run.create_node(name='121_combine_brains', type='node', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    
    nodes['121_combine_brains'].in_ports['in_images'] = Port(node=nodes['121_combine_brains'], name='in_images', description='')
    nodes['121_combine_brains'].in_ports['in_method'] = Port(node=nodes['121_combine_brains'], name='in_method', description='')
    nodes['121_combine_brains'].in_ports['in_number_of_classes'] = Port(node=nodes['121_combine_brains'], name='in_number_of_classes', description='')
    nodes['121_combine_brains'].in_ports['in_original_labels'] = Port(node=nodes['121_combine_brains'], name='in_original_labels', description='')
    nodes['121_combine_brains'].in_ports['in_substitute_labels'] = Port(node=nodes['121_combine_brains'], name='in_substitute_labels', description='')

    
    nodes['121_combine_brains'].out_ports['out_hard_segment'] = Port(node=nodes['121_combine_brains'], name='out_hard_segment', description='')
    
    nodes['122_t1_with_brain'] = run.create_node(name='122_t1_with_brain', type='node', parent=nodes['6_qc_overview'], no_in_ports=0, no_out_ports=0)

    
    nodes['122_t1_with_brain'].in_ports['in_image'] = Port(node=nodes['122_t1_with_brain'], name='in_image', description='')
    nodes['122_t1_with_brain'].in_ports['in_segmentation'] = Port(node=nodes['122_t1_with_brain'], name='in_segmentation', description='')

    
    nodes['122_t1_with_brain'].out_ports['out_png_image'] = Port(node=nodes['122_t1_with_brain'], name='out_png_image', description='')
    
    nodes['123_t1_with_wm_outline'] = run.create_node(name='123_t1_with_wm_outline', type='node', parent=nodes['6_qc_overview'], no_in_ports=0, no_out_ports=0)

    
    nodes['123_t1_with_wm_outline'].in_ports['in_image'] = Port(node=nodes['123_t1_with_wm_outline'], name='in_image', description='')
    nodes['123_t1_with_wm_outline'].in_ports['in_segmentation'] = Port(node=nodes['123_t1_with_wm_outline'], name='in_segmentation', description='')

    
    nodes['123_t1_with_wm_outline'].out_ports['out_png_image'] = Port(node=nodes['123_t1_with_wm_outline'], name='out_png_image', description='')
    
    nodes['124_const_combine_labels_number_of_classes_0'] = run.create_node(name='124_const_combine_labels_number_of_classes_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['124_const_combine_labels_number_of_classes_0'].out_ports['out_output'] = Port(node=nodes['124_const_combine_labels_number_of_classes_0'], name='out_output', description='')
    
    nodes['125_dilate_brainmask'] = run.create_node(name='125_dilate_brainmask', type='node', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    
    nodes['125_dilate_brainmask'].in_ports['in_image'] = Port(node=nodes['125_dilate_brainmask'], name='in_image', description='')
    nodes['125_dilate_brainmask'].in_ports['in_operation'] = Port(node=nodes['125_dilate_brainmask'], name='in_operation', description='')
    nodes['125_dilate_brainmask'].in_ports['in_operation_type'] = Port(node=nodes['125_dilate_brainmask'], name='in_operation_type', description='')
    nodes['125_dilate_brainmask'].in_ports['in_compression_flag'] = Port(node=nodes['125_dilate_brainmask'], name='in_compression_flag', description='')
    nodes['125_dilate_brainmask'].in_ports['in_radius'] = Port(node=nodes['125_dilate_brainmask'], name='in_radius', description='')
    nodes['125_dilate_brainmask'].in_ports['in_boundary_condition'] = Port(node=nodes['125_dilate_brainmask'], name='in_boundary_condition', description='')
    nodes['125_dilate_brainmask'].in_ports['in_foreground_background_value'] = Port(node=nodes['125_dilate_brainmask'], name='in_foreground_background_value', description='')
    nodes['125_dilate_brainmask'].in_ports['in_gradient_algorithm_enum'] = Port(node=nodes['125_dilate_brainmask'], name='in_gradient_algorithm_enum', description='')
    nodes['125_dilate_brainmask'].in_ports['in_component_type'] = Port(node=nodes['125_dilate_brainmask'], name='in_component_type', description='')

    
    nodes['125_dilate_brainmask'].out_ports['out_image'] = Port(node=nodes['125_dilate_brainmask'], name='out_image', description='')
    
    nodes['126_get_wm_volume'] = run.create_node(name='126_get_wm_volume', type='node', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    
    nodes['126_get_wm_volume'].in_ports['in_label'] = Port(node=nodes['126_get_wm_volume'], name='in_label', description='')
    nodes['126_get_wm_volume'].in_ports['in_image'] = Port(node=nodes['126_get_wm_volume'], name='in_image', description='')
    nodes['126_get_wm_volume'].in_ports['in_mask'] = Port(node=nodes['126_get_wm_volume'], name='in_mask', description='')
    nodes['126_get_wm_volume'].in_ports['in_volume'] = Port(node=nodes['126_get_wm_volume'], name='in_volume', description='')
    nodes['126_get_wm_volume'].in_ports['in_stddev'] = Port(node=nodes['126_get_wm_volume'], name='in_stddev', description='')
    nodes['126_get_wm_volume'].in_ports['in_mean'] = Port(node=nodes['126_get_wm_volume'], name='in_mean', description='')
    nodes['126_get_wm_volume'].in_ports['in_selection'] = Port(node=nodes['126_get_wm_volume'], name='in_selection', description='')
    nodes['126_get_wm_volume'].in_ports['in_ddof'] = Port(node=nodes['126_get_wm_volume'], name='in_ddof', description='')
    nodes['126_get_wm_volume'].in_ports['in_flatten'] = Port(node=nodes['126_get_wm_volume'], name='in_flatten', description='')

    
    nodes['126_get_wm_volume'].out_ports['out_statistics'] = Port(node=nodes['126_get_wm_volume'], name='out_statistics', description='')
    
    nodes['127_t1_with_brain_sink'] = run.create_node(name='127_t1_with_brain_sink', type='sink', parent=nodes['6_qc_overview'], no_in_ports=0, no_out_ports=0)

    
    nodes['127_t1_with_brain_sink'].in_ports['in_input'] = Port(node=nodes['127_t1_with_brain_sink'], name='in_input', description='')

    
    
    nodes['128_gm_volume'] = run.create_node(name='128_gm_volume', type='sink', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    
    nodes['128_gm_volume'].in_ports['in_input'] = Port(node=nodes['128_gm_volume'], name='in_input', description='')

    
    
    nodes['129_const_get_gm_volume_selection_0'] = run.create_node(name='129_const_get_gm_volume_selection_0', type='constant', parent=nodes['0_statistics'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['129_const_get_gm_volume_selection_0'].out_ports['out_output'] = Port(node=nodes['129_const_get_gm_volume_selection_0'], name='out_output', description='')
    
    nodes['130_bet'] = run.create_node(name='130_bet', type='node', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    
    nodes['130_bet'].in_ports['in_image'] = Port(node=nodes['130_bet'], name='in_image', description='')
    nodes['130_bet'].in_ports['in_fraction_threshold'] = Port(node=nodes['130_bet'], name='in_fraction_threshold', description='')
    nodes['130_bet'].in_ports['in_fraction_gradient'] = Port(node=nodes['130_bet'], name='in_fraction_gradient', description='')
    nodes['130_bet'].in_ports['in_head_radius'] = Port(node=nodes['130_bet'], name='in_head_radius', description='')
    nodes['130_bet'].in_ports['in_center_of_gravity'] = Port(node=nodes['130_bet'], name='in_center_of_gravity', description='')
    nodes['130_bet'].in_ports['in_apply_threshold'] = Port(node=nodes['130_bet'], name='in_apply_threshold', description='')
    nodes['130_bet'].in_ports['in_robust_estimation'] = Port(node=nodes['130_bet'], name='in_robust_estimation', description='')
    nodes['130_bet'].in_ports['in_eye_cleanup'] = Port(node=nodes['130_bet'], name='in_eye_cleanup', description='')
    nodes['130_bet'].in_ports['in_bias_cleanup'] = Port(node=nodes['130_bet'], name='in_bias_cleanup', description='')
    nodes['130_bet'].in_ports['in_small_fov'] = Port(node=nodes['130_bet'], name='in_small_fov', description='')
    nodes['130_bet'].in_ports['in_apply_4D'] = Port(node=nodes['130_bet'], name='in_apply_4D', description='')
    nodes['130_bet'].in_ports['in_run_bet2_surf'] = Port(node=nodes['130_bet'], name='in_run_bet2_surf', description='')
    nodes['130_bet'].in_ports['in_T2_image'] = Port(node=nodes['130_bet'], name='in_T2_image', description='')

    
    nodes['130_bet'].out_ports['out_brain_image'] = Port(node=nodes['130_bet'], name='out_brain_image', description='')
    nodes['130_bet'].out_ports['out_overlay_image'] = Port(node=nodes['130_bet'], name='out_overlay_image', description='')
    nodes['130_bet'].out_ports['out_mask_image'] = Port(node=nodes['130_bet'], name='out_mask_image', description='')
    nodes['130_bet'].out_ports['out_skull_image'] = Port(node=nodes['130_bet'], name='out_skull_image', description='')
    nodes['130_bet'].out_ports['out_mesh_image'] = Port(node=nodes['130_bet'], name='out_mesh_image', description='')
    
    nodes['131_const_dilate_brainmask_operation_0'] = run.create_node(name='131_const_dilate_brainmask_operation_0', type='constant', parent=nodes['5_BET_and_REG'], no_in_ports=0, no_out_ports=0)

    

    
    nodes['131_const_dilate_brainmask_operation_0'].out_ports['out_output'] = Port(node=nodes['131_const_dilate_brainmask_operation_0'], name='out_output', description='')
    

    # Links
    run.links.append(Link(from_node=nodes['79_transform_csf'], to_node=nodes['84_csf_spm'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['79_transform_csf'], to_node=nodes['83_spm_hard_segment'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['83_spm_hard_segment'], to_node=nodes['80_gm_map'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['87_transform_gm'], to_node=nodes['83_spm_hard_segment'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['89_transform_wm'], to_node=nodes['83_spm_hard_segment'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['87_transform_gm'], to_node=nodes['88_gm_spm'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['89_transform_wm'], to_node=nodes['90_wm_spm'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['83_spm_hard_segment'], to_node=nodes['73_wm_map'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['83_spm_hard_segment'], to_node=nodes['72_csf_map'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['71_const_edit_transform_file_set_0'], to_node=nodes['86_edit_transform_file'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['82_invert_gm'], to_node=nodes['86_edit_transform_file'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['74_const_invert_gm_parameters_0'], to_node=nodes['82_invert_gm'], data_type='ElastixParameterFile'))
    run.links.append(Link(from_node=nodes['76_reg_t1_to_spmtemplate'], to_node=nodes['82_invert_gm'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['86_edit_transform_file'], to_node=nodes['89_transform_wm'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['70_spm_segmantation'], to_node=nodes['89_transform_wm'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['86_edit_transform_file'], to_node=nodes['87_transform_gm'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['70_spm_segmantation'], to_node=nodes['87_transform_gm'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['86_edit_transform_file'], to_node=nodes['79_transform_csf'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['70_spm_segmantation'], to_node=nodes['79_transform_csf'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['75_t1_passthrough'], to_node=nodes['82_invert_gm'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['70_spm_segmantation'], to_node=nodes['82_invert_gm'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['75_t1_passthrough'], to_node=nodes['76_reg_t1_to_spmtemplate'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['85_const_reg_t1_to_spmtemplate_fixed_image_0'], to_node=nodes['76_reg_t1_to_spmtemplate'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['77_const_t1_passthrough_operator_0'], to_node=nodes['75_t1_passthrough'], data_type='String'))
    run.links.append(Link(from_node=nodes['81_T1_images'], to_node=nodes['75_t1_passthrough'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['91_transform_t1'], to_node=nodes['70_spm_segmantation'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['75_t1_passthrough'], to_node=nodes['91_transform_t1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['76_reg_t1_to_spmtemplate'], to_node=nodes['91_transform_t1'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['78_const_reg_t1_to_spmtemplate_parameters_0'], to_node=nodes['76_reg_t1_to_spmtemplate'], data_type='ElastixParameterFile'))
    run.links.append(Link(from_node=nodes['44_reformat_t1'], to_node=nodes['122_t1_with_brain'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['7_threshold_labels'], to_node=nodes['122_t1_with_brain'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['27_const_hammers_brain_transform_threads_0'], to_node=nodes['17_hammers_brain_transform'], data_type='Int'))
    run.links.append(Link(from_node=nodes['17_hammers_brain_transform'], to_node=nodes['121_combine_brains'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['44_reformat_t1'], to_node=nodes['123_t1_with_wm_outline'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['73_wm_map'], to_node=nodes['123_t1_with_wm_outline'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['44_reformat_t1'], to_node=nodes['42_t1_with_gm_outline'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['80_gm_map'], to_node=nodes['42_t1_with_gm_outline'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['125_dilate_brainmask'], to_node=nodes['12_t1w_registration'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['56_brain_mask_registation'], to_node=nodes['12_t1w_registration'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['64_hammers_brains_r5'], to_node=nodes['12_t1w_registration'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['28_n4_bias_correction'], to_node=nodes['12_t1w_registration'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['50_hammers_brains'], to_node=nodes['17_hammers_brain_transform'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['12_t1w_registration'], to_node=nodes['17_hammers_brain_transform'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['49_const_t1w_registration_parameters_0'], to_node=nodes['12_t1w_registration'], data_type='ElastixParameterFile'))
    run.links.append(Link(from_node=nodes['59_const_t1w_registration_threads_0'], to_node=nodes['12_t1w_registration'], data_type='Int'))
    run.links.append(Link(from_node=nodes['52_const_get_brain_volume_selection_0'], to_node=nodes['14_get_brain_volume'], data_type='TxtFile'))
    run.links.append(Link(from_node=nodes['30_combine_labels'], to_node=nodes['21_t1_with_hammers'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['18_const_get_wm_volume_flatten_0'], to_node=nodes['126_get_wm_volume'], data_type='Boolean'))
    run.links.append(Link(from_node=nodes['30_combine_labels'], to_node=nodes['11_get_gm_volume'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['60_const_get_brain_volume_volume_0'], to_node=nodes['14_get_brain_volume'], data_type='String'))
    run.links.append(Link(from_node=nodes['30_combine_labels'], to_node=nodes['126_get_wm_volume'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['90_wm_spm'], to_node=nodes['126_get_wm_volume'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['26_const_get_wm_volume_volume_0'], to_node=nodes['126_get_wm_volume'], data_type='String'))
    run.links.append(Link(from_node=nodes['45_const_get_wm_volume_selection_0'], to_node=nodes['126_get_wm_volume'], data_type='TxtFile'))
    run.links.append(Link(from_node=nodes['44_reformat_t1'], to_node=nodes['21_t1_with_hammers'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['30_combine_labels'], to_node=nodes['19_mask_hammers_labels'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['80_gm_map'], to_node=nodes['19_mask_hammers_labels'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['41_const_mask_hammers_labels_operator_0'], to_node=nodes['19_mask_hammers_labels'], data_type='String'))
    run.links.append(Link(from_node=nodes['121_combine_brains'], to_node=nodes['14_get_brain_volume'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['34_t1w_dicom'], to_node=nodes['20_t1_dicom_to_nifti'], data_type='DicomImageFile'))
    run.links.append(Link(from_node=nodes['122_t1_with_brain'], to_node=nodes['127_t1_with_brain_sink'], data_type='PngImageFile'))
    run.links.append(Link(from_node=nodes['44_reformat_t1'], to_node=nodes['81_T1_images'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['80_gm_map'], to_node=nodes['40_gm_map_output'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['129_const_get_gm_volume_selection_0'], to_node=nodes['11_get_gm_volume'], data_type='TxtFile'))
    run.links.append(Link(from_node=nodes['39_const_get_gm_volume_volume_0'], to_node=nodes['11_get_gm_volume'], data_type='String'))
    run.links.append(Link(from_node=nodes['42_t1_with_gm_outline'], to_node=nodes['62_t1_with_gm_outline_sink'], data_type='PngImageFile'))
    run.links.append(Link(from_node=nodes['8_hammers_t1w'], to_node=nodes['12_t1w_registration'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['44_reformat_t1'], to_node=nodes['28_n4_bias_correction'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['35_const_dilate_brainmask_radius_0'], to_node=nodes['125_dilate_brainmask'], data_type='Float'))
    run.links.append(Link(from_node=nodes['32_const_dilate_brainmask_operation_type_0'], to_node=nodes['125_dilate_brainmask'], data_type='__PxMorphology_0.3.0_interface__operation_type__Enum__'))
    run.links.append(Link(from_node=nodes['131_const_dilate_brainmask_operation_0'], to_node=nodes['125_dilate_brainmask'], data_type='__PxMorphology_0.3.0_interface__operation__Enum__'))
    run.links.append(Link(from_node=nodes['13_const_threshold_labels_upper_threshold_0'], to_node=nodes['7_threshold_labels'], data_type='Float'))
    run.links.append(Link(from_node=nodes['121_combine_brains'], to_node=nodes['7_threshold_labels'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['64_hammers_brains_r5'], to_node=nodes['56_brain_mask_registation'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['130_bet'], to_node=nodes['28_n4_bias_correction'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['33_label_registration'], to_node=nodes['30_combine_labels'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['53_const_label_registration_threads_0'], to_node=nodes['33_label_registration'], data_type='Int'))
    run.links.append(Link(from_node=nodes['124_const_combine_labels_number_of_classes_0'], to_node=nodes['30_combine_labels'], data_type='Int'))
    run.links.append(Link(from_node=nodes['63_const_brain_mask_registation_threads_0'], to_node=nodes['56_brain_mask_registation'], data_type='Int'))
    run.links.append(Link(from_node=nodes['51_const_combine_brains_number_of_classes_0'], to_node=nodes['121_combine_brains'], data_type='Int'))
    run.links.append(Link(from_node=nodes['58_const_combine_brains_method_0'], to_node=nodes['121_combine_brains'], data_type='__PxCombineSegmentations_0.3.2_interface__method__Enum__'))
    run.links.append(Link(from_node=nodes['12_t1w_registration'], to_node=nodes['33_label_registration'], data_type='ElastixTransformFile'))
    run.links.append(Link(from_node=nodes['36_hammers_labels'], to_node=nodes['33_label_registration'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['46_t1_qc'], to_node=nodes['54_t1_qc_sink'], data_type='PngImageFile'))
    run.links.append(Link(from_node=nodes['21_t1_with_hammers'], to_node=nodes['31_t1_with_hammers_sink'], data_type='PngImageFile'))
    run.links.append(Link(from_node=nodes['24_const_combine_labels_method_0'], to_node=nodes['30_combine_labels'], data_type='__PxCombineSegmentations_0.3.2_interface__method__Enum__'))
    run.links.append(Link(from_node=nodes['44_reformat_t1'], to_node=nodes['29_t1_nifti_images'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['44_reformat_t1'], to_node=nodes['46_t1_qc'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['48_const_get_brain_volume_flatten_0'], to_node=nodes['14_get_brain_volume'], data_type='Boolean'))
    run.links.append(Link(from_node=nodes['30_combine_labels'], to_node=nodes['23_multi_atlas_segm'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['73_wm_map'], to_node=nodes['16_wm_map_output'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['121_combine_brains'], to_node=nodes['14_get_brain_volume'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['7_threshold_labels'], to_node=nodes['57_hammer_brain_mask'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['14_get_brain_volume'], to_node=nodes['55_brain_volume'], data_type='CsvFile'))
    run.links.append(Link(from_node=nodes['11_get_gm_volume'], to_node=nodes['128_gm_volume'], data_type='CsvFile'))
    run.links.append(Link(from_node=nodes['123_t1_with_wm_outline'], to_node=nodes['37_t1_with_wm_outline_sink'], data_type='PngImageFile'))
    run.links.append(Link(from_node=nodes['88_gm_spm'], to_node=nodes['11_get_gm_volume'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['22_const_brain_mask_registation_parameters_0'], to_node=nodes['56_brain_mask_registation'], data_type='ElastixParameterFile'))
    run.links.append(Link(from_node=nodes['9_const_get_gm_volume_flatten_0'], to_node=nodes['11_get_gm_volume'], data_type='Boolean'))
    run.links.append(Link(from_node=nodes['130_bet'], to_node=nodes['125_dilate_brainmask'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['43_convert_brain'], to_node=nodes['38_threshold_brain'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['126_get_wm_volume'], to_node=nodes['25_wm_volume'], data_type='CsvFile'))
    run.links.append(Link(from_node=nodes['44_reformat_t1'], to_node=nodes['130_bet'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['20_t1_dicom_to_nifti'], to_node=nodes['44_reformat_t1'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['125_dilate_brainmask'], to_node=nodes['56_brain_mask_registation'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['61_const_t1_dicom_to_nifti_file_order_0'], to_node=nodes['20_t1_dicom_to_nifti'], data_type='String'))
    run.links.append(Link(from_node=nodes['47_const_convert_brain_component_type_0'], to_node=nodes['43_convert_brain'], data_type='__PxCastConvert_0.3.2_interface__component_type__Enum__'))
    run.links.append(Link(from_node=nodes['130_bet'], to_node=nodes['43_convert_brain'], data_type='NiftiImageFileCompressed'))
    run.links.append(Link(from_node=nodes['10_const_bet_fraction_threshold_0'], to_node=nodes['130_bet'], data_type='Float'))
    run.links.append(Link(from_node=nodes['15_const_bet_bias_cleanup_0'], to_node=nodes['130_bet'], data_type='Boolean'))

    return run

