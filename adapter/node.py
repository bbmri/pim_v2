import os, asyncio

# For generating random properties
from adapter.utilities import *
from adapter.port import Port
from adapter.job import Job
from adapter.signal import Signal
import adapter.log as log

# Create logger
logger = log.setup_custom_logger('as')

probs = list()

probs.append([100, 0, 5, 2, 3, 0])
probs.append([0, 100, 50, 50, 0, 0])
probs.append([20, 12, 100, 5, 2, 0])
probs.append([20, 12, 100, 1, 0, 4])
probs.append([0, 500, 30, 20, 100, 0])
probs.append([23, 1, 234, 50, 10, 0])
probs.append([0, 5, 300, 0, 4, 0])
probs.append([100, 50, 30, 5, 1, 0])

cumSums = []
pdfs = []

for prob in probs:
    pdf = []

    for id, sum in enumerate(prob):
        for i in range(sum):
            pdf.append(id)

    pdfs.append(pdf)


def sample_job_type(pdf):
    return random.choice(pdfs[pdf])


class Node:
    """Class that represents a node in a pipeline run"""

    def __init__(self, **kwargs):
        self.run = kwargs.get('run', None)  # Run
        self.name = kwargs.get('name', ran_name())  # Name, random if not provided
        self.title = kwargs.get('title', '')  # Title, camel cased name if not provided
        self.description = kwargs.get('description', ran_description())  # Description
        self.custom_data = kwargs.get('custom_data', ran_custom_data())  # Custom data, random if not provided
        self.parent = kwargs.get('parent', None)  # Parent node
        self.children = list()  # Child node(s)
        self.type = kwargs.get('type', ran_name(2))  # Type
        self.in_ports = kwargs.get('in_ports', dict())  # Input ports
        self.out_ports = kwargs.get('in_ports', dict())  # Output ports
        self.jobs = list()  # Jobs
        self.futures = list()
        self.no_in_ports = kwargs.get('no_in_ports', random.randint(1, 3))
        self.no_out_ports = kwargs.get('no_out_ports', random.randint(1, 3))
        self.in_nodes = list()
        self.out_nodes = list()

        if self.parent:
            self.parent.children.append(self)

    def is_leaf(self):
        """Determines if the node is a leaf node"""

        return len(self.children) == 0

    def is_group(self):
        """Determines if the node is a group node"""

        return len(self.children) > 0

    def is_source(self):
        """Determines if the node is a source node"""

        if not self.is_leaf():
            return False

        for name, in_port in self.in_ports.items():
            if in_port.is_connected:
                return False

        for name, out_port in self.out_ports.items():
            if out_port.is_connected:
                return True

        return False

    def schedule_jobs(self):
        """Generate jobs for the node"""

        no_jobs = int(os.environ.get('NO_JOBS_PER_LEAF'))

        for job_id in range(no_jobs):
            self.jobs.append(Job(id=job_id, run=self.run, node=self))

    def processing_complete(self):
        """If all jobs have completed"""

        jobs_done = True

        # Check the status of all asynchronous jobs
        for future in self.futures:
            if future._state == 'PENDING':
                jobs_done = False

        return jobs_done

    def job_ended(self, job):
        """Callback when a job has finished

        This callback allows us also to check if all jobs have finished, and take additional actions

        :param job: Job that ended
        """

        if self.processing_complete():
            for out_node in self.out_nodes:
                out_node.simulate()

    def simulate(self):
        """Simulate the node as though it is being processed in a HPC cluster

        """

        # All input nodes should have their processing completed
        for in_node in self.in_nodes:
            if not in_node.processing_complete():
                return

        # Schedule the jobs
        self.schedule_jobs()

        logger.debug('Simulating {} job(s) for "{}"'.format(len(self.jobs), self.name))

        # Start jobs asynchronously
        for job in self.jobs:
            self.futures.append(asyncio.ensure_future(job.start()))
            self.futures[-1].add_done_callback(self.job_ended)

    def generate_port(self, name):
        """Generate a port with a random name"""

        options = dict(name=name, node=self)
        return options['name'], Port(**options)

    def generate_ports(self):
        """Generate input and output ports"""

        # Generate random in/out ports for leaf nodes
        for in_port in range(self.no_in_ports):
            name, port = self.generate_port(name=ran_name(count=2))
            self.in_ports[name] = port

        for out_port in range(self.no_out_ports):
            name, port = self.generate_port(name=ran_name(count=2))
            self.out_ports[name] = port

    def add_jobs(self, sums=None):
        """Adds processing jobs for the node

        :type sums: Array that specifies how many jobs to generate for each status type (list index is status type)
        """

        if self.is_leaf():
            if sums:
                for s, sum in enumerate(sums):
                    for i in range(sum):
                        self.jobs.append(Job(id=len(self.jobs), status=s, node=self, run=self.run))
            else:
                no_jobs = int(os.environ.get('NO_JOBS_PER_LEAF', 10))
                pdf = random.randint(0, len(probs) - 1)
                self.jobs = [Job(id=j, node=self, run=self.run, status=sample_job_type(pdf)) for j in range(no_jobs)]

        if self.is_group():
            for child in self.children:
                child.add_jobs()

    def path(self):
        """Determines the absolute node path

        :return: Node path
        """

        if not self.parent:
            return self.name

        parent_path = self.parent.path()
        # print(parent_path)
        # if parent_path == '':
        #     return '{}'.format(parent_path, self.name)

        return '{}/{}'.format(parent_path, self.name)

    def ran_in_port(self):
        """Fetch random input port

        :return: Random input port
        """

        return random.choice(list(self.in_ports.values()))

    def ran_out_port(self):
        """Fetch random output port

        :return: Random output port
        """

        return random.choice(list(self.out_ports.values()))

    def __getstate__(self):
        return dict(name=self.name,
                    title=self.title,
                    description=self.description,
                    customData=self.custom_data,
                    children=self.children,
                    type=self.type,
                    inPorts=list(self.in_ports.values()),
                    outPorts=list(self.out_ports.values()))

    def __repr__(self):
        return str(self.__dict__)
