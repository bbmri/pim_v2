import os

from adapter.link import Link
from adapter.run import Run
from adapter.port import Port


def create_run(**kwargs):
    run = Run()
    run.name = '{{id}}'

    os.environ['RUN_NAME'] = run.name

    nodes = dict()

    nodes['root'] = run.create_node(name='root', parent=None)

    run.root = nodes['root']

    {% for group in groups %}
    nodes['{{group.id}}'] = run.create_node(name='{{group.id}}', description='Leaf', parent=nodes['{{group.parent_group}}']){% endfor %}

    {% for node in nodes %}
    nodes['{{node.id}}'] = run.create_node(name='{{node.id}}', type='{{node.type}}', parent=nodes['{{node.group_id}}'], no_in_ports=0, no_out_ports=0)

    {% for in_port in node.in_ports %}
    nodes['{{node.id}}'].in_ports['{{in_port.id}}'] = Port(node=nodes['{{node.id}}'], name='{{in_port.id}}', description=''){% endfor %}

    {% for out_port in node.out_ports %}
    nodes['{{node.id}}'].out_ports['{{out_port.id}}'] = Port(node=nodes['{{node.id}}'], name='{{out_port.id}}', description=''){% endfor %}
    {% endfor %}

    # Links{% for link in links %}
    run.links.append(Link(from_node=nodes['{{link.from_node}}'], to_node=nodes['{{link.to_node}}'], data_type='{{link.type}}')){% endfor %}

    return run


