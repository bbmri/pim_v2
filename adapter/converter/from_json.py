import json, jinja2, os, glob

pipelines = glob.glob('./*json')


def render(tpl_path, context):
    path, filename = os.path.split(tpl_path)
    return jinja2.Environment(
        loader=jinja2.FileSystemLoader(path or './')
    ).get_template(filename).render(context)


for pipeline in pipelines:
    file_name   = os.path.splitext(os.path.basename(pipeline))[0]
    run         = json.loads(open(file_name + '.json').read())

    context = {
        'id': file_name,
        'no_samples': 25,
        'groups': run['network']['groups'],
        'nodes': run['network']['nodes'],
        'links': run['network']['links']
    }

    result = render('template.py', context)

    text_file = open('./../examples/{}.py'.format(file_name), "w")
    text_file.write(result)
    text_file.close()
