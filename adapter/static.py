# Parsing command line arguments
import os, argparse, requests, json, jsonpickle, time, random, datetime, logging, asyncio

# Import model
from adapter.run import Run
from adapter.node import Node
from adapter.link import Link
from adapter.job import Job
from adapter.utilities import ran_name

import adapter.utilities as utils
import adapter.log as log

from adapter.examples.basic import create_run as create_basic_run
from adapter.examples.average_brain import create_run as create_average_brain_run
from adapter.examples.cocosco import create_run as create_cocosco_run
from adapter.examples.epi_fat import create_run as create_epi_fat_run
from adapter.examples.intracranial_volume import create_run as create_intracranial_volume_run
from adapter.examples.iris import create_run as create_iris_run

# Create logger
logger = log.setup_custom_logger('as')

# Display only warnings for the requests library
logging.getLogger("requests").setLevel(logging.WARNING)


def simulate_static(**kwargs):
    """Simulate run statically

    Add a single run (including a collection of jobs) to PIM at once

    :type kwargs: Keyword arguments
    """

    try:

        # Set process-wide environment variables
        os.environ['PIM_RUNS_URL']          = '{}/api/runs/'.format(kwargs.get('host'))
        os.environ['PIM_RUN_URL']           = '{}/api/runs/{}'.format(kwargs.get('host'), kwargs.get('run_name'))
        os.environ['PIM_JOBS_URL']          = '{}/api/runs/{}/jobs'.format(kwargs.get('host'), kwargs.get('run_name'))
        os.environ['NO_JOBS_PER_LEAF']      = str(kwargs.get('no_jobs_node', 10))  # Create n jobs per node
        os.environ['RANDOM_DESCRIPTION']    = str(kwargs.get('description', False))  # Fill description with random words
        os.environ['RANDOM_CUSTOM_DATA']    = str(kwargs.get('custom_data', False))  # Fill custom data with random items

        if 'initial_job_status' in kwargs:
            os.environ['INITIAL_JOB_STATUS'] = str(kwargs.get('initial_job_status'))

        if kwargs['run_name'] == 'basic':
            run = create_basic_run()

        if kwargs['run_name'] == 'average_brain':
            run = create_average_brain_run()

        if kwargs['run_name'] == 'cocosco':
            run = create_cocosco_run()

        if kwargs['run_name'] == 'epi_fat':
            run = create_epi_fat_run()

        if kwargs['run_name'] == 'intracranial_volume':
            run = create_intracranial_volume_run()

        if kwargs['run_name'] == 'iris':
            run = create_iris_run()

        # Create event loop
        event_loop = asyncio.get_event_loop()

        run.simulate()

        #  Periodically send added/updated jobs to the server
        event_loop.create_task(run.periodically_dispatch_jobs())

        # Start the event loop
        event_loop.run_forever()

    except Exception as e:
        logger.error(str(e))
        raise e


if __name__ == '__main__':

    # Set up the command-line arguments parser
    parser = argparse.ArgumentParser(description='Simulate single run statically')

    # Add options
    parser.add_argument('--host', help='PIM server URL', default='http://localhost:5000')
    parser.add_argument('--description', action='store_true', help='Add a random description to items')
    parser.add_argument('--custom_data', action='store_true', help='Add random custom data to items')
    parser.add_argument('--verbose', action='store_true', help='Extensive logger')
    parser.add_argument('--no_jobs_node', type=int, default=10, help='Number of jobs per leaf node')
    parser.add_argument('--start_delay_max', type=int, default=0, help='Start delay (seconds)')
    parser.add_argument('--initial_job_status', type=int, default=-1, help='Initial job status (-1 for random)')
    parser.add_argument('--save_run', action='store_true', help='Save run to disk in JSON format')
    parser.add_argument('--run_name', help='Example run name to simulate')

    # Parse the command line arguments
    arguments = parser.parse_args()

    # Convert to keyword arguments
    kwargs = vars(arguments)

    # Random seed based on current time
    random.seed(datetime.datetime.now())

    # Suspend the simulation for start_delay seconds
    kwargs['start_delay'] = 0

    if arguments.start_delay_max > 0:
        kwargs['start_delay'] = random.uniform(0, arguments.start_delay_max)
        print(kwargs['start_delay'])
        time.sleep(kwargs['start_delay'])

    # Perform the simulation
    simulate_static(**kwargs)
