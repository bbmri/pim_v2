
import os, logging, jsonpickle, requests, asyncio

# For generating random properties
from adapter.utilities import *


class Job:
    """Class that simulates a processing entity (job) in a cluster environment"""

    def __init__(self, **kwargs):
        job_id = kwargs.get('id', 0) + 1

        self.run            = kwargs.get('run', None)                           # Run
        self.node           = kwargs.get('node', None)                          # Node this port is attached to
        self.name           = 'job___{id:03d}'.format(id=job_id)                # Name, random if not provided
        self.title          = kwargs.get('title', '')                           # Title, camel cased name if not provided
        self.description    = kwargs.get('description', '')                     # Description
        self.custom_data    = kwargs.get('custom_data', dict())                 # Custom data, random if not provided
        self.status         = kwargs.get('status', 0)                           # Status

        self.set_status(self.status)

    def set_status(self, status):
        """Sets te job status and adds itself to the queue"""

        self.status = status

        # Add to the job queue in the run
        self.run.jobs[self.path()] = self

    @staticmethod
    def random_exit_status():
        """Generates a random exit status based on a PDF"""

        prob = [0, 0, 100, 1, 1, 0]
        pdf = []

        for id, sum in enumerate(prob):
            for i in range(sum):
                pdf.append(id)

        return random.choice(pdf)

    async def start(self):
        """Start job asynchronously"""

        # Change status to idle
        self.set_status(0)

        # Wait a while and then set the status to running
        await asyncio.sleep(random.uniform(1, 2))

        # Change status to idle
        self.set_status(1)

        # Wait a while and then set the status to random exit status
        await asyncio.sleep(random.uniform(1, 2))

        # Assign random exit status
        self.set_status(Job.random_exit_status())

    def path(self):
        """Absolute job path in run"""

        return '{}/{}'.format(self.node.path(), self.name)

    def __getstate__(self):
        return dict(path=self.path(),
                    title=self.title,
                    description=self.description,
                    status=self.status,
                    customData=self.custom_data)

    def __repr__(self):
        return str(self.__getstate__())
