
# For generating random properties
from adapter.utilities import *


class Link:
    """Class that represents links between node via ports

    """

    def __init__(self, **kwargs):
        self.node           = kwargs.get('node', None)                          # Node this port is attached to
        self.name           = kwargs.get('name', ran_name())                    # Name, random if not provided
        self.title          = kwargs.get('title', '')                           # Title, camel cased name if not provided
        # self.title          = kwargs.get('title', name_to_title(self.name))     # Title, camel cased name if not provided
        self.description    = kwargs.get('description', ran_description())      # Description
        self.custom_data    = kwargs.get('custom_data', ran_custom_data())      # Custom data, random if not provided
        self.data_type      = kwargs.get('data_type', ran_name())               # Data type
        self.from_node      = kwargs.get('from_node', None)                     # From node
        self.from_port      = kwargs.get('from_port', None)                     # From port
        self.to_node        = kwargs.get('to_node', None)                       # To node
        self.to_port        = kwargs.get('to_port', None)                       # To port

        if not self.from_port:
            try:
                self.from_port = self.from_node.ran_out_port()
            except:
                pass

        if not self.to_port:
            try:
                self.to_port = self.to_node.ran_in_port()
            except:
                pass

        self.from_port.out_links.append(self)
        self.to_port.in_links.append(self)

        self.to_node.in_nodes.append(self.from_node)
        self.from_node.out_nodes.append(self.to_node)

    def path(self):
        return '{}/{}'.format(self.node.path(), self.name)

    def __getstate__(self):
        from_port = self.from_port.path() if self.from_port else '{}/v_out_port'.format(self.from_node.path())
        to_port = self.to_port.path() if self.to_port else '{}/v_in_port'.format(self.to_node.path())

        return dict(name=self.name,
                    title=self.title,
                    description=self.description,
                    customData=self.custom_data,
                    dataType=self.data_type,
                    fromPort=from_port,
                    toPort=to_port)

    def __repr__(self):
        return str(self.__dict__)

# from adapter.log import logger
# from adapter.signal import Signal
#
#
# class Link:
#     """Class that facilitates graph flow connections between node ports
#
#     """
#
#     def __init__(self, from_node, from_port, to_node, to_port, link_type='default'):
#         self.id             = '{}@{} > {}@{}'.format(from_node.id, from_port.id, to_node.id, to_port.id)
#         self.from_node      = from_node
#         self.from_port      = from_port
#         self.to_node        = to_node
#         self.to_port        = to_port
#         self.link_type      = link_type
#         self.finished       = Signal(self)
#
#         self.from_port.connect(self)
#         self.to_port.connect(self)
#
#         # Inform us when the state input port finishes
#         self.from_port.finished.connect(self.on_input_finished)
#
#     def on_input_finished(self, input_port):
#         logger.debug('Link {0}:{1} finished'.format(self.from_node.id, self.from_port.id))
#
#         self.finished.fire()
#
#     @staticmethod
#     def connect(network, node_a, node_b, port_id_a=-1, port_id_b=-1, link_type='default'):
#         port_a = node_a.out_ports[port_id_a]
#         port_b = node_b.in_ports[port_id_b]
#
#         if not port_a or not port_b:
#             raise Exception("{0}:{1}".format(port_a, port_b))
#
#         network.links.append(Link(node_a, port_a, node_b, port_b, link_type))
#
#     def from_node_id(self):
#         if self.from_node:
#             return self.from_node.id
#
#         return ''
#
#     def from_port_id(self):
#         if self.from_port:
#             return self.from_port.id
#
#         return ''
#
#     def to_node_id(self):
#         if self.to_node:
#             return self.to_node.id
#
#         return ''
#
#     def to_port_id(self):
#         if self.to_port:
#             return self.to_port.id
#
#         return ''
#
#     def __getstate__(self):
#         return dict(id=self.id, from_node=self.from_node_id(), from_port=self.from_port_id(), to_node=self.to_node_id(),
#                     to_port=self.to_port_id(), type=self.link_type)
#
#     def __repr__(self):
#         return str(self.__dict__)
