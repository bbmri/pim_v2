
# For generating random properties
from adapter.utilities import *


class Port:
    """Class that represents a port in a pipeline run

    """

    def __init__(self, **kwargs):
        self.node           = kwargs.get('node', None)                          # Node this port is attached to
        self.name           = kwargs.get('name', ran_name())                    # Name, random if not provided
        self.title          = kwargs.get('title', '')                           # Title, camel cased name if not provided
        # self.title          = kwargs.get('title', name_to_title(self.name))     # Title, camel cased name if not provided
        self.description    = kwargs.get('description', ran_description())      # Description
        self.custom_data    = kwargs.get('custom_data', ran_custom_data())      # Custom data, random if not provided
        self.data_type      = kwargs.get('data_type', ran_name())               # Data type
        self.in_links       = list()
        self.out_links      = list()

    def is_connected(self):
        return len(self.in_links) > 0 or len(self.out_links) > 0

    def path(self):
        return '{}/{}'.format(self.node.path(), self.name)

    def __getstate__(self):
        return dict(name=self.name,
                    title=self.title,
                    description=self.description,
                    customData=self.custom_data,
                    dataType=self.data_type)

    def __repr__(self):
        return str(self.__dict__)
