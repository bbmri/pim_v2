class Signal(object):
    """Subject observer pattern class

    """

    def __init__(self, sender):
        super(Signal).__init__()

        self._handlers  = []
        self.sender     = sender

    def connect(self, handler):
        self._handlers.append(handler)

    def fire(self, *args, **kwargs):
        for handler in self._handlers:
            handler(self.sender, *args, **kwargs)
