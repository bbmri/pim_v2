
import os, random, time, datetime, json

# For generating random strings
from random_words import RandomWords

# Context manager for measuring
from contextlib import contextmanager

# Random words global instance
random_words = RandomWords()


def ran_name(count=1):
    """Generates a random name (lorem ipsum style)

    :type count: Number of words to generate
    :return: Random name
    """

    return '_'.join(random_words.random_words(count=count))


def name_to_title(name):
    """Converts a name to a title

    :type name: Name to convert
    :return: Title
    """

    return name.replace('_', ' ').title()


def ran_description(count=5):
    """Generates a random description (lorem ipsum style)

    :type count: Number of words to generate
    :return: Random description
    """

    description = ''

    if "RANDOM_DESCRIPTION" in os.environ:
        if eval(os.environ['RANDOM_DESCRIPTION']) == False:
            return description
    else:
        return description

    description = ' '.join(random_words.random_words(count=count))
    description = "%s%s" % (description[0].upper(), description[1:])

    return description


def ran_custom_data(no_level_items=2, no_levels=2):
    """Generates random custom data

    :type no_level_items: Number of items per level
    :type no_levels: Number of levels to generate
    :return: Random custom data
    """

    custom_data = dict()

    if "RANDOM_CUSTOM_DATA" in os.environ:
        if eval(os.environ['RANDOM_CUSTOM_DATA']) == False:
            return custom_data
    else:
        return custom_data

    def generate_level(parent, level=0):
        for item in range(no_level_items):
            item_id = random_words.random_word()
            # print(item_id)
            if level < no_levels:
                parent[item_id] = dict()
                generate_level(parent[item_id], level + 1)

            if level + 1 == no_levels:
                if random.random() < 0.5:
                    parent[item_id] = random_words.random_word()
                else:
                    if random.random() < 0.5:
                        parent[item_id] = random.randint(0, 1000)
                    else:
                        parent[item_id] = None

    generate_level(custom_data)

    return custom_data


@contextmanager
def elapsed_timer():
    """Measure elapsed time"""

    time_start = time.time()
    elapser = lambda: (time.time() - time_start) * 1000.0
    yield lambda: elapser()


def now_string():
    """Generates a datetime as a string

    :return: Datetime as string
    """

    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')


def pad_or_truncate(input_list, target_length):
    """Pads and possibly truncates a list to target_length

    :type input_list: Input list to pad
    :type target_length: Target length of the input list
    :rtype: Padded/truncated list
    """

    return input_list[:target_length] + [0] * (target_length - len(input_list))


class MyEncoder(json.JSONEncoder):
    """Limit precision of floating point numbers when printing JSON objects with numbers"""

    def encode(self, obj):
        if isinstance(obj, float):
            return format(obj, '.2f')
        return json.JSONEncoder.encode(self, obj)


def batchify(items, batch_size):
    """Divide list of items up in chunks of batch_size

    :type items: List of jobs
    :type batch_size: Batch size
    :rtype: Chunks
    """

    for i in range(0, len(items), batch_size):
        yield items[i:i + batch_size]