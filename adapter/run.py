import os, argparse, requests, json, jsonpickle, time, random, datetime, logging

# For generating random properties
from adapter.utilities import *
from adapter.node import *
import adapter.utilities as utils

# Create logger
import adapter.log as log
logger = log.setup_custom_logger('as')

# Display only warnings for the requests library
logging.getLogger("requests").setLevel(logging.WARNING)

class Run:
    """The run class represents an instance of a pipeline

    """

    def __init__(self, **kwargs):
        self.name = kwargs.get('name', ran_name(count=5))  # Name, random if not provided
        self.title = kwargs.get('title', '')  # Title, camel cased name if not provided
        # self.title          = kwargs.get('title', name_to_title(self.name))     # Title, camel cased name if not provided
        self.description = kwargs.get('description', ran_description())  # Description
        self.custom_data = kwargs.get('custom_data', ran_custom_data())  # Custom data, random if not provided
        self.user = kwargs.get('user', 'simulator')  # User
        self.assigned_to = kwargs.get('assigned_to', ['simulations'])  # Assigned to role groups
        self.root = None  # Root node
        self.links = list()  # Links
        self.nodes = list()  # Nodes in the run
        self.jobs = dict()  # List of jobs that are ready to be dispatched

    def create_node(self, **kwargs):
        """Create node from keyword arguments

        :param kwargs: Node options
        """

        # Add run to the keyword arguments
        kwargs['run'] = self

        # Create node from keyword arguments and add to the list of nodes
        self.nodes.append(Node(**kwargs))

        # self.nodes[-1].generate_ports()

        return self.nodes[-1]

    def json_payload(self):
        """Convert run to JSON

        :return: JSON payload
        """

        return jsonpickle.encode(self, unpicklable=False)

    def dispatch_run(self):
        """Dispatches the run to the server in an HTTP POST request"""

        runs_url = os.environ.get('PIM_RUNS_URL')

        with utils.elapsed_timer() as elapsed:
            stats = dict()

            stats['run'] = 'HTTP post {}{}'.format(runs_url, self.name)

            http_response = requests.post(runs_url, json=json.loads(self.json_payload()))

            if http_response.status_code != 200:
                raise Exception('HTTP post {}{} failed: {}'.format(runs_url, self.name, http_response.text))

            # Obtain API execution time
            stats['api'] = json.loads(http_response.text)['apiRequest']['duration']

            # Measure HTTP post round trip time
            stats['post'] = elapsed()

            logger.debug('{run}: total: {post:,.1f}ms, api: {api:,.1f}ms'.format(**stats))

    def dispatch_jobs(self, batch_size=10):
        """Dispatches jobs to the server in batch form using HTTP PUT requests

        :type batch_size: Number of jobs to dispatch in one request
        """

        jobs_url = os.environ.get('PIM_JOBS_URL')
        keys_to_dispatch = list(self.jobs.keys())[:batch_size]

        if len(keys_to_dispatch) > 0:
            logger.info('Dispatching {} jobs'.format(len(keys_to_dispatch)))

            jobs_payload = jsonpickle.encode([self.jobs[key] for key in keys_to_dispatch], unpicklable=False)
            requests.put(jobs_url, json=json.loads(jobs_payload))

        # Remove jobs that were dispatched
        for key_to_remove in keys_to_dispatch:
            del self.jobs[key_to_remove]

    @staticmethod
    def job_batches(jobs, batch_size=100):
        """Generate list of batches of large jobs list

        :type jobs: List of jobs
        :type batch_size: Batch size
        :rtype: List of job batches
        """

        for i in range(0, len(jobs), batch_size):
            yield jobs[i:i + batch_size]

    async def periodically_dispatch_jobs(self):
        """Periodically dispatches added/updated jobs to the server"""

        while True:
            self.dispatch_jobs()
            await asyncio.sleep(1)

    def simulate(self):
        self.dispatch_run()

        for node in self.nodes:
            if node.is_source():
                node.simulate()

    def __getstate__(self):
        return dict(name=self.name,
                    title=self.title,
                    description=self.description,
                    customData=self.custom_data,
                    user=self.user,
                    assignedTo=self.assigned_to,
                    root=self.root,
                    links=self.links)

    def __repr__(self):
        return str(self.__dict__)
