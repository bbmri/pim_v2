# Parsing command line arguments
import argparse, requests, json, logging, time

from subprocess import Popen

# Import utilities
import adapter.utilities as utils

# Import logger
import adapter.log as log

# Create logger
logger = log.setup_custom_logger('pf')

# Display only warnings for the requests library
logging.getLogger("requests").setLevel(logging.WARNING)


def remove_existing_runs(**kwargs):
    """Remove existing simulation runs from PIM

    :param kwargs: Options
    """

    runs_url = '{}/api/runs/'.format(kwargs.get('host'))
    response = requests.get('{}?user=simulator&page_size=100&page_index=0'.format(runs_url))
    existing_runs = json.loads(response.text)['runs']
    no_existing_runs = len(existing_runs)

    if no_existing_runs > 0:
        for existing_run in existing_runs:
            try:
                run_url = '{}{}'.format(runs_url, existing_run['name'])

                with utils.elapsed_timer() as elapsed:
                    result = requests.delete(run_url)

                    if result.status_code != 200:
                        raise Exception('Unable to remove {}: {}'.format(existing_run, result.text))

                    logger.debug('HTTP delete {}: {:,.1f} ms'.format(run_url, elapsed()))
            except Exception as e:
                logger.error(str(e))


if __name__ == "__main__":

    # Set up the command-line arguments parser
    parser = argparse.ArgumentParser(description='Simulate one or more runs statically or dynamically')

    default_runs = 'average_brain cocosco epi_fat intracranial_volume iris'

    # Add options
    parser.add_argument('--host', help='PIM server URL', default='http://localhost:5000')
    parser.add_argument('--runs', nargs='*', help='Number of runs to simulate', default=default_runs)
    parser.add_argument('--dynamic', action='store_true', help='Simulate run over time')
    parser.add_argument('--remove_existing', action='store_true', help='Remove existing runs')
    parser.add_argument('--description', action='store_true', help='Add a random description to items')
    parser.add_argument('--custom_data', action='store_true', help='Add random custom data to items')
    parser.add_argument('--no_jobs_node', type=int, default=1000, help='Number of jobs per leaf node')
    parser.add_argument('--verbose', action='store_true', help='Extensive logging')
    parser.add_argument('--start_delay_max', type=int, default=0, help='Random start delay maximum (seconds)')
    parser.add_argument('--save_run', action='store_true', help='Save run to disk in JSON format')

    # Parse the command line arguments
    arguments = parser.parse_args()

    # Convert to keyword arguments
    kwargs = vars(arguments)

    # Remove existing runs (user=simulator)
    if arguments.remove_existing:
        remove_existing_runs(**kwargs)

    # Process command
    command = list()

    command.append('python3')

    if arguments.dynamic:
        command.append('dynamic.py')
    else:
        command.append('static.py')

    command.append('--host={}'.format(arguments.host))

    if arguments.description:
        command.append('--description')

    if arguments.custom_data:
        command.append('--custom_data')

    if arguments.verbose:
        command.append('--verbose')

    command.append('--start_delay_max={}'.format(arguments.start_delay_max))
    command.append('--no_jobs_node={}'.format(arguments.no_jobs_node))

    if arguments.save_run:
        command.append('--save_run')

    command.append('')

    # Number of runs that are processed in parallel
    no_runs_parallel = 4

    # Running and finished processes
    processes = list()
    no_running = 0
    no_finished = 0

    no_runs = len(arguments.runs)

    while no_finished < no_runs:
        if no_running < no_runs_parallel and len(processes) < no_runs:
            command[-1] = '--run_name={}'.format(arguments.runs[len(processes)])
            process = { 'id': len(processes) + 1, 'handle': Popen(command), 'running': True}
            processes.append(process)
            no_running += 1
            logger.debug('Starting simulator {}'.format(process['id']))

        for process in processes:
            if process['running'] and process['handle'].poll() is not None:
                process['running'] = False
                no_running -= 1
                no_finished += 1
                logger.debug('Simulator {} finished'.format(process['id']))
                break
            else:
                time.sleep(0.1)
                continue

    logger.debug('All simulators have finished')
