
# Flask imports
from flask import request, g, current_app
from flask_restplus import Resource, reqparse

# SQL Alchemy imports
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import *
from sqlalchemy import desc

# Restplus imports
from server.api.restplus import api, PimJsonResponse, PimExceptionResponse

import server.orm as orm
import server.serialization as serialization

import adapter.utilities as utils

# API models
import server.api.model as model

import server.database as database

import json

ns = api.namespace('runs', description='Runs endpoints')

get_jobs_args = reqparse.RequestParser()
get_jobs_args.add_argument('node', type=str, required=False, default='root')
get_jobs_args.add_argument('filter', type=str, required=False, default='["idle", "running", "success", "failed", "cancelled"]')
get_jobs_args.add_argument('sort_column', type=str, required=False, default='name')
get_jobs_args.add_argument('sort_direction', type=str, required=False, default='ascending')
get_jobs_args.add_argument('page_size', type=int, required=False, default=10)
get_jobs_args.add_argument('page_index', type=int, required=False, default=0)


@ns.route('/<string:run_name>/jobs')
@api.doc(params={'run_name': 'The unique run name'})
class Jobs(Resource):
    # Maximum amount of jobs allowed per call
    max_no_jobs = 1000

    @api.expect(get_jobs_args)
    @api.response(200, 'Job(s) successfully fetched')
    @api.response(404, 'Unable to fetch job(s) due to a bad request')
    @api.response(500, 'Unable to fetch job(s) due to an internal server error')
    def get(self, run_name):
        """
        Get jobs
        """

        try:
            # Request parameters
            args = get_jobs_args.parse_args()

            # Sorting/pagination parameters
            node = args['node']
            sort_column = args['sort_column']
            sort_direction = args['sort_direction']
            page_index = args['page_index']
            page_size = args['page_size']

            sort_columns = [
                'title',
                'path',
                'status',
                'duration',
                'created',
                'modified'
            ]

            status_map = {
                'idle': 0,
                'running': 1,
                'success': 2,
                'failed': 3,
                'cancelled': 4,
                'undefined': 5,
            }

            status_filter = []
            # current_app.logger.info(args)

            for status_type in json.loads(args['filter']):
                status_filter.append(status_map[status_type])

            jobs_qry = orm.Job.query.join(orm.Run).filter(orm.Run.name == run_name)

            jobs_qry = jobs_qry.filter(orm.Job.node_path.like('{}/{}%'.format(run_name, node)))
            jobs_qry = jobs_qry.filter(orm.Job.status.in_(status_filter))

            if sort_column in sort_columns:
                # Obtain sorting ORM column
                column = getattr(orm.Job, sort_column)

                # Sorting
                if sort_direction == 'descending':
                    jobs_qry = jobs_qry.order_by(desc(column))
                else:
                    jobs_qry = jobs_qry.order_by(column)

            paginated_jobs = jobs_qry.limit(page_size).offset(page_index * page_size)

            # Serialize query results
            jobs = serialization.JobSchema(many=True).dump(paginated_jobs).data

            # Check run existence
            orm.Run.query.filter(orm.Run.name == run_name).one()

            aggregate_qry = orm.Aggregate.query.join(orm.Node).filter(orm.Node.path == '{}/{}'.format(run_name, node))

            aggregate = aggregate_qry.first().status

            return PimJsonResponse(200, aggregate=aggregate, jobs=jobs, noJobs=jobs_qry.count(), noFilteredJobs=jobs_qry.count())
        except NoResultFound as e:
            g.db_session.rollback()
            message = 'Unable to fetch job(s). Run does not exist: {}.'.format(run_name)
            return PimJsonResponse(404, message=message, error=str(e))
        except SQLAlchemyError as e:
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to fetch job(s) due to a database error.')
        except Exception as e:
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to fetch job(s) due to an unhandled error.')

    @api.expect([model.job])
    @api.response(200, 'Job(s) successfully updated')
    @api.response(404, 'Unable to updateSize job(s) due to a bad request')
    @api.response(500, 'Unable to updateSize job(s) due to an internal server error')
    def put(self, run_name):
        """
        Update jobs
        """

        try:
            # Current timestamp
            now = utils.now_string()

            # Jobs in JSON format
            jobs_json = request.get_json()

            # Determine the number of jobs provided
            no_jobs = len(jobs_json)

            if no_jobs > Jobs.max_no_jobs:
                message = 'Maximum number of jobs per API call exceeded {} > {}'.format(no_jobs, Jobs.max_no_jobs)
                return PimJsonResponse(400, message=message)

            # Check run existence
            run = orm.Run.query.filter(orm.Run.name == run_name).one()

            # Modify jobs
            result = database.set_jobs(run, now, jobs_json)

            # Run has changed
            run.modified = now

            # Commit the changes to the database
            g.db_session.commit()

            # Produce message for API consumer
            message = '{} job(s) updated.'.format(len(jobs_json))

            return PimJsonResponse(200, message=message, timeStamp=now, aggregationTime=result['aggregation_time'])
        except NoResultFound as e:
            g.db_session.rollback()
            message = 'Unable to add job(s). Run does not exist: {}.'.format(run_name)
            return PimJsonResponse(404, message=message, error=str(e))
        except SQLAlchemyError as e:
            # current_app.logger.info(e)
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to add job(s) due to a database error.')
        except Exception as e:
            # current_app.logger.info(e)
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to add job(s) due to an unhandled error.')
