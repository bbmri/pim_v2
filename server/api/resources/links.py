import json

# Flask imports
from flask import g, current_app
from flask_restplus import Resource, reqparse

# SQL Alchemy imports
from sqlalchemy import select, func, bindparam, and_, not_, or_, any_, join
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import *

# Restplus imports
from server.api.restplus import api, PimJsonResponse, PimExceptionResponse

# Import the database ORM
import server.orm as orm

# Import utilities
import adapter.utilities as utils

ns = api.namespace('runs', description='Runs endpoints')

get_links_args = reqparse.RequestParser()
get_links_args.add_argument('nodes', required=False, type=str, default='',
                            help='Nodes for which to fetch the links (e.g. ["/root/node_a", "/root/node_b"])')


# Todo: Add documentation


@ns.route('/<string:run_name>/links')
@api.doc(params={'run_name': 'The unique run name'})
class Links(Resource):
    @api.expect(get_links_args)
    @api.response(200, 'Links successfully fetched')
    @api.response(500, 'Unable to fetch links due to an internal server error')
    def get(self, run_name):
        """
        Get links
        """

        try:
            # Check run existence
            run = orm.Run.query.filter(orm.Run.name == run_name).one()

            # Parse arguments
            args = get_links_args.parse_args()

            nodes = None

            if args['nodes'] == '':
                return PimJsonResponse(400, message='Unable to fetch links, no nodes provided')
            else:
                try:
                    nodes = json.loads(args['nodes'].replace("'", '"'))
                except json.JSONDecodeError:
                    return PimExceptionResponse(message='Cannot filter nodes, unable to decode JSON.')

            if len(nodes) == 0:
                return PimJsonResponse(400, message='Unable to fetch links, no nodes provided')

            # Current timestamp
            now = utils.now_string()

            # Tables
            links_tbl = orm.Link.__table__
            link_node_refs_tbl = orm.LinkNodeRef.__table__

            # Generate the data that is to be inserted in the temporary table
            link_node_refs = [dict(node_path='{}/{}'.format(run.name, node), created=now) for node in nodes]

            # Insert temporary reference nodes (delete them at the end)
            g.db_session.execute(link_node_refs_tbl.insert().values(link_node_refs))
            g.db_session.commit()

            # Select the temporary reference nodes we just inserted and generate an alias for the query
            link_ref_nodes = select([link_node_refs_tbl]).where(link_node_refs_tbl.c.created == now).alias()

            # Use regular expression to deduce the node path from the port id
            from_node = func.regexp_replace(links_tbl.c.from_port_id, '\/[^\/]*(?:\/)?$', '')
            to_node = func.regexp_replace(links_tbl.c.to_port_id, '\/[^\/]*(?:\/)?$', '')

            # Count the number of from and to nodes based on the node path
            from_node_count = select([func.count('*')], link_ref_nodes.c.node_path == from_node).label('from_count')
            to_node_count = select([func.count('*')], link_ref_nodes.c.node_path == to_node).label('to_count')

            # If the from and to port are both in the list we have a visible link
            explicit_links_filter = and_(links_tbl.c.run_id == run.id, and_(from_node_count == 1, to_node_count == 1))

            # Query the links table with the filter
            explicit_links_qry = select([links_tbl]).where(explicit_links_filter)

            links = list()

            for row in g.db_session.execute(explicit_links_qry):
                link = dict()

                link['title'] = row.title
                link['description'] = row.description
                link['customData'] = row.custom_data
                link['fromNode'] = '/'.join(row.from_port_id.split('/')[1:-1])
                link['toNode'] = '/'.join(row.to_port_id.split('/')[1:-1])
                link['fromPort'] = row.from_port_id.split('/')[-1]
                link['toPort'] = row.to_port_id.split('/')[-1]
                link['dataType'] = row.data_type
                link['implicit'] = False

                links.append(link)

            # Use regular expression to deduce the from and to node parent
            from_node_parent = func.regexp_replace(links_tbl.c.from_port_id, '\/[^\/]\/+[^\/]+$', '')
            to_node_parent = func.regexp_replace(links_tbl.c.to_port_id, '\/[^\/]\/+[^\/]+$', '')

            # Determines if the link is visible based on the provided nodes
            include_semi_connected = or_(from_node_count == 1, to_node_count == 1)

            # Filter out orphaned links
            filter_out_orphan = and_(and_(from_node_count == 0, to_node_count == 0), from_node_parent != to_node_parent)

            # Filter implicit links
            implicit_links_filter = and_(from_node != to_node,
                                         and_(~explicit_links_filter, or_(include_semi_connected, filter_out_orphan)))

            implicit_links = dict()

            # Generate list of node paths
            node_paths = ['{}/{}'.format(run.name, node) for node in nodes]

            for row in g.db_session.execute(select([links_tbl]).where(and_(links_tbl.c.run_id == run.id, implicit_links_filter))):
                original_link = dict()

                original_link['title'] = row.title
                original_link['description'] = row.description
                original_link['customData'] = row.custom_data
                original_link['fromNode'] = '/'.join(row.from_port_id.split('/')[1:-1])
                original_link['toNode'] = '/'.join(row.to_port_id.split('/')[1:-1])
                original_link['fromPort'] = row.from_port_id.split('/')[-1]
                original_link['toPort'] = row.to_port_id.split('/')[-1]
                original_link['dataType'] = row.data_type

                link = dict()

                from_nodes = list()
                to_nodes = list()

                for node_leaf in node_paths:
                    if row.from_port_id.count(node_leaf) > 0:
                        from_nodes.append(node_leaf)
                    if row.to_port_id.count(node_leaf) > 0:
                        to_nodes.append(node_leaf)

                link['title'] = ''
                link['description'] = ''
                link['customData'] = row.custom_data
                link['fromNode'] = '/'.join(max(from_nodes, key=len).split('/')[1:])
                link['toNode'] = '/'.join(max(to_nodes, key=len).split('/')[1:])
                link['fromPort'] = 'v_out_port'
                link['toPort'] = 'v_in_port'
                link['implicit'] = True
                link['dataType'] = ''

                # Discard improper links
                if link['fromNode'] == link['toNode']:
                    continue

                link_name = '{} {}'.format(link['fromNode'], link['toNode'])

                if link_name in implicit_links:
                    implicit_links[link_name]['represents'].append(original_link)
                else:
                    implicit_links[link_name] = dict(represents=[original_link], **link)

            links.extend(list(implicit_links.values()))

            # Remove the temporary records from the temp node refs table
            g.db_session.execute(link_node_refs_tbl.delete().where(link_node_refs_tbl.c.created == now))
            g.db_session.commit()

            return PimJsonResponse(200, links=links)
        except NoResultFound as e:
            g.db_session.rollback()
            message = 'Unable to fetch link(s). Run does not exist: {}.'.format(run_name)
            return PimJsonResponse(404, message=message, error=str(e))
        except SQLAlchemyError as e:
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to fetch links due to database error.')
        except Exception as e:
            return PimExceptionResponse(message='Unable to fetch links due to an unhandled error.')
