
# Flask imports
from flask import request, g
from flask_restplus import Resource, reqparse

# SQL Alchemy imports
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import *
from sqlalchemy import desc

# Restplus imports
from server.api.restplus import api, PimJsonResponse, PimExceptionResponse

import server.orm as orm
import server.serialization as serialization

import adapter.utilities as utils

# API models
import server.api.model as model

import server.database as database

import json

ns = api.namespace('runs', description='Runs endpoints')


get_job_args = reqparse.RequestParser()
get_job_args.add_argument('path', type=str, required=True, default='')


@ns.route('/<string:run_name>/job')
@api.doc(params={'run_name': 'The unique run name'})
class Job(Resource):

    @api.expect(get_job_args)
    @api.doc(params={'path': 'The job path e.g. root/node/job_a'})
    @api.response(200, 'Job successfully fetched')
    @api.response(404, 'Unable to fetch job due to a bad request')
    @api.response(500, 'Unable to fetch job due to an internal server error')
    def get(self, run_name):
        """
        Get job
        """

        path = '{}/{}'.format(run_name, get_job_args.parse_args()['path'])

        try:
            try:
                orm.Run.query.filter(orm.Run.name == run_name).one()
            except NoResultFound as e:
                message = 'Unable fetch job, run {} does not exist.'.format(run_name)
                return PimJsonResponse(404, message=message, error=str(e))
            try:
                job = orm.Job.query.filter(orm.Job.path == path).one()
                return PimJsonResponse(200, job=serialization.JobSchema().dump(job).data)
            except NoResultFound as e:
                message = 'Unable fetch job, {} does not exist.'.format(path)
                return PimJsonResponse(404, message=message, error=str(e))
        except SQLAlchemyError as e:
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to fetch job due to a database error.')
        except Exception as e:
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to fetch job due to an unhandled error.')
