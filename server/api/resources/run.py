
# Flask imports
from flask import g
from flask_restplus import Resource

# Restplus imports
from server.api.restplus import api, PimJsonResponse, PimExceptionResponse

# Import the database ORM
import server.orm as orm

# Import the serialization schemas
import server.serialization as serialization

# SQL Alchemy exceptions
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import *

# Database operations
import server.database as database

ns = api.namespace('runs', description='Runs endpoints')


@ns.route('/<string:run_name>')
@api.doc(params={'run_name': 'The unique run name'})
class RunItem(Resource):
    @api.response(200, 'Run summary successfully fetched')
    @api.response(404, 'Unable to fetch run summary because the run does not exist')
    @api.response(500, 'Unable to fetch run summary due to an internal server error')
    def get(self, run_name):
        """
        Get run summary
        """

        try:
            run = serialization.RunShortSchema().dump(orm.Run.query.filter(orm.Run.name == run_name).one()).data
            return PimJsonResponse(200, run=run)
        except NoResultFound as e:
            message = 'Unable to fetch run. Run does not exist: {}.'.format(run_name)
            return PimJsonResponse(404, message=message, error=str(e))
        except SQLAlchemyError as e:
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to fetch run due to a database error.')
        except Exception as e:
            return PimExceptionResponse(message='Unable to fetch run summary due to an unhandled error.')

    @api.response(200, 'Run successfully deleted')
    @api.response(404, 'Unable to delete run due to a bad request')
    @api.response(500, 'Unable to delete run due to an internal server error')
    def delete(self, run_name):
        """
        Delete run
        """

        try:
            # Check run existence
            run = g.db_session.query(orm.Run).filter(orm.Run.name == run_name).one()

            # Delete the run
            database.delete_run(run)

            return PimJsonResponse(200, message='Run deleted.')
        except NoResultFound as e:
            message = 'Unable to delete run. Run does not exist: {}.'.format(run_name)
            return PimJsonResponse(404, message=message, error=str(e))
        except SQLAlchemyError as e:
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to delete run due to a database error.')
        except Exception as e:
            return PimExceptionResponse(message='Unable to delete run due to an unhandled error.')
