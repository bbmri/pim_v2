import sys, os, subprocess, pytz

from datetime import datetime

from flask_restplus import Resource

from server.api.restplus import api, PimJsonResponse, PimExceptionResponse

import server.orm as orm

ns = api.namespace('info', description='Info endpoints')


def get_git_revision_hash():
    return subprocess.check_output(['git', 'rev-parse', 'HEAD']).decode(sys.stdout.encoding).strip()


def get_git_revision_info(fmt):
    return subprocess.check_output(['git', 'log', '-1', '--date=iso', '--pretty=%{}'.format(fmt)]).decode(sys.stdout.encoding).strip()


@ns.route('/')
class Info(Resource):
    @api.response(200, 'Info successfully fetched')
    def get(self):
        """
        Get api information
        """

        try:
            home_dir = os.getenv("HOME")

            try:
                file = open('./../deploy_info'.format(home_dir), 'r')

                last_updated = file.readline()
                current_revision = file.readline()
            except IOError as e:
                last_updated = None
                current_revision = None

            deployment = {'lastUpdated': last_updated, 'currentRevision': current_revision}

            return PimJsonResponse(200, version=2, deployment=deployment)
        except Exception as e:
            return PimExceptionResponse(message='Unable to fetch info due to an unhandled error.')
