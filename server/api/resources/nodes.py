
import json

# Flask imports
from flask import g
from flask_restplus import Resource, reqparse

# SQL Alchemy imports
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import *

# Restplus imports
from server.api.restplus import api, PimJsonResponse, PimExceptionResponse

# Import the database ORM
import server.orm as orm

import server.database as database

ns = api.namespace('runs', description='Runs endpoints')

get_nodes_args = reqparse.RequestParser()
get_nodes_args.add_argument('max_depth', required=False, type=int)
get_nodes_args.add_argument('filter_nodes', required=False, type=str)

# Todo: Review documentation


@ns.route('/<string:run_name>/nodes')
@api.doc(params={'run_name': 'The unique run name'})
class Nodes(Resource):
    @api.expect(get_nodes_args)
    @api.doc(params={'max_depth': 'Max node depth to fetch'})
    @api.doc(params={'filter_nodes': 'Nodes to filter'})
    @api.response(200, 'Nodes successfully fetched')
    @api.response(400, 'Unable to fetch nodes due to a bad request')
    @api.response(500, 'Unable to fetch nodes due to an internal server error')
    def get(self, run_name):
        """
        Get nodes
        """

        try:
            # Check run existence
            run = orm.Run.query.filter(orm.Run.name == run_name).one()

            # Parse arguments
            args = get_nodes_args.parse_args()

            filter_nodes = list()

            if args['filter_nodes']:
                try:
                    filter_nodes = json.loads(args['filter_nodes'].replace("'", '"'))
                except json.JSONDecodeError:
                    return PimExceptionResponse(message='Cannot filter nodes, unable to decode JSON.')

            # Fetch nodes from the database
            nodes = database.get_nodes(run, filter_nodes, args['max_depth'])

            return PimJsonResponse(200, nodes=nodes)
        except NoResultFound as e:
            message = 'Unable to fetch aggregates. Run does not exist: {}.'.format(run_name)
            return PimJsonResponse(404, message=message, error=str(e))
        except SQLAlchemyError as e:
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to fetch nodes due to database error.')
        except Exception as e:
            return PimExceptionResponse(message='Unable to fetch nodes due to an unhandled error.')
