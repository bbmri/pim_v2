
import json

# Flask imports
from flask import g
from flask_restplus import Resource, reqparse

# SQL Alchemy imports
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm.exc import *

# Restplus imports
from server.api.restplus import api, PimJsonResponse, PimExceptionResponse

# Import the database ORM
import server.orm as orm

import server.database as database

ns = api.namespace('runs', description='Runs endpoints')

get_aggregates_args = reqparse.RequestParser()
get_aggregates_args.add_argument('nodes', required=False, type=str, default='', help='Nodes to fetch')

# Todo: Review documentation


@ns.route('/<string:run_name>/aggregates')
@api.doc(params={'run_name': 'The unique run name'})
class Aggregates(Resource):
    @api.expect(get_aggregates_args)
    @api.doc(params={'nodes': 'List of nodes for which to fetch the node aggregates (e.g. ["/root/node_a", "/root/node_b"])'})
    @api.response(200, 'Aggregates successfully fetched')
    @api.response(400, 'Unable to fetch aggregates due to a bad request')
    @api.response(500, 'Unable to fetch aggregates due to an internal server error')
    def get(self, run_name):
        """
        Get aggregated node status
        """

        try:
            # Check run existence
            run = orm.Run.query.filter(orm.Run.name == run_name).one()

            # Parse arguments
            args = get_aggregates_args.parse_args()

            nodes = None

            # Filter nodes, if specified
            if args['nodes'] == '':
                return PimJsonResponse(400, message='Unable to fetch links, no nodes provided')
            else:
                try:
                    nodes = json.loads(args['nodes'].replace("'", '"'))
                except json.JSONDecodeError:
                    return PimExceptionResponse(message='Cannot filter nodes, unable to decode JSON.')

            # Fetch aggregates from databases
            aggregates = database.get_aggregates(run, nodes)

            return PimJsonResponse(200, aggregates=aggregates)
        except NoResultFound as e:
            message = 'Unable to fetch aggregates. Run does not exist: {}.'.format(run_name)
            return PimJsonResponse(404, message=message, error=str(e))
        except SQLAlchemyError as e:
            g.db_session.rollback()
            return PimExceptionResponse(message='Unable to fetch aggregates due to database error.')
        except Exception as e:
            return PimExceptionResponse(message='Unable to fetch aggregates due to an unhandled error.')
