import json
from flask import g, current_app

# Import the database ORM
import server.orm as orm

# Import utilities
import adapter.utilities as utils

# SQL Alchemy imports
from sqlalchemy import select, func, and_, bindparam, case
from sqlalchemy.orm import aliased
from sqlalchemy.dialects.postgresql import array

# Todo: Review comments


def unset_job_status(run, job):
    """Unsets the job status and updates the aggregation hierarchy in a bottom-up fashion

    :param run: Run to aggregate
    :param job: Job to unset
    """

    # current_app.logger.info('Unset job {}'.format(job['path']))

    nodes_tbl = orm.Node.__table__
    aggregates_tbl = orm.Aggregate.__table__
    node_path = '/'.join(job['path'].split('/')[:-1])
    nodes_alias = aliased(orm.Node, name='nodes_alias')

    status_cte = select([
        nodes_tbl.c.id,
        nodes_tbl.c.parent_id,
        nodes_tbl.c.status]). \
        where(and_(nodes_tbl.c.run_id == run.id, nodes_tbl.c.path == node_path)). \
        cte(recursive=True)

    nodes_cte_alias = status_cte.alias()

    status_cte = status_cte.union_all(
        select([
            nodes_alias.id,
            nodes_alias.parent_id,
            nodes_cte_alias.c.status]).where(nodes_alias.id == nodes_cte_alias.c.parent_id))

    status_map = dict()

    for row in g.db_session.execute(select([aggregates_tbl], aggregates_tbl.c.node_id == status_cte.c.id)):
        record = dict(row)
        status_map[record['node_id']] = record['status']
        status_map[record['node_id']][job['status']] -= 1

    # current_app.logger.info(status_map)

    update_data = [dict(node=k, status=v) for k, v in status_map.items()]

    update_node_status_qry = aggregates_tbl.update().values({
        'status': bindparam('status')
    }).where(aggregates_tbl.c.node_id == bindparam('node'))

    g.db_session.execute(update_node_status_qry, update_data)
    g.db_session.commit()


def set_job_status(run, job):
    """Sets the job status and updates the aggregation hierarchy in a bottom-up fashion

    :param run: Run to aggregate
    :param job: Job to set
    """

    # current_app.logger.info('Set job {}'.format(job['path']))

    nodes_tbl = orm.Node.__table__
    aggregates_tbl = orm.Aggregate.__table__
    node_path = '{}/{}'.format(run.name, '/'.join(job['path'].split('/')[:-1]))
    nodes_alias = aliased(orm.Node, name='nodes_alias')

    status_cte = select([
        nodes_tbl.c.id,
        nodes_tbl.c.parent_id,
        nodes_tbl.c.status]). \
        where(and_(nodes_tbl.c.run_id == run.id, nodes_tbl.c.path == node_path)). \
        cte(recursive=True)

    nodes_cte_alias = status_cte.alias()

    status_cte = status_cte.union_all(
        select([
            nodes_alias.id,
            nodes_alias.parent_id,
            nodes_cte_alias.c.status]).where(nodes_alias.id == nodes_cte_alias.c.parent_id))

    status_map = dict()

    for row in g.db_session.execute(select([aggregates_tbl], aggregates_tbl.c.node_id == status_cte.c.id)):
        record = dict(row)
        status_map[record['node_id']] = record['status']
        status_map[record['node_id']][job['status']] += 1

    # current_app.logger.info(status_map)

    update_data = [dict(node=k, status=v) for k, v in status_map.items()]

    update_node_status_qry = aggregates_tbl.update().values({
        'status': bindparam('status')
    }).where(aggregates_tbl.c.node_id == bindparam('node'))

    g.db_session.execute(update_node_status_qry, update_data)
    g.db_session.commit()


def set_jobs(run, time_stamp, jobs):
    """Update jobs in run

    :param run: Run in which to modify the jobs
    :param time_stamp: Job time stamp to add
    :param jobs: Jobs to update (JSON)
    """

    # current_app.logger.info('Updating {} job(s)'.format(len(jobs)))

    jobs_tbl            = orm.Job.__table__
    nodes_tbl           = orm.Node.__table__
    aggregation_time    = 0

    for job in jobs:

        # Establish absolute db job path
        absolute_job_path = '{}/{}'.format(run.name, job['path'])

        # Create query to fetch the job
        job_qry = select([jobs_tbl]) \
            .where(jobs_tbl.c.path == absolute_job_path)

        status_available    = 'status' in job
        should_create       = g.db_session.execute(job_qry).rowcount == 0

        # Create a new job in the database when it does not exist yet
        if should_create:
            insert_job = dict()

            insert_job['path']          = '{}/{}'.format(run.name, job['path'])
            insert_job['title']         = job['path'].split('/')[-1]
            insert_job['description']   = job.get('description', '')
            insert_job['created']       = time_stamp
            insert_job['modified']      = time_stamp
            insert_job['custom_data']   = job['customData']
            insert_job['run_id']        = run.id
            insert_job['node_path']     = '{}/{}'.format(run.name, '/'.join(job['path'].split('/')[:-1]))
            insert_job['status']        = job['status'] if status_available else 0

            # Insert the job and commit the changes to the database
            g.db_session.execute(jobs_tbl.insert(), insert_job)
            g.db_session.commit()

            # Query to set the node ID of the job that was just added
            assign_node_id_qry = jobs_tbl. \
                update().values(node_id=select([nodes_tbl.c.id], nodes_tbl.c.path == jobs_tbl.c.node_path)). \
                where(and_(jobs_tbl.c.run_id == run.id, jobs_tbl.c.modified == time_stamp))

            # Execute the query and commit the changes to the database
            g.db_session.execute(assign_node_id_qry)
            g.db_session.commit()

        # Fetch current job from the database
        current_job = dict(g.db_session.execute(job_qry).fetchone())

        # Unset the job status if the job exists in the database
        if status_available:
            if not should_create:
                with utils.elapsed_timer() as unset_job_status_elapsed:
                    unset_job_status(run, current_job)

                aggregation_time += unset_job_status_elapsed()

            # Set the job status
            with utils.elapsed_timer() as set_job_status_elapsed:
                set_job_status(run, job)

            aggregation_time += set_job_status_elapsed()

        job['modified'] = time_stamp

        # Merge the new job onto the existing job
        merged_job = {**dict(current_job), **job}
        update_job = dict()

        # Dictionary keys to keep
        if 'title' in job:
            update_job['title'] = merged_job['title']

        if 'description' in job:
            update_job['description'] = merged_job['description']

        if 'customData' in job:
            update_job['custom_data'] = merged_job['customData']

        if 'status' in job:
            update_job['status'] = merged_job['status']

        update_job['modified'] = merged_job['modified']

        # Query to update the jobs table with the updated job dictionary
        update_job_qry = jobs_tbl.update() \
            .values(update_job) \
            .where(jobs_tbl.c.path == absolute_job_path)

        # Execute the query and commit the changes to the database
        g.db_session.execute(update_job_qry)
        g.db_session.commit()

    return dict(aggregation_time=aggregation_time)


def delete_run(run):
    """Removes a run from the database

    :type run: Run to remove
    """

    # synchronize_session = False)
    g.db_session.query(orm.Run).filter(orm.Run.id == run.id).delete(synchronize_session=False)

    # g.db_session.delete(run)
    g.db_session.commit()


def get_aggregates(run, nodes=None):
    """Get aggregated node status

    :type run: Run for which to get aggregated node status
    :type nodes: Filter node for which to get aggregated node status
    :rtype: Dictionary that maps node path to aggregated node status
    """

    # Tables
    nodes_tbl = orm.Node.__table__
    aggregates_tbl = orm.Aggregate.__table__

    # Regular expression to remove the run id from the node path
    node_path = func.regexp_replace(nodes_tbl.c.path, "^[^*]*?\/", '')

    # Basic filter on run id
    aggregate_filter = (nodes_tbl.c.run_id == run.id)

    # Filter on nodes if provided
    if nodes:
        aggregate_filter = and_(aggregate_filter, node_path.in_(nodes))

    # Create query to fetch node aggregates
    aggregates_qry = select([nodes_tbl.c.path, aggregates_tbl.c.status]) \
        .where(aggregate_filter) \
        .select_from(aggregates_tbl.join(nodes_tbl, aggregates_tbl.c.node_id == nodes_tbl.c.id))

    aggregates = dict()

    # Build a dictionary that maps node path to node status
    for aggregate in g.db_session.execute(aggregates_qry):
        aggregates['/'.join(aggregate.path.split('/')[1:])] = aggregate.status

    return aggregates


# ToDo: add documentation
def get_nodes(run, nodes=None, max_depth=-1):
    """Get nodes until max depth

    :type run: Run for which to get nodes
    :type nodes: Node for which to get aggregated node status
    :rtype: Dictionary that maps node path to aggregated node status
    """

    # Tables
    nodes_tbl = orm.Node.__table__
    in_ports_tbl = orm.InPort.__table__
    out_ports_tbl = orm.OutPort.__table__

    # Regular expression to remove the run id from the node path
    node_path = func.regexp_replace(nodes_tbl.c.path, "^[^*]*?\/", '')

    # Basic filter on run id
    filter = (nodes_tbl.c.run_id == run.id)

    # Filter on node path
    if nodes:
        filter = and_(filter, node_path.in_(nodes))

    # Filter on node path
    if max_depth and max_depth >= 0:
        filter = and_(filter, nodes_tbl.c.level <= max_depth)

    # Create query to fetch nodes
    nodes_qry = select([nodes_tbl]) \
        .where(filter)

    nodes = dict()

    def remove_run_from_path(path):
        return '/'.join(path.split('/')[1:])

    def port_name(path):
        return path.split('/')[-1]

    # Build a dictionary that maps node path to node properties
    for record in g.db_session.execute(nodes_qry):
        node = dict()

        node['path'] = remove_run_from_path(record.path)
        node['title'] = record.title
        node['description'] = record.description
        node['customData'] = record.custom_data
        node['type'] = record.type
        node['level'] = record.level
        node['children'] = list()

        # Todo: Integrate with nodes query
        children_qry = select([nodes_tbl.c.path]) \
            .where(nodes_tbl.c.parent_id == record.id)

        for child in g.db_session.execute(children_qry):
            node['children'].append('/'.join(child.path.split('/')[1:]))

        node['inPorts'] = dict()
        node['outPorts'] = dict()

        # Todo: Integrate with nodes query
        in_ports_qry = select(
            [in_ports_tbl.c.path, in_ports_tbl.c.title, in_ports_tbl.c.description, in_ports_tbl.c.custom_data]) \
            .where(in_ports_tbl.c.node_id == record.id)

        # Todo: Integrate with nodes query
        out_ports_qry = select(
            [out_ports_tbl.c.path, out_ports_tbl.c.title, out_ports_tbl.c.description, out_ports_tbl.c.custom_data]) \
            .where(out_ports_tbl.c.node_id == record.id)

        for p in g.db_session.execute(in_ports_qry):
            node['inPorts'][port_name(p.path)] = dict(name=port_name(p.path), title=p.title, description=p.description,
                                                      customData=p.custom_data)

        for p in g.db_session.execute(out_ports_qry):
            node['outPorts'][port_name(p.path)] = dict(name=port_name(p.path), title=p.title, description=p.description,
                                                       customData=p.custom_data)

        nodes[remove_run_from_path(record.path)] = node

    return nodes
