#!/bin/bash

until PGPASSWORD=pim psql -h "pim_db" -U "pim" -p "5432" -c '\l'; do
  >&2 echo "PIM database is unavailable - sleeping"
  sleep 1
done

>&2 echo "PIM PostgreSQL database is up"

# Development Flask application with debugging and API logging
if [ $APP_CONTEXT == "development" ]; then
    >&2 echo "Starting development Flask application"
    cd /pim/server && python3 app.py --database_uri=postgresql://pim:pim@pim_db:5432/pim --flask_debug --flask_werkzeug_logging
fi

# Production Flask application (recommended for testing only)
if [ $APP_CONTEXT == "production" ]; then
    >&2 echo "Starting production Flask application"
    cd /pim/server && python3 app.py --database_uri=postgresql://pim:pim@pim_db:5432/pim
fi

# Production Flask application with uWSGI
if [ $APP_CONTEXT == "production_uwsgi" ]; then
    >&2 echo "Starting production uWSGI Flask application"
    gunicorn -w 1 --bind 0.0.0.0:5000 "server.app:create_app_from_config('production')" -k gevent
fi