import os

from flask import Blueprint, g, jsonify, make_response

home_bp = Blueprint('home', __name__, url_prefix='')


@home_bp.route('/version')
def version():
    return make_response(jsonify({'version': 2}), 200)