from marshmallow import Schema, fields, ValidationError, pre_load, post_load, pre_dump, post_dump

# Object relational mappings
from server.orm import *


class NodeSchema(Schema):
    id          = fields.Integer(dump_only=True)
    title       = fields.String(default='')
    description = fields.String(default='')
    custom_data = fields.Dict(load_from='customData', dump_to='customData')
    path        = fields.String(dump_only=True)
    type        = fields.String()
    in_ports    = fields.Nested('InPortSchema', many=True, load_from='inPorts', dump_to='inPorts', dump_only=True)
    out_ports   = fields.Nested('OutPortSchema', many=True, load_from='outPorts', dump_to='outPorts', dump_only=True)

    @post_load
    def make_node(self, data):
        return Node(**data)


class InPortSchema(Schema):
    id          = fields.Integer(dump_only=True)
    title       = fields.String(default='')
    description = fields.String(default='')
    custom_data = fields.Dict(load_from='customData', dump_to='customData')

    @post_load
    def make_in_port(self, data):
        return InPort(**data)


class OutPortSchema(Schema):
    id          = fields.Integer(dump_only=True)
    title       = fields.String(default='')
    description = fields.String(default='')
    custom_data = fields.Dict(load_from='customData', dump_to='customData')

    @post_load
    def make_out_port(self, data):
        return OutPort(**data)


class LinkSchema(Schema):
    id              = fields.Integer(dump_only=True)
    title           = fields.String(load_only=True)
    description     = fields.String(load_only=True)
    custom_data     = fields.Dict(load_from='customData', dump_to='customData')
    type            = fields.String()
    from_port_id    = fields.String(load_from='fromPort', dump_to='fromPort')
    to_port_id      = fields.String(load_from='toPort', dump_to='toPort')
    data_type       = fields.String(load_from='dataType', dump_to='dataType')

    @post_load
    def make_link(self, data):
        return Link(**data)


class RunSchema(Schema):
    id          = fields.Integer(dump_only=True)
    name        = fields.String()
    title       = fields.String(default='')
    description = fields.String(default='')
    created     = fields.String(dump_only=True)
    modified    = fields.String(dump_only=True)
    custom_data = fields.Dict(load_from='customData', dump_to='customData')
    user        = fields.String()
    nodes       = fields.Nested('NodeSchema', many=True, dump_only=True)
    links       = fields.Nested('LinkSchema', many=True, dump_only=True)

    @post_load
    def make_run(self, data):
        return Run(**data)


class RunShortSchema(Schema):
    name        = fields.String(dump_only=True)
    created     = fields.String(dump_only=True)
    modified    = fields.String(dump_only=True)
    user        = fields.String(dump_only=True)
    status      = fields.Function(lambda obj: obj.nodes[0].aggregate.status)
    progress    = fields.Float(dump_only=True)


class JobSchema(Schema):
    path        = fields.String(default='')
    title       = fields.String(default='')
    description = fields.String(default='')
    created     = fields.String(dump_only=True)
    modified    = fields.String(dump_only=True)
    custom_data = fields.Dict(load_from='customData', dump_to='customData')
    status      = fields.Integer()
    node_id     = fields.String(load_from='node', load_only=True)
    duration    = fields.String(dump_only=True)

    @post_load
    def make_job(self, data):
        return Job(**data)
