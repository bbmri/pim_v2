import argparse
import configparser
import logging
import os
import time

# Flask imports
from flask import Flask, Blueprint, g, current_app

# Do our monkey patching
from gevent.monkey import patch_all
from psycogreen.gevent import patch_psycopg

# Sentry
from raven.contrib.flask import Sentry
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

# Import the database ORM
import server.orm as orm
from server.api.restplus import api, PimJsonResponse

# Resources
from server.views.home import home_bp
from server.api.resources.run import ns as run_ns
from server.api.resources.runs import ns as runs_ns
from server.api.resources.aggregates import ns as aggregates_ns
from server.api.resources.nodes import ns as nodes_ns
from server.api.resources.jobs import ns as jobs_ns
from server.api.resources.job import ns as job_ns
from server.api.resources.links import ns as links_ns
from server.api.resources.info import ns as info_ns

from gevent.monkey import patch_all
patch_all()
from psycogreen.gevent import patch_psycopg
patch_psycopg()

# patch_all()
# patch_psycopg()

werkzeug_logger = logging.getLogger('werkzeug')

# Sentry instance
sentry = Sentry(dsn='https://ce035f93fa3e4e4d898b3ab4a57e5dcd:90feeb87a7f4469b870cb3674c12afde@sentry.io/216476')

from flask_restplus import Api as rpapi
import flask



@property
def specs_url(self):
    """Fixes issue where swagger-ui makes a call to swagger.json over HTTP (mixed-content problem).
       This can ONLY be used on servers that actually use HTTPS.  On servers that use HTTP,
       this code should not be used at all.
    """
    return flask.url_for(self.endpoint('specs'), _external=True, _scheme='https')


def create_app(**kwargs):
    """Creates a custom PIM-Flask application

    :param kwargs: PIM-Flask configuration
    :return: Flask application
    """

    # Defaults from kwargs
    flask_debug             = bool(kwargs.get('flask_debug', False))
    flask_profile           = bool(kwargs.get('flask_profile', False))
    database_uri            = kwargs.get('database_uri', '')
    aggregation_no_items    = int(kwargs.get('aggregation_no_items', 6))

    # Create flask application object
    app = Flask(__name__)


    # Configure application
    app.config['SWAGGER_UI_DOC_EXPANSION']          = 'list'
    app.config['SWAGGER_UI_JSONEDITOR']             = False
    app.config['RESTPLUS_VALIDATE']                 = True
    app.config['RESTPLUS_MASK_SWAGGER']             = False
    app.config['ERROR_404_HELP']                    = False
    app.config['SQLALCHEMY_DATABASE_URI']           = database_uri
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS']    = False
    # app.config['SQLALCHEMY_ECHO']                   = True
    app.config['SQLALCHEMY_POOL_SIZE']              = 20

    # Get application context
    if 'APP_CONTEXT' in os.environ:
        app_context = os.environ['APP_CONTEXT']

        # Override the default specs URL with one that returns an HTTPS scheme
        if app_context.startswith('production'):
            rpapi.specs_url = specs_url

        # Enable application debugging in the context of development
        if app_context == 'development':
            flask_debug = True

    # Set the number of items per node to aggregate
    app.aggregation_no_items = aggregation_no_items

    # Create api blueprint
    api_bp = Blueprint('api', __name__, url_prefix='/api')

    # Initialize rest api blueprint
    api.init_app(api_bp)

    # Register blueprints
    app.register_blueprint(api_bp)

    # Set application logger
    app.log = logging.getLogger('app')

    # If the user selects the profiling option, then we need to do a little extra setup
    if flask_profile:
        from werkzeug.contrib.profiler import ProfilerMiddleware

        app.config['PROFILE']   = True
        app.wsgi_app            = ProfilerMiddleware(app.wsgi_app, restrictions=[30])
        app.debug               = True

    # Flask debugging on/off
    app.debug = flask_debug

    app.db_engine = create_engine(database_uri, convert_unicode=True, pool_size=30, max_overflow=0)

    app.db_engine.pool._use_threadlocal = True

    orm.Base.metadata.create_all(bind=app.db_engine)

    # Obtain the type of Flask application configuration
    config = kwargs.get('config', '')

    # Initialize Sentry in production Flask applications only
    if config == 'production':
        sentry.init_app(app)

    @app.before_request
    def before_request():
        g.time_start = time.time()
        g.db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=current_app.db_engine))

        orm.Base.query = g.db_session.query_property()
    # @app.teardown_request
    # def shutdown_session(exception=None):
    #     g.db_session =

    @app.after_request
    def per_request_callbacks(response):
        g.db_session.remove()

        time_start = g.get('time_start')

        if isinstance(response, PimJsonResponse):
            response.finalize(time_start)

        return response

    return app


def create_app_from_config(config=''):
    """Creates a PIM-Flask application from a template

    :param config: Type of application configuration
    :param kwargs: PIM-Flask configuration name
    :return: Flask application
    """

    # Available configurations
    configs = ['development', 'production', 'unit_test_ci', 'unit_test_local']

    if config not in configs:
        raise Exception('{} not available!'.format(config))

    # Create the config parser
    config_parser = configparser.ConfigParser()

    # Get absolute PIM server directory name
    server_path = os.path.dirname(__file__)

    # Read from configuration file
    config_parser.read('{}/config/{}.cfg'.format(server_path, config))

    arguments = dict()

    # Add Flask configuration type to the arguments
    arguments['config'] = config

    # Establish database settings
    arguments['flask_debug']            = bool(config_parser.get('flask', 'debug'))
    arguments['flask_testing']          = bool(config_parser.get('flask', 'testing'))
    arguments['database_uri']           = config_parser.get('database', 'uri')
    arguments['aggregation_no_items']   = config_parser.get('aggregation', 'no_items')

    return create_app(**arguments)


def main():
    """Stand-alone PIM application

    """

    # Set up the command-line arguments parser
    parser = argparse.ArgumentParser(description='PIM - Webserver')

    # Create Flask option group and options
    flask_group = parser.add_argument_group('Flask', 'Flask options')

    flask_group.add_argument('--flask_host', help='Hostname of the Flask app', default='0.0.0.0')
    flask_group.add_argument('--flask_port', type=int, help='Port for the Flask app ', default=5000)
    flask_group.add_argument('--flask_debug', action='store_true', dest='flask_debug', help='Flask debugging')
    flask_group.add_argument('--flask_profile', action='store_true', dest='flask_profile', help='Flask profiling')
    flask_group.add_argument('--flask_werkzeug_logging', action='store_true', dest='flask_werkzeug_logging', help='Flask Werkzeug logging')

    # Create database option group
    database_group = parser.add_argument_group('Database', 'Database options')

    # Add options
    database_group.add_argument('--database_uri', dest='database_uri', default='', help='Database URI')

    # Create aggregates option group
    aggregates_group = parser.add_argument_group('Aggregation', 'Aggregation options')

    # Add options
    aggregates_group.add_argument('--aggregation_no_items', type=int, dest='aggregation_no_items', default=6,
                             help='Number of aggregation items')

    # Parse the command line arguments
    arguments = parser.parse_args()

    # Create the flask application and set up the database
    app = create_app(**vars(arguments))

    # Enable/disable werkzeug logging
    if not arguments.flask_werkzeug_logging:
        werkzeug_logger.disabled = True

    # Run the Flask web server
    app.run(debug=arguments.flask_debug, host=arguments.flask_host, port=int(arguments.flask_port))


if __name__ == "__main__":
    main()
