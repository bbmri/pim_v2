
# SQL Alchemy
from sqlalchemy import Column, String, Text, DateTime, ForeignKey, Float, Integer, ARRAY, func
from sqlalchemy.orm import relationship, backref, column_property, object_session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import select, and_, cast, case

from flask import request, g, current_app

# Support for JSON column types
from sqlalchemy_utils.types.json import JSONType

import datetime

Base = declarative_base()

# Todo: Review documentation


def now():
    return datetime.datetime.utcnow


class Aggregate(Base):
    __tablename__ = "aggregates"

    # Fields
    id      = Column(Integer(), primary_key=True, autoincrement=True)                                   # User ID
    status  = Column(ARRAY(Integer))                                                                    # Aggregated number of jobs per type
    node_id = Column(Integer(), ForeignKey('nodes.id', ondelete="CASCADE"), nullable=True, index=True)  # Reference to node


class Node(Base):
    __tablename__ = "nodes"

    # Fields
    id          = Column(Integer(), primary_key=True, autoincrement=True)       # Unique ID
    path        = Column(String(256), nullable=True, unique=True, index=True)   # Absolute path
    title       = Column(String(128), nullable=True)                            # Title in UI
    description = Column(Text(), nullable=True)                                 # Description in UI
    created     = Column(DateTime, default=now())                               # Creation date/time
    modified    = Column(DateTime, default=now())                               # Modification date/time
    custom_data = Column(JSONType)                                              # Custom data in JSON format
    type        = Column(String(128))                                           # Type
    parent_id   = Column(Integer(), ForeignKey('nodes.id'), index=True)         # Reference to parent node
    run_id      = Column(Integer(), ForeignKey('runs.id', ondelete="CASCADE"))  # Reference to run
    status      = Column(ARRAY(Integer))                                        # Aggregated number of jobs per type
    level       = Column(Integer())

    # Relationships
    children    = relationship('Node', backref=backref('parent', remote_side='Node.id'))
    jobs        = relationship('Job', backref='node', cascade='all, delete')
    in_ports    = relationship('InPort', backref='node', cascade='all, delete')
    out_ports   = relationship('OutPort', backref='node', cascade='all, delete')
    aggregate   = relationship('Aggregate', backref='node', uselist=False, primaryjoin="Node.id == Aggregate.node_id", cascade='all, delete')


class Job(Base):
    __tablename__ = "jobs"

    # Fields
    id              = Column(Integer(), primary_key=True, autoincrement=True)                       # Unique ID
    path            = Column(String(256), nullable=True, index=True)                                # Absolute path
    title           = Column(String(128), nullable=True)                                            # Title in UI
    description     = Column(Text(), nullable=True)                                                 # Description in UI
    created         = Column(DateTime, default=now())                                               # Creation date/time
    modified        = Column(DateTime, default=now())                                               # Modification date/time
    custom_data     = Column(JSONType)                                                              # Custom data in JSON format
    run_id          = Column(Integer(), ForeignKey('runs.id', ondelete="CASCADE"), nullable=False)  # Reference to run
    node_id         = Column(Integer())                                                             # Reference to node
    node_path       = Column(String(256), ForeignKey('nodes.path'), nullable=False)                 # Reference to node path
    status          = Column(Integer, nullable=False)                                               # Job status
    duration        = column_property(modified - created)                                           # Duration


class Run(Base):
    __tablename__ = "runs"

    # Fields
    id          = Column(Integer(), primary_key=True, autoincrement=True)       # Run ID
    name        = Column(String(128), nullable=False, unique=True)              # Run name (internal use only)
    title       = Column(String(128), nullable=True)                            # Run title
    description = Column(Text(), nullable=True)                                 # Run description
    created     = Column(DateTime, default=now())                               # Creation date/time
    modified    = Column(DateTime, default=now())                               # Modification date/time
    custom_data = Column(JSONType)                                              # Custom data in JSON format
    user        = Column(String(128), nullable=True)                            # User

    # Relationships
    nodes   = relationship('Node', backref='run', order_by='Node.id', cascade='all, delete')
    links   = relationship('Link', backref='run', cascade='all, delete')

    nodes_tbl = Node.__table__
    jobs_tbl = Job.__table__
    aggregates_tbl = Aggregate.__table__

    # Number of finished jobs: success + failed
    finished = aggregates_tbl.c.status[3] + aggregates_tbl.c.status[4]

    # Total number of jobs in total: idle + running + success + failed + cancelled + undefined
    total = aggregates_tbl.c.status[1] + aggregates_tbl.c.status[2] + aggregates_tbl.c.status[3] + \
            aggregates_tbl.c.status[4] + aggregates_tbl.c.status[5] + aggregates_tbl.c.status[6]

    # Compute progress and avoid division by zero
    progress = case([(total == 0, 0)], else_=(finished / cast(total, Float)))

    # Create query to obtain the progress using the root aggregate node
    aggregate_progress_qry = select([progress]).where(
        and_(nodes_tbl.c.run_id == id, nodes_tbl.c.level == 0)).select_from(
        aggregates_tbl.join(nodes_tbl, aggregates_tbl.c.node_id == nodes_tbl.c.id))

    # Create progress column
    progress = column_property(aggregate_progress_qry)

    # Create duration column
    duration = column_property(modified - created)


    jobs_qry = select([jobs_tbl.c.id]).where(jobs_tbl.c.run_id == id)

    # Create query to count the number of jobs
    no_jobs_qry = select([func.count()]).select_from(jobs_tbl).where(jobs_tbl.c.run_id == id)

    # Create number of jobs column
    no_jobs = column_property(no_jobs_qry)

class InPort(Base):
    __tablename__ = 'in_ports'

    # Fields
    id          = Column(Integer(), primary_key=True, autoincrement=True)               # Unique ID
    path        = Column(String(256), nullable=False, unique=True)                      # Absolute path
    title       = Column(String(128), nullable=True)                                    # Title in UI
    description = Column(Text(), nullable=True)                                         # Description in UI
    created     = Column(DateTime, default=now())                                       # Creation date/time
    modified    = Column(DateTime, default=now())                                       # Modification date/time
    custom_data = Column(JSONType)                                                      # Custom data in JSON format
    node_id     = Column(Integer(), ForeignKey('nodes.id', ondelete="CASCADE"))         # Node ID the input port belongs to

    # Relationships
    links = relationship('Link', backref='to_port')


class OutPort(Base):
    __tablename__ = 'out_ports'

    # Fields
    id          = Column(Integer(), primary_key=True, autoincrement=True)               # Unique ID
    path        = Column(String(256), nullable=False, unique=True)                      # Absolute path
    title       = Column(String(128), nullable=True)                                    # Title in UI
    description = Column(Text(), nullable=True)                                         # Description in UI
    created     = Column(DateTime, default=now())                                       # Creation date/time
    modified    = Column(DateTime, default=now())                                       # Modification date/time
    custom_data = Column(JSONType)                                                      # Custom data in JSON format
    node_id     = Column(Integer(), ForeignKey('nodes.id', ondelete="CASCADE"))         # Node ID the output port belongs to

    # Relationships
    links = relationship('Link', backref='from_port')


class Link(Base):
    __tablename__ = "links"

    # Fields
    id                  = Column(Integer(), primary_key=True, autoincrement=True)                       # Unique ID
    title               = Column(String(128), nullable=True)                                            # Title in UI
    description         = Column(Text(), nullable=True)                                                 # Description in UI
    created             = Column(DateTime, default=now())                                               # Creation date/time
    modified            = Column(DateTime, default=now())                                               # Modification date/time
    custom_data         = Column(JSONType)                                                              # Custom data in JSON format
    run_id              = Column(Integer(), ForeignKey('runs.id', ondelete="CASCADE"), nullable=False)  # Reference to run
    from_port_id        = Column(String(256), ForeignKey('out_ports.path'), nullable=False)             # From port reference (path)
    to_port_id          = Column(String(256), ForeignKey('in_ports.path'), nullable=False)              # To port reference (path)
    data_type           = Column(String(128), nullable=True)
    common_ancestor     = Column(String(256), ForeignKey('nodes.path'), nullable=False)                 # Reference to the closest common ancestor


class LinkNodeRef(Base):
    """Temporary table for the links endpoint"""

    __tablename__ = "link_node_refs"

    id          = Column(Integer(), primary_key=True, autoincrement=True)       # Primary key
    node_id     = Column(Integer(), nullable=True, index=True)                  # Reference to node
    node_path   = Column(String(256), nullable=False, index=True)               # Reference to node path
    created     = Column(DateTime, default=now())                               # Date the reference was created
