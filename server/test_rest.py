import json, unittest, jsonpickle, random

from server.app import *

# Import model
from adapter.run import Run
from adapter.job import Job
from adapter.link import Link

import adapter.utilities as utils


class TestREST(unittest.TestCase):
    def setUp(self):
        """This method is run once before each test method is executed"""

        # Get PIM config type from environment
        pim_config = os.environ.get('PIM_CONFIG', 'unit_test_ci')

        # Disable werkzeug logging
        werkzeug_logger.disabled = True

        # Create testing Flask application
        self.app = create_app_from_config(pim_config)

        self.test_client = self.app.test_client()

        # Create tables
        orm.Base.metadata.create_all(bind=self.app.db_engine)

    def tearDown(self):
        """This method is run once after each test method is executed"""

        # Remove tables
        orm.Base.metadata.drop_all(bind=self.app.db_engine)

    def http_put_run(self, run_json, expected_status_code=200):
        """Add run with HTTP put request

        :param run_json: Run in JSON format
        :param expected_status_code: The HTTP status code that is expected from the operation
        """

        # Setup HTTP request headers
        headers = [('Content-Type', 'application/json')]
        headers.append(('Content-Length', len(run_json)))

        # And add to PIM in an HTTP post request
        http_request = self.test_client.post('/api/runs/', headers=headers, data=run_json)

        # Get result as JSON
        result = json.loads(http_request.get_data(as_text=True))

        # Test HTTP status code
        self.assertEqual(http_request.status_code, expected_status_code, msg=result)

        return result

    def http_get_run(self, run_name, expected_status_code=200):
        """Get run with HTTP get request

        :param run_name: Unique run name
        :param expected_status_code: The HTTP status code that is expected from the operation
        """

        # Get the run from the PIM server in an HTTP get request
        http_request = self.test_client.get('/api/runs/{}'.format(run_name))

        # Get result as JSON
        result = json.loads(http_request.get_data(as_text=True))

        # Test HTTP status code
        self.assertEqual(http_request.status_code, expected_status_code, msg=result)

        return result

    def http_get_runs(self, expected_status_code=200):
        """Get runs with HTTP get request

        :param expected_status_code: The HTTP status code that is expected from the operation
        """

        # Get the run from the PIM server in an HTTP get request
        http_request = self.test_client.get('/api/runs/')

        # Get result as JSON
        result = json.loads(http_request.get_data(as_text=True))

        # Test HTTP status code
        self.assertEqual(http_request.status_code, expected_status_code, msg=result)

        return result

    def http_delete_run(self, run_name, expected_status_code=200):
        """Remove run with HTTP delete request

        :param run_name: Unique run name
        :param expected_status_code: The HTTP status code that is expected from the operation
        """

        # Remove the run from the PIM server in an HTTP delete request
        http_request = self.test_client.delete('/api/runs/{}'.format(run_name))

        # Get result as JSON
        result = json.loads(http_request.get_data(as_text=True))

        # Test HTTP status code
        if expected_status_code >= 0:
            self.assertEqual(http_request.status_code, expected_status_code, msg=result)

        return result

    def http_put_jobs(self, run, expected_status_code=200, batch_size=100):
        """Update job(s) with HTTP put request

        :param run: Run for which to send the jobs
        :param batch_size: Number of jobs to send in each request
        :param expected_status_code: The HTTP status code that is expected from the operation
        """

        # Create job batches
        job_batches = Run.job_batches(list(run.jobs.values()), batch_size)

        # print(list(job_batches))
        for job_batch in job_batches:
            # Convert to JSON
            jobs_json = jsonpickle.encode(job_batch, unpicklable=False)

            # Setup HTTP request headers
            headers = [('Content-Type', 'application/json')]
            headers.append(('Content-Length', len(jobs_json)))

            # Add to PIM server in an HTTP PUT request
            http_request = self.test_client.put('/api/runs/{}/jobs'.format(run.name), headers=headers, data=jobs_json)

            # Get result as JSON
            result = json.loads(http_request.get_data(as_text=True))

            # Test HTTP status code
            self.assertEqual(http_request.status_code, expected_status_code, msg=result)

        return result

    def http_put_job(self, run, job, expected_status_code=200):

        payload = jsonpickle.encode(job, unpicklable=False)

        # Setup HTTP request headers
        headers = [('Content-Type', 'application/json')]
        headers.append(('Content-Length', len(payload)))

        # Add to PIM server in an HTTP PUT request
        http_request = self.test_client.put('/api/runs/{}/job'.format(run.name), headers=headers, data=payload)

        # Get result as JSON
        result = json.loads(http_request.get_data(as_text=True))

        # Test HTTP status code
        self.assertEqual(http_request.status_code, expected_status_code, msg=http_request.get_data(as_text=True))

        return result

    def http_get_job(self, run_name, path, expected_status_code=200):

        # Add to PIM server in an HTTP PUT request
        http_request = self.test_client.get('/api/runs/{}/job?path={}'.format(run_name, path))

        # Get result as JSON
        result = json.loads(http_request.get_data(as_text=True))

        # Test HTTP status code
        self.assertEqual(http_request.status_code, expected_status_code, msg=http_request.get_data(as_text=True))

        return result

    def http_get_aggregates(self, run_name, nodes, expected_status_code=200):
        """Get aggregates with HTTP get request

        :param run_name: Unique run name
        :param nodes: Nodes to fetch (paths)
        :param expected_status_code: The HTTP status code that is expected from the operation
        """

        http_request = self.test_client.get('/api/runs/{}/aggregates?nodes={}'.format(run_name, json.dumps(nodes)))

        result = json.loads(http_request.get_data(as_text=True))

        self.assertEqual(http_request.status_code, expected_status_code, msg=result)

        return result

    def create_test_run_a(self):
        """Create basic test run"""

        # Create run
        self.run = Run()

        # Add the root node
        self.run.root = self.run.create_node(name='r')

        self.run.create_node(parent=self.run.root)
        self.run.create_node(parent=self.run.root)
        self.run.create_node(parent=self.run.root)

        self.run.root.generate_ports()
        self.run.root.add_jobs()

    def test_put_run_a(self):
        """Fetching a non-existing run should yield a not found error"""

        self.http_get_run('_test_run', 404)

    def test_put_run_b(self):
        """Add run should yield ok"""

        # Create basic test run
        self.create_test_run_a()

        # Get run in JSON format
        run_payload = self.run.json_payload()

        # Send run
        self.http_put_run(run_payload)

        # Fetching an existing run should yield ok
        self.http_get_run(self.run.name)

    def test_put_run_c(self):
        """Adding same run more than once should yield an error"""

        # Create basic test run
        self.create_test_run_a()

        # Get run in JSON format
        run_payload = self.run.json_payload()

        # Send run
        self.http_put_run(run_payload)

        # Posting same run twice should yield a bad request
        self.http_put_run(run_payload, 400)

    def test_delete_run_a(self):
        """Delete an existing run"""

        self.create_test_run_a()

        self.http_put_run(self.run.json_payload())  # Send run
        self.http_delete_run(self.run.name)  # Deleting an existing run should yield ok
        self.http_get_run(self.run.name, 404)  # Fetching the deleted run should return a 404

    def test_delete_run_b(self):
        """Delete a non-existing run"""

        # Deleting a non-existing run should yield 404
        self.http_delete_run('non_existing_run', 404)

    def test_fetch_runs(self):
        """Add run should yield ok"""

        # Fetch runs should yield ok
        self.http_get_runs()

    def test_get_aggregates_a(self):
        run = Run()

        run.root = run.create_node(name='r')

        node_a = run.create_node(name='a', parent=run.root)
        node_a_a = run.create_node(name='a_a', parent=node_a)
        node_a_b = run.create_node(name='a_b', parent=node_a)
        node_b = run.create_node(name='b', parent=run.root)
        node_c = run.create_node(name='c', parent=run.root)

        node_a_a.add_jobs(sums=[10, 0, 3, 7, 8, 2])
        node_a_b.add_jobs(sums=[4, 2, 9, 4, 5, 6])
        node_b.add_jobs(sums=[1, 12, 6, 7, 1, 3])
        node_c.add_jobs(sums=[10, 0, 4, 6, 8, 2])

        # Dispatch run
        self.http_put_run(run.json_payload())

        # Dispatch jobs
        self.http_put_jobs(run)

        # Pop the jobs from the queue in the run
        run.jobs = dict()

        # Fetch node aggregates
        aggregates = self.http_get_aggregates(run.name, [node.path() for node in run.nodes])['aggregates']

        # Check if node aggregate values are correct
        self.assertEqual(aggregates['r'],
                         utils.pad_or_truncate([25, 14, 22, 24, 22, 13], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a'],
                         utils.pad_or_truncate([14, 2, 12, 11, 13, 8], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a/a_a'],
                         utils.pad_or_truncate([10, 0, 3, 7, 8, 2], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a/a_b'],
                         utils.pad_or_truncate([4, 2, 9, 4, 5, 6], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/b'], utils.pad_or_truncate([1, 12, 6, 7, 1, 3], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/c'], utils.pad_or_truncate([10, 0, 4, 6, 8, 2], self.app.aggregation_no_items))

        node_a_a.add_jobs(sums=[1, 4, 2, 12, 5, 0])

        # Dispatch jobs
        self.http_put_jobs(run)

        # Fetch node aggregates
        aggregates = self.http_get_aggregates(run.name, [node.path() for node in run.nodes])['aggregates']

        # Check if node aggregate values are correct
        self.assertEqual(aggregates['r'],
                         utils.pad_or_truncate([26, 18, 24, 36, 27, 13], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a'],
                         utils.pad_or_truncate([15, 6, 14, 23, 18, 8], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a/a_a'],
                         utils.pad_or_truncate([11, 4, 5, 19, 13, 2], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a/a_b'],
                         utils.pad_or_truncate([4, 2, 9, 4, 5, 6], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/b'], utils.pad_or_truncate([1, 12, 6, 7, 1, 3], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/c'], utils.pad_or_truncate([10, 0, 4, 6, 8, 2], self.app.aggregation_no_items))

        # Pop the jobs from the queue in the run
        run.jobs = dict()

        # Update job status
        for job in node_a_a.jobs[:5]:
            job.set_status(4)

        # Update jobs
        self.http_put_jobs(run)

        # Fetch node aggregates
        aggregates = self.http_get_aggregates(run.name, [node.path() for node in run.nodes])['aggregates']

        # Check if node aggregate values are correct
        self.assertEqual(aggregates['r'],
                         utils.pad_or_truncate([21, 18, 24, 36, 32, 13], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a'],
                         utils.pad_or_truncate([10, 6, 14, 23, 23, 8], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a/a_a'],
                         utils.pad_or_truncate([6, 4, 5, 19, 18, 2], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a/a_b'],
                         utils.pad_or_truncate([4, 2, 9, 4, 5, 6], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/b'], utils.pad_or_truncate([1, 12, 6, 7, 1, 3], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/c'], utils.pad_or_truncate([10, 0, 4, 6, 8, 2], self.app.aggregation_no_items))

        # Pop the jobs from the queue in the run
        run.jobs = dict()

        # Update job status [10, 0, 3, 7, 8, 2]
        for job in node_a_a.jobs[21:24]:
            job.set_status(2)

        # Update job status [4, 2, 9, 4, 5, 6]
        for job in node_a_b.jobs[6:9]:
            job.set_status(5)

        # Update job status [1, 12, 6, 7, 1, 3]
        for job in node_b.jobs[1:11]:
            job.set_status(4)

        # Update job status [10, 0, 4, 6, 8, 2]
        for job in node_c.jobs[14:15]:
            job.set_status(3)

        # Update jobs
        self.http_put_jobs(run)

        # Fetch node aggregates
        aggregates = self.http_get_aggregates(run.name, [node.path() for node in run.nodes])['aggregates']

        # Check if node aggregate values are correct
        self.assertEqual(aggregates['r'], utils.pad_or_truncate([21, 8, 24, 36, 39, 16], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a'],
                         utils.pad_or_truncate([10, 6, 14, 23, 20, 11], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a/a_a'],
                         utils.pad_or_truncate([6, 4, 8, 19, 15, 2], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a/a_b'],
                         utils.pad_or_truncate([4, 2, 6, 4, 5, 9], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/b'], utils.pad_or_truncate([1, 2, 6, 7, 11, 3], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/c'], utils.pad_or_truncate([10, 0, 4, 6, 8, 2], self.app.aggregation_no_items))

    def test_get_aggregates_b(self):
        run = Run()

        run.root = run.create_node(name='r')

        node_a = run.create_node(name='a', parent=run.root)
        node_a_a = run.create_node(name='a_a', parent=node_a)
        node_a_b = run.create_node(name='a_b', parent=node_a)
        node_b = run.create_node(name='b', parent=run.root)
        node_c = run.create_node(name='c', parent=run.root)

        node_a_a.add_jobs(sums=[1, 1, 1, 1, 1, 1])
        node_a_b.add_jobs(sums=[1, 1, 1, 1, 1, 1])
        node_b.add_jobs(sums=[1, 1, 1, 1, 1, 1])
        node_c.add_jobs(sums=[1, 1, 1, 1, 1, 1])

        # Dispatch run
        self.http_put_run(run.json_payload())

        # Dispatch jobs
        self.http_put_jobs(run)

        run.jobs['r/c/job___005'] = dict(customData=dict(text='Hello world'), path='r/c/job___005')

        # Convert to JSON
        jobs_json = jsonpickle.encode([run.jobs['r/c/job___005']], unpicklable=False)

        # Setup HTTP request headers
        headers = [('Content-Type', 'application/json')]
        headers.append(('Content-Length', len(jobs_json)))

        self.test_client.put('/api/runs/{}/jobs'.format(run.name), headers=headers, data=jobs_json)

        # print(run.jobs)
        # Pop the jobs from the queue in the run
        run.jobs = dict()

        # Fetch node aggregates
        aggregates = self.http_get_aggregates(run.name, [node.path() for node in run.nodes])['aggregates']

        # Check if node aggregate values are correct
        self.assertEqual(aggregates['r'],
                         utils.pad_or_truncate([4, 4, 4, 4, 4, 4], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a'],
                         utils.pad_or_truncate([2, 2, 2, 2, 2, 2], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a/a_a'],
                         utils.pad_or_truncate([1, 1, 1, 1, 1, 1], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a/a_b'],
                         utils.pad_or_truncate([1, 1, 1, 1, 1, 1], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/b'],
                         utils.pad_or_truncate([1, 1, 1, 1, 1, 1], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/c'],
                         utils.pad_or_truncate([1, 1, 1, 1, 1, 1], self.app.aggregation_no_items))

    def test_get_aggregates_c(self):

        def instance():
            run_a = Run(name=utils.ran_name())

            run_a.root = run_a.create_node(name='r')

            node_a = run_a.create_node(name='a', parent=run_a.root)
            node_a_a = run_a.create_node(name='a_a', parent=node_a)
            node_b = run_a.create_node(name='b', parent=run_a.root)

            node_a_a.add_jobs(sums=[1, 1, 1, 1, 1, 1])
            node_b.add_jobs(sums=[1, 1, 1, 1, 1, 1])

            # Dispatch run
            self.http_put_run(run_a.json_payload())

            # Dispatch jobs
            self.http_put_jobs(run_a)

            # Pop the jobs from the queue in the run
            run_a.job_queue = list()

            # Fetch node aggregates
            aggregates = self.http_get_aggregates(run_a.name, [node.path() for node in run_a.nodes])['aggregates']

            # Check if node aggregate values are correct
            self.assertEqual(aggregates['r'],
                             utils.pad_or_truncate([2, 2, 2, 2, 2, 2], self.app.aggregation_no_items))
            self.assertEqual(aggregates['r/a'],
                             utils.pad_or_truncate([1, 1, 1, 1, 1, 1], self.app.aggregation_no_items))
            self.assertEqual(aggregates['r/a/a_a'],
                             utils.pad_or_truncate([1, 1, 1, 1, 1, 1], self.app.aggregation_no_items))
            self.assertEqual(aggregates['r/b'],
                             utils.pad_or_truncate([1, 1, 1, 1, 1, 1], self.app.aggregation_no_items))

        instance()
        instance()

    def test_get_aggregates_d(self):
        """Test if node aggregates are initialized properly"""

        run = Run()

        run.root = run.create_node(name='r')

        node_a = run.create_node(name='a', parent=run.root)
        node_a_a = run.create_node(name='a_a', parent=node_a)
        node_a_b = run.create_node(name='a_b', parent=node_a)
        node_b = run.create_node(name='b', parent=run.root)
        node_c = run.create_node(name='c', parent=run.root)

        # Dispatch run
        self.http_put_run(run.json_payload())

        # Fetch node aggregates
        aggregates = self.http_get_aggregates(run.name, [node.path() for node in run.nodes])['aggregates']

        # Check if node aggregate values are correct
        self.assertEqual(aggregates['r'], utils.pad_or_truncate([0, 0, 0, 0, 0, 0], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a'], utils.pad_or_truncate([0, 0, 0, 0, 0, 0], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a/a_a'],
                         utils.pad_or_truncate([0, 0, 0, 0, 0, 0], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/a/a_b'],
                         utils.pad_or_truncate([0, 0, 0, 0, 0, 0], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/b'], utils.pad_or_truncate([0, 0, 0, 0, 0, 0], self.app.aggregation_no_items))
        self.assertEqual(aggregates['r/c'], utils.pad_or_truncate([0, 0, 0, 0, 0, 0], self.app.aggregation_no_items))

    def test_get_aggregates_e(self):

        run = Run()

        run.root = run.create_node(name='root')

        node_a = run.create_node(name='a', parent=run.root)
        node_a_a = run.create_node(name='a_a', parent=node_a)
        node_a_b = run.create_node(name='a_b', parent=node_a)
        node_b = run.create_node(name='b', parent=run.root)
        node_c = run.create_node(name='c', parent=run.root)

        node_a_a.add_jobs(sums=[3, 0, 0, 0, 0, 0])
        node_a_b.add_jobs(sums=[3, 0, 0, 0, 0, 0])
        node_b.add_jobs(sums=[3, 0, 0, 0, 0, 0])
        node_c.add_jobs(sums=[3, 0, 0, 0, 0, 0])

        # Dispatch run
        self.http_put_run(run.json_payload())

        # Dispatch jobs
        self.http_put_jobs(run)

        # Pop the jobs from the queue in the run
        run.jobs = dict()

        # Fetch node aggregates
        aggregates = self.http_get_aggregates(run.name, [node.path() for node in run.nodes])['aggregates']

        # Check if node aggregate values are correct
        self.assertEqual(utils.pad_or_truncate([12, 0, 0, 0, 0, 0], self.app.aggregation_no_items), aggregates['root'])
        self.assertEqual(utils.pad_or_truncate([6, 0, 0, 0, 0, 0], self.app.aggregation_no_items), aggregates['root/a'])
        self.assertEqual(utils.pad_or_truncate([3, 0, 0, 0, 0, 0], self.app.aggregation_no_items),
                         aggregates['root/a/a_a'])
        self.assertEqual(utils.pad_or_truncate([3, 0, 0, 0, 0, 0], self.app.aggregation_no_items),
                         aggregates['root/a/a_b'])
        self.assertEqual(utils.pad_or_truncate([3, 0, 0, 0, 0, 0], self.app.aggregation_no_items), aggregates['root/b'])
        self.assertEqual(utils.pad_or_truncate([3, 0, 0, 0, 0, 0], self.app.aggregation_no_items), aggregates['root/c'])

        # Update job status
        run.jobs = dict()
        node_a_a.jobs[0].set_status(0)
        self.http_put_jobs(run)

        # Update job status
        run.jobs = dict()
        node_a_a.jobs[0].set_status(1)
        self.http_put_jobs(run)

        # Update job status
        run.jobs = dict()
        node_a_a.jobs[0].set_status(2)
        self.http_put_jobs(run)

        # Update job status
        run.jobs = dict()
        node_a_a.jobs[0].set_status(2)
        self.http_put_jobs(run)

        # Update job status
        run.jobs = dict()
        node_a_a.jobs[0].set_status(2)
        self.http_put_jobs(run)

        # Fetch node aggregates
        aggregates = self.http_get_aggregates(run.name, [node.path() for node in run.nodes])['aggregates']

        # Check if node aggregate values are correct
        self.assertEqual(utils.pad_or_truncate([11, 0, 1, 0, 0, 0], self.app.aggregation_no_items), aggregates['root'])
        self.assertEqual(utils.pad_or_truncate([5, 0, 1, 0, 0, 0], self.app.aggregation_no_items), aggregates['root/a'])
        self.assertEqual(utils.pad_or_truncate([3, 0, 0, 0, 0, 0], self.app.aggregation_no_items),
                         aggregates['root/a/a_b'])
        self.assertEqual(utils.pad_or_truncate([2, 0, 1, 0, 0, 0], self.app.aggregation_no_items),
                         aggregates['root/a/a_a'])
        self.assertEqual(utils.pad_or_truncate([3, 0, 0, 0, 0, 0], self.app.aggregation_no_items), aggregates['root/b'])
        self.assertEqual(utils.pad_or_truncate([3, 0, 0, 0, 0, 0], self.app.aggregation_no_items), aggregates['root/c'])

    def test_get_aggregates_f(self):

        run = Run()

        run.root = run.create_node(name='r')

        node_a = run.create_node(name='a', parent=run.root)
        node_a_a = run.create_node(name='a_a', parent=node_a)
        node_a_b = run.create_node(name='a_b', parent=node_a)
        node_b = run.create_node(name='b', parent=run.root)
        node_c = run.create_node(name='c', parent=run.root)

        node_a_a.add_jobs(sums=[4, 0, 0, 0, 0, 0])
        node_a_b.add_jobs(sums=[0, 0, 0, 0, 0, 0])
        node_b.add_jobs(sums=[0, 0, 0, 0, 0, 0])
        node_c.add_jobs(sums=[0, 0, 0, 0, 0, 0])

        self.http_put_run(run.json_payload())
        self.http_put_jobs(run)

        probs = list()

        probs.append([0, 4, 0, 0, 0, 0])

        prob = random.choice(probs)

        sums = [random.randint(1, prob[i]) if prob[i] > 0 else 0 for i in range(6)]
        pdf = []

        for id, sum in enumerate(sums):
            for i in range(sum):
                pdf.append(id)

        r = [0 for i in range(6)]

        for job in list(run.jobs.values()):
            job.status = random.choice(pdf)
            r[job.status] += 1

        # Update jobs
        self.http_put_jobs(run)

        # Fetch node aggregates
        aggregates = self.http_get_aggregates(run.name, [node.path() for node in run.nodes])['aggregates']

        # Check if node aggregate values are correct
        self.assertEqual(utils.pad_or_truncate(r, self.app.aggregation_no_items), aggregates['r'])

    def test_duplicate_job_put(self):
        run = Run()

        run.root = run.create_node(name='r')

        run.root.add_jobs(sums=[10, 0, 3, 7, 8, 2])

        self.http_put_run(run.json_payload())
        self.http_put_jobs(run)

        aggregates = self.http_get_aggregates(run.name, [node.path() for node in run.nodes])['aggregates']

        self.assertEqual(aggregates['r'], utils.pad_or_truncate([10, 0, 3, 7, 8, 2], self.app.aggregation_no_items))

        self.http_put_jobs(run)

        aggregates = self.http_get_aggregates(run.name, [node.path() for node in run.nodes])['aggregates']

        self.assertEqual(aggregates['r'], utils.pad_or_truncate([10, 0, 3, 7, 8, 2], self.app.aggregation_no_items))

    def _test_put_job_a(self):

        run = Run()

        run.root = run.create_node(name='r')

        node_a = run.create_node(name='a', parent=run.root)

        no_jobs = 4

        node_a.add_jobs(sums=[no_jobs, 0, 0, 0, 0, 0])

        # Dispatch run
        self.http_put_run(run.json_payload())

        # Dispatch jobs
        for job in node_a.jobs:
            self.http_put_job(run, job)

        job = node_a.jobs[0]

        def test_put_get(job, key, expected_value):
            self.http_put_job(run, job)
            self.assertEqual(self.http_get_job(run.name, job.path())['job'][key], expected_value)

        # Test title change
        job.title = 'Hello world'
        test_put_get(job, 'title', job.title)

        # Test description change
        job.description = 'Job description'
        test_put_get(job, 'description', job.description)

        # Test custom data change
        job.custom_data = {'list': ['a', 'b', 'c']}
        test_put_get(job, 'customData', job.custom_data)

        # Test status changes
        job.status = 4
        test_put_get(job, 'status', job.status)
        job.status = 0
        test_put_get(job, 'status', job.status)

        expected_aggregates = [no_jobs, 0, 0, 0, 0, 0]

        # Randomly change the job status and send them individually in put requests
        for id, job in enumerate(node_a.jobs):
            job.status = random.randint(0, 5)

            if job.status > 0:
                expected_aggregates[job.status] += 1
                expected_aggregates[0] -= 1

            self.http_put_job(run, job)

        # Obtain the changed aggregates
        aggregates = self.http_get_aggregates(run.name, [node_a.path()])['aggregates']['r/a']

        self.assertEqual(expected_aggregates, aggregates)


if __name__ == '__main__':
    unittest.main()
