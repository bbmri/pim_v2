const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: ['babel-polyfill', './src/index.js'],
    context: __dirname,
    devtool: 'cheap-module-source-map',
    output: {
        filename: 'bundle.js',
        sourceMapFilename: 'bundle.map',
        path: path.resolve(__dirname, './dist'),
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: ['src', 'src/components', 'node_modules']
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['babel-preset-es2015', 'babel-preset-react', 'stage-2']
                }
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            },
            {
                test: /\.less$/,
                use: [
                    {loader: "style-loader"},
                    {loader: "css-loader"},
                    {loader: "less-loader"}
                ]
            },
            {
                test: /\.(woff|woff2|ttf|eot)$/,
                loader: 'file-loader'
            },
            {
                test: /\.(png|jp(e*)g|svg|gif)$/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 8000, // Convert images < 8kb to base64 strings
                        name: 'images/[hash]-[name].[ext]'
                    }
                }]
            }

        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'PIM',
            filename: 'index.html',
            template: 'index.template.ejs',
            hash: false,
            favicon: './favicon.ico'
        })
    ],
    devServer: {
        port: 8080,
        host: 'localhost',
        contentBase: __dirname,
        historyApiFallback: true,
        noInfo: false,
        // hot: true,
        stats: 'minimal',
        inline: true,
        publicPath: 'http://localhost:8080',
        proxy: {
            '/api': {
                target: 'http://pim_flask:5000',
                secure: false
            },
            '/swaggerui': {
                target: 'http://pim_flask:5000',
                secure: false
            }
        }
    },
    node: {
        fs: 'empty'
    }
};
