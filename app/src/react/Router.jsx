import React from 'react'
import {Switch, Route} from 'react-router-dom'

import RunPage from './pages/run/Page'
import RunsPage from './pages/runs/Page'
import JobsPage from './pages/jobs/Page'
import JobPage from './pages/job/Page'

export default class Router extends React.Component {
    render() {
        return (
            <div className='page'>
                <Switch>
                    <Route exact path='/runs/:run/jobs' component={JobsPage}/>
                    <Route exact path='/runs/:run' component={RunPage}/>
                    <Route exact path='/runs' component={RunsPage}/>
                    <Route exact path='/runs/:run/job' component={JobPage}/>
                    <Route exact path='/' component={RunsPage}/>
                </Switch>
            </div>
        )
    }
}