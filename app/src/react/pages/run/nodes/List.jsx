import React from 'react'
import _ from 'lodash'

import {
    Message
} from 'semantic-ui-react'

import Node from './node/Node'
import PropTypes from "prop-types";

class List extends React.Component {
    filterNodes() {
        let result = _.filter(this.props.run.nodes, (value, key) => {
            /*if (!this.props.run.nodes[key].active) {
                return false;
            }*/
            
            const node_name = key.split('/').slice(-1)[0];
            return node_name.toUpperCase().includes(this.props.run.nodeFilter.toUpperCase());
        });
        return result;
    }
    
    render() {
        const filterNodes  = this.filterNodes();
        const filterResult = _.map(filterNodes, (node) => (<li key={node.path}>{node.title}</li>));
        
        let filteredNodes = _.map(filterNodes, (node) => (
            <Node key={node.path}
                  nodePath={node.path}
                  level={0}
                  tree={false}
                  run={this.props.run}
                  actions={this.props.actions}/>));
        
        if (filterResult.length === 0) {
            return <Message warning size='mini' style={{width: '100%', textAlign: 'center'}}>No nodes found that
                match <b>{this.props.run.nodeFilter}</b></Message>
        }
        
        return <div className='nodes-list'>{filteredNodes}</div>
    }
}

List.propTypes = {
    run: PropTypes.object.isRequired,
    actions: PropTypes.any.isRequired
};

export default List;