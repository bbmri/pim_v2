import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Icon
} from 'semantic-ui-react'

import _ from 'lodash'

import PubSub from 'pubsub-js'

import RevealNode from './RevealNode'
import Title from './Title'
import Context from './context/Context'
import PimActions from '../../../../../actions'
import MainStageActions from './../../../../../mainstage/actions'

export default class Node extends React.Component {
    constructor() {
        super();
        
        this.state = {
            hovered: false,
            focused: false
        };
        
        PubSub.subscribe(PimActions.nodeReceivedFocus, this.onNodeReceivedFocused.bind(this));
    }
    
    onClick() {
        
        const nodePath = this.props.nodePath;
        
        if (nodePath === '')
            return;
        
        const node = this.props.run.nodes[nodePath];
        
        if (node === undefined)
            return;
        
        if (node.children.length > 0) {
            if (node.path !== 'root') {
                if (node.expanded) {
                    PubSub.publish(PimActions.groupCollapse, this.props.nodePath);
                } else {
                    PubSub.publish(PimActions.groupExpand, this.props.nodePath);
                }
            }
        } else {
            if (node.active) {
                const layout = node.layout;
                
                PubSub.publish(MainStageActions.setZoomRegion, {
                    zoomBox: {
                        position: layout.position,
                        size: layout.size
                    }
                });
            } else {
                PubSub.publish(PimActions.revealNode, this.props.nodePath);
            }
        }
    }
    
    onMouseEnter() {
        this.setState({
            hovered: true,
            focused: true
        });
        
        PubSub.publish(PimActions.nodeReceivedFocus, {
            nodePath: this.props.nodePath
        });
        
        const node = this.props.run.nodes[this.props.nodePath];
        
        if (node.active) {
            PubSub.publish('STATUS_SET', <span>{node.expanded ? 'Collapse' : 'Expand'} {node.title}</span>);
        } else {
            PubSub.publish('STATUS_SET', <span>{node.title}</span>);
        }
    }
    
    onMouseLeave() {
        this.setState({
            hovered: false,
            focused: false
        });
        
        PubSub.publish(PimActions.nodeLostFocus, {
            nodePath: this.props.nodePath
        });
        
        PubSub.publish('STATUS_UNSET');
    }
    
    onNodeReceivedFocused(message, data) {
        /*if (data.nodePath === this.props.nodePath) {
         if (this.state.focused)
         return;
         
         this.setState({
         ...this.state,
         focused: data.focused
         });
         }
         
         if (data.nodePath === '') {
         this.setState({
         ...this.state,
         focused: false
         });
         }*/
    }
    
    inspectFailedJobs() {
        PubSub.publish(PimActions.nodeViewFailedJobs, this.props.nodePath);
    }
    
    viewJobs() {
        PubSub.publish(PimActions.nodeViewJobs, this.props.nodePath);
    }
    
    locateNode() {
        const nodePath = this.props.nodePath;
        const node     = this.props.run.nodes[nodePath];
        
        PubSub.publish(MainStageActions.setZoomRegion, {
            zoomBox: {
                position: node.layout.position,
                size: node.layout.size
            },
            animate: true
        });
    }
    
    showConnected(show = true) {
        logger.debug(`${show ? 'Showing' : 'Not showing'} connected links and nodes`);
        
        const node = this.props.run.nodes[this.props.nodePath];
        
        for (let nodePath of node.fromNodes.concat(node.toNodes)) {
            PubSub.publish('NODE_SET_FOCUSED', {
                nodePath: nodePath,
                focused: show
            });
        }
        
        for (let link of node.links) {
            PubSub.publish('LINK_SET_FOCUSED', {
                linkId: show ? link : '',
                focused: true
            });
        }
    }
    
    render() {
        const nodePath = this.props.nodePath;
        const node     = this.props.run.nodes[nodePath];
        const indent   = this.props.level * 12;
        
        let iconName  = '';
        let iconColor = 'black';
        
        let childItems = null;
        
        if (this.props.tree && node.children.length > 0) {
            if (node.expanded) {
                childItems = _.map(node.children, (childNodePath, index) => (
                    <Node key={childNodePath}
                          nodePath={childNodePath}
                          level={this.props.level + 1}
                          tree={this.props.tree}
                          run={this.props.run}
                          actions={this.props.actions}/>
                ));
                
                childItems = <div>{childItems}</div>;
                
                iconName = 'caret down';
            } else {
                iconName = 'caret right';
            }
            
        } else {
            iconName = '';
        }
        
        if (nodePath === 'root')
            iconName = 'home';
        
        if (this.props.run.nodeFilter !== '') {
            iconName = 'filter';
        }
        
        const failed = node.status.jobs[3] > 0;
        const style  = {marginLeft: `${indent}px`};
        const link   = this.props.tree === true && nodePath !== 'root';
        const icon_a = this.props.tree ? <Icon link={link} name={iconName}/> : null;
        const icon_b = !this.props.tree ? <RevealNode run={this.props.run} node={node}/> : null;
        const icons  = <div className='icon' style={style}>{icon_a}{icon_b}</div>;
        
        let percentage = null;
        
        if (node !== undefined && this.state.hovered) {
            const progress = `${(node.status.progress * 100).toFixed(1)}%`;
            percentage     = <span className='progress'>{progress}</span>;
        }
        
        const children = <div className='children'>{childItems}</div>;
        
        let nodeItemClasses = ['node-item'];
        
        if (this.state.focused)
            nodeItemClasses.push('focused');
        
        if (failed)
            nodeItemClasses.push('failed');
        
        return (
            <div className='node-item-group'>
                <div className={nodeItemClasses.join(' ')}
                     onMouseEnter={this.onMouseEnter.bind(this)}
                     onMouseLeave={this.onMouseLeave.bind(this)}
                     onClick={this.onClick.bind(this)}>
                    {icons}
                    <Title run={this.props.run}
                           node={node}
                           nodeFilter={this.props.run.nodeFilter}/>
                    {percentage}
                    <div style={{flex: 1}}/>
                    {this.state.hovered ? <Context run={this.props.run}
                                                   node={node}
                                                   inspectFailedJobs={this.inspectFailedJobs.bind(this)}
                                                   locateNode={this.locateNode.bind(this)}
                                                   showConnected={this.showConnected.bind(this)}
                                                   viewJobs={this.viewJobs.bind(this)}
                    /> : null}
                </div>
                {children}
            </div>
        );
    }
}


Node.propTypes = {
    nodePath: PropTypes.string.isRequired,
    run: PropTypes.any.isRequired,
    level: PropTypes.number.isRequired,
    tree: PropTypes.bool.isRequired,
    actions: PropTypes.any.isRequired
};

Node.defaultProps = {
    nodePath: '',
    level: 0,
    tree: false
};
                
            