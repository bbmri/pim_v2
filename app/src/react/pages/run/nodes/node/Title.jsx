import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Icon
} from 'semantic-ui-react'

import _ from 'lodash'

export default class Title extends Component {
    title() {
        let nodeTitle = this.props.node.title;
        
        if (this.props.nodeFilter !== '') {
            let filter = this.props.nodeFilter;
            
            if (filter !== '') {
                nodeTitle = _.map(nodeTitle.split(filter).join('#' + filter + '#').split('#', 100), (str, key) => (
                    str === filter ? (<span className='filterMatch' key={key}>{str}</span>) : str
                ));
            }
        }
        
        return nodeTitle;
    }
    
    render() {
        const noChildren = this.props.node.children.length;
        const failed     = this.props.node.status.jobs[3] > 0;
        const className  = `title ${this.props.node.active ? '' : 'inactive'} ${failed ? 'failed' : ''}`;
        const counter    = noChildren > 0 ? <sup>({noChildren})</sup> : null;
        
        return <div className={className}>{this.title()}{counter}</div>
    }
}

Title.propTypes = {
    run: PropTypes.object.isRequired,
    node: PropTypes.object.isRequired,
    nodeFilter: PropTypes.string.isRequired
};