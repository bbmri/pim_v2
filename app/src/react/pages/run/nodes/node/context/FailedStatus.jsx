import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Icon
} from 'semantic-ui-react'

import {withStatus} from '../../../../../WithStatus'

class FailedStatus extends Component {
    onClick(event) {
        event.stopPropagation();
        this.props.inspectFailedJobs();
    }
    
    render() {
        return <Icon link
                     name='exclamation circle'
                     onClick={this.onClick.bind(this)}/>
    }
}

FailedStatus.propTypes = {
    run: PropTypes.object.isRequired,
    node: PropTypes.object.isRequired,
    inspectFailedJobs: PropTypes.func.isRequired
};

export default withStatus(FailedStatus, (props) => {
    const noFailedJobs = props.node.status.jobs[3];
    
    return <span>{noFailedJobs} job{noFailedJobs > 1 ? 's have' : ' has'} failed, click to inspect</span>
});
