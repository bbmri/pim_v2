import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Icon
} from 'semantic-ui-react'

import {withStatus} from '../../../../../WithStatus'

class ViewJobs extends Component {
    onClick(event) {
        event.stopPropagation();
        this.props.viewJobs();
    }
    
    render() {
        return <Icon link
                     name='tasks'
                     onClick={this.onClick.bind(this)}/>
    }
}

ViewJobs.propTypes = {
    run: PropTypes.object.isRequired,
    node: PropTypes.object.isRequired,
    viewJobs: PropTypes.func.isRequired
};

export default withStatus(ViewJobs, (props) => {
    return <span>Inspect <b>{props.node.path.split('/').slice(-1)}</b> jobs</span>
});