import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Icon
} from 'semantic-ui-react'

import {withStatus} from '../../../../../WithStatus'

class LocateNode extends Component {
    onClick(event) {
        event.stopPropagation();
        this.props.locateNode();
    }
    
    render() {
        return <Icon link
                     name='search'
                     onClick={this.onClick.bind(this)}/>
    }
}

LocateNode.propTypes = {
    run: PropTypes.object.isRequired,
    node: PropTypes.object.isRequired,
    locateNode: PropTypes.func.isRequired
};

export default withStatus(LocateNode, (props) => {
    return <span>Locate in the graph view</span>
});
