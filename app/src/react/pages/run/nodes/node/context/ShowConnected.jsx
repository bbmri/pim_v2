import React, {Component} from 'react'
import PropTypes from 'prop-types'
import PubSub from 'pubsub-js'

import {
    Icon
} from 'semantic-ui-react'

import {withStatus} from '../../../../../WithStatus'

import PimActions from '../../../../../../actions'

class ShowConnected extends Component {
    onMouseOver(event) {
    }
    
    onMouseLeave(event) {
    }
    
    onClick(event) {
        event.stopPropagation();
        PubSub.publish(PimActions.zoomNodeLinkage, this.props.node.path);
    }
    
    render() {
        return <Icon link
                     name='plug'
                     onMouseOver={this.onMouseOver.bind(this)}
                     onMouseLeave={this.onMouseLeave.bind(this)}
                     onClick={this.onClick.bind(this)}
                     style={{/*cursor: 'default'*/}}/>
    }
}

ShowConnected.propTypes = {
    run: PropTypes.object.isRequired,
    node: PropTypes.object.isRequired
};

export default withStatus(ShowConnected, (props) => {
    return <span>View all node connections</span>
});
