import React, {Component} from 'react'
import PropTypes from 'prop-types'

import LocateNode from './LocateNode'
import ShowConnected from './ShowConnected'
import FailedStatus from './FailedStatus'
import ViewJobs from './ViewJobs'

export default class Context extends Component {
    isGroup() {
        return this.props.node.children.length > 0;
    }
    
    isActive() {
        return this.props.node.active;
    }
    
    hasFailedJobs() {
        return this.props.node.status.jobs[3] > 0;
    }
    
    failedStatus() {
        return this.hasFailedJobs() ? <FailedStatus run={this.props.run}
                                                    node={this.props.node}
                                                    inspectFailedJobs={this.props.inspectFailedJobs.bind(this)}/> : null;
    }
    
    locateNode() {
        if (!this.isActive())
            return null;
        
        return <LocateNode run={this.props.run}
                           node={this.props.node}
                           locateNode={this.props.locateNode.bind(this)}/>;
    }
    
    showConnected() {
        if (this.props.node.expanded)
            return null;
        
        if (!this.isActive())
            return null;
        
        return <ShowConnected run={this.props.run}
                              node={this.props.node}/>;
    }
    
    viewJobs() {
        return <ViewJobs run={this.props.run}
                         node={this.props.node}
                         viewJobs={this.props.viewJobs.bind(this)}/>;
    }
    
    render() {
        return <div className='context'>
            {this.failedStatus()}
            {this.locateNode()}
            {this.showConnected()}
            {this.viewJobs()}
        </div>
    }
}

Context.propTypes = {
    node: PropTypes.object.isRequired,
    inspectFailedJobs: PropTypes.func.isRequired,
    locateNode: PropTypes.func.isRequired,
    showConnected: PropTypes.func.isRequired,
    viewJobs: PropTypes.func.isRequired,
};