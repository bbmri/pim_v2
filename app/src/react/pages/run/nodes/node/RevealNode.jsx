import React, {Component} from 'react'
import PropTypes from 'prop-types'
import PubSub from 'pubsub-js'

import {
    Icon
} from 'semantic-ui-react'

import {withStatus} from '../../../../WithStatus'

import PimActions from '../../../../../actions'

class RevealNode extends Component {
    onClick(event) {
        event.stopPropagation();
        
        PubSub.publish(PimActions.revealNode, this.props.node.path);
    }
    
    render() {
        const color = this.props.node.active ? 'black' : 'grey';
        const style = {
            cursor: this.props.node.active ? 'default' : 'pointer'
        };
        
        return <Icon link
                     name='eye'
                     color={color}
                     style={style}
                     onClick={this.onClick.bind(this)}/>
    }
}

RevealNode.propTypes = {
    run: PropTypes.object.isRequired,
    node: PropTypes.object.isRequired
};

export default withStatus(RevealNode, (props) => {
    if (props.node.active) {
        return <span>{props.node.title}</span>
    }
    
    return <span>Reveal {props.node.title}</span>
});
