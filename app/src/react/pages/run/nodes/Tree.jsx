import React from 'react';

import Node from './node/Node'

export default class Tree extends React.Component {
    constructor() {
        super();
    }

    render() {
        if (Object.keys(this.props.run.nodes).length === 0) {
            return <div/>;
        }

        return <div className='nodes-tree'><Node nodePath='root' level={0} tree={true} run={this.props.run} actions={this.props.actions}/></div>;
    }
}