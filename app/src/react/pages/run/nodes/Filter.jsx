import React, {Component} from 'react'

import {
    Input
} from 'semantic-ui-react'

import {withStatus} from '../../../WithStatus'
import PubSub from "pubsub-js";

class Filter extends Component {
    constructor() {
        super();
    }
    
    onInputChanged(event, data) {
        this.props.actions.setNodeFilter(data.value);
    }
    
    shouldComponentUpdate(nextProps, nextState) {
        return this.props.run.nodeFilter !== nextProps.run.nodeFilter;
    }
    
    render() {
        return <Input placeholder='Search nodes...' onChange={this.onInputChanged.bind(this)} icon='search'
                   value={this.props.run.nodeFilter} fluid/>
    }
}

export default withStatus(Filter, (props) => {
    return <span>Search nodes by name</span>
});