import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
import PropTypes from 'prop-types'
import PubSub from 'pubsub-js'

import {
    Segment,
    Header,
    Button,
    Loader,
    Dimmer
} from 'semantic-ui-react'

import Tree from './Tree'
import Filter from '../nodes/Filter'
import List from '../nodes/List'

import {withStatus} from '../../../WithStatus'

import PimActions from '../../../../actions'

class CollapseAll extends Component {
    render() {
        return <Button basic size='tiny' style={{flex: 1}}
                       onClick={() => PubSub.publish(PimActions.collapseAllNodes)}>Collapse all</Button>
    }
}

let CollapseAllWithStatus = withStatus(CollapseAll, (props) => {
    return <span>Click to collapse all nodes in the graph</span>
});

class ExpandAll extends Component {
    render() {
        return <Button basic size='tiny' style={{flex: 1}}
                       onClick={() => PubSub.publish(PimActions.expandAllNodes)}>Expand all</Button>
    }
}

let ExpandAllWithStatus = withStatus(ExpandAll, (props) => {
    return <span>Click to expand all nodes in the graph</span>
});

class Panel extends React.Component {
    render() {
        const hasNodeFilter = this.props.run.nodeFilter.length > 0;
        
        return (
            <div style={{width: 400, display: 'flex', flexDirection: 'column'}}>
                
                <Segment secondary attached='top' textAlign='left'>
                    <Header as='h3'>
                        Tree view
                        <Header.Subheader>Hierarchical pipeline view</Header.Subheader>
                    </Header>
                </Segment>
                <Segment secondary attached>
                    <Filter run={this.props.run} actions={this.props.actions}/>
                </Segment>
                
                <Segment attached={hasNodeFilter ? 'bottom' : true}
                         style={{flexGrow: 1, overflowX: 'auto', overflowY: 'auto'}}>
                    {/*<Loader size='medium' active={this.props.run.busy}/>*/}
                    {/*<Dimmer.Dimmable dimmed={this.props.run.get('busy')}>*/}
                    {/*<Dimmer active={this.props.run.get('busy')} inverted/>*/}
                    <div style={{flexGrow: 1, display: 'flex'}}>
                        {hasNodeFilter ? <List run={this.props.run}
                                               style={{flex: 1}}
                                               actions={this.props.actions}/> :
                            <Tree run={this.props.run}
                                  actions={this.props.actions}
                                  style={{flex: 1}}/>}
                    </div>
                    {/*</Dimmer.Dimmable>*/}
                </Segment>
                
                {hasNodeFilter ? null :
                    <Segment attached='bottom' secondary size='mini'>
                        {/*<Header as='h5' textAlign={'center'}>
                         <Header.Subheader>Tree visibility level</Header.Subheader>
                         </Header>*/}
                        <div style={{display: 'flex'}}>
                            <CollapseAllWithStatus/>
                            <div style={{width: 7}}/>
                            <ExpandAllWithStatus/>
                        </div>
                        {/*<Divider/>
                         <div style={{display: 'flex'}}>
                         <Button basic size='tiny' style={{flex: 1}} onClick={() => this.props.actions.setViewDepth(1)}>Level 1</Button>
                         <Button basic size='tiny' style={{flex: 1}} onClick={() => this.props.actions.setViewDepth(2)}>Level 2</Button>
                         <Button basic size='tiny' style={{flex: 1}} onClick={() => this.props.actions.setViewDepth(3)}>Level 3</Button>
                         </div>*/}
                    </Segment>}
            </div>
        )
    }
}

export default withRouter(Panel);