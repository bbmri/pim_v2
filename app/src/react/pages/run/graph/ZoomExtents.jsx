import React, {Component} from 'react'

import {
    Button,
    Icon
} from 'semantic-ui-react'
import PropTypes from "prop-types";

export default class ZoomExtents extends Component {
    render() {
        return <Button basic
                       size='tiny'
                       // compact
                       onClick={this.props.zoomExtents.bind(this)}>Zoom extents</Button>
    }
}

ZoomExtents.propTypes = {
    zoomExtents: PropTypes.func.isRequired
};