import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
import PropTypes from 'prop-types'

import {
    Segment,
    Header,
    Button,
    Loader,
    Checkbox
} from 'semantic-ui-react'

import Graph from './Graph'
import ExportSvg from './ExportSvg'
import ZoomExtents from './ZoomExtents'

import {withStatus} from '../../../WithStatus'
import PubSub from "pubsub-js";

import MainStageActions from './../../../../mainstage/actions'

let ExportSvgWithStatus = withStatus(ExportSvg, (props) => {
    return <span>Click to export SVG</span>
});

let ZoomExtentsWithStatus = withStatus(ZoomExtents, (props) => {
    return <span>Click to zoom to extents</span>
});

class EnableAnimation extends Component {
    render() {
        return <Checkbox label='Animation'></Checkbox>
    }
}

EnableAnimation.propTypes = {};

let EnableAnimationWithStatus = withStatus(EnableAnimation, (props) => {
    return <span>Enable/disable animation</span>
});

class Panel extends React.Component {
    zoomExtents() {
        const node = this.props.run.nodes.root;
        
        PubSub.publish(MainStageActions.setZoomRegion, {
            zoomBox: {
                position: node.layout.position,
                size: node.layout.size
            },
            animate: true
        });
    }
    
    render() {
        return (
            <div style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
                <Segment attached='top' secondary>
                    <Header as='h3'>
                        Graph view
                        <Header.Subheader>Directed graph view of the processing pipeline</Header.Subheader>
                    </Header>
                </Segment>
                <Segment attached style={{flex: 1, padding: '0px', display: 'flex'}}>
                    <Loader size='medium' active={this.props.run.busy}/>
                    {/*<Dimmer.Dimmable style={{width: '100%'}} dimmed={this.props.run.get('busy')}>*/}
                    {/*<Dimmer active={this.props.run.get('busy')} inverted/>*/}
                    <Graph run={this.props.run} actions={this.props.actions} style={{flex: 1}}/>
                    {/*</Dimmer.Dimmable>*/}
                </Segment>
                <Segment attached='bottom' secondary size='mini'>
                    {/*<ExportSvgWithStatus/>*/}
                    <div style={{display: 'flex'}}>
                    <ZoomExtentsWithStatus zoomExtents={this.zoomExtents.bind(this)}/>
                    </div>
                    {/*<EnableAnimationWithStatus/>*/}
                </Segment>
            </div>
        )
    }
}

export default withRouter(Panel);