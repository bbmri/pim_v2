import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Link from './Link'

export default class Links extends React.Component {
    constructor() {
        super();
    }
    
    render() {
        const links = _.map(this.props.links, (link) => (<Link key={link.id} link={link}/>));
        
        return <div>{links}</div>;
    }
}

Links.propTypes = {
    run: PropTypes.object.isRequired,
    links: PropTypes.object.isRequired
};