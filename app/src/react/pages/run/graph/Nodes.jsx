import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Node from './Node'

export default class Nodes extends React.Component {
    constructor() {
        super();
    }
    
    render() {
        const nodes = _.map(this.props.nodes, (node) => (node.active ? <Node key={`${this.props.run.name}/${node.path}`} node={node}/> : null));
        
        return <div>{nodes}</div>;
    }
}

Nodes.propTypes = {
    run: PropTypes.object.isRequired,
    nodes: PropTypes.object.isRequired
};