import React, {Component} from 'react'

import {
    Button,
    Icon
} from 'semantic-ui-react'

export default  class ExportSvg extends Component {
    render() {
        return <Button basic
                       icon
                       size='tiny'
                       compact>
            <Icon name='camera'/></Button>
    }
}

ExportSvg.propTypes = {
};