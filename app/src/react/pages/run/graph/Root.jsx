import React, {Component} from 'react'
import _ from 'lodash'

import Renderer from '../../../../mainstage/renderer'

export default class Root extends React.Component {
    shouldComponentUpdate(nextProps, nextState) {
        return false;
    }
    
    render() {
        logger.debug('REACT_RENDER');
        
        let layerGroups = _.map(Array.from({length: Renderer.noLayers()}, (x, i) => i), (layerId) => (
            <g className='layer' id={`layer_${layerId}`} key={`layer_${layerId}`}/>));
        
        return <g id='root'>{layerGroups}</g>;
    }
}