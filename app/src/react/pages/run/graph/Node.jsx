import React from 'react'
import PropTypes from 'prop-types'

import NodeLeafActor from '../../../../visualization/actors/node/leaf'
import NodeGroupActor from '../../../../visualization/actors/node/group'
import NodeTransitionActor from '../../../../visualization/actors/node/transition'

export default class Node extends React.Component {
    constructor() {
        super();
        
        this.actors = {};
    }
    
    componentDidMount() {
        const actorId               = this.props.node.path.split('/').join('_');
        const leafActorId           = `${actorId}_leaf`;
        const groupActorId          = `${actorId}_group`;
        const nodeTransitionActorId = `${actorId}_transition`;
        
        this.actors.leaf       = new NodeLeafActor({id: leafActorId});
        this.actors.group      = new NodeGroupActor({id: groupActorId});
        this.actors.transition = new NodeTransitionActor({id: nodeTransitionActorId});
        
        this.updateNodeData(this.props.node);
    }
    
    componentWillUnmount() {
        for (let actorId in this.actors) {
            this.actors[actorId].terminate();
            delete this.actors[actorId];
        }
    }
    
    shouldComponentUpdate(nextProps, nextState) {
        if (JSON.stringify(nextProps.node) !== JSON.stringify(this.props.node))
            this.updateNodeData(nextProps.node);
        
        return false;
    }
    
    render() {
        return null;
    }
    
    updateNodeData(node) {
        for (let actorId in this.actors) {
            this.actors[actorId].setData(node);
        }
    }
}

Node.propTypes = {
    node: PropTypes.object.isRequired
};