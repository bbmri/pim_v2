import React, {Component} from 'react'
import PropTypes from 'prop-types'
import PubSub from 'pubsub-js'

import Nodes from './Nodes'
import Links from './Links'
import Root from './Root'

import Renderer from '../../../../mainstage/renderer'
import MainStageActions from '../../../../mainstage/actions'

export default class Graph extends React.Component {
    constructor() {
        super();
    }
    
    componentWillMount() {
        // logger.debug('REACT_COMPONENT_WILL_MOUNT');
    }
    
    componentDidMount() {
        // logger.debug('REACT_COMPONENT_DID_MOUNT');
        
        Renderer.initialize();
        Renderer.restoreView(false);
        
        PubSub.publish(MainStageActions.updateAllActors);
    }
    
    componentWillUnmount() {
    }
    
    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.run.modified === -1 && nextProps.run.modified === 0) {
            const node = this.props.run.nodes.root;
            
            PubSub.publish(MainStageActions.setZoomRegion, {
                zoomBox: {
                    position: node.layout.position,
                    size: node.layout.size
                }
            });
        }
        
        return nextProps.run.modified > this.props.run.modified;
    }
    
    componentDidUpdate(prevProps, prevState) {
        PubSub.publish(MainStageActions.updateAllActors);
        
        /*logger.debug({
            noElements: d3.select('#root').selectAll('*').size()
        });*/
    }
    
    render() {
        return <svg id='svg' width='100%' height='100%'>
            <Root/>
            <Nodes run={this.props.run} nodes={this.props.run.nodes}/>
            <Links run={this.props.run} links={this.props.run.links}/>
        </svg>;
    }
}

Graph.propTypes = {
    run: PropTypes.object.isRequired
};