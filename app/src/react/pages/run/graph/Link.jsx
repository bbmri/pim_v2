import React from 'react'
import PropTypes from 'prop-types'

import LinkActor from "../../../../visualization/actors/link";

export default class Link extends React.Component {
    constructor() {
        super();
        
        this.actor = null;
    }
    
    componentDidMount() {
        this.actor = new LinkActor({id: this.props.link.id});
        
        this.updateLinkData(this.props.link);
    }
    
    componentWillUnmount() {
        this.actor.terminate();
        
        delete this.actor;
    }
    
    shouldComponentUpdate(nextProps, nextState) {
        if (JSON.stringify(nextProps.link) !== JSON.stringify(this.props.link))
            this.updateLinkData(nextProps.link);

        return false;
    }
    
    render() {
        return null;
    }
    
    updateLinkData(node) {
        this.actor.setData(node);
    }
}

Link.propTypes = {
    link: PropTypes.object.isRequired
};