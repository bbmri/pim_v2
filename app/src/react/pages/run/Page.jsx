import React from 'react';

import Container from './Container'
import Footer from '../../footer/Footer'

// https://jsfiddle.net/MadLittleMods/LmYagfy/

export default class Page extends React.Component {
    render() {
        return (
            <div className='page content run'>
                <Container/>
                <Footer/>
            </div>
        )
    }
}