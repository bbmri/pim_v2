import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {withRouter} from 'react-router-dom'
import {withCookies, Cookies} from 'react-cookie'

import PubSub from 'pubsub-js'

import * as actions from '../../../actions/run'

import Navigation from './../../Navigation'
import Breadcrumb from './Breadcrumb'
import TreePanel from './nodes/Panel'
import GraphPanel from './graph/Panel'
import Actions from '../../../mainstage/actions'
import MeasureTime from "../../../utilities/measureTime";
import PimActions from '../../../actions'

class Container extends Component {
    constructor() {
        super();
        
        this.synchronizeGraphTimer = null;
        this.listeners             = {};
    }
    
    componentCleanup() {
        logger.debug(`Component cleanup`);
        
        // Remove event listener
        window.removeEventListener('beforeunload', this.componentCleanup.bind(this));
        
        // Remove the timer that periodically synchronizes the graph
        if (this.synchronizeGraphTimer !== null) {
            clearInterval(this.synchronizeGraphTimer);
        }
        
        // Remove PubSub listeners
        for (let listener of Object.values(this.listeners)) {
            PubSub.unsubscribe(listener);
        }
    }
    
    componentWillMount() {
        logger.debug(`Component will mount`);
    }
    
    componentWillUnmount() {
        logger.debug(`Component will unmount`);
        
        this.componentCleanup();
    }
    
    componentDidMount() {
        logger.debug(`Component did mount`);
        
        this.props.actions.load(this.props.match.params.run);
        
        // Cleanup the component before the tab is unloaded
        window.addEventListener('beforeunload', this.componentCleanup.bind(this));
        
        if (this.synchronizeGraphTimer === null) {
            this.synchronizeGraphTimer = setInterval(this.synchronizeGraph.bind(this), 1000);
        }
        
        // Add custom actions
        this.props.actions.goToRunsPage = this.goToRunsPage.bind(this);
        // this.props.actions.viewRunJobs = this.viewRunJobs.bind(this);
        
        this.listeners = {
            linkExpand: PubSub.subscribe(PimActions.linkExpand, this.onLinkExpand.bind(this)),
            groupExpand: PubSub.subscribe(PimActions.groupExpand, this.onGroupExpand.bind(this)),
            groupCollapse: PubSub.subscribe(PimActions.groupCollapse, this.onGroupCollapse.bind(this)),
            collapseAll: PubSub.subscribe(PimActions.collapseAllNodes, this.onCollapseAll.bind(this)),
            expandAll: PubSub.subscribe(PimActions.expandAllNodes, this.onExpandAll.bind(this)),
            revealNode: PubSub.subscribe(PimActions.revealNode, this.onRevealNode.bind(this)),
            zoomNodeLinkage: PubSub.subscribe(PimActions.zoomNodeLinkage, this.onZoomNodeLinkage.bind(this)),
            nodeViewFailedJobs: PubSub.subscribe(PimActions.nodeViewFailedJobs, this.onNodeViewFailedJobs.bind(this)),
            nodeViewAlljobs: PubSub.subscribe(PimActions.nodeViewJobs, this.onNodeViewAllJobs.bind(this))
        }
        
    }
    
    async onLinkExpand(message, data) {
        logger.debug(`Expand link`);
        
        PubSub.publish(PimActions.linkLostFocus, {nodePath: ''});
        PubSub.publish(PimActions.nodeLostFocus, {linkId: ''});
        
        let timer = new MeasureTime();
        
        const run      = this.props.run;
        const link     = run.links[data];
        const fromNode = run.nodes[link.fromNode];
        const toNode   = run.nodes[link.toNode];
        
        await this.props.actions.expandLink(data);
        
        timer.lap('expand link');
        
        const fromNodeAabb = {
            min: {
                x: fromNode.layout.position.x,
                y: fromNode.layout.position.y
            },
            max: {
                x: fromNode.layout.position.x + fromNode.layout.size.width,
                y: fromNode.layout.position.y + fromNode.layout.size.height
            }
        };
        
        const toNodeAabb = {
            min: {
                x: toNode.layout.position.x,
                y: toNode.layout.position.y
            },
            max: {
                x: toNode.layout.position.x + toNode.layout.size.width,
                y: toNode.layout.position.y + toNode.layout.size.height
            }
        };
        
        const aabb = {
            min: {
                x: Math.min(fromNodeAabb.min.x, toNodeAabb.min.x),
                y: Math.min(fromNodeAabb.min.y, toNodeAabb.min.y)
            },
            max: {
                x: Math.max(fromNodeAabb.max.x, toNodeAabb.max.x),
                y: Math.max(fromNodeAabb.max.y, toNodeAabb.max.y)
            }
        };
        
        if (run.autoZoomAndPan) {
            PubSub.publish(PimActions.nodeLostFocus, {nodePath: ''});
            
            PubSub.publish(Actions.setZoomRegion, {
                zoomBox: {
                    position: {
                        x: Math.min(fromNodeAabb.min.x, toNodeAabb.min.x),
                        y: Math.min(fromNodeAabb.min.y, toNodeAabb.min.y)
                    },
                    size: {
                        width: aabb.max.x - aabb.min.x,
                        height: aabb.max.y - aabb.min.y
                    }
                }
            });
        }
    }
    
    async onGroupExpand(message, data) {
        logger.debug(`Expand group`);
    
        PubSub.publish(PimActions.nodeLostFocus, {linkId: ''});
        PubSub.publish(PimActions.linkLostFocus, {nodePath: ''});
        
        let timer = new MeasureTime();
        
        await this.props.actions.expandNode(data);
        
        timer.lap('expand group');
        
        this.zoomToNode(data);
    }
    
    async onGroupCollapse(message, data) {
        logger.debug(`Collapse group`);
        
        PubSub.publish(PimActions.nodeLostFocus, {linkId: ''});
        PubSub.publish(PimActions.linkLostFocus, {nodePath: ''});
        
        let timer = new MeasureTime();
        
        await this.props.actions.collapseNode(data);
        
        timer.lap('collapse group');
        
        const segments     = data.split('/');
        const parentNodeId = segments.slice(0, Math.max(1, segments.length - 1)).join('/');
        
        this.zoomToNode(parentNodeId);
    }
    
    async onExpandAll(message, data) {
        logger.debug('Expand all nodes');
        
        PubSub.publish(PimActions.nodeLostFocus, {linkId: ''});
        PubSub.publish(PimActions.linkLostFocus, {nodePath: ''});
        
        let timer = new MeasureTime();
        
        await this.props.actions.expandAll();
        
        timer.lap('expand all nodes');
        
        this.zoomToNode('root');
    }
    
    async onCollapseAll(message, data) {
        logger.debug('Collapse all nodes');
        
        PubSub.publish(PimActions.nodeLostFocus, {linkId: ''});
        PubSub.publish(PimActions.linkLostFocus, {nodePath: ''});
        
        let timer = new MeasureTime();
        
        await this.props.actions.collapseAll();
        
        timer.lap('collapse all nodes');
        
        this.zoomToNode('root');
    }
    
    async onRevealNode(message, data) {
        logger.debug('Reveal node');
        
        PubSub.publish(PimActions.nodeLostFocus, {linkId: ''});
        PubSub.publish(PimActions.linkLostFocus, {nodePath: ''});
        
        let timer = new MeasureTime();
        
        await this.props.actions.revealNode(data);
        
        timer.lap('reveal node');
        
        this.zoomToNode(data);
    }
    
    async onZoomNodeLinkage(message, data) {
        logger.debug('Zoom to node linkage');
        
        PubSub.publish(PimActions.nodeLostFocus, {linkId: ''});
        PubSub.publish(PimActions.linkLostFocus, {nodePath: ''});
        
        const node = this.props.run.nodes[data];
        
        let aabb = {
            min: {
                x: Number.POSITIVE_INFINITY,
                y: Number.POSITIVE_INFINITY
            },
            max: {
                x: Number.NEGATIVE_INFINITY,
                y: Number.NEGATIVE_INFINITY
            }
        };
        
        for (let nodePath of [data].concat(node.fromNodes).concat(node.toNodes)) {
            const node = this.props.run.nodes[nodePath];
            
            let pMin = {
                x: node.layout.position.x,
                y: node.layout.position.y,
            };
            
            let pMax = {
                x: node.layout.position.x + node.layout.size.width,
                y: node.layout.position.y + node.layout.size.height,
            };
            
            if (pMin.x < aabb.min.x)
                aabb.min.x = pMin.x;
            
            if (pMin.y < aabb.min.y)
                aabb.min.y = pMin.y;
            
            if (pMax.x > aabb.max.x)
                aabb.max.x = pMax.x;
            
            if (pMax.y > aabb.max.y)
                aabb.max.y = pMax.y;
        }
        
        PubSub.publish(Actions.setZoomRegion, {
            zoomBox: {
                position: aabb.min,
                size: {
                    width: aabb.max.x - aabb.min.x,
                    height: aabb.max.y - aabb.min.y
                }
            }
        });
    }
    
    
    synchronizeGraph() {
        // logger.debug(`Synchronize graph`);
        
        this.props.actions.synchronizeStatus();
    }
    
    zoomToNode(nodePath) {
        logger.debug(`Zoom to ${nodePath}`);
        
        const run  = this.props.run;
        const node = run.nodes[nodePath];
        
        if (run.autoZoomAndPan) {
            PubSub.publish(PimActions.nodeLostFocus, {nodePath: ''});
            PubSub.publish(Actions.setZoomRegion, {
                zoomBox: {
                    position: node.layout.position,
                    size: node.layout.size
                },
                animate: run.animateTransitions
            });
        }
    }
    
    pageTitle() {
        return `${this.props.run.name} graph view`;
    }
    
    goToRunsPage(message, data) {
        logger.debug(`Go to runs page`);
        
        this.props.history.push({
            pathname: `/runs`,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.pageTitle()
            }
        });
    }
    
    onNodeViewFailedJobs(message, data) {
        logger.debug(`Go to jobs page`);
        
        this.props.history.push({
            pathname: `/runs/${this.props.run.name}/jobs`,
            search: `?node=${data}&filter=['failed']`,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.pageTitle()
            }
        });
    }
    
    onNodeViewAllJobs(message, data) {
        logger.debug(`Go to jobs page`);
        
        this.props.history.push({
            pathname: `/runs/${this.props.run.name}/jobs`,
            search: `?node=${data}`,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.pageTitle()
            }
        });
    }
    
    render() {
        // logger.debug('Render');
        
        let actions = {
            goToRunsPage: this.goToRunsPage.bind(this),
            goToJobsPage: this.onNodeViewAllJobs.bind(this),
        };
        
        return (
            <div style={{flex: 1, display: 'flex', flexDirection: 'column'}}>
                <Navigation breadcrumbs={<Breadcrumb run={this.props.run} actions={actions}/>}
                            location={this.props.location}/>
                <div style={{flex: 1, paddingTop: '10px', paddingBottom: '0px', display: 'flex'}}>
                    <TreePanel run={this.props.run} actions={this.props.actions}/>
                    <div style={{width: 10}}/>
                    <GraphPanel run={JSON.parse(JSON.stringify(this.props.run))} actions={this.props.actions}/>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        run: state.get('run')
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

export default withCookies(withRouter(connect(mapStateToProps, mapDispatchToProps)(Container)));