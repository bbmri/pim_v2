import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'

import {
    Breadcrumb as SemanticUiBreadcrumb
} from 'semantic-ui-react'

import BreadcrumbLink from  '../../BreadcrumbLink'
import {withStatus} from "../../WithStatus";

class RunsLink extends Component {
    render() {
        return <BreadcrumbLink
                    name='runs'
                    popup='Go to runs overview'
                    action={this.props.actions.goToRunsPage}>
                </BreadcrumbLink>
    }
}

let RunsLinkWithStatus = withStatus(RunsLink, (props) => {
    return <span>Go to the runs overview page</span>
});

class Breadcrumb extends React.Component {
    render() {
        const breadcrumb = (
            <SemanticUiBreadcrumb>
                <SemanticUiBreadcrumb.Section>pim</SemanticUiBreadcrumb.Section>
                <SemanticUiBreadcrumb.Divider icon='right angle'/>
                <RunsLinkWithStatus actions={this.props.actions}/>
                <SemanticUiBreadcrumb.Divider icon='right angle'/>
                <SemanticUiBreadcrumb.Section>{this.props.run.name}</SemanticUiBreadcrumb.Section>
                <SemanticUiBreadcrumb.Divider icon='right angle'/>
                <SemanticUiBreadcrumb.Section active>graph</SemanticUiBreadcrumb.Section>
            </SemanticUiBreadcrumb>
        );
        
        return breadcrumb
    }
}

export default withRouter(Breadcrumb);