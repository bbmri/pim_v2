import React, {Component} from 'react'
import {connect} from "react-redux"
import {bindActionCreators} from "redux"
import {withRouter} from 'react-router-dom'
import {withCookies, Cookies} from 'react-cookie';

const queryString = require('query-string')

import {
    Table,
    Segment,
    Message
} from 'semantic-ui-react'

import * as actions from "../../../actions/jobs";

import PageHeader from './PageHeader'
import Navigation from './../../Navigation'
import Breadcrumb from './Breadcrumb'
import Filter from './Filter'
import TableHeader from './table/Header'
import TableBody from './table/Body'
import TableFooter from './table/Footer'
import Pagination from '../../pagination/Pagination'

import * as constants from './../../../utilities/constants'

class Container extends Component {
    constructor() {
        super();
        
        this.synchronizeJobsTimer = null;
    }
    
    componentCleanup() {
        logger.debug('Component cleanup');
        
        // Remove event listener
        window.removeEventListener('beforeunload', this.componentCleanup.bind(this));
        
        // Remove the timer that periodically synchronizes the aggregates
        if (this.synchronizeJobsTimer !== null) {
            clearInterval(this.synchronizeJobsTimer);
        }
        
        this.props.cookies.set('jobsSortColumn', this.props.jobs.get('sortColumn'));
        this.props.cookies.set('jobsSortDirection', this.props.jobs.get('sortDirection'));
        this.props.cookies.set('jobsPageSize', this.props.jobs.get('pageSize'));
        this.props.cookies.set('jobsPageIndex', this.props.jobs.get('pageIndex'));
    }
    
    componentWillUnmount() {
        logger.debug('Component will unmount');
        
        this.componentCleanup();
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        logger.debug('Component did update');
        
        const prevParams  = queryString.parse(prevProps.location.search);
        const curParams   = queryString.parse(this.props.location.search);
        const filterTypes = ['idle', 'running', 'success', 'failed', 'cancelled', 'undefined'];
        
        let updateJobs = false;
        
        // Respond when run name changes
        if (this.props.match.params.run !== this.props.jobs.get('runName')) {
            logger.debug(`Run name has changed to ${this.props.match.params.run}`);
            
            this.props.actions.setRunName(this.props.match.params.run);
            
            updateJobs = true;
        }
        
        // Respond when node path changes
        if (curParams.node !== this.props.jobs.get('nodePath')) {
            logger.debug(`Node path has changed to ${curParams.node}`);
            
            this.props.actions.setNodePath(curParams.node);
            
            updateJobs = true;
        }
        
        if (curParams.filter !== undefined) {
            const filterMap   = this.props.jobs.get('filter').toJS();
            let filterUpdated = false;
            
            for (let filterType of filterTypes) {
                if (curParams.filter.includes(filterType) && filterMap[filterType] === false) {
                    this.props.actions.setFilter(filterType, true);
                    filterUpdated = true;
                }
                
                if (!curParams.filter.includes(filterType) && filterMap[filterType] === true) {
                    this.props.actions.setFilter(filterType, false);
                    filterUpdated = true;
                }
            }
            
            if (filterUpdated) {
                logger.debug(`Filter has changed to ${curParams.filter}`);
                
                this.props.actions.setPageIndex(0);
                
                updateJobs = true;
            }
        }
        
        // logger.info(curParams.filter, prevParams.filter)
        // Respond when filter changes
        if (curParams.filter !== prevParams.filter && curParams.filter === undefined) {
            this.props.history.replace({
                path: '/jobs',
                search: `?node=${curParams.node}&filter=${prevParams.filter}`
            });
        }
        
        if (updateJobs === true) {
            this.updateJobs();
        }
    }
    
    componentWillMount() {
        logger.debug('Component will mount');
        
        // Restore sorting and pagination from cookies
        this.props.actions.setSortColumn(this.props.cookies.get('jobsSortColumn') || 'name');
        this.props.actions.setSortDirection(this.props.cookies.get('jobsSortDirection') || 'ascending');
        this.props.actions.setPageSize(Math.max(1, this.props.cookies.get('jobsPageSize')) || 1);
        this.props.actions.setPageIndex(this.props.cookies.get('jobsPageIndex') || 0);
    }
    
    componentDidMount() {
        logger.debug('Component did mount');
        
        const parsed      = queryString.parse(this.props.location.search);
        const filterTypes = ['idle', 'running', 'success', 'failed', 'cancelled', 'undefined'];
        
        if (parsed.node === undefined || parsed.filter === undefined) {
            const node   = parsed.node === undefined ? 'node=root' : `node=${parsed.node}`;
            const filter = parsed.filter === undefined ? `filter=${JSON.stringify(filterTypes)}` : `filter=${parsed.filter}`;
            
            this.props.history.push({
                path: '/jobs',
                search: `?${node}&${filter}`
            });
        }
        
        window.addEventListener('beforeunload', this.componentCleanup.bind(this));
        
        if (this.synchronizeJobsTimer === null) {
            this.synchronizeJobsTimer = setInterval(this.updateJobs.bind(this), 1000);
        }
    }
    
    updateJobs() {
        logger.debug('Update jobs');
        
        this.props.actions.fetchJobs();
    }
    
    setNodePath(nodePath) {
        logger.debug(`Set node path to ${nodePath}`);
        
        this.props.history.replace({
            search: `?node=${nodePath}`,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.pageTitle()
            }
        });
    }
    
    setFilter(type, state) {
        logger.debug(`Set filter ${type} to ${state}`);
        
        this.props.actions.setFilter(type, state);
        this.props.actions.setPageIndex(0);
        this.props.actions.fetchJobs();
    }
    
    toggleFilter(type) {
        logger.debug(`Toggle filter ${type}`);
        
        const filterMap = this.props.jobs.get('filter').toJS();
        
        filterMap[type] = !filterMap[type];
        
        let filter = [];
        
        for (let filterType in filterMap) {
            if (filterMap[filterType] === true)
                filter.push(filterType);
        }
        
        let filterString = '[]';
        
        if (filter.length > 0)
            filterString = JSON.stringify(filter);
        
        this.props.history.replace({
            path: '/jobs',
            search: `?node=${this.props.jobs.get('nodePath')}&filter=${filterString}`,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.pageTitle()
            }
        });
    }
    
    setSoloFilter(type) {
        logger.debug(`Set solo filter ${type}`);
        
        this.props.history.replace({
            search: `?node=${this.props.jobs.get('nodePath')}&filter=${JSON.stringify([type])}`,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.pageTitle()
            }
        });
    }
    
    filterAll() {
        logger.debug(`Filter all`);
        
        this.props.history.replace({
            path: '/jobs',
            search: `?node=${this.props.jobs.get('nodePath')}&filter=${JSON.stringify(constants.JOB_STATUS_TYPES)}`,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.pageTitle()
            }
        });
    }
    
    setSortColumn(column) {
        logger.debug(`Set sort column ${column}`);
        
        let direction = 'ascending';
        
        if (this.props.jobs.get('sortColumn') == column) {
            direction = this.props.jobs.get('sortDirection') == 'ascending' ? 'descending' : 'ascending';
            this.props.actions.setSortDirection(direction);
        } else {
            this.props.actions.setSortDirection(direction);
        }
        
        this.props.actions.setSortColumn(column);
        this.props.actions.setPageIndex(0);
        this.props.actions.fetchJobs();
    }
    
    setPageSize(pageSize) {
        logger.debug(`Set page size to ${pageSize}`);
        
        this.props.actions.setPageSize(pageSize);
        this.props.actions.setPageIndex(0);
        this.props.actions.fetchJobs();
    }
    
    setPageIndex(pageIndex) {
        logger.debug(`Set page index to ${pageIndex}`);
        
        this.props.actions.setPageIndex(pageIndex);
        this.props.actions.fetchJobs();
    }
    
    firstPage() {
        logger.debug(`Go to first page`);
        
        this.props.actions.firstPage();
        this.props.actions.fetchJobs();
    }
    
    previousPage() {
        logger.debug(`Go to last page`);
        
        this.props.actions.previousPage();
        this.props.actions.fetchJobs();
    }
    
    nextPage() {
        logger.debug(`Go to next page`);
        
        this.props.actions.nextPage();
        this.props.actions.fetchJobs();
    }
    
    lastPage() {
        logger.debug(`Go to last page`);
        
        this.props.actions.lastPage();
        this.props.actions.fetchJobs();
    }
    
    pageTitle() {
        return `${this.props.jobs.get('runName')} jobs view`;
    }
    
    goToRunsPage() {
        logger.debug(`Go to runs page`);
        
        this.props.history.push({
            pathname: `/runs`,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.pageTitle()
            }
        });
    }
    
    goToGraphPage() {
        logger.debug(`Go to graph page`);
        
        this.props.history.push({
            pathname: `/runs/${this.props.jobs.get('runName')}`,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.pageTitle()
            }
        });
    }
    
    goToJobPage(jobPath) {
        logger.debug(`Go to job page`);
        
        this.props.history.push({
            pathname: `/runs/${this.props.jobs.get('runName')}/job`,
            search: `path=${jobPath}`,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.pageTitle()
            }
        });
    }
    
    render() {
        logger.debug('Render');
        
        let actions = {
            setSortColumn: this.setSortColumn.bind(this),
            setNodePath: this.setNodePath.bind(this),
            setFilter: this.setFilter.bind(this),
            toggleFilter: this.toggleFilter.bind(this),
            setSoloFilter: this.setSoloFilter.bind(this),
            filterAll: this.filterAll.bind(this),
            setPageSize: this.setPageSize.bind(this),
            setPageIndex: this.setPageIndex.bind(this),
            firstPage: this.firstPage.bind(this),
            previousPage: this.previousPage.bind(this),
            nextPage: this.nextPage.bind(this),
            lastPage: this.lastPage.bind(this),
            goToRunsPage: this.goToRunsPage.bind(this),
            goToGraphPage: this.goToGraphPage.bind(this),
            goToJobPage: this.goToJobPage.bind(this)
        };
        
        return (
            <div style={{flexGrow: 1, display: 'flex', flexDirection: 'column'}}>
                <PageHeader store={this.props.jobs} actions={actions}/>
                <Navigation breadcrumbs={<Breadcrumb store={this.props.jobs} actions={actions}/>}
                            location={this.props.location}/>
                <Segment attached='bottom'>
                    <Filter store={this.props.jobs} actions={actions}/>
                    <Table striped sortable selectable celled compact singleLine fixed size='small'>
                        <TableHeader store={this.props.jobs} actions={actions}/>
                        <TableBody store={this.props.jobs} actions={actions}/>
                        <TableFooter store={this.props.jobs} actions={actions}/>
                    </Table>
                    <Pagination store={this.props.jobs} actions={actions}/>
                </Segment>
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        jobs: state.get('jobs')
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

export default withCookies(withRouter(connect(mapStateToProps, mapDispatchToProps)(Container)));