import React from 'react';

import {
    Icon,
    Header
} from 'semantic-ui-react'

export default class PageHeader extends React.Component {
    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.store.get('runName') !== this.props.store.get('runName')) {
            return true;
        }
        
        return false;
    }

    render() {
        return (
            <Header icon textAlign='center' size='small'>
                <Icon name='tasks' color='black'/>Jobs
                <Header.Subheader>Inspect <b>{this.props.store.get('runName')}</b> jobs</Header.Subheader>
            </Header>
        )
    }
}