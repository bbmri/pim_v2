import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {withStatus} from '../../WithStatus'

import {
    Button,
    Label,
    Icon
} from 'semantic-ui-react'


class ToggleFilter extends Component {
    onClick(event) {
        if (event.ctrlKey) {
            this.props.setSoloFilter(this.props.jobType);
        } else {
            this.props.toggleFilter(this.props.jobType);
        }
    }
    
    render() {
        return <Button key={this.props.jobType}
                       size='mini'
                       compact
                       // disabled={this.props.jobCount === 0}
                       primary={/*this.props.jobCount > 0 &&*/ this.props.enabled}
                       style={{width: this.props.width, marginRight: 5, textTransform: 'capitalize'}}
                       onClick={this.onClick.bind(this)}>{this.props.jobType}: {this.props.jobCount}</Button>;
    }
}

ToggleFilter.propTypes = {
    jobType: PropTypes.string.isRequired,
    jobCount: PropTypes.number.isRequired,
    enabled: PropTypes.bool.isRequired,
    toggleFilter: PropTypes.func.isRequired,
    setSoloFilter: PropTypes.func.isRequired
};

const ToggleFilterWithStatus = withStatus(ToggleFilter, (props) => {
    return <span><b>Click</b> to {props.enabled ? 'hide' : 'show'} {props.jobType} jobs, <b>Ctrl + click</b> to only show {props.jobType} jobs</span>
});

class FilterAll extends Component {
    onClick() {
        this.props.filterAll(this.props.jobType);
    }
    
    render() {
        return <Button compact
                       size='mini'
                       onClick={this.onClick.bind(this)}>Show all</Button>
    }
}

FilterAll.propTypes = {
    filterAll: PropTypes.func.isRequired
};

const FilterAllWithStatus = withStatus(FilterAll, (props) => {
    return <span><b>Click</b> to show all job types</span>
});

export default class Filter extends Component {
    render() {
        const statusTypes = ['idle', 'running', 'success', 'failed', 'cancelled', 'undefined'];
        const filter      = this.props.store.get('filter').toJS();
        const aggregate   = this.props.store.get('aggregate').toJS();
        let toggleFilters = [];
        
        for (let type of statusTypes) {
            toggleFilters.push(<ToggleFilterWithStatus
                key={type}
                jobType={type}
                jobCount={aggregate[type]}
                enabled={filter[type]}
                setSoloFilter={this.props.actions.setSoloFilter.bind(this)}
                toggleFilter={this.props.actions.toggleFilter.bind(this)}/>);
        }
        
        return <div><Label pointing='right'>Filter by job type</Label>{toggleFilters}<FilterAllWithStatus
            filterAll={this.props.actions.filterAll.bind(this)}/></div>
    }
}

Filter.propTypes = {
    store: PropTypes.any.isRequired,
    actions: PropTypes.any.isRequired
};