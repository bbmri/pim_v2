import React from 'react'

import Container from './Container'
import Footer from '../../footer/Footer'

export default class Page extends React.Component {
    render() {
        return (
            <div className='page content jobs'>
                <Container/>
                <Footer/>
            </div>
        )
    }
}