import React, {Component} from 'react'

import {
    Table
} from 'semantic-ui-react'

export default class Footer extends Component {
    constructor() {
        super();
    }
    
    render() {
        const firstRow = this.props.store.get('pageIndex') * this.props.store.get('pageSize');
        const rowInfo  = `${firstRow} - ${firstRow + this.props.store.get('pageRows').size} of ${this.props.store.get('noRows')} jobs`;
        
        return (
            <Table.Footer>
                <Table.Row>
                    <Table.HeaderCell colSpan={6}>
                        <div style={{display: 'flex'}}>
                            <div style={{flex: 1}}></div>
                            <div style={{color: 'grey'}}>{rowInfo}</div>
                        </div>
                    </Table.HeaderCell>
                </Table.Row>
            </Table.Footer>
        )
    }
}
