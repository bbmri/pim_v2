import React, {Component} from 'react'

import {
    Table
} from 'semantic-ui-react'

import _ from 'lodash'

import Row from './row/Row'

export default class Body extends Component {
    constructor() {
        super();
    }
    
    shouldComponentUpdate(nextProps, nextState) {
        const attributes = ['pageRows', 'nodePath'];

        for (let attribute of attributes) {
            if (this.props.store.get(attribute) !== nextProps.store.get(attribute)) {
                return true;
            }
        }

        return false;
    }
    
    render() {
        let pageRows = this.props.store.get('pageRows').toJS();
        
        if (pageRows.length === 0) {
            const noJobsAvailableText = 'No jobs to display for node ';
            
            return <Table.Body>
                <Table.Row warning key="no_jobs">
                    <Table.Cell key='no_jobs' colSpan="6" rowSpan={this.props.store.get('pageSize')}
                                textAlign="center">{noJobsAvailableText}
                        <b>{this.props.store.get('nodePath')}</b></Table.Cell>
                </Table.Row>
            </Table.Body>
        }
        
        // Map the filtered runs
        let tableRows = _.map(pageRows, (job) => (
            <Row key={job.path} jobs={this.props.store} job={job} actions={this.props.actions}/>));
        
        return <Table.Body>{tableRows}</Table.Body>
    }
}