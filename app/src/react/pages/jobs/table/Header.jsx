import React, {Component} from 'react'

import {
    Table
} from 'semantic-ui-react'

import HeaderCell from '../../../HeaderCell'

export default class Header extends Component {
    render() {
        return (
            <Table.Header>
                <Table.Row>
                    <HeaderCell sortColumn='title' title='Name' store={this.props.store} actions={this.props.actions} style={{width: 240}}/>
                    <HeaderCell sortColumn='path' title='Node' store={this.props.store} actions={this.props.actions} style={{flex: 1}}/>
                    <HeaderCell sortColumn='status' title='Status' store={this.props.store} actions={this.props.actions} style={{width: 90}}/>
                    <HeaderCell sortColumn='duration' title='Duration' store={this.props.store} actions={this.props.actions} style={{width: 120}}/>
                    <HeaderCell sortColumn='created' title='Creation date' store={this.props.store} actions={this.props.actions} style={{width: 140}}/>
                    <HeaderCell sortColumn='modified' title='Modification date' store={this.props.store} actions={this.props.actions} style={{width: 140}}/>
                </Table.Row>
            </Table.Header>
        )
    }
}
