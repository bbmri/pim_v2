import React, {Component} from 'react'

import {
    Table
} from 'semantic-ui-react'

import NodeJobsLink from '../../../../NodeJobsLink'
import Title from './cell/Title'
import Node from './cell/Node'
import Status from './cell/Status'
import Duration from './cell/Duration'
import Created from './cell/Created'
import Modified from './cell/Modified'

export default class Row extends Component {
    constructor() {
        super();
    }
    
    render() {
        return (
            <Table.Row key={this.props.job.path}>
                <Title jobs={this.props.jobs} job={this.props.job} actions={this.props.actions}/>
                <Node jobs={this.props.jobs} job={this.props.job} actions={this.props.actions}/>
                <Status jobs={this.props.jobs} job={this.props.job} actions={this.props.actions}/>
                <Duration jobs={this.props.jobs} job={this.props.job} actions={this.props.actions}/>
                <Created jobs={this.props.jobs} job={this.props.job} actions={this.props.actions}/>
                <Modified jobs={this.props.jobs} job={this.props.job} actions={this.props.actions}/>
            </Table.Row>
        )
    }
}