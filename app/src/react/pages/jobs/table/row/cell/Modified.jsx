import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Table
} from 'semantic-ui-react'

export default class Modified extends Component {
    render() {
        return <Table.Cell key='modified' textAlign='right' style={{cursor: 'default'}}>
            <span>{this.props.job.modified.slice(0, 10)}</span>
            &nbsp;
            <span style={{fontWeight: 'bold'}}>{this.props.job.modified.slice(11, 19)}</span>
        </Table.Cell>
    }
}

Modified.propTypes = {
    job: PropTypes.object.isRequired
};