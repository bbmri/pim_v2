import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Table
} from 'semantic-ui-react'

import NodeJobsLink from '../../../../../NodeJobsLink'

export default class Node extends Component {
    constructor() {
        super();
        
        this.state = {
            hover: false
        }
    }
    
    onMouseOver() {
        this.setState({
            hover: true
        })
    }
    
    onMouseLeave() {
        this.setState({
            hover: false
        })
    }
    
    render() {
        const segments = this.props.job.path.split('/').slice(1, -1);
        
        let path = [];
        
        for (let i = 0; i < segments.length; i++) {
            const nodePath = this.props.job.path.split('/').slice(1, i + 2).join('/');
            
            path.push(<NodeJobsLink key={nodePath}
                                    runName={this.props.jobs.get('runName')}
                                    title={segments[i]}
                                    nodePath={nodePath}
                                    pageTitle={'jobs'}>{segments[i]}</NodeJobsLink>);
            
            if (i < segments.length - 1)
                path.push('/')
        }
        
        // <Icon link color={'black'} name={'search'} style={{visibility: `${this.state.hover ? 'visible' : 'hidden'}`}}/>
        
        return <Table.Cell key='nodePath'
                           onMouseOver={this.onMouseOver.bind(this)}
                           onMouseLeave={this.onMouseLeave.bind(this)}>{path}</Table.Cell>
    }
}

Node.propTypes = {
    job: PropTypes.object.isRequired
};