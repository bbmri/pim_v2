import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Table
} from 'semantic-ui-react'

export default class Duration extends Component {
    render() {
        return <Table.Cell key='duration' textAlign='right'
                           style={{cursor: 'default'}}>{this.props.job.duration}</Table.Cell>
    }
}

Duration.propTypes = {
    job: PropTypes.object.isRequired
};