import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Table
} from 'semantic-ui-react'

export default class Created extends Component {
    render() {
        return <Table.Cell key='created' textAlign='right' style={{cursor: 'default'}}>
            <span>{this.props.job.created.slice(0, 10)}</span>
            &nbsp;
            <span style={{fontWeight: 'bold'}}>{this.props.job.created.slice(11, 19)}</span>
        </Table.Cell>
    }
}

Created.propTypes = {
    job: PropTypes.object.isRequired
};