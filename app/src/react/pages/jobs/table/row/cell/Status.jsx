import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Table
} from 'semantic-ui-react'

export default class Status extends Component {
    render() {
        const statusTypes = ['idle', ' running', ' success', ' failed', ' cancelled', ' undefined'];
        
        return <Table.Cell key='status' textAlign='center'
                           style={{cursor: 'default', backgroundColor: statusTypes[this.props.job.status]}}>
            <div className={'cell job-status ' + statusTypes[this.props.job.status]}>{statusTypes[this.props.job.status]}</div>
        </Table.Cell>
    }
}

Status.propTypes = {
    job: PropTypes.object.isRequired
};