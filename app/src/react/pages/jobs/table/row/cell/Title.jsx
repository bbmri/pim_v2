import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {
    Table,
} from 'semantic-ui-react'

import {withStatus} from '../../../../../WithStatus'

class JobDetails extends Component {
    render() {
        const nodeName = this.props.job.path.split('/').slice(-1).join('/');
        
        return <a style={{cursor: 'pointer'}}
                  onClick={() => this.props.actions.goToJobPage(this.props.job.path)}>{nodeName}</a>
    }
}

JobDetails.propTypes = {
    job: PropTypes.object.isRequired
};

let JobDetailsWithStatus = withStatus(JobDetails, (props) => {
    return <span>Click to view <b>{props.job.title}</b> details</span>
});

export default class Title extends Component {
    render() {
        return <Table.Cell key='title'><JobDetailsWithStatus {...this.props}/></Table.Cell>
    }
}