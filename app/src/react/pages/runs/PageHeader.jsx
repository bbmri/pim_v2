import React from 'react';

import {
    Icon,
    Header
} from 'semantic-ui-react'

export default class PageHeader extends React.Component {
    shouldComponentUpdate(nextProps, nextState) {
        return false;
    }

    render() {
        return (
            <Header as='h1' icon textAlign='center' size='small'>
                <Icon name='settings' color='black'/>Pipeline runs
                <Header.Subheader>Manage and view pipeline runs</Header.Subheader>
            </Header>
        )
    }
}