import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {withRouter} from 'react-router-dom'
import {withCookies, Cookies} from 'react-cookie'

import {
    Segment
} from 'semantic-ui-react'

import * as actions from '../../../actions/runs'
import * as constants from './../../../utilities/constants'
import * as  settings from '../../../mainstage/settings'

import PageHeader from './PageHeader'
import Navigation from './../../Navigation'
import Breadcrumb from './Breadcrumb'
import Search from './Search'
import Table from './table/Table'
import Pagination from '../../pagination/Pagination'

class Container extends Component {
    constructor() {
        super();

        this.filterTimer = null;
        this.synchronizeRunsTimer = null;
    }

    componentCleanup() {
        logger.debug(`Cleanup`);
        
        // Remove event listener
        window.removeEventListener('beforeunload', this.componentCleanup.bind(this));

        // Remove the timer that periodically synchronizes the runs
        if (this.synchronizeRunsTimer !== null) {
            clearInterval(this.synchronizeRunsTimer);
        }

        this.props.cookies.set('runsNameFilter', this.props.runs.get('nameFilter'));
        this.props.cookies.set('runsSortColumn', this.props.runs.get('sortColumn'));
        this.props.cookies.set('runsSortDirection', this.props.runs.get('sortDirection'));
        this.props.cookies.set('runsPageSize', this.props.runs.get('pageSize'));
        this.props.cookies.set('runsPageIndex', this.props.runs.get('pageIndex'));
    }

    componentWillMount() {
        logger.debug(`Component will mount`);
        
        this.props.actions.setNameFilter(this.props.cookies.get('runsNameFilter') || '');
        this.props.actions.setSortColumn(this.props.cookies.get('runsSortColumn') || 'name');
        this.props.actions.setSortDirection(this.props.cookies.get('runsSortDirection') || 'ascending');
        this.props.actions.setPageSize(this.props.cookies.get('runsPageSize') || 10);
        this.props.actions.setPageIndex(this.props.cookies.get('runsPageIndex') || 0);

        this.props.actions.fetchRuns();
    }

    componentWillUnmount() {
        logger.debug(`Component will unmount`);
        
        this.componentCleanup();
    }

    componentDidMount() {
        logger.debug(`Component did mount`);
        
        this.props.actions.fetchRuns();

        // Cleanup the component before the tab is unloaded
        window.addEventListener('beforeunload', this.componentCleanup.bind(this));

        // Periodically updateSize the runs
        if (this.synchronizeRunsTimer === null) {
            this.synchronizeRunsTimer = setInterval(this.synchronizeRuns.bind(this), 1000);
        }
    }

    synchronizeRuns() {
        logger.debug(`Synchronize runs`);
        
        this.props.actions.fetchRuns();
    }

    setNameFilter(name) {
        this.props.actions.setNameFilter(name);
        this.props.actions.setPageIndex(0);

        if (this.filterTimer !== null) {
            clearInterval(this.filterTimer);
        }

        this.filterTimer = setInterval(this.filterTimeOut.bind(this), 200);
    }

    filterTimeOut() {
        clearInterval(this.filterTimer);

        this.props.actions.fetchRuns();
    }

    setSortColumn(sortColumn) {
        logger.debug(`Set sort column to ${sortColumn}`);
        
        let sortDirection = 'ascending';

        if (this.props.runs.get('sortColumn') == sortColumn) {
            sortDirection = this.props.runs.get('sortDirection') == 'ascending' ? 'descending' : 'ascending';
            this.props.actions.setSortDirection(sortDirection);
        } else {
            this.props.actions.setSortDirection(sortDirection);
        }

        this.props.actions.setSortColumn(sortColumn);
        this.props.actions.setPageIndex(0);
        this.props.actions.fetchRuns();
    }

    setPageSize(pageSize) {
        logger.debug(`Set page size to ${pageSize}`);
        
        this.props.actions.setPageSize(pageSize);
        this.props.actions.setPageIndex(0);
        this.props.actions.fetchRuns();
    }

    setPageIndex(pageIndex) {
        logger.debug(`Set page index to ${pageIndex}`);
        
        this.props.actions.setPageIndex(pageIndex);
        this.props.actions.fetchRuns();
    }

    firstPage() {
        logger.debug(`Go to first page`);
        
        this.props.actions.firstPage();
        this.props.actions.fetchRuns();
    }

    previousPage() {
        logger.debug(`Go to previous page`);
        
        this.props.actions.previousPage();
        this.props.actions.fetchRuns();
    }

    nextPage() {
        logger.debug(`Go to next page`);
        
        this.props.actions.nextPage();
        this.props.actions.fetchRuns();
    }

    lastPage() {
        logger.debug(`Go to last page`);
        
        this.props.actions.lastPage();
        this.props.actions.fetchRuns();
    }
    
    /*lastPage() {
        logger.debug(`Go to last page`);
        
        this.props.actions.lastPage();
        this.props.actions.fetchRuns();
    }*/
    
    pageTitle() {
        return `runs overview`;
    }
    
    goToGraphPage(run) {
        logger.debug(`Go to graph page`);
        
        this.props.history.push({
            pathname: `/runs/${run}`,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.pageTitle()
            }
        });
    }

    goToJobsPage(run, filter = constants.JOB_STATUS_TYPES) {
        logger.debug(`Go to jobs page`);
        
        const filterParam = filter ? `filter=${JSON.stringify(filter)}` : ``;
        
        this.props.history.push({
            pathname: `/runs/${run}/jobs`,
            search: filterParam ? `?${filterParam}` : ``,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.pageTitle()
            }
        });
    }
    
    removeRun(run) {
        logger.debug(`Remove run`);
        
        this.props.actions.removeRun(run)
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !nextProps.runs.get('updating');
    }

    render() {
        logger.debug('Render');
        
        let actions = {
            setSortColumn: this.setSortColumn.bind(this),
            setNameFilter: this.setNameFilter.bind(this),
            setPageSize: this.setPageSize.bind(this),
            setPageIndex: this.setPageIndex.bind(this),
            firstPage: this.firstPage.bind(this),
            previousPage: this.previousPage.bind(this),
            nextPage: this.nextPage.bind(this),
            lastPage: this.lastPage.bind(this),
            goToGraphPage: this.goToGraphPage.bind(this),
            goToJobsPage: this.goToJobsPage.bind(this),
            removeRun: this.removeRun.bind(this)
        };
        
        return (
            <div style={{flexGrow: 1, display: 'flex', flexDirection: 'column'}}>
                <PageHeader runs={this.props.runs}/>
                <Navigation breadcrumbs={<Breadcrumb store={this.props.job} actions={actions}/>} location={this.props.location}/>
                <Segment attached='bottom'>
                    <Search runs={this.props.runs} actions={actions}/>
                    <Table runs={this.props.runs} actions={actions}/>
                    <Pagination store={this.props.runs} actions={actions}/>
                </Segment>
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        runs: state.get('runs')
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

export default withCookies(withRouter(connect(mapStateToProps, mapDispatchToProps)(Container)));