import React from 'react'
import {withRouter} from 'react-router-dom'

import {
    Breadcrumb as SemanticUiBreadcrumb
} from 'semantic-ui-react'

class Breadcrumb extends React.Component {
    render() {
        return (
            <SemanticUiBreadcrumb>
                <SemanticUiBreadcrumb.Section>pim</SemanticUiBreadcrumb.Section>
                <SemanticUiBreadcrumb.Divider icon='right angle'/>
                <SemanticUiBreadcrumb.Section active>runs</SemanticUiBreadcrumb.Section>
            </SemanticUiBreadcrumb>
        );
    }
}

export default withRouter(Breadcrumb);