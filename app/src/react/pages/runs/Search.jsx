import React, {Component} from 'react'

import {
    Input
} from 'semantic-ui-react'

import {withStatus} from '../../WithStatus'

class Search extends Component {
    constructor() {
        super();
    }

    onInputChanged(event, data) {
        this.props.actions.setNameFilter(data.value);
    }
    
    render() {
        return <Input placeholder='Search pipeline run...'
                   onChange={this.onInputChanged.bind(this)}
                   icon='search'
                   style={{width: '500px'}}
                   value={this.props.runs.get('nameFilter')}/>;
    }
}

export default withStatus(Search, (props) => {
    return <span>Search pipeline runs by name</span>
});