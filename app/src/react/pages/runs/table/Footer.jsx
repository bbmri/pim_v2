import React, {Component} from 'react'

import {
    Table
} from 'semantic-ui-react'

export default class Footer extends Component {
    constructor() {
        super();
    }

    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }

    render() {
        const firstRow = this.props.runs.get('pageIndex') * this.props.runs.get('pageSize');
        const rowInfo  = `${firstRow} - ${firstRow + this.props.runs.get('pageRows').size} of ${this.props.runs.get('noRows')} runs`;
        
        return (
            <Table.Footer>
                <Table.Row>
                    <Table.HeaderCell colSpan={7}>
                        <div style={{display: 'flex'}}>
                            <div style={{flex: 1}}>{}</div>
                            <div style={{color: 'grey'}}>{rowInfo}</div>
                        </div>
                    </Table.HeaderCell>
                </Table.Row>
            </Table.Footer>
        )
    }
}
