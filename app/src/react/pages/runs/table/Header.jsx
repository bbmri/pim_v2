import React, {Component} from 'react'

import {
    Table
} from 'semantic-ui-react'

import HeaderCell from '../../../HeaderCell'

export default class Header extends Component {
    constructor() {
        super();
    }
    
    shouldComponentUpdate(nextProps, nextState) {
        const attributes = ['sortColumn', 'sortDirection'];

        for (let attribute of attributes) {
            if (this.props.runs.get(attribute) !== nextProps.runs.get(attribute)) {
                return true;
            }
        }

        return false;
    }
    
    render() {
        return (
            <Table.Header>
                <Table.Row>
                    <HeaderCell sortColumn='name' title='Name' store={this.props.runs} actions={this.props.actions} style={{width: 200}}/>
                    <HeaderCell sortColumn='user' title='User' store={this.props.runs} actions={this.props.actions} style={{width: 50}}/>
                    <HeaderCell sortColumn='no_jobs' title='No. jobs' store={this.props.runs} actions={this.props.actions} style={{width: 50}}/>
                    <HeaderCell sortColumn='progress' title='Progress' store={this.props.runs} actions={this.props.actions} style={{width: 50}}/>
                    <HeaderCell sortColumn='' title='Jobs' store={this.props.runs} actions={this.props.actions} style={{width: 200}}/>
                    <HeaderCell sortColumn='created' title='Created' store={this.props.runs} actions={this.props.actions} style={{width: 65}}/>
                    <HeaderCell sortColumn='modified' title='Modified' store={this.props.runs} actions={this.props.actions} style={{width: 65}}/>
                </Table.Row>
            </Table.Header>
        )
    }
}
