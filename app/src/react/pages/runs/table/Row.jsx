import React, {Component} from 'react'

import {
    Table,
    Icon
} from 'semantic-ui-react'

import {withStatus} from '../../../WithStatus'

import Jobs from '../Jobs'

class ViewRunGraph extends Component {
    render() {
        return <Icon name='eye'
                     link
                     color='black'
                     onClick={() => this.props.actions.goToGraphPage(this.props.runName)}/>
    }
}

let ViewGraphWithStatus = withStatus(ViewRunGraph, (props) => {
    return <span>Click to view graph representation of the <b>{props.runName}</b> pipeline</span>
});

class ViewRunJobs extends Component {
    render() {
        return <Icon name='tasks'
                     link
                     color='black'
                     onClick={() => this.props.actions.goToJobsPage(this.props.runName)}/>
    }
}

let ViewRunJobsStatus = withStatus(ViewRunJobs, (props) => {
    return <span>Click to inspect <b>{props.runName}</b> jobs</span>
});

class RemoveRun extends Component {
    render() {
        return <Icon name='trash'
                     link
                     color='black'
                     onClick={() => this.props.actions.removeRun(this.props.runName)}/>
    }
}

let RemoveRunStatus = withStatus(RemoveRun, (props) => {
    return <span>Click to remove <b>{props.runName}</b></span>
});

class CellName extends Component {
    constructor() {
        super();
        
        this.state = {
            hover: false
        }
    }
    
    onMouseOver() {
        this.setState({
            hover: true
        })
    }
    
    onMouseLeave() {
        this.setState({
            hover: false
        })
    }
    
    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.runs.get('nameFilter') !== this.props.runs.get('nameFilter')) {
            return true;
        }
        
        if (nextState.hover !== this.state.hover) {
            return true;
        }
        
        return false;
    }
    
    render() {
        let filter = this.props.runs.get('nameFilter');
        let name   = this.props.run.name;
        
        if (filter !== '') {
            name = _.map(name.split(filter).join('#' + filter + '#').split('#', 100), (str, key) => (
                str === filter ? (<span className='filterMatch' key={key}>{str}</span>) : str
            ));
        }
        
        return <Table.Cell key='name'
                           style={{cursor: 'default'}}
                           onMouseOver={this.onMouseOver.bind(this)}
                           onMouseLeave={this.onMouseLeave.bind(this)}>
            <div style={{display: 'flex'}}>
                <div style={{flex: 1, overflow: 'hidden', textOverflow: 'ellipsis'}}>{name}</div>
                <div style={{marginLeft: 10, visibility: `${this.state.hover ? 'visible' : 'hidden'}`}}>
                    <span style={{fontStyle: 'normal', color: 'hsl(0, 0%, 50%)'}}>actions: </span>
                    <ViewGraphWithStatus runName={this.props.run.name} actions={this.props.actions}/>
                    <ViewRunJobsStatus runName={this.props.run.name} actions={this.props.actions}/>
                    <RemoveRunStatus runName={this.props.run.name} actions={this.props.actions}/>
                </div>
            </div>
        </Table.Cell>
    }
}

let CellNameWithStatus = withStatus(CellName, (props) => {
    return <span><b>{props.run.name}</b></span>
});

class CellCreated extends Component {
    render() {
        return <Table.Cell key='created' textAlign='right' style={{cursor: 'default'}}>
            <span>{this.props.run.created.slice(0, 10)}</span>
            &nbsp;
            <span style={{fontWeight: 'bold'}}>{this.props.run.created.slice(11, 19)}</span>
        </Table.Cell>
    }
}

class CellModified extends Component {
    render() {
        return <Table.Cell key='created' textAlign='right' style={{cursor: 'default'}}>
            <span>{this.props.run.modified.slice(0, 10)}</span>
            &nbsp;
            <span style={{fontWeight: 'bold'}}>{this.props.run.modified.slice(11, 19)}</span>
        </Table.Cell>
    }
}

export default class Row extends Component {
    render() {
        const noJobsTotal = this.props.run.status.reduce((a, b) => a + b, 0);

        return (
            <Table.Row key={this.props.run.name}>
                <CellNameWithStatus runs={this.props.runs} run={this.props.run} actions={this.props.actions}/>
                <Table.Cell key="user" style={{cursor: 'default'}}>{this.props.run.user}</Table.Cell>
                <Table.Cell key="noJobs" textAlign="right" style={{cursor: 'default'}}>{noJobsTotal}</Table.Cell>
                <Table.Cell key="progress"
                            textAlign="right"
                            style={{cursor: 'default'}}>{(100.0 * this.props.run.progress).toFixed(1)}%</Table.Cell>
                <Table.Cell key="jobs"><Jobs run={this.props.run} jobs={this.props.run.status}
                                             actions={this.props.actions}/></Table.Cell>
                <CellCreated key='created' run={this.props.run}/>
                <CellModified key='modified' run={this.props.run}/>
            </Table.Row>
        )
    }
}