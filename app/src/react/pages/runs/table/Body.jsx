import React, {Component} from 'react'

import {
    Table
} from 'semantic-ui-react'

import _ from 'lodash'

import Row from './Row'

export default class Body extends Component {
    constructor() {
        super();
    }

    shouldComponentUpdate(nextProps, nextState) {
        const attributes = ['pageRows', 'nameFilter'];

        for (let attribute of attributes) {
            if (this.props.runs.get(attribute) !== nextProps.runs.get(attribute)) {
                return true;
            }
        }

        return false;
    }

    render() {
        let pageRows = this.props.runs.get('pageRows').toJS();

        // Map the filtered runs
        let tableRows = _.map(pageRows, (run) => (
            <Row runs={this.props.runs} actions={this.props.actions} key={run.name} run={run}/>));

        if (pageRows.length === 0) {
            const noFilterMatchText = <span>No runs found for which the name matches with <b>{this.props.runs.get('nameFilter')}</b></span>;
            const noRunsAvailableText = `No runs to display`;
            const rowText = this.props.runs.get('nameFilter').length > 0 ? noFilterMatchText : noRunsAvailableText;

            return (
                <Table.Body>
                    <Table.Row warning key="no_runs">
                        <Table.Cell colSpan="7" textAlign="center">{rowText}</Table.Cell>
                    </Table.Row>
                </Table.Body>
            )
        }

        return <Table.Body>{tableRows}</Table.Body>
    }
}