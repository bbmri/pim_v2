import React, {Component} from 'react'

import {
    Table as SemanticUiTable
} from 'semantic-ui-react'

import Header from './Header'
import Body from './Body'
import Footer from './Footer'

export default class Table extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <SemanticUiTable striped sortable selectable celled compact singleLine fixed size='small'>
                <Header runs={this.props.runs} actions={this.props.actions}/>
                <Body runs={this.props.runs} actions={this.props.actions}/>
                <Footer runs={this.props.runs} actions={this.props.actions}/>
            </SemanticUiTable>
        )
    }
}