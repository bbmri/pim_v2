import React, {Component} from 'react'

import * as constants from '../../../utilities/constants'
import {withStatus} from "../../WithStatus";
import PropTypes from "prop-types";

class JobCountBar extends Component {
    render() {
        return <div key={this.props.jobType}
                    className={this.props.classes}
                    style={this.props.style}
                    onClick={() => this.props.viewJobs(constants.JOB_STATUS_TYPES[this.props.jobType])}/>
    }
}

JobCountBar.propTypes = {
    jobCount: PropTypes.number.isRequired,
    jobType: PropTypes.number.isRequired,
    classes: PropTypes.string.isRequired,
    style: PropTypes.object.isRequired
};

let JobCountBarWithStatus = withStatus(JobCountBar, (props) => {
    function jobCountDescription(jobCount, jobType) {
        const descriptions = {
            single: ['is idle', 'is running', 'has succeeded', 'has failed', 'is cancelled', 'is undefined'],
            multiple: ['are idle', 'are running', 'have succeeded', 'have failed', 'are cancelled', 'are undefined']
        };
        
        let description = descriptions[jobCount === 1 ? 'single' : 'multiple'][jobType];
        
        return `${jobCount} ${jobCount === 1 ? 'job' : 'jobs'} ${description}`
    }
    
    return <span>{jobCountDescription(props.jobCount, props.jobType)}, click to inspect</span>
});

export default class Jobs extends Component {
    viewJobs(statusType) {
        this.props.actions.goToJobsPage(this.props.run.name, [statusType]);
    }
    
    render() {
        const noJobs = this.props.jobs.reduce((a, b) => a + b, 0);
        
        if (noJobs === 0) {
            let style = {
                flex: '100%',
                height: '100%',
                float: 'left',
                backgroundColor: 'hsl(0, 0%, 60%)',
                opacity: 0.95
            };
            
            return (
                <div className="run jobs">
                    <div className="bars">
                        <div style={style}/>
                    </div>
                </div>
            )
        }
        
        const bars = this.props.jobs.map((jobCount, index) => {
                const jobStatusType = constants.JOB_STATUS_TYPES[index];
                const percentage    = 100.0 * (jobCount / noJobs);
                const classes       = `${jobStatusType} bar`;
                const style         = {
                    flex: `${percentage}%`,
                    height: "100%",
                    float: ' left'
                };
                
                return <JobCountBarWithStatus key={jobStatusType}
                                              jobCount={jobCount}
                                              jobType={index}
                                              classes={classes}
                                              style={style}
                                              viewJobs={this.viewJobs.bind(this)}/>
            }
        );
        
        return (
            <div className="run jobs">
                <div className="bars">{bars}</div>
            </div>
        )
    }
}
