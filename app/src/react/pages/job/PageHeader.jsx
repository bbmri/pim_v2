import React from 'react';

import {
    Icon,
    Header
} from 'semantic-ui-react'

export default class PageHeader extends React.Component {
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.store.get('jobPath') !== this.props.store.get('jobPath');
    }

    render() {
        const postfix = ['is idle', 'is running', 'has succeeded', 'has failed', 'is cancelled', 'is undefined'];

        return (
            <Header icon textAlign='center' size='small'>
                <Icon name='tasks' color='black'/>View job
                <Header.Subheader>
                    {/*{this.props.store.get('runName')}
                    {'/.../'}*/}
                    {this.props.store.get('jobPath').split('/').slice(-1)}
                    &nbsp;
                    {postfix[this.props.store.get('job').get('status')]}
                </Header.Subheader>
            </Header>
        )
    }
}