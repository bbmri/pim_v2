import React from 'react';

import JSONTree from 'react-json-tree'

export default class CustomData extends React.Component {
    shouldExpandNode(keyName, data, level) {
        return level < 1;
    }

    render() {
        const job = this.props.store.get('job').toJS();

        const theme = {
            scheme: 'pim',
            author: 'Thomas Kroes',
            base00: '#ffffff',
            base01: '#ffffff',
            base02: '#a0a0a0',
            base03: '#a0a0a0',
            base04: '#0020a5',
            base05: '#f82f1e',
            base06: '#f52924',
            base07: '#f9322e',
            base08: '#ac0e00',
            base09: '#f9ba02',
            base0A: '#f4bf75',
            base0B: '#000000',
            base0C: '#f9322e',
            base0D: '#3c3c3c',
            base0E: '#f9322e',
            base0F: '#f9322e'
        };

        return (
            <JSONTree data={job}
                      hideRoot={true}
                      theme={theme}
                      invertTheme={false}
                      valueRenderer={raw => <em>{raw}</em>}
                      shouldExpandNode={this.shouldExpandNode.bind(this)}
                      getItemString={(type, data, itemType, itemString) => null}/>
        )
    }
}