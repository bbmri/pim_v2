import React, {Component} from 'react'
import {connect} from "react-redux"
import {bindActionCreators} from "redux"
import {withRouter} from 'react-router-dom'
import {withCookies, Cookies} from 'react-cookie';

const queryString = require('query-string')

import {
    Segment,
    Header
} from 'semantic-ui-react'

import * as actions from "../../../actions/job";

import PageHeader from './PageHeader'
import Navigation from './../../Navigation'
import Breadcrumb from './Breadcrumb'
import CustomData from './CustomData'

class Container extends Component {
    constructor() {
        super();
    }
    
    componentCleanup() {
        
        // Remove event listener
        window.removeEventListener('beforeunload', this.componentCleanup.bind(this));
    }
    
    componentWillMount() {
    }
    
    componentWillUnmount() {
        this.componentCleanup();
    }
    
    componentDidMount() {
        const run    = this.props.match.params.run;
        const parsed = queryString.parse(this.props.location.search);
        
        // Determine the relative job path (with respect to the run)
        const relativeJobPath = parsed.path.split('/').slice(1).join('/');
        
        // logger.info(parsed.path, relativeJobPath);
        
        // Initialize the reducer
        this.props.actions.initialize(run, relativeJobPath);
        
        // Cleanup the component before the tab is unloaded
        window.addEventListener('beforeunload', this.componentCleanup.bind(this));
    }
    
    /*    shouldComponentUpdate(nextProps, nextState) {
     return this.props != nextProps;
     }*/
    
    pageTitle() {
        return `${this.props.job.get('runName')} job view`;
    }
    
    goToRunsPage() {
        this.props.history.push({
            pathname: `/runs`,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.pageTitle()
            }
        });
    }
    
    goToGraphPage() {
        this.props.history.push({
            pathname: `/runs/${this.props.job.get('runName')}`,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.pageTitle()
            }
        });
    }
    
    goToJobsPage(nodePath) {
        this.props.history.push({
            pathname: `/runs/${this.props.job.get('runName')}/jobs`,
            search: `?node=${nodePath}`,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.pageTitle()
            }
        });
    }
    
    render() {
        let actions = {
            goToRunsPage: this.goToRunsPage.bind(this),
            goToGraphPage: this.goToGraphPage.bind(this),
            goToJobsPage: this.goToJobsPage.bind(this)
        };
        
        return (
            <div style={{flexGrow: 1, display: 'flex', flexDirection: 'column'}}>
                <PageHeader store={this.props.job} actions={actions}/>
                <Navigation breadcrumbs={<Breadcrumb store={this.props.job} actions={actions}/>}
                            location={this.props.location}/>
                <Segment attached='bottom'>
                    <Header as='h2'>Job details</Header>
                    <CustomData store={this.props.job} actions={actions}/>
                </Segment>
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        job: state.get('job')
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

export default withCookies(withRouter(connect(mapStateToProps, mapDispatchToProps)(Container)));