import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'

import {
    Breadcrumb as SemanticUiBreadcrumb
} from 'semantic-ui-react'

import BreadcrumbLink from '../../BreadcrumbLink'
import NodeJobsLink from './../../NodeJobsLink'
import {withStatus} from "../../WithStatus";

class RunsLink extends Component {
    render() {
        return <BreadcrumbLink
            name='runs'
            popup='Go to runs overview'
            action={this.props.actions.goToRunsPage}>
        </BreadcrumbLink>
    }
}

let RunsLinkWithStatus = withStatus(RunsLink, (props) => {
    return <span>Go to the runs overview page</span>
});

class GraphLink extends Component {
    render() {
        return <BreadcrumbLink
            name={this.props.runName}
            popup='Go to runs overview'
            action={this.props.actions.goToGraphPage}>
        </BreadcrumbLink>
    }
}

let GraphLinkWithStatus = withStatus(GraphLink, (props) => {
    return <span>View graph representation of the pipeline run</span>
});

class Breadcrumb extends React.Component {
    render() {
        const nodePathSegments = this.props.store.get('jobPath').split('/');
        
        const nodePath = [];
        let paths      = [];
        
        for (let i = 0; i < nodePathSegments.length; i++) {
            const nodeName = nodePathSegments[i];
            
            paths.push(nodeName);
            
            if (i === nodePathSegments.length - 1) {
                nodePath.push(<SemanticUiBreadcrumb.Section key={nodeName} active>{nodeName}</SemanticUiBreadcrumb.Section>)
            } else {
                nodePath.push(<NodeJobsLink key={nodeName}
                                            runName={this.props.store.get('runName')}
                                            title={nodeName}
                                            nodePath={nodePathSegments.slice(0, i + 1).join('/')}
                                            pageTitle={'job'}>{nodeName}</NodeJobsLink>);
            }
            
            if (i < nodePathSegments.length - 1) {
                nodePath.push(<SemanticUiBreadcrumb.Divider key={i} icon='right angle'/>);
            }
        }
        
        return (
            <SemanticUiBreadcrumb>
                <SemanticUiBreadcrumb.Section>pim</SemanticUiBreadcrumb.Section>
                <SemanticUiBreadcrumb.Divider icon='right angle'/>
                <RunsLinkWithStatus actions={this.props.actions}/>
                <SemanticUiBreadcrumb.Divider icon='right angle'/>
                <GraphLinkWithStatus runName={this.props.store.get('runName')} actions={this.props.actions}/>
                <SemanticUiBreadcrumb.Divider icon='right angle'/>
                {nodePath}
            </SemanticUiBreadcrumb>
        );
    }
}

export default withRouter(Breadcrumb);