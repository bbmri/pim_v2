import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'
import {withStatus} from './WithStatus'

class NodeJobsLink extends Component {
    onClick() {
        const filter = this.props.filter === undefined ? '' : `filter=${this.props.filter}`;
        
        this.props.history.push({
            pathname: `/runs/${this.props.runName}/jobs`,
            search: `?node=${this.props.nodePath}${filter}`,
            state: {
                from: this.props.location.pathname,
                fromPageTitle: this.props.pageTitle
            }
        });
    }
    
    render() {
        return (
            <a className='node-path' onClick={this.onClick.bind(this)}>{this.props.title}</a>
        );
    }
}

export default withRouter(withStatus(NodeJobsLink, (props) => {
    return <span>Click to inspect <b>{props.title}</b> jobs</span>
}));