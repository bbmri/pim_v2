import React from 'react'
import {withRouter} from 'react-router-dom'

import {
    Popup,
    Breadcrumb
} from 'semantic-ui-react'

class BreadcrumbLink extends React.Component {
    render() {
        return <Breadcrumb.Section link onClick={() => this.props.action()}>
            {this.props.name}
        </Breadcrumb.Section>;
    }
}

export default withRouter(BreadcrumbLink);