import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {withStatus} from './WithStatus'

import {
    Table
} from 'semantic-ui-react'

class HeaderCell extends Component {
    render() {
        const sortable    = this.props.store.get('sortColumn') !== '';
        const sortActive  = sortable && (this.props.store.get('sortColumn') === this.props.sortColumn);
        
        return <Table.HeaderCell style={this.props.style}
                                 textAlign="left"
                                 sorted={sortActive ? this.props.store.get('sortDirection') : null}
                                 onClick={() => this.props.actions.setSortColumn(this.props.sortColumn)}>{this.props.title}</Table.HeaderCell>
    }
}

HeaderCell.propTypes = {
    sortColumn: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    style: PropTypes.object.isRequired
};

export default withStatus(HeaderCell, (props) => {
    if (props.sortColumn === '')
        return null;
    
    const sorted     = props.store.get('sortColumn') === props.sortColumn;
    const directions = ['ascending', 'descending'];
    
    let sortDirection = directions[0];
    
    if (sorted) {
        sortDirection = props.store.get('sortDirection') === directions[0] ? directions[1] : directions[0];
    }
    
    return <div>Click to sort by <b style={{textTransform: 'lowerCase'}}>{props.title}</b> (in {sortDirection} order)
    </div>;
});