import React, {Component} from 'react'
import ReactDOM from 'react-dom'

import {
    Popup
} from 'semantic-ui-react'

import PubSub from 'pubsub-js'

export function withStatus(WrappedComponent, status = null) {
    return class extends React.Component {
        constructor(props) {
            super(props);
            
            this.handleMouseEnter         = this.handleMouseEnter.bind(this);
            this.handleMouseLeave         = this.handleMouseLeave.bind(this);
            this.bindListeners            = this.bindListeners.bind(this);
            this.state                    = {componentIsHovered: false};
            this.status                   = status;
            this.wrappedComponent         = null;
            this.wrappedComponentInstance = null;
        }
        
        componentWillUnmount() {
            if (this.wrappedComponent) {
                this.wrappedComponent.removeEventListener('mouseenter', this.handleMouseEnter);
                this.wrappedComponent.removeEventListener('mouseleave', this.handleMouseLeave);
            }
        }
        
        handleMouseEnter() {
            this.setState({componentIsHovered: true});
            
            if (this.status !== null) {
                PubSub.publish('STATUS_SET', this.status(this.props));
            }
        }
        
        handleMouseLeave() {
            this.setState({componentIsHovered: false});
            
            PubSub.publish('STATUS_UNSET');
        }
        
        componentDidUpdate(prevProps, prevState, snapshot) {
            if (this.state.componentIsHovered && this.status !== null) {
                PubSub.publish('STATUS_SET', this.status(this.props));
            }
        }
        
        bindListeners(wrappedComponentInstance) {
            if (!wrappedComponentInstance) {
                return;
            }
            
            this.wrappedComponentInstance = wrappedComponentInstance;
            this.wrappedComponent         = ReactDOM.findDOMNode(wrappedComponentInstance);
            this.wrappedComponent.addEventListener('mouseenter', this.handleMouseEnter);
            this.wrappedComponent.addEventListener('mouseleave', this.handleMouseLeave);
        }
        
        render() {
            const props   = Object.assign({}, this.props, {ref: this.bindListeners})
            const wrapped = <WrappedComponent ref='wrappedComponent'
                                              componentIsHovered={this.state.componentIsHovered}
                                              {...props}/>
            return wrapped;
            return <Popup ref='popup'
                          trigger={wrapped}
                          content="The default theme's basic popup removes the pointing arrow."
                          basic/>
        }
    };
}