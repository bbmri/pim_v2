import React from 'react';

import Container from './pages/run/Container'
import Footer from './footer/Footer'

export default class JobStatusLegend extends React.Component {
    render() {
        return (
            <div className='legend job-status'>
                <div className='status idle'></div><div className='label'>Idle</div>
                <div className='status running'></div><div className='label'>Running</div>
                <div className='status success'></div><div className='label'>Success</div>
                <div className='status failed'></div><div className='label'>Failed</div>
                <div className='status cancelled'></div><div className='label'>Cancelled</div>
                <div className='status uninitialized'></div><div className='label'>Uninitialized</div>
            </div>
        )
    }
}