import React, {Component} from 'react'
import {withRouter} from 'react-router-dom'

import {
    Segment,
    Popup
} from 'semantic-ui-react'

class Navigation extends Component {
    constructor() {
        super();
    }
    
    goBack() {
        this.props.history.push({
            pathname: this.props.location.state.from,
            state: {
                from: this.props.location.state.from,
                fromPageTitle: this.props.location.state.fromPageTitle
            }
        });
    }
    
    render() {
        let link = null;
        
        try {
            const a = <a href='#' onClick={this.goBack.bind(this)}>Back</a>
            
            link = <Popup flowing
                             trigger={a}
                             content={`Go back to ${this.props.location.state.fromPageTitle}`}/>;
        } catch (e) {
        }
        
        return (
            <Segment attached='top' secondary>
                <div style={{display: 'flex'}}>
                    <div style={{flex: 1}}>
                        {this.props.breadcrumbs}
                    </div>
                    {/*{link}*/}
                </div>
            </Segment>
        )
    }
}

export default withRouter(Navigation);