import React, {Component} from 'react'
import PubSub from 'pubsub-js'

import {
    Menu
} from 'semantic-ui-react'

export default class Status extends Component {
    constructor() {
        super();
        
        this.state = {
            status: null
        };
    }
    
    componentDidMount() {
        PubSub.subscribe('STATUS_SET', this.onStatusSet.bind(this));
        PubSub.subscribe('STATUS_UNSET', this.onStatusUnset.bind(this));
    }
    
    componentWillUnmount() {
        PubSub.unsubscribe('STATUS_SET');
        PubSub.unsubscribe('STATUS_UNSET');
    }
    
    shouldComponentUpdate(nextProps, nextState) {
        return nextState !== this.state;
    }
    
    onStatusSet(message, data) {
        // if (!this.refs.status)
        //     return;
        
        this.setState(_.merge({}, this.state, {
            status: data === null ? <span>&nbsp;</span> : data
        }));
    }
    
    onStatusUnset(message, data) {
        this.setState(_.merge({}, this.state, {
            status: null
        }));
    }
    
    render() {
        let status = this.state.status;
        
        if (this.state.status === null) {
            return <Menu.Item name='status' style={{flex: 1}}>
                <span style={{color: 'grey'}}>Ready</span>
            </Menu.Item>
        }
        
        return <Menu.Item name='status' style={{flex: 1, overflow: 'hidden', textOverflow: 'ellipsis'}}>
            {/*<div style={{overflow: 'hidden', textOverflow: 'ellipsis'}}>*/}
            <span style={{flex: 1, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'noWrap'}}>{status}</span>
            {/*</div>*/}
        </Menu.Item>
    }
}