import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'

import {
    Menu,
    Icon
} from 'semantic-ui-react'

import Status from './Status'
import JobStatusLegend from './../JobStatusLegend'
import LastUpdated from './LastUpdated'
import CurrentRevision from './CurrentRevision'
import ApiDocumentation from './ApiDocumentation'
import AppApi from '../../api/api'


class Footer extends Component {
    constructor() {
        super();
        
        this.state = {
            lastUpdated: '',
            currentRevision: ''
        };
    }
    
    componentDidMount() {
        if (this.state.lastUpdated !== '')
            return;
        
        AppApi.fetchApiInfo()
            .then((response) => {
                this.setState({
                    lastUpdated: response.data.deployment.lastUpdated,
                    currentRevision: response.data.deployment.currentRevision
                });
            })
    }
    
    render() {
        return (
            <div className="footer">
                <Menu size='small' style={{display: 'flex', backgroundColor: '#f3f4f5'}} compact>
                    <Menu.Item name='logo'>
                        <Icon name='sitemap'/>
                    </Menu.Item>
                    <Status/>
                    <Menu.Item name='jobStatusLegend'>
                    <JobStatusLegend/>
                    </Menu.Item>
                        <LastUpdated lastUpdated={this.state.lastUpdated}/>
                    <CurrentRevision currentRevision={this.state.currentRevision}/>
                    <ApiDocumentation/>
                </Menu>
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        app: state
    }
}

function mapDispatchToProps(dispatch) {
    return {}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Footer));