import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'

import {
    Menu,
    Icon,
    Popup,
    Message
} from 'semantic-ui-react'

import PubSub from 'pubsub-js'

import AppApi from '../../api/api'
import {withStatus} from "../WithStatus";

class CurrentRevision extends Component {
    constructor() {
        super();
    }
    
    render() {
        const repoLink = `https://gitlab.com/bbmri/pim/commit/${this.props.currentRevision}`;
        
        return <Menu.Item name='currentRevision'>
            <a href={repoLink}>{this.props.currentRevision.substring(0, 7)}</a>
        </Menu.Item>
    }
}

export default withStatus(CurrentRevision, (props) => {
    return <span>Explore revision <b>{props.currentRevision}</b> on GitHub</span>;
});