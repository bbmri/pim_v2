import React, {Component} from 'react'

import {
    Menu
} from 'semantic-ui-react'

import {withStatus} from "../WithStatus";

class LastUpdated extends Component {
    constructor() {
        super();
    }
    
    render() {
        return <Menu.Item name='lastUpdated'>
            {this.props.lastUpdated}
        </Menu.Item>
    }
}

export default withStatus(LastUpdated, (props) => {
    return <span>PIM was last updated on <b>{props.lastUpdated}</b></span>;
});