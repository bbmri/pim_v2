import React, {Component} from 'react'
import {withStatus} from '../WithStatus'

import {
    Menu
} from 'semantic-ui-react'

class ApiDocumentation extends Component {
    render() {
        return <Menu.Item name='apiDocumentation'>
            <a href='/api/documentation'>API Documentation</a>
        </Menu.Item>
    }
}

export default withStatus(ApiDocumentation, (props) => {
    return <span>Visit the Swagger REST API documentation page</span>;
});