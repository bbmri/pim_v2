import React, {Component} from 'react'

import {
    Menu
} from 'semantic-ui-react'

import _ from 'lodash'

import PageSizes from './PageSizes'
import FirstPage from './FirstPage'
import PreviousPage from './PreviousPage'
import Page from './Page'
import NextPage from './NextPage'
import LastPage from './LastPage'

export default class Pagination extends Component {
    constructor() {
        super();
    }

    shouldComponentUpdate(nextProps, nextState) {
        const attributes = ['noPages', 'pageSize', 'pageIndex', 'noPagesPerView'];

        for (let attribute of attributes) {
            if (this.props.store.get(attribute) !== nextProps.store.get(attribute)) {
                return true;
            }
        }

        return false;
    }

    render() {
        let pages = _.map(_.range(this.props.store.get('noPages')), (pageIndex) => (
            <Page store={this.props.store} actions={this.props.actions} key={pageIndex}
                            pageIndex={pageIndex}/>));

        // Limit the amount of page buttons at a time
        const viewIndex = Math.floor(this.props.store.get('pageIndex') / this.props.store.get('noPagesPerView'));
        const pageSegmentStart = (viewIndex) * this.props.store.get('noPagesPerView');
        const pageSegmentEnd = (viewIndex + 1) * this.props.store.get('noPagesPerView');

        pages.splice(pageSegmentEnd, this.props.store.get('noPages') - pageSegmentEnd);
        pages.splice(0, pageSegmentStart);
        
        return (
            <Menu floated='right' pagination size='tiny'>
                <PageSizes store={this.props.store} actions={this.props.actions}/>
                <FirstPage store={this.props.store} actions={this.props.actions}/>
                <PreviousPage store={this.props.store} actions={this.props.actions}/>
                {pages}
                <NextPage store={this.props.store} actions={this.props.actions}/>
                <LastPage store={this.props.store} actions={this.props.actions}/>
            </Menu>
        )
    }
}
