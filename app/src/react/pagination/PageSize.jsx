import React, {Component} from 'react'

import {
    Dropdown
} from 'semantic-ui-react'

import {withStatus} from '../WithStatus'

class PageSize extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <Dropdown.Item
                onClick={() => { this.props.actions.setPageSize(this.props.pageSize); }}
            >{this.props.pageSize} Runs/page</Dropdown.Item>
        )
    }
}

export default withStatus(PageSize, (props) => {
    if (props.store.get('noPages') === 0)
        return null;
    
    if (props.store.get('pageIndex') === (props.store.get('noPages') - 1))
        return null;
    
    return <span>Show {props.pageSize} items per page</span>;
})