import React, {Component} from 'react'

import {
    Menu,
    Icon
} from 'semantic-ui-react'

import {withStatus} from '../WithStatus'

class LastPage extends Component {
    constructor() {
        super();
    }

    render() {
        let disabled = this.props.store.get('pageIndex') === (this.props.store.get('noPages') - 1);

        if (this.props.store.get('noPages') === 0)
            disabled = true;

        return (
            <Menu.Item as='a'
                       onClick={() => { this.props.actions.lastPage(); }}
                       disabled={disabled}>
                <Icon name='double angle right'/>
            </Menu.Item>
        )
    }
}

export default withStatus(LastPage, (props) => {
    if (props.store.get('noPages') === 0)
        return null;
    
    if (props.store.get('pageIndex') === (props.store.get('noPages') - 1))
        return null;
    
    return <span>Go to the last page (page {props.store.get('noPages')} of {props.store.get('noPages')})</span>;
})