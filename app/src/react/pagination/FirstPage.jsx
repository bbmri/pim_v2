import React, {Component} from 'react'

import {
    Menu,
    Icon
} from 'semantic-ui-react'

import {withStatus} from '../WithStatus'

class FirstPage extends Component {
    constructor() {
        super();
    }

    render() {
        let disabled = this.props.store.get('pageIndex') === 0;

        if (this.props.store.get('noPages') === 0)
            disabled = true;

        return (
            <Menu.Item as='a'
                       onClick={() => { this.props.actions.firstPage(); }}
                       disabled={disabled}>
                <Icon name='double angle left'/>
            </Menu.Item>
        )
    }
}

export default withStatus(FirstPage, (props) => {
    if (props.store.get('noPages') === 0)
        return null;
    
    if (props.store.get('pageIndex') === 0)
        return null;
    
    return <span>Go to the first page (page 1 of {props.store.get('noPages')})</span>;
})