import React, {Component} from 'react'

import {
    Menu,
    Icon
} from 'semantic-ui-react'

import {withStatus} from '../WithStatus'

class NextPage extends Component {
    constructor() {
        super();
    }

    render() {
        let disabled = this.props.store.get('pageIndex') === (this.props.store.get('noPages') - 1);

        if (this.props.store.get('noPages') === 0)
            disabled = true;

        return (
            <Menu.Item as='a'
                       onClick={() => { this.props.actions.nextPage(); }}
                       disabled={disabled}>
                <Icon name='angle right'/>
            </Menu.Item>
        )
    }
}

export default withStatus(NextPage, (props) => {
    if (props.store.get('noPages') === 0)
        return null;
    
    if (props.store.get('pageIndex') === (props.store.get('noPages') - 1))
        return null;
    
    return <span>Go to the next page (page {props.store.get('pageIndex') + 2} of {props.store.get('noPages')})</span>;
})