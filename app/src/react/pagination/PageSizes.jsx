import React, {Component} from 'react'

import {
    Dropdown
} from 'semantic-ui-react'

import {withStatus} from '../WithStatus'

import PageSize from './PageSize'

class PageSizes extends Component {
    constructor() {
        super();
    }
    
    render() {
        const pageSizeDisabled = this.props.store.get('noPages') === 0;
        
        return <Dropdown item
                         text={this.props.store.get('pageSize') + ' Rows/page'}
                         compact={false} upward
                         disabled={pageSizeDisabled}>
            <Dropdown.Menu>
                <PageSize pageSize={5} store={this.props.store} actions={this.props.actions}/>
                <PageSize pageSize={10} store={this.props.store} actions={this.props.actions}/>
                <PageSize pageSize={25} store={this.props.store} actions={this.props.actions}/>
                <PageSize pageSize={50} store={this.props.store} actions={this.props.actions}/>
                <PageSize pageSize={100} store={this.props.store} actions={this.props.actions}/>
                <PageSize pageSize={250} store={this.props.store} actions={this.props.actions}/>
            </Dropdown.Menu>
        </Dropdown>
    }
}

export default withStatus(PageSizes, (props) => {
    if (props.store.get('noPages') === 0)
        return null;
    
    return <span>Show {props.store.get('pageSize')} rows per page (Click to change)</span>;
})