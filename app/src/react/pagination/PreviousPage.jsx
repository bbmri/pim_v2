import React, {Component} from 'react'

import {
    Menu,
    Icon
} from 'semantic-ui-react'

import {withStatus} from '../WithStatus'

class PreviousPage extends Component {
    constructor() {
        super();
    }

    render() {
        let disabled = this.props.store.get('pageIndex') === 0;

        if (this.props.store.get('noPages') === 0)
            disabled = true;

        return (
            <Menu.Item as='a'
                       onClick={() => { this.props.actions.previousPage(); }}
                       disabled={disabled}>
                <Icon name='angle left'/>
            </Menu.Item>
        )
    }
}

export default withStatus(PreviousPage, (props) => {
    if (props.store.get('noPages') === 0)
        return null;
    
    if (props.store.get('pageIndex') === 0)
        return null;
    
    return <span>Go to the previous page (page {props.store.get('pageIndex')} of {props.store.get('noPages')})</span>;
})