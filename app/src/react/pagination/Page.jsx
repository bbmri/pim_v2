import React, {Component} from 'react'

import {
    Menu
} from 'semantic-ui-react'

import {withStatus} from '../WithStatus'

class Page extends Component {
    constructor() {
        super();
    }

    render() {
        let disabled = this.props.pageIndex === this.props.store.get('pageIndex');

        return (
            <Menu.Item as='a'
                       onClick={() => {
                           this.props.actions.setPageIndex(this.props.pageIndex);
                       }}
                       disabled={disabled}>
                {this.props.pageIndex + 1}
            </Menu.Item>
        )
    }
}

export default withStatus(Page, (props) => {
    if (props.store.get('noPages') === 0)
        return null;
    
    if (props.pageIndex === props.store.get('pageIndex'))
        return null;
    
    return <span>Go to page {props.pageIndex + 1} of {props.store.get('noPages')}</span>;
})