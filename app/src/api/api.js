import axios from 'axios'

class AppApi {
    static fetchApiInfo() {
        logger.info('Fetching aggregates from server');
        
        return axios.get(`/api/info`);
    }
    
    static fetchRuns({nameFilter = '', userFilter = '', sortColumn = '', sortDirection = 'ascending', pageSize = 10, pageIndex = 0}) {
        // logger.info('Fetching runs from server');
        
        const nameFilterArg    = `name_filter=${nameFilter}`;
        const userFilterArg    = `user_filter=${userFilter}`;
        const sortColumnArg    = `sort_column=${sortColumn}`;
        const sortDirectionArg = `sort_direction=${sortDirection}`;
        const pageSizeArg      = `page_size=${pageSize}`;
        const pageIndexArg     = `page_index=${pageIndex}`;
        
        return axios.get(`/api/runs?${nameFilterArg}&${userFilterArg}&${sortColumnArg}&${sortDirectionArg}&${pageSizeArg}&${pageIndexArg}`);
    }
    
    static removeRun(name) {
        // logger.info('Removing run from server');
        
        return axios.delete(`/api/runs/${name}`);
    }
    
    static fetchNodes(runName, nodes) {
         // logger.info('Fetching nodes from server');
        
        const url = `/api/runs/${runName}/nodes?filter_nodes=${JSON.stringify(nodes)}`;
        return axios.get(url);
    }

    static fetchNodesByMaxDepth(runName, maxDepth) {
        // logger.info('Fetching nodes by max depth from server');
        
        const url = `/api/runs/${runName}/nodes?max_depth=${maxDepth}`;
        return axios.get(url);
    }
    
    static fetchLinks(runName, nodes) {
        // logger.info('Fetching links from server');
        
        const url = `/api/runs/${runName}/links?nodes=${JSON.stringify(nodes)}`;
        return axios.get(url);
    }
    
    static fetchAggregates(runName, nodes) {
        // logger.info('Fetching aggregates from server');
        
        const url = `/api/runs/${runName}/aggregates?nodes=${JSON.stringify(nodes)}`;
        return axios.get(url);
    }
    
    static fetchJobs({runName = '', nodePath = 'root', filter = [], sortColumn = '', sortDirection = 'ascending', pageSize = 10, pageIndex = 0}) {
        // logger.info('Fetching jobs from server');
        
        const nodeArg          = `node=${nodePath}`;
        const filterArg        = `filter=${filter}`;
        const sortColumnArg    = `sort_column=${sortColumn}`;
        const sortDirectionArg = `sort_direction=${sortDirection}`;
        const pageSizeArg      = `page_size=${pageSize}`;
        const pageIndexArg     = `page_index=${pageIndex}`;
        const url              = `/api/runs/${runName}/jobs?${nodeArg}&${filterArg}&${sortColumnArg}&${sortDirectionArg}&${pageSizeArg}&${pageIndexArg}`;
        
        return axios.get(url);
    }
    
    static fetchJob(runName, jobPath) {
        // logger.info('Fetching job from server');
        
        return axios.get(`/api/runs/${runName}/job?path=${jobPath}`);
    }
}

export default AppApi;