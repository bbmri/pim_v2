import * as types from './types';

import AppApi from '../api/api';
import {synchronizeStructure} from "./run";

export function setUpdating(updating) {
    return function (dispatch) {
        dispatch({type: 'RUNS_SET_UPDATING', payload: updating});
    }
}

export function fetchRuns() {
    logger.debug('Fetching runs');
    
    return function (dispatch, getState) {

        dispatch({type: 'RUNS_SET_UPDATING', payload: true});

        const runs = getState().get('runs');

        let params = {
            nameFilter: runs.get('nameFilter'),
            userFilter: runs.get('userFilter'),
            sortColumn: runs.get('sortColumn'),
            sortDirection: runs.get('sortDirection'),
            pageSize: runs.get('pageSize'),
            pageIndex: runs.get('pageIndex')
        };

        AppApi.fetchRuns(params)
            .then((response) => {
                dispatch({type: 'RUNS_SET_PAGE_ROWS', payload: response.data.runs});
                dispatch({type: 'RUNS_SET_NO_ROWS', payload: response.data.noRuns});
                dispatch({type: 'RUNS_UPDATE_PAGINATION', payload: response.data.noFilteredRuns});
                dispatch({type: 'RUNS_SET_UPDATING', payload: false});
            });
    }
}

export function removeRun(name) {
    logger.debug('Removing run');
    
    return function (dispatch, getState) {

        dispatch({type: 'RUNS_SET_UPDATING', payload: true});

        AppApi.removeRun(name)
            .then((response) => {
                dispatch({type: 'RUNS_SET_UPDATING', payload: false});

                dispatch(fetchRuns());
            });
    }
}

export function setNameFilter(nameFilter) {
    return function (dispatch) {
        dispatch({type: 'RUNS_SET_NAME_FILTER', payload: nameFilter});
    }
}

export function setSortColumn(sortColumn) {
    return function (dispatch) {
        dispatch({type: 'RUNS_SET_SORT_COLUMN', payload: sortColumn});
    }
}

export function setSortDirection(sortDirection) {
    return function (dispatch) {
        dispatch({type: 'RUNS_SET_SORT_DIRECTION', payload: sortDirection});
    }
}

export function setPageSize(pageSize) {
    return function (dispatch) {
        dispatch({type: 'RUNS_SET_PAGE_SIZE', payload: pageSize});
    }
}

export function setPageIndex(pageIndex) {
    return function (dispatch) {
        dispatch({type: 'RUNS_SET_PAGE_INDEX', payload: pageIndex});
    }
}

export function firstPage() {
    return function (dispatch) {
        dispatch({type: 'RUNS_SET_PAGE_INDEX', payload: 0});
    }
}

export function previousPage() {
    return function (dispatch, getState) {
        dispatch({type: 'RUNS_SET_PAGE_INDEX', payload: getState().getIn(['runs', 'pageIndex']) - 1});
    }
}

export function nextPage() {
    return function (dispatch, getState) {
        dispatch({type: 'RUNS_SET_PAGE_INDEX', payload: getState().getIn(['runs', 'pageIndex']) + 1});
    }
}

export function lastPage() {
    return function (dispatch, getState) {
        dispatch({type: 'RUNS_SET_PAGE_INDEX', payload: getState().getIn(['runs', 'noPages']) - 1});
    }
}