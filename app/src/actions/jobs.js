import * as types from './types'

import AppApi from '../api/api'

export function setRunName(runName) {
    return function (dispatch) {
        dispatch({type: 'JOBS_SET_RUN_NAME', payload: runName});
    }
}

export function setNodePath(nodePath) {
    return function (dispatch) {
        dispatch({type: 'JOBS_SET_NODE_PATH', payload: nodePath});
    }
}

export function fetchJobs() {
    return function (dispatch, getState) {
        
        const jobs = getState().get('jobs');
        
        let filterMap = jobs.get('filter').toJS();
        let filter    = [];
        
        for (let key in filterMap) {
            if (filterMap[key]) {
                filter.push(key);
            }
        }
        
        let params = {
            runName: jobs.get('runName'),
            nodePath: jobs.get('nodePath'),
            recursive: jobs.get('recursive'),
            filter: JSON.stringify(filter),
            sortColumn: jobs.get('sortColumn'),
            sortDirection: jobs.get('sortDirection'),
            pageSize: jobs.get('pageSize'),
            pageIndex: jobs.get('pageIndex')
        };
        
        try {
            AppApi.fetchJobs(params)
                .then((response) => {
                    dispatch({type: 'JOBS_SET_PAGE_ROWS', payload: response.data.jobs});
                    dispatch({type: 'JOBS_SET_NO_ROWS', payload: response.data.noJobs});
                    dispatch({type: 'JOBS_SET_AGGREGATE', payload: response.data.aggregate});
                    dispatch({type: 'JOBS_UPDATE_PAGINATION', payload: response.data.noFilteredJobs});
                })
                .catch((error) => {
                    throw error;
                });
        } catch(error) {
            dispatch({type: 'JOBS_SET_FETCH_ERROR', payload: error});
        }
    }
}

export function setFilter(type, state) {
    return function (dispatch, getState) {
        dispatch({type: 'JOBS_SET_FILTER', payload: {type: type, state: state}});
    }
}

export function toggleFilter(type) {
    return function (dispatch, getState) {
        dispatch({type: 'JOBS_TOGGLE_FILTER', payload: type});
    }
}

export function setSortColumn(sortColumn) {
    return function (dispatch) {
        dispatch({type: 'JOBS_SET_SORT_COLUMN', payload: sortColumn});
    }
}

export function setSortDirection(sortDirection) {
    return function (dispatch) {
        dispatch({type: 'JOBS_SET_SORT_DIRECTION', payload: sortDirection});
    }
}

export function setPageSize(pageSize) {
    return function (dispatch) {
        dispatch({type: 'JOBS_SET_PAGE_SIZE', payload: pageSize});
    }
}

export function setPageIndex(pageIndex) {
    return function (dispatch) {
        dispatch({type: 'JOBS_SET_PAGE_INDEX', payload: pageIndex});
    }
}

export function firstPage() {
    return function (dispatch) {
        dispatch({type: 'JOBS_SET_PAGE_INDEX', payload: 0});
    }
}

export function previousPage() {
    return function (dispatch, getState) {
        dispatch({type: 'JOBS_SET_PAGE_INDEX', payload: getState().getIn(['jobs', 'pageIndex']) - 1});
    }
}

export function nextPage() {
    return function (dispatch, getState) {
        dispatch({type: 'JOBS_SET_PAGE_INDEX', payload: getState().getIn(['jobs', 'pageIndex']) + 1});
    }
}

export function lastPage() {
    return function (dispatch, getState) {
        dispatch({type: 'JOBS_SET_PAGE_INDEX', payload: getState().getIn(['jobs', 'noPages']) - 1});
    }
}