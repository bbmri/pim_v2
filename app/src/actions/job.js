import * as types from './types'

import AppApi from '../api/api'

import * as constants from './../utilities/constants'

export function initialize(runName, jobPath) {
    logger.debug('Fetching job');
    
    return function (dispatch) {
        AppApi.fetchJob(runName, jobPath)
            .then((response) => {
                dispatch({
                    type: 'JOB_INITIALIZE', payload: {
                        runName: runName,
                        jobPath: jobPath,
                        job: response.data.job
                    }
                });
            });
    }
}
