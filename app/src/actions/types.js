export const RUNS_SET_PAGE_ROWS      = 'RUNS_SET_PAGE_ROWS';
export const RUNS_SET_NO_ROWS        = 'RUNS_SET_NO_ROWS';
export const RUNS_SET_NAME_FILTER    = 'RUNS_SET_NAME_FILTER';
export const RUNS_SET_SORT_COLUMN    = 'RUNS_SET_SORT_COLUMN';
export const RUNS_SET_SORT_DIRECTION = 'RUNS_SET_SORT_DIRECTION';
export const RUNS_UPDATE_PAGINATION  = 'RUNS_UPDATE_PAGINATION';
export const RUNS_SET_PAGE_SIZE      = 'RUNS_SET_PAGE_SIZE';
export const RUNS_SET_PAGE_INDEX     = 'RUNS_SET_PAGE_INDEX';
export const RUNS_SET_UPDATING       = 'RUNS_SET_UPDATING';

export const JOBS_SET_RUN_NAME       = 'JOBS_SET_RUN_NAME';
export const JOBS_SET_NODE_PATH      = 'JOBS_SET_NODE_PATH';
export const JOBS_SET_SORT_COLUMN    = 'JOBS_SET_SORT_COLUMN';
export const JOBS_SET_SORT_DIRECTION = 'JOBS_SET_SORT_DIRECTION';
export const JOBS_SET_FILTER         = 'JOBS_SET_FILTER';
export const JOBS_TOGGLE_FILTER      = 'JOBS_TOGGLE_FILTER';
export const JOBS_FILTER_ALL         = 'JOBS_FILTER_ALL';
export const JOBS_SET_PAGE_INDEX     = 'JOBS_SET_PAGE_INDEX';
export const JOBS_SET_PAGE_SIZE      = 'JOBS_SET_PAGE_SIZE';
export const JOBS_UPDATE_PAGINATION  = 'JOBS_UPDATE_PAGINATION';
export const JOBS_SET_PAGE_ROWS      = 'JOBS_SET_PAGE_ROWS';
export const JOBS_SET_NO_ROWS        = 'JOBS_SET_NO_ROWS';
export const JOBS_SET_AGGREGATE      = 'JOBS_SET_AGGREGATE';
export const JOBS_SET_FETCH_ERROR    = 'JOBS_SET_FETCH_ERROR';

export const RUN_SET_NAME             = 'RUN_SET_NAME';
export const RUN_LOAD                 = 'RUN_LOAD';
export const RUN_SET_ERROR            = 'RUN_SET_ERROR';
export const RUN_SET_BUSY             = 'RUN_SET_BUSY';
export const RUN_FETCH_ERROR          = 'RUN_FETCH_ERROR';
export const RUN_SET_NODES            = 'RUN_SET_NODES';
export const RUN_SET_LINKS            = 'RUN_SET_LINKS';
export const RUN_SET_AGGREGATES       = 'RUN_SET_AGGREGATES';
export const RUN_EXPAND_LINK          = 'RUN_EXPAND_LINK';
export const RUN_EXPAND_NODE          = 'RUN_EXPAND_NODE';
export const RUN_COLLAPSE_NODE        = 'RUN_COLLAPSE_NODE';
export const RUN_UPDATE_NODES         = 'RUN_UPDATE_NODES';
export const RUN_EXPAND_NODE_HUB      = 'RUN_EXPAND_NODE_HUB';
export const RUN_COLLAPSE_NODE_HUB    = 'RUN_COLLAPSE_NODE_HUB';
export const RUN_REVEAL_NODE          = 'RUN_REVEAL_NODE';
export const RUN_SET_VISIBILITY_LEVEL = 'RUN_SET_VISIBILITY_LEVEL';
export const RUN_SET_LAYOUT           = 'RUN_SET_LAYOUT';
export const RUN_SET_MODIFIED         = 'RUN_SET_MODIFIED';
export const RUN_SET_NODE_FILTER      = 'RUN_SET_NODE_FILTER';


export const JOB_INITIALIZE = 'JOB_INITIALIZE';
