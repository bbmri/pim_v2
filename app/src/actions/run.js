import * as types from './types'

import AppApi from '../api/api'
import Layout from '../visualization/layout'
import Renderer from '../mainstage/renderer'

import MeasureTime from './../utilities/measureTime'
import PubSub from "pubsub-js";


export function setNodeFilter(nodeFilter) {
    return function (dispatch) {
        dispatch({type: 'RUN_SET_NODE_FILTER', payload: nodeFilter});
    }
}

export function expandLink(linkId) {
    return async function (dispatch, getState) {
        dispatch({type: types.RUN_SET_BUSY, payload: true});
        dispatch({type: types.RUN_EXPAND_LINK, payload: linkId});
        dispatch({type: types.RUN_UPDATE_NODES});
        
        await dispatch(synchronizeStructure());
        
        dispatch({type: types.RUN_SET_BUSY, payload: false});
    }
}

// Expand node
export function expandNode(nodeId) {
    return async function (dispatch, getState) {
        dispatch({type: types.RUN_SET_BUSY, payload: true});
        dispatch({type: types.RUN_EXPAND_NODE, payload: nodeId});
        dispatch({type: types.RUN_UPDATE_NODES});
        
        await dispatch(synchronizeStructure());
        
        dispatch({type: types.RUN_SET_BUSY, payload: false});
    }
}

// Collapse node
export function collapseNode(nodeId) {
    return async function (dispatch, getState) {
        dispatch({type: types.RUN_SET_BUSY, payload: true});
        dispatch({type: types.RUN_COLLAPSE_NODE, payload: nodeId});
        dispatch({type: types.RUN_UPDATE_NODES});
        
        await dispatch(synchronizeStructure());
        
        dispatch({type: types.RUN_SET_BUSY, payload: false});
    }
}

// Toggle the expanded state of a node
export function toggleNodeExpanded(nodeId) {
    return async function (dispatch, getState) {
        dispatch({type: types.RUN_SET_BUSY, payload: true});
        
        const expanded = getState().get('run').nodes[nodeId].expanded;
        
        if (expanded === true) {
            dispatch({type: types.RUN_COLLAPSE_NODE, payload: nodeId});
        } else {
            dispatch({type: types.RUN_EXPAND_NODE, payload: nodeId});
        }
        
        dispatch({type: types.RUN_UPDATE_NODES});
        
        await dispatch(synchronizeStructure());
        
        dispatch({type: types.RUN_SET_BUSY, payload: false});
    }
}

// Expand all group nodes in the graph
export function expandAll() {
    return async function (dispatch, getState) {
        dispatch({type: types.RUN_SET_BUSY, payload: true});
        dispatch({type: types.RUN_SET_VISIBILITY_LEVEL, payload: 100});
        dispatch({type: types.RUN_UPDATE_NODES});
        
        await dispatch(synchronizeStructure());
        
        dispatch({type: types.RUN_SET_BUSY, payload: false});
    }
}

// Collapse all group nodes in the graph
export function collapseAll() {
    return async function (dispatch, getState) {
        let timer = new MeasureTime();
        
        dispatch({type: types.RUN_SET_BUSY, payload: true});
        dispatch({type: types.RUN_SET_VISIBILITY_LEVEL, payload: 1});
        dispatch({type: types.RUN_UPDATE_NODES});
        
        await dispatch(synchronizeStructure());
        
        dispatch({type: types.RUN_SET_BUSY, payload: false});
        
        timer.lap('collapse all')
    }
}

// Expand all group nodes until view depth
export function setViewDepth(viewDepth) {
    return function (dispatch, getState) {
        dispatch({type: types.RUN_SET_VISIBILITY_LEVEL, payload: viewDepth});
        dispatch({type: types.RUN_UPDATE_NODES});
    }
}

export function revealNode(nodeId) {
    return async function (dispatch, getState) {
        dispatch({type: types.RUN_SET_BUSY, payload: true});
        
        dispatch({type: types.RUN_REVEAL_NODE, payload: nodeId});
        dispatch({type: types.RUN_UPDATE_NODES});
        
        await dispatch(synchronizeStructure());
        
        dispatch({type: types.RUN_SET_BUSY, payload: false});
    }
}

// Load a pipeline run
export function load(runName) {
    return async function (dispatch, getState) {
        if (runName !== getState().get('run').name) {
            dispatch({type: types.RUN_LOAD, payload: runName});
            
            dispatch({type: types.RUN_SET_BUSY, payload: true});
            
            fetchNodes(runName)
                .then(nodes => {
                    dispatch({type: types.RUN_SET_NODES, payload: nodes});
                    dispatch({type: types.RUN_UPDATE_NODES});
                    
                    dispatch(synchronizeStructure());
                    dispatch(synchronizeStatus());
                    
                    dispatch({type: types.RUN_SET_BUSY, payload: false});
                })
                .catch(error => {
                    dispatch({
                        type: types.RUN_SET_ERROR, payload: {
                            title: 'Unable to fetch nodes',
                            error: error
                        }
                    });
                });
        }
    }
}

// Synchronizes the pipeline status
export function synchronizeStatus() {
    return async function (dispatch, getState) {
        // logger.debug('Synchronizing graph status');
        
        let run = getState().get('run');
        
        if (Renderer.zoomTimer === null && !document.hidden) {
            const nodeIds = Object.keys(run.nodes);
            
            if (nodeIds.length > 0) {
                await AppApi.fetchAggregates(run.name, nodeIds)
                    .then(response => {
                        dispatch({type: types.RUN_SET_AGGREGATES, payload: response.data.aggregates});
                        dispatch({type: types.RUN_SET_MODIFIED});
                    })
                    .catch(error => {
                        dispatch({
                            type: types.RUN_SET_ERROR, payload: {
                                title: 'Unable to synchronize pipeline status',
                                error: error
                            }
                        });
                    });
            }
        }
    }
}

// Fetches links from the server based on the current node tree
async function updateLinks(dispatch, getState) {
    // logger.debug('Update links');
    
    let timer = new MeasureTime();
    
    let run = getState().get('run');
    
    function visibleNodeIds({nodeId = 'root', nodes}) {
        let node = nodes[nodeId];
        
        if (node === undefined)
            return [];
        
        let children = nodeId === 'root' ? ['root'] : [];
        
        if (!node.expanded)
            return children;
        
        for (let childId of node.children) {
            if (nodes[childId] !== undefined) {
                children.push(childId);
                children = children.concat(visibleNodeIds({
                    nodeId: childId,
                    nodes: nodes
                }));
            }
        }
        
        return children;
    }
    
    let nodeIds = visibleNodeIds({nodes: run.nodes});
    
    timer.lap('determine visible node ids');
    
    await AppApi.fetchLinks(run.name, nodeIds)
        .then(response => {
            timer.lap('fetch links');
            
            dispatch({type: types.RUN_SET_LINKS, payload: response.data.links});
            
            timer.lap('set links');
        })
        .catch(error => {
            dispatch({
                type: types.RUN_SET_ERROR, payload: {
                    title: 'Unable to update links',
                    error: error
                }
            });
        });
}

// Updates the graph layout
async function updateLayout(dispatch, getState) {
    // logger.debug('Updating layout');
    
    let run = getState().get('run');
    
    try {
        let computeLayoutTimer = new MeasureTime();
        
        let layout = await Layout.compute(run);
        
        computeLayoutTimer.lap('Compute layout');
        
        dispatch({type: types.RUN_SET_LAYOUT, payload: layout});
        
        computeLayoutTimer.lap('Set layout');
        
    } catch (error) {
        logger.error(error);
        /*dispatch({
         type: types.RUN_SET_ERROR, payload: {
         title: 'Unable to update layout',
         error: error
         }
         });*/
    }
}

// Updates the graph structure
function synchronizeStructure(onlyLayout = false) {
    // logger.debug(Updating graph structure');
    
    let timer = new MeasureTime();
    
    PubSub.publish('NODE_SET_FOCUSED', {
        nodePath: '',
        focused: false
    });
    
    PubSub.publish('LINK_SET_FOCUS', {
        linkId: '',
        focused: false
    });
    
    return async function (dispatch, getState) {
        if (!onlyLayout) {
            await updateLinks(dispatch, getState);
        }
        
        await updateLayout(dispatch, getState);
        // await synchronizeStatus(dispatch, getState);
        
        timer.lap('update graph structure');
        
        dispatch({type: types.RUN_SET_MODIFIED});
    }
}

// Fetches nodes from the server
function fetchNodes(runName) {
    // logger.debug('Fetching nodes');
    
    let timer = new MeasureTime();
    
    return new Promise(function (resolve, reject) {
        const initialDepth = 10;
        
        AppApi.fetchNodesByMaxDepth(runName, initialDepth)
            .then(response => {
                let nodes = response.data.nodes;
                
                for (let nodeId in nodes) {
                    let node          = response.data.nodes[nodeId];
                    const isGroupNode = node.children.length > 0;
                    node.expanded     = (node.level < 1) && isGroupNode;
                    node.hubExpanded  = true;
                }
                
                timer.lap('fetch nodes');
                
                resolve(nodes);
            })
            .catch(error => {
                reject(error);
            })
    })
}