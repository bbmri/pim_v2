export default {
    linkReceivedFocus: 'PIM_LINK_RECEIVED_FOCUS',       // Callback event which is invoked when a link received focus
    linkLostFocus: 'PIM_LINK_LOST_FOCUS',               // Callback event which is invoked when a link lost focus
    linkHighlight: 'PIM_LINK_HIGHLIGHT',                // Action to highlight a link
    linkUnhighlight: 'PIM_LINK_UNHIGHLIGHT',            // Action to unhighlight a link
    nodeReceivedFocus: 'PIM_NODE_RECEIVED_FOCUS',       // Callback event which is invoked when a node received focus
    nodeLostFocus: 'PIM_NODE_LOST_FOCUS',               // Callback event which is invoked when a node lost focus
    nodeHighlight: 'PIM_NODE_HIGHLIGHT',                // Action to highlight a node
    nodeUnhighlight: 'PIM_NODE_UNHIGHLIGHT',            // Action to unhighlight a node
    groupExpand: 'PIM_GROUP_EXPAND',                    // Action to expand a compound node
    groupCollapse: 'PIM_GROUP_COLLAPSE',                // Action to collapse a compound node
    revealNode: 'PIM_REVEAL_NODE',                      // Action to reveal a hidden node
    expandAllNodes: 'PIM_EXPAND_ALL_NODES',             // Action to expand all nodes in the graph
    collapseAllNodes: 'PIM_COLLAPSE_ALL_NODES',         // Action to collapse all nodes in the graph
    zoomNodeLinkage: 'PIM_ZOOM_NODE_LINKAGE',           // Action to zoom to the link and is connected nodes
    nodeViewFailedJobs: 'PIM_NODE_VIEW_FAILED_JOBS',    // Action to explore node failed jobs
    nodeViewJobs: 'PIM_NODE_VIEW_JOBS'                  // Action to explore node jobs
};