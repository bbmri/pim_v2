const progressBar = {
    borderRadius: 8
};

export default {
    progressBar,
    leafActorBorderRadius: 30,
    groupActorBorderRadius: 50,
    layerConstants: {
        leafNodeStart: 15,
        groupNodeStart: 7
    }
};