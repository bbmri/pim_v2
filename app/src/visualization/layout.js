import klay from 'klayjs'
import KGraph from './kgraph'
import Group from "./actors/node/group";

export default class Layout {
    /**
     *
     * @param kGraphNode
     * @param layout
     * @param offset
     */
    static flatten(kGraphNode, layout, offset = {x: 0, y: 0}) {
        layout[kGraphNode.id] = {
            position: {
                x: offset.x + kGraphNode.x + (kGraphNode.id === 'root' ? 0 : 0),
                y: offset.y + kGraphNode.y + (kGraphNode.id === 'root' ? 0 : 0)
            },
            size: {
                width: kGraphNode.width,
                height: kGraphNode.height
            },
            inPorts: kGraphNode.inPorts,
            outPorts: kGraphNode.outPorts,
            regions: kGraphNode.regions,
            markup: kGraphNode.markup
        };
        
        layout[kGraphNode.id].regions.node = {
            position: {
                x: 0,
                y: 0
            },
            size: {
                width: kGraphNode.width,
                height: kGraphNode.height
            }
        };
        
        if (kGraphNode.node.children.length > 0 && kGraphNode.node.expanded) {
            let groupLayout = Group.computeLayout(kGraphNode.node, layout[kGraphNode.id].regions.node.size);
            
            layout[kGraphNode.id].regions = groupLayout.regions;
        }
        
        if (kGraphNode.children.length > 0) {
            layout[kGraphNode.id].regions.node = {
                position: {
                    x: 0,
                    y: 0
                },
                size: {
                    width: kGraphNode.width,
                    height: kGraphNode.height
                }
            };
            
            
        }
        
        layout[kGraphNode.id].ports = kGraphNode.ports;
        
        for (let child of kGraphNode.children) {
            Layout.flatten(child, layout, {
                x: layout[kGraphNode.id].position.x + kGraphNode.padding.left,
                y: layout[kGraphNode.id].position.y + kGraphNode.padding.top
            });
        }
    }
    
    /**
     *
     * @param run
     * @returns {Promise<any>}
     */
    static generateGraph(run) {
        return new Promise(function (resolve, reject) {
            try {
                resolve(KGraph.generate(run));
            } catch (error) {
                reject(error);
            }
        });
    }
    
    /**
     *
     * @param kgraph
     * @returns {Promise<any>}
     */
    static computeGraphLayout(kgraph) {
        return new Promise(function (resolve, reject) {
            const layoutOptions = {
                algorithm: 'de.cau.cs.kieler.klay.layered',
                spacing: 400,
                borderSpacing: 100,
                layoutHierarchy: true,
                intCoordinates: true,
                direction: 'RIGHT',
                
                // UNDEFINED, POLYLINE, ORTHOGONAL, SPLINES
                edgeRouting: 'ORTHOGONAL',
                
                // NETWORK_SIMPLEX, LONGEST_PATH, COFFMAN_GRAHAM, INTERACTIVE, STRETCH_WIDTH, MIN_WIDTH
                nodeLayering: 'INTERACTIVE',
                
                // SIMPLE, INTERACTIVE
                nodePlace: 'SIMPLE',
                
                // GREEDY, DEPTH_FIRST, INTERACTIVE
                // cycleBreaking: 'INTERACTIVE'
            };
            
            klay.layout({
                graph: kgraph,
                options: layoutOptions,
                success: (graph) => {
                    graph.x = -graph.width / 2;
                    graph.y = -graph.height / 2;
                    
                    let layout = {root: {x: 0, y: 0, width: 0, height: 0}};
                    
                    Layout.flatten(graph, layout);
                    
                    resolve(layout);
                },
                error: (error) => {
                    resolve(error);
                }
            });
        });
    }
    
    /**
     *
     * @param run
     * @returns {Promise<any>}
     */
    static async compute(run) {
        const graph  = await Layout.generateGraph(run);
        const layout = await Layout.computeGraphLayout(graph);
        
        return layout;
    }
}