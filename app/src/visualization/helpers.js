export function roundedRectanglePath(p0, p1, r) {
    let path = '';
    
    let pts = [p0, p1];
    
    const size = {
        width: p1.x - p0.x,
        height: p1.y - p0.y
    };
    
    // Move to first point
    path += `M ${pts[0].x}, ${pts[0].y + r} `;
    
    // Draw arc clockwise 90 degrees
    path += `a${r}, ${r} 0 0 1 ${r}, ${-r} `;
    
    // Draw horizontal line to the right
    path += `h${size.width - 2 * r} `;
    
    // Draw arc clockwise 90 degrees
    path += `a${r}, ${r} 0 0 1 ${r}, ${r} `;
    
    // Draw vertical line downward
    path += `v${size.height - 2 * r} `;
    
    // Draw arc clockwise 90 degrees
    path += `a${r}, ${r} 0 0 1 ${-r}, ${r} `;
    
    // Draw horizontal line to the left
    path += `h${-(size.width - 2 * r)} `;
    
    // Draw arc clockwise 90 degrees
    path += `a${r}, ${r} 0 0 1 ${-r}, ${-r} `;
    
    path += 'Z';
    
    return path;
}

export function statusBarBars(jobs, size, minimumBarWidth = 20) {
    let bars = {};
    
    const statusTypes = ['idle', 'running', 'success', 'failed', 'cancelled'];
    const noJobsTotal = jobs.reduce((a, b) => a + b, 0);
    
    if (noJobsTotal > 0) {
        
        // Total amount of width that has been padded to meet each bar's minimum bar width
        let paddedWidth = 0;
        
        // Bars to deflate
        let deflateBars = [];
        
        // Compute bar width
        for (let i of _.range(5)) {
            
            // Compute the percentage
            const statusType = statusTypes[i];
            const percentage = jobs[i] / noJobsTotal;
            
            // Compute the width of the bar (proportional to the number of total jobs)
            let width = percentage * size.width;
            
            if (percentage > 0) {
                
                // Ensure the bar remains visible by keeping a minimum bar width
                if (width < minimumBarWidth) {
                    
                    // Compute how much padding needs to be added to meet the minimum bar width
                    const padding = minimumBarWidth - width;
                    
                    // Add to global padding width
                    paddedWidth += padding;
                    
                    // Assign minimum bar width
                    width = minimumBarWidth;
                } else {
                    
                    // This bar is eligible for deflation
                    deflateBars.push(statusType);
                }
            }
            
            bars[statusType] = {
                size: {
                    width: width,
                    height: size.height
                },
                type: statusType
            };
        }
        
        let currentX = 0;
        
        for (let statusType of statusTypes) {
            
            // Possibly deflate bars that are wide enough
            if (paddedWidth > 0 && deflateBars.includes(statusType)) {
                bars[statusType].size.width -= paddedWidth / deflateBars.length;
            }
            
            // Determine the position
            bars[statusType].position = {
                x: currentX,
                y: 0
            };
            
            currentX += bars[statusType].size.width;
        }
    }
    
    else {
        bars.uninitialized = {
            position: {
                x: 0,
                y: 0
            },
            size: {
                width: size.width,
                height: size.height
            },
            type: 'uninitialized'
        };
    }
    
    return bars;
}

export function aggregateStatus(jobs) {
    
    // Determine total number of jobs
    const noJobsTotal = jobs.reduce((a, b) => a + b, 0);
    
    if (noJobsTotal === 0)
        return 'uninitialized';
    
    // Status should be set to failed if one or more jobs have failed
    if (jobs[3] > 0)
        return 'failed';
    
    // Status should be set to cancelled if one or more jobs have been cancelled
    if (jobs[4] > 0)
        return 'cancelled';
    
    if (jobs[0] > 0 && jobs[0] === noJobsTotal)
        return 'idle';
    
    if (jobs[1] > 0 && jobs[1] === noJobsTotal)
        return 'running';
    
    // If all jobs have succeeded the status should be success
    if (jobs[2] > 0 && jobs[2] === noJobsTotal)
        return 'success';
    
     // If one or more jobs are running the aggregate status should be running
     if (jobs[1] > 0)
        return 'running';
    
    return 'uninitialized';
}