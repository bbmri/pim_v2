export default class Group {
    static _createLayout(type, node, content, proxy = true) {
        let markup = '';
        
        markup += `<div class="layout group ${type}">`;
        markup += content(node, proxy);
        markup += `</div>`;
        
        return markup;
    }
    
    static _headerMarkup(node, proxy) {
        const proxyClass = proxy ? 'proxy' : '';
        
        let markup = '';
        
        markup += `<div class="header">`;
        
        
        // Title
        markup += `<div class="label ${proxyClass}">`;
        markup += `<div class="name">${node.title}</div>`;
        
        markup += `<div class="progress">${(node.status.progress * 100.0).toFixed(1)}%</div>`
        
        markup += `</div>`;
        
        // Status
        markup += `<div class="progressbar"></div>`;
        
        markup += `</div>`;
        
        return markup;
    }
    
    static headerLayout(node) {
        return Group._createLayout('header', node, Group._headerMarkup)
    }
}