import {aggregateStatus} from '../helpers'

class MainMarkup {
    static header(node, proxy = true) {
        const proxyClass = proxy ? 'proxy' : '';
        
        let markup = '';
        
        markup += `<div class="header leaf">`;
        markup += `    <div class="title ${proxyClass}">${node.title}</div>`;
        markup += `    <div class="sub ${proxyClass}">${node.type}</div>`;
        markup += `    <div class="progress ${proxyClass} ${aggregateStatus(node.status.jobs)}">${(node.status.progress * 100.0).toFixed(1)}%</div>`;
        markup += `</div>`;
        
        return markup;
    }
    
    static hub(node) {
        let markup = '';
        
        function addPortMarkup(type, name, text, color) {
            let items = [
                `<div class="anchor" id="${name}_anchor" style="background-color: ${color}"></div>`,
                `<div class="text" id="${name}_text">${text}</div>`
            ];
            
            markup += `<div class="port ${type} ">`;
            
            if (type === 'in')
                markup += items[0] + items[1];
            
            if (type === 'out')
                markup += items[1] + items[0];
            
            markup += `</div>`;
        }
        
        function addPortsMarkup(type, ports) {
            markup += `<div class="ports ${type}">`;
            
            if (!node.expanded && node.children.length > 0) {
                const portId = `v_${type}_port`;
                const name   = `hub_${type}_ports_${portId}`;
                
                if (ports[portId] !== undefined)
                    addPortMarkup(type, name, '', 'rgb(200, 200, 200)');
            } else {
                if (node.hubExpanded) {
                    for (let portId in ports) {
                        const name = `hub_${type}_ports_${portId}`;
                        
                        // if (!['v_in_port', 'v_out_port'].includes(portId)) {
                            addPortMarkup(type, name, portId, ports[portId].color);
                        // }
                    }
                } else {
                    const portId = `v_${type}_port`;
                    const name   = `hub_${type}_ports_${portId}`;
                    
                    if (ports[portId] !== undefined)
                        addPortMarkup(type, name, Array(Object.keys(node[`${type}Ports`]).length + 1).join('•'), ports[portId].color);
                }
            }
            
            markup += `</div>`;
        }
        
        const type = !node.expanded && node.children.length > 0 ? 'group' : 'leaf';
        
        markup += `<div class="hub ${type}" style="visibility: ${type === 'group' ? 'hidden' : 'visible'}">`;
        
        addPortsMarkup('in', node.inPorts);
        
        markup += `<div class="gutter"></div>`;
        
        addPortsMarkup('out', node.outPorts);
        
        markup += `</div>`;
        
        return markup;
    }
    
    static status() {
        let markup = '';
        
        markup += `<div class="status">`;
        markup += `    <div class="progressbar"/>`;
        markup += `</div>`;
        
        return markup;
    }
    
    static markup(node, proxy) {
        const proxyClass = proxy ? 'proxy' : '';
        
        let markup = '';
        
        markup += MainMarkup.header(node, proxy);
        markup += MainMarkup.hub(node);
        markup += MainMarkup.status();
        
        return markup;
    }
}

class OverviewMarkup {
    static markup(node, proxy) {
        let markup = '';
        
        let classes = `progress ${aggregateStatus(node.status.jobs)}`;
        
        markup += `<div class="${classes}">${(node.status.progress * 100.0).toFixed(1)}%</div>`;
        
        return markup;
    }
}

export default class Leaf {
    static _createLayout(type, node, content, proxy = true) {
        const id    = `${node.path}_leaf`;
        
        let markup = '';
        
        markup += `<div class="layout leaf ${type}" id="${id}">`;
        markup += `<div class="node">`;
        markup += content(node, proxy);
        markup += `</div>`;
        markup += `</div>`;
        
        return markup;
    }
    
    static mainLayout(node, proxy = false) {
        return Leaf._createLayout('main', node, MainMarkup.markup, proxy);
    }
    
    static overviewLayout(node, proxy = false) {
        return Leaf._createLayout('overview', node, OverviewMarkup.markup, proxy);
    }
}