import _ from 'lodash'

import Leaf from './actors/node/leaf'
import Group from './actors/node/group'
import Layout from "./layout";
import MeasureTime from "../utilities/measureTime";

export default class KGraph {
    static buildNodeTree({node, parent = null, run}) {
        let kGraphNode = {};
        
        const position = node.layout === null ? {x: 0, y: 0} : node.layout.position;
        
        kGraphNode.id       = node.path;//.split('/').join('_');
        kGraphNode.parent   = parent;
        kGraphNode.node     = node;
        kGraphNode.x        = position.x;
        kGraphNode.y        = position.y;
        kGraphNode.width    = 100;
        kGraphNode.height   = 100;
        kGraphNode.ports    = [];
        kGraphNode.children = [];
        kGraphNode.inPorts  = {};
        kGraphNode.outPorts = {};
        kGraphNode.regions  = {};
        kGraphNode.markup   = {
            leaf: '',
            group: ''
        };
        
        // Obtain leaf node size and port positions
        if (node.children.length === 0 || !node.expanded) {
            let layout = Leaf.computeLayout(node);
            
            kGraphNode.width    = layout.regions.node.size.width;
            kGraphNode.height   = layout.regions.node.size.height;
            kGraphNode.inPorts  = layout.inPorts;
            kGraphNode.outPorts = layout.outPorts;
            kGraphNode.regions  = layout.regions;
            kGraphNode.markup   = layout.markup.leaf;
            
            const paddingOverall = 150;
            
            kGraphNode.padding = {
                left: paddingOverall,
                top: paddingOverall + layout.regions.header.size.height,
                right: paddingOverall,
                bottom: paddingOverall
            };
        } else {
            let layout = Group.computeLayout(node);
            
            kGraphNode.padding  = layout.padding;
        }
        
        kGraphNode.ports.push({
            'id': `${kGraphNode.id}/v_in_port`,
            'properties': {
                'portSide': 'WEST'
            }
        });
        
        kGraphNode.ports.push({
            'id': `${kGraphNode.id}/v_out_port`,
            'properties': {
                'portSide': 'EAST'
            }
        });
        
        if (node.expanded && node.children.length > 0) {
            for (let childNodeId of node.children) {
                if (run.nodes[childNodeId] !== undefined) {
                    kGraphNode.children.push(this.buildNodeTree({
                        node: run.nodes[childNodeId],
                        parent: run.nodes[childNodeId],
                        run: run
                    }));
                }
            }
        }
        
        return kGraphNode;
    }
    
    static createEdge(link) {
        const source         = link.fromNode;
        const target         = link.toNode;
        const sourceSegments = source.split('/');
        const targetSegments = target.split('/');
        
        for (let i of _.range(10)) {
            if (sourceSegments[i] !== targetSegments[i]) {
                const containerNode = sourceSegments.slice(0, i).join('/');
                const sourceNode    = sourceSegments.slice(0, i + 1).join('/');
                const targetNode    = targetSegments.slice(0, i + 1).join('/');
                
                return {
                    id: `${sourceNode}/v_out_port::${targetNode}/v_in_port`,
                    containerNode: containerNode,
                    source: sourceNode,
                    target: targetNode,
                    sourcePort: `${sourceNode}/v_out_port`,
                    targetPort: `${targetNode}/v_in_port`
                }
            }
        }
    }
    
    static generate(run) {
        let timer = new MeasureTime();
        
        let kGraph = KGraph.buildNodeTree({
            node: run.nodes.root,
            run: run
        });
        
        timer.lap('build node tree');
        
        // Edges input for ELK
        kGraph.edges = [];
        
        let linkIds = [];
        
        // Fix cross-hierarcy links and remove duplicates
        for (let linkId in run.links) {
            let link = run.links[linkId];
            let edge = KGraph.createEdge(link);
            
            if (!linkIds.includes(edge.id)) {
                kGraph.edges.push(KGraph.createEdge(link));
                linkIds.push(kGraph.edges.slice(-1)[0].id);
            }
        }
        
        timer.lap('fix edges');
        
        return kGraph;
    }
}