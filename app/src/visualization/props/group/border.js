import PubSub from 'pubsub-js'
import * as d3 from 'd3'

import Prop from '../../../mainstage/prop'
import AABB from "../../../mainstage/aabb"
import * as helpers from '../../helpers'
import Config from '../../config'

export default class Border extends Prop {
    constructor(options) {
        options.type = 'group-border';
        
        super(options);
        
        this.focus = options.focus !== undefined ? options.focus : false;
        this.opacity = this.focus ? 0 : 1;
    }
    
    _addToDOM() {
        super._addToDOM();
        
        this.selection.background = this.selection.prop
            .append('path')
            .attr('fill-rule', 'evenodd')
            .attr('class', `background ${this.focus ? 'focused' : ''}`);
        
        this.selection.arrows = this.selection.prop
            .append('path')
            .attr('visibility', this.canCollapse() ? 'visible' : 'hidden')
            .attr('class', `arrows ${this.focus ? 'focused' : ''}`);
        
        this._updateDOM();
    }
    
    _updateDOM(transition = false) {
        super._updateDOM();
        
        this.selection.background
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('d', this.backgroundPath());
        
        this.selection.arrows
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('d', this.arrowsPath());
    }
    
    _shouldUpdateDOM(params) {
        return Prop.notEqualParams(params, 'data.layout.size');
    }
    
    canCollapse() {
        return this.data().level > 0 && this.data().expanded && this.data().children.length > 0;
    }
    
    backgroundPath(width = 45, hit = false) {
        const region = this.data().layout.regions.node;
        
        let pts = [
            {
                x: region.position.x - width,
                y: region.position.y - width
            },
            {
                x: region.position.x + region.size.width + width,
                y: region.position.y + region.size.height + width
            }
        ];
        
        let path = '';
        
        path += helpers.roundedRectanglePath(pts[0], pts[1], Config.groupActorBorderRadius + width);
        
        if (!hit) {
            pts[0].x += width;
            pts[0].y += width;
            
            pts[1].x -= width;
            pts[1].y -= width;
            
            path += helpers.roundedRectanglePath(pts[0], pts[1], Config.groupActorBorderRadius);
        }
        
        return path;
    }
    
    arrowsPath() {
        const magnify = 5.0;
        const offset  = [5 + magnify * 20, 5 + magnify * 7], width = magnify * 12;
        const region  = this.data().layout.regions.node;
        let nodeAABB  = new AABB(region.position, region.size);
        
        nodeAABB.inflate(30);
        
        let path = '';
        
        // Left arrow
        path += `M ${nodeAABB.topLeft.x - offset[0]} ${nodeAABB.center.y - width}`;
        path += `L ${nodeAABB.topLeft.x - offset[1]} ${nodeAABB.center.y}`;
        path += `L ${nodeAABB.topLeft.x - offset[0]} ${nodeAABB.center.y + width}`;
        path += `L ${nodeAABB.topLeft.x - offset[0]} ${nodeAABB.center.y - width}`;
        
        // Right arrow
        path += `M ${nodeAABB.bottomRight.x + offset[0]} ${nodeAABB.center.y - width}`;
        path += `L ${nodeAABB.bottomRight.x + offset[1]} ${nodeAABB.center.y}`;
        path += `L ${nodeAABB.bottomRight.x + offset[0]} ${nodeAABB.center.y + width}`;
        path += `L ${nodeAABB.bottomRight.x + offset[0]} ${nodeAABB.center.y - width}`;
        
        // Top arrow
        path += `M ${nodeAABB.center.x - width} ${nodeAABB.topLeft.y - offset[0]}`;
        path += `L ${nodeAABB.center.x} ${nodeAABB.topLeft.y - offset[1]}`;
        path += `L ${nodeAABB.center.x + width} ${nodeAABB.topLeft.y - offset[0]}`;
        path += `L ${nodeAABB.center.x - width} ${nodeAABB.topLeft.y - offset[0]}`;
        
        // Bottom arrow
        path += `M ${nodeAABB.center.x - width} ${nodeAABB.bottomRight.y + offset[0]}`;
        path += `L ${nodeAABB.center.x} ${nodeAABB.bottomRight.y + offset[1]}`;
        path += `L ${nodeAABB.center.x + width} ${nodeAABB.bottomRight.y + offset[0]}`;
        path += `L ${nodeAABB.center.x - width} ${nodeAABB.bottomRight.y + offset[0]}`;
        
        return path;
    }
}