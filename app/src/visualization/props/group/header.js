import * as d3 from 'd3'

import Prop from '../../../mainstage/prop'
import GroupLayout from '../../layout/group'
import {aggregateStatus} from '../../helpers'

export default class Header extends Prop {
    constructor(options) {
        options.type = 'group-header';
        
        super(options);
        
        this.proxy        = options.proxy === undefined ? false : options.proxy;
        this.borderRadius = 30;
    }
    
    _addToDOM() {
        super._addToDOM();
        
        this.selection.nameBackground = this.selection.prop
            .append('rect')
            .attr('class', 'name background');
        
        this.selection.progressBackground = this.selection.prop
            .append('rect')
            .attr('class', `progress background ${aggregateStatus(this.data().status.jobs)}`);
        
        this.selection.foreignObject = this.selection.prop
            .append('foreignObject')
            .attr('id', this.id);
        
        this.selection.div = this.selection.foreignObject
            .html(this.htmlMarkup())
            .select('div.layout')
            .style('position', 'static')
            .style('visibility', 'visible');
        
        this._updateDOM();
    }
    
    _updateDOM(transition = false) {
        super._updateDOM();
        
        this.selection.progressBackground
            .attr('class', `progress background ${aggregateStatus(this.data().status.jobs)}`);
        
        const nameRegion = this.data().layout.regions.headerName;
        
        this.selection.prop.selectAll('rect.name')
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('x', nameRegion.position.x)
            .attr('y', nameRegion.position.y)
            .attr('width', nameRegion.size.width)
            .attr('height', nameRegion.size.height)
            .attr('rx', this.borderRadius)
            .attr('ry', this.borderRadius);
        
        const progressRegion = this.data().layout.regions.headerProgress;
        
        this.selection.prop.selectAll('rect.progress')
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('x', progressRegion.position.x)
            .attr('y', progressRegion.position.y)
            .attr('width', progressRegion.size.width)
            .attr('height', progressRegion.size.height)
            .attr('rx', this.borderRadius)
            .attr('ry', this.borderRadius);
        
        this.selection.foreignObject
            .html(this.htmlMarkup())
            .select('div.layout')
            .style('position', 'static')
            .style('visibility', 'visible');
        
        this.selection.foreignObject
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('width', this.region().size.width)
            .attr('height', this.region().size.height)
            .on('end', () => {
                this.selection.foreignObject
                    .html(this.htmlMarkup())
                    .select('div.layout')
                    .style('position', 'static')
                    .style('visibility', 'visible');
            });
    }
    
    _shouldUpdateDOM(params) {
        if (Prop.notEqualParams(params, 'data.layout.size'))
            return true;
        
        if (Prop.notEqualParams(params, 'data.status'))
            return true;
        
        return false;
    }
    
    region() {
        return this.data().layout.regions.node;
    }
    
    htmlMarkup() {
        return GroupLayout.headerLayout(this.data(), this.proxy);
    }
}