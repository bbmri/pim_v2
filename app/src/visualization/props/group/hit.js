import PubSub from 'pubsub-js'
import * as d3 from 'd3'

import Prop from '../../../mainstage/prop'
import * as helpers from '../../helpers'
import Config from '../../config'
import PimActions from '../../../actions'

export default class Hit extends Prop {
    constructor(options) {
        options.type = 'group-hit';
        
        super(options);
    }
    
    _addToDOM() {
        super._addToDOM();
        
        this.selection.hit = this.selection.prop
            .append('path')
            .on('mouseover', () => {
                PubSub.publish(PimActions.nodeReceivedFocus, {
                    nodePath: this.data().path
                });
            })
            .on('mouseleave', () => {
                PubSub.publish(PimActions.nodeLostFocus, {
                    nodePath: this.data().path
                });
                
                PubSub.publish('STATUS_UNSET');
            })
            .on('click', () => {
                if (this.canCollapse()){
                    PubSub.publish(PimActions.groupCollapse, this.data().path);
                }
            })
            .attr('class', 'hit');
        
        this._updateDOM();
    }
    
    _updateDOM(transition = false) {
        super._updateDOM();
        
        this.selection.hit
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('d', this.backgroundPath(45, true));
    }
    
    _shouldUpdateDOM(params) {
        return Prop.notEqualParams(params, 'data.layout.size');
    }
    
    canCollapse() {
        return this.data().level > 0 && this.data().expanded && this.data().children.length > 0;
    }
    
    backgroundPath(width = 45, hit = false) {
        const region = this.data().layout.regions.node;
        
        let pts = [
            {
                x: region.position.x - width,
                y: region.position.y - width
            },
            {
                x: region.position.x + region.size.width + width,
                y: region.position.y + region.size.height + width
            }
        ];
        
        let path = '';
        
        path += helpers.roundedRectanglePath(pts[0], pts[1], Config.groupActorBorderRadius + width);
        
        if (!hit) {
            pts[0].x += width;
            pts[0].y += width;
            
            pts[1].x -= width;
            pts[1].y -= width;
            
            path += helpers.roundedRectanglePath(pts[0], pts[1], Config.groupActorBorderRadius);
        }
        
        return path;
    }
}