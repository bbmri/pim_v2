import * as d3 from 'd3'
import PubSub from 'pubsub-js'

import Prop from '../../../mainstage/prop'
import PimActions from '../../../actions'

export default class Hit extends Prop {
    constructor(options) {
        options.type = 'link-hit';
        
        super(options);
    }
    
    _addToDOM(selection) {
        super._addToDOM(selection);
        
        this.selection.border = this.selection.prop
            .append('path')
            .on('mouseover', d => {
                PubSub.publish(PimActions.linkReceivedFocus, {
                    linkId: this.data().id,
                    fromNode: this.data().fromNode,
                    toNode: this.data().toNode,
                    index: 0
                });
            })
            .on('mouseleave', d => {
                PubSub.publish(PimActions.linkLostFocus, {
                    linkId: this.data().id,
                    fromNode: this.data().fromNode,
                    toNode: this.data().toNode,
                    index: 0
                });
            })
            .on('click', d => {
                if (this.data().implicit) {
                    PubSub.publish(PimActions.linkExpand, this.data().id);
                } else {
                    PubSub.publish(PimActions.zoomNodeLinkage, this.data().id);
                }
            })
            .attr('class', 'hit');
        
        this._updateDOM();
    }
    
    _updateDOM(transition = false) {
        super._updateDOM();
        
        this.selection.prop.selectAll('path')
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('d', this.data().geometry.path);
    }
    
    _shouldUpdateDOM(params) {
        const current  = params.current;
        const previous = params.previous;
        
        if (current !== previous) {
            return true;
        }
        
        return false;
    }
}