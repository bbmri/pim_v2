import * as d3 from 'd3'

import Prop from '../../../mainstage/prop'

export default class Border extends Prop {
    constructor(options) {
        options.type = 'link-border';
        
        super(options);
    }
    
    _addToDOM(selection) {
        super._addToDOM(selection);
        
        this.selection.border = this.selection.prop
            .append('path')
            .attr('class', 'border');
        
        this._updateDOM();
    }
    
    _updateDOM(transition = false) {
        super._updateDOM();
        
        this.selection.prop.selectAll('path')
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('d', this.data().geometry.path);
    }
    
    _shouldUpdateDOM(params) {
        const current  = params.current;
        const previous = params.previous;
        
        if (current !== previous) {
            return true;
        }
        
        return false;
    }
}