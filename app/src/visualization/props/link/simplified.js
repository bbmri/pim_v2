import * as d3 from 'd3'

import Prop from '../../../mainstage/prop'

export default class SimplifiedLink extends Prop {
    constructor(options) {
        options.type = 'simplified-link';
        
        super(options);
    }
    
    _addToDOM(selection) {
        super._addToDOM(selection);
        
        this.selection.main = this.selection.prop
            .append('path')
            .attr('class', `main ${this.data().implicit ? 'implicit' : ''}`);
        
        this._updateDOM();
    }
    
    _updateDOM(transition = false) {
        super._updateDOM();
        
        this.selection.prop.selectAll('path')
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('d', this.data().geometry.path);
    }
    
    _shouldUpdateDOM(params) {
        const current  = params.current;
        const previous = params.previous;
        
        if (current !== previous) {
            return true;
        }
        
        return false;
    }
}