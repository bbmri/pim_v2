import * as d3 from 'd3'

import Prop from '../../mainstage/prop'
import Config from '../config'

export default class Transition extends Prop {
    constructor(options) {
        options.type = 'transition';
        
        super(options);
    }
    
    _addToDOM() {
        super._addToDOM();
        
        this.selection.group = this.selection.prop
            .append('g')
            .attr('class', 'opacity')
            .attr('opacity', 0);
        
        this.selection.rect = this.selection.group
            .append('rect')
            .attr('x', this.region().position.x)
            .attr('y', this.region().position.y)
            .attr('width', this.region().size.width)
            .attr('height', this.region().size.height)
            .attr('rx', this.radius())
            .attr('ry', this.radius());
    }
    
    _updateDOM(transition = false) {
        super._updateDOM();
        
        const updateAnimation = this.animation.update;
        
        this.selection.group
            .transition()
            .ease(d3[updateAnimation.ease])
            .duration(updateAnimation.duration * (6 / 8))
            .delay(updateAnimation.delay)
            .attr('opacity', 1)
            .on('end', function () {
                d3.select(this)
                    .transition()
                    .ease(d3[updateAnimation.ease])
                    .duration(updateAnimation.duration * (2 / 8))
                    .delay(updateAnimation.delay)
                    .attr('opacity', 0);
            });
        
        this.selection.rect
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('x', this.region().position.x)
            .attr('y', this.region().position.y)
            .attr('width', this.region().size.width)
            .attr('height', this.region().size.height)
            .attr('rx', this.radius())
            .attr('ry', this.radius());
    }
    
    _shouldUpdateDOM(params) {
        return Prop.notEqualParams(params, 'data.expanded');
    }
    
    radius() {
        return this.data().expanded ? Config.groupActorBorderRadius : Config.leafActorBorderRadius;
    }
    
    region() {
        return this.data().layout.regions.node;
    }
}