import * as d3 from 'd3'

import Prop from '../../mainstage/prop'
import PubSub from "pubsub-js";
import {statusBarBars} from '../helpers'
import Config from '../config'
import PimActions from "../../actions";

export default class Progressbar extends Prop {
    constructor(options) {
        options.type = 'progressbar';
        
        super(options);
    }
    
    _addToDOM() {
        super._addToDOM();
        
        this.selection.border = this.selection.prop
            .append('rect');
        
        this.selection.bars = this.selection.prop
            .append('rect')
            .on('mouseover', d => {
                this.onMouseOver();
            })
            .on('mouseleave', d => {
                this.onMouseLeave();
            })
            .on('click', () => {
                this.onClick();
            });
        
        this.selection.outline = this.selection.prop
            .append('rect');
        
        this.selection.pattern = this.selection.prop
            .append('defs')
            .append('pattern')
            .attr('id', this.data().path)
            .attr('width', 1000)
            .attr('height', 10)
            .attr('patternUnits', 'userSpaceOnUse');
        
        this._updateDOM();
    }
    
    _updateDOM(transition = false) {
        super._updateDOM();
        
        const focusWidth = 20;
        
        this.selection.border
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('x', this.region().position.x - focusWidth)
            .attr('y', this.region().position.y - focusWidth)
            .attr('width', this.region().size.width + (2 * focusWidth))
            .attr('height', this.region().size.height + (2 * focusWidth))
            .attr('rx', Config.progressBar.borderRadius + focusWidth)
            .attr('ry', Config.progressBar.borderRadius + focusWidth)
            .attr('class', 'border');
        
        this.selection.pattern
            .transition()
            .ease(d3['easeLinear'])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('x', this.region().position.x)
            .attr('y', this.region().position.y)
            .attr('width', this.region().size.width);
        
        const bars = statusBarBars(this.data().status.jobs, this.region().size);
        
        let patternData = this.selection.pattern.selectAll('rect')
            .data(d3.entries(bars), function (d) {
                return d.key;
            });
        
        patternData
            .enter()
            .append('rect')
            .attr('x', entry => {
                return entry.value.position.x;
            })
            .attr('y', entry => {
                return entry.value.position.y;
            })
            .attr('width', entry => {
                return entry.value.size.width;
            })
            .attr('height', entry => {
                return entry.value.size.height;
            })
            .attr('class', entry => {
                return `bar ${entry.value.type}`;
            });
        
        patternData
            .exit()
            .remove();
        
        this.selection.pattern.selectAll('rect')
            .transition()
            .ease(d3['easeLinear'])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('x', entry => {
                return entry.value.position.x;
            })
            .attr('y', entry => {
                return entry.value.position.y;
            })
            .attr('width', entry => {
                return entry.value.size.width;
            })
            .attr('height', entry => {
                return entry.value.size.height;
            });
        
        this.selection.bars
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('x', this.region().position.x)
            .attr('y', this.region().position.y)
            .attr('width', this.region().size.width)
            .attr('height', this.region().size.height)
            .attr('rx', Config.progressBar.borderRadius)
            .attr('ry', Config.progressBar.borderRadius)
            .attr('fill', `url(#${this.data().path})`);
        
        this.selection.outline
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('x', this.region().position.x)
            .attr('y', this.region().position.y)
            .attr('width', this.region().size.width)
            .attr('height', this.region().size.height)
            .attr('rx', Config.progressBar.borderRadius)
            .attr('ry', Config.progressBar.borderRadius)
            .attr('class', 'outline');
    }
    
    _shouldUpdateDOM(params) {
        if (Prop.notEqualParams(params, 'data.layout.size'))
            return true;
        
        if (Prop.notEqualParams(params, 'data.status'))
            return true;
        
        return false;
    }
    
    onMouseOver() {
        this.selection.border
            .attr('class', 'border focused');
        
        PubSub.publish('STATUS_SET', `${this.data().title} jobs (Click to inspect)`);
    }
    
    onMouseLeave() {
        this.selection.border
            .attr('class', 'border');
        
        PubSub.publish('STATUS_UNSET');
    }
    
    onClick() {
        PubSub.publish(PimActions.nodeViewJobs, this.data().path)
    }
    
    region() {
        return this.data().layout.regions.progressbar;
    }
}