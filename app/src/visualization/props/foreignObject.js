import * as d3 from 'd3'

import Prop from '../../mainstage/prop'

export default class ForeignObject extends Prop {
    constructor(options) {
        options.type = 'foreignObject';
        
        super(options);
        
        this.createMarkup = options.createLayout;
        this.updateMarkup = options.updateMarkup;
        this.attachEvents = options.attachEvents;
    }
    
    _addToDOM() {
        super._addToDOM();
        
        this.aabb.setPosition(this.data.position);
        this.aabb.setSize(this.data.size);
        
        this.selection.foreignObject = this.selection.prop
            .append('foreignObject')
            .attr('width', this.data.size.width)
            .attr('height', this.data.size.height)
            .attr('pointer-events', 'none')
            .attr('id', this.id);
        
        this.selection.div = this.selection.foreignObject
            .html(this.createMarkup(this));
        
        this.selection.div.select('div')
            .style('position', 'static')
            .style('visibility', 'visible');
        
        if (this.attachEvents !== undefined)
            this.attachEvents(this);
    }
    
    _updateDOM() {
        this.selection.foreignObject
            .attr('opacity', 0);
        
        this.selection.foreignObject.html(this.updateMarkup(this));
        
        this.selection.foreignObject
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(this.animation.update.duration)
            .delay(this.animation.update.duration)
            .attr('opacity', 1);
    }
}