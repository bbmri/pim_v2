import PubSub from 'pubsub-js'
import * as d3 from 'd3'

import Prop from '../../../mainstage/prop'
import AABB from "../../../mainstage/aabb"
import MainStageActions from "../../../mainstage/actions"
import * as helpers from '../../helpers'
import Config from '../../config'
import PimActions from '../../../actions'

export default class Border extends Prop {
    constructor(options) {
        options.type = 'leaf-border';
        
        super(options);
        
        this.focus   = options.focus !== undefined ? options.focus : false;
        this.opacity = this.focus ? 0 : 1;
    }
    
    _addToDOM() {
        super._addToDOM();
        
        this.selection.background = this.selection.prop
            .append('path')
            // .attr('visibility', this.canExpand() ? 'visible' : 'hidden')
            .on('mouseover', () => {
                PubSub.publish(PimActions.nodeReceivedFocus, {
                    nodePath: this.data().path
                });
            })
            .on('mouseleave', () => {
                PubSub.publish(PimActions.nodeLostFocus, {
                    nodePath: this.data().path
                });
            })
            .on('click', () => {
                if (this.canExpand()) {
                    PubSub.publish(PimActions.groupExpand, this.data().path);
                } else {
                    const layout = this.data().layout;
                    
                    PubSub.publish(MainStageActions.setZoomRegion, {
                        zoomBox: {
                            position: layout.position,
                            size: layout.size
                        }
                    });
                }
            })
            .attr('class', `background ${this.focus ? 'focused' : ''}`);
        
        this.selection.arrows = this.selection.prop
            .append('path')
            .attr('visibility', this.canExpand() ? 'visible' : 'hidden')
            .attr('class', `arrows ${this.focus ? 'focused' : ''}`);
        
        this._updateDOM();
    }
    
    _updateDOM(transition = false) {
        super._updateDOM();
        
        this.selection.background
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            
            .attr('d', this.backgroundPath());
        
        this.selection.arrows
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('d', this.arrowsPath());
    }
    
    _shouldUpdateDOM(params) {
        return Prop.notEqualParams(params, 'data.layout.size');
    }
    
    canExpand() {
        return !this.data().expanded && this.data().children.length > 0;
    }
    
    backgroundPath(width = 40) {
        const region = this.data().layout.regions.node;
        
        let pts = [
            {
                x: region.position.x - width,
                y: region.position.y - width
            },
            {
                x: region.position.x + region.size.width + width,
                y: region.position.y + region.size.height + width
            }
        ];
        
        let path = '';
        
        path += helpers.roundedRectanglePath(pts[0], pts[1], 10 + width);
        
        return path;
    }
    
    arrowsPath() {
        const magnify = 5.0;
        const offset  = [35 + magnify * 7, 35 + magnify * 20], width = magnify * 12;
        const region  = this.data().layout.regions.node;
        const bb      = new AABB(region.position, region.size);
        
        let path = '';
        
        // Left arrow
        path += `M ${bb.topLeft.x - offset[0]} ${bb.center.y - width}`;
        path += `L ${bb.topLeft.x - offset[1]} ${bb.center.y}`;
        path += `L ${bb.topLeft.x - offset[0]} ${bb.center.y + width}`;
        path += `L ${bb.topLeft.x - offset[0]} ${bb.center.y - width}`;
        
        // Right arrow
        path += `M ${bb.bottomRight.x + offset[0]} ${bb.center.y - width}`;
        path += `L ${bb.bottomRight.x + offset[1]} ${bb.center.y}`;
        path += `L ${bb.bottomRight.x + offset[0]} ${bb.center.y + width}`;
        path += `L ${bb.bottomRight.x + offset[0]} ${bb.center.y - width}`;
        
        // Top arrow
        path += `M ${bb.center.x - width} ${bb.topLeft.y - offset[0]}`;
        path += `L ${bb.center.x} ${bb.topLeft.y - offset[1]}`;
        path += `L ${bb.center.x + width} ${bb.topLeft.y - offset[0]}`;
        path += `L ${bb.center.x - width} ${bb.topLeft.y - offset[0]}`;
        
        // Bottom arrow
        path += `M ${bb.center.x - width} ${bb.bottomRight.y + offset[0]}`;
        path += `L ${bb.center.x} ${bb.bottomRight.y + offset[1]}`;
        path += `L ${bb.center.x + width} ${bb.bottomRight.y + offset[0]}`;
        path += `L ${bb.center.x - width} ${bb.bottomRight.y + offset[0]}`;
        
        return path;
    }
    
    radius() {
        return this.data().expanded ? Config.groupActorBorderRadius : Config.leafActorBorderRadius;
    }
}