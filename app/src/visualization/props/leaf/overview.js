import * as d3 from 'd3'

import Prop from '../../../mainstage/prop'
import LeafLayout from "../../layout/leaf";

export default class Overview extends Prop {
    constructor(options) {
        options.type = 'leaf-overview';
        
        super(options);
    }
    
    _addToDOM() {
        super._addToDOM();
        
        this.selection.foreignObject = this.selection.prop
            .append('foreignObject')
            .attr('pointer-events', 'none')
            .attr('id', this.id);
        
        this.selection.div = this.selection.foreignObject
            .html(this.htmlMarkup());
        
        this.selection.div.select('div.layout')
            .style('position', 'static')
            .style('visibility', 'visible');
        
        this._updateDOM();
    }
    
    _updateDOM(transition = false) {
        super._updateDOM();
        
        this.selection.foreignObject
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('width', this.region().size.width)
            .attr('height', this.region().size.height)
            .on('end', () => {
                this.selection.foreignObject.html(this.htmlMarkup());
                
                this.selection.div.select('div.layout')
                    .style('position', 'static')
                    .style('visibility', 'visible');
            })
    }
    
    _shouldUpdateDOM(params) {
        if (Prop.notEqualParams(params, 'data.layout.size'))
            return true;
        
        if (Prop.notEqualParams(params, 'data.status'))
            return true;
        
        return false;
    }
    
    region() {
        return this.data().layout.regions.node;
    }
    
    htmlMarkup() {
        return LeafLayout.overviewLayout(this.data(), this.proxy);
    }
}