import * as d3 from 'd3'

import Prop from '../../../mainstage/prop'
import LeafLayout from '../../layout/leaf'
import PubSub from "pubsub-js";

export default class Hub extends Prop {
    constructor(options) {
        options.type = 'leaf-hub';
        
        super(options)
    }
    
    _addToDOM() {
        super._addToDOM();
        
        if (this.canExpandCollapse()) {
            this.selection.border = this.selection.prop
                .append('rect');
            
            this.selection.background = this.selection.prop
                .append('rect')
                .on('mouseover', d => {
                    this.onMouseOver();
                })
                .on('mouseleave', () => {
                    this.onMouseLeave();
                })
                .attr('class', 'background');
        }
        
        this._updateDOM();
    }
    
    _updateDOM(transition = false) {
        super._updateDOM();
        
        if (this.canExpandCollapse()) {
            let borderWidth = 20;
            
            this.selection.border
                .transition()
                .ease(d3[this.animation.update.ease])
                .duration(transition ? this.animation.update.duration : 0)
                .delay(transition ? this.animation.update.delay : 0)
                .attr('x', this.region().position.x)
                .attr('y', this.region().position.y - borderWidth)
                .attr('width', this.region().size.width)
                .attr('height', this.region().size.height + (2 * borderWidth))
                .attr('class', 'border');
            
            this.selection.background
                .transition()
                .ease(d3[this.animation.update.ease])
                .duration(transition ? this.animation.update.duration : 0)
                .delay(transition ? this.animation.update.delay : 0)
                .attr('x', this.region().position.x)
                .attr('y', this.region().position.y)
                .attr('width', this.region().size.width)
                .attr('height', this.region().size.height);
        }
    }
    
    _shouldUpdateDOM(params) {
        return Prop.notEqualParams(params, 'data.layout.size');
    }
    
    onMouseOver() {
        if (this.canExpandCollapse()) {
            this.selection.border
                .attr('class', 'border focused');
            
            PubSub.publish('STATUS_SET', `Click to ${this.data().hubExpanded ? 'collapse' : 'expand'} the port hub`);
        }
    }
    
    onMouseLeave() {
        if (this.canExpandCollapse()) {
            this.selection.border
                .attr('class', 'border');
            
            PubSub.publish('STATUS_UNSET');
        }
    }
    
    region() {
        return this.data().layout.regions.hub;
    }
    
    canExpandCollapse() {
        return !this.data().expanded && this.data().children.length === 0;
    }
}