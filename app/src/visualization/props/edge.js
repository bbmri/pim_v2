import * as d3 from 'd3'

import Prop from '../../mainstage/prop'
import Config from '../config'

export default class Edge extends Prop {
    constructor(options) {
        options.type = 'edge';
        
        super(options);
    }
    
    _addToDOM() {
        super._addToDOM();
        
        this.selection.rectangle = this.selection.prop
            .append('rect')
            .attr('rx', this.radius())
            .attr('ry', this.radius());
        
        this._updateDOM();
    }
    
    _updateDOM(transition = false) {
        super._updateDOM();
        
        const region = this.data().layout.regions.node;
        
        let classes = [this.data().expanded ? 'group' : 'leaf'];
        
        if (this.hasFailedJobs()) {
            classes.push('failed');
        }
        
        this.selection.rectangle
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('x', region.position.x)
            .attr('y', region.position.y)
            .attr('width', region.size.width)
            .attr('height', region.size.height)
            .attr('class', classes.join(' '));
    }
    
    _shouldUpdateDOM(params) {
        return Prop.notEqualParams(params, 'data.layout.size');
    }
    
    radius() {
        return this.data().expanded ? Config.groupActorBorderRadius : Config.leafActorBorderRadius;
    }
    
    hasFailedJobs() {
        return this.data().status.jobs[3] > 0;
    }
}