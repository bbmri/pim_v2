import * as d3 from 'd3'

import Prop from '../../mainstage/prop'
import Config from '../config'

export default class Background extends Prop {
    constructor(options) {
        options.type = 'background';
        
        super(options);
    }
    
    _addToDOM() {
        super._addToDOM();
        
        this.selection.rectangle = this.selection.prop
            .append('rect')
            .on('click', () => {
                if (this.callbacks.click !== undefined)
                    this.callbacks.click(this);
            });
        
        this._updateDOM();
    }
    
    _updateDOM(transition = false) {
        super._updateDOM();
        
        const node   = this.data();
        const region = node.layout.regions.node;
        
        this.selection.rectangle
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('x', region.position.x)
            .attr('y', region.position.y)
            .attr('width', region.size.width)
            .attr('height', region.size.height)
            .attr('rx', this.radius())
            .attr('ry', this.radius())
            .attr('class', `background ${this.nodeType()}`);
    }
    
    _shouldUpdateDOM(params) {
        return Prop.notEqualParams(params, 'data.layout.size');
    }
    
    nodeType() {
        return this.data().children.length > 0 ? (this.data().expanded ? 'expanded-group' : 'collapsed-group') : 'leaf';
    }
    
    radius() {
        return this.data().expanded ? Config.groupActorBorderRadius : Config.leafActorBorderRadius;
    }
}