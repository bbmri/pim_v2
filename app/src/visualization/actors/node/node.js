import Actor from '../../../mainstage/actor'
import PubSub from "pubsub-js";
import PimActions from './../../../actions'
import MainStageActions from './../../../mainstage/actions'

export default class Node extends Actor {
    constructor(options) {
        super(options);
    }
    
    /**
     * Sets the actor data
     *
     * @param {data} params - New actor data (immutable map)
     *
     */
    setData(data) {
        this.params.current.data = data;
        
        this.params.current.position = {
            x: this.data().layout.position.x,
            y: this.data().layout.position.y
        };
        
        this.params.current.size = {
            width: this.data().layout.size.width,
            height: this.data().layout.size.height
        };
        
        let visible = false;
        
        if (this.type === 'leaf' && this.data().active && !this.data().expanded)
            visible = true;
        
        if (this.type === 'group' && this.data().active && this.data().expanded)
            visible = true;
        
        if (this.type === 'transition')
            visible = true;
        
        const visibleOnCanvas = this.onStage();
        
        this.params.current.visible = visible;
        this.params.current.onStage = visibleOnCanvas;
        this.params.current.inDOM   = visible && visibleOnCanvas;
        
        if (this.data().level > 0) {
            this.animation.show.duration = this.animation.update.duration / 4;
            this.animation.show.delay    = this.animation.update.duration + ((this.data().level - 1) * (this.animation.update.duration / 4));
        } else {
            this.animation.show.duration = this.animation.update.duration;
            this.animation.show.delay    = this.animation.update.delay;
        }
        
        
        if (this.type === 'transition')
            // this.animation.show.duration = this.animation.update.duration;
            this.animation.show.delay = 0;
        
        if (this.params.previous === null) {
            // this.animation.show.duration /= 2;
            
            this._createProps();
        }
        
        super._commitParamChanges();
    }
    
    _createProps() {
        super._createProps();
        
        PubSub.subscribe(PimActions.nodeReceivedFocus, this.onNodeReceivedFocus.bind(this));
        PubSub.subscribe(PimActions.nodeLostFocus, this.onNodeLostFocus.bind(this));
    }
    
    onNodeReceivedFocus(message, data) {
        if (data.nodePath === this.node().path) {
            if (this.node().children.length > 0) {
                PubSub.publish('STATUS_SET', `Click to ${this.node().expanded ? 'collapse' : 'expand'} ${this.node().title}`);
            } else {
                PubSub.publish('STATUS_SET', `Click to zoom to ${this.node().title}`);
            }
        }
    }
    
    onNodeLostFocus(message, data) {
        PubSub.publish('STATUS_UNSET');
    }
    
    node() {
        return this.data();
    }
}