import Node from './node'
import Interval from '../../../mainstage/interval'
import TransitionProp from '../../props/transition'

export default class Transition extends Node {
    constructor(options) {
        options.type  = 'transition';
        
        super(options);
    }
    
    _createProps() {
        super._createProps();
        
        new TransitionProp({
            actor: this,
            id: 'transition',
            z: 18,
            intervals: [new Interval(0.0, 1000.0, 1)]
        });
    }
}
