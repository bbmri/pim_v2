import Node from './node'
import GroupProp from '../../layout/group'
import Interval from '../../../mainstage/interval'
import Background from '../../props/background'
import Border from '../../props/group/border'
import Header from '../../props/group/header'
import Hit from '../../props/group/hit'
import Edge from '../../props/edge'
import Progressbar from '../../props/progressbar'
import Config from '../../config'
import MainStageActions from './../../../mainstage/actions'
import * as d3 from 'd3'
import PubSub from "pubsub-js";

export default class Group extends Node {
    constructor(options) {
        options.type = 'group';
        
        super(options);
    }
    
    _createProps() {
        super._createProps();
        
        new Background({
            actor: this,
            id: 'background',
            z: this.data().level,
            intervals: [new Interval(0.0, 100.1)]
        });
        
        new Border({
            actor: this,
            id: 'border',
            z: Config.layerConstants.groupNodeStart - 3,
            intervals: [new Interval(0.0, 100.0)]
        });
        
        new Border({
            actor: this,
            id: 'focus',
            z: Config.layerConstants.groupNodeStart - 2,
            intervals: [new Interval(0.0, 100.0)],
            focus: true
        });
        
        new Edge({
            actor: this,
            id: 'edge',
            z: Config.layerConstants.groupNodeStart + 4,
            intervals: [new Interval(0.0, 100.0)]
        });
        
        new Header({
            actor: this,
            id: 'header',
            z: Config.layerConstants.groupNodeStart + 2,
            intervals: [new Interval(0.08, 1000.0)]
        });
        
        new Header({
            actor: this,
            id: 'header_proxy',
            z: Config.layerConstants.groupNodeStart + 2,
            intervals: [new Interval(0.0, 0.08)],
            proxy: true
        });
        
        new Hit({
            actor: this,
            id: 'hit',
            z: this.data().level,
            intervals: [new Interval(0.0, 100.0)]
        });
        
        PubSub.subscribe(MainStageActions.nodeReceivedFocus, this.onNodeReceivedFocus.bind(this));
        PubSub.subscribe(MainStageActions.nodeLostFocus, this.onNodeLostFocus.bind(this));
    }
    
    onNodeReceivedFocus(message, data) {
        if (this.data().path === 'root')
            return;
        
        if (data.nodePath === this.data().path) {
            this.props.focus.setOpacity(1);
        }
    }
    
    onNodeLostFocus(message, data) {
        if (this.data().path === 'root')
            return;
        
        if (data.nodePath === '' || data.nodePath === this.data().path) {
            this.props.focus.setOpacity(0);
        }
    }
    
    static computeLayout(node, size = {width: 100, height: 100}) {
        let layout = {
            inPorts: {},
            outPorts: {},
            regions: {},
            padding: {},
            markup: {
                group: GroupProp.headerLayout(node, false)
            }
        };
        
        // if (node.layout.markup.group === layout.markup.group) {
        //     return node.layout;
        
        let nodeGroup = d3.select('body')
            .append('div')
            .html(layout.markup.group);
        
        const layoutDiv = nodeGroup.select('div.layout');
        
        nodeGroup
            .style('width', size.width)
            .style('height', size.height);
        
        function regionFromNode(node) {
            return {
                position: {
                    x: node.offsetLeft,
                    y: node.offsetTop - nodeGroup.node().offsetTop,
                },
                size: {
                    width: node.offsetWidth,
                    height: node.offsetHeight
                }
            }
        }
        
        layout.regions.header         = regionFromNode(layoutDiv.select('div.header').node());
        layout.regions.headerName     = regionFromNode(layoutDiv.select('div.header').select('div.name').node());
        layout.regions.headerProgress = regionFromNode(layoutDiv.select('div.header').select('div.progress').node());
        
        const paddingOverall = 150;
        
        layout.padding = {
            left: paddingOverall,
            top: paddingOverall + layout.regions.header.size.height,
            right: paddingOverall,
            bottom: paddingOverall
        };
        
        nodeGroup.remove();
        
        this.layout = JSON.parse(JSON.stringify(layout));
        
        return layout;
    }
}
