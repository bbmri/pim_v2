import PubSub from 'pubsub-js'
import * as d3 from 'd3'

import Interval from '../../../mainstage/interval'
import LeafLayout from '../../layout/leaf'

import * as  settings from '../../../mainstage/settings'

import Node from './node'
import Background from '../../props/background'
import Border from '../../props/leaf/border'
import Header from '../../props/leaf/header'
import Hub from '../../props/leaf/hub'
import Progressbar from '../../props/progressbar'
import Edge from '../../props/edge'
import Overview from '../../props/leaf/overview'
import Config from '../../config'
import PimActions from '../../../actions'

export default class Leaf extends Node {
    constructor(options) {
        options.type = 'leaf';
        
        super(options);
        
        this.layout = null;
    }
    
    _createProps() {
        super._createProps();
        
        new Background({
            actor: this,
            id: 'background',
            z: Config.layerConstants.leafNodeStart + 2,
            intervals: [new Interval(0.0, 100.0)],
            callbacks: {
                mouseOver: this.onBackgroundMouseOver.bind(this),
                mouseLeave: this.onBackgroundMouseLeave.bind(this),
                click: this.onBackgroundClick.bind(this)
            }
        });
        
        new Edge({
            actor: this,
            id: 'edge',
            z: Config.layerConstants.leafNodeStart + 4,
            intervals: [new Interval(0.0, 100.0, 1)]
        });
        
        new Border({
            actor: this,
            id: 'border',
            z: Config.layerConstants.leafNodeStart,
            intervals: [new Interval(0.0, 100.0)]
        });
        
        new Border({
            actor: this,
            id: 'focus',
            z: Config.layerConstants.leafNodeStart + 1,
            intervals: [new Interval(0.0, 100.0)],
            focus: true
        });
        
        new Header({
            actor: this,
            id: 'header',
            z: Config.layerConstants.leafNodeStart + 3,
            intervals: [new Interval(0.09, 100.0)]
        });
        
        /*
        new Header({
            actor: this,
            id: 'header_proxy',
            z: Config.layerConstants.leafNodeStart + 3,
            intervals: [new Interval(0.09, 0.12)],
            proxy: true
        });
    
        new Hub({
            actor: this,
            id: 'hub',
            z: Config.layerConstants.leafNodeStart + 2,
            intervals: [new Interval(0.1, 1000.0)]
        });*/
        
        new Progressbar({
            actor: this,
            id: 'progressbar',
            z: Config.layerConstants.leafNodeStart + 2,
            intervals: [new Interval(0.09, 100.0)]
        });
        
        new Overview({
            actor: this,
            id: 'overview',
            z: Config.layerConstants.leafNodeStart + 4,
            intervals: [new Interval(0.0, 0.09)]
        });
        
        PubSub.subscribe(PimActions.linkReceivedFocus, this.onLinkReceivedFocus.bind(this));
        PubSub.subscribe(PimActions.linkLostFocus, this.onLinkLostFocus.bind(this));
        PubSub.subscribe(PimActions.nodeReceivedFocus, this.onNodeReceivedFocus.bind(this));
        PubSub.subscribe(PimActions.nodeLostFocus, this.onNodeLostFocus.bind(this));
        PubSub.subscribe(PimActions.nodeHighlight, this.onNodeHighlight.bind(this));
        PubSub.subscribe(PimActions.nodeUnhighlight, this.onNodeUnhighlight.bind(this));
    }
    
    onNodeReceivedFocus(message, data) {
        if (data.nodePath === this.data().path) {
            this.props.focus.setOpacity(1);
        }
    }
    
    onNodeLostFocus(message, data) {
        if (data.nodePath === '' || data.nodePath === this.data().path) {
            this.props.focus.setOpacity(0);
        }
    }
    
    onNodeHighlight(message, data) {
        if (this.node().path !== data)
            return;
        
        this.props.focus.setOpacity(1);
    }
    
    onNodeUnhighlight(message, data) {
        if (this.node().path !== data)
            return;
        
        this.props.focus.setOpacity(0);
    }
    
    onLinkReceivedFocus(message, data) {
        /*if (data.fromNode === this.data().path || data.toNode === this.data().path) {
            this.selection.background
                .attr('class', 'background focused');
        }*/
    }
    
    onLinkLostFocus(message, data) {
        /*if (data.fromNode === this.data().path || data.toNode === this.data().path) {
            this.selection.background
                .attr('class', 'background');
        }*/
    }
    
    static computeLayout(node) {
        let layout = {
            inPorts: {},
            outPorts: {},
            regions: {},
            markup: {
                leaf: LeafLayout.mainLayout(node)
            }
        };
        
        if (node.layout.markup.leaf === layout.markup.leaf) {
            return node.layout;
        }
        
        let nodeLeaf = d3.select('body')
            .append('div')
            .html(layout.markup.leaf);
        
        const offset = 5;
        
        // Obtain input port positions
        for (let inPortId in node.inPorts) {
            const name   = `hub_in_ports_${node.hubExpanded ? inPortId : 'v_in_port'}_anchor`;
            const anchor = nodeLeaf.select(`#${name}`).node();
            
            layout.inPorts[inPortId] = {
                x: (anchor.offsetLeft + anchor.offsetWidth / 2) - offset,
                y: anchor.offsetTop + anchor.offsetHeight / 2
            }
        }
        
        // Obtain output port positions
        for (let outPortId in node.outPorts) {
            const name   = `hub_out_ports_${node.hubExpanded ? outPortId : 'v_out_port'}_anchor`;
            const anchor = nodeLeaf.select(`#${name}`).node();
            
            layout.outPorts[outPortId] = {
                x: (anchor.offsetLeft + anchor.offsetWidth / 2) + offset,
                y: anchor.offsetTop + anchor.offsetHeight / 2
            }
        }
        
        function regionFromNode(node) {
            return {
                position: {
                    x: node.offsetLeft,
                    y: node.offsetTop,
                },
                size: {
                    width: node.offsetWidth,
                    height: node.offsetHeight
                }
            }
        }
        
        layout.regions.node        = regionFromNode(nodeLeaf.select('div.node').node());
        layout.regions.header      = regionFromNode(nodeLeaf.select('div.header').node());
        layout.regions.hub         = regionFromNode(nodeLeaf.select('div.hub').node());
        layout.regions.status      = regionFromNode(nodeLeaf.select('div.status').node());
        layout.regions.progressbar = regionFromNode(nodeLeaf.select('div.status').select('div.progressbar').node());
        
        nodeLeaf
            .remove();
        
        this.layout = JSON.parse(JSON.stringify(layout));
        
        return layout;
    }
    
    onBackgroundMouseOver(prop) {
        this.props['border'].setFocus();
        
        const node = this.data;
        
        // PubSub.publish('STATUS_SET', '');
    }
    
    onBackgroundMouseLeave(prop) {
        this.props['border'].setFocus(false);
        
        // PubSub.publish('STATUS_UNSET');
    }
    
    onBackgroundClick(prop) {
        // PubSub.publish('NODE_SELECT_TOGGLE', this.data.path);
    }
}