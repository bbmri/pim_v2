import Actor from '../../mainstage/actor'
import Interval from '../../mainstage/interval'
import ImplicitLinkProp from './../props/link/implicit'
import ExplicitLinkProp from './../props/link/explicit'
import SimplifiedLinkProp from './../props/link/simplified'
import Border from './../props/link/border'
import Hit from './../props/link/hit'
import Focus from './../props/link/focus'
import Config from '../config'
import PimActions from '../../actions'
import PubSub from "pubsub-js";

export default class Link extends Actor {
    constructor(options) {
        options.type = 'link';
        
        super(options);
    }
    
    /**
     * Sets the actor data
     *
     * @param {data} params - New actor data (immutable map)
     *
     */
    setData(data) {
        this.params.current.data    = data;
        this.params.current.visible = true;
        
        this.params.current.position = {
            x: this.link().fromAnchor.x,
            y: this.link().fromAnchor.y
        };
        
        this.params.current.size = {
            width: this.link().toAnchor.x - this.link().fromAnchor.x,
            height: this.link().toAnchor.y - this.link().fromAnchor.y
        };
        
        this.params.current.data.geometry = this.computeGeometry();
        
        const visible         = true;
        const visibleOnCanvas = this.onStage();
        
        this.params.current.visible = visible;
        this.params.current.onStage = visibleOnCanvas;
        this.params.current.inDOM   = visible && visibleOnCanvas;
        
        if (this.params.previous === null) {
            this._createProps();
        }
        
        super._commitParamChanges();
    }
    
    _createProps() {
        super._createProps();
        
        const maxDepth = Math.max(this.link().fromNode.split('/').length, this.link().toNode.split('/').length);
        const fadeAt   = 0.35;
        const z        = 2;//maxDepth - 1;
        
        if (this.link().implicit) {
            new ImplicitLinkProp({
                actor: this,
                id: 'implicit',
                z: z,
                intervals: [new Interval(0.15, fadeAt)]
            });
            
            new ImplicitLinkProp({
                actor: this,
                id: 'implicit-fade',
                z: z,
                intervals: [new Interval(fadeAt, 100.0, 0.1)]
            });
        } else {
            new ExplicitLinkProp({
                actor: this,
                id: 'explicit',
                z: z,
                intervals: [new Interval(0.15, fadeAt)]
            });
            
            new ExplicitLinkProp({
                actor: this,
                id: 'explicit-fade',
                z: z,
                intervals: [new Interval(fadeAt, 100.0, 0.1)]
            });
        }
        
        new SimplifiedLinkProp({
            actor: this,
            id: 'simplified',
            z: z,
            intervals: [new Interval(0.0, 0.15)]
        });
        
        new Border({
            actor: this,
            id: 'border',
            z: z - 2,
            intervals: [new Interval(0.0, 100.0)]
        });
        
        new Hit({
            actor: this,
            id: 'hit',
            z: Config.layerConstants.leafNodeStart - 1,
            intervals: [new Interval(0.0, 100.0)]
        });
        
        new Focus({
            actor: this,
            id: 'focus',
            z: z - 1,
            intervals: [new Interval(0.0, 100.0)]
        });
        
        this.animation.show.duration = this.animation.update.duration / 3;
        this.animation.show.delay    = this.animation.update.duration + (maxDepth * (this.animation.update.duration / 5));
        
        PubSub.subscribe(PimActions.linkReceivedFocus, this.onLinkReceivedFocus.bind(this));
        PubSub.subscribe(PimActions.linkLostFocus, this.onLinkLostFocus.bind(this));
        PubSub.subscribe(PimActions.nodeReceivedFocus, this.onNodeReceivedFocus.bind(this));
        PubSub.subscribe(PimActions.nodeLostFocus, this.onNodeLostFocus.bind(this));
    }
    
    onLinkReceivedFocus(message, data) {
        if (data.linkId === this.link().id) {
            this.props.focus.setOpacity(1);
            
            if (this.link().implicit) {
                PubSub.publish('STATUS_SET', `Click to expand implicit link`)
            } else {
                const fromNode = this.link().fromNode.split('/').slice(-1)[0];
                const toNode   = this.link().toNode.split('/').slice(-1)[0];
                
                PubSub.publish('STATUS_SET', `${fromNode} > ${toNode} [${this.link().dataType}]`);
            }
            
            PubSub.publish(PimActions.nodeHighlight, this.link().fromNode);
            PubSub.publish(PimActions.nodeHighlight, this.link().toNode);
        }
    }
    
    onLinkLostFocus(message, data) {
        if (data.linkId === '' || data.linkId === this.link().id) {
            this.props.focus.setOpacity(0);
            PubSub.publish('STATUS_UNSET');
            
            PubSub.publish(PimActions.nodeUnhighlight, this.link().fromNode);
            PubSub.publish(PimActions.nodeUnhighlight, this.link().toNode);
        }
    }
    
    onNodeReceivedFocus(message, data) {
        if (data.nodePath === this.link().fromNode || data.nodePath === this.link().toNode) {
            this.props.focus.setOpacity(1);
            
            PubSub.publish(PimActions.nodeHighlight, this.link().fromNode);
            PubSub.publish(PimActions.nodeHighlight, this.link().toNode);
        }
    }
    
    onNodeLostFocus(message, data) {
        if (data.nodePath === this.link().fromNode || data.nodePath === this.link().toNode) {
            this.props.focus.setOpacity(0);
            
            PubSub.publish(PimActions.nodeUnhighlight, this.link().fromNode);
            PubSub.publish(PimActions.nodeUnhighlight, this.link().toNode);
        }
    }
    
    computeGeometry() {
        let dockPoints = [this.link().fromAnchor, this.link().toAnchor];
        
        const position = this.position();
        
        this.link().fromAnchor.x -= position.x;
        this.link().fromAnchor.y -= position.y;
        this.link().toAnchor.x -= position.x;
        this.link().toAnchor.y -= position.y;
        
        if (dockPoints[0].x > dockPoints[1].x) {
            const tempDockPoint = dockPoints[0];
            
            dockPoints[0] = dockPoints[1];
            dockPoints[1] = tempDockPoint;
        }
        
        const deltaX       = dockPoints[1].x - dockPoints[0].x;
        const offset       = 15;
        const minRadius    = (deltaX - 2 * offset) / 2;
        const eccentricity = 8;
        const radius       = Math.min(minRadius, deltaX / eccentricity);
        
        const controlPoints = [
            {
                x: dockPoints[0].x + offset + radius,
                y: dockPoints[0].y
            },
            {
                x: dockPoints[1].x - offset - radius,
                y: dockPoints[1].y
            }
        ];
        
        let v1 = {
            x: controlPoints[1].x - controlPoints[0].x,
            y: controlPoints[1].y - controlPoints[0].y
        };
        
        let v2 = {
            x: -v1.x,
            y: -v1.y
        };
        
        function normalize(vector) {
            const length = Math.sqrt(Math.pow(vector.x, 2) + Math.pow(vector.y, 2));
            vector.x /= length;
            vector.y /= length;
        }
        
        normalize(v1);
        normalize(v2);
        
        function pointAlongVector(vector, point, distance) {
            return {
                x: point.x + distance * vector.x,
                y: point.y + distance * vector.y
            }
        }
        
        const p0 = {
            x: dockPoints[0].x,
            y: dockPoints[0].y
        };
        
        let p1 = {
            x: p0.x + offset,
            y: p0.y
        };
        
        let p2 = {
            x: p1.x + radius,
            y: p1.y
        };
        
        let p3 = pointAlongVector(v1, p2, radius);
        
        let p7 = {
            x: dockPoints[1].x,
            y: dockPoints[1].y
        };
        
        let p6 = {
            x: p7.x - offset,
            y: p7.y
        };
        
        let p5 = {
            x: p6.x - radius,
            y: p6.y
        };
        
        let p4 = pointAlongVector(v2, p5, radius);
        
        let curvePath = '';
        
        curvePath += 'M' + p0.x + ' ' + p0.y;
        curvePath += ' ' + p1.x + ' ' + p1.y;
        curvePath += ' ' + 'Q ' + p2.x + ' ' + p2.y;
        curvePath += ' ' + p3.x + ' ' + p3.y;
        curvePath += ' L' + p4.x + ' ' + p4.y;
        curvePath += ' Q' + p5.x + ' ' + p5.y;
        curvePath += ' ' + p6.x + ' ' + p6.y;
        curvePath += ' L' + p7.x + ' ' + p7.y;
        
        let textCenter = {
            x: p1.x + (p6.x - p1.x) / 2,
            y: p1.y + (p6.y - p1.y) / 2
        };
        
        return {
            path: curvePath,
            text: {
                position: textCenter,
                rotation: Math.atan((p5.y - p2.y) / (p5.x - p2.x)) * (180.0 / Math.PI)
            }
        };
    }
    
    link() {
        return this.data();
    }
    
    /*label() {
     if (link.implicit === false) {
     if (link.dataType === '') {
     return '<undefined>';
     }
     
     return link.dataType;
     }
     
     const noLinks   = link.represents.length;
     const character = '•';
     
     if (noLinks === 1) {
     return character;
     }
     
     return Array(noLinks).join(character);
     }
     
     
     }*/
}