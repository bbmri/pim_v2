export default class MeasureTime {
    constructor() {
        this.timeStart = performance.now();
    }
    
    capitalizeEventName(eventName) {
        return eventName.charAt(0).toUpperCase() + eventName.slice(1);
    }
    
    lap(eventName, restart = true) {
        const interval = performance.now() - this.timeStart;
        
        logger.debug(`${this.capitalizeEventName(eventName)} took ${interval.toFixed(1)} ms`);
        
        if (restart)
            this.timeStart = performance.now();
    }
}