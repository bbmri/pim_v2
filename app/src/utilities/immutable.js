export function ptol(path) {
    return path.split('.');
}

export function getIn(state, path) {
    return state.getIn(path.split('.'));
}

export function setIn(state, path, value) {
    let newState = state;

    newState = newState.setIn(path.split('.'), value);

    return newState;
}