export const JOB_STATUS_TYPES = ['idle', 'running', 'success', 'failed', 'cancelled', 'undefined'];
export const DEFAULT_JOBS_FILTER = {
    idle: false,
    running: false,
    success: false,
    failed: false,
    cancelled: false,
    undefined: false
};
export const DEFAULT_AGGREGATE = {
    idle: 0,
    running: 0,
    success: 0,
    failed: 0,
    cancelled: 0,
    undefined: 0
};