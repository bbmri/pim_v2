import './utilities/logger'

import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'
import {CookiesProvider} from 'react-cookie'

import configureStore from './store/store'
import App from 'react/App.jsx'

import 'styles/main.less'

const store = configureStore();

render(
    <Provider store={store}>
        <CookiesProvider>
            <App/>
        </CookiesProvider>
    </Provider>,
    document.getElementById('app'));
