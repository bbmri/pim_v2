import * as types from '../actions/types'

let initialState = {
    name: '',
    busy: false,
    nodes: {},
    links: {},
    dataTypes: {},
    modified: -1,
    nodeFilter: '',
    autoZoomAndPan: true,
    animateTransitions: true
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        
        case types.RUN_SET_NAME: {
            return {
                ...state,
                name: action.payload
            }
        }
        
        case types.RUN_LOAD: {
            return {
                ...initialState,
                name: action.payload
            }
        }
        
        case types.RUN_SET_BUSY: {
            return {
                ...state,
                busy: action.payload
            }
        }
        
        case types.RUN_SET_NODES: {
            let nodes = {};
            
            for (let nodeId in action.payload) {
                nodes[nodeId] = action.payload[nodeId];
                
                let node = nodes[nodeId];
                
                // delete node.inPorts['v_in_port'];
                // delete node.outPorts['v_out_port'];
                
                for (let inPortId in node.inPorts) {
                    node.inPorts[inPortId].noConnections = 0;
                }
                
                for (let outPortId in node.outPorts) {
                    node.outPorts[outPortId].noConnections = 0;
                }
                
                node.status = {
                    jobs: [0, 0, 0, 0, 0, 0],
                    progress: 0
                };
                
                if (node.children.length > 0)
                    node.type = 'Group';
                
                node.layout = {
                    position: {
                        x: 0,
                        y: 0
                    },
                    size: {
                        width: 100,
                        height: 100
                    },
                    inPorts: {},
                    outPorts: {},
                    modified: null,
                    regions: {},
                    markup: {
                        leaf: '',
                        group: ''
                    }
                };
                
                node.links     = [];
                node.toNodes   = [];
                node.fromNodes = [];
                
                if (nodeId === 'root') {
                    node.title = state.name;
                }
            }
            
            return {
                ...state,
                nodes: nodes
            }
        }
        
        case types.RUN_SET_NODE_FILTER: {
            return {
                ...state,
                nodeFilter: action.payload
            }
        }
        
        case types.RUN_SET_LINKS: {
            // Set of data types present in the run
            let dataTypes = new Set();
            
            let links = {};
            
            let nodes = {
                ...state.nodes
            };
            
            for (let nodePath in nodes) {
                const node = nodes[nodePath];
                
                // Reset the number of connections for input ports of the node
                for (let inPortId in node.inPorts) {
                    node.inPorts[inPortId].noConnections = 0;
                }
                
                // Reset the number of connections for output ports of the node
                for (let outPortId in node.outPorts) {
                    node.outPorts[outPortId].noConnections = 0;
                }
                
                node.fromNodes = [];
                node.toNodes   = [];
            }
            
            for (let link of action.payload) {
                let linkId = `${link.fromNode}/${link.fromPort}_${link.toNode}/${link.toPort}`;
                linkId     = linkId.replace(/\//g, '_');
                
                link.id         = linkId;
                link.selected   = false;
                link.fromAnchor = {x: 0, y: 0}
                link.toAnchor   = {x: 10, y: 10}
                
                nodes[link.toNode].links.push(link.id);
                nodes[link.fromNode].links.push(link.id);
                
                nodes[link.toNode].fromNodes.push(link.fromNode);
                nodes[link.fromNode].toNodes.push(link.toNode);
                
                links[linkId] = link;
                
                if (!link.implicit) {
                    dataTypes.add(link.dataType)
                }
            }
            
            // Set of data types present in the run
            let dataTypeColors = {};
            
            // Assign color to each data type
            for (let dataType of dataTypes) {
                dataTypeColors[dataType] = `hsl(${Object.keys(dataTypeColors).length * 30}, 60%, 50%)`;
            }
            
            // Assign colors to links
            for (let linkId in links) {
                const link = links[linkId];
                
                // Set link color
                if (!link.implicit) {
                    link.color = dataTypeColors[link.dataType];
                }
                
                // Obtain connected nodes
                const fromNode = nodes[link.fromNode];
                const toNode   = nodes[link.toNode];
                
                // Obtain current number of connections to in/out ports
                const fromPortNoConnections = fromNode.outPorts[link.fromPort].noConnections;
                const toPortNoConnections   = toNode.inPorts[link.toPort].noConnections;
                
                // Increment number of connections to in/out ports
                fromNode.outPorts[link.fromPort].noConnections = fromPortNoConnections + 1;
                toNode.inPorts[link.toPort].noConnections      = toPortNoConnections + 1;
                
                // Set port color to match the link color (data type)
                if (!link.implicit) {
                    const dataTypeColor = dataTypeColors[link.dataType];
                    const portColor     = dataTypeColor === undefined ? 'hsl(0, 0%, 50%)' : dataTypeColor;
                    
                    fromNode.outPorts[link.fromPort].color = portColor;
                    toNode.inPorts[link.toPort].color      = portColor;
                }
            }
            
            return {
                ...state,
                dataTypes: Array.from(dataTypes),
                links: links
            }
        }
        
        case types.RUN_SET_AGGREGATES: {
            let nodes = {
                ...state.nodes
            };
            
            for (let nodeId in action.payload) {
                let jobs        = action.payload[nodeId];
                let noJobsTotal = jobs.reduce(function (a, b) {
                    return a + b;
                }, 0);
                let progress    = 0.0;
                
                if (noJobsTotal > 0) {
                    progress = (jobs[2] + jobs[3]) / noJobsTotal;
                }
                
                nodes[nodeId].status.jobs     = jobs;
                nodes[nodeId].status.progress = progress;
            }
            
            return {
                ...state,
                nodes: nodes
            }
        }
        
        case types.RUN_EXPAND_LINK: {
            let nodes = {
                ...state.nodes
            };
            
            const link = state.links[action.payload];
            
            if (state.nodes[link.fromNode].children.length > 0) {
                nodes[link.fromNode].expanded = true;
            }
            
            if (state.nodes[link.toNode].children.length > 0) {
                nodes[link.toNode].expanded = true;
            }
            
            return {
                ...state,
                nodes: nodes
            }
        }
        
        case types.RUN_EXPAND_NODE: {
            let nodes = {
                ...state.nodes
            };
            
            nodes[action.payload].expanded = true;
            
            return {
                ...state,
                nodes: nodes
            }
        }
        
        case types.RUN_COLLAPSE_NODE: {
            let nodes = {
                ...state.nodes
            };
            
            nodes[action.payload].expanded = false;
            
            return {
                ...state,
                nodes: nodes
            }
        }
        
        case types.RUN_UPDATE_NODES: {
            let nodes = {
                ...state.nodes
            };
            
            // Determine which nodes are active/inactive
            for (let nodeId in nodes) {
                const level    = nodes[nodeId].level;
                const segments = nodeId.split('/');
                
                // Node is active by default
                let active = true;
                
                // Ignore root level
                if (level > 0) {
                    
                    // Walk up the hierarchy
                    for (let l = 0; l < level; l++) {
                        const parentNodeId       = segments.slice(0, level - l).join('/');
                        const parentNodeExpanded = nodes[parentNodeId].expanded;
                        
                        // If one of the parent nodes is inactive the node is inactive
                        if (!parentNodeExpanded) {
                            active = false;
                            break;
                        }
                    }
                }
                
                nodes[nodeId].active    = active;
                nodes[nodeId].fromNodes = [];
                nodes[nodeId].toNodes   = [];
            }
            
            let links = {
                ...state.links
            };
            
            for (let linkId in links) {
                let link = links[linkId];
                
                nodes[link.toNode].links.push(link.id);
                nodes[link.fromNode].links.push(link.id);
                nodes[link.toNode].fromNodes.push(link.fromNode);
                nodes[link.fromNode].toNodes.push(link.toNode);
            }
            
            return {
                ...state,
                nodes: nodes
            }
        }
        
        case types.RUN_EXPAND_NODE_HUB: {
            let nodes = {
                ...state.nodes
            };
            
            nodes[action.payload].hubExpanded = true;
            
            return {
                ...state,
                nodes: nodes
            }
        }
        
        case types.RUN_COLLAPSE_NODE_HUB: {
            let nodes = {
                ...state.nodes
            };
            
            nodes[action.payload].hubExpanded = false;
            
            return {
                ...state,
                nodes: nodes
            }
        }
        
        case types.RUN_SET_VISIBILITY_LEVEL: {
            let nodes = {
                ...state.nodes
            };
            
            for (let nodeId in nodes) {
                const level       = nodes[nodeId].level;
                const hasChildren = nodes[nodeId].children.length > 0;
                
                nodes[nodeId].expanded = level < action.payload && hasChildren;
            }
            
            return {
                ...state,
                nodes: nodes
            }
        }
        
        case types.RUN_SET_LAYOUT: {
            let nodes = {
                ...state.nodes
            };
            
            let links = {
                ...state.links
            };
            
            let layout = action.payload;
            
            for (let nodeId in layout) {
                if (nodes[nodeId].layout === null) {
                    layout[nodeId].modified = 0;
                } else {
                    const modified          = nodes[nodeId].layout.modified;
                    layout[nodeId].modified = modified + 1;
                }
                
                nodes[nodeId].layout = layout[nodeId];
            }
            
            for (let linkId in links) {
                const link = links[linkId];
                
                // Obtain connected nodes
                const fromNode = nodes[link.fromNode];
                const toNode   = nodes[link.toNode];
                
                // Obtain input/output node position
                const fromNodePosition = fromNode.layout.position;
                const toNodePosition   = toNode.layout.position;
                
                // Obtain input/output port position
                const fromPortPosition = fromNode.layout.outPorts[link.fromPort];
                const toPortPosition   = toNode.layout.inPorts[link.toPort]
                
                // Absolute world coordinates of the from-port
                let fromAnchor = {
                    x: fromNodePosition.x + fromPortPosition.x,
                    y: fromNodePosition.y + fromPortPosition.y
                };
                
                // Absolute world coordinates of the to-port
                let toAnchor = {
                    x: toNodePosition.x + toPortPosition.x,
                    y: toNodePosition.y + toPortPosition.y
                };
                
                // Update link from/to anchor position
                links[linkId].fromAnchor = fromAnchor;
                links[linkId].toAnchor   = toAnchor;
            }
            
            return {
                ...state,
                nodes: nodes,
                links: links
            }
        }
        
        case types.RUN_REVEAL_NODE: {
            let nodes = {
                ...state.nodes
            };
            
            const segments = action.payload.split('/');
            
            for (let i = 1; i < segments.length; i++) {
                const parentNodeId = segments.slice(0, i).join('/');
                nodes[parentNodeId].expanded = true;
            }
            
            return {
                ...state,
                nodes: nodes
            }
        }
        
        case types.RUN_SET_MODIFIED: {
            return {
                ...state,
                modified: state.modified + 1,
            }
        }
        
        default:
            return state
    }
    
    return initialState;
}
