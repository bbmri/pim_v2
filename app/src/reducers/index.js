import {combineReducers} from 'redux-immutable';

import runs from './runs';
import run from './run';
import jobs from './jobs';
import job from './job';

const rootReducer = combineReducers({
    runs: runs,
    run: run,
    jobs: jobs,
    job: job
});

export default rootReducer;