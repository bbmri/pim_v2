import * as types from '../actions/types'

import Immutable from 'immutable'

let initialState = Immutable.Map({
    sortColumn: 'name',
    sortDirection: 'ascending',
    nameFilter: '',
    userFilter: '',
    noPages: 0,
    pageSize: 10,
    pageIndex: 0,
    noPagesPerView: 5,
    pageRows: Immutable.List(),
    noRows: 0,
    updating: false
});

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case types.RUNS_SET_UPDATING: {
            // logger.info('RUNS_SET_UPDATING');
            return state.set('updating', action.payload);
        }

        case types.RUNS_SET_PAGE_ROWS: {
            // logger.info('RUNS_SET_PAGE_ROWS');
            return state.set('pageRows', Immutable.fromJS(action.payload));
        }

        case types.RUNS_SET_NO_ROWS: {
            // logger.info('RUNS_SET_NO_ROWS');
            return state.set('noRows', Immutable.fromJS(action.payload));
        }

        case types.RUNS_SET_NAME_FILTER: {
            // logger.info('RUNS_SET_NAME_FILTER');
            return state.set('nameFilter', action.payload);
        }

        case types.RUNS_SET_SORT_COLUMN: {
            // logger.info('RUNS_SET_SORT_COLUMN');
            return state.set('sortColumn', action.payload);
        }

        case types.RUNS_SET_SORT_DIRECTION: {
            // logger.info('RUNS_SET_SORT_DIRECTION');
            return state.set('sortDirection', action.payload);
        }

        case types.RUNS_SET_PAGE_INDEX: {
            // logger.info('RUNS_SET_PAGE_INDEX');
            return state.set('pageIndex', action.payload);
        }

        case types.RUNS_SET_PAGE_SIZE: {
            // logger.info('RUNS_SET_PAGE_SIZE');
            return state.set('pageSize', action.payload);
        }

        case types.RUNS_UPDATE_PAGINATION: {
            // logger.info('RUNS_UPDATE_PAGINATION');

            const noPages = Math.ceil(action.payload / state.get('pageSize'));
            const pageIndex = Math.min(Math.max(0, noPages - 1), state.get('pageIndex'));

            let newState = state;

            newState = newState.set('noPages', noPages);
            newState = newState.set('pageIndex', pageIndex);

            return newState;
        }

        default:
            return state
    }

    return initialState;
}
