import {Map} from 'extendable-immutable'

export default class Nodes extends Map {
    expandedGroupIds() {
        let expandedGroupIds = [];

        this.keySeq().forEach(nodeId => {
            let node = this.get(nodeId);

            if (node.visible === true && node.expanded === true && node.children.length > 0) {
                expandedGroupIds.push(nodeId);
            }
        });

        return expandedGroupIds;
    }

    collapsedGroupIds() {
        let collapsedGroupIds = [];

        this.keySeq().forEach(nodeId => {
            let node = this.get(nodeId);

            if (node.visible === true && node.expanded === false && node.children.length > 0) {
                collapsedGroupIds.push(nodeId);
            }
        });

        return collapsedGroupIds;
    }

    parentNode(nodeId) {
        let node = this.get(nodeId);
        let parentId = node.parentId();

        return this.get(parentId);
    }
}