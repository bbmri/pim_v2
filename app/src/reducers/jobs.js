import * as types from '../actions/types'

import Immutable from 'immutable'

import * as constants from './../utilities/constants';

let initialState = Immutable.Map({
    runName: '',
    nodePath: '',
    sortColumn: 'name',
    sortDirection: 'ascending',
    filter: Immutable.Map(constants.DEFAULT_JOBS_FILTER),
    noPages: 0,
    pageSize: 10,
    pageIndex: 0,
    noPagesPerView: 8,
    pageRows: Immutable.List(),
    noRows: 0,
    aggregate: Immutable.Map(constants.DEFAULT_AGGREGATE),
    noFetches: 0,
    fetchError: null
});

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case types.JOBS_SET_RUN_NAME: {
            return state.set('runName', action.payload);
        }

        case types.JOBS_SET_NODE_PATH: {
            return state.set('nodePath', action.payload);
        }
        
        case types.JOBS_SET_SORT_COLUMN: {
            return state.set('sortColumn', action.payload);
        }

        case types.JOBS_SET_SORT_DIRECTION: {
            return state.set('sortDirection', action.payload);
        }
        
        case types.JOBS_SET_FILTER: {
            let filter = state.get('filter').toJS();

            filter[action.payload.type] = action.payload.state;

            return state.set('filter', Immutable.fromJS(filter));
        }

        case types.JOBS_TOGGLE_FILTER: {
            let filter = state.get('filter').toJS();
            
            filter[action.payload] = !filter[action.payload];

            return state.set('filter', Immutable.fromJS(filter));
        }
        
        case types.JOBS_SET_PAGE_INDEX: {
            return state.set('pageIndex', action.payload);
        }

        case types.JOBS_SET_PAGE_SIZE: {
            return state.set('pageSize', action.payload);
        }

        case types.JOBS_UPDATE_PAGINATION: {
            const noPages = Math.ceil(action.payload / state.get('pageSize'));
            const pageIndex = Math.min(Math.max(0, noPages - 1), state.get('pageIndex'));

            let newState = state;

            newState = newState.set('noPages', noPages);
            newState = newState.set('pageIndex', pageIndex);

            return newState;
        }
        
        case types.JOBS_SET_PAGE_ROWS: {
            let newState = state;
            
            newState = newState.set('pageRows', Immutable.fromJS(action.payload));
            newState = newState.set('noFetches', newState.get('noFetches') + 1);
            
            return newState;
        }

        case types.JOBS_SET_NO_ROWS: {
            return state.set('noRows', Immutable.fromJS(action.payload));
        }

        case types.JOBS_SET_AGGREGATE: {
            let newState = state;

            newState = newState.setIn(['aggregate', 'idle'], action.payload[0]);
            newState = newState.setIn(['aggregate', 'running'], action.payload[1]);
            newState = newState.setIn(['aggregate', 'success'], action.payload[2]);
            newState = newState.setIn(['aggregate', 'failed'], action.payload[3]);
            newState = newState.setIn(['aggregate', 'cancelled'], action.payload[4]);
            newState = newState.setIn(['aggregate', 'undefined'], action.payload[5]);

            return newState;
        }
        
        case types.JOBS_SET_FETCH_ERROR: {
            return state.set('fetchError', action.payload);
        }
        
        default:
            return state
    }

    return initialState;
}
