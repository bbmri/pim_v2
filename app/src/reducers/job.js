import * as types from '../actions/types'

import Immutable from 'immutable'

import * as constants from './../utilities/constants';

let initialState = Immutable.Map({
    runName: '',
    jobPath: '',
    job: Immutable.Map({})
});

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case types.JOB_INITIALIZE: {
            let newState = state;

            /*function Props(obj) {

                function updateProps(obj) {
                    for (var property in obj) {
                        if (obj.hasOwnProperty(property)) {
                            obj.expanded = true;

                            if (obj[property].constructor == Object) {
                                // console.log('**Object -> ' + property + ': ');
                                updateProps(obj[property]);
                            } else {
                                // obj.expanded = true;
                                // console.log(property + " " + obj[property]);
                            }
                        }
                    }
                }

                updateProps(obj);
            }

            Props(action.payload.job)

            logger.info(action.payload.job);*/

            newState = newState.set('runName', action.payload.runName);
            newState = newState.set('jobPath', action.payload.jobPath);
            newState = newState.set('job', Immutable.fromJS(action.payload.job));

            return newState;
        }

        default:
            return state
    }

    return initialState;
}