import {Map} from 'extendable-immutable'

export default class Links extends Map {
    getExplicitIds() {
        let explicitIds = [];

        this.keySeq().forEach(linkId => {
            let link = this.get(linkId);

            if (link.get('implicit') === false) {
                explicitIds.push(linkId);
            }
        });

        return explicitIds;
    }

    getImplicitIds() {
        let implicitIds = [];

        this.keySeq().forEach(linkId => {
            let link = this.get(linkId);

            if (link.get('implicit') === true) {
                implicitIds.push(linkId);
            }
        });

        return implicitIds;
    }

    getIds() {
        let ids = [];

        this.keySeq().forEach(linkId => {
            ids.push(linkId);
        });

        return ids;
    }
}