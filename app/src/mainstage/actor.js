import * as d3 from 'd3'

import PubSub from 'pubsub-js'

import Renderer from './renderer'
import Publisher from './publisher'
import Animation from './animation'
import Prop from './prop'
import Actions from './actions'
import AABB from './aabb'

export default class Actor extends Publisher {
    constructor({id, type, animation = new Animation({})}) {
        super();
        
        this.id     = id;
        this.type   = type;
        this.params = {
            previous: null,
            current: {
                visible: false,
                opacity: 1,
                selected: false,
                visibleOnCanvas: false,
                inDOM: false,
                position: {
                    x: 0,
                    y: 0
                },
                size: {
                    width: 0,
                    height: 0
                },
                rotation: 0,
                data: null
            }
        };
        
        this.animation   = animation;
        this.propLayers  = Array(Renderer.noLayers()).fill().map((e, i) => new Object());
        this.props       = {};
        this.subscribers = {
            updateDOM: PubSub.subscribe(Actions.updateAllActors, this._onUpdateDOM.bind(this))
        };
        this.dirty       = {
            position: false,
            opacity: false
        };
    }
    
    /**
     * ToDo: Write documentation
     */
    _createProps() {
        for (let propLayer of this.propLayers) {
            for (let propId in propLayer) {
                propLayer[propId].unsubscribe();
                delete propLayer[propId];
            }
        }
    }
    
    /**
     * ToDo: Write documentation
     */
    _commitParamChanges() {
        this._handleParamsChange();
        
        // if (this.params.current.data.path === 'root')
        //     logger.info(this.id, this.params)
        
        this.params.previous = JSON.parse(JSON.stringify(this.params.current));
    }
    
    /**
     * ToDo: Write documentation
     */
    _handleParamsChange() {
        const current  = this.params.current;
        const previous = this.params.previous;
        
        if (previous === null) {
            if (current.inDOM === true) {
                this.dirty.opacity = true;
                this._enterStage();
            }
        } else {
            if (current.inDOM === true && previous.inDOM === false) {
                this.dirty.opacity = true;
                this._enterStage();
            }
            
            if (current.inDOM === false && previous.inDOM === true) {
                this._leaveStage();
            }
            
            if (Prop.notEqualParams(this.params, 'position')) {
                this.dirty.position = true;
            }
            
            if (Prop.notEqualParams(this.params, 'data')) {
                this._updateData();
            }
        }
    }
    
    /**
     * ToDo: Write documentation
     */
    _enterStage() {
        // logger.debug(Actions.actorEnteringStage, this.id);
        
        if (!this.params.current.visible)
            return;
        
        PubSub.publish(Actions.actorEnteringStage, this);
        
        this._addToDOM();
        
        PubSub.publish(Actions.actorEnteredStage, this);
    }
    
    /**
     * ToDo: Write documentation
     */
    _leaveStage() {
        // logger.debug(Actions.actorLeavingStage, this.id);
        
        // if (!this.params.current.visible)
        //     return;
        
        PubSub.publish(Actions.actorLeavingStage, this);
        
        this._removeFromDOM();
        
        PubSub.publish(Actions.actorLeftStage, this);
    }
    
    /**
     * ToDo: Write documentation
     */
    _updateData() {
        this.publish(Actions.actorUpdatingData);
        
        if (!this.params.current.visible)
            return;
        
        for (let prop of Object.values(this.props))
            prop.checkDirty();
        
        this.publish(Actions.actorUpdatedData);
    }
    
    /**
     * ToDo: Write documentation
     */
    _addToDOM() {
        PubSub.publish(Actions.actorAddingToDOM, this);
        
        const layerIds = Array.from({length: Renderer.noLayers()}, (x, i) => i);
        const root     = d3.select('#root');
        const position = this.position();
        
        for (let layerId of layerIds) {
            if (Object.keys(this.propLayers[layerId]).length > 0) {
                let group = root.select(`#layer_${layerId}`)
                    .selectAll(`g.actor[id=${this.id}]`)
                    .data([this.id])
                    .enter()
                    .append('g')
                    .attr('class', 'actor')
                    .attr('id', this.id)
                    .attr('transform', `translate(${position.x}, ${position.y})`);
                
                group
                    .append('g')
                    .attr('class', 'visibility')
                    .attr('opacity', 0);
            }
        }
        
        this.params.current.inDOM = true;
        
        for (let prop of Object.values(this.props))
            prop._synchronizeDOM();
        
        PubSub.publish(Actions.actorAddedToDOM, this);
    }
    
    /**
     * ToDo: Write documentation
     */
    _updateDOM() {
        // logger.debug('UPDATE_DOM', this.id);
        
        PubSub.publish(Actions.actorUpdatingDOM, this);
        
        if (this.dirty.opacity) {
            this.setOpacity();
            this.dirty.opacity = false;
        }
        
        if (this.dirty.position) {
            this.setPosition();
            this.dirty.position = false;
        }
        
        PubSub.publish(Actions.actorUpdatedDOM, this);
    }
    
    /**
     * ToDo: Write documentation
     */
    _removeFromDOM() {
        // logger.debug('REMOVE_ACTOR_FROM_DOM', this.id);
        
        PubSub.publish(Actions.actorRemovingFromDOM, this);
        
        this.params.current.inDOM = false;
        
        // logger.info(d3.select('#root').selectAll(`g.actor[id=${this.id}]`))
        
        d3.select('#root').selectAll(`g.actor[id=${this.id}]`).remove();
        
        /*d3.select('#root')
            .selectAll(`g.actor[id=${this.id}]`)
            .transition()
            .ease(d3[this.animation.hide.ease])
            .duration(this.animation.hide.duration)
            .delay(this.animation.hide.delay)
            .attr('opacity', 0)
            .on('end', function () {
                d3.select(this).remove();
            });*/
        
        for (let prop of Object.values(this.props))
            prop._synchronizeDOM();
        
        PubSub.publish(Actions.actorRemovedFromDOM, this);
    }
    
    /**
     * ToDo: Write documentation
     */
    _onUpdateDOM(publisher, args) {
        if (!this.params.current.visible)
            return;
        
        this._updateDOM();
        
        for (let prop of Object.values(this.props))
            prop.updateDOM();
    }
    
    /**
     * Returns the actor position
     */
    position() {
        return this.params.current.position;
    }
    
    /**
     * ToDo: Write documentation
     */
    setPosition() {
        const transition = true;
        
        d3.select('#root').selectAll(`g.actor[id=${this.id}]`)
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.update.duration : 0)
            .delay(transition ? this.animation.update.delay : 0)
            .attr('transform', `translate(${this.position().x}, ${this.position().y})`);
    }
    
    /**
     * Returns the actor size
     */
    size() {
        return this.params.current.size;
    }
    
    /**
     * ToDo: Write documentation
     */
    setOpacity() {
        const transition = true;
        
        d3.select('#root')
            .selectAll(`g.actor[id=${this.id}]`)
            .selectAll('g.visibility')
            .transition()
            .ease(d3[this.animation.show.ease])
            .duration(transition ? this.animation.show.duration : 0)
            .delay(transition ? this.animation.show.delay : 0)
            .attr('opacity', 1);
    }
    
    /**
     * ToDo: Write documentation
     */
    data() {
        return this.params.current.data;
    }
    
    /**
     * ToDo: Write documentation
     */
    inDOM() {
        return this.params.current.inDOM;
    }
    
    /**
     * ToDo: Write documentation
     */
    onStage() {
        return true;
        
        let aabb = new AABB(this.position(), this.size());
        
        aabb.inflate(100);
        
        return Renderer.canvasAabbToWorld().intersects(aabb);
    }
    
    /**
     * ToDo: Write documentation
     */
    terminate() {
        for (let subscriberId in this.subscribers) {
            PubSub.unsubscribe(this.subscribers[subscriberId]);
        }
        
        this._removeFromDOM();
        
        for (let prop of Object.values(this.props)) {
            prop.unsubscribe();
        }
        
    }
}