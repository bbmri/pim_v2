export default class AABB {
    constructor(position = { x: 0, y: 0}, size = {width: 0, height: 0}) {
        this.topLeft = {
            x: 0,
            y: 0
        };
        
        this.bottomRight = {
            x: 0,
            y: 0
        };
        
        this.size = {
            width: 0,
            height: 0
        };
        
        this.center = {
            x: 0,
            y: 0
        };
        
        this.setPosition(position);
        this.setSize(size);
    }
    
    updateCenter() {
        this.center.x = this.topLeft.x + this.size.width / 2;
        this.center.y = this.topLeft.y + this.size.height / 2;
    }
    
    setPosition(position) {
        this.topLeft = {
            x: position.x,
            y: position.y
        };
        
        this.bottomRight = {
            x: this.topLeft.x + this.size.width,
            y: this.topLeft.y + this.size.height
        };
        
        this.updateCenter();
    }
    
    setSize(size) {
        this.size = {
            width: size.width,
            height: size.height
        };
        
        this.bottomRight = {
            x: this.topLeft.x + this.size.width,
            y: this.topLeft.y + this.size.height
        };
        
        this.updateCenter();
    }
    
    set(position, size) {
        this.setPosition(position);
        this.setSize(size);
    }
    
    setPoints(pA, pB) {
        this.topLeft = {
            x: Math.min(pA.x, pB.x),
            y: Math.min(pA.y, pB.y),
        };
        
        this.bottomRight = {
            x: Math.max(pA.x, pB.x),
            y: Math.max(pA.y, pB.y),
        };
        
        this.setSize({
            width: this.bottomRight.x - this.topLeft.x,
            height: this.bottomRight.y - this.topLeft.y
        });
    }
    
    move(offset) {
        this.setPosition({
            x: this.topLeft.x + offset.x,
            y: this.topLeft.y + offset.y
        });
    }
    
    intersects(other) {
        if (other.topLeft.y > this.bottomRight.y || other.bottomRight.y < this.topLeft.y)
            return false;
        
        if (other.topLeft.x > this.bottomRight.x || other.bottomRight.x < this.topLeft.x)
            return false;
        
        return true;
    }
    
    inflate(amount) {
        this.topLeft.x -= amount;
        this.topLeft.y -= amount;
        
        this.setSize({
            width: this.size.width + 2 * amount,
            height: this.size.height + 2 * amount
        })
    }
}