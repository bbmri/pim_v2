export default class Interval {
    sampleInterval(start, end) {
        return Math.random() * (end - start) + start;
    }
    
    constructor(start, end, opacity = null, randomInterval = 0.0) {
        if (randomInterval > 0) {
            const halfRandom = 0.5 * randomInterval;
            const middle     = start + (0.5 * (end - start));
            
            this.start = this.sampleInterval(start - halfRandom, Math.min(start + randomInterval, middle));
            this.end   = this.sampleInterval(end - halfRandom, Math.max(end + randomInterval, middle));
        } else {
            this.start = start;
            this.end   = end;
        }
        
        this.opacity = opacity;
    }
}