export default {
    // Event to set the zoom region
    setZoomRegion: 'MS_SET_ZOOM_REGION',
    
    // Callback event which is invoked when the zoom region has changed
    zoomRegionChanged: 'MS_ZOOM_REGION_CHANGED',
    
    // Callback event which is invoked when all actors need to update their DOM
    updateAllActors: 'MS_UPDATE_ALL_ACTORS',
    
    // Callback event which is invoked prior to the actor entering the stage
    actorEnteringStage: 'MS_ACTOR_ENTERING_STAGE',
    
    // Callback event which is invoked when the actor has entered the stage
    actorEnteredStage: 'MS_ACTOR_ENTERED_STAGE',
    
    // Callback event which is invoked when the actor is leaving the stage
    actorLeavingStage: 'MS_ACTOR_LEAVING_STAGE',
    
    // Callback event which is invoked when the actor has left the stage
    actorLeftStage: 'MS_ACTOR_LEFT_STAGE',
    
    // Callback event which is invoked prior to the actor being added to the DOM
    actorAddingToDOM: 'MS_ACTOR_ADDING_TO_DOM',
    
    // Callback event which is invoked when the actor has been added to the DOM
    actorAddedToDOM: 'MS_ACTOR_ADDED_TO_DOM',
    
    // Callback event which is invoked just before updating the DOM
    actorUpdatingDOM: 'MS_ACTOR_UPDATING_DOM',
    
    // Callback event which is invoked when the DOM has been updated
    actorUpdatedDOM: 'MS_ACTOR_UPDATED_DOM',
    
    // Callback event which is invoked prior to the actor being removed from the DOM
    actorRemovingFromDOM: 'MS_ACTOR_REMOVING_FROM_DOM',
    
    // Callback event which is invoked when the actor has been removed from the DOM
    actorRemovedFromDOM: 'MS_ACTOR_REMOVED_FROM_DOM',
    
    // Callback event which is invoked prior to updating actor data
    actorUpdatingData: 'MS_ACTOR_UPDATING_DATA',
    
    // Callback event which is invoked when actor data has changed
    actorUpdatedData: 'MS_ACTOR_UPDATED_DATA',
};