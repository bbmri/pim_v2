export const animation = {
    duration: 250
};

export const actor = {
    noLayers: 10,
    leafRadius: 8,
    groupRadius: 8,
    shadowOffset: {
        x: 10,
        y: 10
    },
    groupPadding: {
        left: 0,
        top: 0,
        right: 0,
        bottom: 0
    }
};

export const layout = {
    spacing: 25,
    borderSpacing: 200
};

export const synchronize = {
    runs: 1000,
    graph: 1000
};

export const layers = {
    group: 1,
    link: 7,
    leaf: 10
};

export const renderer = {
    noLayers: 20,
    log: {
        enter: true,
        exit: true,
        update: true
    }
};