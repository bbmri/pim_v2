import _ from "lodash";

export default class Animation {
    constructor({show = {}, hide = {}, update = {}}) {
        
        this.show = _.merge({
            ease: Animation.ease,
            duration: Animation.duration,
            delay: Animation.delay
        }, show);
        
        this.hide = _.merge({
            ease: Animation.ease,
            duration: Animation.duration,
            delay: Animation.delay
        }, hide);
        
        this.update = _.merge({
            ease: Animation.ease,
            duration: Animation.duration,
            delay: Animation.delay
        }, update);
        
        this.opacity = _.merge({
            ease: Animation.ease,
            duration: 200,
            delay: 0
        }, update);
    }
}

// Default animation settings
Animation.ease     = 'easeQuad';
Animation.duration = 600;
Animation.delay    = 0;

