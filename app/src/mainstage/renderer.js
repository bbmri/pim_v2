import * as d3 from 'd3'

import PubSub from 'pubsub-js'

import AABB from './aabb'
import Animation from "./animation";
import MeasureTime from "../utilities/measureTime";
import Actions from "./actions";

class Renderer {
    constructor() {
        this.zoomTimer          = null;
        this.zoomTransform      = null;
        this.animateTransitions = true;
        this.initialized        = false;
        
        PubSub.subscribe(Actions.setZoomRegion, this.onSetZoomBox.bind(this));
    }
    
    noLayers() {
        return 20;
    }
    
    /**
     * Returns the SVG canvas size
     */
    clientSize() {
        let svg = document.getElementById('svg');
        
        return {
            width: svg.clientWidth,
            height: svg.clientHeight
        };
    }
    
    zoomFactor() {
        if (this.zoomTransform === null)
            return 1;
        
        return this.zoomTransform.k;
    }
    
    onZoomPanTimeOut() {
        PubSub.publish(Actions.zoomRegionChanged, this.zoomFactor());
        
        window.clearInterval(this.zoomTimer);
        
        this.zoomTimer = null;
    }
    
    /**
     * Invoked when the pan/zoom changed
     */
    pannedZoomed() {
        d3.select('#root')
            .attr('transform', d3.event.transform);
        
        this.zoomTransform = d3.event.transform;
        
        if (this.zoomTimer !== null)
            window.clearInterval(this.zoomTimer);
        
        this.zoomTimer = window.setInterval(this.onZoomPanTimeOut.bind(this), 100);
    }
    
    /**
     * Initializes the renderer
     */
    initialize() {
        if (this.initialized)
            return;
        
        this.zoom = d3.zoom()
            .on("zoom", this.pannedZoomed.bind(this));
        
        // Cache SVG selection
        let svg = d3.select('#svg');
        
        // Turn off double clicking
        // svg.on('dblclick.zoom', null);
        
        /*// Removing existing root element
        svg.select('#root').remove();
        
        
        // Add root element
        svg
            .append('g')
            .attr('id', 'root');
        
        svg.select('#root').selectAll('g.layer')
            .data(Array.from({length: 20}, (x, i) => i))
            .enter()
            .append('g')
            .attr('class', 'layer')
            .attr('id', function (id) {
                return `layer_${id}`
            });
        
        
        let pattern = d3.select('svg')
            .append('defs')
            .append('pattern')
            .attr('id', 'diagonal_stripes')
            .attr('width', 40)
            .attr('height', 40)
            .attr('patternUnits', 'userSpaceOnUse')
            .attr('patternTransform', 'rotate(45)');
        
        pattern
            .append('rect')
            .attr('width', 20)
            .attr('height', 40)
            .attr('transform', 'translate(0,0)')
            .attr('fill', 'hsl(0, 0%, 50%)')
            .attr('fill-opacity', 0.3);*/
        
        this.initialized = true;
    }
    
    /**
     * ToDo: Write documentation
     */
    setZoomBox(zoomBox, animate = false) {
        // logger.debug('SET_ZOOM_BOX', zoomBox);
        
        const clientSize   = this.clientSize();
        const margin       = 250;
        const regionWidth  = zoomBox.size.width + 2 * margin;
        const regionHeight = zoomBox.size.height + 2 * margin;
        const scale        = 1 / Math.max(regionWidth / clientSize.width, regionHeight / clientSize.height);
        const x            = zoomBox.position.x + (zoomBox.size.width / 2);
        const y            = zoomBox.position.y + (zoomBox.size.height / 2);
        const translate    = [clientSize.width / 2 - scale * x, clientSize.height / 2 - scale * y];
        
        this.zoomTransform = d3.zoomIdentity.translate(translate[0], translate[1]).scale(scale);
        
        d3.select('#svg')
            .transition()
            .ease(d3[Animation.ease])
            .duration(animate ? Animation.duration : 0)
            .delay(animate ? Animation.delay : 0)
            .call(this.zoom.transform, this.zoomTransform);
    }
    
    /**
     * ToDo: Write documentation
     */
    onSetZoomBox(message, data) {
        this.setZoomBox(data.zoomBox, this.animateTransitions);
    }
    
    /**
     * Converts the canvas axis aligned bounding box to world coordinates
     * @returns {aabb} Canvas axis aligned bounding box in world coordinates
     */
    canvasAabbToWorld() {
        const clientSize    = this.clientSize();
        const zoomTransform = d3.zoomTransform(d3.select('#svg').node());
        
        const size = {
            width: clientSize.width / zoomTransform.k,
            height: clientSize.height / zoomTransform.k
        };
        
        const position = {
            x: -zoomTransform.x / zoomTransform.k,
            y: -zoomTransform.y / zoomTransform.k
        };
        
        /*d3.select('#root').selectAll('rect.temp').remove();
         
         d3.select('#root')
         .append('rect')
         .attr('class', 'temp')
         .attr('x', position.x + 5)
         .attr('y', position.y + 5)
         .attr('width', 10)
         .attr('height', 10)
         .attr('opacity', 0.1);
         
         d3.select('#root')
         .append('rect')
         .attr('class', 'temp')
         .attr('x', position.x + 5)
         .attr('y', position.y + 5)
         .attr('width', size.width-10)
         .attr('height', size.height-10)
         .attr('opacity', 0.1);*/
        
        return new AABB(position, size);
    }
    
    /**
     * Restores the view
     * @param {boolean} animate - Whether the view update should be animated or not
     */
    restoreView(animate = false) {
        logger.debug('Restoring view');
        
        d3.select('#svg')
            .call(this.zoom)
            .call(this.zoom.transform, this.zoomTransform);
    }
    
    /**
     * ToDo: Write documentation
     */
    defaultView() {
        const clientSize = this.clientSize();
        
        let initialTransform = d3.zoomIdentity
            .translate(clientSize.width / 2, clientSize.height / 2)
            .scale(0.01);
        
        d3.select('#svg')
            .call(this.zoom)
            .call(this.zoom.transform, initialTransform);
    }
}

export default new Renderer();