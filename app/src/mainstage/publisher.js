export default class Publisher {
    /**
     * Initializes the publisher with an emtpy subscribers map
     * Each message type has a list of subscribers
     * A subscriber is a function in the form: onEvent(messageType, ...args)
     */
    constructor() {
        this.subscribers = {};
    }
    
    /**
     * Subscribe to messages of messageType and call subscriberFunction when messageType is fired
     * @param {string} messageType - Type of message
     * @param {function} subscriberFunction - Function to call when messageType is fired
     */
    subscribe(messageType, subscriberFunction) {
        if (this.subscribers[messageType] === undefined) {
            this.subscribers[messageType] = [];
        }
        
        this.subscribers[messageType].push(subscriberFunction);
    }
    
    /**
     * ToDo: Write documentation
     */
    unsubscribe(subscriberFunction) {
        for (let messageType in this.subscribers) {
            if (this.subscribers[messageType] !== undefined) {
                const index = this.subscribers[messageType].indexOf(subscriberFunction);
                
                if (index >= 0)
                    delete this.subscribers[messageType][index];
            }
        }
    }
    
    /**
     * Notify all subscribers that messageType has been fired
     * @param {string} messageType - Type of message
     * @param {args} args - Arguments
     */
    publish(messageType, ...args) {
        if (this.subscribers[messageType] === undefined)
            return;
        
        for (let subscriber of this.subscribers[messageType]) {
            this.callAsync(subscriber, args).then(/*()=>{ console.log('DOM synchronized') }*/);
        }
    }
    
    async callAsync(subscriber, args) {
        // logger.debug('callAsync')
        subscriber(this, ...args);
    }
}