import * as d3 from 'd3'
import _ from 'lodash'

import Renderer from './renderer'
import AABB from './aabb'
import Animation from './animation'
import PubSub from "pubsub-js";
import Actions from "./actions";

export default class Prop {
    constructor({actor, id, type, visible = true, z = 0, callbacks = {}, intervals = [[0, 100]]}) {
        
        this.actor       = actor;
        this.id          = `${actor.id}_${id}`;
        this.aabb        = new AABB();
        this.modified    = 0;
        this.exiting     = false;
        this.type        = type;
        this.visible     = visible;
        this.opacity     = 1;
        this.z           = z;
        this.animation   = new Animation({});
        this.callbacks   = _.merge({}, {}, callbacks);
        this.intervals   = intervals;
        this.selection   = {};
        this.inDOM       = false;
        this.dirty       = false;
        this.subscribers = {
            zoomChanged: PubSub.subscribe(Actions.zoomRegionChanged, this.onRendererZoomChanged.bind(this))
        };
        
        this.actor.propLayers[this.z][this.id] = this;
        this.actor.props[id]                   = this;
    }
    
    /**
     *
     */
    updateDOM() {
        if (!this.inDOM || !this.dirty)
            return;
        
        this._updateDOM(true);
        
        this.dirty = false;
    }
    
    /**
     * Determines whether the prop should be in the DOM or not
     *
     * @returns {boolean}
     */
    _shouldBeInDOM() {
        return this.opacity * this.zoomOpacity(Renderer.zoomFactor()) > 0;
    }
    
    /**
     * Synchronizes the prop by adding or removing DOM elements
     *
     * The behaviour is zoom dependent
     */
    _synchronizeDOM() {
        if (this.actor.inDOM() && this._shouldBeInDOM()) {
            if (!this.inDOM) {
                this._addToDOM();
                this.inDOM = true;
            }
            
            // this.setOpacity(this.opacity, true);
        } else {
            if (this.inDOM) {
                this._removeFromDOM();
                this.inDOM = false;
            }
        }
    }
    
    /**
     * Invoked when the renderer zoom changes
     *
     * Used to establish zoom dependent behaviour
     */
    onRendererZoomChanged(message, data) {
        // logger.debug(this.id, 'ACTOR_ZOOM_CHANGED', data.zoomFactor);
        
        this._synchronizeDOM();
    }
    
    /**
     *
     */
    checkDirty() {
        this.dirty = this._shouldUpdateDOM(this.actor.params);
    }
    
    /**
     * Return the D3 element in which the props resides
     */
    _propContainer() {
        return d3.select(`#layer_${this.z}`).select(`g.actor[id=${this.actor.id}]`).select('g.visibility');
    }
    
    /**
     * Add prop to the DOM
     */
    _addToDOM() {
        // logger.debug(this.id, 'ADD_PROP_TO_DOM', this.propContainer());
        
        /*this.selection.zoomOpacity = this.propContainer()
         .append('g')
         .attr('class', `prop ${this.type}`)
         .attr('id', this.id)
         .attr('opacity', this.zoomOpacity(Renderer.zoomFactor()));*/
        
        this.selection.prop = this._propContainer()
            .append('g')
            .attr('class', `prop ${this.type}`)
            .attr('id', this.id)
            .attr('opacity', 0);
        
        // this.setOpacity(this.opacity * this.zoomOpacity(Renderer.zoomFactor()));
        
        this.selection.prop
            .transition()
            .ease(d3[this.animation.opacity.ease])
            .duration(this.animation.opacity.duration)
            .delay(Math.random() * this.animation.opacity.duration)
            // .delay(this.animation.show.delay)
            .attr('opacity', this.opacity);
    }
    
    /**
     * Updates the prop DOM elements
     */
    _updateDOM() {
    }
    
    /**
     * Remove prop from the DOM
     */
    _removeFromDOM() {
        // logger.debug(this.id, 'REMOVE_PROP_FROM_DOM', this.selection.prop);
        
        this.selection.prop
            .transition()
            .ease(d3[this.animation.opacity.ease])
            .duration(this.animation.opacity.duration)
            .delay(Math.random() * 0.5 * this.animation.opacity.duration)
            // .delay(this.animation.hide.delay)
            .attr('opacity', 0)
            .on('end', () => {
                this.inDOM = false;
                this.selection.prop.remove();
            });
    }
    
    /**
     * Determines whether the DOM should be updated or not
     */
    _shouldUpdateDOM(params) {
        return false;
    }
    
    /**
     * Computes zoom dependent prop opacity
     */
    zoomOpacity(zoomFactor) {
        for (let interval of this.intervals) {
            if (zoomFactor >= interval.start && zoomFactor < interval.end) {
                return interval.opacity === null ? 1 : interval.opacity;
            }
        }
        
        return 0;
    }
    
    /**
     * Sets the prop opacity
     *
     * @param opacity
     */
    setOpacity(opacity, transition = false) {
        this.opacity = opacity;
        
        this._synchronizeDOM();
        
        if (this.selection.prop === undefined)
            return;
        
        this.selection.prop
            .transition()
            .ease(d3[this.animation.update.ease])
            .duration(transition ? this.animation.opacity.duration : 0)
            .delay(0)
            .attr('opacity', this.opacity * this.zoomOpacity(Renderer.zoomFactor()))
            .on('end', () => {
                this._synchronizeDOM();
            });
    }
    
    /**
     * Helper function to obtain the actor data
     */
    data() {
        return this.actor.data();
    }
    
    /**
     * Cancel all pub-sub and subject-observer listeners
     */
    unsubscribe() {
        for (let subscriberId in this.subscribers) {
            PubSub.unsubscribe(this.subscribers[subscriberId]);
        }
    }
    
    /**
     * Get nested object property from path
     */
    static resolve(path, obj = self, separator = '.') {
        let properties = Array.isArray(path) ? path : path.split(separator);
        return properties.reduce((prev, curr) => prev && prev[curr], obj);
    }
    
    /**
     * Test whether two object properties (by path) are not equal to each other
     */
    static notEqualParams(params, path) {
        const previous = Prop.resolve(`previous.${path}`, params);
        const current  = Prop.resolve(`current.${path}`, params);
        
        if (params.previous === null)
            return true;
        
        return JSON.stringify(current) !== JSON.stringify(previous);
    }
}