--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.9
-- Dumped by pg_dump version 9.6.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: aggregates; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.aggregates (
    id integer NOT NULL,
    status integer[],
    node_id integer
);


ALTER TABLE public.aggregates OWNER TO pim;

--
-- Name: aggregates_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.aggregates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aggregates_id_seq OWNER TO pim;

--
-- Name: aggregates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.aggregates_id_seq OWNED BY public.aggregates.id;


--
-- Name: in_ports; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.in_ports (
    id integer NOT NULL,
    path character varying(256) NOT NULL,
    title character varying(128),
    description text,
    created timestamp without time zone,
    modified timestamp without time zone,
    custom_data json,
    node_id integer
);


ALTER TABLE public.in_ports OWNER TO pim;

--
-- Name: in_ports_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.in_ports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.in_ports_id_seq OWNER TO pim;

--
-- Name: in_ports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.in_ports_id_seq OWNED BY public.in_ports.id;


--
-- Name: jobs; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.jobs (
    id integer NOT NULL,
    path character varying(256),
    title character varying(128),
    description text,
    created timestamp without time zone,
    modified timestamp without time zone,
    custom_data json,
    run_id integer NOT NULL,
    node_id integer,
    node_path character varying(256) NOT NULL,
    status integer NOT NULL
);


ALTER TABLE public.jobs OWNER TO pim;

--
-- Name: jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jobs_id_seq OWNER TO pim;

--
-- Name: jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.jobs_id_seq OWNED BY public.jobs.id;


--
-- Name: link_node_refs; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.link_node_refs (
    id integer NOT NULL,
    node_id integer,
    node_path character varying(256) NOT NULL,
    created timestamp without time zone
);


ALTER TABLE public.link_node_refs OWNER TO pim;

--
-- Name: link_node_refs_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.link_node_refs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.link_node_refs_id_seq OWNER TO pim;

--
-- Name: link_node_refs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.link_node_refs_id_seq OWNED BY public.link_node_refs.id;


--
-- Name: links; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.links (
    id integer NOT NULL,
    title character varying(128),
    description text,
    created timestamp without time zone,
    modified timestamp without time zone,
    custom_data json,
    run_id integer NOT NULL,
    from_port_id character varying(256) NOT NULL,
    to_port_id character varying(256) NOT NULL,
    data_type character varying(128),
    common_ancestor character varying(256) NOT NULL
);


ALTER TABLE public.links OWNER TO pim;

--
-- Name: links_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.links_id_seq OWNER TO pim;

--
-- Name: links_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.links_id_seq OWNED BY public.links.id;


--
-- Name: nodes; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.nodes (
    id integer NOT NULL,
    path character varying(256),
    title character varying(128),
    description text,
    created timestamp without time zone,
    modified timestamp without time zone,
    custom_data json,
    type character varying(128),
    parent_id integer,
    run_id integer,
    status integer[],
    level integer
);


ALTER TABLE public.nodes OWNER TO pim;

--
-- Name: nodes_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.nodes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nodes_id_seq OWNER TO pim;

--
-- Name: nodes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.nodes_id_seq OWNED BY public.nodes.id;


--
-- Name: out_ports; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.out_ports (
    id integer NOT NULL,
    path character varying(256) NOT NULL,
    title character varying(128),
    description text,
    created timestamp without time zone,
    modified timestamp without time zone,
    custom_data json,
    node_id integer
);


ALTER TABLE public.out_ports OWNER TO pim;

--
-- Name: out_ports_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.out_ports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.out_ports_id_seq OWNER TO pim;

--
-- Name: out_ports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.out_ports_id_seq OWNED BY public.out_ports.id;


--
-- Name: runs; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.runs (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    title character varying(128),
    description text,
    created timestamp without time zone,
    modified timestamp without time zone,
    custom_data json,
    "user" character varying(128)
);


ALTER TABLE public.runs OWNER TO pim;

--
-- Name: runs_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.runs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.runs_id_seq OWNER TO pim;

--
-- Name: runs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.runs_id_seq OWNED BY public.runs.id;


--
-- Name: update_jobs; Type: TABLE; Schema: public; Owner: pim
--

CREATE TABLE public.update_jobs (
    id integer NOT NULL,
    path character varying(256),
    modified timestamp without time zone,
    node_id integer,
    status integer NOT NULL
);


ALTER TABLE public.update_jobs OWNER TO pim;

--
-- Name: update_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: pim
--

CREATE SEQUENCE public.update_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.update_jobs_id_seq OWNER TO pim;

--
-- Name: update_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pim
--

ALTER SEQUENCE public.update_jobs_id_seq OWNED BY public.update_jobs.id;


--
-- Name: aggregates id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.aggregates ALTER COLUMN id SET DEFAULT nextval('public.aggregates_id_seq'::regclass);


--
-- Name: in_ports id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.in_ports ALTER COLUMN id SET DEFAULT nextval('public.in_ports_id_seq'::regclass);


--
-- Name: jobs id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.jobs ALTER COLUMN id SET DEFAULT nextval('public.jobs_id_seq'::regclass);


--
-- Name: link_node_refs id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.link_node_refs ALTER COLUMN id SET DEFAULT nextval('public.link_node_refs_id_seq'::regclass);


--
-- Name: links id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.links ALTER COLUMN id SET DEFAULT nextval('public.links_id_seq'::regclass);


--
-- Name: nodes id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.nodes ALTER COLUMN id SET DEFAULT nextval('public.nodes_id_seq'::regclass);


--
-- Name: out_ports id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.out_ports ALTER COLUMN id SET DEFAULT nextval('public.out_ports_id_seq'::regclass);


--
-- Name: runs id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.runs ALTER COLUMN id SET DEFAULT nextval('public.runs_id_seq'::regclass);


--
-- Name: update_jobs id; Type: DEFAULT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.update_jobs ALTER COLUMN id SET DEFAULT nextval('public.update_jobs_id_seq'::regclass);


--
-- Data for Name: aggregates; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.aggregates (id, status, node_id) FROM stdin;
1972	{0,0,0,0,0,0}	1972
1980	{0,0,0,0,0,0}	1980
1981	{0,0,0,0,0,0}	1981
1984	{0,0,22,4,33,0}	1984
1989	{0,0,1,0,0,0}	1989
1993	{0,0,2,0,2,0}	1993
1987	{0,0,2,2,0,0}	1987
1970	{0,0,1,0,3,0}	1970
1985	{0,0,2,2,0,0}	1985
2059	{0,0,1,0,0,0}	2059
1971	{0,0,2,2,0,0}	1971
1988	{0,0,0,0,4,0}	1988
2065	{0,0,5,0,0,0}	2065
1983	{0,0,4,0,0,0}	1983
1990	{0,0,2,0,2,0}	1990
2062	{0,0,5,0,0,0}	2062
1979	{0,0,1,0,0,0}	1979
2058	{0,0,34,0,0,0}	2058
2060	{0,0,5,0,0,0}	2060
1992	{0,0,1,0,3,0}	1992
1978	{0,0,1,0,0,0}	1978
1982	{0,0,4,0,0,0}	1982
2063	{0,0,16,0,0,0}	2063
2061	{0,0,1,0,0,0}	2061
1974	{0,0,2,0,2,0}	1974
1991	{0,0,1,0,3,0}	1991
1973	{0,0,1,0,0,0}	1973
2124	{0,0,16,0,0,0}	2124
2126	{0,0,4,0,0,0}	2126
1977	{0,0,2,0,2,0}	1977
1986	{0,0,1,0,3,0}	1986
2127	{0,0,4,0,0,0}	2127
1975	{0,0,0,0,4,0}	1975
2064	{0,0,1,0,0,0}	2064
2125	{0,0,4,0,0,0}	2125
2143	{0,0,4,0,0,0}	2143
2128	{0,0,4,0,0,0}	2128
2157	{0,0,4,0,0,0}	2157
1969	{0,0,2,2,0,0}	1969
1976	{0,0,0,0,4,0}	1976
1968	{0,0,20,4,15,0}	1968
2142	{0,0,4,0,0,0}	2142
2187	{0,0,3,0,0,0}	2187
2139	{0,0,16,0,0,0}	2139
2141	{0,0,4,0,0,0}	2141
2140	{0,0,4,0,0,0}	2140
2155	{0,0,4,0,0,0}	2155
2154	{0,0,16,0,0,0}	2154
2158	{0,0,4,0,0,0}	2158
2156	{0,0,4,0,0,0}	2156
2170	{0,0,4,0,0,0}	2170
2173	{0,0,4,0,0,0}	2173
2172	{0,0,4,0,0,0}	2172
2171	{0,0,4,0,0,0}	2171
2186	{0,0,1,0,0,0}	2186
2169	{0,0,16,0,0,0}	2169
2188	{0,0,3,0,0,0}	2188
2185	{0,0,3,0,0,0}	2185
2189	{0,0,3,0,0,0}	2189
2190	{0,0,3,0,0,0}	2190
2191	{0,0,3,0,0,0}	2191
2192	{0,0,3,0,0,0}	2192
2193	{0,0,3,0,0,0}	2193
2184	{0,0,28,0,0,0}	2184
1994	{0,0,1,0,0,0}	1994
1995	{0,0,1,0,0,0}	1995
1998	{0,0,4,0,0,0}	1998
1999	{0,0,4,0,0,0}	1999
1996	{0,0,0,0,12,0}	1996
1997	{0,0,0,0,4,0}	1997
2002	{0,0,1,0,0,0}	2002
2003	{0,0,1,0,0,0}	2003
2005	{0,0,2,2,0,0}	2008
2004	{1,0,34,4,9,0}	2004
2007	{0,0,2,2,0,0}	2010
2006	{0,0,1,0,3,0}	2009
2010	{0,0,0,0,0,0}	2013
2011	{0,0,0,0,0,0}	2014
2013	{0,0,0,0,0,0}	2016
2025	{0,0,2,0,0,0}	2027
2008	{1,0,0,0,3,0}	2011
2022	{0,0,1,0,0,0}	2007
2030	{0,0,5,0,0,0}	2038
2050	{0,0,4,0,0,0}	2050
2001	{0,0,1,0,0,0}	2001
2029	{0,0,12,0,0,0}	2032
2031	{0,0,5,0,0,0}	2039
2042	{0,0,1,0,0,0}	2045
2015	{0,0,4,0,0,0}	2018
2035	{0,0,5,0,0,0}	2034
2047	{0,0,8,0,0,0}	2047
2055	{0,0,6,0,0,0}	2056
2159	{0,0,16,0,0,0}	2159
2131	{0,0,4,0,0,0}	2131
2049	{0,0,3,0,0,0}	2049
2020	{0,0,1,0,3,0}	2005
2016	{0,0,4,0,0,0}	2019
2051	{0,0,2,0,0,0}	2051
2068	{0,0,10,0,0,0}	2068
2129	{0,0,16,0,0,0}	2129
2021	{0,0,1,0,3,0}	2006
2000	{1,0,40,4,15,0}	2000
2017	{0,0,4,0,0,0}	2020
2018	{0,0,4,0,0,0}	2021
2036	{0,0,0,0,0,0}	2030
2009	{0,0,4,0,0,0}	2012
2012	{0,0,1,0,3,0}	2015
2043	{0,0,0,0,0,0}	2036
2045	{0,0,0,0,0,0}	2026
2014	{0,0,4,0,0,0}	2017
2028	{0,0,22,0,0,0}	2029
2019	{0,0,4,0,0,0}	2022
2033	{0,0,1,0,0,0}	2041
2034	{0,0,5,0,0,0}	2033
2041	{0,0,1,0,0,0}	2044
2069	{0,0,2,0,0,0}	2069
2052	{0,0,58,0,0,0}	2052
2054	{0,0,24,0,0,0}	2055
2163	{0,0,4,0,0,0}	2163
2048	{0,0,6,0,0,0}	2048
2032	{0,0,1,0,0,0}	2040
2053	{0,0,24,0,0,0}	2054
2067	{0,0,2,0,0,0}	2067
2024	{0,0,2,0,0,0}	2024
2046	{0,0,89,0,0,0}	2046
2027	{0,0,5,0,0,0}	2028
2070	{0,0,10,0,0,0}	2070
2040	{0,0,0,0,5,0}	2043
2066	{0,0,24,0,0,0}	2066
2044	{0,0,5,0,0,0}	2037
2160	{0,0,4,0,0,0}	2160
2177	{0,0,4,0,0,0}	2177
2145	{0,0,4,0,0,0}	2145
2038	{0,0,2,0,10,0}	2035
2023	{0,0,36,0,10,0}	2023
2037	{0,0,7,0,10,0}	2031
2026	{0,0,34,0,10,0}	2025
2039	{0,0,0,0,5,0}	2042
2201	{0,0,4,0,0,0}	2201
2133	{0,0,4,0,0,0}	2133
2130	{0,0,4,0,0,0}	2130
2148	{0,0,4,0,0,0}	2148
2147	{0,0,0,4,0,0}	2147
2200	{0,0,12,4,0,0}	2200
2162	{0,0,4,0,0,0}	2162
2175	{0,0,4,0,0,0}	2175
2146	{0,0,0,0,4,0}	2146
2132	{0,0,4,0,0,0}	2132
2144	{0,0,8,4,4,0}	2144
2194	{0,0,3,0,0,0}	2194
2161	{0,0,4,0,0,0}	2161
2178	{0,0,4,0,0,0}	2178
2204	{0,0,4,0,0,0}	2204
2174	{0,0,16,0,0,0}	2174
2176	{0,0,4,0,0,0}	2176
2203	{0,0,4,0,0,0}	2203
2202	{0,0,0,4,0,0}	2202
2214	{0,0,4,0,0,0}	2214
2211	{0,0,4,0,0,0}	2211
2213	{0,0,4,0,0,0}	2213
2212	{0,0,4,0,0,0}	2212
2210	{0,0,16,0,0,0}	2210
2219	{0,0,4,0,0,0}	2219
2216	{0,0,4,0,0,0}	2216
2218	{0,0,4,0,0,0}	2218
2217	{0,0,4,0,0,0}	2217
2215	{0,0,16,0,0,0}	2215
2226	{0,0,4,0,0,0}	2226
2111	{0,0,1,0,0,0}	2111
2236	{0,0,4,0,0,0}	2236
2224	{0,0,4,0,0,0}	2224
2179	{0,0,16,0,0,0}	2179
2056	{0,0,4,0,0,0}	2057
2181	{0,0,4,0,0,0}	2181
2197	{0,0,4,0,0,0}	2197
2195	{0,0,16,0,0,0}	2195
2113	{0,0,2,0,2,0}	2113
2122	{0,0,4,0,0,0}	2122
2199	{0,0,4,0,0,0}	2199
2135	{0,0,4,0,0,0}	2135
2164	{0,0,16,0,0,0}	2164
2166	{0,0,4,0,0,0}	2166
2120	{0,0,1,0,3,0}	2120
2150	{0,0,4,0,0,0}	2150
2057	{0,0,8,0,0,0}	2053
2118	{0,0,2,2,0,0}	2118
2114	{0,0,1,0,3,0}	2114
2165	{0,0,4,0,0,0}	2165
2117	{0,0,2,2,0,0}	2117
2137	{0,0,4,0,0,0}	2137
2183	{0,0,4,0,0,0}	2183
2182	{0,0,4,0,0,0}	2182
2241	{0,0,1,0,0,0}	2241
2243	{0,0,1,0,0,0}	2243
2233	{0,0,4,0,0,0}	2233
2123	{0,0,4,0,0,0}	2123
2238	{0,0,4,0,0,0}	2238
2198	{0,0,4,0,0,0}	2198
2239	{0,0,4,0,0,0}	2239
2223	{0,0,4,0,0,0}	2223
2138	{0,0,4,0,0,0}	2138
2109	{0,0,1,0,0,0}	2109
2230	{0,0,16,0,0,0}	2230
2229	{0,0,4,0,0,0}	2229
2209	{0,0,4,0,0,0}	2209
2232	{0,0,4,0,0,0}	2232
2121	{0,0,1,0,3,0}	2121
2196	{0,0,4,0,0,0}	2196
2153	{0,0,4,0,0,0}	2153
2167	{0,0,4,0,0,0}	2167
2227	{0,0,4,0,0,0}	2227
2152	{0,0,0,4,0,0}	2152
2168	{0,0,4,0,0,0}	2168
2151	{0,0,0,0,4,0}	2151
2112	{0,0,2,0,2,0}	2112
2205	{0,0,16,0,0,0}	2205
2149	{0,0,8,4,4,0}	2149
2110	{0,0,1,0,0,0}	2110
2207	{0,0,4,0,0,0}	2207
2115	{0,0,9,0,3,0}	2115
2180	{0,0,4,0,0,0}	2180
2136	{0,0,4,0,0,0}	2136
2116	{0,0,1,0,3,0}	2116
2108	{0,0,33,4,22,0}	2108
2119	{0,0,1,0,3,0}	2119
2134	{0,0,16,0,0,0}	2134
2208	{0,0,4,0,0,0}	2208
2206	{0,0,4,0,0,0}	2206
2225	{0,0,16,0,0,0}	2225
2228	{0,0,4,0,0,0}	2228
2221	{0,0,4,0,0,0}	2221
2222	{0,0,4,0,0,0}	2222
2220	{0,0,16,0,0,0}	2220
2231	{0,0,4,0,0,0}	2231
2245	{0,0,4,0,0,0}	2248
2234	{0,0,4,0,0,0}	2234
2242	{0,0,1,0,0,0}	2242
2240	{0,0,48,5,19,0}	2240
2244	{0,0,44,5,11,0}	2244
2246	{0,0,4,0,0,0}	2249
2247	{0,0,4,0,0,0}	2250
2237	{0,0,4,0,0,0}	2237
2235	{0,0,16,0,0,0}	2235
2248	{0,0,4,0,0,0}	2251
2249	{0,0,4,0,0,0}	2252
2250	{0,0,4,0,0,0}	2253
2261	{0,0,0,0,4,0}	2246
2262	{0,0,1,0,0,0}	2247
2282	{0,0,5,0,0,0}	2282
2314	{0,0,2,0,2,0}	2314
2254	{0,0,2,2,0,0}	2257
2287	{0,0,1,0,0,0}	2287
2311	{0,0,1,0,3,0}	2311
2297	{0,0,2,0,0,0}	2297
2258	{0,0,4,0,0,0}	2261
2255	{0,0,0,1,3,0}	2258
2270	{0,0,2,0,0,0}	2270
2256	{0,0,0,0,4,0}	2259
2316	{0,0,1,0,0,0}	2316
2291	{0,0,3,0,0,0}	2291
2266	{0,0,1,0,0,0}	2266
2313	{0,0,1,0,0,0}	2313
2259	{0,0,4,0,0,0}	2262
2265	{0,0,5,0,0,0}	2265
2306	{0,0,2,2,0,0}	2306
2257	{0,0,0,0,4,0}	2260
2288	{0,0,3,0,0,0}	2288
2264	{0,0,2,0,0,0}	2264
2278	{0,0,8,0,0,0}	2274
2307	{0,0,4,0,0,0}	2307
2279	{0,0,8,0,0,0}	2275
2268	{0,0,89,0,0,0}	2268
2271	{0,0,3,0,0,0}	2271
2251	{0,0,4,0,0,0}	2254
2305	{0,0,1,0,0,0}	2305
2293	{0,0,3,0,0,0}	2293
2312	{0,0,2,0,2,0}	2312
2273	{0,0,58,0,0,0}	2273
2277	{0,0,24,0,0,0}	2279
2286	{0,0,3,0,0,0}	2286
2304	{0,0,9,0,3,0}	2304
2275	{0,0,4,0,0,0}	2277
2302	{0,0,1,0,3,0}	2302
2252	{0,0,4,0,0,0}	2255
2285	{0,0,28,0,0,0}	2285
2295	{0,0,3,0,0,0}	2295
2253	{0,0,2,2,0,0}	2256
2269	{0,0,6,0,0,0}	2269
2283	{0,0,1,0,0,0}	2283
2276	{0,0,24,0,0,0}	2278
2290	{0,0,3,0,0,0}	2290
2272	{0,0,4,0,0,0}	2272
2267	{0,0,5,0,0,0}	2267
2263	{0,0,13,0,0,0}	2263
2260	{0,0,0,0,4,0}	2245
2303	{0,0,4,0,0,0}	2303
2315	{0,0,1,0,3,0}	2315
2280	{0,0,13,0,0,0}	2280
2281	{0,0,2,0,0,0}	2281
2284	{0,0,5,0,0,0}	2284
2274	{0,0,6,0,0,0}	2276
2292	{0,0,3,0,0,0}	2292
2310	{0,0,1,0,3,0}	2310
2300	{0,0,0,0,5,0}	2300
2301	{0,0,33,4,22,0}	2301
2317	{0,0,0,0,0,0}	2317
2318	{0,0,0,0,0,0}	2318
2308	{0,0,2,2,0,0}	2308
2299	{0,0,0,0,5,0}	2299
2294	{0,0,3,0,0,0}	2294
2296	{0,0,3,0,10,0}	2296
2298	{0,0,1,0,0,0}	2298
2289	{0,0,3,0,0,0}	2289
2319	{0,0,0,0,0,0}	2330
2320	{0,0,0,0,0,0}	2331
2321	{0,0,0,0,0,0}	2332
2309	{0,0,1,0,3,0}	2309
2322	{0,0,0,0,0,0}	2319
2323	{0,0,0,0,0,0}	2333
2324	{0,0,0,0,0,0}	2320
2325	{0,0,0,0,0,0}	2334
2326	{0,0,0,0,0,0}	2335
2327	{0,0,0,0,0,0}	2336
2328	{0,0,0,0,0,0}	2321
2329	{0,0,0,0,0,0}	2337
2330	{0,0,0,0,0,0}	2322
2331	{0,0,0,0,0,0}	2338
2332	{0,0,0,0,0,0}	2323
2333	{0,0,0,0,0,0}	2339
2334	{0,0,0,0,0,0}	2340
2335	{0,0,0,0,0,0}	2341
2336	{0,0,0,0,0,0}	2342
2337	{0,0,0,0,0,0}	2343
2338	{0,0,0,0,0,0}	2344
2339	{0,0,0,0,0,0}	2345
2340	{0,0,0,0,0,0}	2346
2341	{0,0,0,0,0,0}	2324
2342	{0,0,0,0,0,0}	2347
2343	{0,0,0,0,0,0}	2348
2344	{0,0,0,0,0,0}	2349
2345	{0,0,0,0,0,0}	2350
2346	{0,0,0,0,0,0}	2351
2347	{0,0,0,0,0,0}	2352
2348	{0,0,0,0,0,0}	2325
2349	{0,0,0,0,0,0}	2353
2350	{0,0,0,0,0,0}	2326
2351	{0,0,0,0,0,0}	2354
2352	{0,0,0,0,0,0}	2327
2353	{0,0,0,0,0,0}	2355
2354	{0,0,0,0,0,0}	2328
2355	{0,0,0,0,0,0}	2356
2356	{0,0,0,0,0,0}	2329
2357	{0,0,0,0,0,0}	2357
2379	{4,0,177,3,2,0}	2379
2368	{0,0,1,0,3,0}	2368
2383	{0,0,2,0,0,0}	2390
2385	{0,0,2,0,0,0}	2392
2370	{0,0,2,2,0,0}	2370
2375	{0,0,1,0,0,0}	2375
2411	{0,0,2,0,0,0}	2414
2384	{0,0,2,0,0,0}	2391
2365	{0,0,4,0,0,0}	2365
2374	{0,0,2,0,1,0}	2374
2376	{0,0,1,0,1,0}	2376
2372	{0,0,1,0,0,0}	2372
2367	{0,0,1,0,0,0}	2367
2364	{0,0,1,0,0,0}	2364
2362	{0,0,4,0,0,0}	2362
2359	{0,0,9,0,3,0}	2359
2358	{0,0,33,4,22,0}	2358
2369	{0,0,1,0,3,0}	2369
2371	{0,0,1,0,3,0}	2371
2366	{0,0,2,2,0,0}	2366
2360	{0,0,2,0,2,0}	2360
2377	{0,0,0,0,0,0}	2377
2378	{0,0,0,0,0,0}	2378
2373	{0,0,2,0,2,0}	2373
2361	{0,0,1,0,3,0}	2361
2363	{0,0,1,0,3,0}	2363
2387	{0,0,2,0,0,0}	2394
2399	{0,0,2,0,0,0}	2405
2400	{0,0,2,0,0,0}	2406
2403	{0,0,2,0,0,0}	2408
2388	{0,0,2,0,0,0}	2395
2407	{0,0,2,0,0,0}	2383
2405	{0,0,2,0,0,0}	2410
2415	{0,0,57,1,1,0}	2385
2410	{0,0,2,0,0,0}	2413
2394	{0,0,2,1,0,0}	2401
2382	{0,0,2,0,0,0}	2389
2412	{0,0,2,0,0,0}	2415
2395	{0,0,2,0,0,0}	2402
2386	{0,0,2,0,0,0}	2393
2393	{0,0,2,0,0,0}	2400
2390	{0,0,2,0,0,0}	2397
2416	{0,0,2,0,0,0}	2418
2397	{0,0,2,0,0,0}	2404
2392	{0,0,2,0,0,0}	2399
2381	{0,0,2,0,0,0}	2388
2414	{0,0,2,0,0,0}	2417
2406	{0,0,2,0,0,0}	2411
2404	{0,0,2,0,0,0}	2409
2417	{0,0,2,0,0,0}	2419
2401	{0,0,2,0,0,0}	2407
2418	{0,0,2,0,0,0}	2420
2380	{0,0,34,1,0,0}	2380
2389	{0,0,2,0,0,0}	2396
2413	{0,0,2,0,0,0}	2416
2396	{0,0,2,0,0,0}	2403
2408	{0,0,2,0,0,0}	2412
2391	{0,0,2,0,0,0}	2398
2402	{0,0,8,0,0,0}	2382
2409	{0,0,10,0,0,0}	2384
2398	{0,0,6,0,0,0}	2381
2434	{0,0,2,0,0,0}	2436
2452	{0,0,2,0,0,0}	2453
2468	{0,0,2,0,0,0}	2469
2436	{0,0,2,0,0,0}	2438
2466	{0,0,2,0,0,0}	2467
2463	{0,0,2,0,0,0}	2464
2441	{0,0,2,0,0,0}	2443
2451	{0,0,2,0,0,0}	2452
2420	{0,0,2,0,0,0}	2422
2455	{0,0,2,0,0,0}	2456
2473	{0,0,2,0,0,0}	2474
2431	{0,0,2,0,0,0}	2433
2462	{0,0,26,0,0,0}	2459
2464	{0,0,2,0,0,0}	2465
2427	{0,0,2,0,0,0}	2429
2484	{0,0,2,0,0,0}	2484
2424	{0,0,2,0,0,0}	2426
2446	{0,0,2,0,0,0}	2447
2482	{0,0,2,0,0,0}	2482
2438	{0,0,2,0,0,0}	2440
2485	{0,0,5,0,0,0}	2485
2429	{0,0,2,0,0,0}	2431
2439	{0,0,2,0,0,0}	2441
2422	{0,0,2,0,0,0}	2424
2479	{0,0,2,0,0,0}	2479
2481	{3,0,0,0,0,0}	2481
2476	{4,0,9,0,1,0}	2460
2477	{0,0,2,0,0,0}	2477
2443	{0,0,2,0,0,0}	2445
2447	{0,0,2,0,0,0}	2448
2449	{0,0,2,0,0,0}	2450
2470	{0,0,2,0,0,0}	2471
2472	{0,0,2,0,0,0}	2473
2471	{0,0,2,0,0,0}	2472
2487	{0,0,5,0,0,0}	2487
2426	{0,0,2,0,0,0}	2428
2459	{0,0,3,1,0,0}	2458
2456	{4,0,40,1,1,0}	2387
2450	{0,0,2,0,0,0}	2451
2461	{0,0,1,1,0,0}	2463
2480	{0,0,2,0,0,0}	2480
2478	{1,0,1,0,1,0}	2478
2432	{0,0,2,0,0,0}	2434
2469	{0,0,2,0,0,0}	2470
2440	{0,0,2,0,0,0}	2442
2448	{0,0,2,0,0,0}	2449
2465	{0,0,2,0,0,0}	2466
2421	{0,0,2,0,0,0}	2423
2454	{0,0,2,0,0,0}	2455
2460	{0,0,2,0,0,0}	2462
2483	{0,0,13,0,0,0}	2483
2486	{0,0,1,0,0,0}	2486
2419	{0,0,2,1,0,0}	2421
2430	{0,0,2,0,0,0}	2432
2444	{0,0,2,0,0,0}	2446
2433	{0,0,2,0,0,0}	2435
2453	{0,0,2,0,0,0}	2454
2475	{0,0,2,0,0,0}	2476
2435	{0,0,2,0,0,0}	2437
2445	{0,0,20,0,0,0}	2386
2457	{0,0,2,0,0,0}	2457
2458	{0,0,2,0,0,0}	2461
2425	{0,0,2,0,0,0}	2427
2467	{0,0,2,0,0,0}	2468
2437	{0,0,2,0,0,0}	2439
2474	{0,0,2,0,0,0}	2475
2428	{0,0,2,0,0,0}	2430
2442	{0,0,2,0,0,0}	2444
2423	{0,0,1,0,1,0}	2425
\.


--
-- Name: aggregates_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.aggregates_id_seq', 2487, true);


--
-- Data for Name: in_ports; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.in_ports (id, path, title, description, created, modified, custom_data, node_id) FROM stdin;
5853	add_ints_2018-09-13T15-55-06/root/source/input	input	\N	2018-09-13 13:55:07.010075	2018-09-13 13:55:07.010081	{"datatype": "Int", "dimension_names": ["source"]}	2484
5854	add_ints_2018-09-13T15-55-06/root/source/v_in_port	•••	Virtual input port	2018-09-13 13:55:07.011425	2018-09-13 13:55:07.011429	null	2484
5855	add_ints_2018-09-13T15-55-06/root/add/left_hand	left_hand	\N	2018-09-13 13:55:07.012085	2018-09-13 13:55:07.012089	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2485
5856	add_ints_2018-09-13T15-55-06/root/add/right_hand	right_hand	\N	2018-09-13 13:55:07.012673	2018-09-13 13:55:07.012677	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_add_right_hand_0"]}	2485
5857	add_ints_2018-09-13T15-55-06/root/add/v_in_port	•••	Virtual input port	2018-09-13 13:55:07.01318	2018-09-13 13:55:07.013184	null	2485
5858	add_ints_2018-09-13T15-55-06/root/const_add_right_hand_0/v_in_port	•••	Virtual input port	2018-09-13 13:55:07.013887	2018-09-13 13:55:07.013891	null	2486
5859	add_ints_2018-09-13T15-55-06/root/sink/input	input	\N	2018-09-13 13:55:07.014384	2018-09-13 13:55:07.014388	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2487
5860	add_ints_2018-09-13T15-55-06/root/sink/v_in_port	•••	Virtual input port	2018-09-13 13:55:07.014894	2018-09-13 13:55:07.014897	null	2487
5039	example_2018-09-03T10-32-43/root/source1/input	input	\N	2018-09-03 08:32:43.884102	2018-09-03 08:32:43.884108	{"datatype": "Int", "dimension_names": ["source1"]}	2125
5040	example_2018-09-03T10-32-43/root/source1/v_in_port	•••	Virtual input port	2018-09-03 08:32:43.885161	2018-09-03 08:32:43.885165	null	2125
5041	example_2018-09-03T10-32-43/root/sink1/input	input	\N	2018-09-03 08:32:43.885765	2018-09-03 08:32:43.885769	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2126
5042	example_2018-09-03T10-32-43/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 08:32:43.886457	2018-09-03 08:32:43.886461	null	2126
5527	iris/root/0_statistics/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.875992	2018-09-08 13:18:12.876	null	2380
5043	example_2018-09-03T10-32-43/root/addint/left_hand	left_hand	\N	2018-09-03 08:32:43.887174	2018-09-03 08:32:43.887178	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2127
5044	example_2018-09-03T10-32-43/root/addint/right_hand	right_hand	\N	2018-09-03 08:32:43.887797	2018-09-03 08:32:43.887801	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2127
5045	example_2018-09-03T10-32-43/root/addint/v_in_port	•••	Virtual input port	2018-09-03 08:32:43.888351	2018-09-03 08:32:43.888355	null	2127
5046	example_2018-09-03T10-32-43/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 08:32:43.88879	2018-09-03 08:32:43.888794	null	2128
5047	example_2018-09-03T10-36-42/root/source1/input	input	\N	2018-09-03 08:36:43.187274	2018-09-03 08:36:43.187279	{"datatype": "Int", "dimension_names": ["source1"]}	2130
5048	example_2018-09-03T10-36-42/root/source1/v_in_port	•••	Virtual input port	2018-09-03 08:36:43.187764	2018-09-03 08:36:43.187768	null	2130
5049	example_2018-09-03T10-36-42/root/sink1/input	input	\N	2018-09-03 08:36:43.188185	2018-09-03 08:36:43.188188	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2131
5050	example_2018-09-03T10-36-42/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 08:36:43.188651	2018-09-03 08:36:43.188655	null	2131
5051	example_2018-09-03T10-36-42/root/addint/left_hand	left_hand	\N	2018-09-03 08:36:43.189067	2018-09-03 08:36:43.189072	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2132
5052	example_2018-09-03T10-36-42/root/addint/right_hand	right_hand	\N	2018-09-03 08:36:43.189555	2018-09-03 08:36:43.189559	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2132
5053	example_2018-09-03T10-36-42/root/addint/v_in_port	•••	Virtual input port	2018-09-03 08:36:43.190136	2018-09-03 08:36:43.19014	null	2132
5054	example_2018-09-03T10-36-42/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 08:36:43.190651	2018-09-03 08:36:43.190655	null	2133
5055	example_2018-09-03T10-37-58/root/source1/input	input	\N	2018-09-03 08:37:59.165274	2018-09-03 08:37:59.16528	{"datatype": "Int", "dimension_names": ["source1"]}	2135
5056	example_2018-09-03T10-37-58/root/source1/v_in_port	•••	Virtual input port	2018-09-03 08:37:59.1658	2018-09-03 08:37:59.165804	null	2135
5057	example_2018-09-03T10-37-58/root/sink1/input	input	\N	2018-09-03 08:37:59.166205	2018-09-03 08:37:59.166208	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2136
5058	example_2018-09-03T10-37-58/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 08:37:59.166711	2018-09-03 08:37:59.166715	null	2136
5059	example_2018-09-03T10-37-58/root/addint/left_hand	left_hand	\N	2018-09-03 08:37:59.167152	2018-09-03 08:37:59.167156	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2137
5060	example_2018-09-03T10-37-58/root/addint/right_hand	right_hand	\N	2018-09-03 08:37:59.167604	2018-09-03 08:37:59.167608	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2137
5061	example_2018-09-03T10-37-58/root/addint/v_in_port	•••	Virtual input port	2018-09-03 08:37:59.168087	2018-09-03 08:37:59.168091	null	2137
5062	example_2018-09-03T10-37-58/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 08:37:59.16859	2018-09-03 08:37:59.168594	null	2138
5063	example_2018-09-03T10-41-42/root/source1/input	input	\N	2018-09-03 08:41:42.76744	2018-09-03 08:41:42.767445	{"datatype": "Int", "dimension_names": ["source1"]}	2140
5064	example_2018-09-03T10-41-42/root/source1/v_in_port	•••	Virtual input port	2018-09-03 08:41:42.767961	2018-09-03 08:41:42.767965	null	2140
5065	example_2018-09-03T10-41-42/root/sink1/input	input	\N	2018-09-03 08:41:42.768434	2018-09-03 08:41:42.768438	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2141
5066	example_2018-09-03T10-41-42/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 08:41:42.768889	2018-09-03 08:41:42.768892	null	2141
5067	example_2018-09-03T10-41-42/root/addint/left_hand	left_hand	\N	2018-09-03 08:41:42.769297	2018-09-03 08:41:42.769317	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2142
5068	example_2018-09-03T10-41-42/root/addint/right_hand	right_hand	\N	2018-09-03 08:41:42.769779	2018-09-03 08:41:42.769783	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2142
5069	example_2018-09-03T10-41-42/root/addint/v_in_port	•••	Virtual input port	2018-09-03 08:41:42.77024	2018-09-03 08:41:42.770244	null	2142
5070	example_2018-09-03T10-41-42/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 08:41:42.770685	2018-09-03 08:41:42.770689	null	2143
5071	example_2018-09-03T10-47-39/root/source1/input	input	\N	2018-09-03 08:47:39.88812	2018-09-03 08:47:39.888125	{"datatype": "Int", "dimension_names": ["source1"]}	2145
5072	example_2018-09-03T10-47-39/root/source1/v_in_port	•••	Virtual input port	2018-09-03 08:47:39.888828	2018-09-03 08:47:39.888832	null	2145
5073	example_2018-09-03T10-47-39/root/sink1/input	input	\N	2018-09-03 08:47:39.88936	2018-09-03 08:47:39.889363	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2146
5074	example_2018-09-03T10-47-39/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 08:47:39.889826	2018-09-03 08:47:39.88983	null	2146
5075	example_2018-09-03T10-47-39/root/addint/left_hand	left_hand	\N	2018-09-03 08:47:39.890214	2018-09-03 08:47:39.890217	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2147
5076	example_2018-09-03T10-47-39/root/addint/right_hand	right_hand	\N	2018-09-03 08:47:39.890762	2018-09-03 08:47:39.890766	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2147
5077	example_2018-09-03T10-47-39/root/addint/v_in_port	•••	Virtual input port	2018-09-03 08:47:39.891351	2018-09-03 08:47:39.891354	null	2147
5078	example_2018-09-03T10-47-39/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 08:47:39.891778	2018-09-03 08:47:39.891782	null	2148
5079	example_2018-09-03T11-01-12/root/source1/input	input	\N	2018-09-03 09:01:12.301219	2018-09-03 09:01:12.301225	{"datatype": "Int", "dimension_names": ["source1"]}	2150
5080	example_2018-09-03T11-01-12/root/source1/v_in_port	•••	Virtual input port	2018-09-03 09:01:12.301887	2018-09-03 09:01:12.301891	null	2150
5081	example_2018-09-03T11-01-12/root/sink1/input	input	\N	2018-09-03 09:01:12.302447	2018-09-03 09:01:12.30245	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2151
5082	example_2018-09-03T11-01-12/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 09:01:12.302886	2018-09-03 09:01:12.30289	null	2151
5083	example_2018-09-03T11-01-12/root/addint/left_hand	left_hand	\N	2018-09-03 09:01:12.303316	2018-09-03 09:01:12.30332	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2152
5084	example_2018-09-03T11-01-12/root/addint/right_hand	right_hand	\N	2018-09-03 09:01:12.303776	2018-09-03 09:01:12.30378	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2152
5085	example_2018-09-03T11-01-12/root/addint/v_in_port	•••	Virtual input port	2018-09-03 09:01:12.304271	2018-09-03 09:01:12.304275	null	2152
5086	example_2018-09-03T11-01-12/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 09:01:12.304802	2018-09-03 09:01:12.304806	null	2153
5087	example_2018-09-03T11-07-53/root/source1/input	input	\N	2018-09-03 09:07:54.388791	2018-09-03 09:07:54.388796	{"datatype": "Int", "dimension_names": ["source1"]}	2155
5088	example_2018-09-03T11-07-53/root/source1/v_in_port	•••	Virtual input port	2018-09-03 09:07:54.389388	2018-09-03 09:07:54.389392	null	2155
5089	example_2018-09-03T11-07-53/root/sink1/input	input	\N	2018-09-03 09:07:54.389853	2018-09-03 09:07:54.389857	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2156
5090	example_2018-09-03T11-07-53/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 09:07:54.390326	2018-09-03 09:07:54.390332	null	2156
5091	example_2018-09-03T11-07-53/root/addint/left_hand	left_hand	\N	2018-09-03 09:07:54.390829	2018-09-03 09:07:54.390833	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2157
5092	example_2018-09-03T11-07-53/root/addint/right_hand	right_hand	\N	2018-09-03 09:07:54.391268	2018-09-03 09:07:54.391272	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2157
5093	example_2018-09-03T11-07-53/root/addint/v_in_port	•••	Virtual input port	2018-09-03 09:07:54.391757	2018-09-03 09:07:54.391761	null	2157
5094	example_2018-09-03T11-07-53/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 09:07:54.392166	2018-09-03 09:07:54.39217	null	2158
5095	example_2018-09-03T11-12-21/root/source1/input	input	\N	2018-09-03 09:12:21.58432	2018-09-03 09:12:21.584325	{"datatype": "Int", "dimension_names": ["source1"]}	2160
5096	example_2018-09-03T11-12-21/root/source1/v_in_port	•••	Virtual input port	2018-09-03 09:12:21.584874	2018-09-03 09:12:21.584878	null	2160
5097	example_2018-09-03T11-12-21/root/sink1/input	input	\N	2018-09-03 09:12:21.585442	2018-09-03 09:12:21.585447	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2161
5098	example_2018-09-03T11-12-21/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 09:12:21.585901	2018-09-03 09:12:21.585905	null	2161
5099	example_2018-09-03T11-12-21/root/addint/left_hand	left_hand	\N	2018-09-03 09:12:21.586374	2018-09-03 09:12:21.586378	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2162
5100	example_2018-09-03T11-12-21/root/addint/right_hand	right_hand	\N	2018-09-03 09:12:21.586839	2018-09-03 09:12:21.586843	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2162
5101	example_2018-09-03T11-12-21/root/addint/v_in_port	•••	Virtual input port	2018-09-03 09:12:21.587383	2018-09-03 09:12:21.587387	null	2162
5102	example_2018-09-03T11-12-21/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 09:12:21.587884	2018-09-03 09:12:21.587887	null	2163
5103	example_2018-09-03T11-13-16/root/source1/input	input	\N	2018-09-03 09:13:16.476101	2018-09-03 09:13:16.476106	{"datatype": "Int", "dimension_names": ["source1"]}	2165
5104	example_2018-09-03T11-13-16/root/source1/v_in_port	•••	Virtual input port	2018-09-03 09:13:16.476695	2018-09-03 09:13:16.476699	null	2165
5105	example_2018-09-03T11-13-16/root/sink1/input	input	\N	2018-09-03 09:13:16.477416	2018-09-03 09:13:16.47742	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2166
5106	example_2018-09-03T11-13-16/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 09:13:16.477904	2018-09-03 09:13:16.477907	null	2166
4833	failing_macro_top_level_2018-08-22T12-15-04/root/sink/input	input	\N	2018-08-22 10:15:04.864652	2018-08-22 10:15:04.864656	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_a"]}	2006
4834	failing_macro_top_level_2018-08-22T12-15-04/root/sink/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.865127	2018-08-22 10:15:04.865131	null	2006
4835	failing_macro_top_level_2018-08-22T12-15-04/root/const_add_right_hand_0/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.865629	2018-08-22 10:15:04.865633	null	2007
4849	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.152974	2018-08-22 10:36:33.152978	null	2039
4850	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/const_addint1_right_hand_0/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.153529	2018-08-22 10:36:33.153533	null	2040
4851	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/const_addint2_right_hand_0/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.154001	2018-08-22 10:36:33.154005	null	2041
4852	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink/input	input	\N	2018-08-22 10:36:33.154455	2018-08-22 10:36:33.154459	{"datatype": "Int", "input_group": "default", "dimension_names": ["input"]}	2033
4853	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.155019	2018-08-22 10:36:33.155023	null	2033
4854	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input/input	input	\N	2018-08-22 10:36:33.155488	2018-08-22 10:36:33.155491	{"datatype": "Int", "dimension_names": ["input"]}	2034
4855	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.155988	2018-08-22 10:36:33.155992	null	2034
4856	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/output_value/input	input	\N	2018-08-22 10:36:33.156435	2018-08-22 10:36:33.156454	{"datatype": "Int", "input_group": "default", "dimension_names": []}	2030
4857	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/output_value/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.157009	2018-08-22 10:36:33.157012	null	2030
4858	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.157531	2018-08-22 10:36:33.157534	null	2031
4859	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.158027	2018-08-22 10:36:33.158031	null	2035
4860	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1/left_hand	left_hand	\N	2018-08-22 10:36:33.158411	2018-08-22 10:36:33.158415	{"datatype": "Int", "input_group": "default", "dimension_names": ["input"]}	2042
4861	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1/right_hand	right_hand	\N	2018-08-22 10:36:33.158805	2018-08-22 10:36:33.158808	{"datatype": "Int", "input_group": "default", "dimension_names": ["const_addint1_right_hand_0"]}	2042
4862	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.159187	2018-08-22 10:36:33.15919	null	2042
4863	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2/left_hand	left_hand	\N	2018-08-22 10:36:33.159577	2018-08-22 10:36:33.15958	{"datatype": "Int", "input_group": "default", "dimension_names": ["input"]}	2043
4864	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2/right_hand	right_hand	\N	2018-08-22 10:36:33.160002	2018-08-22 10:36:33.160005	{"datatype": "Int", "input_group": "default", "dimension_names": ["const_addint2_right_hand_0"]}	2043
4865	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.160541	2018-08-22 10:36:33.160545	null	2043
4866	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/const_addint1_right_hand_0/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.161155	2018-08-22 10:36:33.161159	null	2044
4867	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/const_addint2_right_hand_0/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.161577	2018-08-22 10:36:33.16158	null	2045
4868	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/macro_sink/input	input	\N	2018-08-22 10:36:33.162034	2018-08-22 10:36:33.162038	{"datatype": "Int", "input_group": "default", "dimension_names": ["input"]}	2036
4869	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/macro_sink/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.162462	2018-08-22 10:36:33.162466	null	2036
5763	iris/root/6_qc_overview/123_t1_with_wm_outline/in_image	in_image		2018-09-08 13:18:12.99465	2018-09-08 13:18:12.994653	{}	2455
5107	example_2018-09-03T11-13-16/root/addint/left_hand	left_hand	\N	2018-09-03 09:13:16.478367	2018-09-03 09:13:16.478371	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2167
5108	example_2018-09-03T11-13-16/root/addint/right_hand	right_hand	\N	2018-09-03 09:13:16.478835	2018-09-03 09:13:16.478838	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2167
5109	example_2018-09-03T11-13-16/root/addint/v_in_port	•••	Virtual input port	2018-09-03 09:13:16.479328	2018-09-03 09:13:16.479332	null	2167
5110	example_2018-09-03T11-13-16/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 09:13:16.47983	2018-09-03 09:13:16.479834	null	2168
5111	example_2018-09-03T11-14-49/root/source1/input	input	\N	2018-09-03 09:14:49.304027	2018-09-03 09:14:49.304031	{"datatype": "Int", "dimension_names": ["source1"]}	2170
5112	example_2018-09-03T11-14-49/root/source1/v_in_port	•••	Virtual input port	2018-09-03 09:14:49.304478	2018-09-03 09:14:49.304483	null	2170
5113	example_2018-09-03T11-14-49/root/sink1/input	input	\N	2018-09-03 09:14:49.304885	2018-09-03 09:14:49.304889	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2171
5114	example_2018-09-03T11-14-49/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 09:14:49.305254	2018-09-03 09:14:49.305257	null	2171
5115	example_2018-09-03T11-14-49/root/addint/left_hand	left_hand	\N	2018-09-03 09:14:49.305646	2018-09-03 09:14:49.305649	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2172
5116	example_2018-09-03T11-14-49/root/addint/right_hand	right_hand	\N	2018-09-03 09:14:49.306162	2018-09-03 09:14:49.306166	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2172
5117	example_2018-09-03T11-14-49/root/addint/v_in_port	•••	Virtual input port	2018-09-03 09:14:49.306658	2018-09-03 09:14:49.306662	null	2172
5118	example_2018-09-03T11-14-49/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 09:14:49.307205	2018-09-03 09:14:49.307224	null	2173
5119	example_2018-09-03T11-17-35/root/source1/input	input	\N	2018-09-03 09:17:35.581479	2018-09-03 09:17:35.581485	{"datatype": "Int", "dimension_names": ["source1"]}	2175
5120	example_2018-09-03T11-17-35/root/source1/v_in_port	•••	Virtual input port	2018-09-03 09:17:35.58197	2018-09-03 09:17:35.581974	null	2175
5121	example_2018-09-03T11-17-35/root/sink1/input	input	\N	2018-09-03 09:17:35.582383	2018-09-03 09:17:35.582387	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2176
5122	example_2018-09-03T11-17-35/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 09:17:35.582784	2018-09-03 09:17:35.582788	null	2176
5123	example_2018-09-03T11-17-35/root/addint/left_hand	left_hand	\N	2018-09-03 09:17:35.583165	2018-09-03 09:17:35.583169	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2177
5124	example_2018-09-03T11-17-35/root/addint/right_hand	right_hand	\N	2018-09-03 09:17:35.583584	2018-09-03 09:17:35.583588	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2177
5125	example_2018-09-03T11-17-35/root/addint/v_in_port	•••	Virtual input port	2018-09-03 09:17:35.584	2018-09-03 09:17:35.584004	null	2177
5126	example_2018-09-03T11-17-35/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 09:17:35.584383	2018-09-03 09:17:35.584387	null	2178
5127	example_2018-09-03T11-31-38/root/source1/input	input	\N	2018-09-03 09:31:39.074788	2018-09-03 09:31:39.074793	{"datatype": "Int", "dimension_names": ["source1"]}	2180
5128	example_2018-09-03T11-31-38/root/source1/v_in_port	•••	Virtual input port	2018-09-03 09:31:39.075474	2018-09-03 09:31:39.075478	null	2180
5129	example_2018-09-03T11-31-38/root/sink1/input	input	\N	2018-09-03 09:31:39.07598	2018-09-03 09:31:39.075984	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2181
5130	example_2018-09-03T11-31-38/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 09:31:39.07649	2018-09-03 09:31:39.076494	null	2181
5131	example_2018-09-03T11-31-38/root/addint/left_hand	left_hand	\N	2018-09-03 09:31:39.076968	2018-09-03 09:31:39.076972	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2182
5132	example_2018-09-03T11-31-38/root/addint/right_hand	right_hand	\N	2018-09-03 09:31:39.077473	2018-09-03 09:31:39.077476	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2182
5133	example_2018-09-03T11-31-38/root/addint/v_in_port	•••	Virtual input port	2018-09-03 09:31:39.077947	2018-09-03 09:31:39.07795	null	2182
5134	example_2018-09-03T11-31-38/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 09:31:39.078357	2018-09-03 09:31:39.078361	null	2183
5135	auto_prefix_test_2018-09-03T11-34-05/root/const/v_in_port	•••	Virtual input port	2018-09-03 09:34:06.648055	2018-09-03 09:34:06.64806	null	2185
5136	auto_prefix_test_2018-09-03T11-34-05/root/source/input	input	\N	2018-09-03 09:34:06.648495	2018-09-03 09:34:06.648499	{"datatype": "Int", "dimension_names": ["source"]}	2186
5137	auto_prefix_test_2018-09-03T11-34-05/root/source/v_in_port	•••	Virtual input port	2018-09-03 09:34:06.64902	2018-09-03 09:34:06.649023	null	2186
5138	auto_prefix_test_2018-09-03T11-34-05/root/m_p/left_hand	left_hand	\N	2018-09-03 09:34:06.649432	2018-09-03 09:34:06.649436	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2187
5139	auto_prefix_test_2018-09-03T11-34-05/root/m_p/right_hand	right_hand	\N	2018-09-03 09:34:06.649896	2018-09-03 09:34:06.6499	{"input_group": "default", "datatype": "Int", "dimension_names": ["const"]}	2187
5140	auto_prefix_test_2018-09-03T11-34-05/root/m_p/v_in_port	•••	Virtual input port	2018-09-03 09:34:06.650422	2018-09-03 09:34:06.650426	null	2187
5141	auto_prefix_test_2018-09-03T11-34-05/root/a_p/left_hand	left_hand	\N	2018-09-03 09:34:06.650884	2018-09-03 09:34:06.650888	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2188
5142	auto_prefix_test_2018-09-03T11-34-05/root/a_p/right_hand	right_hand	\N	2018-09-03 09:34:06.651375	2018-09-03 09:34:06.651378	{"input_group": "default", "datatype": "Int", "dimension_names": ["const"]}	2188
5143	auto_prefix_test_2018-09-03T11-34-05/root/a_p/v_in_port	•••	Virtual input port	2018-09-03 09:34:06.651861	2018-09-03 09:34:06.651865	null	2188
4712	failing_network_2018-08-22T11-29-01/root/step_1/in_1	in_1	\N	2018-08-22 09:29:02.271074	2018-08-22 09:29:02.271083	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1969
4713	failing_network_2018-08-22T11-29-01/root/step_1/in_2	in_2	\N	2018-08-22 09:29:02.27267	2018-08-22 09:29:02.272676	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_2"]}	1969
4714	failing_network_2018-08-22T11-29-01/root/step_1/fail_1	fail_1	\N	2018-08-22 09:29:02.27321	2018-08-22 09:29:02.273215	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	1969
4715	failing_network_2018-08-22T11-29-01/root/step_1/fail_2	fail_2	\N	2018-08-22 09:29:02.273804	2018-08-22 09:29:02.273808	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["const_step_1_fail_2_0"]}	1969
4716	failing_network_2018-08-22T11-29-01/root/step_1/v_in_port	•••	Virtual input port	2018-08-22 09:29:02.274462	2018-08-22 09:29:02.274466	null	1969
4717	failing_network_2018-08-22T11-29-01/root/step_3/in_1	in_1	\N	2018-08-22 09:29:02.27507	2018-08-22 09:29:02.275074	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1970
4718	failing_network_2018-08-22T11-29-01/root/step_3/in_2	in_2	\N	2018-08-22 09:29:02.275749	2018-08-22 09:29:02.275753	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_3"]}	1970
5144	auto_prefix_test_2018-09-03T11-34-05/root/m_n/left_hand	left_hand	\N	2018-09-03 09:34:06.652509	2018-09-03 09:34:06.652513	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2189
4719	failing_network_2018-08-22T11-29-01/root/step_3/fail_1	fail_1	\N	2018-08-22 09:29:02.276355	2018-08-22 09:29:02.276359	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	1970
4720	failing_network_2018-08-22T11-29-01/root/step_3/fail_2	fail_2	\N	2018-08-22 09:29:02.276996	2018-08-22 09:29:02.277	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	1970
4721	failing_network_2018-08-22T11-29-01/root/step_3/v_in_port	•••	Virtual input port	2018-08-22 09:29:02.277635	2018-08-22 09:29:02.277641	null	1970
4722	failing_network_2018-08-22T11-29-01/root/step_2/in_1	in_1	\N	2018-08-22 09:29:02.278165	2018-08-22 09:29:02.278169	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_3"]}	1971
4723	failing_network_2018-08-22T11-29-01/root/step_2/in_2	in_2	\N	2018-08-22 09:29:02.278711	2018-08-22 09:29:02.278715	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1971
4724	failing_network_2018-08-22T11-29-01/root/step_2/fail_1	fail_1	\N	2018-08-22 09:29:02.279279	2018-08-22 09:29:02.279283	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["const_step_2_fail_1_0"]}	1971
4725	failing_network_2018-08-22T11-29-01/root/step_2/fail_2	fail_2	\N	2018-08-22 09:29:02.279799	2018-08-22 09:29:02.279803	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	1971
4726	failing_network_2018-08-22T11-29-01/root/step_2/v_in_port	•••	Virtual input port	2018-08-22 09:29:02.280448	2018-08-22 09:29:02.280452	null	1971
4727	failing_network_2018-08-22T11-29-01/root/sum/values	values	\N	2018-08-22 09:29:02.28093	2018-08-22 09:29:02.280934	{"datatype": "Number", "input_group": "default", "dimension_names": ["source_1"]}	1972
4728	failing_network_2018-08-22T11-29-01/root/sum/v_in_port	•••	Virtual input port	2018-08-22 09:29:02.281529	2018-08-22 09:29:02.281533	null	1972
4729	failing_network_2018-08-22T11-29-01/root/source_1/input	input	\N	2018-08-22 09:29:02.282284	2018-08-22 09:29:02.282289	{"datatype": "Int", "dimension_names": ["source_1"]}	1973
4730	failing_network_2018-08-22T11-29-01/root/source_1/v_in_port	•••	Virtual input port	2018-08-22 09:29:02.282866	2018-08-22 09:29:02.28287	null	1973
4731	failing_network_2018-08-22T11-29-01/root/sink_2/input	input	\N	2018-08-22 09:29:02.283467	2018-08-22 09:29:02.283471	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_3"]}	1974
4732	failing_network_2018-08-22T11-29-01/root/sink_2/v_in_port	•••	Virtual input port	2018-08-22 09:29:02.283982	2018-08-22 09:29:02.283986	null	1974
4733	failing_network_2018-08-22T11-29-01/root/sink_3/input	input	\N	2018-08-22 09:29:02.284593	2018-08-22 09:29:02.284597	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1975
4734	failing_network_2018-08-22T11-29-01/root/sink_3/v_in_port	•••	Virtual input port	2018-08-22 09:29:02.285101	2018-08-22 09:29:02.285105	null	1975
4735	failing_network_2018-08-22T11-29-01/root/range/value	value	\N	2018-08-22 09:29:02.285666	2018-08-22 09:29:02.285671	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1976
4736	failing_network_2018-08-22T11-29-01/root/range/v_in_port	•••	Virtual input port	2018-08-22 09:29:02.286152	2018-08-22 09:29:02.286156	null	1976
4737	failing_network_2018-08-22T11-29-01/root/sink_1/input	input	\N	2018-08-22 09:29:02.28662	2018-08-22 09:29:02.286624	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1977
4738	failing_network_2018-08-22T11-29-01/root/sink_1/v_in_port	•••	Virtual input port	2018-08-22 09:29:02.287097	2018-08-22 09:29:02.287101	null	1977
4739	failing_network_2018-08-22T11-29-01/root/source_2/input	input	\N	2018-08-22 09:29:02.287645	2018-08-22 09:29:02.28765	{"datatype": "Int", "dimension_names": ["source_2"]}	1978
4740	failing_network_2018-08-22T11-29-01/root/source_2/v_in_port	•••	Virtual input port	2018-08-22 09:29:02.288114	2018-08-22 09:29:02.288135	null	1978
4741	failing_network_2018-08-22T11-29-01/root/source_3/input	input	\N	2018-08-22 09:29:02.288625	2018-08-22 09:29:02.288629	{"datatype": "Int", "dimension_names": ["source_3"]}	1979
4742	failing_network_2018-08-22T11-29-01/root/source_3/v_in_port	•••	Virtual input port	2018-08-22 09:29:02.289138	2018-08-22 09:29:02.289142	null	1979
4743	failing_network_2018-08-22T11-29-01/root/sink_4/input	input	\N	2018-08-22 09:29:02.289651	2018-08-22 09:29:02.289655	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1980
4744	failing_network_2018-08-22T11-29-01/root/sink_4/v_in_port	•••	Virtual input port	2018-08-22 09:29:02.290116	2018-08-22 09:29:02.29012	null	1980
4745	failing_network_2018-08-22T11-29-01/root/sink_5/input	input	\N	2018-08-22 09:29:02.290728	2018-08-22 09:29:02.290732	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1981
4746	failing_network_2018-08-22T11-29-01/root/sink_5/v_in_port	•••	Virtual input port	2018-08-22 09:29:02.291339	2018-08-22 09:29:02.291343	null	1981
4747	failing_network_2018-08-22T11-29-01/root/const_step_2_fail_1_0/v_in_port	•••	Virtual input port	2018-08-22 09:29:02.291904	2018-08-22 09:29:02.291908	null	1982
4748	failing_network_2018-08-22T11-29-01/root/const_step_1_fail_2_0/v_in_port	•••	Virtual input port	2018-08-22 09:29:02.292488	2018-08-22 09:29:02.292492	null	1983
4836	macro_top_level_2018-08-22T12-36-32/root/source/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.145383	2018-08-22 10:36:33.145388	null	2024
4837	macro_top_level_2018-08-22T12-36-32/root/source/source/input	input	\N	2018-08-22 10:36:33.146545	2018-08-22 10:36:33.146549	{"datatype": "Int", "dimension_names": ["source"]}	2027
4838	macro_top_level_2018-08-22T12-36-32/root/source/source/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.147151	2018-08-22 10:36:33.147157	null	2027
4839	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.1477	2018-08-22 10:36:33.147704	null	2025
4840	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value/input	input	\N	2018-08-22 10:36:33.148271	2018-08-22 10:36:33.148275	{"datatype": "Int", "dimension_names": ["input_value"]}	2028
4841	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.148832	2018-08-22 10:36:33.148835	null	2028
4842	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.149374	2018-08-22 10:36:33.149378	null	2029
4843	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.149893	2018-08-22 10:36:33.149897	null	2032
4844	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1/left_hand	left_hand	\N	2018-08-22 10:36:33.150482	2018-08-22 10:36:33.150486	{"datatype": "Int", "input_group": "default", "dimension_names": ["input"]}	2038
4845	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1/right_hand	right_hand	\N	2018-08-22 10:36:33.150996	2018-08-22 10:36:33.151	{"datatype": "Int", "input_group": "default", "dimension_names": ["const_addint1_right_hand_0"]}	2038
4846	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.151458	2018-08-22 10:36:33.151461	null	2038
4847	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2/left_hand	left_hand	\N	2018-08-22 10:36:33.15189	2018-08-22 10:36:33.151894	{"datatype": "Int", "input_group": "default", "dimension_names": ["input"]}	2039
4848	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2/right_hand	right_hand	\N	2018-08-22 10:36:33.152403	2018-08-22 10:36:33.152407	{"datatype": "Int", "input_group": "default", "dimension_names": ["const_addint2_right_hand_0"]}	2039
4913	input_groups_2018-08-22T15-39-07/root/source/v_in_port	•••	Virtual input port	2018-08-22 13:39:07.340747	2018-08-22 13:39:07.340751	null	2067
4749	failing_network_2018-08-22T12-10-16/root/step_1/in_1	in_1	\N	2018-08-22 10:10:17.531405	2018-08-22 10:10:17.53141	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1985
4750	failing_network_2018-08-22T12-10-16/root/step_1/in_2	in_2	\N	2018-08-22 10:10:17.532835	2018-08-22 10:10:17.532839	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_2"]}	1985
4751	failing_network_2018-08-22T12-10-16/root/step_1/fail_1	fail_1	\N	2018-08-22 10:10:17.533509	2018-08-22 10:10:17.533514	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	1985
4752	failing_network_2018-08-22T12-10-16/root/step_1/fail_2	fail_2	\N	2018-08-22 10:10:17.534067	2018-08-22 10:10:17.534071	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["const_step_1_fail_2_0"]}	1985
4753	failing_network_2018-08-22T12-10-16/root/step_1/v_in_port	•••	Virtual input port	2018-08-22 10:10:17.534632	2018-08-22 10:10:17.534636	null	1985
4754	failing_network_2018-08-22T12-10-16/root/step_3/in_1	in_1	\N	2018-08-22 10:10:17.535179	2018-08-22 10:10:17.535183	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1986
4755	failing_network_2018-08-22T12-10-16/root/step_3/in_2	in_2	\N	2018-08-22 10:10:17.535766	2018-08-22 10:10:17.53577	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_3"]}	1986
4756	failing_network_2018-08-22T12-10-16/root/step_3/fail_1	fail_1	\N	2018-08-22 10:10:17.536257	2018-08-22 10:10:17.536261	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	1986
4757	failing_network_2018-08-22T12-10-16/root/step_3/fail_2	fail_2	\N	2018-08-22 10:10:17.536743	2018-08-22 10:10:17.536747	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	1986
4758	failing_network_2018-08-22T12-10-16/root/step_3/v_in_port	•••	Virtual input port	2018-08-22 10:10:17.537242	2018-08-22 10:10:17.537246	null	1986
4759	failing_network_2018-08-22T12-10-16/root/step_2/in_1	in_1	\N	2018-08-22 10:10:17.537753	2018-08-22 10:10:17.537757	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_3"]}	1987
4760	failing_network_2018-08-22T12-10-16/root/step_2/in_2	in_2	\N	2018-08-22 10:10:17.5382	2018-08-22 10:10:17.538204	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1987
4761	failing_network_2018-08-22T12-10-16/root/step_2/fail_1	fail_1	\N	2018-08-22 10:10:17.538667	2018-08-22 10:10:17.53867	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["const_step_2_fail_1_0"]}	1987
4762	failing_network_2018-08-22T12-10-16/root/step_2/fail_2	fail_2	\N	2018-08-22 10:10:17.539102	2018-08-22 10:10:17.539106	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	1987
4763	failing_network_2018-08-22T12-10-16/root/step_2/v_in_port	•••	Virtual input port	2018-08-22 10:10:17.539505	2018-08-22 10:10:17.539509	null	1987
4764	failing_network_2018-08-22T12-10-16/root/sum/values	values	\N	2018-08-22 10:10:17.540009	2018-08-22 10:10:17.540013	{"datatype": "Number", "input_group": "default", "dimension_names": ["source_1"]}	1988
4765	failing_network_2018-08-22T12-10-16/root/sum/v_in_port	•••	Virtual input port	2018-08-22 10:10:17.540432	2018-08-22 10:10:17.540436	null	1988
4766	failing_network_2018-08-22T12-10-16/root/source_1/input	input	\N	2018-08-22 10:10:17.540866	2018-08-22 10:10:17.54087	{"datatype": "Int", "dimension_names": ["source_1"]}	1989
4767	failing_network_2018-08-22T12-10-16/root/source_1/v_in_port	•••	Virtual input port	2018-08-22 10:10:17.541335	2018-08-22 10:10:17.541339	null	1989
4768	failing_network_2018-08-22T12-10-16/root/sink_2/input	input	\N	2018-08-22 10:10:17.541911	2018-08-22 10:10:17.541916	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_3"]}	1990
4769	failing_network_2018-08-22T12-10-16/root/sink_2/v_in_port	•••	Virtual input port	2018-08-22 10:10:17.542405	2018-08-22 10:10:17.542409	null	1990
4770	failing_network_2018-08-22T12-10-16/root/sink_3/input	input	\N	2018-08-22 10:10:17.542924	2018-08-22 10:10:17.542928	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1991
4771	failing_network_2018-08-22T12-10-16/root/sink_3/v_in_port	•••	Virtual input port	2018-08-22 10:10:17.543375	2018-08-22 10:10:17.543379	null	1991
4772	failing_network_2018-08-22T12-10-16/root/range/value	value	\N	2018-08-22 10:10:17.543843	2018-08-22 10:10:17.543847	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1992
4773	failing_network_2018-08-22T12-10-16/root/range/v_in_port	•••	Virtual input port	2018-08-22 10:10:17.544336	2018-08-22 10:10:17.54434	null	1992
4774	failing_network_2018-08-22T12-10-16/root/sink_1/input	input	\N	2018-08-22 10:10:17.544881	2018-08-22 10:10:17.544886	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1993
4775	failing_network_2018-08-22T12-10-16/root/sink_1/v_in_port	•••	Virtual input port	2018-08-22 10:10:17.54535	2018-08-22 10:10:17.545354	null	1993
4776	failing_network_2018-08-22T12-10-16/root/source_2/input	input	\N	2018-08-22 10:10:17.545799	2018-08-22 10:10:17.545803	{"datatype": "Int", "dimension_names": ["source_2"]}	1994
4777	failing_network_2018-08-22T12-10-16/root/source_2/v_in_port	•••	Virtual input port	2018-08-22 10:10:17.546283	2018-08-22 10:10:17.546287	null	1994
4778	failing_network_2018-08-22T12-10-16/root/source_3/input	input	\N	2018-08-22 10:10:17.546722	2018-08-22 10:10:17.546726	{"datatype": "Int", "dimension_names": ["source_3"]}	1995
4779	failing_network_2018-08-22T12-10-16/root/source_3/v_in_port	•••	Virtual input port	2018-08-22 10:10:17.547262	2018-08-22 10:10:17.547266	null	1995
4780	failing_network_2018-08-22T12-10-16/root/sink_4/input	input	\N	2018-08-22 10:10:17.547733	2018-08-22 10:10:17.547737	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1996
4781	failing_network_2018-08-22T12-10-16/root/sink_4/v_in_port	•••	Virtual input port	2018-08-22 10:10:17.548271	2018-08-22 10:10:17.548275	null	1996
4782	failing_network_2018-08-22T12-10-16/root/sink_5/input	input	\N	2018-08-22 10:10:17.548719	2018-08-22 10:10:17.548723	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	1997
4783	failing_network_2018-08-22T12-10-16/root/sink_5/v_in_port	•••	Virtual input port	2018-08-22 10:10:17.549243	2018-08-22 10:10:17.549247	null	1997
4784	failing_network_2018-08-22T12-10-16/root/const_step_2_fail_1_0/v_in_port	•••	Virtual input port	2018-08-22 10:10:17.549728	2018-08-22 10:10:17.549733	null	1998
4785	failing_network_2018-08-22T12-10-16/root/const_step_1_fail_2_0/v_in_port	•••	Virtual input port	2018-08-22 10:10:17.550357	2018-08-22 10:10:17.550361	null	1999
4786	failing_macro_top_level_2018-08-22T12-15-04/root/source_b/input	input	\N	2018-08-22 10:15:04.840292	2018-08-22 10:15:04.840297	{"datatype": "Int", "dimension_names": ["source_b"]}	2001
4787	failing_macro_top_level_2018-08-22T12-15-04/root/source_b/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.841107	2018-08-22 10:15:04.841113	null	2001
4788	failing_macro_top_level_2018-08-22T12-15-04/root/source_c/input	input	\N	2018-08-22 10:15:04.84173	2018-08-22 10:15:04.841734	{"datatype": "Int", "dimension_names": ["source_c"]}	2002
4789	failing_macro_top_level_2018-08-22T12-15-04/root/source_c/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.842287	2018-08-22 10:15:04.842291	null	2002
4790	failing_macro_top_level_2018-08-22T12-15-04/root/source_a/input	input	\N	2018-08-22 10:15:04.842731	2018-08-22 10:15:04.842735	{"datatype": "Int", "dimension_names": ["source_a"]}	2003
4791	failing_macro_top_level_2018-08-22T12-15-04/root/source_a/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.843265	2018-08-22 10:15:04.843268	null	2003
4792	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.84373	2018-08-22 10:15:04.843734	null	2004
4793	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/in_1	in_1	\N	2018-08-22 10:15:04.84427	2018-08-22 10:15:04.844273	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	2008
4794	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/in_2	in_2	\N	2018-08-22 10:15:04.84481	2018-08-22 10:15:04.844814	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_2"]}	2008
4795	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/fail_1	fail_1	\N	2018-08-22 10:15:04.845475	2018-08-22 10:15:04.84548	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	2008
4796	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/fail_2	fail_2	\N	2018-08-22 10:15:04.846	2018-08-22 10:15:04.846004	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["const_step_1_fail_2_0"]}	2008
4797	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.846472	2018-08-22 10:15:04.846476	null	2008
4798	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/in_1	in_1	\N	2018-08-22 10:15:04.846909	2018-08-22 10:15:04.846913	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	2009
4799	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/in_2	in_2	\N	2018-08-22 10:15:04.847324	2018-08-22 10:15:04.847328	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_3"]}	2009
4800	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/fail_1	fail_1	\N	2018-08-22 10:15:04.847778	2018-08-22 10:15:04.847782	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	2009
4801	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/fail_2	fail_2	\N	2018-08-22 10:15:04.84841	2018-08-22 10:15:04.848414	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	2009
4802	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.849046	2018-08-22 10:15:04.849049	null	2009
4803	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/in_1	in_1	\N	2018-08-22 10:15:04.849522	2018-08-22 10:15:04.849526	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_3"]}	2010
4804	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/in_2	in_2	\N	2018-08-22 10:15:04.850011	2018-08-22 10:15:04.850015	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	2010
4805	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/fail_1	fail_1	\N	2018-08-22 10:15:04.850473	2018-08-22 10:15:04.850477	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["const_step_2_fail_1_0"]}	2010
4806	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/fail_2	fail_2	\N	2018-08-22 10:15:04.851013	2018-08-22 10:15:04.851017	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	2010
4807	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.851542	2018-08-22 10:15:04.851546	null	2010
4808	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sum/values	values	\N	2018-08-22 10:15:04.852044	2018-08-22 10:15:04.852048	{"datatype": "Number", "input_group": "default", "dimension_names": ["source_1"]}	2011
4809	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sum/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.852632	2018-08-22 10:15:04.852637	null	2011
4810	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_5/input	input	\N	2018-08-22 10:15:04.853092	2018-08-22 10:15:04.853096	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	2012
4811	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_5/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.853634	2018-08-22 10:15:04.853639	null	2012
4812	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_2/input	input	\N	2018-08-22 10:15:04.854079	2018-08-22 10:15:04.854083	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_3"]}	2013
4813	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_2/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.854498	2018-08-22 10:15:04.854502	null	2013
4814	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_3/input	input	\N	2018-08-22 10:15:04.854932	2018-08-22 10:15:04.854936	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	2014
4815	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_3/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.855363	2018-08-22 10:15:04.855367	null	2014
4816	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range/value	value	\N	2018-08-22 10:15:04.855815	2018-08-22 10:15:04.855819	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	2015
4817	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.856379	2018-08-22 10:15:04.856383	null	2015
4818	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_1/input	input	\N	2018-08-22 10:15:04.856863	2018-08-22 10:15:04.856867	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	2016
4819	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_1/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.85738	2018-08-22 10:15:04.857384	null	2016
4820	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_2/input	input	\N	2018-08-22 10:15:04.857915	2018-08-22 10:15:04.857919	{"datatype": "Int", "dimension_names": ["source_2"]}	2017
4821	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_2/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.858374	2018-08-22 10:15:04.858378	null	2017
4822	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_3/input	input	\N	2018-08-22 10:15:04.858802	2018-08-22 10:15:04.858805	{"datatype": "Int", "dimension_names": ["source_3"]}	2018
4823	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_3/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.859255	2018-08-22 10:15:04.859259	null	2018
4824	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_4/input	input	\N	2018-08-22 10:15:04.859796	2018-08-22 10:15:04.8598	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	2019
4825	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_4/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.860313	2018-08-22 10:15:04.860316	null	2019
4826	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1/input	input	\N	2018-08-22 10:15:04.860832	2018-08-22 10:15:04.860836	{"datatype": "Int", "dimension_names": ["source_1"]}	2020
4827	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.861401	2018-08-22 10:15:04.861405	null	2020
4828	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_2_fail_1_0/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.862139	2018-08-22 10:15:04.862143	null	2021
4829	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_1_fail_2_0/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.862663	2018-08-22 10:15:04.862667	null	2022
4830	failing_macro_top_level_2018-08-22T12-15-04/root/add/left_hand	left_hand	\N	2018-08-22 10:15:04.863114	2018-08-22 10:15:04.863118	{"datatype": "Int", "input_group": "default", "dimension_names": []}	2005
4831	failing_macro_top_level_2018-08-22T12-15-04/root/add/right_hand	right_hand	\N	2018-08-22 10:15:04.863576	2018-08-22 10:15:04.863598	{"datatype": "Int", "input_group": "default", "dimension_names": ["const_add_right_hand_0"]}	2005
4832	failing_macro_top_level_2018-08-22T12-15-04/root/add/v_in_port	•••	Virtual input port	2018-08-22 10:15:04.864058	2018-08-22 10:15:04.864062	null	2005
4870	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input/input	input	\N	2018-08-22 10:36:33.162879	2018-08-22 10:36:33.162883	{"datatype": "Int", "dimension_names": ["input"]}	2037
4871	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.163367	2018-08-22 10:36:33.163371	null	2037
4872	macro_top_level_2018-08-22T12-36-32/root/sink/input	input	\N	2018-08-22 10:36:33.163816	2018-08-22 10:36:33.16382	{"datatype": "Int", "input_group": "default", "dimension_names": []}	2026
4873	macro_top_level_2018-08-22T12-36-32/root/sink/v_in_port	•••	Virtual input port	2018-08-22 10:36:33.164343	2018-08-22 10:36:33.164348	null	2026
4874	macro_node_2_2018-08-22T12-40-46/root/sum/values	values	\N	2018-08-22 10:40:46.872733	2018-08-22 10:40:46.872738	{"datatype": "Number", "input_group": "default", "dimension_names": ["source_1", "source_3"]}	2047
4875	macro_node_2_2018-08-22T12-40-46/root/sum/v_in_port	•••	Virtual input port	2018-08-22 10:40:46.873355	2018-08-22 10:40:46.873359	null	2047
4876	macro_node_2_2018-08-22T12-40-46/root/addint/left_hand	left_hand	\N	2018-08-22 10:40:46.873933	2018-08-22 10:40:46.873937	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1"]}	2048
4877	macro_node_2_2018-08-22T12-40-46/root/addint/right_hand	right_hand	\N	2018-08-22 10:40:46.874574	2018-08-22 10:40:46.874578	{"datatype": "Int", "input_group": "right", "dimension_names": ["source_2"]}	2048
4878	macro_node_2_2018-08-22T12-40-46/root/addint/v_in_port	•••	Virtual input port	2018-08-22 10:40:46.875114	2018-08-22 10:40:46.875118	null	2048
4879	macro_node_2_2018-08-22T12-40-46/root/source_2/input	input	\N	2018-08-22 10:40:46.875629	2018-08-22 10:40:46.875633	{"datatype": "Int", "dimension_names": ["source_2"]}	2049
4880	macro_node_2_2018-08-22T12-40-46/root/source_2/v_in_port	•••	Virtual input port	2018-08-22 10:40:46.876189	2018-08-22 10:40:46.876193	null	2049
4881	macro_node_2_2018-08-22T12-40-46/root/source_3/input	input	\N	2018-08-22 10:40:46.876741	2018-08-22 10:40:46.876745	{"datatype": "Int", "dimension_names": ["source_3"]}	2050
4882	macro_node_2_2018-08-22T12-40-46/root/source_3/v_in_port	•••	Virtual input port	2018-08-22 10:40:46.8772	2018-08-22 10:40:46.877204	null	2050
4883	macro_node_2_2018-08-22T12-40-46/root/source_1/input	input	\N	2018-08-22 10:40:46.877727	2018-08-22 10:40:46.877731	{"datatype": "Int", "dimension_names": ["source_1"]}	2051
4884	macro_node_2_2018-08-22T12-40-46/root/source_1/v_in_port	•••	Virtual input port	2018-08-22 10:40:46.878224	2018-08-22 10:40:46.878228	null	2051
4885	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/v_in_port	•••	Virtual input port	2018-08-22 10:40:46.878666	2018-08-22 10:40:46.87867	null	2052
4886	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/input	input	\N	2018-08-22 10:40:46.879105	2018-08-22 10:40:46.879109	{"datatype": "Int", "input_group": "default", "dimension_names": ["macro_input_1", "macro_input_2"]}	2054
4887	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/v_in_port	•••	Virtual input port	2018-08-22 10:40:46.879678	2018-08-22 10:40:46.879682	null	2054
4888	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/left_hand	left_hand	\N	2018-08-22 10:40:46.880216	2018-08-22 10:40:46.88022	{"datatype": "Int", "input_group": "default", "dimension_names": ["macro_input_1"]}	2055
4889	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/right_hand	right_hand	\N	2018-08-22 10:40:46.88079	2018-08-22 10:40:46.880794	{"datatype": "Int", "input_group": "other", "dimension_names": ["macro_input_2"]}	2055
4890	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/v_in_port	•••	Virtual input port	2018-08-22 10:40:46.881267	2018-08-22 10:40:46.881271	null	2055
4891	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1/input	input	\N	2018-08-22 10:40:46.881742	2018-08-22 10:40:46.881746	{"datatype": "Int", "dimension_names": ["macro_input_1"]}	2056
4892	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1/v_in_port	•••	Virtual input port	2018-08-22 10:40:46.882232	2018-08-22 10:40:46.882236	null	2056
4893	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_2/input	input	\N	2018-08-22 10:40:46.88267	2018-08-22 10:40:46.882674	{"datatype": "Int", "dimension_names": ["macro_input_2"]}	2057
4894	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_2/v_in_port	•••	Virtual input port	2018-08-22 10:40:46.883137	2018-08-22 10:40:46.883141	null	2057
4895	macro_node_2_2018-08-22T12-40-46/root/sink/input	input	\N	2018-08-22 10:40:46.883609	2018-08-22 10:40:46.883612	{"datatype": "Int", "input_group": "default", "dimension_names": ["source_1", "source_3"]}	2053
4896	macro_node_2_2018-08-22T12-40-46/root/sink/v_in_port	•••	Virtual input port	2018-08-22 10:40:46.884161	2018-08-22 10:40:46.884165	null	2053
4897	cross_validation_2018-08-22T15-29-33/root/crossvalidtion/items	items	\N	2018-08-22 13:29:33.785662	2018-08-22 13:29:33.785667	{"datatype": "AnyType", "input_group": "default", "dimension_names": ["numbers"]}	2059
4898	cross_validation_2018-08-22T15-29-33/root/crossvalidtion/method	method	\N	2018-08-22 13:29:33.786442	2018-08-22 13:29:33.786446	{"datatype": "__CrossValidation_0.1_interface__method__Enum__", "input_group": "default", "dimension_names": ["const_crossvalidtion_method_0"]}	2059
4899	cross_validation_2018-08-22T15-29-33/root/crossvalidtion/number_of_folds	number_of_folds	\N	2018-08-22 13:29:33.786932	2018-08-22 13:29:33.786936	{"datatype": "Int", "input_group": "default", "dimension_names": ["const_crossvalidtion_number_of_folds_0"]}	2059
4900	cross_validation_2018-08-22T15-29-33/root/crossvalidtion/v_in_port	•••	Virtual input port	2018-08-22 13:29:33.787432	2018-08-22 13:29:33.787436	null	2059
4901	cross_validation_2018-08-22T15-29-33/root/sum/values	values	\N	2018-08-22 13:29:33.78801	2018-08-22 13:29:33.788014	{"datatype": "Number", "input_group": "default", "dimension_names": ["numbers_test", "crossvalidtion"]}	2060
4902	cross_validation_2018-08-22T15-29-33/root/sum/v_in_port	•••	Virtual input port	2018-08-22 13:29:33.78859	2018-08-22 13:29:33.788594	null	2060
4903	cross_validation_2018-08-22T15-29-33/root/const_crossvalidtion_method_0/v_in_port	•••	Virtual input port	2018-08-22 13:29:33.789106	2018-08-22 13:29:33.78911	null	2061
4904	cross_validation_2018-08-22T15-29-33/root/numbers/input	input	\N	2018-08-22 13:29:33.789607	2018-08-22 13:29:33.789611	{"datatype": "Int", "dimension_names": ["numbers"]}	2062
4905	cross_validation_2018-08-22T15-29-33/root/numbers/v_in_port	•••	Virtual input port	2018-08-22 13:29:33.790095	2018-08-22 13:29:33.790098	null	2062
4906	cross_validation_2018-08-22T15-29-33/root/multiply/left_hand	left_hand	\N	2018-08-22 13:29:33.790538	2018-08-22 13:29:33.790542	{"datatype": "Number", "input_group": "default", "dimension_names": ["numbers_train", "crossvalidtion"]}	2063
4907	cross_validation_2018-08-22T15-29-33/root/multiply/right_hand	right_hand	\N	2018-08-22 13:29:33.79097	2018-08-22 13:29:33.790974	{"datatype": "Number", "input_group": "right", "dimension_names": ["numbers_test", "crossvalidtion"]}	2063
4908	cross_validation_2018-08-22T15-29-33/root/multiply/v_in_port	•••	Virtual input port	2018-08-22 13:29:33.791374	2018-08-22 13:29:33.791378	null	2063
4909	cross_validation_2018-08-22T15-29-33/root/const_crossvalidtion_number_of_folds_0/v_in_port	•••	Virtual input port	2018-08-22 13:29:33.791966	2018-08-22 13:29:33.79197	null	2064
4910	cross_validation_2018-08-22T15-29-33/root/sink/input	input	\N	2018-08-22 13:29:33.792517	2018-08-22 13:29:33.792521	{"datatype": "Int", "input_group": "default", "dimension_names": ["numbers_test", "crossvalidtion"]}	2065
4911	cross_validation_2018-08-22T15-29-33/root/sink/v_in_port	•••	Virtual input port	2018-08-22 13:29:33.793018	2018-08-22 13:29:33.793022	null	2065
4912	input_groups_2018-08-22T15-39-07/root/source/input	input	\N	2018-08-22 13:39:07.340105	2018-08-22 13:39:07.34011	{"datatype": "Int", "dimension_names": ["source"]}	2067
4914	input_groups_2018-08-22T15-39-07/root/add/left_hand	left_hand	\N	2018-08-22 13:39:07.341208	2018-08-22 13:39:07.341212	{"datatype": "Int", "input_group": "default", "dimension_names": ["source"]}	2068
4915	input_groups_2018-08-22T15-39-07/root/add/right_hand	right_hand	\N	2018-08-22 13:39:07.341758	2018-08-22 13:39:07.341762	{"datatype": "Int", "input_group": "right", "dimension_names": ["const_add_right_hand_0"]}	2068
4916	input_groups_2018-08-22T15-39-07/root/add/v_in_port	•••	Virtual input port	2018-08-22 13:39:07.342218	2018-08-22 13:39:07.342232	null	2068
4917	input_groups_2018-08-22T15-39-07/root/const_add_right_hand_0/v_in_port	•••	Virtual input port	2018-08-22 13:39:07.342679	2018-08-22 13:39:07.342683	null	2069
4918	input_groups_2018-08-22T15-39-07/root/sink/input	input	\N	2018-08-22 13:39:07.343122	2018-08-22 13:39:07.343126	{"datatype": "Int", "input_group": "default", "dimension_names": ["source", "const_add_right_hand_0"]}	2070
4919	input_groups_2018-08-22T15-39-07/root/sink/v_in_port	•••	Virtual input port	2018-08-22 13:39:07.343586	2018-08-22 13:39:07.34359	null	2070
5145	auto_prefix_test_2018-09-03T11-34-05/root/m_n/right_hand	right_hand	\N	2018-09-03 09:34:06.653057	2018-09-03 09:34:06.65306	{"input_group": "default", "datatype": "Int", "dimension_names": ["const"]}	2189
5146	auto_prefix_test_2018-09-03T11-34-05/root/m_n/v_in_port	•••	Virtual input port	2018-09-03 09:34:06.653532	2018-09-03 09:34:06.653554	null	2189
5147	auto_prefix_test_2018-09-03T11-34-05/root/a_n/left_hand	left_hand	\N	2018-09-03 09:34:06.653969	2018-09-03 09:34:06.653972	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2190
5148	auto_prefix_test_2018-09-03T11-34-05/root/a_n/right_hand	right_hand	\N	2018-09-03 09:34:06.654368	2018-09-03 09:34:06.654371	{"input_group": "default", "datatype": "Int", "dimension_names": ["const"]}	2190
5149	auto_prefix_test_2018-09-03T11-34-05/root/a_n/v_in_port	•••	Virtual input port	2018-09-03 09:34:06.654824	2018-09-03 09:34:06.654828	null	2190
5150	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_p/input	input	\N	2018-09-03 09:34:06.655295	2018-09-03 09:34:06.655299	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2191
5151	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_p/v_in_port	•••	Virtual input port	2018-09-03 09:34:06.655733	2018-09-03 09:34:06.655737	null	2191
5152	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_p/input	input	\N	2018-09-03 09:34:06.656303	2018-09-03 09:34:06.656307	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2192
5153	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_p/v_in_port	•••	Virtual input port	2018-09-03 09:34:06.656766	2018-09-03 09:34:06.65677	null	2192
5154	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_n/input	input	\N	2018-09-03 09:34:06.657173	2018-09-03 09:34:06.657177	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2193
5155	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_n/v_in_port	•••	Virtual input port	2018-09-03 09:34:06.657572	2018-09-03 09:34:06.657576	null	2193
5156	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_n/input	input	\N	2018-09-03 09:34:06.657991	2018-09-03 09:34:06.657995	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2194
5157	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_n/v_in_port	•••	Virtual input port	2018-09-03 09:34:06.658366	2018-09-03 09:34:06.65837	null	2194
5213	example_2018-09-03T13-11-45/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 11:11:45.557757	2018-09-03 11:11:45.557761	null	2229
5214	example_2018-09-03T13-12-39/root/source1/input	input	\N	2018-09-03 11:12:40.063027	2018-09-03 11:12:40.063032	{"datatype": "Int", "dimension_names": ["source1"]}	2231
5215	example_2018-09-03T13-12-39/root/source1/v_in_port	•••	Virtual input port	2018-09-03 11:12:40.063534	2018-09-03 11:12:40.063538	null	2231
5216	example_2018-09-03T13-12-39/root/sink1/input	input	\N	2018-09-03 11:12:40.064098	2018-09-03 11:12:40.064102	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2232
5217	example_2018-09-03T13-12-39/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 11:12:40.064601	2018-09-03 11:12:40.064605	null	2232
5218	example_2018-09-03T13-12-39/root/addint/left_hand	left_hand	\N	2018-09-03 11:12:40.065111	2018-09-03 11:12:40.065115	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2233
5219	example_2018-09-03T13-12-39/root/addint/right_hand	right_hand	\N	2018-09-03 11:12:40.065622	2018-09-03 11:12:40.065626	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2233
5220	example_2018-09-03T13-12-39/root/addint/v_in_port	•••	Virtual input port	2018-09-03 11:12:40.066155	2018-09-03 11:12:40.066159	null	2233
5221	example_2018-09-03T13-12-39/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 11:12:40.066673	2018-09-03 11:12:40.066676	null	2234
5222	example_2018-09-03T13-13-28/root/source1/input	input	\N	2018-09-03 11:13:28.422531	2018-09-03 11:13:28.422536	{"datatype": "Int", "dimension_names": ["source1"]}	2236
5223	example_2018-09-03T13-13-28/root/source1/v_in_port	•••	Virtual input port	2018-09-03 11:13:28.423032	2018-09-03 11:13:28.423053	null	2236
5224	example_2018-09-03T13-13-28/root/sink1/input	input	\N	2018-09-03 11:13:28.423561	2018-09-03 11:13:28.423565	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2237
5225	example_2018-09-03T13-13-28/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 11:13:28.42402	2018-09-03 11:13:28.424023	null	2237
5226	example_2018-09-03T13-13-28/root/addint/left_hand	left_hand	\N	2018-09-03 11:13:28.424559	2018-09-03 11:13:28.424562	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2238
5227	example_2018-09-03T13-13-28/root/addint/right_hand	right_hand	\N	2018-09-03 11:13:28.425029	2018-09-03 11:13:28.425033	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2238
5228	example_2018-09-03T13-13-28/root/addint/v_in_port	•••	Virtual input port	2018-09-03 11:13:28.425582	2018-09-03 11:13:28.425586	null	2238
5229	example_2018-09-03T13-13-28/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 11:13:28.426005	2018-09-03 11:13:28.426009	null	2239
5230	failing_macro_top_level_2018-09-03T14-01-00/root/source_a/input	input	\N	2018-09-03 12:01:00.792638	2018-09-03 12:01:00.792643	{"datatype": "Int", "dimension_names": ["source_a"]}	2241
5231	failing_macro_top_level_2018-09-03T14-01-00/root/source_a/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.793136	2018-09-03 12:01:00.79314	null	2241
5232	failing_macro_top_level_2018-09-03T14-01-00/root/source_b/input	input	\N	2018-09-03 12:01:00.793536	2018-09-03 12:01:00.79354	{"datatype": "Int", "dimension_names": ["source_b"]}	2242
5233	failing_macro_top_level_2018-09-03T14-01-00/root/source_b/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.794014	2018-09-03 12:01:00.794018	null	2242
5234	failing_macro_top_level_2018-09-03T14-01-00/root/source_c/input	input	\N	2018-09-03 12:01:00.794436	2018-09-03 12:01:00.79444	{"datatype": "Int", "dimension_names": ["source_c"]}	2243
5235	failing_macro_top_level_2018-09-03T14-01-00/root/source_c/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.794862	2018-09-03 12:01:00.794866	null	2243
5236	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.795298	2018-09-03 12:01:00.795301	null	2244
5237	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1/input	input	\N	2018-09-03 12:01:00.795713	2018-09-03 12:01:00.795717	{"datatype": "Int", "dimension_names": ["source_1"]}	2248
5238	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.796146	2018-09-03 12:01:00.79615	null	2248
5239	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_2/input	input	\N	2018-09-03 12:01:00.796534	2018-09-03 12:01:00.796537	{"datatype": "Int", "dimension_names": ["source_2"]}	2249
5158	example_2018-09-03T12-17-58/root/source1/input	input	\N	2018-09-03 10:17:58.529742	2018-09-03 10:17:58.529748	{"datatype": "Int", "dimension_names": ["source1"]}	2196
5159	example_2018-09-03T12-17-58/root/source1/v_in_port	•••	Virtual input port	2018-09-03 10:17:58.530378	2018-09-03 10:17:58.530382	null	2196
5160	example_2018-09-03T12-17-58/root/sink1/input	input	\N	2018-09-03 10:17:58.530865	2018-09-03 10:17:58.530869	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2197
5161	example_2018-09-03T12-17-58/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 10:17:58.531401	2018-09-03 10:17:58.531406	null	2197
5162	example_2018-09-03T12-17-58/root/addint/left_hand	left_hand	\N	2018-09-03 10:17:58.53191	2018-09-03 10:17:58.531914	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2198
5163	example_2018-09-03T12-17-58/root/addint/right_hand	right_hand	\N	2018-09-03 10:17:58.53249	2018-09-03 10:17:58.532494	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2198
5164	example_2018-09-03T12-17-58/root/addint/v_in_port	•••	Virtual input port	2018-09-03 10:17:58.533033	2018-09-03 10:17:58.533037	null	2198
5165	example_2018-09-03T12-17-58/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 10:17:58.533593	2018-09-03 10:17:58.533597	null	2199
5166	example_2018-09-03T12-31-11/root/source1/input	input	\N	2018-09-03 10:31:12.12585	2018-09-03 10:31:12.125856	{"datatype": "Int", "dimension_names": ["source1"]}	2201
5167	example_2018-09-03T12-31-11/root/source1/v_in_port	•••	Virtual input port	2018-09-03 10:31:12.126524	2018-09-03 10:31:12.126529	null	2201
5168	example_2018-09-03T12-31-11/root/sink1/input	input	\N	2018-09-03 10:31:12.127148	2018-09-03 10:31:12.127152	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2202
5169	example_2018-09-03T12-31-11/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 10:31:12.127781	2018-09-03 10:31:12.127785	null	2202
5170	example_2018-09-03T12-31-11/root/addint/left_hand	left_hand	\N	2018-09-03 10:31:12.128388	2018-09-03 10:31:12.128391	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2203
5171	example_2018-09-03T12-31-11/root/addint/right_hand	right_hand	\N	2018-09-03 10:31:12.129089	2018-09-03 10:31:12.129093	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2203
5172	example_2018-09-03T12-31-11/root/addint/v_in_port	•••	Virtual input port	2018-09-03 10:31:12.129582	2018-09-03 10:31:12.129586	null	2203
5173	example_2018-09-03T12-31-11/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 10:31:12.130053	2018-09-03 10:31:12.130057	null	2204
5174	example_2018-09-03T12-47-05/root/source1/input	input	\N	2018-09-03 10:47:06.281631	2018-09-03 10:47:06.281636	{"datatype": "Int", "dimension_names": ["source1"]}	2206
5175	example_2018-09-03T12-47-05/root/source1/v_in_port	•••	Virtual input port	2018-09-03 10:47:06.282117	2018-09-03 10:47:06.282121	null	2206
5176	example_2018-09-03T12-47-05/root/sink1/input	input	\N	2018-09-03 10:47:06.282514	2018-09-03 10:47:06.282518	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2207
5177	example_2018-09-03T12-47-05/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 10:47:06.282912	2018-09-03 10:47:06.282916	null	2207
5178	example_2018-09-03T12-47-05/root/addint/left_hand	left_hand	\N	2018-09-03 10:47:06.283349	2018-09-03 10:47:06.283353	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2208
5179	example_2018-09-03T12-47-05/root/addint/right_hand	right_hand	\N	2018-09-03 10:47:06.283902	2018-09-03 10:47:06.283906	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2208
5180	example_2018-09-03T12-47-05/root/addint/v_in_port	•••	Virtual input port	2018-09-03 10:47:06.28446	2018-09-03 10:47:06.284465	null	2208
5181	example_2018-09-03T12-47-05/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 10:47:06.28493	2018-09-03 10:47:06.284934	null	2209
5182	example_2018-09-03T12-54-57/root/source1/input	input	\N	2018-09-03 10:54:57.330086	2018-09-03 10:54:57.330091	{"datatype": "Int", "dimension_names": ["source1"]}	2211
5183	example_2018-09-03T12-54-57/root/source1/v_in_port	•••	Virtual input port	2018-09-03 10:54:57.33066	2018-09-03 10:54:57.330663	null	2211
5184	example_2018-09-03T12-54-57/root/sink1/input	input	\N	2018-09-03 10:54:57.33118	2018-09-03 10:54:57.331184	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2212
5185	example_2018-09-03T12-54-57/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 10:54:57.331845	2018-09-03 10:54:57.331849	null	2212
5186	example_2018-09-03T12-54-57/root/addint/left_hand	left_hand	\N	2018-09-03 10:54:57.332427	2018-09-03 10:54:57.332431	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2213
5187	example_2018-09-03T12-54-57/root/addint/right_hand	right_hand	\N	2018-09-03 10:54:57.333023	2018-09-03 10:54:57.333027	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2213
5188	example_2018-09-03T12-54-57/root/addint/v_in_port	•••	Virtual input port	2018-09-03 10:54:57.333539	2018-09-03 10:54:57.333543	null	2213
5189	example_2018-09-03T12-54-57/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 10:54:57.333993	2018-09-03 10:54:57.333997	null	2214
5190	example_2018-09-03T12-55-46/root/source1/input	input	\N	2018-09-03 10:55:46.995505	2018-09-03 10:55:46.995511	{"datatype": "Int", "dimension_names": ["source1"]}	2216
5191	example_2018-09-03T12-55-46/root/source1/v_in_port	•••	Virtual input port	2018-09-03 10:55:46.996067	2018-09-03 10:55:46.996071	null	2216
5192	example_2018-09-03T12-55-46/root/sink1/input	input	\N	2018-09-03 10:55:46.996543	2018-09-03 10:55:46.996547	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2217
5193	example_2018-09-03T12-55-46/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 10:55:46.996974	2018-09-03 10:55:46.996978	null	2217
5194	example_2018-09-03T12-55-46/root/addint/left_hand	left_hand	\N	2018-09-03 10:55:46.997493	2018-09-03 10:55:46.997497	{"input_group": "default", "datatype": "Int", "dimension_names": ["source1"]}	2218
5195	example_2018-09-03T12-55-46/root/addint/right_hand	right_hand	\N	2018-09-03 10:55:46.997944	2018-09-03 10:55:46.997948	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2218
5196	example_2018-09-03T12-55-46/root/addint/v_in_port	•••	Virtual input port	2018-09-03 10:55:46.998394	2018-09-03 10:55:46.998398	null	2218
5197	example_2018-09-03T12-55-46/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 10:55:46.998872	2018-09-03 10:55:46.998876	null	2219
5198	example_2018-09-03T13-09-35/root/sink1/input	input	\N	2018-09-03 11:09:36.238184	2018-09-03 11:09:36.238189	{"input_group": "default", "dimension_names": ["source1"], "datatype": "Int"}	2221
5199	example_2018-09-03T13-09-35/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 11:09:36.238738	2018-09-03 11:09:36.238741	null	2221
5200	example_2018-09-03T13-09-35/root/addint/left_hand	left_hand	\N	2018-09-03 11:09:36.239197	2018-09-03 11:09:36.239201	{"input_group": "default", "dimension_names": ["source1"], "datatype": "Int"}	2222
5002	failing_network_2018-08-24T15-34-44/root/source_1/input	input	\N	2018-08-24 13:34:44.957312	2018-08-24 13:34:44.957317	{"datatype": "Int", "dimension_names": ["source_1"]}	2109
5003	failing_network_2018-08-24T15-34-44/root/source_1/v_in_port	•••	Virtual input port	2018-08-24 13:34:44.957821	2018-08-24 13:34:44.957826	null	2109
5201	example_2018-09-03T13-09-35/root/addint/right_hand	right_hand	\N	2018-09-03 11:09:36.239621	2018-09-03 11:09:36.239625	{"input_group": "default", "dimension_names": ["const_addint_right_hand_0"], "datatype": "Int"}	2222
5202	example_2018-09-03T13-09-35/root/addint/v_in_port	•••	Virtual input port	2018-09-03 11:09:36.240023	2018-09-03 11:09:36.240027	null	2222
5004	failing_network_2018-08-24T15-34-44/root/source_2/input	input	\N	2018-08-24 13:34:44.958294	2018-08-24 13:34:44.958298	{"datatype": "Int", "dimension_names": ["source_2"]}	2110
5005	failing_network_2018-08-24T15-34-44/root/source_2/v_in_port	•••	Virtual input port	2018-08-24 13:34:44.958833	2018-08-24 13:34:44.958837	null	2110
5006	failing_network_2018-08-24T15-34-44/root/source_3/input	input	\N	2018-08-24 13:34:44.959254	2018-08-24 13:34:44.959258	{"datatype": "Int", "dimension_names": ["source_3"]}	2111
5007	failing_network_2018-08-24T15-34-44/root/source_3/v_in_port	•••	Virtual input port	2018-08-24 13:34:44.959696	2018-08-24 13:34:44.95971	null	2111
5008	failing_network_2018-08-24T15-34-44/root/sink_1/input	input	\N	2018-08-24 13:34:44.960095	2018-08-24 13:34:44.960099	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2112
5009	failing_network_2018-08-24T15-34-44/root/sink_1/v_in_port	•••	Virtual input port	2018-08-24 13:34:44.960571	2018-08-24 13:34:44.960575	null	2112
5010	failing_network_2018-08-24T15-34-44/root/sink_2/input	input	\N	2018-08-24 13:34:44.961017	2018-08-24 13:34:44.961021	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_3"]}	2113
5011	failing_network_2018-08-24T15-34-44/root/sink_2/v_in_port	•••	Virtual input port	2018-08-24 13:34:44.961485	2018-08-24 13:34:44.961489	null	2113
5012	failing_network_2018-08-24T15-34-44/root/sink_3/input	input	\N	2018-08-24 13:34:44.961924	2018-08-24 13:34:44.961928	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2114
5013	failing_network_2018-08-24T15-34-44/root/sink_3/v_in_port	•••	Virtual input port	2018-08-24 13:34:44.96234	2018-08-24 13:34:44.962344	null	2114
5014	failing_network_2018-08-24T15-34-44/root/sink_4/input	input	\N	2018-08-24 13:34:44.962806	2018-08-24 13:34:44.96281	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2115
5015	failing_network_2018-08-24T15-34-44/root/sink_4/v_in_port	•••	Virtual input port	2018-08-24 13:34:44.963207	2018-08-24 13:34:44.963211	null	2115
5016	failing_network_2018-08-24T15-34-44/root/sink_5/input	input	\N	2018-08-24 13:34:44.963649	2018-08-24 13:34:44.963653	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2116
5017	failing_network_2018-08-24T15-34-44/root/sink_5/v_in_port	•••	Virtual input port	2018-08-24 13:34:44.964053	2018-08-24 13:34:44.964057	null	2116
5018	failing_network_2018-08-24T15-34-44/root/step_1/in_1	in_1	\N	2018-08-24 13:34:44.964465	2018-08-24 13:34:44.964469	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2117
5019	failing_network_2018-08-24T15-34-44/root/step_1/in_2	in_2	\N	2018-08-24 13:34:44.964908	2018-08-24 13:34:44.964912	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_2"]}	2117
5020	failing_network_2018-08-24T15-34-44/root/step_1/fail_1	fail_1	\N	2018-08-24 13:34:44.965311	2018-08-24 13:34:44.965314	{"input_group": "default", "datatype": "Boolean", "dimension_names": []}	2117
5021	failing_network_2018-08-24T15-34-44/root/step_1/fail_2	fail_2	\N	2018-08-24 13:34:44.96575	2018-08-24 13:34:44.965754	{"input_group": "default", "datatype": "Boolean", "dimension_names": ["const_step_1_fail_2_0"]}	2117
5022	failing_network_2018-08-24T15-34-44/root/step_1/v_in_port	•••	Virtual input port	2018-08-24 13:34:44.966138	2018-08-24 13:34:44.966142	null	2117
5023	failing_network_2018-08-24T15-34-44/root/step_2/in_1	in_1	\N	2018-08-24 13:34:44.966561	2018-08-24 13:34:44.966565	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_3"]}	2118
5024	failing_network_2018-08-24T15-34-44/root/step_2/in_2	in_2	\N	2018-08-24 13:34:44.967005	2018-08-24 13:34:44.967009	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2118
5025	failing_network_2018-08-24T15-34-44/root/step_2/fail_1	fail_1	\N	2018-08-24 13:34:44.967403	2018-08-24 13:34:44.967407	{"input_group": "default", "datatype": "Boolean", "dimension_names": ["const_step_2_fail_1_0"]}	2118
5026	failing_network_2018-08-24T15-34-44/root/step_2/fail_2	fail_2	\N	2018-08-24 13:34:44.967971	2018-08-24 13:34:44.967975	{"input_group": "default", "datatype": "Boolean", "dimension_names": []}	2118
5027	failing_network_2018-08-24T15-34-44/root/step_2/v_in_port	•••	Virtual input port	2018-08-24 13:34:44.96842	2018-08-24 13:34:44.968424	null	2118
5028	failing_network_2018-08-24T15-34-44/root/step_3/in_1	in_1	\N	2018-08-24 13:34:44.96885	2018-08-24 13:34:44.968854	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2119
5029	failing_network_2018-08-24T15-34-44/root/step_3/in_2	in_2	\N	2018-08-24 13:34:44.969239	2018-08-24 13:34:44.969243	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_3"]}	2119
5030	failing_network_2018-08-24T15-34-44/root/step_3/fail_1	fail_1	\N	2018-08-24 13:34:44.969691	2018-08-24 13:34:44.969695	{"input_group": "default", "datatype": "Boolean", "dimension_names": []}	2119
5031	failing_network_2018-08-24T15-34-44/root/step_3/fail_2	fail_2	\N	2018-08-24 13:34:44.97014	2018-08-24 13:34:44.970143	{"input_group": "default", "datatype": "Boolean", "dimension_names": []}	2119
5032	failing_network_2018-08-24T15-34-44/root/step_3/v_in_port	•••	Virtual input port	2018-08-24 13:34:44.97061	2018-08-24 13:34:44.970614	null	2119
5033	failing_network_2018-08-24T15-34-44/root/range/value	value	\N	2018-08-24 13:34:44.971175	2018-08-24 13:34:44.97118	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2120
5034	failing_network_2018-08-24T15-34-44/root/range/v_in_port	•••	Virtual input port	2018-08-24 13:34:44.971826	2018-08-24 13:34:44.97183	null	2120
5035	failing_network_2018-08-24T15-34-44/root/sum/values	values	\N	2018-08-24 13:34:44.972265	2018-08-24 13:34:44.97227	{"input_group": "default", "datatype": "Number", "dimension_names": ["source_1"]}	2121
5036	failing_network_2018-08-24T15-34-44/root/sum/v_in_port	•••	Virtual input port	2018-08-24 13:34:44.972738	2018-08-24 13:34:44.972742	null	2121
5037	failing_network_2018-08-24T15-34-44/root/const_step_1_fail_2_0/v_in_port	•••	Virtual input port	2018-08-24 13:34:44.973152	2018-08-24 13:34:44.973156	null	2122
5038	failing_network_2018-08-24T15-34-44/root/const_step_2_fail_1_0/v_in_port	•••	Virtual input port	2018-08-24 13:34:44.973652	2018-08-24 13:34:44.973656	null	2123
5203	example_2018-09-03T13-09-35/root/source1/input	input	\N	2018-09-03 11:09:36.240488	2018-09-03 11:09:36.240492	{"dimension_names": ["source1"], "datatype": "Int"}	2223
5204	example_2018-09-03T13-09-35/root/source1/v_in_port	•••	Virtual input port	2018-09-03 11:09:36.240941	2018-09-03 11:09:36.240944	null	2223
5205	example_2018-09-03T13-09-35/root/const_addint_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 11:09:36.241329	2018-09-03 11:09:36.241332	null	2224
5206	example_2018-09-03T13-11-45/root/addint/left_hand	left_hand	\N	2018-09-03 11:11:45.554314	2018-09-03 11:11:45.554323	{"input_group": "default", "dimension_names": ["source1"], "datatype": "Int"}	2226
5207	example_2018-09-03T13-11-45/root/addint/right_hand	right_hand	\N	2018-09-03 11:11:45.554901	2018-09-03 11:11:45.554905	{"input_group": "default", "dimension_names": ["const_addint_right_hand_0"], "datatype": "Int"}	2226
5208	example_2018-09-03T13-11-45/root/addint/v_in_port	•••	Virtual input port	2018-09-03 11:11:45.555398	2018-09-03 11:11:45.555402	null	2226
5209	example_2018-09-03T13-11-45/root/source1/input	input	\N	2018-09-03 11:11:45.555858	2018-09-03 11:11:45.555862	{"dimension_names": ["source1"], "datatype": "Int"}	2227
5210	example_2018-09-03T13-11-45/root/source1/v_in_port	•••	Virtual input port	2018-09-03 11:11:45.556326	2018-09-03 11:11:45.55633	null	2227
5211	example_2018-09-03T13-11-45/root/sink1/input	input	\N	2018-09-03 11:11:45.556848	2018-09-03 11:11:45.556852	{"input_group": "default", "dimension_names": ["source1"], "datatype": "Int"}	2228
5212	example_2018-09-03T13-11-45/root/sink1/v_in_port	•••	Virtual input port	2018-09-03 11:11:45.557313	2018-09-03 11:11:45.557316	null	2228
5240	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_2/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.796969	2018-09-03 12:01:00.796973	null	2249
5241	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_3/input	input	\N	2018-09-03 12:01:00.797397	2018-09-03 12:01:00.7974	{"datatype": "Int", "dimension_names": ["source_3"]}	2250
5242	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_3/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.797818	2018-09-03 12:01:00.797822	null	2250
5243	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_1/input	input	\N	2018-09-03 12:01:00.79822	2018-09-03 12:01:00.798224	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2251
5244	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_1/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.798645	2018-09-03 12:01:00.79865	null	2251
5245	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_2/input	input	\N	2018-09-03 12:01:00.799055	2018-09-03 12:01:00.799059	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_3"]}	2252
5246	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_2/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.799439	2018-09-03 12:01:00.799443	null	2252
5247	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_3/input	input	\N	2018-09-03 12:01:00.799863	2018-09-03 12:01:00.799866	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2253
5248	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_3/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.800303	2018-09-03 12:01:00.800306	null	2253
5249	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_4/input	input	\N	2018-09-03 12:01:00.800715	2018-09-03 12:01:00.800719	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2254
5250	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_4/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.801119	2018-09-03 12:01:00.801123	null	2254
5251	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_5/input	input	\N	2018-09-03 12:01:00.80154	2018-09-03 12:01:00.801544	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2255
5252	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_5/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.80196	2018-09-03 12:01:00.801964	null	2255
5253	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/in_1	in_1	\N	2018-09-03 12:01:00.802383	2018-09-03 12:01:00.802387	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2256
5254	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/in_2	in_2	\N	2018-09-03 12:01:00.802805	2018-09-03 12:01:00.802809	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_2"]}	2256
5255	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/fail_1	fail_1	\N	2018-09-03 12:01:00.803203	2018-09-03 12:01:00.803207	{"input_group": "default", "datatype": "Boolean", "dimension_names": []}	2256
5256	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/fail_2	fail_2	\N	2018-09-03 12:01:00.803595	2018-09-03 12:01:00.803608	{"input_group": "default", "datatype": "Boolean", "dimension_names": ["const_step_1_fail_2_0"]}	2256
5257	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.804021	2018-09-03 12:01:00.804025	null	2256
5258	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/in_1	in_1	\N	2018-09-03 12:01:00.804412	2018-09-03 12:01:00.804415	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_3"]}	2257
5259	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/in_2	in_2	\N	2018-09-03 12:01:00.804833	2018-09-03 12:01:00.804837	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2257
5260	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/fail_1	fail_1	\N	2018-09-03 12:01:00.805264	2018-09-03 12:01:00.805267	{"input_group": "default", "datatype": "Boolean", "dimension_names": ["const_step_2_fail_1_0"]}	2257
5261	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/fail_2	fail_2	\N	2018-09-03 12:01:00.805692	2018-09-03 12:01:00.805696	{"input_group": "default", "datatype": "Boolean", "dimension_names": []}	2257
5262	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.806101	2018-09-03 12:01:00.806105	null	2257
5263	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/in_1	in_1	\N	2018-09-03 12:01:00.806554	2018-09-03 12:01:00.806558	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2258
5264	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/in_2	in_2	\N	2018-09-03 12:01:00.806976	2018-09-03 12:01:00.80698	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_3"]}	2258
5265	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/fail_1	fail_1	\N	2018-09-03 12:01:00.807369	2018-09-03 12:01:00.807373	{"input_group": "default", "datatype": "Boolean", "dimension_names": []}	2258
5266	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/fail_2	fail_2	\N	2018-09-03 12:01:00.807806	2018-09-03 12:01:00.80781	{"input_group": "default", "datatype": "Boolean", "dimension_names": []}	2258
5267	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.80825	2018-09-03 12:01:00.808254	null	2258
5268	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range/value	value	\N	2018-09-03 12:01:00.808667	2018-09-03 12:01:00.808671	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2259
5269	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.809071	2018-09-03 12:01:00.809075	null	2259
5270	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sum/values	values	\N	2018-09-03 12:01:00.809461	2018-09-03 12:01:00.809465	{"input_group": "default", "datatype": "Number", "dimension_names": ["source_1"]}	2260
5271	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sum/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.809923	2018-09-03 12:01:00.809927	null	2260
5272	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_1_fail_2_0/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.810316	2018-09-03 12:01:00.81032	null	2261
5273	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_2_fail_1_0/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.810724	2018-09-03 12:01:00.810728	null	2262
5274	failing_macro_top_level_2018-09-03T14-01-00/root/add/left_hand	left_hand	\N	2018-09-03 12:01:00.811109	2018-09-03 12:01:00.811113	{"input_group": "default", "datatype": "Int", "dimension_names": []}	2245
5275	failing_macro_top_level_2018-09-03T14-01-00/root/add/right_hand	right_hand	\N	2018-09-03 12:01:00.811531	2018-09-03 12:01:00.811535	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_add_right_hand_0"]}	2245
5276	failing_macro_top_level_2018-09-03T14-01-00/root/add/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.811981	2018-09-03 12:01:00.811985	null	2245
5277	failing_macro_top_level_2018-09-03T14-01-00/root/sink/input	input	\N	2018-09-03 12:01:00.812433	2018-09-03 12:01:00.812437	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_a"]}	2246
5278	failing_macro_top_level_2018-09-03T14-01-00/root/sink/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.812909	2018-09-03 12:01:00.812913	null	2246
5279	failing_macro_top_level_2018-09-03T14-01-00/root/const_add_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 12:01:00.8133	2018-09-03 12:01:00.813304	null	2247
5280	add_ints_2018-09-03T14-04-33/root/source/input	input	\N	2018-09-03 12:04:33.327752	2018-09-03 12:04:33.327758	{"datatype": "Int", "dimension_names": ["source"]}	2264
5281	add_ints_2018-09-03T14-04-33/root/source/v_in_port	•••	Virtual input port	2018-09-03 12:04:33.328556	2018-09-03 12:04:33.32856	null	2264
5282	add_ints_2018-09-03T14-04-33/root/add/left_hand	left_hand	\N	2018-09-03 12:04:33.329109	2018-09-03 12:04:33.329113	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2265
5283	add_ints_2018-09-03T14-04-33/root/add/right_hand	right_hand	\N	2018-09-03 12:04:33.329581	2018-09-03 12:04:33.329585	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_add_right_hand_0"]}	2265
5284	add_ints_2018-09-03T14-04-33/root/add/v_in_port	•••	Virtual input port	2018-09-03 12:04:33.330104	2018-09-03 12:04:33.330108	null	2265
5285	add_ints_2018-09-03T14-04-33/root/const_add_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 12:04:33.330561	2018-09-03 12:04:33.330565	null	2266
5286	add_ints_2018-09-03T14-04-33/root/sink/input	input	\N	2018-09-03 12:04:33.33113	2018-09-03 12:04:33.331134	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2267
5287	add_ints_2018-09-03T14-04-33/root/sink/v_in_port	•••	Virtual input port	2018-09-03 12:04:33.331591	2018-09-03 12:04:33.331595	null	2267
5288	macro_node_2_2018-09-03T14-14-40/root/addint/left_hand	left_hand	\N	2018-09-03 12:14:40.51732	2018-09-03 12:14:40.517325	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1"]}	2269
5289	macro_node_2_2018-09-03T14-14-40/root/addint/right_hand	right_hand	\N	2018-09-03 12:14:40.517935	2018-09-03 12:14:40.517954	{"input_group": "right", "datatype": "Int", "dimension_names": ["source_2"]}	2269
5290	macro_node_2_2018-09-03T14-14-40/root/addint/v_in_port	•••	Virtual input port	2018-09-03 12:14:40.518414	2018-09-03 12:14:40.518418	null	2269
5291	macro_node_2_2018-09-03T14-14-40/root/source_1/input	input	\N	2018-09-03 12:14:40.51902	2018-09-03 12:14:40.519024	{"datatype": "Int", "dimension_names": ["source_1"]}	2270
5292	macro_node_2_2018-09-03T14-14-40/root/source_1/v_in_port	•••	Virtual input port	2018-09-03 12:14:40.519588	2018-09-03 12:14:40.519592	null	2270
5293	macro_node_2_2018-09-03T14-14-40/root/source_2/input	input	\N	2018-09-03 12:14:40.520077	2018-09-03 12:14:40.520081	{"datatype": "Int", "dimension_names": ["source_2"]}	2271
5294	macro_node_2_2018-09-03T14-14-40/root/source_2/v_in_port	•••	Virtual input port	2018-09-03 12:14:40.520721	2018-09-03 12:14:40.520726	null	2271
5295	macro_node_2_2018-09-03T14-14-40/root/source_3/input	input	\N	2018-09-03 12:14:40.521283	2018-09-03 12:14:40.521287	{"datatype": "Int", "dimension_names": ["source_3"]}	2272
5296	macro_node_2_2018-09-03T14-14-40/root/source_3/v_in_port	•••	Virtual input port	2018-09-03 12:14:40.521839	2018-09-03 12:14:40.521843	null	2272
5297	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/v_in_port	•••	Virtual input port	2018-09-03 12:14:40.522427	2018-09-03 12:14:40.52243	null	2273
5298	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1/input	input	\N	2018-09-03 12:14:40.523054	2018-09-03 12:14:40.523058	{"datatype": "Int", "dimension_names": ["macro_input_1"]}	2276
5299	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1/v_in_port	•••	Virtual input port	2018-09-03 12:14:40.523672	2018-09-03 12:14:40.523676	null	2276
5300	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_2/input	input	\N	2018-09-03 12:14:40.524161	2018-09-03 12:14:40.524165	{"datatype": "Int", "dimension_names": ["macro_input_2"]}	2277
5301	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_2/v_in_port	•••	Virtual input port	2018-09-03 12:14:40.52472	2018-09-03 12:14:40.524724	null	2277
5302	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/left_hand	left_hand	\N	2018-09-03 12:14:40.52518	2018-09-03 12:14:40.525184	{"input_group": "default", "datatype": "Int", "dimension_names": ["macro_input_1"]}	2278
5303	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/right_hand	right_hand	\N	2018-09-03 12:14:40.525725	2018-09-03 12:14:40.525729	{"input_group": "other", "datatype": "Int", "dimension_names": ["macro_input_2"]}	2278
5304	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/v_in_port	•••	Virtual input port	2018-09-03 12:14:40.526244	2018-09-03 12:14:40.526247	null	2278
5305	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/input	input	\N	2018-09-03 12:14:40.526752	2018-09-03 12:14:40.526756	{"input_group": "default", "datatype": "Int", "dimension_names": ["macro_input_1", "macro_input_2"]}	2279
5306	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/v_in_port	•••	Virtual input port	2018-09-03 12:14:40.527264	2018-09-03 12:14:40.527268	null	2279
5307	macro_node_2_2018-09-03T14-14-40/root/sum/values	values	\N	2018-09-03 12:14:40.527862	2018-09-03 12:14:40.527866	{"input_group": "default", "datatype": "Number", "dimension_names": ["source_1", "source_3"]}	2274
5308	macro_node_2_2018-09-03T14-14-40/root/sum/v_in_port	•••	Virtual input port	2018-09-03 12:14:40.528469	2018-09-03 12:14:40.528473	null	2274
5309	macro_node_2_2018-09-03T14-14-40/root/sink/input	input	\N	2018-09-03 12:14:40.528974	2018-09-03 12:14:40.528978	{"input_group": "default", "datatype": "Int", "dimension_names": ["source_1", "source_3"]}	2275
5310	macro_node_2_2018-09-03T14-14-40/root/sink/v_in_port	•••	Virtual input port	2018-09-03 12:14:40.52952	2018-09-03 12:14:40.529524	null	2275
5311	add_ints_2018-09-03T14-18-44/root/source/input	input	\N	2018-09-03 12:18:44.663219	2018-09-03 12:18:44.663224	{"datatype": "Int", "dimension_names": ["source"]}	2281
5312	add_ints_2018-09-03T14-18-44/root/source/v_in_port	•••	Virtual input port	2018-09-03 12:18:44.66387	2018-09-03 12:18:44.663874	null	2281
5313	add_ints_2018-09-03T14-18-44/root/add/left_hand	left_hand	\N	2018-09-03 12:18:44.664496	2018-09-03 12:18:44.6645	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2282
5314	add_ints_2018-09-03T14-18-44/root/add/right_hand	right_hand	\N	2018-09-03 12:18:44.665058	2018-09-03 12:18:44.665062	{"input_group": "default", "datatype": "Int", "dimension_names": ["const_add_right_hand_0"]}	2282
5315	add_ints_2018-09-03T14-18-44/root/add/v_in_port	•••	Virtual input port	2018-09-03 12:18:44.665559	2018-09-03 12:18:44.665563	null	2282
5316	add_ints_2018-09-03T14-18-44/root/const_add_right_hand_0/v_in_port	•••	Virtual input port	2018-09-03 12:18:44.666063	2018-09-03 12:18:44.666067	null	2283
5317	add_ints_2018-09-03T14-18-44/root/sink/input	input	\N	2018-09-03 12:18:44.666492	2018-09-03 12:18:44.666495	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2284
5318	add_ints_2018-09-03T14-18-44/root/sink/v_in_port	•••	Virtual input port	2018-09-03 12:18:44.666978	2018-09-03 12:18:44.666982	null	2284
5319	auto_prefix_test_2018-09-03T14-30-01/root/const/v_in_port	•••	Virtual input port	2018-09-03 12:30:02.211412	2018-09-03 12:30:02.21142	null	2286
5320	auto_prefix_test_2018-09-03T14-30-01/root/source/input	input	\N	2018-09-03 12:30:02.212099	2018-09-03 12:30:02.212103	{"datatype": "Int", "dimension_names": ["source"]}	2287
5321	auto_prefix_test_2018-09-03T14-30-01/root/source/v_in_port	•••	Virtual input port	2018-09-03 12:30:02.212662	2018-09-03 12:30:02.212666	null	2287
5322	auto_prefix_test_2018-09-03T14-30-01/root/m_p/left_hand	left_hand	\N	2018-09-03 12:30:02.213236	2018-09-03 12:30:02.213255	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2288
5323	auto_prefix_test_2018-09-03T14-30-01/root/m_p/right_hand	right_hand	\N	2018-09-03 12:30:02.213795	2018-09-03 12:30:02.213799	{"input_group": "default", "datatype": "Int", "dimension_names": ["const"]}	2288
5324	auto_prefix_test_2018-09-03T14-30-01/root/m_p/v_in_port	•••	Virtual input port	2018-09-03 12:30:02.214318	2018-09-03 12:30:02.214321	null	2288
5325	auto_prefix_test_2018-09-03T14-30-01/root/a_p/left_hand	left_hand	\N	2018-09-03 12:30:02.214849	2018-09-03 12:30:02.214853	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2289
5326	auto_prefix_test_2018-09-03T14-30-01/root/a_p/right_hand	right_hand	\N	2018-09-03 12:30:02.215597	2018-09-03 12:30:02.215601	{"input_group": "default", "datatype": "Int", "dimension_names": ["const"]}	2289
5327	auto_prefix_test_2018-09-03T14-30-01/root/a_p/v_in_port	•••	Virtual input port	2018-09-03 12:30:02.21625	2018-09-03 12:30:02.216255	null	2289
5328	auto_prefix_test_2018-09-03T14-30-01/root/m_n/left_hand	left_hand	\N	2018-09-03 12:30:02.216753	2018-09-03 12:30:02.216757	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2290
5329	auto_prefix_test_2018-09-03T14-30-01/root/m_n/right_hand	right_hand	\N	2018-09-03 12:30:02.217263	2018-09-03 12:30:02.217267	{"input_group": "default", "datatype": "Int", "dimension_names": ["const"]}	2290
5330	auto_prefix_test_2018-09-03T14-30-01/root/m_n/v_in_port	•••	Virtual input port	2018-09-03 12:30:02.217768	2018-09-03 12:30:02.217772	null	2290
5331	auto_prefix_test_2018-09-03T14-30-01/root/a_n/left_hand	left_hand	\N	2018-09-03 12:30:02.218279	2018-09-03 12:30:02.218283	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2291
5332	auto_prefix_test_2018-09-03T14-30-01/root/a_n/right_hand	right_hand	\N	2018-09-03 12:30:02.21888	2018-09-03 12:30:02.218884	{"input_group": "default", "datatype": "Int", "dimension_names": ["const"]}	2291
5333	auto_prefix_test_2018-09-03T14-30-01/root/a_n/v_in_port	•••	Virtual input port	2018-09-03 12:30:02.219369	2018-09-03 12:30:02.219374	null	2291
5334	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_p/input	input	\N	2018-09-03 12:30:02.219903	2018-09-03 12:30:02.219923	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2292
5335	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_p/v_in_port	•••	Virtual input port	2018-09-03 12:30:02.220438	2018-09-03 12:30:02.220442	null	2292
5336	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_p/input	input	\N	2018-09-03 12:30:02.221006	2018-09-03 12:30:02.22101	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2293
5337	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_p/v_in_port	•••	Virtual input port	2018-09-03 12:30:02.221575	2018-09-03 12:30:02.221579	null	2293
5338	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_n/input	input	\N	2018-09-03 12:30:02.222118	2018-09-03 12:30:02.222123	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2294
5339	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_n/v_in_port	•••	Virtual input port	2018-09-03 12:30:02.222659	2018-09-03 12:30:02.222663	null	2294
5340	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_n/input	input	\N	2018-09-03 12:30:02.223246	2018-09-03 12:30:02.22325	{"input_group": "default", "datatype": "Int", "dimension_names": ["source"]}	2295
5341	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_n/v_in_port	•••	Virtual input port	2018-09-03 12:30:02.223842	2018-09-03 12:30:02.223846	null	2295
5342	add_ints_2018-09-06T13-57-24/root/source/input	input	\N	2018-09-06 11:57:24.887512	2018-09-06 11:57:24.887519	{"dimension_names": ["source"], "datatype": "Int"}	2297
5343	add_ints_2018-09-06T13-57-24/root/source/v_in_port	•••	Virtual input port	2018-09-06 11:57:24.888654	2018-09-06 11:57:24.888658	null	2297
5344	add_ints_2018-09-06T13-57-24/root/const_add_right_hand_0/v_in_port	•••	Virtual input port	2018-09-06 11:57:24.889308	2018-09-06 11:57:24.889312	null	2298
5345	add_ints_2018-09-06T13-57-24/root/sink/input	input	\N	2018-09-06 11:57:24.889822	2018-09-06 11:57:24.889826	{"dimension_names": ["source"], "input_group": "default", "datatype": "Int"}	2299
5346	add_ints_2018-09-06T13-57-24/root/sink/v_in_port	•••	Virtual input port	2018-09-06 11:57:24.890385	2018-09-06 11:57:24.890388	null	2299
5347	add_ints_2018-09-06T13-57-24/root/add/left_hand	left_hand	\N	2018-09-06 11:57:24.891075	2018-09-06 11:57:24.891079	{"dimension_names": ["source"], "input_group": "default", "datatype": "Int"}	2300
5348	add_ints_2018-09-06T13-57-24/root/add/right_hand	right_hand	\N	2018-09-06 11:57:24.891651	2018-09-06 11:57:24.891655	{"dimension_names": ["const_add_right_hand_0"], "input_group": "default", "datatype": "Int"}	2300
5349	add_ints_2018-09-06T13-57-24/root/add/v_in_port	•••	Virtual input port	2018-09-06 11:57:24.892263	2018-09-06 11:57:24.892267	null	2300
5350	failing_network_2018-09-06T14-00-46/root/step_3/in_1	in_1	\N	2018-09-06 12:00:46.546843	2018-09-06 12:00:46.546848	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2302
5351	failing_network_2018-09-06T14-00-46/root/step_3/in_2	in_2	\N	2018-09-06 12:00:46.547551	2018-09-06 12:00:46.547555	{"input_group": "default", "dimension_names": ["source_3"], "datatype": "Int"}	2302
5352	failing_network_2018-09-06T14-00-46/root/step_3/fail_1	fail_1	\N	2018-09-06 12:00:46.548281	2018-09-06 12:00:46.548285	{"input_group": "default", "dimension_names": [], "datatype": "Boolean"}	2302
5353	failing_network_2018-09-06T14-00-46/root/step_3/fail_2	fail_2	\N	2018-09-06 12:00:46.548756	2018-09-06 12:00:46.54876	{"input_group": "default", "dimension_names": [], "datatype": "Boolean"}	2302
5354	failing_network_2018-09-06T14-00-46/root/step_3/v_in_port	•••	Virtual input port	2018-09-06 12:00:46.549239	2018-09-06 12:00:46.549243	null	2302
5355	failing_network_2018-09-06T14-00-46/root/const_step_2_fail_1_0/v_in_port	•••	Virtual input port	2018-09-06 12:00:46.549705	2018-09-06 12:00:46.549708	null	2303
5356	failing_network_2018-09-06T14-00-46/root/sink_4/input	input	\N	2018-09-06 12:00:46.55011	2018-09-06 12:00:46.550114	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2304
5357	failing_network_2018-09-06T14-00-46/root/sink_4/v_in_port	•••	Virtual input port	2018-09-06 12:00:46.550608	2018-09-06 12:00:46.550612	null	2304
5358	failing_network_2018-09-06T14-00-46/root/source_3/input	input	\N	2018-09-06 12:00:46.55111	2018-09-06 12:00:46.551114	{"dimension_names": ["source_3"], "datatype": "Int"}	2305
5359	failing_network_2018-09-06T14-00-46/root/source_3/v_in_port	•••	Virtual input port	2018-09-06 12:00:46.551662	2018-09-06 12:00:46.551666	null	2305
5360	failing_network_2018-09-06T14-00-46/root/step_1/in_1	in_1	\N	2018-09-06 12:00:46.552206	2018-09-06 12:00:46.55221	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2306
5361	failing_network_2018-09-06T14-00-46/root/step_1/in_2	in_2	\N	2018-09-06 12:00:46.552675	2018-09-06 12:00:46.552679	{"input_group": "default", "dimension_names": ["source_2"], "datatype": "Int"}	2306
5362	failing_network_2018-09-06T14-00-46/root/step_1/fail_1	fail_1	\N	2018-09-06 12:00:46.553148	2018-09-06 12:00:46.553151	{"input_group": "default", "dimension_names": [], "datatype": "Boolean"}	2306
5363	failing_network_2018-09-06T14-00-46/root/step_1/fail_2	fail_2	\N	2018-09-06 12:00:46.553625	2018-09-06 12:00:46.553629	{"input_group": "default", "dimension_names": ["const_step_1_fail_2_0"], "datatype": "Boolean"}	2306
5364	failing_network_2018-09-06T14-00-46/root/step_1/v_in_port	•••	Virtual input port	2018-09-06 12:00:46.554043	2018-09-06 12:00:46.554047	null	2306
5365	failing_network_2018-09-06T14-00-46/root/const_step_1_fail_2_0/v_in_port	•••	Virtual input port	2018-09-06 12:00:46.554523	2018-09-06 12:00:46.554527	null	2307
5366	failing_network_2018-09-06T14-00-46/root/step_2/in_1	in_1	\N	2018-09-06 12:00:46.55496	2018-09-06 12:00:46.554964	{"input_group": "default", "dimension_names": ["source_3"], "datatype": "Int"}	2308
5367	failing_network_2018-09-06T14-00-46/root/step_2/in_2	in_2	\N	2018-09-06 12:00:46.555501	2018-09-06 12:00:46.555505	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2308
5368	failing_network_2018-09-06T14-00-46/root/step_2/fail_1	fail_1	\N	2018-09-06 12:00:46.556077	2018-09-06 12:00:46.556096	{"input_group": "default", "dimension_names": ["const_step_2_fail_1_0"], "datatype": "Boolean"}	2308
5369	failing_network_2018-09-06T14-00-46/root/step_2/fail_2	fail_2	\N	2018-09-06 12:00:46.556619	2018-09-06 12:00:46.556623	{"input_group": "default", "dimension_names": [], "datatype": "Boolean"}	2308
5370	failing_network_2018-09-06T14-00-46/root/step_2/v_in_port	•••	Virtual input port	2018-09-06 12:00:46.557194	2018-09-06 12:00:46.557198	null	2308
5371	failing_network_2018-09-06T14-00-46/root/sink_3/input	input	\N	2018-09-06 12:00:46.557665	2018-09-06 12:00:46.557669	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2309
5372	failing_network_2018-09-06T14-00-46/root/sink_3/v_in_port	•••	Virtual input port	2018-09-06 12:00:46.558093	2018-09-06 12:00:46.558096	null	2309
5373	failing_network_2018-09-06T14-00-46/root/range/value	value	\N	2018-09-06 12:00:46.558642	2018-09-06 12:00:46.558646	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2310
5374	failing_network_2018-09-06T14-00-46/root/range/v_in_port	•••	Virtual input port	2018-09-06 12:00:46.559135	2018-09-06 12:00:46.559139	null	2310
5375	failing_network_2018-09-06T14-00-46/root/sum/values	values	\N	2018-09-06 12:00:46.559598	2018-09-06 12:00:46.559602	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Number"}	2311
5376	failing_network_2018-09-06T14-00-46/root/sum/v_in_port	•••	Virtual input port	2018-09-06 12:00:46.560014	2018-09-06 12:00:46.560017	null	2311
5377	failing_network_2018-09-06T14-00-46/root/sink_1/input	input	\N	2018-09-06 12:00:46.560498	2018-09-06 12:00:46.560502	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2312
5378	failing_network_2018-09-06T14-00-46/root/sink_1/v_in_port	•••	Virtual input port	2018-09-06 12:00:46.561009	2018-09-06 12:00:46.561013	null	2312
5379	failing_network_2018-09-06T14-00-46/root/source_2/input	input	\N	2018-09-06 12:00:46.561454	2018-09-06 12:00:46.561458	{"dimension_names": ["source_2"], "datatype": "Int"}	2313
5380	failing_network_2018-09-06T14-00-46/root/source_2/v_in_port	•••	Virtual input port	2018-09-06 12:00:46.561891	2018-09-06 12:00:46.561895	null	2313
5381	failing_network_2018-09-06T14-00-46/root/sink_2/input	input	\N	2018-09-06 12:00:46.562324	2018-09-06 12:00:46.562328	{"input_group": "default", "dimension_names": ["source_3"], "datatype": "Int"}	2314
5382	failing_network_2018-09-06T14-00-46/root/sink_2/v_in_port	•••	Virtual input port	2018-09-06 12:00:46.562778	2018-09-06 12:00:46.562782	null	2314
5383	failing_network_2018-09-06T14-00-46/root/sink_5/input	input	\N	2018-09-06 12:00:46.563185	2018-09-06 12:00:46.563189	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2315
5384	failing_network_2018-09-06T14-00-46/root/sink_5/v_in_port	•••	Virtual input port	2018-09-06 12:00:46.563663	2018-09-06 12:00:46.563666	null	2315
5385	failing_network_2018-09-06T14-00-46/root/source_1/input	input	\N	2018-09-06 12:00:46.564053	2018-09-06 12:00:46.564056	{"dimension_names": ["source_1"], "datatype": "Int"}	2316
5386	failing_network_2018-09-06T14-00-46/root/source_1/v_in_port	•••	Virtual input port	2018-09-06 12:00:46.564537	2018-09-06 12:00:46.56454	null	2316
5387	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.169929	2018-09-06 13:00:59.169937	null	2318
5388	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Num_Of_Iterations/input	input	\N	2018-09-06 13:00:59.171478	2018-09-06 13:00:59.171482	{"datatype": "String", "dimension_names": ["Num_Of_Iterations"]}	2330
5389	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Num_Of_Iterations/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.172235	2018-09-06 13:00:59.172239	null	2330
5390	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Step_Size/input	input	\N	2018-09-06 13:00:59.172738	2018-09-06 13:00:59.172742	{"datatype": "String", "dimension_names": ["Step_Size"]}	2331
5391	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Step_Size/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.173265	2018-09-06 13:00:59.173269	null	2331
5392	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Blur_FWHM/input	input	\N	2018-09-06 13:00:59.173748	2018-09-06 13:00:59.173752	{"datatype": "String", "dimension_names": ["Blur_FWHM"]}	2332
5393	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Blur_FWHM/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.174284	2018-09-06 13:00:59.174289	null	2332
5394	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Linear_Registration_Arguments/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.17475	2018-09-06 13:00:59.174754	null	2319
5395	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Linear_Registration_Arguments/Linear_Normalization/input	input	\N	2018-09-06 13:00:59.175158	2018-09-06 13:00:59.175162	{"datatype": "Boolean", "dimension_names": ["Linear_Normalization"]}	2333
5396	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Linear_Registration_Arguments/Linear_Normalization/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.175621	2018-09-06 13:00:59.175625	null	2333
5397	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.17606	2018-09-06 13:00:59.176064	null	2320
5398	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Linear_Estimate/input	input	\N	2018-09-06 13:00:59.176534	2018-09-06 13:00:59.176538	{"datatype": "String", "dimension_names": ["Linear_Estimate"]}	2334
5399	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Linear_Estimate/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.17694	2018-09-06 13:00:59.176943	null	2334
5400	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Num_Of_Registrations/input	input	\N	2018-09-06 13:00:59.177359	2018-09-06 13:00:59.177362	{"datatype": "Int", "dimension_names": ["Num_Of_Registrations"]}	2335
5401	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Num_Of_Registrations/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.177771	2018-09-06 13:00:59.177775	null	2335
5402	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Registration_Blur_FWHM/input	input	\N	2018-09-06 13:00:59.178169	2018-09-06 13:00:59.178172	{"datatype": "Int", "dimension_names": ["Registration_Blur_FWHM"]}	2336
5403	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Registration_Blur_FWHM/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.178613	2018-09-06 13:00:59.178616	null	2336
5404	IntracranialVolume_2018-09-06T15-00-57/root/Subject_Name/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.179019	2018-09-06 13:00:59.179023	null	2321
5405	IntracranialVolume_2018-09-06T15-00-57/root/Subject_Name/ICV_Subject_Name/input	input	\N	2018-09-06 13:00:59.179423	2018-09-06 13:00:59.179427	{"datatype": "String", "dimension_names": ["ICV_Subject_Name"]}	2337
5406	IntracranialVolume_2018-09-06T15-00-57/root/Subject_Name/ICV_Subject_Name/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.179913	2018-09-06 13:00:59.179917	null	2337
5407	IntracranialVolume_2018-09-06T15-00-57/root/Model_Name/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.180364	2018-09-06 13:00:59.180368	null	2322
5408	IntracranialVolume_2018-09-06T15-00-57/root/Model_Name/Model/input	input	\N	2018-09-06 13:00:59.180933	2018-09-06 13:00:59.180937	{"datatype": "String", "dimension_names": ["Model"]}	2338
5409	IntracranialVolume_2018-09-06T15-00-57/root/Model_Name/Model/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.181407	2018-09-06 13:00:59.181411	null	2338
5410	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.181835	2018-09-06 13:00:59.181857	null	2323
5411	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Weight/input	input	\N	2018-09-06 13:00:59.182333	2018-09-06 13:00:59.182337	{"datatype": "Float", "dimension_names": ["Weight"]}	2339
5412	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Weight/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.182784	2018-09-06 13:00:59.182788	null	2339
5413	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Stiffness/input	input	\N	2018-09-06 13:00:59.183401	2018-09-06 13:00:59.183405	{"datatype": "Float", "dimension_names": ["Stiffness"]}	2340
5414	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Stiffness/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.183876	2018-09-06 13:00:59.18388	null	2340
5415	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Similarity/input	input	\N	2018-09-06 13:00:59.184347	2018-09-06 13:00:59.184351	{"datatype": "Float", "dimension_names": ["Similarity"]}	2341
5416	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Similarity/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.184797	2018-09-06 13:00:59.184801	null	2341
5417	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Sub_lattice/input	input	\N	2018-09-06 13:00:59.185222	2018-09-06 13:00:59.185226	{"datatype": "Int", "dimension_names": ["Sub_lattice"]}	2342
5418	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Sub_lattice/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.185658	2018-09-06 13:00:59.185662	null	2342
5419	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Debug/input	input	\N	2018-09-06 13:00:59.18611	2018-09-06 13:00:59.186113	{"datatype": "Boolean", "dimension_names": ["Debug"]}	2343
5420	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Debug/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.186587	2018-09-06 13:00:59.186591	null	2343
5421	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Clobber/input	input	\N	2018-09-06 13:00:59.18705	2018-09-06 13:00:59.187054	{"datatype": "Boolean", "dimension_names": ["Clobber"]}	2344
5422	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Clobber/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.187553	2018-09-06 13:00:59.187557	null	2344
5423	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Non_linear/input	input	\N	2018-09-06 13:00:59.18811	2018-09-06 13:00:59.188114	{"datatype": "String", "dimension_names": ["Non_linear"]}	2345
5424	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Non_linear/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.188635	2018-09-06 13:00:59.188639	null	2345
5425	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Mask_To_Binary_Min/input	input	\N	2018-09-06 13:00:59.189096	2018-09-06 13:00:59.189099	{"datatype": "Float", "dimension_names": ["Mask_To_Binary_Min"]}	2346
5426	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Mask_To_Binary_Min/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.189558	2018-09-06 13:00:59.189562	null	2346
5427	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.189976	2018-09-06 13:00:59.18998	null	2324
5428	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Mask/input	input	\N	2018-09-06 13:00:59.190407	2018-09-06 13:00:59.190411	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["Delete_Files"]}	2347
5429	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Mask/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.190971	2018-09-06 13:00:59.190974	null	2347
5430	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Parameters/input	input	\N	2018-09-06 13:00:59.191538	2018-09-06 13:00:59.191541	{"datatype": "TxtFile", "input_group": "default", "dimension_names": ["Delete_Files"]}	2348
5431	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Parameters/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.191985	2018-09-06 13:00:59.191989	null	2348
5432	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Volume/input	input	\N	2018-09-06 13:00:59.192496	2018-09-06 13:00:59.192499	{"datatype": "TxtFile", "input_group": "default", "dimension_names": ["Delete_Files"]}	2349
5433	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Volume/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.193008	2018-09-06 13:00:59.193012	null	2349
5434	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_Log/input	input	\N	2018-09-06 13:00:59.193471	2018-09-06 13:00:59.193475	{"datatype": "TxtFile", "input_group": "default", "dimension_names": ["Delete_Files"]}	2350
5435	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_Log/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.194035	2018-09-06 13:00:59.194039	null	2350
5436	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Original_Subject/input	input	\N	2018-09-06 13:00:59.19451	2018-09-06 13:00:59.194514	{"datatype": "NiftiImageFileCompressed", "input_group": "default", "dimension_names": ["Delete_Files"]}	2351
5437	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Original_Subject/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.194985	2018-09-06 13:00:59.194989	null	2351
5438	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_JPG/input	input	\N	2018-09-06 13:00:59.195422	2018-09-06 13:00:59.195426	{"datatype": "JPGFile", "input_group": "default", "dimension_names": ["Delete_Files"]}	2352
5439	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_JPG/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.195842	2018-09-06 13:00:59.195846	null	2352
5440	IntracranialVolume_2018-09-06T15-00-57/root/Delete_Files/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.196332	2018-09-06 13:00:59.196336	null	2325
5441	IntracranialVolume_2018-09-06T15-00-57/root/Delete_Files/Delete_Files/input	input	\N	2018-09-06 13:00:59.196725	2018-09-06 13:00:59.196729	{"datatype": "Boolean", "dimension_names": ["Delete_Files"]}	2353
5442	IntracranialVolume_2018-09-06T15-00-57/root/Delete_Files/Delete_Files/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.197118	2018-09-06 13:00:59.197122	null	2353
5443	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.197522	2018-09-06 13:00:59.197525	null	2326
5444	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Delete_Files	Delete_Files	\N	2018-09-06 13:00:59.197912	2018-09-06 13:00:59.197916	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["Delete_Files"]}	2354
5445	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Help_Argument	Help_Argument	\N	2018-09-06 13:00:59.198348	2018-09-06 13:00:59.198352	{"datatype": "String", "input_group": "default", "dimension_names": []}	2354
5446	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Model	Model	\N	2018-09-06 13:00:59.198796	2018-09-06 13:00:59.198799	{"datatype": "String", "input_group": "default", "dimension_names": ["Model"]}	2354
5447	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Parameters_File	Parameters_File	\N	2018-09-06 13:00:59.199244	2018-09-06 13:00:59.199247	{"datatype": "String", "input_group": "default", "dimension_names": []}	2354
5448	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Processed_File	Processed_File	\N	2018-09-06 13:00:59.199653	2018-09-06 13:00:59.199657	{"datatype": "ICVInputFiletypes", "input_group": "default", "dimension_names": []}	2354
5449	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Processed_Folder	Processed_Folder	\N	2018-09-06 13:00:59.200065	2018-09-06 13:00:59.200069	{"datatype": "Directory", "input_group": "default", "dimension_names": []}	2354
5486	failing_network_2018-09-06T15-03-16/root/sum/values	values	\N	2018-09-06 13:03:16.74397	2018-09-06 13:03:16.743974	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Number"}	2361
5450	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Processed_XNAT_DICOM	Processed_XNAT_DICOM	\N	2018-09-06 13:00:59.200596	2018-09-06 13:00:59.2006	{"datatype": "DicomImageFile", "input_group": "default", "dimension_names": ["Input_To_Process"]}	2354
5451	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Linear_Estimate	Linear_Estimate	\N	2018-09-06 13:00:59.201139	2018-09-06 13:00:59.201143	{"datatype": "String", "input_group": "default", "dimension_names": ["Linear_Estimate"]}	2354
5452	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Num_Of_Registrations	Num_Of_Registrations	\N	2018-09-06 13:00:59.201615	2018-09-06 13:00:59.201619	{"datatype": "Int", "input_group": "default", "dimension_names": ["Num_Of_Registrations"]}	2354
5453	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Registration_Blur_FWHM	Registration_Blur_FWHM	\N	2018-09-06 13:00:59.202079	2018-09-06 13:00:59.202083	{"datatype": "Int", "input_group": "default", "dimension_names": ["Registration_Blur_FWHM"]}	2354
5454	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Linear_Normalization	Linear_Normalization	\N	2018-09-06 13:00:59.202511	2018-09-06 13:00:59.202515	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["Linear_Normalization"]}	2354
5455	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Num_Of_Iterations	Num_Of_Iterations	\N	2018-09-06 13:00:59.203022	2018-09-06 13:00:59.203026	{"datatype": "String", "input_group": "default", "dimension_names": ["Num_Of_Iterations"]}	2354
5456	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Step_Size	Step_Size	\N	2018-09-06 13:00:59.203518	2018-09-06 13:00:59.203522	{"datatype": "String", "input_group": "default", "dimension_names": ["Step_Size"]}	2354
5457	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Blur_FWHM	Blur_FWHM	\N	2018-09-06 13:00:59.204217	2018-09-06 13:00:59.204221	{"datatype": "String", "input_group": "default", "dimension_names": ["Blur_FWHM"]}	2354
5458	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Weight	Weight	\N	2018-09-06 13:00:59.204664	2018-09-06 13:00:59.204668	{"datatype": "Float", "input_group": "default", "dimension_names": ["Weight"]}	2354
5459	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Stiffness	Stiffness	\N	2018-09-06 13:00:59.2051	2018-09-06 13:00:59.205104	{"datatype": "Float", "input_group": "default", "dimension_names": ["Stiffness"]}	2354
5460	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Similarity	Similarity	\N	2018-09-06 13:00:59.205565	2018-09-06 13:00:59.205569	{"datatype": "Float", "input_group": "default", "dimension_names": ["Similarity"]}	2354
5461	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Sub_lattice	Sub_lattice	\N	2018-09-06 13:00:59.206073	2018-09-06 13:00:59.206077	{"datatype": "Int", "input_group": "default", "dimension_names": ["Sub_lattice"]}	2354
5462	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Debug	Debug	\N	2018-09-06 13:00:59.206556	2018-09-06 13:00:59.20656	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["Debug"]}	2354
5463	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Clobber	Clobber	\N	2018-09-06 13:00:59.207313	2018-09-06 13:00:59.207317	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["Clobber"]}	2354
5464	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Non_linear	Non_linear	\N	2018-09-06 13:00:59.20776	2018-09-06 13:00:59.207764	{"datatype": "String", "input_group": "default", "dimension_names": ["Non_linear"]}	2354
5465	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/NonLinear_Normalization	NonLinear_Normalization	\N	2018-09-06 13:00:59.208281	2018-09-06 13:00:59.208285	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["NonLinear_Normalization"]}	2354
5466	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Mask_To_Binary_Min	Mask_To_Binary_Min	\N	2018-09-06 13:00:59.208743	2018-09-06 13:00:59.208747	{"datatype": "Float", "input_group": "default", "dimension_names": ["Mask_To_Binary_Min"]}	2354
5467	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/RUN_GUI	RUN_GUI	\N	2018-09-06 13:00:59.209163	2018-09-06 13:00:59.209167	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	2354
5468	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Calc_Crude	Calc_Crude	\N	2018-09-06 13:00:59.209603	2018-09-06 13:00:59.209607	{"datatype": "Boolean", "input_group": "default", "dimension_names": ["Calc_Crude"]}	2354
5469	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Calc_Fine	Calc_Fine	\N	2018-09-06 13:00:59.210082	2018-09-06 13:00:59.210086	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	2354
5470	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Clac_Ave_Brain	Clac_Ave_Brain	\N	2018-09-06 13:00:59.210542	2018-09-06 13:00:59.210546	{"datatype": "Boolean", "input_group": "default", "dimension_names": []}	2354
5471	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Subject_Name	Subject_Name	\N	2018-09-06 13:00:59.210958	2018-09-06 13:00:59.210962	{"datatype": "String", "input_group": "default", "dimension_names": ["ICV_Subject_Name"]}	2354
5472	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.211382	2018-09-06 13:00:59.211386	null	2354
5473	IntracranialVolume_2018-09-06T15-00-57/root/Operation_Type/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.211848	2018-09-06 13:00:59.211852	null	2327
5474	IntracranialVolume_2018-09-06T15-00-57/root/Operation_Type/Calc_Crude/input	input	\N	2018-09-06 13:00:59.212341	2018-09-06 13:00:59.212345	{"datatype": "Boolean", "dimension_names": ["Calc_Crude"]}	2355
5475	IntracranialVolume_2018-09-06T15-00-57/root/Operation_Type/Calc_Crude/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.212788	2018-09-06 13:00:59.212792	null	2355
5476	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Extended_Arguments/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.213179	2018-09-06 13:00:59.213183	null	2328
5477	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Extended_Arguments/NonLinear_Normalization/input	input	\N	2018-09-06 13:00:59.213611	2018-09-06 13:00:59.213615	{"datatype": "Boolean", "dimension_names": ["NonLinear_Normalization"]}	2356
5478	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Extended_Arguments/NonLinear_Normalization/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.214007	2018-09-06 13:00:59.214011	null	2356
5479	IntracranialVolume_2018-09-06T15-00-57/root/Program_File_Arguments/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.214435	2018-09-06 13:00:59.214439	null	2329
5480	IntracranialVolume_2018-09-06T15-00-57/root/Program_File_Arguments/Input_To_Process/input	input	\N	2018-09-06 13:00:59.214908	2018-09-06 13:00:59.214912	{"datatype": "DicomImageFile", "dimension_names": ["Input_To_Process"]}	2357
5481	IntracranialVolume_2018-09-06T15-00-57/root/Program_File_Arguments/Input_To_Process/v_in_port	•••	Virtual input port	2018-09-06 13:00:59.21544	2018-09-06 13:00:59.215444	null	2357
5482	failing_network_2018-09-06T15-03-16/root/sink_4/input	input	\N	2018-09-06 13:03:16.74163	2018-09-06 13:03:16.741636	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2359
5483	failing_network_2018-09-06T15-03-16/root/sink_4/v_in_port	•••	Virtual input port	2018-09-06 13:03:16.742392	2018-09-06 13:03:16.742397	null	2359
5484	failing_network_2018-09-06T15-03-16/root/sink_2/input	input	\N	2018-09-06 13:03:16.742933	2018-09-06 13:03:16.742937	{"input_group": "default", "dimension_names": ["source_3"], "datatype": "Int"}	2360
5485	failing_network_2018-09-06T15-03-16/root/sink_2/v_in_port	•••	Virtual input port	2018-09-06 13:03:16.743474	2018-09-06 13:03:16.743478	null	2360
5487	failing_network_2018-09-06T15-03-16/root/sum/v_in_port	•••	Virtual input port	2018-09-06 13:03:16.744467	2018-09-06 13:03:16.744471	null	2361
5488	failing_network_2018-09-06T15-03-16/root/const_step_1_fail_2_0/v_in_port	•••	Virtual input port	2018-09-06 13:03:16.745083	2018-09-06 13:03:16.745087	null	2362
5489	failing_network_2018-09-06T15-03-16/root/range/value	value	\N	2018-09-06 13:03:16.745614	2018-09-06 13:03:16.745618	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2363
5490	failing_network_2018-09-06T15-03-16/root/range/v_in_port	•••	Virtual input port	2018-09-06 13:03:16.746091	2018-09-06 13:03:16.746095	null	2363
5491	failing_network_2018-09-06T15-03-16/root/source_1/input	input	\N	2018-09-06 13:03:16.746556	2018-09-06 13:03:16.74656	{"dimension_names": ["source_1"], "datatype": "Int"}	2364
5492	failing_network_2018-09-06T15-03-16/root/source_1/v_in_port	•••	Virtual input port	2018-09-06 13:03:16.74713	2018-09-06 13:03:16.747134	null	2364
5493	failing_network_2018-09-06T15-03-16/root/const_step_2_fail_1_0/v_in_port	•••	Virtual input port	2018-09-06 13:03:16.747667	2018-09-06 13:03:16.74767	null	2365
5494	failing_network_2018-09-06T15-03-16/root/step_2/in_1	in_1	\N	2018-09-06 13:03:16.748208	2018-09-06 13:03:16.748213	{"input_group": "default", "dimension_names": ["source_3"], "datatype": "Int"}	2366
5495	failing_network_2018-09-06T15-03-16/root/step_2/in_2	in_2	\N	2018-09-06 13:03:16.748638	2018-09-06 13:03:16.748642	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2366
5496	failing_network_2018-09-06T15-03-16/root/step_2/fail_1	fail_1	\N	2018-09-06 13:03:16.749174	2018-09-06 13:03:16.749178	{"input_group": "default", "dimension_names": ["const_step_2_fail_1_0"], "datatype": "Boolean"}	2366
5497	failing_network_2018-09-06T15-03-16/root/step_2/fail_2	fail_2	\N	2018-09-06 13:03:16.749667	2018-09-06 13:03:16.749671	{"input_group": "default", "dimension_names": [], "datatype": "Boolean"}	2366
5498	failing_network_2018-09-06T15-03-16/root/step_2/v_in_port	•••	Virtual input port	2018-09-06 13:03:16.750165	2018-09-06 13:03:16.750169	null	2366
5499	failing_network_2018-09-06T15-03-16/root/source_3/input	input	\N	2018-09-06 13:03:16.750655	2018-09-06 13:03:16.750659	{"dimension_names": ["source_3"], "datatype": "Int"}	2367
5500	failing_network_2018-09-06T15-03-16/root/source_3/v_in_port	•••	Virtual input port	2018-09-06 13:03:16.751228	2018-09-06 13:03:16.751232	null	2367
5501	failing_network_2018-09-06T15-03-16/root/sink_3/input	input	\N	2018-09-06 13:03:16.751685	2018-09-06 13:03:16.751689	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2368
5502	failing_network_2018-09-06T15-03-16/root/sink_3/v_in_port	•••	Virtual input port	2018-09-06 13:03:16.752221	2018-09-06 13:03:16.752225	null	2368
5503	failing_network_2018-09-06T15-03-16/root/sink_5/input	input	\N	2018-09-06 13:03:16.752647	2018-09-06 13:03:16.752651	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2369
5504	failing_network_2018-09-06T15-03-16/root/sink_5/v_in_port	•••	Virtual input port	2018-09-06 13:03:16.753064	2018-09-06 13:03:16.753068	null	2369
5505	failing_network_2018-09-06T15-03-16/root/step_1/in_1	in_1	\N	2018-09-06 13:03:16.753508	2018-09-06 13:03:16.753512	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2370
5506	failing_network_2018-09-06T15-03-16/root/step_1/in_2	in_2	\N	2018-09-06 13:03:16.753916	2018-09-06 13:03:16.75392	{"input_group": "default", "dimension_names": ["source_2"], "datatype": "Int"}	2370
5507	failing_network_2018-09-06T15-03-16/root/step_1/fail_1	fail_1	\N	2018-09-06 13:03:16.754343	2018-09-06 13:03:16.754347	{"input_group": "default", "dimension_names": [], "datatype": "Boolean"}	2370
5508	failing_network_2018-09-06T15-03-16/root/step_1/fail_2	fail_2	\N	2018-09-06 13:03:16.754749	2018-09-06 13:03:16.754753	{"input_group": "default", "dimension_names": ["const_step_1_fail_2_0"], "datatype": "Boolean"}	2370
5509	failing_network_2018-09-06T15-03-16/root/step_1/v_in_port	•••	Virtual input port	2018-09-06 13:03:16.75517	2018-09-06 13:03:16.755174	null	2370
5510	failing_network_2018-09-06T15-03-16/root/step_3/in_1	in_1	\N	2018-09-06 13:03:16.755609	2018-09-06 13:03:16.755612	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2371
5511	failing_network_2018-09-06T15-03-16/root/step_3/in_2	in_2	\N	2018-09-06 13:03:16.756058	2018-09-06 13:03:16.756062	{"input_group": "default", "dimension_names": ["source_3"], "datatype": "Int"}	2371
5512	failing_network_2018-09-06T15-03-16/root/step_3/fail_1	fail_1	\N	2018-09-06 13:03:16.756538	2018-09-06 13:03:16.756542	{"input_group": "default", "dimension_names": [], "datatype": "Boolean"}	2371
5513	failing_network_2018-09-06T15-03-16/root/step_3/fail_2	fail_2	\N	2018-09-06 13:03:16.756965	2018-09-06 13:03:16.756968	{"input_group": "default", "dimension_names": [], "datatype": "Boolean"}	2371
5514	failing_network_2018-09-06T15-03-16/root/step_3/v_in_port	•••	Virtual input port	2018-09-06 13:03:16.757397	2018-09-06 13:03:16.757401	null	2371
5515	failing_network_2018-09-06T15-03-16/root/source_2/input	input	\N	2018-09-06 13:03:16.757797	2018-09-06 13:03:16.757801	{"dimension_names": ["source_2"], "datatype": "Int"}	2372
5516	failing_network_2018-09-06T15-03-16/root/source_2/v_in_port	•••	Virtual input port	2018-09-06 13:03:16.758341	2018-09-06 13:03:16.758345	null	2372
5517	failing_network_2018-09-06T15-03-16/root/sink_1/input	input	\N	2018-09-06 13:03:16.758772	2018-09-06 13:03:16.758776	{"input_group": "default", "dimension_names": ["source_1"], "datatype": "Int"}	2373
5518	failing_network_2018-09-06T15-03-16/root/sink_1/v_in_port	•••	Virtual input port	2018-09-06 13:03:16.759377	2018-09-06 13:03:16.759381	null	2373
5519	add_ints_2018-09-07T12-00-02/root/const_add_right_hand_0/v_in_port	•••	Virtual input port	2018-09-07 10:00:02.666815	2018-09-07 10:00:02.66683	null	2375
5520	add_ints_2018-09-07T12-00-02/root/source/input	input	\N	2018-09-07 10:00:02.6679	2018-09-07 10:00:02.667906	{"datatype": "Int", "dimension_names": ["source"]}	2376
5521	add_ints_2018-09-07T12-00-02/root/source/v_in_port	•••	Virtual input port	2018-09-07 10:00:02.668626	2018-09-07 10:00:02.668631	null	2376
5522	add_ints_2018-09-07T12-00-02/root/add/left_hand	left_hand	\N	2018-09-07 10:00:02.669276	2018-09-07 10:00:02.66928	{"datatype": "Int", "dimension_names": ["source"], "input_group": "default"}	2377
5523	add_ints_2018-09-07T12-00-02/root/add/right_hand	right_hand	\N	2018-09-07 10:00:02.669983	2018-09-07 10:00:02.669988	{"datatype": "Int", "dimension_names": ["const_add_right_hand_0"], "input_group": "default"}	2377
5524	add_ints_2018-09-07T12-00-02/root/add/v_in_port	•••	Virtual input port	2018-09-07 10:00:02.670527	2018-09-07 10:00:02.670531	null	2377
5525	add_ints_2018-09-07T12-00-02/root/sink/input	input	\N	2018-09-07 10:00:02.671057	2018-09-07 10:00:02.671061	{"datatype": "Int", "dimension_names": ["source"], "input_group": "default"}	2378
5526	add_ints_2018-09-07T12-00-02/root/sink/v_in_port	•••	Virtual input port	2018-09-07 10:00:02.67154	2018-09-07 10:00:02.671544	null	2378
5528	iris/root/0_statistics/9_const_get_gm_volume_flatten_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.877702	2018-09-08 13:18:12.877707	null	2388
5529	iris/root/0_statistics/11_get_gm_volume/in_ddof	in_ddof		2018-09-08 13:18:12.878455	2018-09-08 13:18:12.878459	{}	2389
5530	iris/root/0_statistics/11_get_gm_volume/in_stddev	in_stddev		2018-09-08 13:18:12.87904	2018-09-08 13:18:12.879044	{}	2389
5531	iris/root/0_statistics/11_get_gm_volume/in_label	in_label		2018-09-08 13:18:12.87975	2018-09-08 13:18:12.879754	{}	2389
5532	iris/root/0_statistics/11_get_gm_volume/in_image	in_image		2018-09-08 13:18:12.880269	2018-09-08 13:18:12.880273	{}	2389
5533	iris/root/0_statistics/11_get_gm_volume/in_mean	in_mean		2018-09-08 13:18:12.880764	2018-09-08 13:18:12.880768	{}	2389
5534	iris/root/0_statistics/11_get_gm_volume/in_volume	in_volume		2018-09-08 13:18:12.881262	2018-09-08 13:18:12.881266	{}	2389
5535	iris/root/0_statistics/11_get_gm_volume/in_selection	in_selection		2018-09-08 13:18:12.881955	2018-09-08 13:18:12.881959	{}	2389
5536	iris/root/0_statistics/11_get_gm_volume/in_flatten	in_flatten		2018-09-08 13:18:12.882495	2018-09-08 13:18:12.882499	{}	2389
5537	iris/root/0_statistics/11_get_gm_volume/in_mask	in_mask		2018-09-08 13:18:12.883021	2018-09-08 13:18:12.883025	{}	2389
5538	iris/root/0_statistics/11_get_gm_volume/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.883501	2018-09-08 13:18:12.883505	null	2389
5539	iris/root/0_statistics/14_get_brain_volume/in_ddof	in_ddof		2018-09-08 13:18:12.883972	2018-09-08 13:18:12.883976	{}	2390
5540	iris/root/0_statistics/14_get_brain_volume/in_stddev	in_stddev		2018-09-08 13:18:12.884423	2018-09-08 13:18:12.884427	{}	2390
5541	iris/root/0_statistics/14_get_brain_volume/in_label	in_label		2018-09-08 13:18:12.884917	2018-09-08 13:18:12.884921	{}	2390
5542	iris/root/0_statistics/14_get_brain_volume/in_image	in_image		2018-09-08 13:18:12.885321	2018-09-08 13:18:12.885325	{}	2390
5543	iris/root/0_statistics/14_get_brain_volume/in_mean	in_mean		2018-09-08 13:18:12.885768	2018-09-08 13:18:12.885772	{}	2390
5544	iris/root/0_statistics/14_get_brain_volume/in_volume	in_volume		2018-09-08 13:18:12.886209	2018-09-08 13:18:12.886213	{}	2390
5545	iris/root/0_statistics/14_get_brain_volume/in_selection	in_selection		2018-09-08 13:18:12.886622	2018-09-08 13:18:12.886626	{}	2390
5546	iris/root/0_statistics/14_get_brain_volume/in_flatten	in_flatten		2018-09-08 13:18:12.887122	2018-09-08 13:18:12.887126	{}	2390
5547	iris/root/0_statistics/14_get_brain_volume/in_mask	in_mask		2018-09-08 13:18:12.887581	2018-09-08 13:18:12.887585	{}	2390
5548	iris/root/0_statistics/14_get_brain_volume/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.888152	2018-09-08 13:18:12.888156	null	2390
5549	iris/root/0_statistics/18_const_get_wm_volume_flatten_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.888619	2018-09-08 13:18:12.888623	null	2391
5550	iris/root/0_statistics/19_mask_hammers_labels/in_left_hand	in_left_hand		2018-09-08 13:18:12.889104	2018-09-08 13:18:12.889107	{}	2392
5551	iris/root/0_statistics/19_mask_hammers_labels/in_operator	in_operator		2018-09-08 13:18:12.889526	2018-09-08 13:18:12.88953	{}	2392
5552	iris/root/0_statistics/19_mask_hammers_labels/in_right_hand	in_right_hand		2018-09-08 13:18:12.890012	2018-09-08 13:18:12.890016	{}	2392
5553	iris/root/0_statistics/19_mask_hammers_labels/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.890651	2018-09-08 13:18:12.890655	null	2392
5554	iris/root/0_statistics/25_wm_volume/in_input	in_input		2018-09-08 13:18:12.891188	2018-09-08 13:18:12.891191	{}	2393
5555	iris/root/0_statistics/25_wm_volume/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.89161	2018-09-08 13:18:12.891614	null	2393
5556	iris/root/0_statistics/26_const_get_wm_volume_volume_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.892074	2018-09-08 13:18:12.892078	null	2394
5557	iris/root/0_statistics/39_const_get_gm_volume_volume_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.892617	2018-09-08 13:18:12.892621	null	2395
5558	iris/root/0_statistics/41_const_mask_hammers_labels_operator_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.893157	2018-09-08 13:18:12.893161	null	2396
5559	iris/root/0_statistics/45_const_get_wm_volume_selection_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.893667	2018-09-08 13:18:12.893671	null	2397
5560	iris/root/0_statistics/48_const_get_brain_volume_flatten_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.894154	2018-09-08 13:18:12.894157	null	2398
5561	iris/root/0_statistics/52_const_get_brain_volume_selection_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.894657	2018-09-08 13:18:12.894661	null	2399
5562	iris/root/0_statistics/55_brain_volume/in_input	in_input		2018-09-08 13:18:12.89512	2018-09-08 13:18:12.895124	{}	2400
5563	iris/root/0_statistics/55_brain_volume/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.8956	2018-09-08 13:18:12.895604	null	2400
5564	iris/root/0_statistics/60_const_get_brain_volume_volume_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.89616	2018-09-08 13:18:12.896164	null	2401
5565	iris/root/0_statistics/126_get_wm_volume/in_ddof	in_ddof		2018-09-08 13:18:12.896737	2018-09-08 13:18:12.896741	{}	2402
5566	iris/root/0_statistics/126_get_wm_volume/in_stddev	in_stddev		2018-09-08 13:18:12.897228	2018-09-08 13:18:12.897232	{}	2402
5567	iris/root/0_statistics/126_get_wm_volume/in_label	in_label		2018-09-08 13:18:12.897777	2018-09-08 13:18:12.897781	{}	2402
5568	iris/root/0_statistics/126_get_wm_volume/in_image	in_image		2018-09-08 13:18:12.898277	2018-09-08 13:18:12.898281	{}	2402
5569	iris/root/0_statistics/126_get_wm_volume/in_mean	in_mean		2018-09-08 13:18:12.898789	2018-09-08 13:18:12.898793	{}	2402
5570	iris/root/0_statistics/126_get_wm_volume/in_volume	in_volume		2018-09-08 13:18:12.899253	2018-09-08 13:18:12.899257	{}	2402
5571	iris/root/0_statistics/126_get_wm_volume/in_selection	in_selection		2018-09-08 13:18:12.899754	2018-09-08 13:18:12.899757	{}	2402
5572	iris/root/0_statistics/126_get_wm_volume/in_flatten	in_flatten		2018-09-08 13:18:12.900256	2018-09-08 13:18:12.90026	{}	2402
5573	iris/root/0_statistics/126_get_wm_volume/in_mask	in_mask		2018-09-08 13:18:12.900791	2018-09-08 13:18:12.900795	{}	2402
5574	iris/root/0_statistics/126_get_wm_volume/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.901254	2018-09-08 13:18:12.901258	null	2402
5575	iris/root/0_statistics/128_gm_volume/in_input	in_input		2018-09-08 13:18:12.901754	2018-09-08 13:18:12.901758	{}	2403
5576	iris/root/0_statistics/128_gm_volume/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.902222	2018-09-08 13:18:12.902226	null	2403
5577	iris/root/0_statistics/129_const_get_gm_volume_selection_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.902692	2018-09-08 13:18:12.902695	null	2404
5578	iris/root/1_dicom_to_nifti/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.903159	2018-09-08 13:18:12.903163	null	2381
5579	iris/root/1_dicom_to_nifti/20_t1_dicom_to_nifti/in_threshold	in_threshold		2018-09-08 13:18:12.903639	2018-09-08 13:18:12.903643	{}	2405
5580	iris/root/1_dicom_to_nifti/20_t1_dicom_to_nifti/in_file_order	in_file_order		2018-09-08 13:18:12.904152	2018-09-08 13:18:12.904156	{}	2405
5581	iris/root/1_dicom_to_nifti/20_t1_dicom_to_nifti/in_dicom_image	in_dicom_image		2018-09-08 13:18:12.904719	2018-09-08 13:18:12.904723	{}	2405
5582	iris/root/1_dicom_to_nifti/20_t1_dicom_to_nifti/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.905604	2018-09-08 13:18:12.905608	null	2405
5583	iris/root/1_dicom_to_nifti/44_reformat_t1/in_image	in_image		2018-09-08 13:18:12.906155	2018-09-08 13:18:12.906159	{}	2406
5584	iris/root/1_dicom_to_nifti/44_reformat_t1/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.906665	2018-09-08 13:18:12.906669	null	2406
5585	iris/root/1_dicom_to_nifti/61_const_t1_dicom_to_nifti_file_order_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.907122	2018-09-08 13:18:12.907125	null	2407
5586	iris/root/2_hammers_atlas/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.907649	2018-09-08 13:18:12.907653	null	2382
5587	iris/root/2_hammers_atlas/8_hammers_t1w/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.90814	2018-09-08 13:18:12.908145	null	2408
5588	iris/root/2_hammers_atlas/36_hammers_labels/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.908635	2018-09-08 13:18:12.908639	null	2409
5589	iris/root/2_hammers_atlas/50_hammers_brains/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.909209	2018-09-08 13:18:12.909213	null	2410
5590	iris/root/2_hammers_atlas/64_hammers_brains_r5/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.909682	2018-09-08 13:18:12.909686	null	2411
5591	iris/root/3_subjects/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.910209	2018-09-08 13:18:12.910213	null	2383
5592	iris/root/3_subjects/34_t1w_dicom/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.910682	2018-09-08 13:18:12.910686	null	2412
5593	iris/root/4_output/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.911125	2018-09-08 13:18:12.911129	null	2384
5594	iris/root/4_output/16_wm_map_output/in_input	in_input		2018-09-08 13:18:12.911618	2018-09-08 13:18:12.911622	{}	2413
5595	iris/root/4_output/16_wm_map_output/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.912133	2018-09-08 13:18:12.912137	null	2413
5596	iris/root/4_output/23_multi_atlas_segm/in_input	in_input		2018-09-08 13:18:12.912619	2018-09-08 13:18:12.912623	{}	2414
5597	iris/root/4_output/23_multi_atlas_segm/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.91308	2018-09-08 13:18:12.913084	null	2414
5598	iris/root/4_output/29_t1_nifti_images/in_input	in_input		2018-09-08 13:18:12.913586	2018-09-08 13:18:12.91359	{}	2415
5599	iris/root/4_output/29_t1_nifti_images/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.914076	2018-09-08 13:18:12.91408	null	2415
5600	iris/root/4_output/40_gm_map_output/in_input	in_input		2018-09-08 13:18:12.914534	2018-09-08 13:18:12.914539	{}	2416
5601	iris/root/4_output/40_gm_map_output/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.915058	2018-09-08 13:18:12.915062	null	2416
5602	iris/root/4_output/57_hammer_brain_mask/in_input	in_input		2018-09-08 13:18:12.915534	2018-09-08 13:18:12.915538	{}	2417
5603	iris/root/4_output/57_hammer_brain_mask/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.916081	2018-09-08 13:18:12.916085	null	2417
5604	iris/root/5_BET_and_REG/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.916622	2018-09-08 13:18:12.916626	null	2385
5605	iris/root/5_BET_and_REG/7_threshold_labels/in_power	in_power		2018-09-08 13:18:12.91714	2018-09-08 13:18:12.917144	{}	2418
5606	iris/root/5_BET_and_REG/7_threshold_labels/in_number_of_controlpoints	in_number_of_controlpoints		2018-09-08 13:18:12.917724	2018-09-08 13:18:12.917728	{}	2418
5607	iris/root/5_BET_and_REG/7_threshold_labels/in_method	in_method		2018-09-08 13:18:12.91832	2018-09-08 13:18:12.918324	{}	2418
5608	iris/root/5_BET_and_REG/7_threshold_labels/in_image	in_image		2018-09-08 13:18:12.918895	2018-09-08 13:18:12.91891	{}	2418
5609	iris/root/5_BET_and_REG/7_threshold_labels/in_upper_threshold	in_upper_threshold		2018-09-08 13:18:12.919386	2018-09-08 13:18:12.919389	{}	2418
5610	iris/root/5_BET_and_REG/7_threshold_labels/in_number_of_samples	in_number_of_samples		2018-09-08 13:18:12.919976	2018-09-08 13:18:12.91998	{}	2418
5611	iris/root/5_BET_and_REG/7_threshold_labels/in_lower_threshold	in_lower_threshold		2018-09-08 13:18:12.920461	2018-09-08 13:18:12.920465	{}	2418
5612	iris/root/5_BET_and_REG/7_threshold_labels/in_number_of_histbins	in_number_of_histbins		2018-09-08 13:18:12.920953	2018-09-08 13:18:12.920957	{}	2418
5613	iris/root/5_BET_and_REG/7_threshold_labels/in_number_of_iterations	in_number_of_iterations		2018-09-08 13:18:12.921384	2018-09-08 13:18:12.921388	{}	2418
5614	iris/root/5_BET_and_REG/7_threshold_labels/in_compression_flag	in_compression_flag		2018-09-08 13:18:12.921897	2018-09-08 13:18:12.921901	{}	2418
5615	iris/root/5_BET_and_REG/7_threshold_labels/in_mixture_type	in_mixture_type		2018-09-08 13:18:12.922324	2018-09-08 13:18:12.922328	{}	2418
5616	iris/root/5_BET_and_REG/7_threshold_labels/in_number_of_thresholds	in_number_of_thresholds		2018-09-08 13:18:12.922793	2018-09-08 13:18:12.922797	{}	2418
5617	iris/root/5_BET_and_REG/7_threshold_labels/in_inside_value	in_inside_value		2018-09-08 13:18:12.923319	2018-09-08 13:18:12.923323	{}	2418
5618	iris/root/5_BET_and_REG/7_threshold_labels/in_radius	in_radius		2018-09-08 13:18:12.923806	2018-09-08 13:18:12.92381	{}	2418
5619	iris/root/5_BET_and_REG/7_threshold_labels/in_mask_value	in_mask_value		2018-09-08 13:18:12.924277	2018-09-08 13:18:12.924281	{}	2418
5620	iris/root/5_BET_and_REG/7_threshold_labels/in_number_of_levels	in_number_of_levels		2018-09-08 13:18:12.924788	2018-09-08 13:18:12.924792	{}	2418
5621	iris/root/5_BET_and_REG/7_threshold_labels/in_spline_order	in_spline_order		2018-09-08 13:18:12.925269	2018-09-08 13:18:12.925273	{}	2418
5622	iris/root/5_BET_and_REG/7_threshold_labels/in_outside_value	in_outside_value		2018-09-08 13:18:12.925882	2018-09-08 13:18:12.925886	{}	2418
5623	iris/root/5_BET_and_REG/7_threshold_labels/in_mask	in_mask		2018-09-08 13:18:12.92636	2018-09-08 13:18:12.926364	{}	2418
5624	iris/root/5_BET_and_REG/7_threshold_labels/in_sigma	in_sigma		2018-09-08 13:18:12.926981	2018-09-08 13:18:12.926986	{}	2418
5625	iris/root/5_BET_and_REG/7_threshold_labels/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.927682	2018-09-08 13:18:12.927688	null	2418
5626	iris/root/5_BET_and_REG/10_const_bet_fraction_threshold_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.928375	2018-09-08 13:18:12.928382	null	2419
5627	iris/root/5_BET_and_REG/12_t1w_registration/in_moving_image	in_moving_image		2018-09-08 13:18:12.929057	2018-09-08 13:18:12.929063	{}	2420
5628	iris/root/5_BET_and_REG/12_t1w_registration/in_parameters	in_parameters		2018-09-08 13:18:12.929767	2018-09-08 13:18:12.929774	{}	2420
5629	iris/root/5_BET_and_REG/12_t1w_registration/in_threads	in_threads		2018-09-08 13:18:12.930409	2018-09-08 13:18:12.930414	{}	2420
5630	iris/root/5_BET_and_REG/12_t1w_registration/in_priority	in_priority		2018-09-08 13:18:12.931031	2018-09-08 13:18:12.931035	{}	2420
5631	iris/root/5_BET_and_REG/12_t1w_registration/in_fixed_image	in_fixed_image		2018-09-08 13:18:12.931515	2018-09-08 13:18:12.931519	{}	2420
5632	iris/root/5_BET_and_REG/12_t1w_registration/in_initial_transform	in_initial_transform		2018-09-08 13:18:12.931981	2018-09-08 13:18:12.931985	{}	2420
5633	iris/root/5_BET_and_REG/12_t1w_registration/in_fixed_mask	in_fixed_mask		2018-09-08 13:18:12.932439	2018-09-08 13:18:12.932442	{}	2420
5634	iris/root/5_BET_and_REG/12_t1w_registration/in_moving_mask	in_moving_mask		2018-09-08 13:18:12.932845	2018-09-08 13:18:12.932864	{}	2420
5635	iris/root/5_BET_and_REG/12_t1w_registration/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.933338	2018-09-08 13:18:12.933341	null	2420
5636	iris/root/5_BET_and_REG/13_const_threshold_labels_upper_threshold_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.933743	2018-09-08 13:18:12.933746	null	2421
5637	iris/root/5_BET_and_REG/15_const_bet_bias_cleanup_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.934202	2018-09-08 13:18:12.934206	null	2422
5638	iris/root/5_BET_and_REG/17_hammers_brain_transform/in_transform	in_transform		2018-09-08 13:18:12.934771	2018-09-08 13:18:12.934774	{}	2423
5639	iris/root/5_BET_and_REG/17_hammers_brain_transform/in_jacobian_matrix_flag	in_jacobian_matrix_flag		2018-09-08 13:18:12.935239	2018-09-08 13:18:12.935243	{}	2423
5640	iris/root/5_BET_and_REG/17_hammers_brain_transform/in_threads	in_threads		2018-09-08 13:18:12.935684	2018-09-08 13:18:12.935688	{}	2423
5641	iris/root/5_BET_and_REG/17_hammers_brain_transform/in_image	in_image		2018-09-08 13:18:12.936242	2018-09-08 13:18:12.936245	{}	2423
5642	iris/root/5_BET_and_REG/17_hammers_brain_transform/in_points	in_points		2018-09-08 13:18:12.936651	2018-09-08 13:18:12.936655	{}	2423
5643	iris/root/5_BET_and_REG/17_hammers_brain_transform/in_priority	in_priority		2018-09-08 13:18:12.937106	2018-09-08 13:18:12.937109	{}	2423
5644	iris/root/5_BET_and_REG/17_hammers_brain_transform/in_determinant_of_jacobian_flag	in_determinant_of_jacobian_flag		2018-09-08 13:18:12.937732	2018-09-08 13:18:12.937736	{}	2423
5645	iris/root/5_BET_and_REG/17_hammers_brain_transform/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.938261	2018-09-08 13:18:12.938265	null	2423
5703	iris/root/5_BET_and_REG/56_brain_mask_registation/in_initial_transform	in_initial_transform		2018-09-08 13:18:12.966609	2018-09-08 13:18:12.966613	{}	2438
5646	iris/root/5_BET_and_REG/22_const_brain_mask_registation_parameters_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.938792	2018-09-08 13:18:12.938796	null	2424
5647	iris/root/5_BET_and_REG/24_const_combine_labels_method_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.939396	2018-09-08 13:18:12.939399	null	2425
5648	iris/root/5_BET_and_REG/27_const_hammers_brain_transform_threads_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.939934	2018-09-08 13:18:12.939938	null	2426
5649	iris/root/5_BET_and_REG/28_n4_bias_correction/in_n4_parameters	in_n4_parameters		2018-09-08 13:18:12.94042	2018-09-08 13:18:12.940424	{}	2427
5650	iris/root/5_BET_and_REG/28_n4_bias_correction/in_mask	in_mask		2018-09-08 13:18:12.94093	2018-09-08 13:18:12.940933	{}	2427
5651	iris/root/5_BET_and_REG/28_n4_bias_correction/in_image	in_image		2018-09-08 13:18:12.941376	2018-09-08 13:18:12.94138	{}	2427
5652	iris/root/5_BET_and_REG/28_n4_bias_correction/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.941924	2018-09-08 13:18:12.941931	null	2427
5653	iris/root/5_BET_and_REG/30_combine_labels/in_number_of_classes	in_number_of_classes		2018-09-08 13:18:12.942414	2018-09-08 13:18:12.942418	{}	2428
5654	iris/root/5_BET_and_REG/30_combine_labels/in_images	in_images		2018-09-08 13:18:12.942905	2018-09-08 13:18:12.942909	{}	2428
5655	iris/root/5_BET_and_REG/30_combine_labels/in_method	in_method		2018-09-08 13:18:12.943313	2018-09-08 13:18:12.943317	{}	2428
5656	iris/root/5_BET_and_REG/30_combine_labels/in_substitute_labels	in_substitute_labels		2018-09-08 13:18:12.94375	2018-09-08 13:18:12.943754	{}	2428
5657	iris/root/5_BET_and_REG/30_combine_labels/in_original_labels	in_original_labels		2018-09-08 13:18:12.944213	2018-09-08 13:18:12.944217	{}	2428
5658	iris/root/5_BET_and_REG/30_combine_labels/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.944663	2018-09-08 13:18:12.944667	null	2428
5659	iris/root/5_BET_and_REG/32_const_dilate_brainmask_operation_type_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.945089	2018-09-08 13:18:12.945093	null	2429
5660	iris/root/5_BET_and_REG/33_label_registration/in_transform	in_transform		2018-09-08 13:18:12.945549	2018-09-08 13:18:12.945552	{}	2430
5661	iris/root/5_BET_and_REG/33_label_registration/in_jacobian_matrix_flag	in_jacobian_matrix_flag		2018-09-08 13:18:12.946028	2018-09-08 13:18:12.946032	{}	2430
5662	iris/root/5_BET_and_REG/33_label_registration/in_threads	in_threads		2018-09-08 13:18:12.94648	2018-09-08 13:18:12.946484	{}	2430
5663	iris/root/5_BET_and_REG/33_label_registration/in_image	in_image		2018-09-08 13:18:12.946999	2018-09-08 13:18:12.947003	{}	2430
5664	iris/root/5_BET_and_REG/33_label_registration/in_points	in_points		2018-09-08 13:18:12.947449	2018-09-08 13:18:12.947453	{}	2430
5665	iris/root/5_BET_and_REG/33_label_registration/in_priority	in_priority		2018-09-08 13:18:12.948046	2018-09-08 13:18:12.94805	{}	2430
5666	iris/root/5_BET_and_REG/33_label_registration/in_determinant_of_jacobian_flag	in_determinant_of_jacobian_flag		2018-09-08 13:18:12.948502	2018-09-08 13:18:12.948507	{}	2430
5667	iris/root/5_BET_and_REG/33_label_registration/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.949015	2018-09-08 13:18:12.949018	null	2430
5668	iris/root/5_BET_and_REG/35_const_dilate_brainmask_radius_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.949563	2018-09-08 13:18:12.949567	null	2431
5669	iris/root/5_BET_and_REG/38_threshold_brain/in_power	in_power		2018-09-08 13:18:12.950086	2018-09-08 13:18:12.95009	{}	2432
5670	iris/root/5_BET_and_REG/38_threshold_brain/in_number_of_controlpoints	in_number_of_controlpoints		2018-09-08 13:18:12.950554	2018-09-08 13:18:12.950558	{}	2432
5671	iris/root/5_BET_and_REG/38_threshold_brain/in_method	in_method		2018-09-08 13:18:12.951051	2018-09-08 13:18:12.951055	{}	2432
5672	iris/root/5_BET_and_REG/38_threshold_brain/in_image	in_image		2018-09-08 13:18:12.95154	2018-09-08 13:18:12.951544	{}	2432
5673	iris/root/5_BET_and_REG/38_threshold_brain/in_upper_threshold	in_upper_threshold		2018-09-08 13:18:12.95199	2018-09-08 13:18:12.951994	{}	2432
5674	iris/root/5_BET_and_REG/38_threshold_brain/in_number_of_samples	in_number_of_samples		2018-09-08 13:18:12.952463	2018-09-08 13:18:12.952467	{}	2432
5675	iris/root/5_BET_and_REG/38_threshold_brain/in_lower_threshold	in_lower_threshold		2018-09-08 13:18:12.952952	2018-09-08 13:18:12.952956	{}	2432
5676	iris/root/5_BET_and_REG/38_threshold_brain/in_number_of_histbins	in_number_of_histbins		2018-09-08 13:18:12.953404	2018-09-08 13:18:12.953408	{}	2432
5677	iris/root/5_BET_and_REG/38_threshold_brain/in_number_of_iterations	in_number_of_iterations		2018-09-08 13:18:12.953905	2018-09-08 13:18:12.953909	{}	2432
5678	iris/root/5_BET_and_REG/38_threshold_brain/in_compression_flag	in_compression_flag		2018-09-08 13:18:12.954422	2018-09-08 13:18:12.954426	{}	2432
5679	iris/root/5_BET_and_REG/38_threshold_brain/in_mixture_type	in_mixture_type		2018-09-08 13:18:12.954892	2018-09-08 13:18:12.954896	{}	2432
5680	iris/root/5_BET_and_REG/38_threshold_brain/in_number_of_thresholds	in_number_of_thresholds		2018-09-08 13:18:12.955308	2018-09-08 13:18:12.955312	{}	2432
5681	iris/root/5_BET_and_REG/38_threshold_brain/in_inside_value	in_inside_value		2018-09-08 13:18:12.95572	2018-09-08 13:18:12.955724	{}	2432
5682	iris/root/5_BET_and_REG/38_threshold_brain/in_radius	in_radius		2018-09-08 13:18:12.956172	2018-09-08 13:18:12.956176	{}	2432
5683	iris/root/5_BET_and_REG/38_threshold_brain/in_mask_value	in_mask_value		2018-09-08 13:18:12.956641	2018-09-08 13:18:12.956644	{}	2432
5684	iris/root/5_BET_and_REG/38_threshold_brain/in_number_of_levels	in_number_of_levels		2018-09-08 13:18:12.957099	2018-09-08 13:18:12.957104	{}	2432
5685	iris/root/5_BET_and_REG/38_threshold_brain/in_spline_order	in_spline_order		2018-09-08 13:18:12.957584	2018-09-08 13:18:12.957592	{}	2432
5686	iris/root/5_BET_and_REG/38_threshold_brain/in_outside_value	in_outside_value		2018-09-08 13:18:12.958053	2018-09-08 13:18:12.958057	{}	2432
5687	iris/root/5_BET_and_REG/38_threshold_brain/in_mask	in_mask		2018-09-08 13:18:12.958537	2018-09-08 13:18:12.958541	{}	2432
5688	iris/root/5_BET_and_REG/38_threshold_brain/in_sigma	in_sigma		2018-09-08 13:18:12.958996	2018-09-08 13:18:12.959001	{}	2432
5689	iris/root/5_BET_and_REG/38_threshold_brain/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.959445	2018-09-08 13:18:12.959449	null	2432
5690	iris/root/5_BET_and_REG/43_convert_brain/in_component_type	in_component_type		2018-09-08 13:18:12.959926	2018-09-08 13:18:12.95993	{}	2433
5691	iris/root/5_BET_and_REG/43_convert_brain/in_image	in_image		2018-09-08 13:18:12.960349	2018-09-08 13:18:12.960353	{}	2433
5692	iris/root/5_BET_and_REG/43_convert_brain/in_compression_flag	in_compression_flag		2018-09-08 13:18:12.960905	2018-09-08 13:18:12.960909	{}	2433
5693	iris/root/5_BET_and_REG/43_convert_brain/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.961321	2018-09-08 13:18:12.961325	null	2433
5694	iris/root/5_BET_and_REG/47_const_convert_brain_component_type_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.961876	2018-09-08 13:18:12.96188	null	2434
5695	iris/root/5_BET_and_REG/49_const_t1w_registration_parameters_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.962298	2018-09-08 13:18:12.962302	null	2435
5696	iris/root/5_BET_and_REG/51_const_combine_brains_number_of_classes_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.962699	2018-09-08 13:18:12.962703	null	2436
5697	iris/root/5_BET_and_REG/53_const_label_registration_threads_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.96319	2018-09-08 13:18:12.963194	null	2437
5698	iris/root/5_BET_and_REG/56_brain_mask_registation/in_moving_image	in_moving_image		2018-09-08 13:18:12.96368	2018-09-08 13:18:12.963684	{}	2438
5699	iris/root/5_BET_and_REG/56_brain_mask_registation/in_parameters	in_parameters		2018-09-08 13:18:12.96416	2018-09-08 13:18:12.964164	{}	2438
5700	iris/root/5_BET_and_REG/56_brain_mask_registation/in_threads	in_threads		2018-09-08 13:18:12.964689	2018-09-08 13:18:12.964693	{}	2438
5701	iris/root/5_BET_and_REG/56_brain_mask_registation/in_priority	in_priority		2018-09-08 13:18:12.965207	2018-09-08 13:18:12.965211	{}	2438
5702	iris/root/5_BET_and_REG/56_brain_mask_registation/in_fixed_image	in_fixed_image		2018-09-08 13:18:12.96608	2018-09-08 13:18:12.966084	{}	2438
5704	iris/root/5_BET_and_REG/56_brain_mask_registation/in_fixed_mask	in_fixed_mask		2018-09-08 13:18:12.967152	2018-09-08 13:18:12.967156	{}	2438
5705	iris/root/5_BET_and_REG/56_brain_mask_registation/in_moving_mask	in_moving_mask		2018-09-08 13:18:12.967738	2018-09-08 13:18:12.967742	{}	2438
5706	iris/root/5_BET_and_REG/56_brain_mask_registation/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.968221	2018-09-08 13:18:12.968225	null	2438
5707	iris/root/5_BET_and_REG/58_const_combine_brains_method_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.968683	2018-09-08 13:18:12.968687	null	2439
5708	iris/root/5_BET_and_REG/59_const_t1w_registration_threads_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.969157	2018-09-08 13:18:12.969161	null	2440
5709	iris/root/5_BET_and_REG/63_const_brain_mask_registation_threads_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.969664	2018-09-08 13:18:12.969668	null	2441
5710	iris/root/5_BET_and_REG/121_combine_brains/in_number_of_classes	in_number_of_classes		2018-09-08 13:18:12.970217	2018-09-08 13:18:12.970221	{}	2442
5711	iris/root/5_BET_and_REG/121_combine_brains/in_images	in_images		2018-09-08 13:18:12.970723	2018-09-08 13:18:12.970729	{}	2442
5712	iris/root/5_BET_and_REG/121_combine_brains/in_method	in_method		2018-09-08 13:18:12.971237	2018-09-08 13:18:12.971241	{}	2442
5713	iris/root/5_BET_and_REG/121_combine_brains/in_substitute_labels	in_substitute_labels		2018-09-08 13:18:12.971647	2018-09-08 13:18:12.971651	{}	2442
5714	iris/root/5_BET_and_REG/121_combine_brains/in_original_labels	in_original_labels		2018-09-08 13:18:12.972073	2018-09-08 13:18:12.972077	{}	2442
5715	iris/root/5_BET_and_REG/121_combine_brains/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.972489	2018-09-08 13:18:12.972493	null	2442
5716	iris/root/5_BET_and_REG/124_const_combine_labels_number_of_classes_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.972905	2018-09-08 13:18:12.972909	null	2443
5717	iris/root/5_BET_and_REG/125_dilate_brainmask/in_operation	in_operation		2018-09-08 13:18:12.973304	2018-09-08 13:18:12.973308	{}	2444
5718	iris/root/5_BET_and_REG/125_dilate_brainmask/in_foreground_background_value	in_foreground_background_value		2018-09-08 13:18:12.973721	2018-09-08 13:18:12.973725	{}	2444
5719	iris/root/5_BET_and_REG/125_dilate_brainmask/in_radius	in_radius		2018-09-08 13:18:12.974162	2018-09-08 13:18:12.974172	{}	2444
5720	iris/root/5_BET_and_REG/125_dilate_brainmask/in_boundary_condition	in_boundary_condition		2018-09-08 13:18:12.974667	2018-09-08 13:18:12.974671	{}	2444
5721	iris/root/5_BET_and_REG/125_dilate_brainmask/in_image	in_image		2018-09-08 13:18:12.975098	2018-09-08 13:18:12.975103	{}	2444
5722	iris/root/5_BET_and_REG/125_dilate_brainmask/in_operation_type	in_operation_type		2018-09-08 13:18:12.975614	2018-09-08 13:18:12.975618	{}	2444
5723	iris/root/5_BET_and_REG/125_dilate_brainmask/in_component_type	in_component_type		2018-09-08 13:18:12.976086	2018-09-08 13:18:12.97609	{}	2444
5724	iris/root/5_BET_and_REG/125_dilate_brainmask/in_gradient_algorithm_enum	in_gradient_algorithm_enum		2018-09-08 13:18:12.976597	2018-09-08 13:18:12.976603	{}	2444
5725	iris/root/5_BET_and_REG/125_dilate_brainmask/in_compression_flag	in_compression_flag		2018-09-08 13:18:12.977089	2018-09-08 13:18:12.977093	{}	2444
5726	iris/root/5_BET_and_REG/125_dilate_brainmask/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.977541	2018-09-08 13:18:12.977545	null	2444
5727	iris/root/5_BET_and_REG/130_bet/in_head_radius	in_head_radius		2018-09-08 13:18:12.977991	2018-09-08 13:18:12.977995	{}	2445
5728	iris/root/5_BET_and_REG/130_bet/in_image	in_image		2018-09-08 13:18:12.978428	2018-09-08 13:18:12.978432	{}	2445
5729	iris/root/5_BET_and_REG/130_bet/in_fraction_gradient	in_fraction_gradient		2018-09-08 13:18:12.978896	2018-09-08 13:18:12.9789	{}	2445
5730	iris/root/5_BET_and_REG/130_bet/in_fraction_threshold	in_fraction_threshold		2018-09-08 13:18:12.979317	2018-09-08 13:18:12.979321	{}	2445
5731	iris/root/5_BET_and_REG/130_bet/in_small_fov	in_small_fov		2018-09-08 13:18:12.979807	2018-09-08 13:18:12.979811	{}	2445
5732	iris/root/5_BET_and_REG/130_bet/in_apply_threshold	in_apply_threshold		2018-09-08 13:18:12.980239	2018-09-08 13:18:12.980243	{}	2445
5733	iris/root/5_BET_and_REG/130_bet/in_T2_image	in_T2_image		2018-09-08 13:18:12.980637	2018-09-08 13:18:12.980641	{}	2445
5734	iris/root/5_BET_and_REG/130_bet/in_apply_4D	in_apply_4D		2018-09-08 13:18:12.981071	2018-09-08 13:18:12.981074	{}	2445
5735	iris/root/5_BET_and_REG/130_bet/in_eye_cleanup	in_eye_cleanup		2018-09-08 13:18:12.9815	2018-09-08 13:18:12.981504	{}	2445
5736	iris/root/5_BET_and_REG/130_bet/in_robust_estimation	in_robust_estimation		2018-09-08 13:18:12.982002	2018-09-08 13:18:12.982006	{}	2445
5737	iris/root/5_BET_and_REG/130_bet/in_run_bet2_surf	in_run_bet2_surf		2018-09-08 13:18:12.982441	2018-09-08 13:18:12.982445	{}	2445
5738	iris/root/5_BET_and_REG/130_bet/in_bias_cleanup	in_bias_cleanup		2018-09-08 13:18:12.982922	2018-09-08 13:18:12.982926	{}	2445
5739	iris/root/5_BET_and_REG/130_bet/in_center_of_gravity	in_center_of_gravity		2018-09-08 13:18:12.983324	2018-09-08 13:18:12.983328	{}	2445
5740	iris/root/5_BET_and_REG/130_bet/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.983793	2018-09-08 13:18:12.983797	null	2445
5741	iris/root/5_BET_and_REG/131_const_dilate_brainmask_operation_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.984247	2018-09-08 13:18:12.984251	null	2446
5742	iris/root/6_qc_overview/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.984685	2018-09-08 13:18:12.984689	null	2386
5743	iris/root/6_qc_overview/21_t1_with_hammers/in_segmentation	in_segmentation		2018-09-08 13:18:12.985146	2018-09-08 13:18:12.98515	{}	2447
5744	iris/root/6_qc_overview/21_t1_with_hammers/in_image	in_image		2018-09-08 13:18:12.985675	2018-09-08 13:18:12.985679	{}	2447
5745	iris/root/6_qc_overview/21_t1_with_hammers/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.986149	2018-09-08 13:18:12.986153	null	2447
5746	iris/root/6_qc_overview/31_t1_with_hammers_sink/in_input	in_input		2018-09-08 13:18:12.986626	2018-09-08 13:18:12.98663	{}	2448
5747	iris/root/6_qc_overview/31_t1_with_hammers_sink/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.987055	2018-09-08 13:18:12.987059	null	2448
5748	iris/root/6_qc_overview/37_t1_with_wm_outline_sink/in_input	in_input		2018-09-08 13:18:12.98746	2018-09-08 13:18:12.987464	{}	2449
5749	iris/root/6_qc_overview/37_t1_with_wm_outline_sink/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.987865	2018-09-08 13:18:12.987869	null	2449
5750	iris/root/6_qc_overview/42_t1_with_gm_outline/in_segmentation	in_segmentation		2018-09-08 13:18:12.988284	2018-09-08 13:18:12.988288	{}	2450
5751	iris/root/6_qc_overview/42_t1_with_gm_outline/in_image	in_image		2018-09-08 13:18:12.988706	2018-09-08 13:18:12.98871	{}	2450
5752	iris/root/6_qc_overview/42_t1_with_gm_outline/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.989148	2018-09-08 13:18:12.989152	null	2450
5753	iris/root/6_qc_overview/46_t1_qc/in_image	in_image		2018-09-08 13:18:12.989604	2018-09-08 13:18:12.98962	{}	2451
5754	iris/root/6_qc_overview/46_t1_qc/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.990086	2018-09-08 13:18:12.99009	null	2451
5755	iris/root/6_qc_overview/54_t1_qc_sink/in_input	in_input		2018-09-08 13:18:12.990587	2018-09-08 13:18:12.990591	{}	2452
5756	iris/root/6_qc_overview/54_t1_qc_sink/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.991117	2018-09-08 13:18:12.991121	null	2452
5757	iris/root/6_qc_overview/62_t1_with_gm_outline_sink/in_input	in_input		2018-09-08 13:18:12.991606	2018-09-08 13:18:12.99161	{}	2453
5758	iris/root/6_qc_overview/62_t1_with_gm_outline_sink/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.992066	2018-09-08 13:18:12.99207	null	2453
5759	iris/root/6_qc_overview/122_t1_with_brain/in_segmentation	in_segmentation		2018-09-08 13:18:12.992535	2018-09-08 13:18:12.992538	{}	2454
5760	iris/root/6_qc_overview/122_t1_with_brain/in_image	in_image		2018-09-08 13:18:12.993084	2018-09-08 13:18:12.993087	{}	2454
5761	iris/root/6_qc_overview/122_t1_with_brain/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.993555	2018-09-08 13:18:12.993559	null	2454
5762	iris/root/6_qc_overview/123_t1_with_wm_outline/in_segmentation	in_segmentation		2018-09-08 13:18:12.994114	2018-09-08 13:18:12.994118	{}	2455
5764	iris/root/6_qc_overview/123_t1_with_wm_outline/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.995125	2018-09-08 13:18:12.995129	null	2455
5765	iris/root/6_qc_overview/127_t1_with_brain_sink/in_input	in_input		2018-09-08 13:18:12.99564	2018-09-08 13:18:12.995643	{}	2456
5766	iris/root/6_qc_overview/127_t1_with_brain_sink/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.99611	2018-09-08 13:18:12.996113	null	2456
5767	iris/root/65_spm_tissue_quantification/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.996613	2018-09-08 13:18:12.996616	null	2387
5768	iris/root/65_spm_tissue_quantification/66_InputImages/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.997143	2018-09-08 13:18:12.997147	null	2457
5769	iris/root/65_spm_tissue_quantification/66_InputImages/81_T1_images/in_source	in_source		2018-09-08 13:18:12.997597	2018-09-08 13:18:12.997601	{}	2461
5770	iris/root/65_spm_tissue_quantification/66_InputImages/81_T1_images/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.998122	2018-09-08 13:18:12.998125	null	2461
5771	iris/root/65_spm_tissue_quantification/67_InputImage/v_in_port	•••	Virtual input port	2018-09-08 13:18:12.998707	2018-09-08 13:18:12.998711	null	2458
5772	iris/root/65_spm_tissue_quantification/67_InputImage/75_t1_passthrough/in_left_hand	in_left_hand		2018-09-08 13:18:12.999192	2018-09-08 13:18:12.999196	{}	2462
5773	iris/root/65_spm_tissue_quantification/67_InputImage/75_t1_passthrough/in_operator	in_operator		2018-09-08 13:18:12.999658	2018-09-08 13:18:12.999662	{}	2462
5774	iris/root/65_spm_tissue_quantification/67_InputImage/75_t1_passthrough/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.000109	2018-09-08 13:18:13.000113	null	2462
5775	iris/root/65_spm_tissue_quantification/67_InputImage/77_const_t1_passthrough_operator_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.000723	2018-09-08 13:18:13.000727	null	2463
5776	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.001164	2018-09-08 13:18:13.001168	null	2459
5777	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation/in_input_image	in_input_image		2018-09-08 13:18:13.001616	2018-09-08 13:18:13.00162	{}	2464
5778	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.002154	2018-09-08 13:18:13.002158	null	2464
5779	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/71_const_edit_transform_file_set_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.002572	2018-09-08 13:18:13.002576	null	2465
5780	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/74_const_invert_gm_parameters_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.00312	2018-09-08 13:18:13.003124	null	2466
5781	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/in_moving_image	in_moving_image		2018-09-08 13:18:13.003645	2018-09-08 13:18:13.003649	{}	2467
5782	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/in_parameters	in_parameters		2018-09-08 13:18:13.004169	2018-09-08 13:18:13.004173	{}	2467
5783	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/in_threads	in_threads		2018-09-08 13:18:13.004647	2018-09-08 13:18:13.00465	{}	2467
5784	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/in_priority	in_priority		2018-09-08 13:18:13.005156	2018-09-08 13:18:13.00516	{}	2467
5785	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/in_fixed_image	in_fixed_image		2018-09-08 13:18:13.005634	2018-09-08 13:18:13.005638	{}	2467
5786	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/in_initial_transform	in_initial_transform		2018-09-08 13:18:13.006179	2018-09-08 13:18:13.006183	{}	2467
5787	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/in_fixed_mask	in_fixed_mask		2018-09-08 13:18:13.006821	2018-09-08 13:18:13.006825	{}	2467
5788	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/in_moving_mask	in_moving_mask		2018-09-08 13:18:13.007345	2018-09-08 13:18:13.007349	{}	2467
5789	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.007904	2018-09-08 13:18:13.007908	null	2467
5790	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/78_const_reg_t1_to_spmtemplate_parameters_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.008396	2018-09-08 13:18:13.0084	null	2468
5791	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/in_transform	in_transform		2018-09-08 13:18:13.008989	2018-09-08 13:18:13.008993	{}	2469
5792	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/in_jacobian_matrix_flag	in_jacobian_matrix_flag		2018-09-08 13:18:13.009503	2018-09-08 13:18:13.009507	{}	2469
5793	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/in_threads	in_threads		2018-09-08 13:18:13.010096	2018-09-08 13:18:13.0101	{}	2469
5794	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/in_image	in_image		2018-09-08 13:18:13.010744	2018-09-08 13:18:13.010748	{}	2469
5795	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/in_points	in_points		2018-09-08 13:18:13.011349	2018-09-08 13:18:13.011353	{}	2469
5796	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/in_priority	in_priority		2018-09-08 13:18:13.011968	2018-09-08 13:18:13.011972	{}	2469
5797	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/in_determinant_of_jacobian_flag	in_determinant_of_jacobian_flag		2018-09-08 13:18:13.012473	2018-09-08 13:18:13.012477	{}	2469
5798	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.013038	2018-09-08 13:18:13.013042	null	2469
5799	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/in_moving_image	in_moving_image		2018-09-08 13:18:13.013642	2018-09-08 13:18:13.013646	{}	2470
5800	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/in_parameters	in_parameters		2018-09-08 13:18:13.01414	2018-09-08 13:18:13.014144	{}	2470
5801	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/in_threads	in_threads		2018-09-08 13:18:13.014694	2018-09-08 13:18:13.014698	{}	2470
5802	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/in_priority	in_priority		2018-09-08 13:18:13.015228	2018-09-08 13:18:13.015231	{}	2470
5803	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/in_fixed_image	in_fixed_image		2018-09-08 13:18:13.015675	2018-09-08 13:18:13.015678	{}	2470
5804	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/in_initial_transform	in_initial_transform		2018-09-08 13:18:13.016209	2018-09-08 13:18:13.016213	{}	2470
5805	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/in_fixed_mask	in_fixed_mask		2018-09-08 13:18:13.016714	2018-09-08 13:18:13.016718	{}	2470
5806	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/in_moving_mask	in_moving_mask		2018-09-08 13:18:13.017299	2018-09-08 13:18:13.017302	{}	2470
5807	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.017889	2018-09-08 13:18:13.017893	null	2470
5808	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/in_gray_matter	in_gray_matter		2018-09-08 13:18:13.018383	2018-09-08 13:18:13.018386	{}	2471
5809	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/in_white_matter	in_white_matter		2018-09-08 13:18:13.018898	2018-09-08 13:18:13.018902	{}	2471
5810	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/in_cerebral_spinal_fluid	in_cerebral_spinal_fluid		2018-09-08 13:18:13.019382	2018-09-08 13:18:13.019385	{}	2471
5811	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.019987	2018-09-08 13:18:13.019991	null	2471
5812	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/85_const_reg_t1_to_spmtemplate_fixed_image_0/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.02049	2018-09-08 13:18:13.020494	null	2472
5813	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/86_edit_transform_file/in_transform	in_transform		2018-09-08 13:18:13.020979	2018-09-08 13:18:13.020983	{}	2473
5814	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/86_edit_transform_file/in_set	in_set		2018-09-08 13:18:13.021449	2018-09-08 13:18:13.021453	{}	2473
5815	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/86_edit_transform_file/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.021922	2018-09-08 13:18:13.021926	null	2473
5816	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/in_transform	in_transform		2018-09-08 13:18:13.022437	2018-09-08 13:18:13.02244	{}	2474
5817	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/in_jacobian_matrix_flag	in_jacobian_matrix_flag		2018-09-08 13:18:13.023007	2018-09-08 13:18:13.023011	{}	2474
5818	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/in_threads	in_threads		2018-09-08 13:18:13.023479	2018-09-08 13:18:13.023483	{}	2474
5819	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/in_image	in_image		2018-09-08 13:18:13.023892	2018-09-08 13:18:13.023896	{}	2474
5820	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/in_points	in_points		2018-09-08 13:18:13.024413	2018-09-08 13:18:13.024416	{}	2474
5821	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/in_priority	in_priority		2018-09-08 13:18:13.024898	2018-09-08 13:18:13.024902	{}	2474
5822	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/in_determinant_of_jacobian_flag	in_determinant_of_jacobian_flag		2018-09-08 13:18:13.025348	2018-09-08 13:18:13.025352	{}	2474
5823	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.02582	2018-09-08 13:18:13.025823	null	2474
5824	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/in_transform	in_transform		2018-09-08 13:18:13.026316	2018-09-08 13:18:13.026336	{}	2475
5825	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/in_jacobian_matrix_flag	in_jacobian_matrix_flag		2018-09-08 13:18:13.026967	2018-09-08 13:18:13.026971	{}	2475
5826	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/in_threads	in_threads		2018-09-08 13:18:13.027474	2018-09-08 13:18:13.027483	{}	2475
5827	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/in_image	in_image		2018-09-08 13:18:13.027978	2018-09-08 13:18:13.027982	{}	2475
5828	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/in_points	in_points		2018-09-08 13:18:13.028477	2018-09-08 13:18:13.02848	{}	2475
5829	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/in_priority	in_priority		2018-09-08 13:18:13.028964	2018-09-08 13:18:13.028968	{}	2475
5830	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/in_determinant_of_jacobian_flag	in_determinant_of_jacobian_flag		2018-09-08 13:18:13.029474	2018-09-08 13:18:13.029478	{}	2475
5831	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.030021	2018-09-08 13:18:13.030024	null	2475
5832	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/in_transform	in_transform		2018-09-08 13:18:13.030662	2018-09-08 13:18:13.030683	{}	2476
5833	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/in_jacobian_matrix_flag	in_jacobian_matrix_flag		2018-09-08 13:18:13.031263	2018-09-08 13:18:13.031267	{}	2476
5834	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/in_threads	in_threads		2018-09-08 13:18:13.03187	2018-09-08 13:18:13.031874	{}	2476
5835	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/in_image	in_image		2018-09-08 13:18:13.032337	2018-09-08 13:18:13.032341	{}	2476
5836	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/in_points	in_points		2018-09-08 13:18:13.032925	2018-09-08 13:18:13.032929	{}	2476
5837	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/in_priority	in_priority		2018-09-08 13:18:13.033409	2018-09-08 13:18:13.033413	{}	2476
5838	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/in_determinant_of_jacobian_flag	in_determinant_of_jacobian_flag		2018-09-08 13:18:13.033981	2018-09-08 13:18:13.033984	{}	2476
5839	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.034478	2018-09-08 13:18:13.034482	null	2476
5840	iris/root/65_spm_tissue_quantification/69_outputs/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.03504	2018-09-08 13:18:13.035044	null	2460
5841	iris/root/65_spm_tissue_quantification/69_outputs/72_csf_map/in_input	in_input		2018-09-08 13:18:13.035462	2018-09-08 13:18:13.035466	{}	2477
5842	iris/root/65_spm_tissue_quantification/69_outputs/72_csf_map/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.035922	2018-09-08 13:18:13.035926	null	2477
5843	iris/root/65_spm_tissue_quantification/69_outputs/73_wm_map/in_input	in_input		2018-09-08 13:18:13.036436	2018-09-08 13:18:13.03644	{}	2478
5844	iris/root/65_spm_tissue_quantification/69_outputs/73_wm_map/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.036994	2018-09-08 13:18:13.036998	null	2478
5845	iris/root/65_spm_tissue_quantification/69_outputs/80_gm_map/in_input	in_input		2018-09-08 13:18:13.037469	2018-09-08 13:18:13.037473	{}	2479
5846	iris/root/65_spm_tissue_quantification/69_outputs/80_gm_map/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.038016	2018-09-08 13:18:13.03802	null	2479
5847	iris/root/65_spm_tissue_quantification/69_outputs/84_csf_spm/in_input	in_input		2018-09-08 13:18:13.038537	2018-09-08 13:18:13.038541	{}	2480
5848	iris/root/65_spm_tissue_quantification/69_outputs/84_csf_spm/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.039015	2018-09-08 13:18:13.039019	null	2480
5849	iris/root/65_spm_tissue_quantification/69_outputs/88_gm_spm/in_input	in_input		2018-09-08 13:18:13.03949	2018-09-08 13:18:13.039493	{}	2481
5850	iris/root/65_spm_tissue_quantification/69_outputs/88_gm_spm/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.039968	2018-09-08 13:18:13.039972	null	2481
5851	iris/root/65_spm_tissue_quantification/69_outputs/90_wm_spm/in_input	in_input		2018-09-08 13:18:13.040414	2018-09-08 13:18:13.040435	{}	2482
5852	iris/root/65_spm_tissue_quantification/69_outputs/90_wm_spm/v_in_port	•••	Virtual input port	2018-09-08 13:18:13.040934	2018-09-08 13:18:13.040938	null	2482
\.


--
-- Name: in_ports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.in_ports_id_seq', 5860, true);


--
-- Data for Name: jobs; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.jobs (id, path, title, description, created, modified, custom_data, run_id, node_id, node_path, status) FROM stdin;
13742	iris/root/65_spm_tissue_quantification/66_InputImages/81_T1_images/job___002			2018-09-08 13:18:42.004717	2018-09-08 13:18:42.004717	{}	75	2461	iris/root/65_spm_tissue_quantification/66_InputImages/81_T1_images	2
13743	iris/root/6_qc_overview/54_t1_qc_sink/job___002			2018-09-08 13:18:42.004717	2018-09-08 13:18:42.004717	{}	75	2452	iris/root/6_qc_overview/54_t1_qc_sink	2
13737	iris/root/65_spm_tissue_quantification/69_outputs/80_gm_map/job___001			2018-09-08 13:18:35.232791	2018-09-08 13:19:01.053432	{}	75	2479	iris/root/65_spm_tissue_quantification/69_outputs/80_gm_map	2
13740	iris/root/6_qc_overview/31_t1_with_hammers_sink/job___001			2018-09-08 13:18:40.640311	2018-09-08 13:18:55.694804	{}	75	2448	iris/root/6_qc_overview/31_t1_with_hammers_sink	2
13735	iris/root/6_qc_overview/37_t1_with_wm_outline_sink/job___002			2018-09-08 13:18:31.123136	2018-09-08 13:19:09.40607	{}	75	2449	iris/root/6_qc_overview/37_t1_with_wm_outline_sink	2
13744	iris/root/65_spm_tissue_quantification/69_outputs/88_gm_spm/job___001			2018-09-08 13:18:59.72705	2018-09-09 17:03:52.839459	{}	75	2481	iris/root/65_spm_tissue_quantification/69_outputs/88_gm_spm	0
13739	iris/root/6_qc_overview/62_t1_with_gm_outline_sink/job___001			2018-09-08 13:18:39.28045	2018-09-08 13:19:07.842096	{}	75	2453	iris/root/6_qc_overview/62_t1_with_gm_outline_sink	2
13736	iris/root/4_output/16_wm_map_output/job___001			2018-09-08 13:18:32.479942	2018-09-08 13:19:05.143426	{}	75	2413	iris/root/4_output/16_wm_map_output	2
13738	iris/root/6_qc_overview/127_t1_with_brain_sink/job___001			2018-09-08 13:18:35.232791	2018-09-08 13:18:58.396256	{}	75	2456	iris/root/6_qc_overview/127_t1_with_brain_sink	2
12943	failing_network_2018-08-24T15-34-44/root/sum/failing_network___sum___sample_1_1			2018-08-24 13:36:02.406054	2018-08-24 13:36:04.103065	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	42	2121	failing_network_2018-08-24T15-34-44/root/sum	4
13705	iris/root/0_statistics/128_gm_volume/job___002			2018-09-08 13:18:20.161172	2018-09-08 13:19:05.143426	{}	75	2403	iris/root/0_statistics/128_gm_volume	2
13741	iris/root/4_output/23_multi_atlas_segm/job___002			2018-09-08 13:18:40.640311	2018-09-08 13:18:58.396256	{}	75	2414	iris/root/4_output/23_multi_atlas_segm	2
13745	iris/root/65_spm_tissue_quantification/69_outputs/72_csf_map/job___002			2018-09-08 13:18:59.72705	2018-09-08 13:19:02.425715	{}	75	2477	iris/root/65_spm_tissue_quantification/69_outputs/72_csf_map	2
12944	failing_network_2018-08-24T15-34-44/root/sum/failing_network___sum___sample_1_2			2018-08-24 13:36:02.491847	2018-08-24 13:36:04.189817	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	42	2121	failing_network_2018-08-24T15-34-44/root/sum	4
12950	failing_network_2018-08-24T15-34-44/root/sink_4/failing_network___sink_4___sample_1_0___4			2018-08-24 13:36:03.01507	2018-08-24 13:36:29.452795	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2115	failing_network_2018-08-24T15-34-44/root/sink_4	2
12945	failing_network_2018-08-24T15-34-44/root/sum/failing_network___sum___sample_1_3			2018-08-24 13:36:02.575546	2018-08-24 13:36:04.277659	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	42	2121	failing_network_2018-08-24T15-34-44/root/sum	4
12942	failing_network_2018-08-24T15-34-44/root/sum/failing_network___sum___sample_1_0			2018-08-24 13:36:02.319808	2018-08-24 13:36:07.47791	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2121	failing_network_2018-08-24T15-34-44/root/sum	2
12946	failing_network_2018-08-24T15-34-44/root/sink_4/failing_network___sink_4___sample_1_0___0			2018-08-24 13:36:02.662636	2018-08-24 13:36:11.757308	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2115	failing_network_2018-08-24T15-34-44/root/sink_4	2
12947	failing_network_2018-08-24T15-34-44/root/sink_4/failing_network___sink_4___sample_1_0___1			2018-08-24 13:36:02.750673	2018-08-24 13:36:16.069776	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2115	failing_network_2018-08-24T15-34-44/root/sink_4	2
12949	failing_network_2018-08-24T15-34-44/root/sink_4/failing_network___sink_4___sample_1_0___3			2018-08-24 13:36:02.928056	2018-08-24 13:36:24.732957	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2115	failing_network_2018-08-24T15-34-44/root/sink_4	2
12951	failing_network_2018-08-24T15-34-44/root/sink_4/failing_network___sink_4___sample_1_0___5			2018-08-24 13:36:03.097974	2018-08-24 13:36:35.557683	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2115	failing_network_2018-08-24T15-34-44/root/sink_4	2
12948	failing_network_2018-08-24T15-34-44/root/sink_4/failing_network___sink_4___sample_1_0___2			2018-08-24 13:36:02.837819	2018-08-24 13:36:20.378639	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2115	failing_network_2018-08-24T15-34-44/root/sink_4	2
13746	iris/root/5_BET_and_REG/56_brain_mask_registation/job___002			2018-09-08 13:19:02.425715	2018-09-08 13:19:02.425715	{}	75	2438	iris/root/5_BET_and_REG/56_brain_mask_registation	2
13752	iris/root/65_spm_tissue_quantification/67_InputImage/77_const_t1_passthrough_operator_0/job___001			2018-09-08 13:19:03.799795	2018-09-08 13:19:03.799795	{}	75	2463	iris/root/65_spm_tissue_quantification/67_InputImage/77_const_t1_passthrough_operator_0	2
13766	iris/root/5_BET_and_REG/43_convert_brain/job___002			2018-09-08 13:19:07.842096	2018-09-08 13:19:07.842096	{}	75	2433	iris/root/5_BET_and_REG/43_convert_brain	2
13769	iris/root/5_BET_and_REG/53_const_label_registration_threads_0/job___001			2018-09-08 13:19:09.40607	2018-09-08 13:19:09.40607	{}	75	2437	iris/root/5_BET_and_REG/53_const_label_registration_threads_0	2
13753	iris/root/0_statistics/25_wm_volume/job___002			2018-09-08 13:19:05.143426	2018-09-08 13:19:05.143426	{}	75	2393	iris/root/0_statistics/25_wm_volume	2
13754	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/71_const_edit_transform_file_set_0/job___001			2018-09-08 13:19:05.143426	2018-09-08 13:19:05.143426	{}	75	2465	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/71_const_edit_transform_file_set_0	2
13755	iris/root/5_BET_and_REG/43_convert_brain/job___001			2018-09-08 13:19:05.143426	2018-09-08 13:19:05.143426	{}	75	2433	iris/root/5_BET_and_REG/43_convert_brain	2
13756	iris/root/6_qc_overview/122_t1_with_brain/job___002			2018-09-08 13:19:06.473781	2018-09-08 13:19:06.473781	{}	75	2454	iris/root/6_qc_overview/122_t1_with_brain	2
13757	iris/root/0_statistics/45_const_get_wm_volume_selection_0/job___001			2018-09-08 13:19:06.473781	2018-09-08 13:19:06.473781	{}	75	2397	iris/root/0_statistics/45_const_get_wm_volume_selection_0	2
13758	iris/root/1_dicom_to_nifti/20_t1_dicom_to_nifti/job___001			2018-09-08 13:19:06.473781	2018-09-08 13:19:06.473781	{}	75	2405	iris/root/1_dicom_to_nifti/20_t1_dicom_to_nifti	2
13759	iris/root/0_statistics/45_const_get_wm_volume_selection_0/job___002			2018-09-08 13:19:06.473781	2018-09-08 13:19:06.473781	{}	75	2397	iris/root/0_statistics/45_const_get_wm_volume_selection_0	2
13747	iris/root/5_BET_and_REG/63_const_brain_mask_registation_threads_0/job___002			2018-09-08 13:19:03.799795	2018-09-08 13:19:03.799795	{}	75	2441	iris/root/5_BET_and_REG/63_const_brain_mask_registation_threads_0	2
13748	iris/root/5_BET_and_REG/22_const_brain_mask_registation_parameters_0/job___001			2018-09-08 13:19:03.799795	2018-09-08 13:19:03.799795	{}	75	2424	iris/root/5_BET_and_REG/22_const_brain_mask_registation_parameters_0	2
13749	iris/root/4_output/40_gm_map_output/job___001			2018-09-08 13:19:03.799795	2018-09-08 13:19:03.799795	{}	75	2416	iris/root/4_output/40_gm_map_output	2
13750	iris/root/5_BET_and_REG/51_const_combine_brains_number_of_classes_0/job___002			2018-09-08 13:19:03.799795	2018-09-08 13:19:03.799795	{}	75	2436	iris/root/5_BET_and_REG/51_const_combine_brains_number_of_classes_0	2
13751	iris/root/0_statistics/52_const_get_brain_volume_selection_0/job___001			2018-09-08 13:19:03.799795	2018-09-08 13:19:03.799795	{}	75	2399	iris/root/0_statistics/52_const_get_brain_volume_selection_0	2
13760	iris/root/2_hammers_atlas/64_hammers_brains_r5/job___002			2018-09-08 13:19:06.473781	2018-09-08 13:19:06.473781	{}	75	2411	iris/root/2_hammers_atlas/64_hammers_brains_r5	2
13767	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/74_const_invert_gm_parameters_0/job___001			2018-09-08 13:19:09.40607	2018-09-08 13:19:09.40607	{}	75	2466	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/74_const_invert_gm_parameters_0	2
13768	iris/root/5_BET_and_REG/38_threshold_brain/job___002			2018-09-08 13:19:09.40607	2018-09-08 13:19:09.40607	{}	75	2432	iris/root/5_BET_and_REG/38_threshold_brain	2
13770	iris/root/1_dicom_to_nifti/61_const_t1_dicom_to_nifti_file_order_0/job___002			2018-09-08 13:19:09.40607	2018-09-08 13:19:09.40607	{}	75	2407	iris/root/1_dicom_to_nifti/61_const_t1_dicom_to_nifti_file_order_0	2
13763	iris/root/2_hammers_atlas/36_hammers_labels/job___002			2018-09-08 13:19:07.842096	2018-09-08 13:19:07.842096	{}	75	2409	iris/root/2_hammers_atlas/36_hammers_labels	2
13761	iris/root/0_statistics/55_brain_volume/job___001			2018-09-08 13:19:07.842096	2018-09-08 13:19:07.842096	{}	75	2400	iris/root/0_statistics/55_brain_volume	2
13762	iris/root/5_BET_and_REG/131_const_dilate_brainmask_operation_0/job___002			2018-09-08 13:19:07.842096	2018-09-08 13:19:07.842096	{}	75	2446	iris/root/5_BET_and_REG/131_const_dilate_brainmask_operation_0	2
13764	iris/root/5_BET_and_REG/17_hammers_brain_transform/job___002			2018-09-08 13:19:07.842096	2018-09-08 13:19:07.842096	{}	75	2423	iris/root/5_BET_and_REG/17_hammers_brain_transform	2
13765	iris/root/5_BET_and_REG/30_combine_labels/job___002			2018-09-08 13:19:07.842096	2018-09-08 13:19:07.842096	{}	75	2428	iris/root/5_BET_and_REG/30_combine_labels	2
12952	failing_network_2018-08-24T15-34-44/root/sink_4/failing_network___sink_4___sample_1_0___6			2018-08-24 13:36:03.184233	2018-08-24 13:36:39.976309	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2115	failing_network_2018-08-24T15-34-44/root/sink_4	2
12953	failing_network_2018-08-24T15-34-44/root/sink_4/failing_network___sink_4___sample_1_0___7			2018-08-24 13:36:03.267601	2018-08-24 13:36:44.319356	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2115	failing_network_2018-08-24T15-34-44/root/sink_4	2
13780	iris/root/65_spm_tissue_quantification/69_outputs/73_wm_map/job___001			2018-09-08 13:19:10.932787	2018-09-09 17:02:28.687382	{}	75	2478	iris/root/65_spm_tissue_quantification/69_outputs/73_wm_map	0
13771	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/85_const_reg_t1_to_spmtemplate_fixed_image_0/job___002			2018-09-08 13:19:09.40607	2018-09-08 13:19:09.40607	{}	75	2472	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/85_const_reg_t1_to_spmtemplate_fixed_image_0	2
12578	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sum/failing_macro_top_level_failing_macro_failing_network___sum___sample_1_0			2018-08-22 10:18:38.272976	2018-08-22 10:18:38.272976	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	34	2011	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sum	0
13772	iris/root/0_statistics/48_const_get_brain_volume_flatten_0/job___002			2018-09-08 13:19:09.40607	2018-09-08 13:19:09.40607	{}	75	2398	iris/root/0_statistics/48_const_get_brain_volume_flatten_0	2
13773	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/job___001			2018-09-08 13:19:09.40607	2018-09-08 13:19:09.40607	{}	75	2467	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate	2
13774	iris/root/0_statistics/126_get_wm_volume/job___002			2018-09-08 13:19:09.40607	2018-09-08 13:19:09.40607	{}	75	2402	iris/root/0_statistics/126_get_wm_volume	2
13782	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/78_const_reg_t1_to_spmtemplate_parameters_0/job___002			2018-09-08 13:19:10.932787	2018-09-08 13:19:10.932787	{}	75	2468	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/78_const_reg_t1_to_spmtemplate_parameters_0	2
12954	failing_network_2018-08-24T15-34-44/root/sink_4/failing_network___sink_4___sample_1_0___8			2018-08-24 13:36:03.392314	2018-08-24 13:36:48.643871	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2115	failing_network_2018-08-24T15-34-44/root/sink_4	2
13775	iris/root/5_BET_and_REG/12_t1w_registration/job___001			2018-09-08 13:19:10.932787	2018-09-08 13:19:10.932787	{}	75	2420	iris/root/5_BET_and_REG/12_t1w_registration	2
13776	iris/root/5_BET_and_REG/27_const_hammers_brain_transform_threads_0/job___002			2018-09-08 13:19:10.932787	2018-09-08 13:19:10.932787	{}	75	2426	iris/root/5_BET_and_REG/27_const_hammers_brain_transform_threads_0	2
12580	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sum/failing_macro_top_level_failing_macro_failing_network___sum___sample_1_2			2018-08-22 10:18:38.405434	2018-08-22 10:18:38.763146	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	34	2011	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sum	4
12581	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sum/failing_macro_top_level_failing_macro_failing_network___sum___sample_1_3			2018-08-22 10:18:38.480772	2018-08-22 10:18:38.906417	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	34	2011	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sum	4
12579	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sum/failing_macro_top_level_failing_macro_failing_network___sum___sample_1_1			2018-08-22 10:18:38.339571	2018-08-22 10:18:38.614723	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	34	2011	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sum	4
13777	iris/root/6_qc_overview/21_t1_with_hammers/job___001			2018-09-08 13:19:10.932787	2018-09-08 13:19:10.932787	{}	75	2447	iris/root/6_qc_overview/21_t1_with_hammers	2
13778	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/job___002			2018-09-08 13:19:10.932787	2018-09-08 13:19:10.932787	{}	75	2471	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment	2
13779	iris/root/5_BET_and_REG/130_bet/job___001			2018-09-08 13:19:10.932787	2018-09-08 13:19:10.932787	{}	75	2445	iris/root/5_BET_and_REG/130_bet	2
13781	iris/root/65_spm_tissue_quantification/67_InputImage/75_t1_passthrough/job___001			2018-09-08 13:19:10.932787	2018-09-08 13:19:10.932787	{}	75	2462	iris/root/65_spm_tissue_quantification/67_InputImage/75_t1_passthrough	2
13783	iris/root/5_BET_and_REG/10_const_bet_fraction_threshold_0/job___002			2018-09-08 13:19:10.932787	2018-09-08 13:19:10.932787	{}	75	2419	iris/root/5_BET_and_REG/10_const_bet_fraction_threshold_0	2
13784	iris/root/4_output/23_multi_atlas_segm/job___001			2018-09-08 13:19:10.932787	2018-09-08 13:19:10.932787	{}	75	2414	iris/root/4_output/23_multi_atlas_segm	2
13785	iris/root/65_spm_tissue_quantification/66_InputImages/81_T1_images/job___001			2018-09-08 13:19:12.533929	2018-09-08 13:19:12.533929	{}	75	2461	iris/root/65_spm_tissue_quantification/66_InputImages/81_T1_images	2
13786	iris/root/0_statistics/9_const_get_gm_volume_flatten_0/job___002			2018-09-08 13:19:12.533929	2018-09-08 13:19:12.533929	{}	75	2388	iris/root/0_statistics/9_const_get_gm_volume_flatten_0	2
13787	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/job___002			2018-09-08 13:19:12.533929	2018-09-08 13:19:12.533929	{}	75	2476	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1	2
13788	iris/root/5_BET_and_REG/35_const_dilate_brainmask_radius_0/job___002			2018-09-08 13:19:12.533929	2018-09-08 13:19:12.533929	{}	75	2431	iris/root/5_BET_and_REG/35_const_dilate_brainmask_radius_0	2
12956	failing_network_2018-08-24T15-34-44/root/sink_4/failing_network___sink_4___sample_1_2___0			2018-08-24 13:36:03.567106	2018-08-24 13:36:04.480297	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	42	2115	failing_network_2018-08-24T15-34-44/root/sink_4	4
12957	failing_network_2018-08-24T15-34-44/root/sink_4/failing_network___sink_4___sample_1_3___0			2018-08-24 13:36:03.653314	2018-08-24 13:36:04.568116	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	42	2115	failing_network_2018-08-24T15-34-44/root/sink_4	4
12959	failing_network_2018-08-24T15-34-44/root/sink_5/failing_network___sink_5___sample_1_1___0			2018-08-24 13:36:03.82497	2018-08-24 13:36:04.657467	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	42	2116	failing_network_2018-08-24T15-34-44/root/sink_5	4
12958	failing_network_2018-08-24T15-34-44/root/sink_5/failing_network___sink_5___sample_1_0___0			2018-08-24 13:36:03.74108	2018-08-24 13:36:53.015941	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2116	failing_network_2018-08-24T15-34-44/root/sink_5	2
12960	failing_network_2018-08-24T15-34-44/root/sink_5/failing_network___sink_5___sample_1_2___0			2018-08-24 13:36:03.905815	2018-08-24 13:36:04.75213	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	42	2116	failing_network_2018-08-24T15-34-44/root/sink_5	4
12955	failing_network_2018-08-24T15-34-44/root/sink_4/failing_network___sink_4___sample_1_1___0			2018-08-24 13:36:03.480259	2018-08-24 13:36:04.393132	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	42	2115	failing_network_2018-08-24T15-34-44/root/sink_4	4
13793	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/job___001			2018-09-08 13:19:13.89146	2018-09-08 13:19:13.89146	{}	75	2469	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf	2
13789	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/job___001			2018-09-08 13:19:12.533929	2018-09-08 13:19:12.533929	{}	75	2474	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm	2
13790	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/85_const_reg_t1_to_spmtemplate_fixed_image_0/job___001			2018-09-08 13:19:12.533929	2018-09-08 13:19:12.533929	{}	75	2472	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/85_const_reg_t1_to_spmtemplate_fixed_image_0	2
13791	iris/root/0_statistics/41_const_mask_hammers_labels_operator_0/job___001			2018-09-08 13:19:12.533929	2018-09-08 13:19:12.533929	{}	75	2396	iris/root/0_statistics/41_const_mask_hammers_labels_operator_0	2
13794	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/job___001			2018-09-08 13:19:13.89146	2018-09-08 13:19:13.89146	{}	75	2470	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm	2
13792	iris/root/4_output/29_t1_nifti_images/job___002			2018-09-08 13:19:12.533929	2018-09-08 13:19:12.533929	{}	75	2415	iris/root/4_output/29_t1_nifti_images	2
13795	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/86_edit_transform_file/job___001			2018-09-08 13:19:13.89146	2018-09-08 13:19:13.89146	{}	75	2473	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/86_edit_transform_file	2
13796	iris/root/4_output/40_gm_map_output/job___002			2018-09-08 13:19:13.89146	2018-09-08 13:19:13.89146	{}	75	2416	iris/root/4_output/40_gm_map_output	2
13797	iris/root/6_qc_overview/42_t1_with_gm_outline/job___001			2018-09-08 13:19:13.89146	2018-09-08 13:19:13.89146	{}	75	2450	iris/root/6_qc_overview/42_t1_with_gm_outline	2
12961	failing_network_2018-08-24T15-34-44/root/sink_5/failing_network___sink_5___sample_1_3___0			2018-08-24 13:36:03.989841	2018-08-24 13:36:04.848694	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	42	2116	failing_network_2018-08-24T15-34-44/root/sink_5	4
13200	example_2018-09-03T12-31-11/root/source1/example___source1___s3			2018-09-03 10:31:12.694506	2018-09-03 10:31:12.801694	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	57	2201	example_2018-09-03T12-31-11/root/source1	2
13201	example_2018-09-03T12-31-11/root/source1/example___source1___s4			2018-09-03 10:31:12.909611	2018-09-03 10:31:13.015412	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	57	2201	example_2018-09-03T12-31-11/root/source1	2
13202	example_2018-09-03T12-31-11/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 10:31:13.137249	2018-09-03 10:31:13.247877	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	57	2204	example_2018-09-03T12-31-11/root/const_addint_right_hand_0	2
13203	example_2018-09-03T12-31-11/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 10:31:13.353863	2018-09-03 10:31:13.475957	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	57	2204	example_2018-09-03T12-31-11/root/const_addint_right_hand_0	2
13798	iris/root/0_statistics/19_mask_hammers_labels/job___002			2018-09-08 13:19:13.89146	2018-09-08 13:19:13.89146	{}	75	2392	iris/root/0_statistics/19_mask_hammers_labels	2
13799	iris/root/5_BET_and_REG/17_hammers_brain_transform/job___001			2018-09-08 13:19:13.89146	2018-09-08 13:19:13.89146	{}	75	2423	iris/root/5_BET_and_REG/17_hammers_brain_transform	2
12962	example_2018-09-03T10-32-43/root/source1/example___source1___s1			2018-09-03 08:32:43.989671	2018-09-03 08:32:44.100611	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	43	2125	example_2018-09-03T10-32-43/root/source1	2
13803	iris/root/6_qc_overview/123_t1_with_wm_outline/job___001			2018-09-08 13:19:15.199075	2018-09-08 13:19:15.199075	{}	75	2455	iris/root/6_qc_overview/123_t1_with_wm_outline	2
13801	iris/root/0_statistics/14_get_brain_volume/job___001			2018-09-08 13:19:15.199075	2018-09-08 13:19:15.199075	{}	75	2390	iris/root/0_statistics/14_get_brain_volume	2
13802	iris/root/2_hammers_atlas/36_hammers_labels/job___001			2018-09-08 13:19:15.199075	2018-09-08 13:19:15.199075	{}	75	2409	iris/root/2_hammers_atlas/36_hammers_labels	2
13804	iris/root/4_output/29_t1_nifti_images/job___001			2018-09-08 13:19:15.199075	2018-09-08 13:19:15.199075	{}	75	2415	iris/root/4_output/29_t1_nifti_images	2
13805	iris/root/3_subjects/34_t1w_dicom/job___001			2018-09-08 13:19:15.199075	2018-09-08 13:19:15.199075	{}	75	2412	iris/root/3_subjects/34_t1w_dicom	2
13800	iris/root/65_spm_tissue_quantification/69_outputs/88_gm_spm/job___002			2018-09-08 13:19:13.89146	2018-09-09 17:04:06.972251	{}	75	2481	iris/root/65_spm_tissue_quantification/69_outputs/88_gm_spm	0
12965	example_2018-09-03T10-32-43/root/source1/example___source1___s4			2018-09-03 08:32:44.630557	2018-09-03 08:32:44.747121	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	43	2125	example_2018-09-03T10-32-43/root/source1	2
12963	example_2018-09-03T10-32-43/root/source1/example___source1___s2			2018-09-03 08:32:44.213266	2018-09-03 08:32:44.319645	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	43	2125	example_2018-09-03T10-32-43/root/source1	2
12969	example_2018-09-03T10-32-43/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 08:32:45.521706	2018-09-03 08:32:45.624708	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	43	2128	example_2018-09-03T10-32-43/root/const_addint_right_hand_0	2
12964	example_2018-09-03T10-32-43/root/source1/example___source1___s3			2018-09-03 08:32:44.427843	2018-09-03 08:32:44.528806	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	43	2125	example_2018-09-03T10-32-43/root/source1	2
12967	example_2018-09-03T10-32-43/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 08:32:45.085875	2018-09-03 08:32:45.197829	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	43	2128	example_2018-09-03T10-32-43/root/const_addint_right_hand_0	2
12966	example_2018-09-03T10-32-43/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 08:32:44.849141	2018-09-03 08:32:44.976835	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	43	2128	example_2018-09-03T10-32-43/root/const_addint_right_hand_0	2
12968	example_2018-09-03T10-32-43/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 08:32:45.306821	2018-09-03 08:32:45.413646	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	43	2128	example_2018-09-03T10-32-43/root/const_addint_right_hand_0	2
12970	example_2018-09-03T10-32-43/root/addint/example___addint___s1			2018-09-03 08:32:45.73505	2018-09-03 08:32:50.039957	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	43	2127	example_2018-09-03T10-32-43/root/addint	2
12971	example_2018-09-03T10-32-43/root/addint/example___addint___s2			2018-09-03 08:32:45.845201	2018-09-03 08:32:54.36056	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	43	2127	example_2018-09-03T10-32-43/root/addint	2
12973	example_2018-09-03T10-32-43/root/addint/example___addint___s4			2018-09-03 08:32:46.06271	2018-09-03 08:33:02.159936	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	43	2127	example_2018-09-03T10-32-43/root/addint	2
12974	example_2018-09-03T10-32-43/root/sink1/example___sink1___s1___0			2018-09-03 08:32:46.173131	2018-09-03 08:33:08.967477	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	43	2126	example_2018-09-03T10-32-43/root/sink1	2
12972	example_2018-09-03T10-32-43/root/addint/example___addint___s3			2018-09-03 08:32:45.955122	2018-09-03 08:32:58.393121	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	43	2127	example_2018-09-03T10-32-43/root/addint	2
13815	iris/root/0_statistics/128_gm_volume/job___001			2018-09-08 13:19:16.544676	2018-09-08 13:19:16.544676	{}	75	2403	iris/root/0_statistics/128_gm_volume	2
13806	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/job___001			2018-09-08 13:19:15.199075	2018-09-08 13:19:15.199075	{}	75	2476	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1	2
13807	iris/root/0_statistics/126_get_wm_volume/job___001			2018-09-08 13:19:15.199075	2018-09-08 13:19:15.199075	{}	75	2402	iris/root/0_statistics/126_get_wm_volume	2
13808	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/job___002			2018-09-08 13:19:15.199075	2018-09-08 13:19:15.199075	{}	75	2475	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm	2
12582	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_4/failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_0			2018-08-22 10:20:57.389733	2018-08-22 10:20:57.466317	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	34	2019	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_4	2
12975	example_2018-09-03T10-32-43/root/sink1/example___sink1___s2___0			2018-09-03 08:32:46.277119	2018-09-03 08:33:14.57189	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	43	2126	example_2018-09-03T10-32-43/root/sink1	2
13813	iris/root/6_qc_overview/31_t1_with_hammers_sink/job___002			2018-09-08 13:19:16.544676	2018-09-08 13:19:16.544676	{}	75	2448	iris/root/6_qc_overview/31_t1_with_hammers_sink	2
13809	iris/root/6_qc_overview/127_t1_with_brain_sink/job___002			2018-09-08 13:19:16.544676	2018-09-08 13:19:16.544676	{}	75	2456	iris/root/6_qc_overview/127_t1_with_brain_sink	2
13810	iris/root/5_BET_and_REG/125_dilate_brainmask/job___001			2018-09-08 13:19:16.544676	2018-09-08 13:19:16.544676	{}	75	2444	iris/root/5_BET_and_REG/125_dilate_brainmask	2
13811	iris/root/5_BET_and_REG/130_bet/job___002			2018-09-08 13:19:16.544676	2018-09-08 13:19:16.544676	{}	75	2445	iris/root/5_BET_and_REG/130_bet	2
13812	iris/root/0_statistics/129_const_get_gm_volume_selection_0/job___002			2018-09-08 13:19:16.544676	2018-09-08 13:19:16.544676	{}	75	2404	iris/root/0_statistics/129_const_get_gm_volume_selection_0	2
13814	iris/root/5_BET_and_REG/13_const_threshold_labels_upper_threshold_0/job___001			2018-09-08 13:19:16.544676	2018-09-08 13:19:16.544676	{}	75	2421	iris/root/5_BET_and_REG/13_const_threshold_labels_upper_threshold_0	2
13816	iris/root/5_BET_and_REG/121_combine_brains/job___002			2018-09-08 13:19:16.544676	2018-09-08 13:19:16.544676	{}	75	2442	iris/root/5_BET_and_REG/121_combine_brains	2
12585	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_4/failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_3			2018-08-22 10:20:57.796399	2018-08-22 10:20:57.855834	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	34	2019	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_4	2
12584	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_4/failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_2			2018-08-22 10:20:57.64605	2018-08-22 10:20:57.708949	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	34	2019	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_4	2
13823	iris/root/5_BET_and_REG/33_label_registration/job___001			2018-09-08 13:19:17.865501	2018-09-08 13:19:17.865501	{}	75	2430	iris/root/5_BET_and_REG/33_label_registration	2
12583	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_4/failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_1			2018-08-22 10:20:57.527804	2018-08-22 10:20:57.587383	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	34	2019	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_4	2
13822	iris/root/0_statistics/48_const_get_brain_volume_flatten_0/job___001			2018-09-08 13:19:17.865501	2018-09-08 13:19:17.865501	{}	75	2398	iris/root/0_statistics/48_const_get_brain_volume_flatten_0	2
13819	iris/root/1_dicom_to_nifti/20_t1_dicom_to_nifti/job___002			2018-09-08 13:19:17.865501	2018-09-08 13:19:17.865501	{}	75	2405	iris/root/1_dicom_to_nifti/20_t1_dicom_to_nifti	2
13817	iris/root/6_qc_overview/123_t1_with_wm_outline/job___002			2018-09-08 13:19:17.865501	2018-09-08 13:19:17.865501	{}	75	2455	iris/root/6_qc_overview/123_t1_with_wm_outline	2
12586	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_5/failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_0			2018-08-22 10:20:57.943499	2018-08-22 10:20:58.000775	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	34	2012	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_5	2
13818	iris/root/0_statistics/55_brain_volume/job___002			2018-09-08 13:19:17.865501	2018-09-08 13:19:17.865501	{}	75	2400	iris/root/0_statistics/55_brain_volume	2
13820	iris/root/5_BET_and_REG/28_n4_bias_correction/job___002			2018-09-08 13:19:17.865501	2018-09-08 13:19:17.865501	{}	75	2427	iris/root/5_BET_and_REG/28_n4_bias_correction	2
12588	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_5/failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_2			2018-08-22 10:20:58.189254	2018-08-22 10:20:58.245943	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	34	2012	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_5	2
12587	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_5/failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_1			2018-08-22 10:20:58.064652	2018-08-22 10:20:58.126959	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	34	2012	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_5	2
12589	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_5/failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_3			2018-08-22 10:20:58.30524	2018-08-22 10:20:58.364975	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	34	2012	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_5	2
12590	failing_macro_top_level_2018-08-22T12-15-04/root/add/failing_macro_top_level___add___sample_1_0			2018-08-22 10:20:58.617077	2018-08-22 10:21:40.478534	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	34	2005	failing_macro_top_level_2018-08-22T12-15-04/root/add	2
13821	iris/root/0_statistics/39_const_get_gm_volume_volume_0/job___002			2018-09-08 13:19:17.865501	2018-09-08 13:19:17.865501	{}	75	2395	iris/root/0_statistics/39_const_get_gm_volume_volume_0	2
13824	iris/root/5_BET_and_REG/125_dilate_brainmask/job___002			2018-09-08 13:19:17.865501	2018-09-08 13:19:17.865501	{}	75	2444	iris/root/5_BET_and_REG/125_dilate_brainmask	2
13825	iris/root/5_BET_and_REG/24_const_combine_labels_method_0/job___001			2018-09-08 13:19:19.216895	2018-09-08 13:19:19.216895	{}	75	2425	iris/root/5_BET_and_REG/24_const_combine_labels_method_0	4
13826	iris/root/5_BET_and_REG/7_threshold_labels/job___001			2018-09-08 13:19:19.216895	2018-09-08 13:19:19.216895	{}	75	2418	iris/root/5_BET_and_REG/7_threshold_labels	2
13827	iris/root/5_BET_and_REG/51_const_combine_brains_number_of_classes_0/job___001			2018-09-08 13:19:19.216895	2018-09-08 13:19:19.216895	{}	75	2436	iris/root/5_BET_and_REG/51_const_combine_brains_number_of_classes_0	2
13828	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/job___002			2018-09-08 13:19:19.216895	2018-09-08 13:19:19.216895	{}	75	2467	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate	2
12723	macro_node_2_2018-08-22T12-40-46/root/sum/macro_node_2___sum___id_1__id_2			2018-08-22 10:42:33.795065	2018-08-22 10:43:11.545986	{"sample_index": [1, 2], "errors": [], "sample_id": ["id_1", "id_2"]}	36	2047	macro_node_2_2018-08-22T12-40-46/root/sum	2
12728	macro_node_2_2018-08-22T12-40-46/root/sink/macro_node_2___sink___id_0__id_3___0			2018-08-22 10:42:34.124781	2018-08-22 10:44:32.690269	{"sample_index": [0, 3], "errors": [], "sample_id": ["id_0", "id_3"]}	36	2053	macro_node_2_2018-08-22T12-40-46/root/sink	2
12724	macro_node_2_2018-08-22T12-40-46/root/sum/macro_node_2___sum___id_1__id_3			2018-08-22 10:42:33.859166	2018-08-22 10:43:16.165158	{"sample_index": [1, 3], "errors": [], "sample_id": ["id_1", "id_3"]}	36	2047	macro_node_2_2018-08-22T12-40-46/root/sum	2
12729	macro_node_2_2018-08-22T12-40-46/root/sink/macro_node_2___sink___id_1__id_0___0			2018-08-22 10:42:34.185469	2018-08-22 10:44:11.454845	{"sample_index": [1, 0], "errors": [], "sample_id": ["id_1", "id_0"]}	36	2053	macro_node_2_2018-08-22T12-40-46/root/sink	2
12727	macro_node_2_2018-08-22T12-40-46/root/sink/macro_node_2___sink___id_0__id_2___0			2018-08-22 10:42:34.063069	2018-08-22 10:44:13.621994	{"sample_index": [0, 2], "errors": [], "sample_id": ["id_0", "id_2"]}	36	2053	macro_node_2_2018-08-22T12-40-46/root/sink	2
12725	macro_node_2_2018-08-22T12-40-46/root/sink/macro_node_2___sink___id_0__id_0___0			2018-08-22 10:42:33.933563	2018-08-22 10:44:19.28179	{"sample_index": [0, 0], "errors": [], "sample_id": ["id_0", "id_0"]}	36	2053	macro_node_2_2018-08-22T12-40-46/root/sink	2
12726	macro_node_2_2018-08-22T12-40-46/root/sink/macro_node_2___sink___id_0__id_1___0			2018-08-22 10:42:33.995602	2018-08-22 10:44:12.474888	{"sample_index": [0, 1], "errors": [], "sample_id": ["id_0", "id_1"]}	36	2053	macro_node_2_2018-08-22T12-40-46/root/sink	2
12722	macro_node_2_2018-08-22T12-40-46/root/sum/macro_node_2___sum___id_1__id_1			2018-08-22 10:42:33.72849	2018-08-22 10:43:11.432518	{"sample_index": [1, 1], "errors": [], "sample_id": ["id_1", "id_1"]}	36	2047	macro_node_2_2018-08-22T12-40-46/root/sum	2
13829	iris/root/5_BET_and_REG/124_const_combine_labels_number_of_classes_0/job___002			2018-09-08 13:19:19.216895	2018-09-08 13:19:19.216895	{}	75	2443	iris/root/5_BET_and_REG/124_const_combine_labels_number_of_classes_0	2
13830	iris/root/5_BET_and_REG/63_const_brain_mask_registation_threads_0/job___001			2018-09-08 13:19:19.216895	2018-09-08 13:19:19.216895	{}	75	2441	iris/root/5_BET_and_REG/63_const_brain_mask_registation_threads_0	2
13831	iris/root/65_spm_tissue_quantification/69_outputs/72_csf_map/job___001			2018-09-08 13:19:19.216895	2018-09-08 13:19:19.216895	{}	75	2477	iris/root/65_spm_tissue_quantification/69_outputs/72_csf_map	2
13832	iris/root/6_qc_overview/42_t1_with_gm_outline/job___002			2018-09-08 13:19:19.216895	2018-09-08 13:19:19.216895	{}	75	2450	iris/root/6_qc_overview/42_t1_with_gm_outline	2
13833	iris/root/5_BET_and_REG/30_combine_labels/job___001			2018-09-08 13:19:19.216895	2018-09-08 13:19:19.216895	{}	75	2428	iris/root/5_BET_and_REG/30_combine_labels	2
13834	iris/root/0_statistics/26_const_get_wm_volume_volume_0/job___002			2018-09-08 13:19:19.216895	2018-09-08 13:19:19.216895	{}	75	2394	iris/root/0_statistics/26_const_get_wm_volume_volume_0	2
13841	iris/root/0_statistics/18_const_get_wm_volume_flatten_0/job___001			2018-09-08 13:19:20.577213	2018-09-08 13:19:20.577213	{}	75	2391	iris/root/0_statistics/18_const_get_wm_volume_flatten_0	2
13837	iris/root/0_statistics/129_const_get_gm_volume_selection_0/job___001			2018-09-08 13:19:20.577213	2018-09-08 13:19:20.577213	{}	75	2404	iris/root/0_statistics/129_const_get_gm_volume_selection_0	2
13835	iris/root/1_dicom_to_nifti/44_reformat_t1/job___001			2018-09-08 13:19:20.577213	2018-09-08 13:19:20.577213	{}	75	2406	iris/root/1_dicom_to_nifti/44_reformat_t1	2
13836	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/job___002			2018-09-08 13:19:20.577213	2018-09-08 13:19:20.577213	{}	75	2470	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm	2
13838	iris/root/0_statistics/60_const_get_brain_volume_volume_0/job___002			2018-09-08 13:19:20.577213	2018-09-08 13:19:20.577213	{}	75	2401	iris/root/0_statistics/60_const_get_brain_volume_volume_0	2
13839	iris/root/5_BET_and_REG/131_const_dilate_brainmask_operation_0/job___001			2018-09-08 13:19:20.577213	2018-09-08 13:19:20.577213	{}	75	2446	iris/root/5_BET_and_REG/131_const_dilate_brainmask_operation_0	2
13844	iris/root/65_spm_tissue_quantification/67_InputImage/77_const_t1_passthrough_operator_0/job___002			2018-09-08 13:19:20.577213	2018-09-09 17:56:01.923144	{"command": ["python", "/home/tkroes/python_356/venv/lib/python3.5/site-packages/fastr/resources/tools/fastr/util/0.1/bin/fail.py", "--in_1", "9", "--in_2", "9", "--fail_1"], "errors": [["FastrOutputValidationError", "Could not find result for output out_1", "/home/tkroes/python_356/venv/lib/python3.5/site-packages/fastr/execution/job.py", 990], ["FastrValueError", "Output values are not valid!", "/home/tkroes/python_356/venv/lib/python3.5/site-packages/fastr/execution/job.py", 749]], "hostinfo": {"cwd": "/home/tkroes/python_356", "drmaa": {"jobid": "2816426", "jobname": "fastr_failing_network___step_2___sample_1_3", "taskid": "undefined"}, "envvar": {"ARC": "linux-x64", "BASH_FUNC_module%%": "() {  eval `/usr/local/Modules/$MODULE_VERSION/bin/modulecmd bash $*`\\n}", "COMP_WORDBREAKS": " \\t\\n\\"'><;|&(:", "DERBY_HOME": "/usr/lib/jvm/java-8-oracle/db", "DISPLAY": "localhost:18.0", "DRMAA_LIBRARY_PATH": "/usr/local/OpenGridScheduler/gridengine/lib/linux-x64/libdrmaa.so.1.0", "ENVIRONMENT": "BATCH", "HISTCONTROL": "ignoreboth", "HISTFILESIZE": "2000", "HISTSIZE": "1000", "HISTTIMEFORMAT": "'%a, %d %b %Y %l:%M:%S%p %z '", "HOME": "/home/tkroes", "HOSTNAME": "res-hpc-gpu02.researchlumc.nl", "J2REDIR": "/usr/lib/jvm/java-8-oracle/jre", "J2SDKDIR": "/usr/lib/jvm/java-8-oracle", "JAVA_HOME": "/usr/lib/jvm/java-8-oracle", "JOB_ID": "2816426", "JOB_NAME": "fastr_failing_network___step_2___sample_1_3", "JOB_SCRIPT": "/home/tkroes/python_356/venv/bin/python", "KRB5CCNAME": "FILE:/tmp/krb5cc_144904_tLvS7Y", "LANG": "en_US.UTF-8", "LANGUAGE": "en_US:", "LC_ADDRESS": "nl_NL.UTF-8", "LC_ALL": "en_US.UTF-8", "LC_IDENTIFICATION": "nl_NL.UTF-8", "LC_MEASUREMENT": "nl_NL.UTF-8", "LC_MONETARY": "nl_NL.UTF-8", "LC_NAME": "nl_NL.UTF-8", "LC_NUMERIC": "nl_NL.UTF-8", "LC_PAPER": "nl_NL.UTF-8", "LC_TELEPHONE": "nl_NL.UTF-8", "LC_TIME": "nl_NL.UTF-8", "LD_LIBRARY_PATH": "/usr/local/OpenGridScheduler/gridengine/lib/linux-x64/:/usr/local/jemalloc/current/lib/:/usr/local/lib/:/usr/local/ScaLAPACK/scalapack-2.0.2/lib/", "LESSCLOSE": "/usr/bin/lesspipe %s %s", "LESSOPEN": "| /usr/bin/lesspipe %s", "LOADEDMODULES": "OpenGridScheduler:History", "LOGNAME": "tkroes", "LS_COLORS": "rs", "MAIL": "/var/mail/tkroes", "MANPATH": "/usr/local/OpenGridScheduler/gridengine/man:/usr/share/man:/usr/local/share/man", "MODULEPATH": "/usr/local/Modules/versions:/usr/local/Modules/$MODULE_VERSION/modulefiles:/usr/local/Modules/modulefiles:/home/tkroes/tkroes/BBMRI/.local/bbmri/easybuild/modules/all", "MODULESHOME": "/usr/local/Modules/3.2.10", "MODULE_VERSION": "3.2.10", "MODULE_VERSION_STACK": "3.2.10", "NHOSTS": "1", "NQUEUES": "1", "NSLOTS": "1", "OLDPWD": "/home/tkroes", "PATH": "/tmp/2816426.1.all.q:/home/tkroes/python_356/venv/bin:/usr/local/OpenGridScheduler/gridengine/bin/linux-x64:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/jvm/java-8-oracle/bin:/usr/lib/jvm/java-8-oracle/db/bin:/usr/lib/jvm/java-8-oracle/jre/bin:/opt/dell/srvadmin/bin", "PBS_O_INITDIR": "/home/tkroes/python_356", "PERL5LIB": "/usr/share/perl5/:/usr/share/perl/:/usr/local/ensembl-api-56/ensembl/modules:/usr/local/ensembl-api-56/ensembl-compara/modules:/usr/local/ensembl-api-56/ensembl-variation/modules:/usr/local/ensembl-api-56/ensembl-functgenomics/modules:/usr/local/MedStat/privatePerl:/usr/local/bcl2fastq/bcl2fastq_v1.8.4/bcl2fastq_v1.8.4-build/lib/bcl2fastq-1.8.4/perl:/usr/local/vcftools/current/share/perl/5.18.2:/usr/lib/perl5/", "PS1": "(venv) \\\\[\\\\e]0;\\\\u@\\\\h: \\\\w\\\\a\\\\]${debian_chroot:+($debian_chroot)}\\\\u@\\\\h:\\\\w\\\\$ ", "PWD": "/home/tkroes/python_356", "QUEUE": "all.q", "REQNAME": "fastr_failing_network___step_2___sample_1_3", "REQUEST": "fastr_failing_network___step_2___sample_1_3", "RESTARTED": "0", "SGE_ACCOUNT": "sge", "SGE_ARCH": "linux-x64", "SGE_BINARY_PATH": "/usr/local/OpenGridScheduler/gridengine/bin/linux-x64", "SGE_CELL": "default", "SGE_CLUSTER_NAME": "p6444", "SGE_CWD_PATH": "/home/tkroes/python_356", "SGE_EXECD_PORT": "6445", "SGE_JOB_SPOOL_DIR": "/var/spool/sge/res-hpc-gpu02/active_jobs/2816426.1", "SGE_O_HOME": "/home/tkroes", "SGE_O_HOST": "wingheadshark", "SGE_O_LOGNAME": "tkroes", "SGE_O_MAIL": "/var/mail/tkroes", "SGE_O_PATH": "/home/tkroes/python_356/venv/bin:/usr/local/OpenGridScheduler/gridengine/bin/linux-x64:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/jvm/java-8-oracle/bin:/usr/lib/jvm/java-8-oracle/db/bin:/usr/lib/jvm/java-8-oracle/jre/bin:/opt/dell/srvadmin/bin", "SGE_O_SHELL": "/bin/bash", "SGE_O_WORKDIR": "/home/tkroes/python_356", "SGE_QMASTER_PORT": "6444", "SGE_ROOT": "/usr/local/OpenGridScheduler/gridengine", "SGE_STDERR_PATH": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_failing_network_2018-09-06T15-03-16_lgalkkq4/step_2/sample_1_3/__fastr_stderr__.txt", "SGE_STDIN_PATH": "/dev/null", "SGE_STDOUT_PATH": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_failing_network_2018-09-06T15-03-16_lgalkkq4/step_2/sample_1_3/__fastr_stdout__.txt", "SGE_TASK_FIRST": "undefined", "SGE_TASK_ID": "undefined", "SGE_TASK_LAST": "undefined", "SGE_TASK_STEPSIZE": "undefined", "SHELL": "/bin/bash", "SHLVL": "1", "SSH_CLIENT": "145.88.35.10 55252 22", "SSH_CONNECTION": "145.88.35.10 55252 145.88.66.103 22", "SSH_TTY": "/dev/pts/37", "TERM": "xterm-256color", "TMP": "/tmp/2816426.1.all.q", "TMPDIR": "/tmp/2816426.1.all.q", "USER": "tkroes", "VIRTUAL_ENV": "/home/tkroes/python_356/venv", "_": "/home/tkroes/python_356/venv/bin/python", "_LMFILES_": "/usr/local/Modules/3.2.10/modulefiles/OpenGridScheduler:/usr/local/Modules/3.2.10/modulefiles/History"}, "os": {"is64bits": 1, "maxsize": 9223372036854775807, "system": "Linux", "version": {"distname": "debian", "id": "", "libc": {"libname": "glibc", "version": "2.9"}, "version": "jessie/sid"}}, "python": {"branch": "", "compiler": "GCC 4.8.5", "implementation": "CPython", "revision": "", "version": "3.5.6"}, "uname": ["Linux", "res-hpc-gpu02", "4.4.0-119-generic", "#143~14.04.1-Ubuntu SMP Mon Apr 2 18:04:36 UTC 2018", "x86_64", "x86_64"], "username": "tkroes"}, "input_hash": null, "output_hash": null, "returncode": 0, "sample_id": ["sample_1_3"], "sample_index": [3], "stderr": "", "stdout": "Namespace(fail_1=True, fail_2=False, in_1=9, in_2=9)\\nin 1  : 9\\nin 2  : 9\\nfail_1: True\\nfail_2: False\\nRESULT_2=[10]\\n", "time_elapsed": 0.05689096450805664, "tool_name": null, "tool_version": null}	75	2463	iris/root/65_spm_tissue_quantification/67_InputImage/77_const_t1_passthrough_operator_0	3
13840	iris/root/5_BET_and_REG/49_const_t1w_registration_parameters_0/job___001			2018-09-08 13:19:20.577213	2018-09-08 13:19:20.577213	{}	75	2435	iris/root/5_BET_and_REG/49_const_t1w_registration_parameters_0	2
13842	iris/root/6_qc_overview/122_t1_with_brain/job___001			2018-09-08 13:19:20.577213	2018-09-08 13:19:20.577213	{}	75	2454	iris/root/6_qc_overview/122_t1_with_brain	2
13205	example_2018-09-03T12-31-11/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 10:31:13.802676	2018-09-03 10:31:13.913362	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	57	2204	example_2018-09-03T12-31-11/root/const_addint_right_hand_0	2
13843	iris/root/5_BET_and_REG/58_const_combine_brains_method_0/job___001			2018-09-08 13:19:20.577213	2018-09-08 13:19:20.577213	{}	75	2439	iris/root/5_BET_and_REG/58_const_combine_brains_method_0	2
13204	example_2018-09-03T12-31-11/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 10:31:13.594782	2018-09-03 10:31:13.698801	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	57	2204	example_2018-09-03T12-31-11/root/const_addint_right_hand_0	2
12976	example_2018-09-03T10-32-43/root/sink1/example___sink1___s3___0			2018-09-03 08:32:46.399397	2018-09-03 08:33:19.184943	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	43	2126	example_2018-09-03T10-32-43/root/sink1	2
12977	example_2018-09-03T10-32-43/root/sink1/example___sink1___s4___0			2018-09-03 08:32:46.510075	2018-09-03 08:33:24.567068	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	43	2126	example_2018-09-03T10-32-43/root/sink1	2
13206	example_2018-09-03T12-31-11/root/addint/example___addint___s1			2018-09-03 10:31:14.029956	2018-09-03 10:31:18.21064	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	57	2203	example_2018-09-03T12-31-11/root/addint	2
13211	example_2018-09-03T12-31-11/root/sink1/example___sink1___s2___0			2018-09-03 10:31:14.580178	2018-09-03 10:31:37.310072	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	57	2202	example_2018-09-03T12-31-11/root/sink1	3
13207	example_2018-09-03T12-31-11/root/addint/example___addint___s2			2018-09-03 10:31:14.137717	2018-09-03 10:31:21.53868	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	57	2203	example_2018-09-03T12-31-11/root/addint	2
13209	example_2018-09-03T12-31-11/root/addint/example___addint___s4			2018-09-03 10:31:14.364186	2018-09-03 10:31:28.615099	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	57	2203	example_2018-09-03T12-31-11/root/addint	2
13851	iris/root/5_BET_and_REG/15_const_bet_bias_cleanup_0/job___001			2018-09-08 13:19:21.933212	2018-09-08 13:19:21.933212	{}	75	2422	iris/root/5_BET_and_REG/15_const_bet_bias_cleanup_0	2
13210	example_2018-09-03T12-31-11/root/sink1/example___sink1___s1___0			2018-09-03 10:31:14.475785	2018-09-03 10:31:32.990661	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	57	2202	example_2018-09-03T12-31-11/root/sink1	3
13208	example_2018-09-03T12-31-11/root/addint/example___addint___s3			2018-09-03 10:31:14.245747	2018-09-03 10:31:24.87337	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	57	2203	example_2018-09-03T12-31-11/root/addint	2
13850	iris/root/5_BET_and_REG/124_const_combine_labels_number_of_classes_0/job___001			2018-09-08 13:19:21.933212	2018-09-08 13:19:21.933212	{}	75	2443	iris/root/5_BET_and_REG/124_const_combine_labels_number_of_classes_0	2
13849	iris/root/5_BET_and_REG/15_const_bet_bias_cleanup_0/job___002			2018-09-08 13:19:21.933212	2018-09-08 13:19:21.933212	{}	75	2422	iris/root/5_BET_and_REG/15_const_bet_bias_cleanup_0	2
13848	iris/root/5_BET_and_REG/56_brain_mask_registation/job___001			2018-09-08 13:19:21.933212	2018-09-08 13:19:21.933212	{}	75	2438	iris/root/5_BET_and_REG/56_brain_mask_registation	2
13856	iris/root/0_statistics/39_const_get_gm_volume_volume_0/job___001			2018-09-08 13:19:23.282157	2018-09-08 13:19:23.282157	{}	75	2395	iris/root/0_statistics/39_const_get_gm_volume_volume_0	2
13845	iris/root/0_statistics/11_get_gm_volume/job___001			2018-09-08 13:19:21.933212	2018-09-08 13:19:21.933212	{}	75	2389	iris/root/0_statistics/11_get_gm_volume	2
13846	iris/root/4_output/16_wm_map_output/job___002			2018-09-08 13:19:21.933212	2018-09-08 13:19:21.933212	{}	75	2413	iris/root/4_output/16_wm_map_output	2
13847	iris/root/0_statistics/14_get_brain_volume/job___002			2018-09-08 13:19:21.933212	2018-09-08 13:19:21.933212	{}	75	2390	iris/root/0_statistics/14_get_brain_volume	2
13852	iris/root/0_statistics/19_mask_hammers_labels/job___001			2018-09-08 13:19:21.933212	2018-09-08 13:19:21.933212	{}	75	2392	iris/root/0_statistics/19_mask_hammers_labels	2
13853	iris/root/5_BET_and_REG/12_t1w_registration/job___002			2018-09-08 13:19:21.933212	2018-09-08 13:19:21.933212	{}	75	2420	iris/root/5_BET_and_REG/12_t1w_registration	2
13854	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/job___002			2018-09-08 13:19:21.933212	2018-09-08 13:19:21.933212	{}	75	2474	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm	2
12981	example_2018-09-03T10-36-42/root/source1/example___source1___s4			2018-09-03 08:36:43.947365	2018-09-03 08:36:44.049741	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	44	2130	example_2018-09-03T10-36-42/root/source1	2
13855	iris/root/65_spm_tissue_quantification/69_outputs/80_gm_map/job___002			2018-09-08 13:19:23.282157	2018-09-08 13:19:23.282157	{}	75	2479	iris/root/65_spm_tissue_quantification/69_outputs/80_gm_map	2
12979	example_2018-09-03T10-36-42/root/source1/example___source1___s2			2018-09-03 08:36:43.504293	2018-09-03 08:36:43.614914	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	44	2130	example_2018-09-03T10-36-42/root/source1	2
12978	example_2018-09-03T10-36-42/root/source1/example___source1___s1			2018-09-03 08:36:43.280336	2018-09-03 08:36:43.385274	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	44	2130	example_2018-09-03T10-36-42/root/source1	2
12980	example_2018-09-03T10-36-42/root/source1/example___source1___s3			2018-09-03 08:36:43.725731	2018-09-03 08:36:43.839756	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	44	2130	example_2018-09-03T10-36-42/root/source1	2
12982	example_2018-09-03T10-36-42/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 08:36:44.15895	2018-09-03 08:36:44.267737	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	44	2133	example_2018-09-03T10-36-42/root/const_addint_right_hand_0	2
12983	example_2018-09-03T10-36-42/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 08:36:44.378091	2018-09-03 08:36:44.483826	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	44	2133	example_2018-09-03T10-36-42/root/const_addint_right_hand_0	2
13212	example_2018-09-03T12-31-11/root/sink1/example___sink1___s3___0			2018-09-03 10:31:14.699923	2018-09-03 10:31:41.664623	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	57	2202	example_2018-09-03T12-31-11/root/sink1	3
13857	iris/root/0_statistics/60_const_get_brain_volume_volume_0/job__001			2018-09-09 16:35:44.56421	2018-09-09 16:35:44.56421	{}	75	2401	iris/root/0_statistics/60_const_get_brain_volume_volume_0	3
13864	add_ints_2018-09-13T15-55-06/root/add/add_ints___add___id_0			2018-09-13 13:55:13.491985	2018-09-13 13:55:18.321385	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	76	2485	add_ints_2018-09-13T15-55-06/root/add	2
13858	iris/root/5_BET_and_REG/13_const_threshold_labels_upper_threshold_0/job__001			2018-09-09 16:36:45.357986	2018-09-09 16:36:45.357986	{}	75	2421	iris/root/5_BET_and_REG/13_const_threshold_labels_upper_threshold_0	3
13869	add_ints_2018-09-13T15-55-06/root/sink/add_ints___sink___id_0___0			2018-09-13 13:55:13.919686	2018-09-13 13:55:38.713808	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	76	2487	add_ints_2018-09-13T15-55-06/root/sink	2
13859	iris/root/65_spm_tissue_quantification/69_outputs/88_gm_spm/job__002			2018-09-09 16:58:20.63495	2018-09-09 16:58:20.63495	{}	75	2481	iris/root/65_spm_tissue_quantification/69_outputs/88_gm_spm	0
13860	iris/root/65_spm_tissue_quantification/69_outputs/73_wm_map/job__001			2018-09-09 16:58:43.589391	2018-09-09 17:01:25.822915	{}	75	2478	iris/root/65_spm_tissue_quantification/69_outputs/73_wm_map	4
13861	add_ints_2018-09-13T15-55-06/root/source/add_ints___source___id_0			2018-09-13 13:55:07.079571	2018-09-13 13:55:07.164586	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	76	2484	add_ints_2018-09-13T15-55-06/root/source	2
13870	add_ints_2018-09-13T15-55-06/root/sink/add_ints___sink___id_1_0___0			2018-09-13 13:55:14.012711	2018-09-13 13:55:43.191115	{"sample_id": ["id_1_0"], "sample_index": [1], "errors": []}	76	2487	add_ints_2018-09-13T15-55-06/root/sink	2
13863	add_ints_2018-09-13T15-55-06/root/const_add_right_hand_0/add_ints___const_add_right_hand_0___id_0			2018-09-13 13:55:07.321215	2018-09-13 13:55:07.403013	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	76	2486	add_ints_2018-09-13T15-55-06/root/const_add_right_hand_0	2
13862	add_ints_2018-09-13T15-55-06/root/source/add_ints___source___id_1			2018-09-13 13:55:07.23971	2018-09-13 13:55:13.219583	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	76	2484	add_ints_2018-09-13T15-55-06/root/source	2
13871	add_ints_2018-09-13T15-55-06/root/sink/add_ints___sink___id_1_1___0			2018-09-13 13:55:14.098041	2018-09-13 13:55:48.167491	{"sample_id": ["id_1_1"], "sample_index": [2], "errors": []}	76	2487	add_ints_2018-09-13T15-55-06/root/sink	2
13865	add_ints_2018-09-13T15-55-06/root/add/add_ints___add___id_1_0			2018-09-13 13:55:13.57326	2018-09-13 13:55:22.33903	{"sample_id": ["id_1_0"], "sample_index": [1], "errors": []}	76	2485	add_ints_2018-09-13T15-55-06/root/add	2
13866	add_ints_2018-09-13T15-55-06/root/add/add_ints___add___id_1_1			2018-09-13 13:55:13.65899	2018-09-13 13:55:25.971391	{"sample_id": ["id_1_1"], "sample_index": [2], "errors": []}	76	2485	add_ints_2018-09-13T15-55-06/root/add	2
13872	add_ints_2018-09-13T15-55-06/root/sink/add_ints___sink___id_1_2___0			2018-09-13 13:55:14.187901	2018-09-13 13:55:53.612796	{"sample_id": ["id_1_2"], "sample_index": [3], "errors": []}	76	2487	add_ints_2018-09-13T15-55-06/root/sink	2
13873	add_ints_2018-09-13T15-55-06/root/sink/add_ints___sink___id_1_3___0			2018-09-13 13:55:14.275317	2018-09-13 13:55:57.823684	{"sample_id": ["id_1_3"], "sample_index": [4], "errors": []}	76	2487	add_ints_2018-09-13T15-55-06/root/sink	2
13867	add_ints_2018-09-13T15-55-06/root/add/add_ints___add___id_1_2			2018-09-13 13:55:13.745753	2018-09-13 13:55:30.966153	{"sample_id": ["id_1_2"], "sample_index": [3], "errors": []}	76	2485	add_ints_2018-09-13T15-55-06/root/add	2
13868	add_ints_2018-09-13T15-55-06/root/add/add_ints___add___id_1_3			2018-09-13 13:55:13.835191	2018-09-13 13:55:34.436821	{"sample_id": ["id_1_3"], "sample_index": [4], "errors": []}	76	2485	add_ints_2018-09-13T15-55-06/root/add	2
12984	example_2018-09-03T10-36-42/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 08:36:44.588193	2018-09-03 08:36:44.694187	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	44	2133	example_2018-09-03T10-36-42/root/const_addint_right_hand_0	2
12985	example_2018-09-03T10-36-42/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 08:36:44.803844	2018-09-03 08:36:44.909571	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	44	2133	example_2018-09-03T10-36-42/root/const_addint_right_hand_0	2
12986	example_2018-09-03T10-36-42/root/addint/example___addint___s1			2018-09-03 08:36:45.0222	2018-09-03 08:36:49.296871	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	44	2132	example_2018-09-03T10-36-42/root/addint	2
12987	example_2018-09-03T10-36-42/root/addint/example___addint___s2			2018-09-03 08:36:45.128107	2018-09-03 08:36:52.739514	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	44	2132	example_2018-09-03T10-36-42/root/addint	2
12991	example_2018-09-03T10-36-42/root/sink1/example___sink1___s2___0			2018-09-03 08:36:45.582406	2018-09-03 08:37:08.571589	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	44	2131	example_2018-09-03T10-36-42/root/sink1	2
12992	example_2018-09-03T10-36-42/root/sink1/example___sink1___s3___0			2018-09-03 08:36:45.695184	2018-09-03 08:37:12.945226	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	44	2131	example_2018-09-03T10-36-42/root/sink1	2
12996	example_2018-09-03T10-37-58/root/source1/example___source1___s3			2018-09-03 08:37:59.692823	2018-09-03 08:37:59.796645	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	45	2135	example_2018-09-03T10-37-58/root/source1	2
12993	example_2018-09-03T10-36-42/root/sink1/example___sink1___s4___0			2018-09-03 08:36:45.79815	2018-09-03 08:37:17.363906	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	44	2131	example_2018-09-03T10-36-42/root/sink1	2
12995	example_2018-09-03T10-37-58/root/source1/example___source1___s2			2018-09-03 08:37:59.479667	2018-09-03 08:37:59.58348	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	45	2135	example_2018-09-03T10-37-58/root/source1	2
12988	example_2018-09-03T10-36-42/root/addint/example___addint___s3			2018-09-03 08:36:45.252633	2018-09-03 08:36:56.227794	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	44	2132	example_2018-09-03T10-36-42/root/addint	2
12989	example_2018-09-03T10-36-42/root/addint/example___addint___s4			2018-09-03 08:36:45.359756	2018-09-03 08:36:59.657275	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	44	2132	example_2018-09-03T10-36-42/root/addint	2
12990	example_2018-09-03T10-36-42/root/sink1/example___sink1___s1___0			2018-09-03 08:36:45.470487	2018-09-03 08:37:04.103222	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	44	2131	example_2018-09-03T10-36-42/root/sink1	2
12994	example_2018-09-03T10-37-58/root/source1/example___source1___s1			2018-09-03 08:37:59.252689	2018-09-03 08:37:59.366814	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	45	2135	example_2018-09-03T10-37-58/root/source1	2
12999	example_2018-09-03T10-37-58/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 08:38:00.342967	2018-09-03 08:38:00.4529	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	45	2138	example_2018-09-03T10-37-58/root/const_addint_right_hand_0	2
12997	example_2018-09-03T10-37-58/root/source1/example___source1___s4			2018-09-03 08:37:59.904725	2018-09-03 08:38:00.013503	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	45	2135	example_2018-09-03T10-37-58/root/source1	2
12998	example_2018-09-03T10-37-58/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 08:38:00.11883	2018-09-03 08:38:00.226856	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	45	2138	example_2018-09-03T10-37-58/root/const_addint_right_hand_0	2
13000	example_2018-09-03T10-37-58/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 08:38:00.560816	2018-09-03 08:38:00.673784	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	45	2138	example_2018-09-03T10-37-58/root/const_addint_right_hand_0	2
13001	example_2018-09-03T10-37-58/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 08:38:00.78326	2018-09-03 08:38:00.891035	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	45	2138	example_2018-09-03T10-37-58/root/const_addint_right_hand_0	2
13002	example_2018-09-03T10-37-58/root/addint/example___addint___s1			2018-09-03 08:38:00.997594	2018-09-03 08:38:05.74492	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	45	2137	example_2018-09-03T10-37-58/root/addint	2
12593	failing_macro_top_level_2018-08-22T12-15-04/root/add/failing_macro_top_level___add___sample_1_3			2018-08-22 10:20:58.857933	2018-08-22 10:20:59.601677	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	34	2005	failing_macro_top_level_2018-08-22T12-15-04/root/add	4
13003	example_2018-09-03T10-37-58/root/addint/example___addint___s2			2018-09-03 08:38:01.105731	2018-09-03 08:38:09.282849	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	45	2137	example_2018-09-03T10-37-58/root/addint	2
12596	failing_macro_top_level_2018-08-22T12-15-04/root/sink/failing_macro_top_level___sink___sample_1_2___0			2018-08-22 10:20:59.0809	2018-08-22 10:20:59.932736	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	34	2006	failing_macro_top_level_2018-08-22T12-15-04/root/sink	4
12595	failing_macro_top_level_2018-08-22T12-15-04/root/sink/failing_macro_top_level___sink___sample_1_1___0			2018-08-22 10:20:59.014965	2018-08-22 10:20:59.795644	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	34	2006	failing_macro_top_level_2018-08-22T12-15-04/root/sink	4
12598	macro_top_level_2018-08-22T12-36-32/root/source/source/macro_top_level___source___id_0			2018-08-22 10:36:33.263715	2018-08-22 10:36:33.322136	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	35	2027	macro_top_level_2018-08-22T12-36-32/root/source/source	2
12597	failing_macro_top_level_2018-08-22T12-15-04/root/sink/failing_macro_top_level___sink___sample_1_3___0			2018-08-22 10:20:59.14644	2018-08-22 10:21:00.066025	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	34	2006	failing_macro_top_level_2018-08-22T12-15-04/root/sink	4
12594	failing_macro_top_level_2018-08-22T12-15-04/root/sink/failing_macro_top_level___sink___sample_1_0___0			2018-08-22 10:20:58.950085	2018-08-22 10:22:35.74179	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	34	2006	failing_macro_top_level_2018-08-22T12-15-04/root/sink	2
12591	failing_macro_top_level_2018-08-22T12-15-04/root/add/failing_macro_top_level___add___sample_1_1			2018-08-22 10:20:58.699421	2018-08-22 10:20:59.276638	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	34	2005	failing_macro_top_level_2018-08-22T12-15-04/root/add	4
12592	failing_macro_top_level_2018-08-22T12-15-04/root/add/failing_macro_top_level___add___sample_1_2			2018-08-22 10:20:58.780395	2018-08-22 10:20:59.438017	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	34	2005	failing_macro_top_level_2018-08-22T12-15-04/root/add	4
12599	macro_top_level_2018-08-22T12-36-32/root/source/source/macro_top_level___source___id_1			2018-08-22 10:36:33.3938	2018-08-22 10:37:19.813265	{"sample_index": [1], "errors": [], "sample_id": ["id_1"]}	35	2027	macro_top_level_2018-08-22T12-36-32/root/source/source	2
13004	example_2018-09-03T10-37-58/root/addint/example___addint___s3			2018-09-03 08:38:01.225895	2018-09-03 08:38:13.17071	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	45	2137	example_2018-09-03T10-37-58/root/addint	2
13005	example_2018-09-03T10-37-58/root/addint/example___addint___s4			2018-09-03 08:38:01.339037	2018-09-03 08:38:16.633571	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	45	2137	example_2018-09-03T10-37-58/root/addint	2
13006	example_2018-09-03T10-37-58/root/sink1/example___sink1___s1___0			2018-09-03 08:38:01.459558	2018-09-03 08:38:21.09104	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	45	2136	example_2018-09-03T10-37-58/root/sink1	2
13008	example_2018-09-03T10-37-58/root/sink1/example___sink1___s3___0			2018-09-03 08:38:01.683466	2018-09-03 08:38:29.962208	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	45	2136	example_2018-09-03T10-37-58/root/sink1	2
13007	example_2018-09-03T10-37-58/root/sink1/example___sink1___s2___0			2018-09-03 08:38:01.565462	2018-09-03 08:38:25.558734	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	45	2136	example_2018-09-03T10-37-58/root/sink1	2
13009	example_2018-09-03T10-37-58/root/sink1/example___sink1___s4___0			2018-09-03 08:38:01.796109	2018-09-03 08:38:34.551014	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	45	2136	example_2018-09-03T10-37-58/root/sink1	2
13010	example_2018-09-03T10-41-42/root/source1/example___source1___s1			2018-09-03 08:41:42.868555	2018-09-03 08:41:42.979636	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	46	2140	example_2018-09-03T10-41-42/root/source1	2
13012	example_2018-09-03T10-41-42/root/source1/example___source1___s3			2018-09-03 08:41:43.270917	2018-09-03 08:41:43.368392	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	46	2140	example_2018-09-03T10-41-42/root/source1	2
13011	example_2018-09-03T10-41-42/root/source1/example___source1___s2			2018-09-03 08:41:43.073475	2018-09-03 08:41:43.170704	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	46	2140	example_2018-09-03T10-41-42/root/source1	2
13013	example_2018-09-03T10-41-42/root/source1/example___source1___s4			2018-09-03 08:41:43.459697	2018-09-03 08:41:43.56623	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	46	2140	example_2018-09-03T10-41-42/root/source1	2
13014	example_2018-09-03T10-41-42/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 08:41:43.680663	2018-09-03 08:41:43.785522	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	46	2143	example_2018-09-03T10-41-42/root/const_addint_right_hand_0	2
13015	example_2018-09-03T10-41-42/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 08:41:43.881751	2018-09-03 08:41:43.99769	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	46	2143	example_2018-09-03T10-41-42/root/const_addint_right_hand_0	2
13016	example_2018-09-03T10-41-42/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 08:41:44.099986	2018-09-03 08:41:44.203163	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	46	2143	example_2018-09-03T10-41-42/root/const_addint_right_hand_0	2
13020	example_2018-09-03T10-41-42/root/addint/example___addint___s3			2018-09-03 08:41:44.749112	2018-09-03 08:41:55.671423	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	46	2142	example_2018-09-03T10-41-42/root/addint	2
13017	example_2018-09-03T10-41-42/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 08:41:44.315633	2018-09-03 08:41:44.41975	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	46	2143	example_2018-09-03T10-41-42/root/const_addint_right_hand_0	2
13018	example_2018-09-03T10-41-42/root/addint/example___addint___s1			2018-09-03 08:41:44.535054	2018-09-03 08:41:48.874809	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	46	2142	example_2018-09-03T10-41-42/root/addint	2
13019	example_2018-09-03T10-41-42/root/addint/example___addint___s2			2018-09-03 08:41:44.641939	2018-09-03 08:41:52.27374	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	46	2142	example_2018-09-03T10-41-42/root/addint	2
13021	example_2018-09-03T10-41-42/root/addint/example___addint___s4			2018-09-03 08:41:44.846106	2018-09-03 08:41:59.701388	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	46	2142	example_2018-09-03T10-41-42/root/addint	2
13025	example_2018-09-03T10-41-42/root/sink1/example___sink1___s4___0			2018-09-03 08:41:45.273885	2018-09-03 08:42:17.332817	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	46	2141	example_2018-09-03T10-41-42/root/sink1	2
13023	example_2018-09-03T10-41-42/root/sink1/example___sink1___s2___0			2018-09-03 08:41:45.062859	2018-09-03 08:42:08.582695	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	46	2141	example_2018-09-03T10-41-42/root/sink1	2
13022	example_2018-09-03T10-41-42/root/sink1/example___sink1___s1___0			2018-09-03 08:41:44.951743	2018-09-03 08:42:04.112791	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	46	2141	example_2018-09-03T10-41-42/root/sink1	2
13024	example_2018-09-03T10-41-42/root/sink1/example___sink1___s3___0			2018-09-03 08:41:45.168107	2018-09-03 08:42:12.956816	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	46	2141	example_2018-09-03T10-41-42/root/sink1	2
13032	example_2018-09-03T10-47-39/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 08:47:41.282462	2018-09-03 08:47:41.387588	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	47	2148	example_2018-09-03T10-47-39/root/const_addint_right_hand_0	2
13026	example_2018-09-03T10-47-39/root/source1/example___source1___s1			2018-09-03 08:47:39.978484	2018-09-03 08:47:40.084041	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	47	2145	example_2018-09-03T10-47-39/root/source1	2
13029	example_2018-09-03T10-47-39/root/source1/example___source1___s4			2018-09-03 08:47:40.628976	2018-09-03 08:47:40.737677	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	47	2145	example_2018-09-03T10-47-39/root/source1	2
13027	example_2018-09-03T10-47-39/root/source1/example___source1___s2			2018-09-03 08:47:40.201978	2018-09-03 08:47:40.306483	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	47	2145	example_2018-09-03T10-47-39/root/source1	2
13031	example_2018-09-03T10-47-39/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 08:47:41.065866	2018-09-03 08:47:41.180185	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	47	2148	example_2018-09-03T10-47-39/root/const_addint_right_hand_0	2
13028	example_2018-09-03T10-47-39/root/source1/example___source1___s3			2018-09-03 08:47:40.414804	2018-09-03 08:47:40.518603	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	47	2145	example_2018-09-03T10-47-39/root/source1	2
13030	example_2018-09-03T10-47-39/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 08:47:40.848949	2018-09-03 08:47:40.959835	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	47	2148	example_2018-09-03T10-47-39/root/const_addint_right_hand_0	2
13033	example_2018-09-03T10-47-39/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 08:47:41.507618	2018-09-03 08:47:41.62282	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	47	2148	example_2018-09-03T10-47-39/root/const_addint_right_hand_0	2
13039	example_2018-09-03T10-47-39/root/sink1/example___sink1___s2___0			2018-09-03 08:47:42.283756	2018-09-03 08:48:01.296738	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	47	2146	example_2018-09-03T10-47-39/root/sink1	4
13037	example_2018-09-03T10-47-39/root/addint/example___addint___s4			2018-09-03 08:47:42.057794	2018-09-03 08:48:19.664634	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	47	2147	example_2018-09-03T10-47-39/root/addint	3
13034	example_2018-09-03T10-47-39/root/addint/example___addint___s1			2018-09-03 08:47:41.743636	2018-09-03 08:47:51.80186	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	47	2147	example_2018-09-03T10-47-39/root/addint	3
13035	example_2018-09-03T10-47-39/root/addint/example___addint___s2			2018-09-03 08:47:41.854106	2018-09-03 08:48:01.174573	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	47	2147	example_2018-09-03T10-47-39/root/addint	3
13036	example_2018-09-03T10-47-39/root/addint/example___addint___s3			2018-09-03 08:47:41.959056	2018-09-03 08:48:10.434775	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	47	2147	example_2018-09-03T10-47-39/root/addint	3
13038	example_2018-09-03T10-47-39/root/sink1/example___sink1___s1___0			2018-09-03 08:47:42.172435	2018-09-03 08:47:51.95314	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	47	2146	example_2018-09-03T10-47-39/root/sink1	4
13041	example_2018-09-03T10-47-39/root/sink1/example___sink1___s4___0			2018-09-03 08:47:42.504831	2018-09-03 08:48:19.776035	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	47	2146	example_2018-09-03T10-47-39/root/sink1	4
13040	example_2018-09-03T10-47-39/root/sink1/example___sink1___s3___0			2018-09-03 08:47:42.395973	2018-09-03 08:48:10.535748	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	47	2146	example_2018-09-03T10-47-39/root/sink1	4
13042	example_2018-09-03T11-01-12/root/source1/example___source1___s1			2018-09-03 09:01:12.392975	2018-09-03 09:01:12.500332	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	48	2150	example_2018-09-03T11-01-12/root/source1	2
13043	example_2018-09-03T11-01-12/root/source1/example___source1___s2			2018-09-03 09:01:12.606763	2018-09-03 09:01:12.720754	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	48	2150	example_2018-09-03T11-01-12/root/source1	2
13213	example_2018-09-03T12-31-11/root/sink1/example___sink1___s4___0			2018-09-03 10:31:14.808068	2018-09-03 10:31:46.105318	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	57	2202	example_2018-09-03T12-31-11/root/sink1	3
13046	example_2018-09-03T11-01-12/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 09:01:13.241	2018-09-03 09:01:13.35286	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	48	2153	example_2018-09-03T11-01-12/root/const_addint_right_hand_0	2
13044	example_2018-09-03T11-01-12/root/source1/example___source1___s3			2018-09-03 09:01:12.824662	2018-09-03 09:01:12.932842	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	48	2150	example_2018-09-03T11-01-12/root/source1	2
13047	example_2018-09-03T11-01-12/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 09:01:13.458283	2018-09-03 09:01:13.567486	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	48	2153	example_2018-09-03T11-01-12/root/const_addint_right_hand_0	2
13045	example_2018-09-03T11-01-12/root/source1/example___source1___s4			2018-09-03 09:01:13.040323	2018-09-03 09:01:13.140948	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	48	2150	example_2018-09-03T11-01-12/root/source1	2
13048	example_2018-09-03T11-01-12/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 09:01:13.67465	2018-09-03 09:01:13.779082	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	48	2153	example_2018-09-03T11-01-12/root/const_addint_right_hand_0	2
13049	example_2018-09-03T11-01-12/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 09:01:13.889908	2018-09-03 09:01:13.997783	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	48	2153	example_2018-09-03T11-01-12/root/const_addint_right_hand_0	2
13050	example_2018-09-03T11-01-12/root/addint/example___addint___s1			2018-09-03 09:01:14.109884	2018-09-03 09:01:24.174071	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	48	2152	example_2018-09-03T11-01-12/root/addint	3
12600	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value/macro_top_level_node_add_ints_macro_container___input_value___id_0			2018-08-22 10:37:20.553187	2018-08-22 10:37:20.613149	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	35	2028	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value	2
13056	example_2018-09-03T11-01-12/root/sink1/example___sink1___s3___0			2018-09-03 09:01:14.772643	2018-09-03 09:01:42.765466	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	48	2151	example_2018-09-03T11-01-12/root/sink1	4
13051	example_2018-09-03T11-01-12/root/addint/example___addint___s2			2018-09-03 09:01:14.218742	2018-09-03 09:01:33.435864	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	48	2152	example_2018-09-03T11-01-12/root/addint	3
13055	example_2018-09-03T11-01-12/root/sink1/example___sink1___s2___0			2018-09-03 09:01:14.656635	2018-09-03 09:01:33.539819	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	48	2151	example_2018-09-03T11-01-12/root/sink1	4
13052	example_2018-09-03T11-01-12/root/addint/example___addint___s3			2018-09-03 09:01:14.329529	2018-09-03 09:01:42.655888	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	48	2152	example_2018-09-03T11-01-12/root/addint	3
13053	example_2018-09-03T11-01-12/root/addint/example___addint___s4			2018-09-03 09:01:14.440864	2018-09-03 09:01:51.913108	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	48	2152	example_2018-09-03T11-01-12/root/addint	3
13058	example_2018-09-03T11-07-53/root/source1/example___source1___s1			2018-09-03 09:07:54.488702	2018-09-03 09:07:54.619888	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	49	2155	example_2018-09-03T11-07-53/root/source1	2
13059	example_2018-09-03T11-07-53/root/source1/example___source1___s2			2018-09-03 09:07:54.741868	2018-09-03 09:07:54.864158	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	49	2155	example_2018-09-03T11-07-53/root/source1	2
13057	example_2018-09-03T11-01-12/root/sink1/example___sink1___s4___0			2018-09-03 09:01:14.881343	2018-09-03 09:01:52.023746	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	48	2151	example_2018-09-03T11-01-12/root/sink1	4
13054	example_2018-09-03T11-01-12/root/sink1/example___sink1___s1___0			2018-09-03 09:01:14.548651	2018-09-03 09:01:24.278619	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	48	2151	example_2018-09-03T11-01-12/root/sink1	4
13060	example_2018-09-03T11-07-53/root/source1/example___source1___s3			2018-09-03 09:07:54.982242	2018-09-03 09:07:55.121557	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	49	2155	example_2018-09-03T11-07-53/root/source1	2
13061	example_2018-09-03T11-07-53/root/source1/example___source1___s4			2018-09-03 09:07:55.23897	2018-09-03 09:07:55.352505	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	49	2155	example_2018-09-03T11-07-53/root/source1	2
12606	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input/macro_container_node_add_multiple_ints_1_add_ints_macro___input___id_1_0			2018-08-22 10:37:21.341873	2018-08-22 10:37:21.404908	{"sample_index": [1], "errors": [], "sample_id": ["id_1_0"]}	35	2034	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input	2
12601	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value/macro_top_level_node_add_ints_macro_container___input_value___id_1_0			2018-08-22 10:37:20.687133	2018-08-22 10:37:20.748343	{"sample_index": [1], "errors": [], "sample_id": ["id_1_0"]}	35	2028	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value	2
13062	example_2018-09-03T11-07-53/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 09:07:55.473243	2018-09-03 09:07:55.601804	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	49	2158	example_2018-09-03T11-07-53/root/const_addint_right_hand_0	2
12604	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value/macro_top_level_node_add_ints_macro_container___input_value___id_1_3			2018-08-22 10:37:21.05098	2018-08-22 10:37:21.1134	{"sample_index": [4], "errors": [], "sample_id": ["id_1_3"]}	35	2028	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value	2
12603	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value/macro_top_level_node_add_ints_macro_container___input_value___id_1_2			2018-08-22 10:37:20.929566	2018-08-22 10:37:20.989153	{"sample_index": [3], "errors": [], "sample_id": ["id_1_2"]}	35	2028	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value	2
12602	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value/macro_top_level_node_add_ints_macro_container___input_value___id_1_1			2018-08-22 10:37:20.80963	2018-08-22 10:37:20.867486	{"sample_index": [2], "errors": [], "sample_id": ["id_1_1"]}	35	2028	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value	2
12605	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input/macro_container_node_add_multiple_ints_1_add_ints_macro___input___id_0			2018-08-22 10:37:21.216936	2018-08-22 10:37:21.276359	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	35	2034	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input	2
12607	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input/macro_container_node_add_multiple_ints_1_add_ints_macro___input___id_1_1			2018-08-22 10:37:21.467335	2018-08-22 10:37:21.530149	{"sample_index": [2], "errors": [], "sample_id": ["id_1_1"]}	35	2034	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input	2
12608	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input/macro_container_node_add_multiple_ints_1_add_ints_macro___input___id_1_2			2018-08-22 10:37:21.596396	2018-08-22 10:37:21.65941	{"sample_index": [3], "errors": [], "sample_id": ["id_1_2"]}	35	2034	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input	2
12443	failing_network_2018-08-22T11-29-01/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_0			2018-08-22 09:29:02.74047	2018-08-22 09:29:02.80548	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	32	1982	failing_network_2018-08-22T11-29-01/root/const_step_2_fail_1_0	2
12442	failing_network_2018-08-22T11-29-01/root/source_1/failing_network___source_1___sample_1			2018-08-22 09:29:02.641793	2018-08-22 09:29:42.725369	{"sample_index": [0], "errors": [], "sample_id": ["sample_1"]}	32	1973	failing_network_2018-08-22T11-29-01/root/source_1	2
12440	failing_network_2018-08-22T11-29-01/root/source_2/failing_network___source_2___sample_1			2018-08-22 09:29:02.448664	2018-08-22 09:29:42.62786	{"sample_index": [0], "errors": [], "sample_id": ["sample_1"]}	32	1978	failing_network_2018-08-22T11-29-01/root/source_2	2
12445	failing_network_2018-08-22T11-29-01/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_2			2018-08-22 09:29:03.031617	2018-08-22 09:29:03.097351	{"sample_index": [2], "errors": [], "sample_id": ["id_2"]}	32	1982	failing_network_2018-08-22T11-29-01/root/const_step_2_fail_1_0	2
12452	failing_network_2018-08-22T11-29-01/root/step_1/failing_network___step_1___sample_1_1			2018-08-22 09:29:43.156936	2018-08-22 09:30:38.356589	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	32	1969	failing_network_2018-08-22T11-29-01/root/step_1	3
12449	failing_network_2018-08-22T11-29-01/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_2			2018-08-22 09:29:03.647692	2018-08-22 09:29:03.713236	{"sample_index": [2], "errors": [], "sample_id": ["id_2"]}	32	1983	failing_network_2018-08-22T11-29-01/root/const_step_1_fail_2_0	2
12444	failing_network_2018-08-22T11-29-01/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_1			2018-08-22 09:29:02.889358	2018-08-22 09:29:02.952488	{"sample_index": [1], "errors": [], "sample_id": ["id_1"]}	32	1982	failing_network_2018-08-22T11-29-01/root/const_step_2_fail_1_0	2
12450	failing_network_2018-08-22T11-29-01/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_3			2018-08-22 09:29:03.794331	2018-08-22 09:29:03.860747	{"sample_index": [3], "errors": [], "sample_id": ["id_3"]}	32	1983	failing_network_2018-08-22T11-29-01/root/const_step_1_fail_2_0	2
12447	failing_network_2018-08-22T11-29-01/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_0			2018-08-22 09:29:03.359196	2018-08-22 09:29:03.422252	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	32	1983	failing_network_2018-08-22T11-29-01/root/const_step_1_fail_2_0	2
12446	failing_network_2018-08-22T11-29-01/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_3			2018-08-22 09:29:03.208493	2018-08-22 09:29:03.269349	{"sample_index": [3], "errors": [], "sample_id": ["id_3"]}	32	1982	failing_network_2018-08-22T11-29-01/root/const_step_2_fail_1_0	2
12448	failing_network_2018-08-22T11-29-01/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_1			2018-08-22 09:29:03.501529	2018-08-22 09:29:03.567112	{"sample_index": [1], "errors": [], "sample_id": ["id_1"]}	32	1983	failing_network_2018-08-22T11-29-01/root/const_step_1_fail_2_0	2
12441	failing_network_2018-08-22T11-29-01/root/source_3/failing_network___source_3___sample_1			2018-08-22 09:29:02.54855	2018-08-22 09:29:39.675335	{"sample_index": [0], "errors": [], "sample_id": ["sample_1"]}	32	1979	failing_network_2018-08-22T11-29-01/root/source_3	2
12451	failing_network_2018-08-22T11-29-01/root/step_1/failing_network___step_1___sample_1_0			2018-08-22 09:29:43.078508	2018-08-22 09:30:40.290742	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	32	1969	failing_network_2018-08-22T11-29-01/root/step_1	2
12454	failing_network_2018-08-22T11-29-01/root/step_1/failing_network___step_1___sample_1_3			2018-08-22 09:29:43.291741	2018-08-22 09:30:39.041101	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	32	1969	failing_network_2018-08-22T11-29-01/root/step_1	3
12455	failing_network_2018-08-22T11-29-01/root/step_2/failing_network___step_2___sample_1_0			2018-08-22 09:29:43.389063	2018-08-22 09:30:40.446723	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	32	1971	failing_network_2018-08-22T11-29-01/root/step_2	2
12456	failing_network_2018-08-22T11-29-01/root/step_2/failing_network___step_2___sample_1_1			2018-08-22 09:29:43.458974	2018-08-22 09:30:45.089062	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	32	1971	failing_network_2018-08-22T11-29-01/root/step_2	2
12453	failing_network_2018-08-22T11-29-01/root/step_1/failing_network___step_1___sample_1_2			2018-08-22 09:29:43.225224	2018-08-22 09:30:49.733584	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	32	1969	failing_network_2018-08-22T11-29-01/root/step_1	2
13214	example_2018-09-03T12-47-05/root/source1/example___source1___s1			2018-09-03 10:47:06.373272	2018-09-03 10:47:06.478271	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	58	2206	example_2018-09-03T12-47-05/root/source1	2
12462	failing_network_2018-08-22T11-29-01/root/sink_1/failing_network___sink_1___sample_1_3___0			2018-08-22 09:29:43.871273	2018-08-22 09:30:39.109523	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	32	1977	failing_network_2018-08-22T11-29-01/root/sink_1	4
12469	failing_network_2018-08-22T11-29-01/root/sink_2/failing_network___sink_2___sample_1_2___0			2018-08-22 09:29:44.426598	2018-08-22 09:30:39.311215	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	32	1974	failing_network_2018-08-22T11-29-01/root/sink_2	4
12466	failing_network_2018-08-22T11-29-01/root/step_3/failing_network___step_3___sample_1_3			2018-08-22 09:29:44.211466	2018-08-22 09:30:45.728211	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	32	1970	failing_network_2018-08-22T11-29-01/root/step_3	4
12460	failing_network_2018-08-22T11-29-01/root/sink_1/failing_network___sink_1___sample_1_1___0			2018-08-22 09:29:43.737185	2018-08-22 09:30:38.430424	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	32	1977	failing_network_2018-08-22T11-29-01/root/sink_1	4
13063	example_2018-09-03T11-07-53/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 09:07:55.722622	2018-09-03 09:07:55.841884	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	49	2158	example_2018-09-03T11-07-53/root/const_addint_right_hand_0	2
12461	failing_network_2018-08-22T11-29-01/root/sink_1/failing_network___sink_1___sample_1_2___0			2018-08-22 09:29:43.803523	2018-08-22 09:31:37.752332	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	32	1977	failing_network_2018-08-22T11-29-01/root/sink_1	2
12468	failing_network_2018-08-22T11-29-01/root/sink_2/failing_network___sink_2___sample_1_1___0			2018-08-22 09:29:44.359212	2018-08-22 09:31:37.905948	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	32	1974	failing_network_2018-08-22T11-29-01/root/sink_2	2
12457	failing_network_2018-08-22T11-29-01/root/step_2/failing_network___step_2___sample_1_2			2018-08-22 09:29:43.527774	2018-08-22 09:30:39.244145	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	32	1971	failing_network_2018-08-22T11-29-01/root/step_2	3
12463	failing_network_2018-08-22T11-29-01/root/step_3/failing_network___step_3___sample_1_0			2018-08-22 09:29:44.018713	2018-08-22 09:31:36.777985	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	32	1970	failing_network_2018-08-22T11-29-01/root/step_3	2
12458	failing_network_2018-08-22T11-29-01/root/step_2/failing_network___step_2___sample_1_3			2018-08-22 09:29:43.594312	2018-08-22 09:30:45.666332	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	32	1971	failing_network_2018-08-22T11-29-01/root/step_2	3
12465	failing_network_2018-08-22T11-29-01/root/step_3/failing_network___step_3___sample_1_2			2018-08-22 09:29:44.146676	2018-08-22 09:30:49.795303	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	32	1970	failing_network_2018-08-22T11-29-01/root/step_3	4
12459	failing_network_2018-08-22T11-29-01/root/sink_1/failing_network___sink_1___sample_1_0___0			2018-08-22 09:29:43.671435	2018-08-22 09:31:43.72188	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	32	1977	failing_network_2018-08-22T11-29-01/root/sink_1	2
12467	failing_network_2018-08-22T11-29-01/root/sink_2/failing_network___sink_2___sample_1_0___0			2018-08-22 09:29:44.29007	2018-08-22 09:31:39.832384	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	32	1974	failing_network_2018-08-22T11-29-01/root/sink_2	2
12464	failing_network_2018-08-22T11-29-01/root/step_3/failing_network___step_3___sample_1_1			2018-08-22 09:29:44.081544	2018-08-22 09:30:45.165313	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	32	1970	failing_network_2018-08-22T11-29-01/root/step_3	4
12477	failing_network_2018-08-22T11-29-01/root/range/failing_network___range___sample_1_2			2018-08-22 09:29:44.974613	2018-08-22 09:30:50.065562	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	32	1976	failing_network_2018-08-22T11-29-01/root/range	4
12471	failing_network_2018-08-22T11-29-01/root/sink_3/failing_network___sink_3___sample_1_0___0			2018-08-22 09:29:44.569012	2018-08-22 09:32:09.543498	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	32	1975	failing_network_2018-08-22T11-29-01/root/sink_3	4
12478	failing_network_2018-08-22T11-29-01/root/range/failing_network___range___sample_1_3			2018-08-22 09:29:45.039824	2018-08-22 09:30:45.981993	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	32	1976	failing_network_2018-08-22T11-29-01/root/range	4
12470	failing_network_2018-08-22T11-29-01/root/sink_2/failing_network___sink_2___sample_1_3___0			2018-08-22 09:29:44.492207	2018-08-22 09:30:46.109901	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	32	1974	failing_network_2018-08-22T11-29-01/root/sink_2	4
12472	failing_network_2018-08-22T11-29-01/root/sink_3/failing_network___sink_3___sample_1_1___0			2018-08-22 09:29:44.631949	2018-08-22 09:30:45.301315	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	32	1975	failing_network_2018-08-22T11-29-01/root/sink_3	4
12609	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input/macro_container_node_add_multiple_ints_1_add_ints_macro___input___id_1_3			2018-08-22 10:37:21.720366	2018-08-22 10:37:21.789635	{"sample_index": [4], "errors": [], "sample_id": ["id_1_3"]}	35	2034	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input	2
12473	failing_network_2018-08-22T11-29-01/root/sink_3/failing_network___sink_3___sample_1_2___0			2018-08-22 09:29:44.696407	2018-08-22 09:30:49.9361	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	32	1975	failing_network_2018-08-22T11-29-01/root/sink_3	4
12475	failing_network_2018-08-22T11-29-01/root/range/failing_network___range___sample_1_0			2018-08-22 09:29:44.844518	2018-08-22 09:32:09.673982	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	32	1976	failing_network_2018-08-22T11-29-01/root/range	4
12476	failing_network_2018-08-22T11-29-01/root/range/failing_network___range___sample_1_1			2018-08-22 09:29:44.90909	2018-08-22 09:30:45.434733	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	32	1976	failing_network_2018-08-22T11-29-01/root/range	4
12474	failing_network_2018-08-22T11-29-01/root/sink_3/failing_network___sink_3___sample_1_3___0			2018-08-22 09:29:44.762789	2018-08-22 09:30:45.854768	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	32	1975	failing_network_2018-08-22T11-29-01/root/sink_3	4
12613	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1/macro_container_node_add_multiple_ints_1_add_ints_macro___addint1___id_1_0			2018-08-22 10:37:22.260801	2018-08-22 10:38:37.940907	{"sample_index": [1], "errors": [], "sample_id": ["id_1_0"]}	35	2038	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1	2
13215	example_2018-09-03T12-47-05/root/source1/example___source1___s2			2018-09-03 10:47:06.589412	2018-09-03 10:47:06.701326	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	58	2206	example_2018-09-03T12-47-05/root/source1	2
12487	failing_network_2018-08-22T12-10-16/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_1			2018-08-22 10:10:18.681107	2018-08-22 10:10:18.750448	{"sample_index": [1], "errors": [], "sample_id": ["id_1"]}	33	1999	failing_network_2018-08-22T12-10-16/root/const_step_1_fail_2_0	2
12480	failing_network_2018-08-22T12-10-16/root/source_3/failing_network___source_3___sample_1			2018-08-22 10:10:17.781333	2018-08-22 10:10:55.991851	{"sample_index": [0], "errors": [], "sample_id": ["sample_1"]}	33	1995	failing_network_2018-08-22T12-10-16/root/source_3	2
12482	failing_network_2018-08-22T12-10-16/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_0			2018-08-22 10:10:17.958763	2018-08-22 10:10:18.021896	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	33	1998	failing_network_2018-08-22T12-10-16/root/const_step_2_fail_1_0	2
12481	failing_network_2018-08-22T12-10-16/root/source_1/failing_network___source_1___sample_1			2018-08-22 10:10:17.868798	2018-08-22 10:11:28.105985	{"sample_index": [0], "errors": [], "sample_id": ["sample_1"]}	33	1989	failing_network_2018-08-22T12-10-16/root/source_1	2
12485	failing_network_2018-08-22T12-10-16/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_3			2018-08-22 10:10:18.390712	2018-08-22 10:10:18.452173	{"sample_index": [3], "errors": [], "sample_id": ["id_3"]}	33	1998	failing_network_2018-08-22T12-10-16/root/const_step_2_fail_1_0	2
12484	failing_network_2018-08-22T12-10-16/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_2			2018-08-22 10:10:18.251293	2018-08-22 10:10:18.313796	{"sample_index": [2], "errors": [], "sample_id": ["id_2"]}	33	1998	failing_network_2018-08-22T12-10-16/root/const_step_2_fail_1_0	2
12483	failing_network_2018-08-22T12-10-16/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_1			2018-08-22 10:10:18.104125	2018-08-22 10:10:18.167536	{"sample_index": [1], "errors": [], "sample_id": ["id_1"]}	33	1998	failing_network_2018-08-22T12-10-16/root/const_step_2_fail_1_0	2
12486	failing_network_2018-08-22T12-10-16/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_0			2018-08-22 10:10:18.538074	2018-08-22 10:10:18.599232	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	33	1999	failing_network_2018-08-22T12-10-16/root/const_step_1_fail_2_0	2
12488	failing_network_2018-08-22T12-10-16/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_2			2018-08-22 10:10:18.830009	2018-08-22 10:10:18.892837	{"sample_index": [2], "errors": [], "sample_id": ["id_2"]}	33	1999	failing_network_2018-08-22T12-10-16/root/const_step_1_fail_2_0	2
12489	failing_network_2018-08-22T12-10-16/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_3			2018-08-22 10:10:18.967242	2018-08-22 10:10:19.029589	{"sample_index": [3], "errors": [], "sample_id": ["id_3"]}	33	1999	failing_network_2018-08-22T12-10-16/root/const_step_1_fail_2_0	2
12479	failing_network_2018-08-22T12-10-16/root/source_2/failing_network___source_2___sample_1			2018-08-22 10:10:17.688253	2018-08-22 10:10:51.72773	{"sample_index": [0], "errors": [], "sample_id": ["sample_1"]}	33	1994	failing_network_2018-08-22T12-10-16/root/source_2	2
12490	failing_network_2018-08-22T12-10-16/root/step_1/failing_network___step_1___sample_1_0			2018-08-22 10:11:28.270016	2018-08-22 10:11:46.1078	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1985	failing_network_2018-08-22T12-10-16/root/step_1	2
12492	failing_network_2018-08-22T12-10-16/root/step_1/failing_network___step_1___sample_1_2			2018-08-22 10:11:28.404166	2018-08-22 10:12:07.653948	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	33	1985	failing_network_2018-08-22T12-10-16/root/step_1	2
12491	failing_network_2018-08-22T12-10-16/root/step_1/failing_network___step_1___sample_1_1			2018-08-22 10:11:28.336361	2018-08-22 10:11:47.515033	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	33	1985	failing_network_2018-08-22T12-10-16/root/step_1	3
12505	failing_network_2018-08-22T12-10-16/root/sink_1/failing_network___sink_1___sample_1_3___0			2018-08-22 10:11:29.381693	2018-08-22 10:11:45.56058	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	33	1993	failing_network_2018-08-22T12-10-16/root/sink_1	4
12500	failing_network_2018-08-22T12-10-16/root/step_3/failing_network___step_3___sample_1_2			2018-08-22 10:11:28.999122	2018-08-22 10:12:07.719179	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	33	1986	failing_network_2018-08-22T12-10-16/root/step_3	4
12511	failing_network_2018-08-22T12-10-16/root/sink_3/failing_network___sink_3___sample_1_1___0			2018-08-22 10:11:29.813601	2018-08-22 10:11:47.707592	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	33	1991	failing_network_2018-08-22T12-10-16/root/sink_3	4
12496	failing_network_2018-08-22T12-10-16/root/step_2/failing_network___step_2___sample_1_2			2018-08-22 10:11:28.704078	2018-08-22 10:11:46.278209	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	33	1987	failing_network_2018-08-22T12-10-16/root/step_2	3
12508	failing_network_2018-08-22T12-10-16/root/sink_2/failing_network___sink_2___sample_1_2___0			2018-08-22 10:11:29.601255	2018-08-22 10:11:46.343343	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	33	1990	failing_network_2018-08-22T12-10-16/root/sink_2	4
12494	failing_network_2018-08-22T12-10-16/root/step_2/failing_network___step_2___sample_1_0			2018-08-22 10:11:28.570016	2018-08-22 10:11:48.149385	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1987	failing_network_2018-08-22T12-10-16/root/step_2	2
12501	failing_network_2018-08-22T12-10-16/root/step_3/failing_network___step_3___sample_1_3			2018-08-22 10:11:29.112079	2018-08-22 10:11:46.588531	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	33	1986	failing_network_2018-08-22T12-10-16/root/step_3	4
12512	failing_network_2018-08-22T12-10-16/root/sink_3/failing_network___sink_3___sample_1_2___0			2018-08-22 10:11:29.878252	2018-08-22 10:12:07.848759	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	33	1991	failing_network_2018-08-22T12-10-16/root/sink_3	4
12502	failing_network_2018-08-22T12-10-16/root/sink_1/failing_network___sink_1___sample_1_0___0			2018-08-22 10:11:29.189667	2018-08-22 10:12:49.290985	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1993	failing_network_2018-08-22T12-10-16/root/sink_1	2
12513	failing_network_2018-08-22T12-10-16/root/sink_3/failing_network___sink_3___sample_1_3___0			2018-08-22 10:11:29.943606	2018-08-22 10:11:46.740494	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	33	1991	failing_network_2018-08-22T12-10-16/root/sink_3	4
12504	failing_network_2018-08-22T12-10-16/root/sink_1/failing_network___sink_1___sample_1_2___0			2018-08-22 10:11:29.318621	2018-08-22 10:12:49.378477	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	33	1993	failing_network_2018-08-22T12-10-16/root/sink_1	2
12506	failing_network_2018-08-22T12-10-16/root/sink_2/failing_network___sink_2___sample_1_0___0			2018-08-22 10:11:29.476667	2018-08-22 10:12:58.932754	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1990	failing_network_2018-08-22T12-10-16/root/sink_2	2
12507	failing_network_2018-08-22T12-10-16/root/sink_2/failing_network___sink_2___sample_1_1___0			2018-08-22 10:11:29.539343	2018-08-22 10:13:06.991856	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	33	1990	failing_network_2018-08-22T12-10-16/root/sink_2	2
12495	failing_network_2018-08-22T12-10-16/root/step_2/failing_network___step_2___sample_1_1			2018-08-22 10:11:28.639169	2018-08-22 10:11:46.177902	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	33	1987	failing_network_2018-08-22T12-10-16/root/step_2	2
12510	failing_network_2018-08-22T12-10-16/root/sink_3/failing_network___sink_3___sample_1_0___0			2018-08-22 10:11:29.751942	2018-08-22 10:13:52.439764	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1991	failing_network_2018-08-22T12-10-16/root/sink_3	2
12497	failing_network_2018-08-22T12-10-16/root/step_2/failing_network___step_2___sample_1_3			2018-08-22 10:11:28.787437	2018-08-22 10:11:46.52142	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	33	1987	failing_network_2018-08-22T12-10-16/root/step_2	3
12499	failing_network_2018-08-22T12-10-16/root/step_3/failing_network___step_3___sample_1_1			2018-08-22 10:11:28.937081	2018-08-22 10:11:47.578748	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	33	1986	failing_network_2018-08-22T12-10-16/root/step_3	4
13064	example_2018-09-03T11-07-53/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 09:07:55.960074	2018-09-03 09:07:56.094076	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	49	2158	example_2018-09-03T11-07-53/root/const_addint_right_hand_0	2
12503	failing_network_2018-08-22T12-10-16/root/sink_1/failing_network___sink_1___sample_1_1___0			2018-08-22 10:11:29.254529	2018-08-22 10:11:48.009042	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	33	1993	failing_network_2018-08-22T12-10-16/root/sink_1	4
12498	failing_network_2018-08-22T12-10-16/root/step_3/failing_network___step_3___sample_1_0			2018-08-22 10:11:28.872742	2018-08-22 10:12:45.946122	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1986	failing_network_2018-08-22T12-10-16/root/step_3	2
12509	failing_network_2018-08-22T12-10-16/root/sink_2/failing_network___sink_2___sample_1_3___0			2018-08-22 10:11:29.668987	2018-08-22 10:11:46.992353	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	33	1990	failing_network_2018-08-22T12-10-16/root/sink_2	4
12493	failing_network_2018-08-22T12-10-16/root/step_1/failing_network___step_1___sample_1_3			2018-08-22 10:11:28.474396	2018-08-22 10:11:45.489375	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	33	1985	failing_network_2018-08-22T12-10-16/root/step_1	3
12516	failing_network_2018-08-22T12-10-16/root/range/failing_network___range___sample_1_2			2018-08-22 10:11:30.155083	2018-08-22 10:12:07.972787	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	33	1992	failing_network_2018-08-22T12-10-16/root/range	4
12514	failing_network_2018-08-22T12-10-16/root/range/failing_network___range___sample_1_0			2018-08-22 10:11:30.026441	2018-08-22 10:13:45.106395	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1992	failing_network_2018-08-22T12-10-16/root/range	2
12517	failing_network_2018-08-22T12-10-16/root/range/failing_network___range___sample_1_3			2018-08-22 10:11:30.219508	2018-08-22 10:11:46.863798	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	33	1992	failing_network_2018-08-22T12-10-16/root/range	4
12515	failing_network_2018-08-22T12-10-16/root/range/failing_network___range___sample_1_1			2018-08-22 10:11:30.090682	2018-08-22 10:11:47.844272	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	33	1992	failing_network_2018-08-22T12-10-16/root/range	4
12610	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/const_addint2_right_hand_0/macro_container_node_add_multiple_ints_1_add_ints_macro___const_addint2_right_hand_0___id_0			2018-08-22 10:37:21.891562	2018-08-22 10:37:21.952992	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	35	2041	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/const_addint2_right_hand_0	2
13216	example_2018-09-03T12-47-05/root/source1/example___source1___s3			2018-09-03 10:47:06.811098	2018-09-03 10:47:06.913385	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	58	2206	example_2018-09-03T12-47-05/root/source1	2
12519	failing_network_2018-08-22T12-10-16/root/sink_4/failing_network___sink_4___sample_1_0___1			2018-08-22 10:13:53.398075	2018-08-22 10:14:27.398936	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1996	failing_network_2018-08-22T12-10-16/root/sink_4	4
12520	failing_network_2018-08-22T12-10-16/root/sink_4/failing_network___sink_4___sample_1_0___2			2018-08-22 10:13:53.459368	2018-08-22 10:14:27.528111	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1996	failing_network_2018-08-22T12-10-16/root/sink_4	4
12521	failing_network_2018-08-22T12-10-16/root/sink_4/failing_network___sink_4___sample_1_0___3			2018-08-22 10:13:53.519326	2018-08-22 10:14:27.655348	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1996	failing_network_2018-08-22T12-10-16/root/sink_4	4
12522	failing_network_2018-08-22T12-10-16/root/sink_4/failing_network___sink_4___sample_1_0___4			2018-08-22 10:13:53.577465	2018-08-22 10:14:27.781901	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1996	failing_network_2018-08-22T12-10-16/root/sink_4	4
12523	failing_network_2018-08-22T12-10-16/root/sink_4/failing_network___sink_4___sample_1_0___5			2018-08-22 10:13:53.641319	2018-08-22 10:14:27.911774	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1996	failing_network_2018-08-22T12-10-16/root/sink_4	4
12524	failing_network_2018-08-22T12-10-16/root/sink_4/failing_network___sink_4___sample_1_0___6			2018-08-22 10:13:53.701301	2018-08-22 10:14:28.039921	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1996	failing_network_2018-08-22T12-10-16/root/sink_4	4
12525	failing_network_2018-08-22T12-10-16/root/sink_4/failing_network___sink_4___sample_1_0___7			2018-08-22 10:13:53.761684	2018-08-22 10:14:28.16896	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1996	failing_network_2018-08-22T12-10-16/root/sink_4	4
12518	failing_network_2018-08-22T12-10-16/root/sink_4/failing_network___sink_4___sample_1_0___0			2018-08-22 10:13:53.330798	2018-08-22 10:14:27.268593	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1996	failing_network_2018-08-22T12-10-16/root/sink_4	4
12528	failing_network_2018-08-22T12-10-16/root/sink_4/failing_network___sink_4___sample_1_2___0			2018-08-22 10:13:53.944453	2018-08-22 10:13:55.06281	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	33	1996	failing_network_2018-08-22T12-10-16/root/sink_4	4
12533	failing_network_2018-08-22T12-10-16/root/sum/failing_network___sum___sample_1_3			2018-08-22 10:13:54.298117	2018-08-22 10:13:55.685346	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	33	1988	failing_network_2018-08-22T12-10-16/root/sum	4
12529	failing_network_2018-08-22T12-10-16/root/sink_4/failing_network___sink_4___sample_1_3___0			2018-08-22 10:13:54.009782	2018-08-22 10:13:55.198422	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	33	1996	failing_network_2018-08-22T12-10-16/root/sink_4	4
12534	failing_network_2018-08-22T12-10-16/root/sink_5/failing_network___sink_5___sample_1_0___0			2018-08-22 10:13:54.384557	2018-08-22 10:14:27.010258	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1997	failing_network_2018-08-22T12-10-16/root/sink_5	4
12531	failing_network_2018-08-22T12-10-16/root/sum/failing_network___sum___sample_1_1			2018-08-22 10:13:54.16589	2018-08-22 10:13:55.383334	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	33	1988	failing_network_2018-08-22T12-10-16/root/sum	4
12532	failing_network_2018-08-22T12-10-16/root/sum/failing_network___sum___sample_1_2			2018-08-22 10:13:54.231099	2018-08-22 10:13:55.540591	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	33	1988	failing_network_2018-08-22T12-10-16/root/sum	4
12526	failing_network_2018-08-22T12-10-16/root/sink_4/failing_network___sink_4___sample_1_0___8			2018-08-22 10:13:53.821164	2018-08-22 10:14:27.138857	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1996	failing_network_2018-08-22T12-10-16/root/sink_4	4
12535	failing_network_2018-08-22T12-10-16/root/sink_5/failing_network___sink_5___sample_1_1___0			2018-08-22 10:13:54.447438	2018-08-22 10:13:55.859187	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	33	1997	failing_network_2018-08-22T12-10-16/root/sink_5	4
12536	failing_network_2018-08-22T12-10-16/root/sink_5/failing_network___sink_5___sample_1_2___0			2018-08-22 10:13:54.512051	2018-08-22 10:13:56.006032	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	33	1997	failing_network_2018-08-22T12-10-16/root/sink_5	4
12537	failing_network_2018-08-22T12-10-16/root/sink_5/failing_network___sink_5___sample_1_3___0			2018-08-22 10:13:54.575515	2018-08-22 10:13:56.149359	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	33	1997	failing_network_2018-08-22T12-10-16/root/sink_5	4
12527	failing_network_2018-08-22T12-10-16/root/sink_4/failing_network___sink_4___sample_1_1___0			2018-08-22 10:13:53.88222	2018-08-22 10:13:54.925034	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	33	1996	failing_network_2018-08-22T12-10-16/root/sink_4	4
12530	failing_network_2018-08-22T12-10-16/root/sum/failing_network___sum___sample_1_0			2018-08-22 10:13:54.098878	2018-08-22 10:14:26.868963	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	33	1988	failing_network_2018-08-22T12-10-16/root/sum	4
12538	failing_macro_top_level_2018-08-22T12-15-04/root/source_b/failing_macro_top_level___source_b___sample_1			2018-08-22 10:15:05.043942	2018-08-22 10:15:43.828033	{"sample_index": [0], "errors": [], "sample_id": ["sample_1"]}	34	2001	failing_macro_top_level_2018-08-22T12-15-04/root/source_b	2
12542	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1/failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_0			2018-08-22 10:16:04.618445	2018-08-22 10:16:04.68511	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	34	2020	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1	2
12539	failing_macro_top_level_2018-08-22T12-15-04/root/source_c/failing_macro_top_level___source_c___sample_1			2018-08-22 10:15:05.155562	2018-08-22 10:16:04.11972	{"sample_index": [0], "errors": [], "sample_id": ["sample_1"]}	34	2002	failing_macro_top_level_2018-08-22T12-15-04/root/source_c	2
12541	failing_macro_top_level_2018-08-22T12-15-04/root/const_add_right_hand_0/failing_macro_top_level___const_add_right_hand_0___id_0			2018-08-22 10:15:05.326158	2018-08-22 10:15:05.384216	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	34	2007	failing_macro_top_level_2018-08-22T12-15-04/root/const_add_right_hand_0	2
12540	failing_macro_top_level_2018-08-22T12-15-04/root/source_a/failing_macro_top_level___source_a___sample_1			2018-08-22 10:15:05.24238	2018-08-22 10:16:03.097022	{"sample_index": [0], "errors": [], "sample_id": ["sample_1"]}	34	2003	failing_macro_top_level_2018-08-22T12-15-04/root/source_a	2
12543	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1/failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_1			2018-08-22 10:16:04.751135	2018-08-22 10:16:04.811866	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	34	2020	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1	2
13065	example_2018-09-03T11-07-53/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 09:07:56.226479	2018-09-03 09:07:56.369409	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	49	2158	example_2018-09-03T11-07-53/root/const_addint_right_hand_0	2
12545	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1/failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_3			2018-08-22 10:16:04.997796	2018-08-22 10:16:05.06049	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	34	2020	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1	2
12544	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1/failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_2			2018-08-22 10:16:04.875012	2018-08-22 10:16:04.937406	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	34	2020	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1	2
12546	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_2/failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_0			2018-08-22 10:16:05.137845	2018-08-22 10:16:05.201292	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	34	2017	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_2	2
12547	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_2/failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_1			2018-08-22 10:16:05.328663	2018-08-22 10:16:05.404501	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	34	2017	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_2	2
13066	example_2018-09-03T11-07-53/root/addint/example___addint___s1			2018-09-03 09:07:56.502567	2018-09-03 09:08:02.559043	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	49	2157	example_2018-09-03T11-07-53/root/addint	2
12657	macro_node_2_2018-08-22T12-40-46/root/addint/macro_node_2___addint___id_1__id_1			2018-08-22 10:40:48.782496	2018-08-22 10:41:23.238127	{"sample_index": [1, 1], "errors": [], "sample_id": ["id_1", "id_1"]}	36	2048	macro_node_2_2018-08-22T12-40-46/root/addint	2
12548	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_2/failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_2			2018-08-22 10:16:05.503303	2018-08-22 10:16:05.561465	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	34	2017	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_2	2
12552	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_3/failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_2			2018-08-22 10:16:06.003337	2018-08-22 10:16:06.061066	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	34	2018	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_3	2
12549	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_2/failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_3			2018-08-22 10:16:05.622147	2018-08-22 10:16:05.682127	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	34	2017	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_2	2
12550	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_3/failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_0			2018-08-22 10:16:05.755621	2018-08-22 10:16:05.818228	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	34	2018	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_3	2
12551	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_3/failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_1			2018-08-22 10:16:05.87974	2018-08-22 10:16:05.941987	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	34	2018	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_3	2
12554	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_2_fail_1_0/failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_0			2018-08-22 10:16:06.313866	2018-08-22 10:16:06.381423	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	34	2021	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_2_fail_1_0	2
12553	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_3/failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_3			2018-08-22 10:16:06.122011	2018-08-22 10:16:06.188104	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	34	2018	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_3	2
13067	example_2018-09-03T11-07-53/root/addint/example___addint___s2			2018-09-03 09:07:56.632084	2018-09-03 09:08:08.207565	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	49	2157	example_2018-09-03T11-07-53/root/addint	2
12658	macro_node_2_2018-08-22T12-40-46/root/addint/macro_node_2___addint___id_1__id_2			2018-08-22 10:40:48.852305	2018-08-22 10:41:41.106479	{"sample_index": [1, 2], "errors": [], "sample_id": ["id_1", "id_2"]}	36	2048	macro_node_2_2018-08-22T12-40-46/root/addint	2
12556	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_2_fail_1_0/failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_2			2018-08-22 10:16:06.598791	2018-08-22 10:16:06.658305	{"sample_index": [2], "errors": [], "sample_id": ["id_2"]}	34	2021	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_2_fail_1_0	2
12555	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_2_fail_1_0/failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_1			2018-08-22 10:16:06.456145	2018-08-22 10:16:06.519725	{"sample_index": [1], "errors": [], "sample_id": ["id_1"]}	34	2021	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_2_fail_1_0	2
12559	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_1_fail_2_0/failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_1			2018-08-22 10:16:07.037318	2018-08-22 10:16:07.098828	{"sample_index": [1], "errors": [], "sample_id": ["id_1"]}	34	2022	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_1_fail_2_0	2
12557	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_2_fail_1_0/failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_3			2018-08-22 10:16:06.731261	2018-08-22 10:16:06.791859	{"sample_index": [3], "errors": [], "sample_id": ["id_3"]}	34	2021	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_2_fail_1_0	2
12558	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_1_fail_2_0/failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_0			2018-08-22 10:16:06.892031	2018-08-22 10:16:06.959525	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	34	2022	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_1_fail_2_0	2
12560	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_1_fail_2_0/failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_2			2018-08-22 10:16:07.176959	2018-08-22 10:16:07.238716	{"sample_index": [2], "errors": [], "sample_id": ["id_2"]}	34	2022	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_1_fail_2_0	2
12561	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_1_fail_2_0/failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_3			2018-08-22 10:16:07.31495	2018-08-22 10:16:07.374069	{"sample_index": [3], "errors": [], "sample_id": ["id_3"]}	34	2022	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_1_fail_2_0	2
13068	example_2018-09-03T11-07-53/root/addint/example___addint___s3			2018-09-03 09:07:56.750521	2018-09-03 09:08:14.765203	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	49	2157	example_2018-09-03T11-07-53/root/addint	2
12576	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range/failing_macro_top_level_failing_macro_failing_network___range___sample_1_2			2018-08-22 10:16:08.451522	2018-08-22 10:16:46.55606	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	34	2015	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range	4
12562	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_0			2018-08-22 10:16:07.463146	2018-08-22 10:16:44.988223	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	34	2008	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1	2
12568	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_2			2018-08-22 10:16:07.887272	2018-08-22 10:16:46.353796	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	34	2010	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2	3
12563	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_1			2018-08-22 10:16:07.531622	2018-08-22 10:16:40.270287	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	34	2008	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1	3
12565	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_3			2018-08-22 10:16:07.662754	2018-08-22 10:16:48.393522	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	34	2008	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1	3
12577	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range/failing_macro_top_level_failing_macro_failing_network___range___sample_1_3			2018-08-22 10:16:08.534531	2018-08-22 10:16:48.596062	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	34	2015	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range	4
12564	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_2			2018-08-22 10:16:07.597267	2018-08-22 10:16:39.304787	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	34	2008	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1	2
12571	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_1			2018-08-22 10:16:08.111267	2018-08-22 10:16:40.340821	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	34	2009	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3	4
12575	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range/failing_macro_top_level_failing_macro_failing_network___range___sample_1_1			2018-08-22 10:16:08.384876	2018-08-22 10:16:40.863491	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	34	2015	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range	4
12573	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_3			2018-08-22 10:16:08.23881	2018-08-22 10:16:48.463758	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	34	2009	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3	4
12570	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_0			2018-08-22 10:16:08.045812	2018-08-22 10:17:39.527352	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	34	2009	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3	2
12567	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_1			2018-08-22 10:16:07.820919	2018-08-22 10:16:39.378954	{"sample_index": [1], "errors": [], "sample_id": ["sample_1_1"]}	34	2010	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2	2
12572	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_2			2018-08-22 10:16:08.174463	2018-08-22 10:16:46.418733	{"sample_index": [2], "errors": [], "sample_id": ["sample_1_2"]}	34	2009	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3	4
12574	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range/failing_macro_top_level_failing_macro_failing_network___range___sample_1_0			2018-08-22 10:16:08.319545	2018-08-22 10:18:37.437928	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	34	2015	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range	2
12611	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/const_addint1_right_hand_0/macro_container_node_add_multiple_ints_1_add_ints_macro___const_addint1_right_hand_0___id_0			2018-08-22 10:37:22.041888	2018-08-22 10:37:22.101611	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	35	2040	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/const_addint1_right_hand_0	2
12566	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_0			2018-08-22 10:16:07.752951	2018-08-22 10:16:39.221909	{"sample_index": [0], "errors": [], "sample_id": ["sample_1_0"]}	34	2010	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2	2
12569	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_3			2018-08-22 10:16:07.95525	2018-08-22 10:16:39.48136	{"sample_index": [3], "errors": [], "sample_id": ["sample_1_3"]}	34	2010	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2	3
12612	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1/macro_container_node_add_multiple_ints_1_add_ints_macro___addint1___id_0			2018-08-22 10:37:22.196037	2018-08-22 10:38:17.554254	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	35	2038	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1	2
12720	macro_node_2_2018-08-22T12-40-46/root/sum/macro_node_2___sum___id_0__id_3			2018-08-22 10:42:33.601616	2018-08-22 10:43:27.077204	{"sample_index": [0, 3], "errors": [], "sample_id": ["id_0", "id_3"]}	36	2047	macro_node_2_2018-08-22T12-40-46/root/sum	2
12721	macro_node_2_2018-08-22T12-40-46/root/sum/macro_node_2___sum___id_1__id_0			2018-08-22 10:42:33.664114	2018-08-22 10:43:11.261667	{"sample_index": [1, 0], "errors": [], "sample_id": ["id_1", "id_0"]}	36	2047	macro_node_2_2018-08-22T12-40-46/root/sum	2
12621	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2/macro_container_node_add_multiple_ints_1_add_ints_macro___addint2___id_1_3			2018-08-22 10:37:22.913869	2018-08-22 10:39:43.126659	{"sample_index": [4], "errors": [], "sample_id": ["id_1_3"]}	35	2039	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2	2
12628	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input/macro_container_node_add_multiple_ints_2_add_ints_macro___input___id_1_0			2018-08-22 10:39:44.298891	2018-08-22 10:39:44.359606	{"sample_index": [1], "errors": [], "sample_id": ["id_1_0"]}	35	2037	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input	2
12622	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink/macro_container_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0			2018-08-22 10:39:43.476056	2018-08-22 10:39:43.537441	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	35	2033	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink	2
12624	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink/macro_container_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1_1			2018-08-22 10:39:43.734789	2018-08-22 10:39:43.799507	{"sample_index": [2], "errors": [], "sample_id": ["id_1_1"]}	35	2033	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink	2
12623	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink/macro_container_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1_0			2018-08-22 10:39:43.600632	2018-08-22 10:39:43.669166	{"sample_index": [1], "errors": [], "sample_id": ["id_1_0"]}	35	2033	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink	2
12627	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input/macro_container_node_add_multiple_ints_2_add_ints_macro___input___id_0			2018-08-22 10:39:44.165202	2018-08-22 10:39:44.22817	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	35	2037	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input	2
12614	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1/macro_container_node_add_multiple_ints_1_add_ints_macro___addint1___id_1_1			2018-08-22 10:37:22.332017	2018-08-22 10:38:15.798632	{"sample_index": [2], "errors": [], "sample_id": ["id_1_1"]}	35	2038	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1	2
12615	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1/macro_container_node_add_multiple_ints_1_add_ints_macro___addint1___id_1_2			2018-08-22 10:37:22.393953	2018-08-22 10:38:17.637965	{"sample_index": [3], "errors": [], "sample_id": ["id_1_2"]}	35	2038	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1	2
12616	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1/macro_container_node_add_multiple_ints_1_add_ints_macro___addint1___id_1_3			2018-08-22 10:37:22.462465	2018-08-22 10:38:38.98945	{"sample_index": [4], "errors": [], "sample_id": ["id_1_3"]}	35	2038	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1	2
12618	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2/macro_container_node_add_multiple_ints_1_add_ints_macro___addint2___id_1_0			2018-08-22 10:37:22.704001	2018-08-22 10:39:15.498102	{"sample_index": [1], "errors": [], "sample_id": ["id_1_0"]}	35	2039	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2	2
12620	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2/macro_container_node_add_multiple_ints_1_add_ints_macro___addint2___id_1_2			2018-08-22 10:37:22.845457	2018-08-22 10:39:18.565716	{"sample_index": [3], "errors": [], "sample_id": ["id_1_2"]}	35	2039	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2	2
12617	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2/macro_container_node_add_multiple_ints_1_add_ints_macro___addint2___id_0			2018-08-22 10:37:22.639243	2018-08-22 10:39:26.281961	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	35	2039	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2	2
12619	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2/macro_container_node_add_multiple_ints_1_add_ints_macro___addint2___id_1_1			2018-08-22 10:37:22.774964	2018-08-22 10:39:38.708249	{"sample_index": [2], "errors": [], "sample_id": ["id_1_1"]}	35	2039	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2	2
12626	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink/macro_container_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1_3			2018-08-22 10:39:43.998649	2018-08-22 10:39:44.062925	{"sample_index": [4], "errors": [], "sample_id": ["id_1_3"]}	35	2033	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink	2
12625	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink/macro_container_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1_2			2018-08-22 10:39:43.871553	2018-08-22 10:39:43.935229	{"sample_index": [3], "errors": [], "sample_id": ["id_1_2"]}	35	2033	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink	2
12629	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input/macro_container_node_add_multiple_ints_2_add_ints_macro___input___id_1_1			2018-08-22 10:39:44.422882	2018-08-22 10:39:44.484251	{"sample_index": [2], "errors": [], "sample_id": ["id_1_1"]}	35	2037	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input	2
12631	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input/macro_container_node_add_multiple_ints_2_add_ints_macro___input___id_1_3			2018-08-22 10:39:44.678926	2018-08-22 10:39:44.740813	{"sample_index": [4], "errors": [], "sample_id": ["id_1_3"]}	35	2037	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input	2
12630	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input/macro_container_node_add_multiple_ints_2_add_ints_macro___input___id_1_2			2018-08-22 10:39:44.55163	2018-08-22 10:39:44.613099	{"sample_index": [3], "errors": [], "sample_id": ["id_1_2"]}	35	2037	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input	2
12632	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/const_addint2_right_hand_0/macro_container_node_add_multiple_ints_2_add_ints_macro___const_addint2_right_hand_0___id_0			2018-08-22 10:39:44.836853	2018-08-22 10:39:44.896915	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	35	2045	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/const_addint2_right_hand_0	2
13069	example_2018-09-03T11-07-53/root/addint/example___addint___s4			2018-09-03 09:07:56.874984	2018-09-03 09:08:22.310796	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	49	2157	example_2018-09-03T11-07-53/root/addint	2
12633	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/const_addint1_right_hand_0/macro_container_node_add_multiple_ints_2_add_ints_macro___const_addint1_right_hand_0___id_0			2018-08-22 10:39:44.98705	2018-08-22 10:39:45.063783	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	35	2044	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/const_addint1_right_hand_0	2
12634	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1/macro_container_node_add_multiple_ints_2_add_ints_macro___addint1___id_0			2018-08-22 10:39:45.157666	2018-08-22 10:40:17.819606	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	35	2042	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1	4
12647	macro_node_2_2018-08-22T12-40-46/root/source_3/macro_node_2___source_3___id_0			2018-08-22 10:40:47.566235	2018-08-22 10:40:47.640937	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	36	2050	macro_node_2_2018-08-22T12-40-46/root/source_3	2
12638	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1/macro_container_node_add_multiple_ints_2_add_ints_macro___addint1___id_1_3			2018-08-22 10:39:45.439723	2018-08-22 10:40:18.010965	{"sample_index": [4], "errors": [], "sample_id": ["id_1_3"]}	35	2042	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1	4
12645	macro_node_2_2018-08-22T12-40-46/root/source_2/macro_node_2___source_2___id_1			2018-08-22 10:40:47.265232	2018-08-22 10:40:47.327641	{"sample_index": [1], "errors": [], "sample_id": ["id_1"]}	36	2049	macro_node_2_2018-08-22T12-40-46/root/source_2	2
12637	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1/macro_container_node_add_multiple_ints_2_add_ints_macro___addint1___id_1_2			2018-08-22 10:39:45.364938	2018-08-22 10:40:18.16165	{"sample_index": [3], "errors": [], "sample_id": ["id_1_2"]}	35	2042	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1	4
12636	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1/macro_container_node_add_multiple_ints_2_add_ints_macro___addint1___id_1_1			2018-08-22 10:39:45.294585	2018-08-22 10:40:18.307965	{"sample_index": [2], "errors": [], "sample_id": ["id_1_1"]}	35	2042	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1	4
12635	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1/macro_container_node_add_multiple_ints_2_add_ints_macro___addint1___id_1_0			2018-08-22 10:39:45.225721	2018-08-22 10:40:18.459728	{"sample_index": [1], "errors": [], "sample_id": ["id_1_0"]}	35	2042	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1	4
12642	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2/macro_container_node_add_multiple_ints_2_add_ints_macro___addint2___id_1_2			2018-08-22 10:39:45.753325	2018-08-22 10:40:16.718443	{"sample_index": [3], "errors": [], "sample_id": ["id_1_2"]}	35	2043	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2	4
12643	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2/macro_container_node_add_multiple_ints_2_add_ints_macro___addint2___id_1_3			2018-08-22 10:39:45.830193	2018-08-22 10:40:17.2698	{"sample_index": [4], "errors": [], "sample_id": ["id_1_3"]}	35	2043	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2	4
12640	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2/macro_container_node_add_multiple_ints_2_add_ints_macro___addint2___id_1_0			2018-08-22 10:39:45.603821	2018-08-22 10:40:17.417682	{"sample_index": [1], "errors": [], "sample_id": ["id_1_0"]}	35	2043	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2	4
12641	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2/macro_container_node_add_multiple_ints_2_add_ints_macro___addint2___id_1_1			2018-08-22 10:39:45.673411	2018-08-22 10:40:17.553438	{"sample_index": [2], "errors": [], "sample_id": ["id_1_1"]}	35	2043	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2	4
12639	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2/macro_container_node_add_multiple_ints_2_add_ints_macro___addint2___id_0			2018-08-22 10:39:45.532711	2018-08-22 10:40:17.685174	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	35	2043	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2	4
12649	macro_node_2_2018-08-22T12-40-46/root/source_3/macro_node_2___source_3___id_2			2018-08-22 10:40:47.865816	2018-08-22 10:40:47.940751	{"sample_index": [2], "errors": [], "sample_id": ["id_2"]}	36	2050	macro_node_2_2018-08-22T12-40-46/root/source_3	2
12644	macro_node_2_2018-08-22T12-40-46/root/source_2/macro_node_2___source_2___id_0			2018-08-22 10:40:47.124051	2018-08-22 10:40:47.188486	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	36	2049	macro_node_2_2018-08-22T12-40-46/root/source_2	2
12646	macro_node_2_2018-08-22T12-40-46/root/source_2/macro_node_2___source_2___id_2			2018-08-22 10:40:47.405738	2018-08-22 10:40:47.470438	{"sample_index": [2], "errors": [], "sample_id": ["id_2"]}	36	2049	macro_node_2_2018-08-22T12-40-46/root/source_2	2
12648	macro_node_2_2018-08-22T12-40-46/root/source_3/macro_node_2___source_3___id_1			2018-08-22 10:40:47.720874	2018-08-22 10:40:47.784543	{"sample_index": [1], "errors": [], "sample_id": ["id_1"]}	36	2050	macro_node_2_2018-08-22T12-40-46/root/source_3	2
12651	macro_node_2_2018-08-22T12-40-46/root/source_1/macro_node_2___source_1___id_0			2018-08-22 10:40:48.18573	2018-08-22 10:40:48.255027	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	36	2051	macro_node_2_2018-08-22T12-40-46/root/source_1	2
12650	macro_node_2_2018-08-22T12-40-46/root/source_3/macro_node_2___source_3___id_3			2018-08-22 10:40:48.022734	2018-08-22 10:40:48.091876	{"sample_index": [3], "errors": [], "sample_id": ["id_3"]}	36	2050	macro_node_2_2018-08-22T12-40-46/root/source_3	2
12652	macro_node_2_2018-08-22T12-40-46/root/source_1/macro_node_2___source_1___id_1			2018-08-22 10:40:48.335701	2018-08-22 10:40:48.40532	{"sample_index": [1], "errors": [], "sample_id": ["id_1"]}	36	2051	macro_node_2_2018-08-22T12-40-46/root/source_1	2
12655	macro_node_2_2018-08-22T12-40-46/root/addint/macro_node_2___addint___id_0__id_2			2018-08-22 10:40:48.641565	2018-08-22 10:41:35.326012	{"sample_index": [0, 2], "errors": [], "sample_id": ["id_0", "id_2"]}	36	2048	macro_node_2_2018-08-22T12-40-46/root/addint	2
12656	macro_node_2_2018-08-22T12-40-46/root/addint/macro_node_2___addint___id_1__id_0			2018-08-22 10:40:48.712401	2018-08-22 10:41:36.51098	{"sample_index": [1, 0], "errors": [], "sample_id": ["id_1", "id_0"]}	36	2048	macro_node_2_2018-08-22T12-40-46/root/addint	2
12654	macro_node_2_2018-08-22T12-40-46/root/addint/macro_node_2___addint___id_0__id_1			2018-08-22 10:40:48.572927	2018-08-22 10:41:36.429296	{"sample_index": [0, 1], "errors": [], "sample_id": ["id_0", "id_1"]}	36	2048	macro_node_2_2018-08-22T12-40-46/root/addint	2
12653	macro_node_2_2018-08-22T12-40-46/root/addint/macro_node_2___addint___id_0__id_0			2018-08-22 10:40:48.504784	2018-08-22 10:41:20.941222	{"sample_index": [0, 0], "errors": [], "sample_id": ["id_0", "id_0"]}	36	2048	macro_node_2_2018-08-22T12-40-46/root/addint	2
12663	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_1___id_1__id_1			2018-08-22 10:40:49.460595	2018-08-22 10:40:49.522627	{"sample_index": [1, 1], "errors": [], "sample_id": ["id_1", "id_1"]}	36	2056	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1	2
12659	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_1___id_0__id_0			2018-08-22 10:40:48.943514	2018-08-22 10:40:49.007675	{"sample_index": [0, 0], "errors": [], "sample_id": ["id_0", "id_0"]}	36	2056	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1	2
12660	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_1___id_0__id_1			2018-08-22 10:40:49.072436	2018-08-22 10:40:49.136895	{"sample_index": [0, 1], "errors": [], "sample_id": ["id_0", "id_1"]}	36	2056	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1	2
12674	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_1__id_1			2018-08-22 10:40:50.653193	2018-08-22 10:42:13.377013	{"sample_index": [1, 1], "errors": [], "sample_id": ["id_0+id_1", "id_1"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12664	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_1___id_1__id_2			2018-08-22 10:40:49.590657	2018-08-22 10:40:49.664968	{"sample_index": [1, 2], "errors": [], "sample_id": ["id_1", "id_2"]}	36	2056	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1	2
12661	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_1___id_0__id_2			2018-08-22 10:40:49.202622	2018-08-22 10:40:49.266606	{"sample_index": [0, 2], "errors": [], "sample_id": ["id_0", "id_2"]}	36	2056	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1	2
12667	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_2/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_2___id_2			2018-08-22 10:40:50.009149	2018-08-22 10:40:50.074192	{"sample_index": [2], "errors": [], "sample_id": ["id_2"]}	36	2057	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_2	2
12662	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_1___id_1__id_0			2018-08-22 10:40:49.331927	2018-08-22 10:40:49.396265	{"sample_index": [1, 0], "errors": [], "sample_id": ["id_1", "id_0"]}	36	2056	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1	2
12671	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_0__id_2			2018-08-22 10:40:50.447305	2018-08-22 10:42:13.211935	{"sample_index": [0, 2], "errors": [], "sample_id": ["id_0+id_0", "id_2"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12665	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_2/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_2___id_0			2018-08-22 10:40:49.746035	2018-08-22 10:40:49.810182	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	36	2057	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_2	2
12668	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_2/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_2___id_3			2018-08-22 10:40:50.141337	2018-08-22 10:40:50.210328	{"sample_index": [3], "errors": [], "sample_id": ["id_3"]}	36	2057	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_2	2
12666	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_2/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_2___id_1			2018-08-22 10:40:49.877575	2018-08-22 10:40:49.945656	{"sample_index": [1], "errors": [], "sample_id": ["id_1"]}	36	2057	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_2	2
12673	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_1__id_0			2018-08-22 10:40:50.58538	2018-08-22 10:42:23.53271	{"sample_index": [1, 0], "errors": [], "sample_id": ["id_0+id_1", "id_0"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12672	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_0__id_3			2018-08-22 10:40:50.517896	2018-08-22 10:42:13.293766	{"sample_index": [0, 3], "errors": [], "sample_id": ["id_0+id_0", "id_3"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12675	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_1__id_2			2018-08-22 10:40:50.724608	2018-08-22 10:42:13.459225	{"sample_index": [1, 2], "errors": [], "sample_id": ["id_0+id_1", "id_2"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12677	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_2__id_0			2018-08-22 10:40:50.936475	2018-08-22 10:42:24.597067	{"sample_index": [2, 0], "errors": [], "sample_id": ["id_0+id_2", "id_0"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12678	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_2__id_1			2018-08-22 10:40:51.002353	2018-08-22 10:42:13.541687	{"sample_index": [2, 1], "errors": [], "sample_id": ["id_0+id_2", "id_1"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12669	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_0__id_0			2018-08-22 10:40:50.307889	2018-08-22 10:42:14.112978	{"sample_index": [0, 0], "errors": [], "sample_id": ["id_0+id_0", "id_0"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12676	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_1__id_3			2018-08-22 10:40:50.79115	2018-08-22 10:42:13.622389	{"sample_index": [1, 3], "errors": [], "sample_id": ["id_0+id_1", "id_3"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12670	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_0__id_1			2018-08-22 10:40:50.376053	2018-08-22 10:42:13.067497	{"sample_index": [0, 1], "errors": [], "sample_id": ["id_0+id_0", "id_1"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
13070	example_2018-09-03T11-07-53/root/sink1/example___sink1___s1___0			2018-09-03 09:07:56.996328	2018-09-03 09:08:33.739502	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	49	2156	example_2018-09-03T11-07-53/root/sink1	2
12691	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_2__id_2			2018-08-22 10:40:51.863165	2018-08-22 10:42:14.022299	{"sample_index": [5, 2], "errors": [], "sample_id": ["id_1+id_2", "id_2"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12681	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_0__id_0			2018-08-22 10:40:51.201544	2018-08-22 10:42:14.198604	{"sample_index": [3, 0], "errors": [], "sample_id": ["id_1+id_0", "id_0"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12695	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_0__id_2			2018-08-22 10:42:30.317463	2018-08-22 10:42:30.377016	{"sample_index": [0, 2], "errors": [], "sample_id": ["id_0+id_0", "id_2"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12684	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_0__id_3			2018-08-22 10:40:51.40027	2018-08-22 10:42:20.174377	{"sample_index": [3, 3], "errors": [], "sample_id": ["id_1+id_0", "id_3"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12693	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_0__id_0			2018-08-22 10:42:30.06097	2018-08-22 10:42:30.12353	{"sample_index": [0, 0], "errors": [], "sample_id": ["id_0+id_0", "id_0"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12686	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_1__id_1			2018-08-22 10:40:51.533476	2018-08-22 10:42:14.349188	{"sample_index": [4, 1], "errors": [], "sample_id": ["id_1+id_1", "id_1"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12685	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_1__id_0			2018-08-22 10:40:51.469176	2018-08-22 10:42:23.615682	{"sample_index": [4, 0], "errors": [], "sample_id": ["id_1+id_1", "id_0"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12687	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_1__id_2			2018-08-22 10:40:51.604098	2018-08-22 10:42:14.432593	{"sample_index": [4, 2], "errors": [], "sample_id": ["id_1+id_1", "id_2"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12690	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_2__id_1			2018-08-22 10:40:51.80056	2018-08-22 10:42:23.701512	{"sample_index": [5, 1], "errors": [], "sample_id": ["id_1+id_2", "id_1"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12682	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_0__id_1			2018-08-22 10:40:51.265522	2018-08-22 10:42:29.099585	{"sample_index": [3, 1], "errors": [], "sample_id": ["id_1+id_0", "id_1"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12688	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_1__id_3			2018-08-22 10:40:51.668474	2018-08-22 10:42:15.281561	{"sample_index": [4, 3], "errors": [], "sample_id": ["id_1+id_1", "id_3"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12694	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_0__id_1			2018-08-22 10:42:30.187519	2018-08-22 10:42:30.253662	{"sample_index": [0, 1], "errors": [], "sample_id": ["id_0+id_0", "id_1"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12679	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_2__id_2			2018-08-22 10:40:51.068846	2018-08-22 10:42:13.706863	{"sample_index": [2, 2], "errors": [], "sample_id": ["id_0+id_2", "id_2"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12680	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_2__id_3			2018-08-22 10:40:51.132072	2018-08-22 10:42:13.787389	{"sample_index": [2, 3], "errors": [], "sample_id": ["id_0+id_2", "id_3"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12683	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_0__id_2			2018-08-22 10:40:51.330263	2018-08-22 10:42:13.870687	{"sample_index": [3, 2], "errors": [], "sample_id": ["id_1+id_0", "id_2"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12689	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_2__id_0			2018-08-22 10:40:51.732524	2018-08-22 10:42:13.95499	{"sample_index": [5, 0], "errors": [], "sample_id": ["id_1+id_2", "id_0"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12692	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_2__id_3			2018-08-22 10:40:51.927122	2018-08-22 10:42:15.371643	{"sample_index": [5, 3], "errors": [], "sample_id": ["id_1+id_2", "id_3"]}	36	2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	2
12697	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_1__id_0			2018-08-22 10:42:30.553984	2018-08-22 10:42:30.614446	{"sample_index": [1, 0], "errors": [], "sample_id": ["id_0+id_1", "id_0"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12696	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_0__id_3			2018-08-22 10:42:30.43675	2018-08-22 10:42:30.494845	{"sample_index": [0, 3], "errors": [], "sample_id": ["id_0+id_0", "id_3"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12698	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_1__id_1			2018-08-22 10:42:30.676433	2018-08-22 10:42:30.734979	{"sample_index": [1, 1], "errors": [], "sample_id": ["id_0+id_1", "id_1"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
13071	example_2018-09-03T11-07-53/root/sink1/example___sink1___s2___0			2018-09-03 09:07:57.115854	2018-09-03 09:08:39.808777	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	49	2156	example_2018-09-03T11-07-53/root/sink1	2
12703	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_2__id_2			2018-08-22 10:42:31.277686	2018-08-22 10:42:31.338204	{"sample_index": [2, 2], "errors": [], "sample_id": ["id_0+id_2", "id_2"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12699	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_1__id_2			2018-08-22 10:42:30.797176	2018-08-22 10:42:30.855173	{"sample_index": [1, 2], "errors": [], "sample_id": ["id_0+id_1", "id_2"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12700	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_1__id_3			2018-08-22 10:42:30.914968	2018-08-22 10:42:30.974276	{"sample_index": [1, 3], "errors": [], "sample_id": ["id_0+id_1", "id_3"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12704	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_2__id_3			2018-08-22 10:42:31.401429	2018-08-22 10:42:31.462467	{"sample_index": [2, 3], "errors": [], "sample_id": ["id_0+id_2", "id_3"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12701	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_2__id_0			2018-08-22 10:42:31.033449	2018-08-22 10:42:31.08967	{"sample_index": [2, 0], "errors": [], "sample_id": ["id_0+id_2", "id_0"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12709	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_1__id_0			2018-08-22 10:42:32.016752	2018-08-22 10:42:32.0741	{"sample_index": [4, 0], "errors": [], "sample_id": ["id_1+id_1", "id_0"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12702	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_2__id_1			2018-08-22 10:42:31.15603	2018-08-22 10:42:31.215091	{"sample_index": [2, 1], "errors": [], "sample_id": ["id_0+id_2", "id_1"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12707	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_0__id_2			2018-08-22 10:42:31.775365	2018-08-22 10:42:31.835552	{"sample_index": [3, 2], "errors": [], "sample_id": ["id_1+id_0", "id_2"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12705	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_0__id_0			2018-08-22 10:42:31.525546	2018-08-22 10:42:31.585001	{"sample_index": [3, 0], "errors": [], "sample_id": ["id_1+id_0", "id_0"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12711	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_1__id_2			2018-08-22 10:42:32.260858	2018-08-22 10:42:32.321868	{"sample_index": [4, 2], "errors": [], "sample_id": ["id_1+id_1", "id_2"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12706	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_0__id_1			2018-08-22 10:42:31.655292	2018-08-22 10:42:31.716475	{"sample_index": [3, 1], "errors": [], "sample_id": ["id_1+id_0", "id_1"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12708	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_0__id_3			2018-08-22 10:42:31.89862	2018-08-22 10:42:31.956882	{"sample_index": [3, 3], "errors": [], "sample_id": ["id_1+id_0", "id_3"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12710	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_1__id_1			2018-08-22 10:42:32.138141	2018-08-22 10:42:32.197152	{"sample_index": [4, 1], "errors": [], "sample_id": ["id_1+id_1", "id_1"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12715	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_2__id_2			2018-08-22 10:42:32.777386	2018-08-22 10:42:32.837342	{"sample_index": [5, 2], "errors": [], "sample_id": ["id_1+id_2", "id_2"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12713	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_2__id_0			2018-08-22 10:42:32.51076	2018-08-22 10:42:32.573322	{"sample_index": [5, 0], "errors": [], "sample_id": ["id_1+id_2", "id_0"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12712	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_1__id_3			2018-08-22 10:42:32.388769	2018-08-22 10:42:32.447566	{"sample_index": [4, 3], "errors": [], "sample_id": ["id_1+id_1", "id_3"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12714	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_2__id_1			2018-08-22 10:42:32.63907	2018-08-22 10:42:32.716169	{"sample_index": [5, 1], "errors": [], "sample_id": ["id_1+id_2", "id_1"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12716	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_2__id_3			2018-08-22 10:42:32.903662	2018-08-22 10:42:32.965325	{"sample_index": [5, 3], "errors": [], "sample_id": ["id_1+id_2", "id_3"]}	36	2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	2
12718	macro_node_2_2018-08-22T12-40-46/root/sum/macro_node_2___sum___id_0__id_1			2018-08-22 10:42:33.47897	2018-08-22 10:43:21.407788	{"sample_index": [0, 1], "errors": [], "sample_id": ["id_0", "id_1"]}	36	2047	macro_node_2_2018-08-22T12-40-46/root/sum	2
12719	macro_node_2_2018-08-22T12-40-46/root/sum/macro_node_2___sum___id_0__id_2			2018-08-22 10:42:33.540547	2018-08-22 10:43:22.492899	{"sample_index": [0, 2], "errors": [], "sample_id": ["id_0", "id_2"]}	36	2047	macro_node_2_2018-08-22T12-40-46/root/sum	2
13072	example_2018-09-03T11-07-53/root/sink1/example___sink1___s3___0			2018-09-03 09:07:57.236158	2018-09-03 09:08:45.211569	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	49	2156	example_2018-09-03T11-07-53/root/sink1	2
12717	macro_node_2_2018-08-22T12-40-46/root/sum/macro_node_2___sum___id_0__id_0			2018-08-22 10:42:33.414094	2018-08-22 10:43:18.034329	{"sample_index": [0, 0], "errors": [], "sample_id": ["id_0", "id_0"]}	36	2047	macro_node_2_2018-08-22T12-40-46/root/sum	2
12736	cross_validation_2018-08-22T15-29-33/root/numbers/cross_validation___numbers___id_3			2018-08-22 13:29:34.303112	2018-08-22 13:29:34.361661	{"sample_index": [3], "errors": [], "sample_id": ["id_3"]}	37	2062	cross_validation_2018-08-22T15-29-33/root/numbers	2
12737	cross_validation_2018-08-22T15-29-33/root/numbers/cross_validation___numbers___id_4			2018-08-22 13:29:34.436397	2018-08-22 13:29:34.495248	{"sample_index": [4], "errors": [], "sample_id": ["id_4"]}	37	2062	cross_validation_2018-08-22T15-29-33/root/numbers	2
12730	macro_node_2_2018-08-22T12-40-46/root/sink/macro_node_2___sink___id_1__id_1___0			2018-08-22 10:42:34.2453	2018-08-22 10:44:11.543941	{"sample_index": [1, 1], "errors": [], "sample_id": ["id_1", "id_1"]}	36	2053	macro_node_2_2018-08-22T12-40-46/root/sink	2
12732	macro_node_2_2018-08-22T12-40-46/root/sink/macro_node_2___sink___id_1__id_3___0			2018-08-22 10:42:34.363674	2018-08-22 10:44:11.63104	{"sample_index": [1, 3], "errors": [], "sample_id": ["id_1", "id_3"]}	36	2053	macro_node_2_2018-08-22T12-40-46/root/sink	2
12731	macro_node_2_2018-08-22T12-40-46/root/sink/macro_node_2___sink___id_1__id_2___0			2018-08-22 10:42:34.304631	2018-08-22 10:44:13.706666	{"sample_index": [1, 2], "errors": [], "sample_id": ["id_1", "id_2"]}	36	2053	macro_node_2_2018-08-22T12-40-46/root/sink	2
12733	cross_validation_2018-08-22T15-29-33/root/numbers/cross_validation___numbers___id_0			2018-08-22 13:29:33.899694	2018-08-22 13:29:33.957956	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	37	2062	cross_validation_2018-08-22T15-29-33/root/numbers	2
12738	cross_validation_2018-08-22T15-29-33/root/const_crossvalidtion_method_0/cross_validation___const_crossvalidtion_method_0___id_0			2018-08-22 13:29:34.581883	2018-08-22 13:29:34.640939	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	37	2061	cross_validation_2018-08-22T15-29-33/root/const_crossvalidtion_method_0	2
12734	cross_validation_2018-08-22T15-29-33/root/numbers/cross_validation___numbers___id_1			2018-08-22 13:29:34.033452	2018-08-22 13:29:34.092421	{"sample_index": [1], "errors": [], "sample_id": ["id_1"]}	37	2062	cross_validation_2018-08-22T15-29-33/root/numbers	2
12746	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_4__id_1__fold_0			2018-08-22 13:29:36.853642	2018-08-22 13:30:21.374056	{"sample_index": [2, 1, 0], "errors": [], "sample_id": ["id_4", "id_1", "fold_0"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
12735	cross_validation_2018-08-22T15-29-33/root/numbers/cross_validation___numbers___id_2			2018-08-22 13:29:34.166253	2018-08-22 13:29:34.226486	{"sample_index": [2], "errors": [], "sample_id": ["id_2"]}	37	2062	cross_validation_2018-08-22T15-29-33/root/numbers	2
12743	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_3__id_0__fold_0			2018-08-22 13:29:36.660682	2018-08-22 13:30:21.179642	{"sample_index": [1, 0, 0], "errors": [], "sample_id": ["id_3", "id_0", "fold_0"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
12739	cross_validation_2018-08-22T15-29-33/root/const_crossvalidtion_number_of_folds_0/cross_validation___const_crossvalidtion_number_of_folds_0___id_0			2018-08-22 13:29:34.725399	2018-08-22 13:29:34.789226	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	37	2064	cross_validation_2018-08-22T15-29-33/root/const_crossvalidtion_number_of_folds_0	2
12744	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_3__id_1__fold_0			2018-08-22 13:29:36.725633	2018-08-22 13:30:21.249173	{"sample_index": [1, 1, 0], "errors": [], "sample_id": ["id_3", "id_1", "fold_0"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
12740	cross_validation_2018-08-22T15-29-33/root/crossvalidtion/cross_validation___crossvalidtion___FLOW			2018-08-22 13:29:34.908031	2018-08-22 13:29:35.556256	{"sample_index": [0], "errors": [], "sample_id": ["FLOW"]}	37	2059	cross_validation_2018-08-22T15-29-33/root/crossvalidtion	2
12745	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_4__id_0__fold_0			2018-08-22 13:29:36.791498	2018-08-22 13:30:21.311563	{"sample_index": [2, 0, 0], "errors": [], "sample_id": ["id_4", "id_0", "fold_0"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
12752	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_4__id_3__fold_1			2018-08-22 13:29:37.265232	2018-08-22 13:30:21.450248	{"sample_index": [2, 1, 1], "errors": [], "sample_id": ["id_4", "id_3", "fold_1"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
12747	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_0__id_2__fold_1			2018-08-22 13:29:36.938105	2018-08-22 13:30:18.140035	{"sample_index": [0, 0, 1], "errors": [], "sample_id": ["id_0", "id_2", "fold_1"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
12751	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_4__id_2__fold_1			2018-08-22 13:29:37.201881	2018-08-22 13:30:18.202348	{"sample_index": [2, 0, 1], "errors": [], "sample_id": ["id_4", "id_2", "fold_1"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
12742	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_2__id_1__fold_0			2018-08-22 13:29:36.596722	2018-08-22 13:30:20.593594	{"sample_index": [0, 1, 0], "errors": [], "sample_id": ["id_2", "id_1", "fold_0"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
12750	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_1__id_3__fold_1			2018-08-22 13:29:37.136866	2018-08-22 13:30:22.060505	{"sample_index": [1, 1, 1], "errors": [], "sample_id": ["id_1", "id_3", "fold_1"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
12754	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_1__id_4__fold_2			2018-08-22 13:29:37.419641	2018-08-22 13:30:22.14178	{"sample_index": [1, 0, 2], "errors": [], "sample_id": ["id_1", "id_4", "fold_2"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
12753	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_0__id_4__fold_2			2018-08-22 13:29:37.345638	2018-08-22 13:30:21.516649	{"sample_index": [0, 0, 2], "errors": [], "sample_id": ["id_0", "id_4", "fold_2"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
12749	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_1__id_2__fold_1			2018-08-22 13:29:37.071302	2018-08-22 13:30:21.581915	{"sample_index": [1, 0, 1], "errors": [], "sample_id": ["id_1", "id_2", "fold_1"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
12741	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_2__id_0__fold_0			2018-08-22 13:29:36.533227	2018-08-22 13:30:23.135778	{"sample_index": [0, 0, 0], "errors": [], "sample_id": ["id_2", "id_0", "fold_0"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
12748	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_0__id_3__fold_1			2018-08-22 13:29:37.005339	2018-08-22 13:30:17.394019	{"sample_index": [0, 1, 1], "errors": [], "sample_id": ["id_0", "id_3", "fold_1"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
13073	example_2018-09-03T11-07-53/root/sink1/example___sink1___s4___0			2018-09-03 09:07:57.350394	2018-09-03 09:08:49.792501	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	49	2156	example_2018-09-03T11-07-53/root/sink1	2
12759	cross_validation_2018-08-22T15-29-33/root/sum/cross_validation___sum___id_4__fold_2			2018-08-22 13:29:37.786447	2018-08-22 13:31:23.176767	{"sample_index": [0, 2], "errors": [], "sample_id": ["id_4", "fold_2"]}	37	2060	cross_validation_2018-08-22T15-29-33/root/sum	2
12760	cross_validation_2018-08-22T15-29-33/root/sum/cross_validation___sum___id_1__fold_0			2018-08-22 13:29:37.846742	2018-08-22 13:31:23.256615	{"sample_index": [1, 0], "errors": [], "sample_id": ["id_1", "fold_0"]}	37	2060	cross_validation_2018-08-22T15-29-33/root/sum	2
12763	cross_validation_2018-08-22T15-29-33/root/sink/cross_validation___sink___id_2__fold_1___0			2018-08-22 13:29:38.050529	2018-08-22 13:32:16.912857	{"sample_index": [0, 1], "errors": [], "sample_id": ["id_2", "fold_1"]}	37	2065	cross_validation_2018-08-22T15-29-33/root/sink	2
12764	cross_validation_2018-08-22T15-29-33/root/sink/cross_validation___sink___id_4__fold_2___0			2018-08-22 13:29:38.113016	2018-08-22 13:32:21.126264	{"sample_index": [0, 2], "errors": [], "sample_id": ["id_4", "fold_2"]}	37	2065	cross_validation_2018-08-22T15-29-33/root/sink	2
12762	cross_validation_2018-08-22T15-29-33/root/sink/cross_validation___sink___id_0__fold_0___0			2018-08-22 13:29:37.988844	2018-08-22 13:32:22.232751	{"sample_index": [0, 0], "errors": [], "sample_id": ["id_0", "fold_0"]}	37	2065	cross_validation_2018-08-22T15-29-33/root/sink	2
12766	cross_validation_2018-08-22T15-29-33/root/sink/cross_validation___sink___id_3__fold_1___0			2018-08-22 13:29:38.241949	2018-08-22 13:32:23.488421	{"sample_index": [1, 1], "errors": [], "sample_id": ["id_3", "fold_1"]}	37	2065	cross_validation_2018-08-22T15-29-33/root/sink	2
12774	input_groups_2018-08-22T15-39-07/root/add/input_groups___add___id_1_0__id_1			2018-08-22 13:40:13.277208	2018-08-22 13:41:07.443392	{"sample_index": [1, 1], "errors": [], "sample_id": ["id_1_0", "id_1"]}	38	2068	input_groups_2018-08-22T15-39-07/root/add	2
12769	input_groups_2018-08-22T15-39-07/root/const_add_right_hand_0/input_groups___const_add_right_hand_0___id_0			2018-08-22 13:39:07.70116	2018-08-22 13:39:07.766178	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	38	2069	input_groups_2018-08-22T15-39-07/root/const_add_right_hand_0	2
12765	cross_validation_2018-08-22T15-29-33/root/sink/cross_validation___sink___id_1__fold_0___0			2018-08-22 13:29:38.174817	2018-08-22 13:32:36.580038	{"sample_index": [1, 0], "errors": [], "sample_id": ["id_1", "fold_0"]}	37	2065	cross_validation_2018-08-22T15-29-33/root/sink	2
12779	input_groups_2018-08-22T15-39-07/root/add/input_groups___add___id_1_3__id_0			2018-08-22 13:40:13.592799	2018-08-22 13:41:08.854348	{"sample_index": [4, 0], "errors": [], "sample_id": ["id_1_3", "id_0"]}	38	2068	input_groups_2018-08-22T15-39-07/root/add	2
12770	input_groups_2018-08-22T15-39-07/root/const_add_right_hand_0/input_groups___const_add_right_hand_0___id_1			2018-08-22 13:39:07.844844	2018-08-22 13:39:07.907762	{"sample_index": [1], "errors": [], "sample_id": ["id_1"]}	38	2069	input_groups_2018-08-22T15-39-07/root/const_add_right_hand_0	2
12768	input_groups_2018-08-22T15-39-07/root/source/input_groups___source___id_1			2018-08-22 13:39:07.610207	2018-08-22 13:40:12.494806	{"sample_index": [1], "errors": [], "sample_id": ["id_1"]}	38	2067	input_groups_2018-08-22T15-39-07/root/source	2
12756	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_3__id_4__fold_2			2018-08-22 13:29:37.56772	2018-08-22 13:30:18.278922	{"sample_index": [3, 0, 2], "errors": [], "sample_id": ["id_3", "id_4", "fold_2"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
12755	cross_validation_2018-08-22T15-29-33/root/multiply/cross_validation___multiply___id_2__id_4__fold_2			2018-08-22 13:29:37.491805	2018-08-22 13:30:18.360367	{"sample_index": [2, 0, 2], "errors": [], "sample_id": ["id_2", "id_4", "fold_2"]}	37	2063	cross_validation_2018-08-22T15-29-33/root/multiply	2
12761	cross_validation_2018-08-22T15-29-33/root/sum/cross_validation___sum___id_3__fold_1			2018-08-22 13:29:37.912855	2018-08-22 13:31:17.470589	{"sample_index": [1, 1], "errors": [], "sample_id": ["id_3", "fold_1"]}	37	2060	cross_validation_2018-08-22T15-29-33/root/sum	2
12758	cross_validation_2018-08-22T15-29-33/root/sum/cross_validation___sum___id_2__fold_1			2018-08-22 13:29:37.727098	2018-08-22 13:31:18.543471	{"sample_index": [0, 1], "errors": [], "sample_id": ["id_2", "fold_1"]}	37	2060	cross_validation_2018-08-22T15-29-33/root/sum	2
12757	cross_validation_2018-08-22T15-29-33/root/sum/cross_validation___sum___id_0__fold_0			2018-08-22 13:29:37.667056	2018-08-22 13:31:23.099938	{"sample_index": [0, 0], "errors": [], "sample_id": ["id_0", "fold_0"]}	37	2060	cross_validation_2018-08-22T15-29-33/root/sum	2
12767	input_groups_2018-08-22T15-39-07/root/source/input_groups___source___id_0			2018-08-22 13:39:07.432989	2018-08-22 13:39:07.535952	{"sample_index": [0], "errors": [], "sample_id": ["id_0"]}	38	2067	input_groups_2018-08-22T15-39-07/root/source	2
12777	input_groups_2018-08-22T15-39-07/root/add/input_groups___add___id_1_2__id_0			2018-08-22 13:40:13.457521	2018-08-22 13:41:07.238938	{"sample_index": [3, 0], "errors": [], "sample_id": ["id_1_2", "id_0"]}	38	2068	input_groups_2018-08-22T15-39-07/root/add	2
12780	input_groups_2018-08-22T15-39-07/root/add/input_groups___add___id_1_3__id_1			2018-08-22 13:40:13.654593	2018-08-22 13:41:13.180056	{"sample_index": [4, 1], "errors": [], "sample_id": ["id_1_3", "id_1"]}	38	2068	input_groups_2018-08-22T15-39-07/root/add	2
12778	input_groups_2018-08-22T15-39-07/root/add/input_groups___add___id_1_2__id_1			2018-08-22 13:40:13.529897	2018-08-22 13:41:07.379606	{"sample_index": [3, 1], "errors": [], "sample_id": ["id_1_2", "id_1"]}	38	2068	input_groups_2018-08-22T15-39-07/root/add	2
12772	input_groups_2018-08-22T15-39-07/root/add/input_groups___add___id_0__id_1			2018-08-22 13:40:13.155406	2018-08-22 13:41:08.788791	{"sample_index": [0, 1], "errors": [], "sample_id": ["id_0", "id_1"]}	38	2068	input_groups_2018-08-22T15-39-07/root/add	2
12771	input_groups_2018-08-22T15-39-07/root/add/input_groups___add___id_0__id_0			2018-08-22 13:40:13.087938	2018-08-22 13:41:07.315133	{"sample_index": [0, 0], "errors": [], "sample_id": ["id_0", "id_0"]}	38	2068	input_groups_2018-08-22T15-39-07/root/add	2
12775	input_groups_2018-08-22T15-39-07/root/add/input_groups___add___id_1_1__id_0			2018-08-22 13:40:13.336794	2018-08-22 13:41:07.158868	{"sample_index": [2, 0], "errors": [], "sample_id": ["id_1_1", "id_0"]}	38	2068	input_groups_2018-08-22T15-39-07/root/add	2
12773	input_groups_2018-08-22T15-39-07/root/add/input_groups___add___id_1_0__id_0			2018-08-22 13:40:13.218317	2018-08-22 13:41:13.10938	{"sample_index": [1, 0], "errors": [], "sample_id": ["id_1_0", "id_0"]}	38	2068	input_groups_2018-08-22T15-39-07/root/add	2
12782	input_groups_2018-08-22T15-39-07/root/sink/input_groups___sink___id_0__id_1___0			2018-08-22 13:40:13.793457	2018-08-22 13:41:39.261321	{"sample_index": [0, 1], "errors": [], "sample_id": ["id_0", "id_1"]}	38	2070	input_groups_2018-08-22T15-39-07/root/sink	2
12781	input_groups_2018-08-22T15-39-07/root/sink/input_groups___sink___id_0__id_0___0			2018-08-22 13:40:13.730881	2018-08-22 13:41:44.565993	{"sample_index": [0, 0], "errors": [], "sample_id": ["id_0", "id_0"]}	38	2070	input_groups_2018-08-22T15-39-07/root/sink	2
12776	input_groups_2018-08-22T15-39-07/root/add/input_groups___add___id_1_1__id_1			2018-08-22 13:40:13.395463	2018-08-22 13:41:06.123785	{"sample_index": [2, 1], "errors": [], "sample_id": ["id_1_1", "id_1"]}	38	2068	input_groups_2018-08-22T15-39-07/root/add	2
13689	iris/root/0_statistics/9_const_get_gm_volume_flatten_0/job___001			2018-09-08 13:18:14.68438	2018-09-08 13:19:13.89146	{}	75	2388	iris/root/0_statistics/9_const_get_gm_volume_flatten_0	2
12783	input_groups_2018-08-22T15-39-07/root/sink/input_groups___sink___id_1_0__id_0___0			2018-08-22 13:40:13.852603	2018-08-22 13:41:39.537198	{"sample_index": [1, 0], "errors": [], "sample_id": ["id_1_0", "id_0"]}	38	2070	input_groups_2018-08-22T15-39-07/root/sink	2
12787	input_groups_2018-08-22T15-39-07/root/sink/input_groups___sink___id_1_2__id_0___0			2018-08-22 13:40:14.10882	2018-08-22 13:41:39.609679	{"sample_index": [3, 0], "errors": [], "sample_id": ["id_1_2", "id_0"]}	38	2070	input_groups_2018-08-22T15-39-07/root/sink	2
13074	example_2018-09-03T11-12-21/root/source1/example___source1___s1			2018-09-03 09:12:21.681466	2018-09-03 09:12:21.795077	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	50	2160	example_2018-09-03T11-12-21/root/source1	2
12784	input_groups_2018-08-22T15-39-07/root/sink/input_groups___sink___id_1_0__id_1___0			2018-08-22 13:40:13.913151	2018-08-22 13:41:43.535017	{"sample_index": [1, 1], "errors": [], "sample_id": ["id_1_0", "id_1"]}	38	2070	input_groups_2018-08-22T15-39-07/root/sink	2
12786	input_groups_2018-08-22T15-39-07/root/sink/input_groups___sink___id_1_1__id_1___0			2018-08-22 13:40:14.039855	2018-08-22 13:41:43.615785	{"sample_index": [2, 1], "errors": [], "sample_id": ["id_1_1", "id_1"]}	38	2070	input_groups_2018-08-22T15-39-07/root/sink	2
13083	example_2018-09-03T11-12-21/root/addint/example___addint___s2			2018-09-03 09:12:23.721559	2018-09-03 09:12:31.385265	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	50	2162	example_2018-09-03T11-12-21/root/addint	2
12785	input_groups_2018-08-22T15-39-07/root/sink/input_groups___sink___id_1_1__id_0___0			2018-08-22 13:40:13.975899	2018-08-22 13:41:44.717235	{"sample_index": [2, 0], "errors": [], "sample_id": ["id_1_1", "id_0"]}	38	2070	input_groups_2018-08-22T15-39-07/root/sink	2
13079	example_2018-09-03T11-12-21/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 09:12:22.831556	2018-09-03 09:12:22.949016	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	50	2163	example_2018-09-03T11-12-21/root/const_addint_right_hand_0	2
13075	example_2018-09-03T11-12-21/root/source1/example___source1___s2			2018-09-03 09:12:21.903137	2018-09-03 09:12:22.019766	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	50	2160	example_2018-09-03T11-12-21/root/source1	2
12788	input_groups_2018-08-22T15-39-07/root/sink/input_groups___sink___id_1_2__id_1___0			2018-08-22 13:40:14.17208	2018-08-22 13:41:38.286197	{"sample_index": [3, 1], "errors": [], "sample_id": ["id_1_2", "id_1"]}	38	2070	input_groups_2018-08-22T15-39-07/root/sink	2
12790	input_groups_2018-08-22T15-39-07/root/sink/input_groups___sink___id_1_3__id_1___0			2018-08-22 13:40:14.297804	2018-08-22 13:41:39.087684	{"sample_index": [4, 1], "errors": [], "sample_id": ["id_1_3", "id_1"]}	38	2070	input_groups_2018-08-22T15-39-07/root/sink	2
12789	input_groups_2018-08-22T15-39-07/root/sink/input_groups___sink___id_1_3__id_0___0			2018-08-22 13:40:14.235837	2018-08-22 13:41:39.157323	{"sample_index": [4, 0], "errors": [], "sample_id": ["id_1_3", "id_0"]}	38	2070	input_groups_2018-08-22T15-39-07/root/sink	2
13076	example_2018-09-03T11-12-21/root/source1/example___source1___s3			2018-09-03 09:12:22.134656	2018-09-03 09:12:22.249191	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	50	2160	example_2018-09-03T11-12-21/root/source1	2
13086	example_2018-09-03T11-12-21/root/sink1/example___sink1___s1___0			2018-09-03 09:12:24.076947	2018-09-03 09:12:43.577652	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	50	2161	example_2018-09-03T11-12-21/root/sink1	2
13080	example_2018-09-03T11-12-21/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 09:12:23.073196	2018-09-03 09:12:23.20337	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	50	2163	example_2018-09-03T11-12-21/root/const_addint_right_hand_0	2
13077	example_2018-09-03T11-12-21/root/source1/example___source1___s4			2018-09-03 09:12:22.36217	2018-09-03 09:12:22.470173	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	50	2160	example_2018-09-03T11-12-21/root/source1	2
13078	example_2018-09-03T11-12-21/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 09:12:22.579271	2018-09-03 09:12:22.688677	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	50	2163	example_2018-09-03T11-12-21/root/const_addint_right_hand_0	2
13081	example_2018-09-03T11-12-21/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 09:12:23.344541	2018-09-03 09:12:23.481238	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	50	2163	example_2018-09-03T11-12-21/root/const_addint_right_hand_0	2
13084	example_2018-09-03T11-12-21/root/addint/example___addint___s3			2018-09-03 09:12:23.834999	2018-09-03 09:12:35.295862	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	50	2162	example_2018-09-03T11-12-21/root/addint	2
13085	example_2018-09-03T11-12-21/root/addint/example___addint___s4			2018-09-03 09:12:23.950069	2018-09-03 09:12:39.009237	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	50	2162	example_2018-09-03T11-12-21/root/addint	2
13087	example_2018-09-03T11-12-21/root/sink1/example___sink1___s2___0			2018-09-03 09:12:24.201032	2018-09-03 09:12:48.004682	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	50	2161	example_2018-09-03T11-12-21/root/sink1	2
13088	example_2018-09-03T11-12-21/root/sink1/example___sink1___s3___0			2018-09-03 09:12:24.31795	2018-09-03 09:12:53.995954	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	50	2161	example_2018-09-03T11-12-21/root/sink1	2
13092	example_2018-09-03T11-13-16/root/source1/example___source1___s3			2018-09-03 09:13:17.038373	2018-09-03 09:13:17.146333	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	51	2165	example_2018-09-03T11-13-16/root/source1	2
13090	example_2018-09-03T11-13-16/root/source1/example___source1___s1			2018-09-03 09:13:16.594748	2018-09-03 09:13:16.714593	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	51	2165	example_2018-09-03T11-13-16/root/source1	2
13082	example_2018-09-03T11-12-21/root/addint/example___addint___s1			2018-09-03 09:12:23.598327	2018-09-03 09:12:27.946582	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	50	2162	example_2018-09-03T11-12-21/root/addint	2
13089	example_2018-09-03T11-12-21/root/sink1/example___sink1___s4___0			2018-09-03 09:12:24.441793	2018-09-03 09:12:59.274844	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	50	2161	example_2018-09-03T11-12-21/root/sink1	2
13091	example_2018-09-03T11-13-16/root/source1/example___source1___s2			2018-09-03 09:13:16.824624	2018-09-03 09:13:16.930928	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	51	2165	example_2018-09-03T11-13-16/root/source1	2
13094	example_2018-09-03T11-13-16/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 09:13:17.462734	2018-09-03 09:13:17.566229	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	51	2168	example_2018-09-03T11-13-16/root/const_addint_right_hand_0	2
13093	example_2018-09-03T11-13-16/root/source1/example___source1___s4			2018-09-03 09:13:17.254771	2018-09-03 09:13:17.360256	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	51	2165	example_2018-09-03T11-13-16/root/source1	2
13095	example_2018-09-03T11-13-16/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 09:13:17.678348	2018-09-03 09:13:17.782971	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	51	2168	example_2018-09-03T11-13-16/root/const_addint_right_hand_0	2
13690	iris/root/2_hammers_atlas/8_hammers_t1w/job___002			2018-09-08 13:18:14.68438	2018-09-08 13:19:15.199075	{}	75	2408	iris/root/2_hammers_atlas/8_hammers_t1w	2
13096	example_2018-09-03T11-13-16/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 09:13:17.888542	2018-09-03 09:13:17.993966	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	51	2168	example_2018-09-03T11-13-16/root/const_addint_right_hand_0	2
13106	example_2018-09-03T11-14-49/root/source1/example___source1___s1			2018-09-03 09:14:49.422256	2018-09-03 09:14:49.553941	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	52	2170	example_2018-09-03T11-14-49/root/source1	2
13097	example_2018-09-03T11-13-16/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 09:13:18.107466	2018-09-03 09:13:18.216398	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	51	2168	example_2018-09-03T11-13-16/root/const_addint_right_hand_0	2
13117	example_2018-09-03T11-14-49/root/addint/example___addint___s4			2018-09-03 09:14:51.668731	2018-09-03 09:15:06.516292	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	52	2172	example_2018-09-03T11-14-49/root/addint	2
13107	example_2018-09-03T11-14-49/root/source1/example___source1___s2			2018-09-03 09:14:49.685589	2018-09-03 09:14:49.815052	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	52	2170	example_2018-09-03T11-14-49/root/source1	2
13098	example_2018-09-03T11-13-16/root/addint/example___addint___s1			2018-09-03 09:13:18.325675	2018-09-03 09:13:22.741608	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	51	2167	example_2018-09-03T11-13-16/root/addint	2
13111	example_2018-09-03T11-14-49/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 09:14:50.696101	2018-09-03 09:14:50.79999	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	52	2173	example_2018-09-03T11-14-49/root/const_addint_right_hand_0	2
13108	example_2018-09-03T11-14-49/root/source1/example___source1___s3			2018-09-03 09:14:49.938656	2018-09-03 09:14:50.074304	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	52	2170	example_2018-09-03T11-14-49/root/source1	2
13099	example_2018-09-03T11-13-16/root/addint/example___addint___s2			2018-09-03 09:13:18.439852	2018-09-03 09:13:26.180499	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	51	2167	example_2018-09-03T11-13-16/root/addint	2
13100	example_2018-09-03T11-13-16/root/addint/example___addint___s3			2018-09-03 09:13:18.558212	2018-09-03 09:13:29.513583	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	51	2167	example_2018-09-03T11-13-16/root/addint	2
13115	example_2018-09-03T11-14-49/root/addint/example___addint___s2			2018-09-03 09:14:51.440273	2018-09-03 09:14:59.651121	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	52	2172	example_2018-09-03T11-14-49/root/addint	2
13109	example_2018-09-03T11-14-49/root/source1/example___source1___s4			2018-09-03 09:14:50.186689	2018-09-03 09:14:50.320195	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	52	2170	example_2018-09-03T11-14-49/root/source1	2
13101	example_2018-09-03T11-13-16/root/addint/example___addint___s4			2018-09-03 09:13:18.676491	2018-09-03 09:13:33.082261	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	51	2167	example_2018-09-03T11-13-16/root/addint	2
13102	example_2018-09-03T11-13-16/root/sink1/example___sink1___s1___0			2018-09-03 09:13:18.791387	2018-09-03 09:13:37.578372	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	51	2166	example_2018-09-03T11-13-16/root/sink1	2
13112	example_2018-09-03T11-14-49/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 09:14:50.902957	2018-09-03 09:14:51.008499	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	52	2173	example_2018-09-03T11-14-49/root/const_addint_right_hand_0	2
13103	example_2018-09-03T11-13-16/root/sink1/example___sink1___s2___0			2018-09-03 09:13:18.90951	2018-09-03 09:13:42.04884	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	51	2166	example_2018-09-03T11-13-16/root/sink1	2
13104	example_2018-09-03T11-13-16/root/sink1/example___sink1___s3___0			2018-09-03 09:13:19.029354	2018-09-03 09:13:47.415356	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	51	2166	example_2018-09-03T11-13-16/root/sink1	2
13110	example_2018-09-03T11-14-49/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 09:14:50.44297	2018-09-03 09:14:50.582477	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	52	2173	example_2018-09-03T11-14-49/root/const_addint_right_hand_0	2
13105	example_2018-09-03T11-13-16/root/sink1/example___sink1___s4___0			2018-09-03 09:13:19.143077	2018-09-03 09:13:52.140369	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	51	2166	example_2018-09-03T11-13-16/root/sink1	2
13116	example_2018-09-03T11-14-49/root/addint/example___addint___s3			2018-09-03 09:14:51.554854	2018-09-03 09:15:03.069037	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	52	2172	example_2018-09-03T11-14-49/root/addint	2
13113	example_2018-09-03T11-14-49/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 09:14:51.116548	2018-09-03 09:14:51.224697	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	52	2173	example_2018-09-03T11-14-49/root/const_addint_right_hand_0	2
13119	example_2018-09-03T11-14-49/root/sink1/example___sink1___s2___0			2018-09-03 09:14:51.895808	2018-09-03 09:15:15.35236	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	52	2171	example_2018-09-03T11-14-49/root/sink1	2
13120	example_2018-09-03T11-14-49/root/sink1/example___sink1___s3___0			2018-09-03 09:14:52.007701	2018-09-03 09:15:19.7838	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	52	2171	example_2018-09-03T11-14-49/root/sink1	2
13217	example_2018-09-03T12-47-05/root/source1/example___source1___s4			2018-09-03 10:47:07.020122	2018-09-03 10:47:07.121482	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	58	2206	example_2018-09-03T12-47-05/root/source1	2
13121	example_2018-09-03T11-14-49/root/sink1/example___sink1___s4___0			2018-09-03 09:14:52.126809	2018-09-03 09:15:25.239752	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	52	2171	example_2018-09-03T11-14-49/root/sink1	2
13118	example_2018-09-03T11-14-49/root/sink1/example___sink1___s1___0			2018-09-03 09:14:51.788626	2018-09-03 09:15:10.958564	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	52	2171	example_2018-09-03T11-14-49/root/sink1	2
13114	example_2018-09-03T11-14-49/root/addint/example___addint___s1			2018-09-03 09:14:51.334474	2018-09-03 09:14:55.66655	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	52	2172	example_2018-09-03T11-14-49/root/addint	2
13219	example_2018-09-03T12-47-05/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 10:47:07.467634	2018-09-03 10:47:07.573321	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	58	2209	example_2018-09-03T12-47-05/root/const_addint_right_hand_0	2
13218	example_2018-09-03T12-47-05/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 10:47:07.241349	2018-09-03 10:47:07.355402	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	58	2209	example_2018-09-03T12-47-05/root/const_addint_right_hand_0	2
13220	example_2018-09-03T12-47-05/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 10:47:07.689257	2018-09-03 10:47:07.792646	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	58	2209	example_2018-09-03T12-47-05/root/const_addint_right_hand_0	2
13230	example_2018-09-03T12-54-57/root/source1/example___source1___s1			2018-09-03 10:54:57.446116	2018-09-03 10:54:57.553383	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	59	2211	example_2018-09-03T12-54-57/root/source1	2
13128	example_2018-09-03T11-17-35/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 09:17:37.193539	2018-09-03 09:17:37.309259	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	53	2178	example_2018-09-03T11-17-35/root/const_addint_right_hand_0	2
13122	example_2018-09-03T11-17-35/root/source1/example___source1___s1			2018-09-03 09:17:35.712751	2018-09-03 09:17:35.837349	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	53	2175	example_2018-09-03T11-17-35/root/source1	2
13133	example_2018-09-03T11-17-35/root/addint/example___addint___s4			2018-09-03 09:17:38.170204	2018-09-03 09:17:58.482081	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	53	2177	example_2018-09-03T11-17-35/root/addint	2
13123	example_2018-09-03T11-17-35/root/source1/example___source1___s2			2018-09-03 09:17:35.980638	2018-09-03 09:17:36.114843	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	53	2175	example_2018-09-03T11-17-35/root/source1	2
13129	example_2018-09-03T11-17-35/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 09:17:37.443484	2018-09-03 09:17:37.589892	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	53	2178	example_2018-09-03T11-17-35/root/const_addint_right_hand_0	2
13124	example_2018-09-03T11-17-35/root/source1/example___source1___s3			2018-09-03 09:17:36.243521	2018-09-03 09:17:36.365135	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	53	2175	example_2018-09-03T11-17-35/root/source1	2
13139	example_2018-09-03T11-31-38/root/source1/example___source1___s2			2018-09-03 09:31:39.399898	2018-09-03 09:31:39.516902	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	54	2180	example_2018-09-03T11-31-38/root/source1	2
13125	example_2018-09-03T11-17-35/root/source1/example___source1___s4			2018-09-03 09:17:36.48074	2018-09-03 09:17:36.593731	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	53	2175	example_2018-09-03T11-17-35/root/source1	2
13134	example_2018-09-03T11-17-35/root/sink1/example___sink1___s1___0			2018-09-03 09:17:38.3078	2018-09-03 09:18:03.21867	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	53	2176	example_2018-09-03T11-17-35/root/sink1	2
13126	example_2018-09-03T11-17-35/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 09:17:36.713318	2018-09-03 09:17:36.819531	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	53	2178	example_2018-09-03T11-17-35/root/const_addint_right_hand_0	2
13127	example_2018-09-03T11-17-35/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 09:17:36.940929	2018-09-03 09:17:37.070513	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	53	2178	example_2018-09-03T11-17-35/root/const_addint_right_hand_0	2
13142	example_2018-09-03T11-31-38/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 09:31:40.082725	2018-09-03 09:31:40.199647	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	54	2183	example_2018-09-03T11-31-38/root/const_addint_right_hand_0	2
13135	example_2018-09-03T11-17-35/root/sink1/example___sink1___s2___0			2018-09-03 09:17:38.446768	2018-09-03 09:18:07.70594	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	53	2176	example_2018-09-03T11-17-35/root/sink1	2
13136	example_2018-09-03T11-17-35/root/sink1/example___sink1___s3___0			2018-09-03 09:17:38.564047	2018-09-03 09:18:13.166293	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	53	2176	example_2018-09-03T11-17-35/root/sink1	2
13140	example_2018-09-03T11-31-38/root/source1/example___source1___s3			2018-09-03 09:31:39.62067	2018-09-03 09:31:39.752686	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	54	2180	example_2018-09-03T11-31-38/root/source1	2
13137	example_2018-09-03T11-17-35/root/sink1/example___sink1___s4___0			2018-09-03 09:17:38.696966	2018-09-03 09:18:17.609802	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	53	2176	example_2018-09-03T11-17-35/root/sink1	2
13130	example_2018-09-03T11-17-35/root/addint/example___addint___s1			2018-09-03 09:17:37.733379	2018-09-03 09:17:46.107567	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	53	2177	example_2018-09-03T11-17-35/root/addint	2
13145	example_2018-09-03T11-31-38/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 09:31:40.786769	2018-09-03 09:31:40.90019	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	54	2183	example_2018-09-03T11-31-38/root/const_addint_right_hand_0	2
13131	example_2018-09-03T11-17-35/root/addint/example___addint___s2			2018-09-03 09:17:37.865489	2018-09-03 09:17:51.065319	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	53	2177	example_2018-09-03T11-17-35/root/addint	2
13132	example_2018-09-03T11-17-35/root/addint/example___addint___s3			2018-09-03 09:17:38.00226	2018-09-03 09:17:54.474089	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	53	2177	example_2018-09-03T11-17-35/root/addint	2
13144	example_2018-09-03T11-31-38/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 09:31:40.552915	2018-09-03 09:31:40.669635	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	54	2183	example_2018-09-03T11-31-38/root/const_addint_right_hand_0	2
13141	example_2018-09-03T11-31-38/root/source1/example___source1___s4			2018-09-03 09:31:39.854923	2018-09-03 09:31:39.971403	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	54	2180	example_2018-09-03T11-31-38/root/source1	2
13138	example_2018-09-03T11-31-38/root/source1/example___source1___s1			2018-09-03 09:31:39.17367	2018-09-03 09:31:39.285956	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	54	2180	example_2018-09-03T11-31-38/root/source1	2
13143	example_2018-09-03T11-31-38/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 09:31:40.31467	2018-09-03 09:31:40.437368	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	54	2183	example_2018-09-03T11-31-38/root/const_addint_right_hand_0	2
13146	example_2018-09-03T11-31-38/root/addint/example___addint___s1			2018-09-03 09:31:41.017967	2018-09-03 09:31:45.421457	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	54	2182	example_2018-09-03T11-31-38/root/addint	2
13151	example_2018-09-03T11-31-38/root/sink1/example___sink1___s2___0			2018-09-03 09:31:41.584703	2018-09-03 09:32:23.450431	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	54	2181	example_2018-09-03T11-31-38/root/sink1	2
13147	example_2018-09-03T11-31-38/root/addint/example___addint___s2			2018-09-03 09:31:41.131497	2018-09-03 09:31:49.151628	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	54	2182	example_2018-09-03T11-31-38/root/addint	2
13148	example_2018-09-03T11-31-38/root/addint/example___addint___s3			2018-09-03 09:31:41.237606	2018-09-03 09:31:52.848432	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	54	2182	example_2018-09-03T11-31-38/root/addint	2
13150	example_2018-09-03T11-31-38/root/sink1/example___sink1___s1___0			2018-09-03 09:31:41.469883	2018-09-03 09:32:10.02918	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	54	2181	example_2018-09-03T11-31-38/root/sink1	2
13152	example_2018-09-03T11-31-38/root/sink1/example___sink1___s3___0			2018-09-03 09:31:41.696589	2018-09-03 09:32:37.405882	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	54	2181	example_2018-09-03T11-31-38/root/sink1	2
13149	example_2018-09-03T11-31-38/root/addint/example___addint___s4			2018-09-03 09:31:41.35172	2018-09-03 09:31:57.380031	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	54	2182	example_2018-09-03T11-31-38/root/addint	2
13159	auto_prefix_test_2018-09-03T11-34-05/root/m_p/auto_prefix_test___m_p___id_1			2018-09-03 09:34:08.056991	2018-09-03 09:34:29.997654	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	55	2187	auto_prefix_test_2018-09-03T11-34-05/root/m_p	2
13153	example_2018-09-03T11-31-38/root/sink1/example___sink1___s4___0			2018-09-03 09:31:41.815721	2018-09-03 09:32:50.244913	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	54	2181	example_2018-09-03T11-31-38/root/sink1	2
13170	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_p/auto_prefix_test___sink_m_p___id_0___0			2018-09-03 09:34:09.782065	2018-09-03 09:35:54.956325	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	55	2191	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_p	2
13161	auto_prefix_test_2018-09-03T11-34-05/root/a_p/auto_prefix_test___a_p___id_0			2018-09-03 09:34:08.345551	2018-09-03 09:34:48.953868	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	55	2188	auto_prefix_test_2018-09-03T11-34-05/root/a_p	2
13154	auto_prefix_test_2018-09-03T11-34-05/root/source/auto_prefix_test___source___id_0			2018-09-03 09:34:06.840993	2018-09-03 09:34:06.972166	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	55	2186	auto_prefix_test_2018-09-03T11-34-05/root/source	2
13166	auto_prefix_test_2018-09-03T11-34-05/root/m_n/auto_prefix_test___m_n___id_2			2018-09-03 09:34:09.142735	2018-09-03 09:35:27.065018	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	55	2189	auto_prefix_test_2018-09-03T11-34-05/root/m_n	2
13155	auto_prefix_test_2018-09-03T11-34-05/root/const/auto_prefix_test___const___id_0			2018-09-03 09:34:07.104749	2018-09-03 09:34:07.225143	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	55	2185	auto_prefix_test_2018-09-03T11-34-05/root/const	2
13165	auto_prefix_test_2018-09-03T11-34-05/root/m_n/auto_prefix_test___m_n___id_1			2018-09-03 09:34:08.954781	2018-09-03 09:35:20.35259	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	55	2189	auto_prefix_test_2018-09-03T11-34-05/root/m_n	2
13156	auto_prefix_test_2018-09-03T11-34-05/root/const/auto_prefix_test___const___id_1			2018-09-03 09:34:07.357396	2018-09-03 09:34:07.485391	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	55	2185	auto_prefix_test_2018-09-03T11-34-05/root/const	2
13162	auto_prefix_test_2018-09-03T11-34-05/root/a_p/auto_prefix_test___a_p___id_1			2018-09-03 09:34:08.488067	2018-09-03 09:34:56.815509	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	55	2188	auto_prefix_test_2018-09-03T11-34-05/root/a_p	2
13157	auto_prefix_test_2018-09-03T11-34-05/root/const/auto_prefix_test___const___id_2			2018-09-03 09:34:07.622262	2018-09-03 09:34:07.756803	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	55	2185	auto_prefix_test_2018-09-03T11-34-05/root/const	2
13158	auto_prefix_test_2018-09-03T11-34-05/root/m_p/auto_prefix_test___m_p___id_0			2018-09-03 09:34:07.911989	2018-09-03 09:34:21.250734	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	55	2187	auto_prefix_test_2018-09-03T11-34-05/root/m_p	2
13163	auto_prefix_test_2018-09-03T11-34-05/root/a_p/auto_prefix_test___a_p___id_2			2018-09-03 09:34:08.634805	2018-09-03 09:35:07.175667	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	55	2188	auto_prefix_test_2018-09-03T11-34-05/root/a_p	2
13167	auto_prefix_test_2018-09-03T11-34-05/root/a_n/auto_prefix_test___a_n___id_0			2018-09-03 09:34:09.342245	2018-09-03 09:35:34.854817	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	55	2190	auto_prefix_test_2018-09-03T11-34-05/root/a_n	2
13178	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_n/auto_prefix_test___sink_m_n___id_2___0			2018-09-03 09:34:10.945604	2018-09-03 09:36:40.116521	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	55	2193	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_n	2
13164	auto_prefix_test_2018-09-03T11-34-05/root/m_n/auto_prefix_test___m_n___id_0			2018-09-03 09:34:08.792443	2018-09-03 09:35:14.114612	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	55	2189	auto_prefix_test_2018-09-03T11-34-05/root/m_n	2
13173	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_p/auto_prefix_test___sink_a_p___id_0___0			2018-09-03 09:34:10.191645	2018-09-03 09:36:10.627379	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	55	2192	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_p	2
13176	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_n/auto_prefix_test___sink_m_n___id_0___0			2018-09-03 09:34:10.604517	2018-09-03 09:36:28.23747	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	55	2193	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_n	2
13168	auto_prefix_test_2018-09-03T11-34-05/root/a_n/auto_prefix_test___a_n___id_1			2018-09-03 09:34:09.478275	2018-09-03 09:35:41.2967	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	55	2190	auto_prefix_test_2018-09-03T11-34-05/root/a_n	2
13171	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_p/auto_prefix_test___sink_m_p___id_1___0			2018-09-03 09:34:09.931257	2018-09-03 09:35:59.540363	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	55	2191	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_p	2
13169	auto_prefix_test_2018-09-03T11-34-05/root/a_n/auto_prefix_test___a_n___id_2			2018-09-03 09:34:09.634768	2018-09-03 09:35:47.474944	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	55	2190	auto_prefix_test_2018-09-03T11-34-05/root/a_n	2
13175	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_p/auto_prefix_test___sink_a_p___id_2___0			2018-09-03 09:34:10.464657	2018-09-03 09:36:22.344474	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	55	2192	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_p	2
13172	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_p/auto_prefix_test___sink_m_p___id_2___0			2018-09-03 09:34:10.067032	2018-09-03 09:36:05.040616	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	55	2191	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_p	2
13174	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_p/auto_prefix_test___sink_a_p___id_1___0			2018-09-03 09:34:10.31957	2018-09-03 09:36:16.491369	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	55	2192	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_p	2
13691	iris/root/2_hammers_atlas/50_hammers_brains/job___001			2018-09-08 13:18:14.68438	2018-09-08 13:19:15.199075	{}	75	2410	iris/root/2_hammers_atlas/50_hammers_brains	2
13177	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_n/auto_prefix_test___sink_m_n___id_1___0			2018-09-03 09:34:10.784337	2018-09-03 09:36:34.220805	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	55	2193	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_n	2
13181	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_n/auto_prefix_test___sink_a_n___id_2___0			2018-09-03 09:34:11.361996	2018-09-03 09:36:55.276221	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	55	2194	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_n	2
13179	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_n/auto_prefix_test___sink_a_n___id_0___0			2018-09-03 09:34:11.081207	2018-09-03 09:36:46.119527	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	55	2194	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_n	2
13160	auto_prefix_test_2018-09-03T11-34-05/root/m_p/auto_prefix_test___m_p___id_2			2018-09-03 09:34:08.188553	2018-09-03 09:34:39.359386	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	55	2187	auto_prefix_test_2018-09-03T11-34-05/root/m_p	2
13180	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_n/auto_prefix_test___sink_a_n___id_1___0			2018-09-03 09:34:11.226293	2018-09-03 09:36:50.88269	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	55	2194	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_n	2
12925	failing_network_2018-08-24T15-34-44/root/sink_1/failing_network___sink_1___sample_1_3___0			2018-08-24 13:35:03.687147	2018-08-24 13:35:19.761925	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	42	2112	failing_network_2018-08-24T15-34-44/root/sink_1	4
12905	failing_network_2018-08-24T15-34-44/root/source_3/failing_network___source_3___sample_1			2018-08-24 13:34:45.237684	2018-08-24 13:35:02.519092	{"sample_id": ["sample_1"], "sample_index": [0], "errors": []}	42	2111	failing_network_2018-08-24T15-34-44/root/source_3	2
12916	failing_network_2018-08-24T15-34-44/root/step_1/failing_network___step_1___sample_1_2			2018-08-24 13:35:02.89781	2018-08-24 13:35:16.140322	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	42	2117	failing_network_2018-08-24T15-34-44/root/step_1	2
12906	failing_network_2018-08-24T15-34-44/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_0			2018-08-24 13:34:45.323882	2018-08-24 13:34:45.407827	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	42	2122	failing_network_2018-08-24T15-34-44/root/const_step_1_fail_2_0	2
12928	failing_network_2018-08-24T15-34-44/root/step_3/failing_network___step_3___sample_1_2			2018-08-24 13:35:03.933646	2018-08-24 13:35:30.166676	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	42	2119	failing_network_2018-08-24T15-34-44/root/step_3	4
12917	failing_network_2018-08-24T15-34-44/root/step_1/failing_network___step_1___sample_1_3			2018-08-24 13:35:02.981466	2018-08-24 13:35:19.686421	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	42	2117	failing_network_2018-08-24T15-34-44/root/step_1	3
12927	failing_network_2018-08-24T15-34-44/root/step_3/failing_network___step_3___sample_1_1			2018-08-24 13:35:03.854382	2018-08-24 13:35:26.551517	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	42	2119	failing_network_2018-08-24T15-34-44/root/step_3	4
12918	failing_network_2018-08-24T15-34-44/root/step_2/failing_network___step_2___sample_1_0			2018-08-24 13:35:03.072802	2018-08-24 13:35:23.120584	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2118	failing_network_2018-08-24T15-34-44/root/step_2	2
12919	failing_network_2018-08-24T15-34-44/root/step_2/failing_network___step_2___sample_1_1			2018-08-24 13:35:03.153582	2018-08-24 13:35:26.464966	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	42	2118	failing_network_2018-08-24T15-34-44/root/step_2	2
12929	failing_network_2018-08-24T15-34-44/root/step_3/failing_network___step_3___sample_1_3			2018-08-24 13:35:04.01533	2018-08-24 13:35:33.739133	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	42	2119	failing_network_2018-08-24T15-34-44/root/step_3	4
12923	failing_network_2018-08-24T15-34-44/root/sink_1/failing_network___sink_1___sample_1_1___0			2018-08-24 13:35:03.519848	2018-08-24 13:35:12.501857	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	42	2112	failing_network_2018-08-24T15-34-44/root/sink_1	4
12920	failing_network_2018-08-24T15-34-44/root/step_2/failing_network___step_2___sample_1_2			2018-08-24 13:35:03.23716	2018-08-24 13:35:30.086367	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	42	2118	failing_network_2018-08-24T15-34-44/root/step_2	3
12921	failing_network_2018-08-24T15-34-44/root/step_2/failing_network___step_2___sample_1_3			2018-08-24 13:35:03.317464	2018-08-24 13:35:33.664311	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	42	2118	failing_network_2018-08-24T15-34-44/root/step_2	3
12926	failing_network_2018-08-24T15-34-44/root/step_3/failing_network___step_3___sample_1_0			2018-08-24 13:35:03.775827	2018-08-24 13:35:45.793755	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2119	failing_network_2018-08-24T15-34-44/root/step_3	2
12914	failing_network_2018-08-24T15-34-44/root/step_1/failing_network___step_1___sample_1_0			2018-08-24 13:35:02.739603	2018-08-24 13:35:08.790993	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2117	failing_network_2018-08-24T15-34-44/root/step_1	2
12904	failing_network_2018-08-24T15-34-44/root/source_2/failing_network___source_2___sample_1			2018-08-24 13:34:45.158447	2018-08-24 13:34:58.163349	{"sample_id": ["sample_1"], "sample_index": [0], "errors": []}	42	2110	failing_network_2018-08-24T15-34-44/root/source_2	2
12922	failing_network_2018-08-24T15-34-44/root/sink_1/failing_network___sink_1___sample_1_0___0			2018-08-24 13:35:03.40639	2018-08-24 13:35:38.156668	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2112	failing_network_2018-08-24T15-34-44/root/sink_1	2
12924	failing_network_2018-08-24T15-34-44/root/sink_1/failing_network___sink_1___sample_1_2___0			2018-08-24 13:35:03.599977	2018-08-24 13:35:42.511277	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	42	2112	failing_network_2018-08-24T15-34-44/root/sink_1	2
12903	failing_network_2018-08-24T15-34-44/root/source_1/failing_network___source_1___sample_1			2018-08-24 13:34:45.082929	2018-08-24 13:34:52.593767	{"sample_id": ["sample_1"], "sample_index": [0], "errors": []}	42	2109	failing_network_2018-08-24T15-34-44/root/source_1	2
13688	iris/root/5_BET_and_REG/59_const_t1w_registration_threads_0/job___002			2018-09-08 13:18:14.68438	2018-09-08 13:18:18.767573	{}	75	2440	iris/root/5_BET_and_REG/59_const_t1w_registration_threads_0	2
12915	failing_network_2018-08-24T15-34-44/root/step_1/failing_network___step_1___sample_1_1			2018-08-24 13:35:02.81805	2018-08-24 13:35:12.425141	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	42	2117	failing_network_2018-08-24T15-34-44/root/step_1	3
12910	failing_network_2018-08-24T15-34-44/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_0			2018-08-24 13:34:45.99675	2018-08-24 13:34:46.081998	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	42	2123	failing_network_2018-08-24T15-34-44/root/const_step_2_fail_1_0	2
12908	failing_network_2018-08-24T15-34-44/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_2			2018-08-24 13:34:45.653444	2018-08-24 13:34:45.740628	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	42	2122	failing_network_2018-08-24T15-34-44/root/const_step_1_fail_2_0	2
12907	failing_network_2018-08-24T15-34-44/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_1			2018-08-24 13:34:45.489672	2018-08-24 13:34:45.570794	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	42	2122	failing_network_2018-08-24T15-34-44/root/const_step_1_fail_2_0	2
12909	failing_network_2018-08-24T15-34-44/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_3			2018-08-24 13:34:45.828872	2018-08-24 13:34:45.910169	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	42	2122	failing_network_2018-08-24T15-34-44/root/const_step_1_fail_2_0	2
12912	failing_network_2018-08-24T15-34-44/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_2			2018-08-24 13:34:46.378375	2018-08-24 13:34:46.457407	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	42	2123	failing_network_2018-08-24T15-34-44/root/const_step_2_fail_1_0	2
12911	failing_network_2018-08-24T15-34-44/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_1			2018-08-24 13:34:46.167238	2018-08-24 13:34:46.253801	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	42	2123	failing_network_2018-08-24T15-34-44/root/const_step_2_fail_1_0	2
12913	failing_network_2018-08-24T15-34-44/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_3			2018-08-24 13:34:46.537556	2018-08-24 13:34:46.619394	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	42	2123	failing_network_2018-08-24T15-34-44/root/const_step_2_fail_1_0	2
13188	example_2018-09-03T12-17-58/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 10:17:59.997345	2018-09-03 10:18:00.104812	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	56	2199	example_2018-09-03T12-17-58/root/const_addint_right_hand_0	2
13182	example_2018-09-03T12-17-58/root/source1/example___source1___s1			2018-09-03 10:17:58.626756	2018-09-03 10:17:58.747397	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	56	2196	example_2018-09-03T12-17-58/root/source1	2
12935	failing_network_2018-08-24T15-34-44/root/range/failing_network___range___sample_1_1			2018-08-24 13:35:04.551676	2018-08-24 13:35:26.650587	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	42	2120	failing_network_2018-08-24T15-34-44/root/range	4
13186	example_2018-09-03T12-17-58/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 10:17:59.557409	2018-09-03 10:17:59.666836	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	56	2199	example_2018-09-03T12-17-58/root/const_addint_right_hand_0	2
13183	example_2018-09-03T12-17-58/root/source1/example___source1___s2			2018-09-03 10:17:58.868292	2018-09-03 10:17:58.985701	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	56	2196	example_2018-09-03T12-17-58/root/source1	2
12939	failing_network_2018-08-24T15-34-44/root/sink_3/failing_network___sink_3___sample_1_1___0			2018-08-24 13:35:04.897185	2018-08-24 13:35:26.745389	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	42	2114	failing_network_2018-08-24T15-34-44/root/sink_3	4
13184	example_2018-09-03T12-17-58/root/source1/example___source1___s3			2018-09-03 10:17:59.101018	2018-09-03 10:17:59.213539	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	56	2196	example_2018-09-03T12-17-58/root/source1	2
12936	failing_network_2018-08-24T15-34-44/root/range/failing_network___range___sample_1_2			2018-08-24 13:35:04.635538	2018-08-24 13:35:30.251935	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	42	2120	failing_network_2018-08-24T15-34-44/root/range	4
12940	failing_network_2018-08-24T15-34-44/root/sink_3/failing_network___sink_3___sample_1_2___0			2018-08-24 13:35:04.981737	2018-08-24 13:35:30.379059	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	42	2114	failing_network_2018-08-24T15-34-44/root/sink_3	4
12932	failing_network_2018-08-24T15-34-44/root/sink_2/failing_network___sink_2___sample_1_2___0			2018-08-24 13:35:04.282783	2018-08-24 13:35:30.470635	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	42	2113	failing_network_2018-08-24T15-34-44/root/sink_2	4
12937	failing_network_2018-08-24T15-34-44/root/range/failing_network___range___sample_1_3			2018-08-24 13:35:04.720208	2018-08-24 13:35:33.822381	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	42	2120	failing_network_2018-08-24T15-34-44/root/range	4
12941	failing_network_2018-08-24T15-34-44/root/sink_3/failing_network___sink_3___sample_1_3___0			2018-08-24 13:35:05.066169	2018-08-24 13:35:33.905502	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	42	2114	failing_network_2018-08-24T15-34-44/root/sink_3	4
12933	failing_network_2018-08-24T15-34-44/root/sink_2/failing_network___sink_2___sample_1_3___0			2018-08-24 13:35:04.381557	2018-08-24 13:35:33.987244	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	42	2113	failing_network_2018-08-24T15-34-44/root/sink_2	4
13187	example_2018-09-03T12-17-58/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 10:17:59.773774	2018-09-03 10:17:59.888198	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	56	2199	example_2018-09-03T12-17-58/root/const_addint_right_hand_0	2
12930	failing_network_2018-08-24T15-34-44/root/sink_2/failing_network___sink_2___sample_1_0___0			2018-08-24 13:35:04.101473	2018-08-24 13:35:50.083012	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2113	failing_network_2018-08-24T15-34-44/root/sink_2	2
13185	example_2018-09-03T12-17-58/root/source1/example___source1___s4			2018-09-03 10:17:59.333692	2018-09-03 10:17:59.449477	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	56	2196	example_2018-09-03T12-17-58/root/source1	2
12931	failing_network_2018-08-24T15-34-44/root/sink_2/failing_network___sink_2___sample_1_1___0			2018-08-24 13:35:04.189182	2018-08-24 13:35:54.423694	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	42	2113	failing_network_2018-08-24T15-34-44/root/sink_2	2
12934	failing_network_2018-08-24T15-34-44/root/range/failing_network___range___sample_1_0			2018-08-24 13:35:04.467072	2018-08-24 13:35:57.669369	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2120	failing_network_2018-08-24T15-34-44/root/range	2
12938	failing_network_2018-08-24T15-34-44/root/sink_3/failing_network___sink_3___sample_1_0___0			2018-08-24 13:35:04.809986	2018-08-24 13:36:01.917741	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	42	2114	failing_network_2018-08-24T15-34-44/root/sink_3	2
13197	example_2018-09-03T12-17-58/root/sink1/example___sink1___s4___0			2018-09-03 10:18:01.253541	2018-09-03 10:18:34.196924	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	56	2197	example_2018-09-03T12-17-58/root/sink1	2
13189	example_2018-09-03T12-17-58/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 10:18:00.209958	2018-09-03 10:18:00.323239	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	56	2199	example_2018-09-03T12-17-58/root/const_addint_right_hand_0	2
13195	example_2018-09-03T12-17-58/root/sink1/example___sink1___s2___0			2018-09-03 10:18:01.018754	2018-09-03 10:18:24.216486	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	56	2197	example_2018-09-03T12-17-58/root/sink1	2
13190	example_2018-09-03T12-17-58/root/addint/example___addint___s1			2018-09-03 10:18:00.436895	2018-09-03 10:18:04.844792	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	56	2198	example_2018-09-03T12-17-58/root/addint	2
13193	example_2018-09-03T12-17-58/root/addint/example___addint___s4			2018-09-03 10:18:00.778727	2018-09-03 10:18:15.160214	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	56	2198	example_2018-09-03T12-17-58/root/addint	2
13191	example_2018-09-03T12-17-58/root/addint/example___addint___s2			2018-09-03 10:18:00.563857	2018-09-03 10:18:08.375001	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	56	2198	example_2018-09-03T12-17-58/root/addint	2
13194	example_2018-09-03T12-17-58/root/sink1/example___sink1___s1___0			2018-09-03 10:18:00.905644	2018-09-03 10:18:19.826682	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	56	2197	example_2018-09-03T12-17-58/root/sink1	2
13192	example_2018-09-03T12-17-58/root/addint/example___addint___s3			2018-09-03 10:18:00.672814	2018-09-03 10:18:11.738246	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	56	2198	example_2018-09-03T12-17-58/root/addint	2
13196	example_2018-09-03T12-17-58/root/sink1/example___sink1___s3___0			2018-09-03 10:18:01.137751	2018-09-03 10:18:28.827966	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	56	2197	example_2018-09-03T12-17-58/root/sink1	2
13198	example_2018-09-03T12-31-11/root/source1/example___source1___s1			2018-09-03 10:31:12.226186	2018-09-03 10:31:12.342431	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	57	2201	example_2018-09-03T12-31-11/root/source1	2
13199	example_2018-09-03T12-31-11/root/source1/example___source1___s2			2018-09-03 10:31:12.452097	2018-09-03 10:31:12.566364	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	57	2201	example_2018-09-03T12-31-11/root/source1	2
13221	example_2018-09-03T12-47-05/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 10:47:07.907297	2018-09-03 10:47:08.013386	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	58	2209	example_2018-09-03T12-47-05/root/const_addint_right_hand_0	2
13223	example_2018-09-03T12-47-05/root/addint/example___addint___s2			2018-09-03 10:47:08.227635	2018-09-03 10:47:16.003974	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	58	2208	example_2018-09-03T12-47-05/root/addint	2
13224	example_2018-09-03T12-47-05/root/addint/example___addint___s3			2018-09-03 10:47:08.346339	2018-09-03 10:47:19.747918	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	58	2208	example_2018-09-03T12-47-05/root/addint	2
13225	example_2018-09-03T12-47-05/root/addint/example___addint___s4			2018-09-03 10:47:08.468747	2018-09-03 10:47:23.909914	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	58	2208	example_2018-09-03T12-47-05/root/addint	2
13226	example_2018-09-03T12-47-05/root/sink1/example___sink1___s1___0			2018-09-03 10:47:08.581883	2018-09-03 10:47:28.773512	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	58	2207	example_2018-09-03T12-47-05/root/sink1	2
13227	example_2018-09-03T12-47-05/root/sink1/example___sink1___s2___0			2018-09-03 10:47:08.681009	2018-09-03 10:47:34.998201	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	58	2207	example_2018-09-03T12-47-05/root/sink1	2
13228	example_2018-09-03T12-47-05/root/sink1/example___sink1___s3___0			2018-09-03 10:47:08.783503	2018-09-03 10:47:41.560098	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	58	2207	example_2018-09-03T12-47-05/root/sink1	2
13229	example_2018-09-03T12-47-05/root/sink1/example___sink1___s4___0			2018-09-03 10:47:08.887977	2018-09-03 10:47:49.195573	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	58	2207	example_2018-09-03T12-47-05/root/sink1	2
13222	example_2018-09-03T12-47-05/root/addint/example___addint___s1			2018-09-03 10:47:08.125473	2018-09-03 10:47:12.583579	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	58	2208	example_2018-09-03T12-47-05/root/addint	2
13237	example_2018-09-03T12-54-57/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 10:54:58.946088	2018-09-03 10:54:59.054946	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	59	2214	example_2018-09-03T12-54-57/root/const_addint_right_hand_0	2
13231	example_2018-09-03T12-54-57/root/source1/example___source1___s2			2018-09-03 10:54:57.660404	2018-09-03 10:54:57.766782	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	59	2211	example_2018-09-03T12-54-57/root/source1	2
13232	example_2018-09-03T12-54-57/root/source1/example___source1___s3			2018-09-03 10:54:57.873335	2018-09-03 10:54:57.986425	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	59	2211	example_2018-09-03T12-54-57/root/source1	2
13244	example_2018-09-03T12-54-57/root/sink1/example___sink1___s3___0			2018-09-03 10:54:59.840116	2018-09-03 10:55:28.354423	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	59	2212	example_2018-09-03T12-54-57/root/sink1	2
13233	example_2018-09-03T12-54-57/root/source1/example___source1___s4			2018-09-03 10:54:58.098245	2018-09-03 10:54:58.215237	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	59	2211	example_2018-09-03T12-54-57/root/source1	2
13245	example_2018-09-03T12-54-57/root/sink1/example___sink1___s4___0			2018-09-03 10:54:59.939167	2018-09-03 10:55:32.742661	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	59	2212	example_2018-09-03T12-54-57/root/sink1	2
13234	example_2018-09-03T12-54-57/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 10:54:58.328287	2018-09-03 10:54:58.434877	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	59	2214	example_2018-09-03T12-54-57/root/const_addint_right_hand_0	2
13254	example_2018-09-03T12-55-46/root/addint/example___addint___s1			2018-09-03 10:55:49.066956	2018-09-03 10:55:53.644577	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	60	2218	example_2018-09-03T12-55-46/root/addint	2
13235	example_2018-09-03T12-54-57/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 10:54:58.532534	2018-09-03 10:54:58.631157	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	59	2214	example_2018-09-03T12-54-57/root/const_addint_right_hand_0	2
13251	example_2018-09-03T12-55-46/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 10:55:48.391297	2018-09-03 10:55:48.501456	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	60	2219	example_2018-09-03T12-55-46/root/const_addint_right_hand_0	2
13236	example_2018-09-03T12-54-57/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 10:54:58.732616	2018-09-03 10:54:58.839473	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	59	2214	example_2018-09-03T12-54-57/root/const_addint_right_hand_0	2
13246	example_2018-09-03T12-55-46/root/source1/example___source1___s1			2018-09-03 10:55:47.097315	2018-09-03 10:55:47.222041	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	60	2216	example_2018-09-03T12-55-46/root/source1	2
13249	example_2018-09-03T12-55-46/root/source1/example___source1___s4			2018-09-03 10:55:47.889277	2018-09-03 10:55:48.012831	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	60	2216	example_2018-09-03T12-55-46/root/source1	2
13238	example_2018-09-03T12-54-57/root/addint/example___addint___s1			2018-09-03 10:54:59.186938	2018-09-03 10:55:04.385499	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	59	2213	example_2018-09-03T12-54-57/root/addint	2
13247	example_2018-09-03T12-55-46/root/source1/example___source1___s2			2018-09-03 10:55:47.364497	2018-09-03 10:55:47.477612	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	60	2216	example_2018-09-03T12-55-46/root/source1	2
13239	example_2018-09-03T12-54-57/root/addint/example___addint___s2			2018-09-03 10:54:59.292599	2018-09-03 10:55:08.20772	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	59	2213	example_2018-09-03T12-54-57/root/addint	2
13240	example_2018-09-03T12-54-57/root/addint/example___addint___s3			2018-09-03 10:54:59.409952	2018-09-03 10:55:11.651187	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	59	2213	example_2018-09-03T12-54-57/root/addint	2
13241	example_2018-09-03T12-54-57/root/addint/example___addint___s4			2018-09-03 10:54:59.518113	2018-09-03 10:55:15.063156	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	59	2213	example_2018-09-03T12-54-57/root/addint	2
13248	example_2018-09-03T12-55-46/root/source1/example___source1___s3			2018-09-03 10:55:47.616305	2018-09-03 10:55:47.739465	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	60	2216	example_2018-09-03T12-55-46/root/source1	2
13242	example_2018-09-03T12-54-57/root/sink1/example___sink1___s1___0			2018-09-03 10:54:59.631665	2018-09-03 10:55:19.557945	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	59	2212	example_2018-09-03T12-54-57/root/sink1	2
13250	example_2018-09-03T12-55-46/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 10:55:48.159071	2018-09-03 10:55:48.274409	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	60	2219	example_2018-09-03T12-55-46/root/const_addint_right_hand_0	2
13243	example_2018-09-03T12-54-57/root/sink1/example___sink1___s2___0			2018-09-03 10:54:59.735433	2018-09-03 10:55:23.975926	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	59	2212	example_2018-09-03T12-54-57/root/sink1	2
13253	example_2018-09-03T12-55-46/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 10:55:48.84076	2018-09-03 10:55:48.95478	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	60	2219	example_2018-09-03T12-55-46/root/const_addint_right_hand_0	2
13252	example_2018-09-03T12-55-46/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 10:55:48.631889	2018-09-03 10:55:48.735173	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	60	2219	example_2018-09-03T12-55-46/root/const_addint_right_hand_0	2
13255	example_2018-09-03T12-55-46/root/addint/example___addint___s2			2018-09-03 10:55:49.193865	2018-09-03 10:55:57.189414	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	60	2218	example_2018-09-03T12-55-46/root/addint	2
13256	example_2018-09-03T12-55-46/root/addint/example___addint___s3			2018-09-03 10:55:49.305875	2018-09-03 10:56:00.629154	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	60	2218	example_2018-09-03T12-55-46/root/addint	2
13260	example_2018-09-03T12-55-46/root/sink1/example___sink1___s3___0			2018-09-03 10:55:49.777489	2018-09-03 10:56:19.7066	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	60	2217	example_2018-09-03T12-55-46/root/sink1	2
13258	example_2018-09-03T12-55-46/root/sink1/example___sink1___s1___0			2018-09-03 10:55:49.535734	2018-09-03 10:56:08.577736	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	60	2217	example_2018-09-03T12-55-46/root/sink1	2
13259	example_2018-09-03T12-55-46/root/sink1/example___sink1___s2___0			2018-09-03 10:55:49.650168	2018-09-03 10:56:14.169499	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	60	2217	example_2018-09-03T12-55-46/root/sink1	2
13257	example_2018-09-03T12-55-46/root/addint/example___addint___s4			2018-09-03 10:55:49.415016	2018-09-03 10:56:04.018681	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	60	2218	example_2018-09-03T12-55-46/root/addint	2
13261	example_2018-09-03T12-55-46/root/sink1/example___sink1___s4___0			2018-09-03 10:55:49.88659	2018-09-03 10:56:24.35088	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	60	2217	example_2018-09-03T12-55-46/root/sink1	2
13262	example_2018-09-03T13-09-35/root/source1/example___source1___s1			2018-09-03 11:09:36.339485	2018-09-03 11:09:36.441071	{"sample_index": [0], "sample_id": ["s1"], "errors": []}	61	2223	example_2018-09-03T13-09-35/root/source1	2
13275	example_2018-09-03T13-09-35/root/sink1/example___sink1___s2___0			2018-09-03 11:09:38.609517	2018-09-03 11:10:03.207848	{"sample_index": [1], "sample_id": ["s2"], "errors": []}	61	2221	example_2018-09-03T13-09-35/root/sink1	2
13263	example_2018-09-03T13-09-35/root/source1/example___source1___s2			2018-09-03 11:09:36.546575	2018-09-03 11:09:36.648732	{"sample_index": [1], "sample_id": ["s2"], "errors": []}	61	2223	example_2018-09-03T13-09-35/root/source1	2
13269	example_2018-09-03T13-09-35/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 11:09:37.85756	2018-09-03 11:09:37.964124	{"sample_index": [3], "sample_id": ["id_3"], "errors": []}	61	2224	example_2018-09-03T13-09-35/root/const_addint_right_hand_0	2
13264	example_2018-09-03T13-09-35/root/source1/example___source1___s3			2018-09-03 11:09:36.750572	2018-09-03 11:09:36.867237	{"sample_index": [2], "sample_id": ["s3"], "errors": []}	61	2223	example_2018-09-03T13-09-35/root/source1	2
13282	example_2018-09-03T13-11-45/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 11:11:46.546261	2018-09-03 11:11:46.662813	{"errors": [], "sample_id": ["id_0"], "sample_index": [0]}	62	2229	example_2018-09-03T13-11-45/root/const_addint_right_hand_0	2
13265	example_2018-09-03T13-09-35/root/source1/example___source1___s4			2018-09-03 11:09:36.971285	2018-09-03 11:09:37.077238	{"sample_index": [3], "sample_id": ["s4"], "errors": []}	61	2223	example_2018-09-03T13-09-35/root/source1	2
13266	example_2018-09-03T13-09-35/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 11:09:37.182019	2018-09-03 11:09:37.289328	{"sample_index": [0], "sample_id": ["id_0"], "errors": []}	61	2224	example_2018-09-03T13-09-35/root/const_addint_right_hand_0	2
13276	example_2018-09-03T13-09-35/root/sink1/example___sink1___s3___0			2018-09-03 11:09:38.717659	2018-09-03 11:10:08.70189	{"sample_index": [2], "sample_id": ["s3"], "errors": []}	61	2221	example_2018-09-03T13-09-35/root/sink1	2
13267	example_2018-09-03T13-09-35/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 11:09:37.400434	2018-09-03 11:09:37.527247	{"sample_index": [1], "sample_id": ["id_1"], "errors": []}	61	2224	example_2018-09-03T13-09-35/root/const_addint_right_hand_0	2
13280	example_2018-09-03T13-11-45/root/source1/example___source1___s3			2018-09-03 11:11:46.116002	2018-09-03 11:11:46.224876	{"errors": [], "sample_id": ["s3"], "sample_index": [2]}	62	2227	example_2018-09-03T13-11-45/root/source1	2
13277	example_2018-09-03T13-09-35/root/sink1/example___sink1___s4___0			2018-09-03 11:09:38.828975	2018-09-03 11:10:14.342541	{"sample_index": [3], "sample_id": ["s4"], "errors": []}	61	2221	example_2018-09-03T13-09-35/root/sink1	2
13268	example_2018-09-03T13-09-35/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 11:09:37.631571	2018-09-03 11:09:37.743238	{"sample_index": [2], "sample_id": ["id_2"], "errors": []}	61	2224	example_2018-09-03T13-09-35/root/const_addint_right_hand_0	2
13285	example_2018-09-03T13-11-45/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 11:11:47.254377	2018-09-03 11:11:47.364482	{"errors": [], "sample_id": ["id_3"], "sample_index": [3]}	62	2229	example_2018-09-03T13-11-45/root/const_addint_right_hand_0	2
13284	example_2018-09-03T13-11-45/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 11:11:47.016886	2018-09-03 11:11:47.134839	{"errors": [], "sample_id": ["id_2"], "sample_index": [2]}	62	2229	example_2018-09-03T13-11-45/root/const_addint_right_hand_0	2
13278	example_2018-09-03T13-11-45/root/source1/example___source1___s1			2018-09-03 11:11:45.656464	2018-09-03 11:11:45.770773	{"errors": [], "sample_id": ["s1"], "sample_index": [0]}	62	2227	example_2018-09-03T13-11-45/root/source1	2
13270	example_2018-09-03T13-09-35/root/addint/example___addint___s1			2018-09-03 11:09:38.07798	2018-09-03 11:09:42.508799	{"sample_index": [0], "sample_id": ["s1"], "errors": []}	61	2222	example_2018-09-03T13-09-35/root/addint	2
13281	example_2018-09-03T13-11-45/root/source1/example___source1___s4			2018-09-03 11:11:46.330195	2018-09-03 11:11:46.438912	{"errors": [], "sample_id": ["s4"], "sample_index": [3]}	62	2227	example_2018-09-03T13-11-45/root/source1	2
13271	example_2018-09-03T13-09-35/root/addint/example___addint___s2			2018-09-03 11:09:38.19183	2018-09-03 11:09:45.967638	{"sample_index": [1], "sample_id": ["s2"], "errors": []}	61	2222	example_2018-09-03T13-09-35/root/addint	2
13272	example_2018-09-03T13-09-35/root/addint/example___addint___s3			2018-09-03 11:09:38.293377	2018-09-03 11:09:49.561761	{"sample_index": [2], "sample_id": ["s3"], "errors": []}	61	2222	example_2018-09-03T13-09-35/root/addint	2
13273	example_2018-09-03T13-09-35/root/addint/example___addint___s4			2018-09-03 11:09:38.39322	2018-09-03 11:09:53.032101	{"sample_index": [3], "sample_id": ["s4"], "errors": []}	61	2222	example_2018-09-03T13-09-35/root/addint	2
13279	example_2018-09-03T13-11-45/root/source1/example___source1___s2			2018-09-03 11:11:45.88645	2018-09-03 11:11:46.000823	{"errors": [], "sample_id": ["s2"], "sample_index": [1]}	62	2227	example_2018-09-03T13-11-45/root/source1	2
13274	example_2018-09-03T13-09-35/root/sink1/example___sink1___s1___0			2018-09-03 11:09:38.49902	2018-09-03 11:09:57.555162	{"sample_index": [0], "sample_id": ["s1"], "errors": []}	61	2221	example_2018-09-03T13-09-35/root/sink1	2
13283	example_2018-09-03T13-11-45/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 11:11:46.783367	2018-09-03 11:11:46.89785	{"errors": [], "sample_id": ["id_1"], "sample_index": [1]}	62	2229	example_2018-09-03T13-11-45/root/const_addint_right_hand_0	2
13286	example_2018-09-03T13-11-45/root/addint/example___addint___s1			2018-09-03 11:11:47.484611	2018-09-03 11:11:52.036144	{"errors": [], "sample_id": ["s1"], "sample_index": [0]}	62	2226	example_2018-09-03T13-11-45/root/addint	2
13288	example_2018-09-03T13-11-45/root/addint/example___addint___s3			2018-09-03 11:11:47.717453	2018-09-03 11:11:59.990225	{"errors": [], "sample_id": ["s3"], "sample_index": [2]}	62	2226	example_2018-09-03T13-11-45/root/addint	2
13289	example_2018-09-03T13-11-45/root/addint/example___addint___s4			2018-09-03 11:11:47.832172	2018-09-03 11:12:03.761103	{"errors": [], "sample_id": ["s4"], "sample_index": [3]}	62	2226	example_2018-09-03T13-11-45/root/addint	2
13290	example_2018-09-03T13-11-45/root/sink1/example___sink1___s1___0			2018-09-03 11:11:47.938441	2018-09-03 11:12:10.273025	{"errors": [], "sample_id": ["s1"], "sample_index": [0]}	62	2228	example_2018-09-03T13-11-45/root/sink1	2
13291	example_2018-09-03T13-11-45/root/sink1/example___sink1___s2___0			2018-09-03 11:11:48.044195	2018-09-03 11:12:14.864324	{"errors": [], "sample_id": ["s2"], "sample_index": [1]}	62	2228	example_2018-09-03T13-11-45/root/sink1	2
13292	example_2018-09-03T13-11-45/root/sink1/example___sink1___s3___0			2018-09-03 11:11:48.156126	2018-09-03 11:12:19.451434	{"errors": [], "sample_id": ["s3"], "sample_index": [2]}	62	2228	example_2018-09-03T13-11-45/root/sink1	2
13287	example_2018-09-03T13-11-45/root/addint/example___addint___s2			2018-09-03 11:11:47.599741	2018-09-03 11:11:55.872201	{"errors": [], "sample_id": ["s2"], "sample_index": [1]}	62	2226	example_2018-09-03T13-11-45/root/addint	2
13304	example_2018-09-03T13-12-39/root/addint/example___addint___s3			2018-09-03 11:12:42.214676	2018-09-03 11:12:53.376022	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	63	2233	example_2018-09-03T13-12-39/root/addint	2
13293	example_2018-09-03T13-11-45/root/sink1/example___sink1___s4___0			2018-09-03 11:11:48.263846	2018-09-03 11:12:24.047807	{"errors": [], "sample_id": ["s4"], "sample_index": [3]}	62	2228	example_2018-09-03T13-11-45/root/sink1	2
13310	example_2018-09-03T13-13-28/root/source1/example___source1___s1			2018-09-03 11:13:28.518029	2018-09-03 11:13:28.630949	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	64	2236	example_2018-09-03T13-13-28/root/source1	2
13300	example_2018-09-03T13-12-39/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 11:12:41.548581	2018-09-03 11:12:41.652932	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	63	2234	example_2018-09-03T13-12-39/root/const_addint_right_hand_0	2
13294	example_2018-09-03T13-12-39/root/source1/example___source1___s1			2018-09-03 11:12:40.159891	2018-09-03 11:12:40.270474	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	63	2231	example_2018-09-03T13-12-39/root/source1	2
13295	example_2018-09-03T13-12-39/root/source1/example___source1___s2			2018-09-03 11:12:40.381067	2018-09-03 11:12:40.499197	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	63	2231	example_2018-09-03T13-12-39/root/source1	2
13301	example_2018-09-03T13-12-39/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 11:12:41.765033	2018-09-03 11:12:41.880288	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	63	2234	example_2018-09-03T13-12-39/root/const_addint_right_hand_0	2
13296	example_2018-09-03T13-12-39/root/source1/example___source1___s3			2018-09-03 11:12:40.617232	2018-09-03 11:12:40.732707	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	63	2231	example_2018-09-03T13-12-39/root/source1	2
13313	example_2018-09-03T13-13-28/root/source1/example___source1___s4			2018-09-03 11:13:29.173326	2018-09-03 11:13:29.287652	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	64	2236	example_2018-09-03T13-13-28/root/source1	2
13297	example_2018-09-03T13-12-39/root/source1/example___source1___s4			2018-09-03 11:12:40.84519	2018-09-03 11:12:40.955186	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	63	2231	example_2018-09-03T13-12-39/root/source1	2
13298	example_2018-09-03T13-12-39/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 11:12:41.084203	2018-09-03 11:12:41.189503	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	63	2234	example_2018-09-03T13-12-39/root/const_addint_right_hand_0	2
13305	example_2018-09-03T13-12-39/root/addint/example___addint___s4			2018-09-03 11:12:42.337435	2018-09-03 11:12:56.950174	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	63	2233	example_2018-09-03T13-12-39/root/addint	2
13299	example_2018-09-03T13-12-39/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 11:12:41.296284	2018-09-03 11:12:41.426638	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	63	2234	example_2018-09-03T13-12-39/root/const_addint_right_hand_0	2
13311	example_2018-09-03T13-13-28/root/source1/example___source1___s2			2018-09-03 11:13:28.737689	2018-09-03 11:13:28.857912	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	64	2236	example_2018-09-03T13-13-28/root/source1	2
13306	example_2018-09-03T13-12-39/root/sink1/example___sink1___s1___0			2018-09-03 11:12:42.448451	2018-09-03 11:13:01.441702	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	63	2232	example_2018-09-03T13-12-39/root/sink1	2
13315	example_2018-09-03T13-13-28/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_1			2018-09-03 11:13:29.619846	2018-09-03 11:13:29.735313	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	64	2239	example_2018-09-03T13-13-28/root/const_addint_right_hand_0	2
13307	example_2018-09-03T13-12-39/root/sink1/example___sink1___s2___0			2018-09-03 11:12:42.567693	2018-09-03 11:13:05.841648	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	63	2232	example_2018-09-03T13-12-39/root/sink1	2
13312	example_2018-09-03T13-13-28/root/source1/example___source1___s3			2018-09-03 11:13:28.962577	2018-09-03 11:13:29.069786	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	64	2236	example_2018-09-03T13-13-28/root/source1	2
13302	example_2018-09-03T13-12-39/root/addint/example___addint___s1			2018-09-03 11:12:41.991241	2018-09-03 11:12:46.560653	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	63	2233	example_2018-09-03T13-12-39/root/addint	2
13314	example_2018-09-03T13-13-28/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_0			2018-09-03 11:13:29.392809	2018-09-03 11:13:29.498998	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	64	2239	example_2018-09-03T13-13-28/root/const_addint_right_hand_0	2
13303	example_2018-09-03T13-12-39/root/addint/example___addint___s2			2018-09-03 11:12:42.103123	2018-09-03 11:12:50.033068	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	63	2233	example_2018-09-03T13-12-39/root/addint	2
13308	example_2018-09-03T13-12-39/root/sink1/example___sink1___s3___0			2018-09-03 11:12:42.682857	2018-09-03 11:13:10.335528	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	63	2232	example_2018-09-03T13-12-39/root/sink1	2
13309	example_2018-09-03T13-12-39/root/sink1/example___sink1___s4___0			2018-09-03 11:12:42.815187	2018-09-03 11:13:14.709713	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	63	2232	example_2018-09-03T13-12-39/root/sink1	2
13318	example_2018-09-03T13-13-28/root/addint/example___addint___s1			2018-09-03 11:13:30.322882	2018-09-03 11:13:34.678564	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	64	2238	example_2018-09-03T13-13-28/root/addint	2
13317	example_2018-09-03T13-13-28/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_3			2018-09-03 11:13:30.078228	2018-09-03 11:13:30.206533	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	64	2239	example_2018-09-03T13-13-28/root/const_addint_right_hand_0	2
13316	example_2018-09-03T13-13-28/root/const_addint_right_hand_0/example___const_addint_right_hand_0___id_2			2018-09-03 11:13:29.849383	2018-09-03 11:13:29.965541	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	64	2239	example_2018-09-03T13-13-28/root/const_addint_right_hand_0	2
13322	example_2018-09-03T13-13-28/root/sink1/example___sink1___s1___0			2018-09-03 11:13:30.80352	2018-09-03 11:13:49.39326	{"sample_id": ["s1"], "sample_index": [0], "errors": []}	64	2237	example_2018-09-03T13-13-28/root/sink1	2
13319	example_2018-09-03T13-13-28/root/addint/example___addint___s2			2018-09-03 11:13:30.431093	2018-09-03 11:13:38.118004	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	64	2238	example_2018-09-03T13-13-28/root/addint	2
13321	example_2018-09-03T13-13-28/root/addint/example___addint___s4			2018-09-03 11:13:30.679125	2018-09-03 11:13:44.798215	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	64	2238	example_2018-09-03T13-13-28/root/addint	2
13323	example_2018-09-03T13-13-28/root/sink1/example___sink1___s2___0			2018-09-03 11:13:30.917344	2018-09-03 11:13:53.771462	{"sample_id": ["s2"], "sample_index": [1], "errors": []}	64	2237	example_2018-09-03T13-13-28/root/sink1	2
13320	example_2018-09-03T13-13-28/root/addint/example___addint___s3			2018-09-03 11:13:30.549408	2018-09-03 11:13:41.439617	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	64	2238	example_2018-09-03T13-13-28/root/addint	2
13327	failing_macro_top_level_2018-09-03T14-01-00/root/source_b/failing_macro_top_level___source_b___sample_1			2018-09-03 12:01:01.072664	2018-09-03 12:01:10.493148	{"sample_id": ["sample_1"], "sample_index": [0], "errors": []}	65	2242	failing_macro_top_level_2018-09-03T14-01-00/root/source_b	2
13328	failing_macro_top_level_2018-09-03T14-01-00/root/source_c/failing_macro_top_level___source_c___sample_1			2018-09-03 12:01:01.183484	2018-09-03 12:01:14.862997	{"sample_id": ["sample_1"], "sample_index": [0], "errors": []}	65	2243	failing_macro_top_level_2018-09-03T14-01-00/root/source_c	2
13339	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_3/failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_1			2018-09-03 12:01:17.588399	2018-09-03 12:01:17.704812	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	65	2250	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_3	2
13324	example_2018-09-03T13-13-28/root/sink1/example___sink1___s3___0			2018-09-03 11:13:31.027864	2018-09-03 11:13:58.161616	{"sample_id": ["s3"], "sample_index": [2], "errors": []}	64	2237	example_2018-09-03T13-13-28/root/sink1	2
13325	example_2018-09-03T13-13-28/root/sink1/example___sink1___s4___0			2018-09-03 11:13:31.151301	2018-09-03 11:14:09.387866	{"sample_id": ["s4"], "sample_index": [3], "errors": []}	64	2237	example_2018-09-03T13-13-28/root/sink1	2
13333	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1/failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_3			2018-09-03 12:01:16.212783	2018-09-03 12:01:16.32531	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	65	2248	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1	2
13330	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1/failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_0			2018-09-03 12:01:15.535599	2018-09-03 12:01:15.657187	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	65	2248	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1	2
13336	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_2/failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_2			2018-09-03 12:01:16.896663	2018-09-03 12:01:17.015063	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	65	2249	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_2	2
13331	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1/failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_1			2018-09-03 12:01:15.773186	2018-09-03 12:01:15.886832	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	65	2248	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1	2
13329	failing_macro_top_level_2018-09-03T14-01-00/root/const_add_right_hand_0/failing_macro_top_level___const_add_right_hand_0___id_0			2018-09-03 12:01:01.297474	2018-09-03 12:01:01.402718	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	65	2247	failing_macro_top_level_2018-09-03T14-01-00/root/const_add_right_hand_0	2
13334	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_2/failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_0			2018-09-03 12:01:16.439975	2018-09-03 12:01:16.543019	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	65	2249	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_2	2
13326	failing_macro_top_level_2018-09-03T14-01-00/root/source_a/failing_macro_top_level___source_a___sample_1			2018-09-03 12:01:00.961832	2018-09-03 12:01:06.062257	{"sample_id": ["sample_1"], "sample_index": [0], "errors": []}	65	2241	failing_macro_top_level_2018-09-03T14-01-00/root/source_a	2
13332	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1/failing_macro_top_level_failing_macro_failing_network___source_1___sample_1_2			2018-09-03 12:01:15.998278	2018-09-03 12:01:16.108091	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	65	2248	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1	2
13338	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_3/failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_0			2018-09-03 12:01:17.359091	2018-09-03 12:01:17.466157	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	65	2250	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_3	2
13335	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_2/failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_1			2018-09-03 12:01:16.681304	2018-09-03 12:01:16.7897	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	65	2249	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_2	2
13337	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_2/failing_macro_top_level_failing_macro_failing_network___source_2___sample_1_3			2018-09-03 12:01:17.122166	2018-09-03 12:01:17.234406	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	65	2249	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_2	2
13343	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_1_fail_2_0/failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_1			2018-09-03 12:01:18.551335	2018-09-03 12:01:18.657348	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	65	2261	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_1_fail_2_0	2
13341	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_3/failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_3			2018-09-03 12:01:18.037457	2018-09-03 12:01:18.158873	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	65	2250	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_3	2
13340	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_3/failing_macro_top_level_failing_macro_failing_network___source_3___sample_1_2			2018-09-03 12:01:17.811409	2018-09-03 12:01:17.924687	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	65	2250	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_3	2
13342	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_1_fail_2_0/failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_0			2018-09-03 12:01:18.309997	2018-09-03 12:01:18.436287	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	65	2261	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_1_fail_2_0	2
13344	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_1_fail_2_0/failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_2			2018-09-03 12:01:18.767237	2018-09-03 12:01:18.881941	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	65	2261	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_1_fail_2_0	2
13345	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_1_fail_2_0/failing_macro_top_level_failing_macro_failing_network___const_step_1_fail_2_0___id_3			2018-09-03 12:01:18.997885	2018-09-03 12:01:19.107728	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	65	2261	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_1_fail_2_0	2
13346	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_2_fail_1_0/failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_0			2018-09-03 12:01:19.223725	2018-09-03 12:01:19.338095	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	65	2262	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_2_fail_1_0	2
13354	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_0			2018-09-03 12:01:20.57756	2018-09-03 12:01:39.363554	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	65	2257	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2	2
13350	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_0			2018-09-03 12:01:20.10902	2018-09-03 12:01:25.417695	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	65	2256	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1	2
13347	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_2_fail_1_0/failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_1			2018-09-03 12:01:19.439138	2018-09-03 12:01:19.547034	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	65	2262	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_2_fail_1_0	2
13348	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_2_fail_1_0/failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_2			2018-09-03 12:01:19.663227	2018-09-03 12:01:19.763503	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	65	2262	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_2_fail_1_0	2
13351	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_1			2018-09-03 12:01:20.225671	2018-09-03 12:01:28.837233	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	65	2256	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1	3
13349	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_2_fail_1_0/failing_macro_top_level_failing_macro_failing_network___const_step_2_fail_1_0___id_3			2018-09-03 12:01:19.878252	2018-09-03 12:01:19.983506	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	65	2262	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_2_fail_1_0	2
13357	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_3			2018-09-03 12:01:20.915339	2018-09-03 12:01:50.385335	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	65	2257	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2	3
13359	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_1			2018-09-03 12:01:21.142289	2018-09-03 12:01:42.896155	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	65	2258	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3	4
13352	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_2			2018-09-03 12:01:20.335537	2018-09-03 12:01:32.331589	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	65	2256	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1	2
13355	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_1			2018-09-03 12:01:20.687722	2018-09-03 12:01:42.785428	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	65	2257	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2	2
13353	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/failing_macro_top_level_failing_macro_failing_network___step_1___sample_1_3			2018-09-03 12:01:20.457734	2018-09-03 12:01:35.791458	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	65	2256	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1	3
13356	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/failing_macro_top_level_failing_macro_failing_network___step_2___sample_1_2			2018-09-03 12:01:20.789612	2018-09-03 12:01:46.231895	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	65	2257	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2	3
13363	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range/failing_macro_top_level_failing_macro_failing_network___range___sample_1_1			2018-09-03 12:01:21.587914	2018-09-03 12:01:43.101391	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	65	2259	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range	4
13365	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range/failing_macro_top_level_failing_macro_failing_network___range___sample_1_3			2018-09-03 12:01:21.808795	2018-09-03 12:01:50.662866	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	65	2259	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range	4
13364	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range/failing_macro_top_level_failing_macro_failing_network___range___sample_1_2			2018-09-03 12:01:21.695535	2018-09-03 12:01:46.475861	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	65	2259	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range	4
13361	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_3			2018-09-03 12:01:21.361956	2018-09-03 12:01:50.518185	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	65	2258	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3	4
13362	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range/failing_macro_top_level_failing_macro_failing_network___range___sample_1_0			2018-09-03 12:01:21.47959	2018-09-03 12:02:00.608096	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	65	2259	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range	4
13360	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_2			2018-09-03 12:01:21.251113	2018-09-03 12:01:46.350072	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	65	2258	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3	4
13358	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/failing_macro_top_level_failing_macro_failing_network___step_3___sample_1_0			2018-09-03 12:01:21.027001	2018-09-03 12:02:00.476739	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	65	2258	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3	3
13366	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sum/failing_macro_top_level_failing_macro_failing_network___sum___sample_1_0			2018-09-03 12:02:01.037552	2018-09-03 12:02:01.490306	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	65	2260	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sum	4
13381	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_3/failing_macro_top_level_failing_macro_failing_network___sink_3___sample_1_3			2018-09-03 12:02:04.542478	2018-09-03 12:02:04.66418	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	65	2253	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_3	2
13375	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_2/failing_macro_top_level_failing_macro_failing_network___sink_2___sample_1_1			2018-09-03 12:02:03.149356	2018-09-03 12:02:03.253577	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	65	2252	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_2	2
13372	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_1/failing_macro_top_level_failing_macro_failing_network___sink_1___sample_1_2			2018-09-03 12:02:02.457653	2018-09-03 12:02:02.569489	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	65	2251	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_1	2
13367	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sum/failing_macro_top_level_failing_macro_failing_network___sum___sample_1_1			2018-09-03 12:02:01.153356	2018-09-03 12:02:01.613467	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	65	2260	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sum	4
13368	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sum/failing_macro_top_level_failing_macro_failing_network___sum___sample_1_2			2018-09-03 12:02:01.26672	2018-09-03 12:02:01.734697	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	65	2260	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sum	4
13369	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sum/failing_macro_top_level_failing_macro_failing_network___sum___sample_1_3			2018-09-03 12:02:01.38152	2018-09-03 12:02:01.87038	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	65	2260	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sum	4
13373	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_1/failing_macro_top_level_failing_macro_failing_network___sink_1___sample_1_3			2018-09-03 12:02:02.681125	2018-09-03 12:02:02.796748	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	65	2251	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_1	2
13370	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_1/failing_macro_top_level_failing_macro_failing_network___sink_1___sample_1_0			2018-09-03 12:02:01.998485	2018-09-03 12:02:02.12969	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	65	2251	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_1	2
13378	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_3/failing_macro_top_level_failing_macro_failing_network___sink_3___sample_1_0			2018-09-03 12:02:03.825655	2018-09-03 12:02:03.934182	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	65	2253	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_3	2
13371	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_1/failing_macro_top_level_failing_macro_failing_network___sink_1___sample_1_1			2018-09-03 12:02:02.248057	2018-09-03 12:02:02.352047	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	65	2251	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_1	2
13376	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_2/failing_macro_top_level_failing_macro_failing_network___sink_2___sample_1_2			2018-09-03 12:02:03.369931	2018-09-03 12:02:03.488777	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	65	2252	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_2	2
13374	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_2/failing_macro_top_level_failing_macro_failing_network___sink_2___sample_1_0			2018-09-03 12:02:02.934035	2018-09-03 12:02:03.035693	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	65	2252	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_2	2
13380	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_3/failing_macro_top_level_failing_macro_failing_network___sink_3___sample_1_2			2018-09-03 12:02:04.304668	2018-09-03 12:02:04.419007	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	65	2253	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_3	2
13377	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_2/failing_macro_top_level_failing_macro_failing_network___sink_2___sample_1_3			2018-09-03 12:02:03.604291	2018-09-03 12:02:03.714039	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	65	2252	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_2	2
13379	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_3/failing_macro_top_level_failing_macro_failing_network___sink_3___sample_1_1			2018-09-03 12:02:04.050398	2018-09-03 12:02:04.165543	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	65	2253	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_3	2
13385	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_4/failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_3			2018-09-03 12:02:05.494283	2018-09-03 12:02:05.606134	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	65	2254	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_4	2
13383	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_4/failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_1			2018-09-03 12:02:05.004314	2018-09-03 12:02:05.120149	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	65	2254	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_4	2
13382	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_4/failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_0			2018-09-03 12:02:04.770401	2018-09-03 12:02:04.875793	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	65	2254	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_4	2
13384	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_4/failing_macro_top_level_failing_macro_failing_network___sink_4___sample_1_2			2018-09-03 12:02:05.250795	2018-09-03 12:02:05.364414	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	65	2254	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_4	2
13386	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_5/failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_0			2018-09-03 12:02:05.723752	2018-09-03 12:02:05.832221	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	65	2255	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_5	2
13387	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_5/failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_1			2018-09-03 12:02:05.942598	2018-09-03 12:02:06.056814	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	65	2255	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_5	2
13720	iris/root/5_BET_and_REG/121_combine_brains/job___001			2018-09-08 13:18:22.96483	2018-09-08 13:18:46.102557	{}	75	2442	iris/root/5_BET_and_REG/121_combine_brains	2
13388	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_5/failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_2			2018-09-03 12:02:06.169107	2018-09-03 12:02:06.276369	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	65	2255	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_5	2
13403	add_ints_2018-09-03T14-04-33/root/add/add_ints___add___id_1_1			2018-09-03 12:04:39.208821	2018-09-03 12:04:50.890387	{"sample_id": ["id_1_1"], "sample_index": [2], "errors": []}	66	2265	add_ints_2018-09-03T14-04-33/root/add	2
13389	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_5/failing_macro_top_level_failing_macro_failing_network___sink_5___sample_1_3			2018-09-03 12:02:06.378313	2018-09-03 12:02:06.495985	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	65	2255	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_5	2
13404	add_ints_2018-09-03T14-04-33/root/add/add_ints___add___id_1_2			2018-09-03 12:04:39.328784	2018-09-03 12:04:54.67616	{"sample_id": ["id_1_2"], "sample_index": [3], "errors": []}	66	2265	add_ints_2018-09-03T14-04-33/root/add	2
13398	add_ints_2018-09-03T14-04-33/root/source/add_ints___source___id_0			2018-09-03 12:04:33.421443	2018-09-03 12:04:33.531274	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	66	2264	add_ints_2018-09-03T14-04-33/root/source	2
13406	add_ints_2018-09-03T14-04-33/root/sink/add_ints___sink___id_0___0			2018-09-03 12:04:39.55423	2018-09-03 12:05:03.034038	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	66	2267	add_ints_2018-09-03T14-04-33/root/sink	2
13408	add_ints_2018-09-03T14-04-33/root/sink/add_ints___sink___id_1_1___0			2018-09-03 12:04:39.76504	2018-09-03 12:05:14.653287	{"sample_id": ["id_1_1"], "sample_index": [2], "errors": []}	66	2267	add_ints_2018-09-03T14-04-33/root/sink	2
13400	add_ints_2018-09-03T14-04-33/root/const_add_right_hand_0/add_ints___const_add_right_hand_0___id_0			2018-09-03 12:04:33.75007	2018-09-03 12:04:33.865769	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	66	2266	add_ints_2018-09-03T14-04-33/root/const_add_right_hand_0	2
13390	failing_macro_top_level_2018-09-03T14-01-00/root/add/failing_macro_top_level___add___sample_1_0			2018-09-03 12:02:06.684342	2018-09-03 12:02:07.592815	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	65	2245	failing_macro_top_level_2018-09-03T14-01-00/root/add	4
13391	failing_macro_top_level_2018-09-03T14-01-00/root/add/failing_macro_top_level___add___sample_1_1			2018-09-03 12:02:06.803352	2018-09-03 12:02:07.735699	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	65	2245	failing_macro_top_level_2018-09-03T14-01-00/root/add	4
13392	failing_macro_top_level_2018-09-03T14-01-00/root/add/failing_macro_top_level___add___sample_1_2			2018-09-03 12:02:06.922202	2018-09-03 12:02:07.867184	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	65	2245	failing_macro_top_level_2018-09-03T14-01-00/root/add	4
13393	failing_macro_top_level_2018-09-03T14-01-00/root/add/failing_macro_top_level___add___sample_1_3			2018-09-03 12:02:07.040878	2018-09-03 12:02:07.99715	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	65	2245	failing_macro_top_level_2018-09-03T14-01-00/root/add	4
13394	failing_macro_top_level_2018-09-03T14-01-00/root/sink/failing_macro_top_level___sink___sample_1_0___0			2018-09-03 12:02:07.160527	2018-09-03 12:02:08.133335	{"sample_id": ["sample_1_0"], "sample_index": [0], "errors": []}	65	2246	failing_macro_top_level_2018-09-03T14-01-00/root/sink	4
13395	failing_macro_top_level_2018-09-03T14-01-00/root/sink/failing_macro_top_level___sink___sample_1_1___0			2018-09-03 12:02:07.271994	2018-09-03 12:02:08.257989	{"sample_id": ["sample_1_1"], "sample_index": [1], "errors": []}	65	2246	failing_macro_top_level_2018-09-03T14-01-00/root/sink	4
13396	failing_macro_top_level_2018-09-03T14-01-00/root/sink/failing_macro_top_level___sink___sample_1_2___0			2018-09-03 12:02:07.369147	2018-09-03 12:02:08.383196	{"sample_id": ["sample_1_2"], "sample_index": [2], "errors": []}	65	2246	failing_macro_top_level_2018-09-03T14-01-00/root/sink	4
13397	failing_macro_top_level_2018-09-03T14-01-00/root/sink/failing_macro_top_level___sink___sample_1_3___0			2018-09-03 12:02:07.482773	2018-09-03 12:02:08.510585	{"sample_id": ["sample_1_3"], "sample_index": [3], "errors": []}	65	2246	failing_macro_top_level_2018-09-03T14-01-00/root/sink	4
13399	add_ints_2018-09-03T14-04-33/root/source/add_ints___source___id_1			2018-09-03 12:04:33.642206	2018-09-03 12:04:38.437325	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	66	2264	add_ints_2018-09-03T14-04-33/root/source	2
13409	add_ints_2018-09-03T14-04-33/root/sink/add_ints___sink___id_1_2___0			2018-09-03 12:04:39.870633	2018-09-03 12:05:20.745525	{"sample_id": ["id_1_2"], "sample_index": [3], "errors": []}	66	2267	add_ints_2018-09-03T14-04-33/root/sink	2
13401	add_ints_2018-09-03T14-04-33/root/add/add_ints___add___id_0			2018-09-03 12:04:38.996091	2018-09-03 12:04:43.645647	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	66	2265	add_ints_2018-09-03T14-04-33/root/add	2
13407	add_ints_2018-09-03T14-04-33/root/sink/add_ints___sink___id_1_0___0			2018-09-03 12:04:39.651962	2018-09-03 12:05:08.760387	{"sample_id": ["id_1_0"], "sample_index": [1], "errors": []}	66	2267	add_ints_2018-09-03T14-04-33/root/sink	2
13410	add_ints_2018-09-03T14-04-33/root/sink/add_ints___sink___id_1_3___0			2018-09-03 12:04:39.989075	2018-09-03 12:05:26.466054	{"sample_id": ["id_1_3"], "sample_index": [4], "errors": []}	66	2267	add_ints_2018-09-03T14-04-33/root/sink	2
13413	macro_node_2_2018-09-03T14-14-40/root/source_2/macro_node_2___source_2___id_0			2018-09-03 12:14:41.132815	2018-09-03 12:14:41.249114	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	67	2271	macro_node_2_2018-09-03T14-14-40/root/source_2	2
13411	macro_node_2_2018-09-03T14-14-40/root/source_1/macro_node_2___source_1___id_0			2018-09-03 12:14:40.679048	2018-09-03 12:14:40.794952	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	67	2270	macro_node_2_2018-09-03T14-14-40/root/source_1	2
13405	add_ints_2018-09-03T14-04-33/root/add/add_ints___add___id_1_3			2018-09-03 12:04:39.442967	2018-09-03 12:04:58.436843	{"sample_id": ["id_1_3"], "sample_index": [4], "errors": []}	66	2265	add_ints_2018-09-03T14-04-33/root/add	2
13402	add_ints_2018-09-03T14-04-33/root/add/add_ints___add___id_1_0			2018-09-03 12:04:39.106222	2018-09-03 12:04:47.156156	{"sample_id": ["id_1_0"], "sample_index": [1], "errors": []}	66	2265	add_ints_2018-09-03T14-04-33/root/add	2
13412	macro_node_2_2018-09-03T14-14-40/root/source_1/macro_node_2___source_1___id_1			2018-09-03 12:14:40.9074	2018-09-03 12:14:41.018279	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	67	2270	macro_node_2_2018-09-03T14-14-40/root/source_1	2
13414	macro_node_2_2018-09-03T14-14-40/root/source_2/macro_node_2___source_2___id_1			2018-09-03 12:14:41.359183	2018-09-03 12:14:41.46721	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	67	2271	macro_node_2_2018-09-03T14-14-40/root/source_2	2
13415	macro_node_2_2018-09-03T14-14-40/root/source_2/macro_node_2___source_2___id_2			2018-09-03 12:14:41.585745	2018-09-03 12:14:41.701347	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	67	2271	macro_node_2_2018-09-03T14-14-40/root/source_2	2
13416	macro_node_2_2018-09-03T14-14-40/root/source_3/macro_node_2___source_3___id_0			2018-09-03 12:14:41.81441	2018-09-03 12:14:41.917543	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	67	2272	macro_node_2_2018-09-03T14-14-40/root/source_3	2
13428	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_1___id_0__id_2			2018-09-03 12:14:43.858969	2018-09-03 12:14:43.969727	{"sample_id": ["id_0", "id_2"], "sample_index": [0, 2], "errors": []}	67	2276	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1	2
13417	macro_node_2_2018-09-03T14-14-40/root/source_3/macro_node_2___source_3___id_1			2018-09-03 12:14:42.026864	2018-09-03 12:14:42.14139	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	67	2272	macro_node_2_2018-09-03T14-14-40/root/source_3	2
13424	macro_node_2_2018-09-03T14-14-40/root/addint/macro_node_2___addint___id_1__id_1			2018-09-03 12:14:43.157173	2018-09-03 12:15:06.325839	{"sample_id": ["id_1", "id_1"], "sample_index": [1, 1], "errors": []}	67	2269	macro_node_2_2018-09-03T14-14-40/root/addint	2
13418	macro_node_2_2018-09-03T14-14-40/root/source_3/macro_node_2___source_3___id_2			2018-09-03 12:14:42.256963	2018-09-03 12:14:42.384395	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	67	2272	macro_node_2_2018-09-03T14-14-40/root/source_3	2
13419	macro_node_2_2018-09-03T14-14-40/root/source_3/macro_node_2___source_3___id_3			2018-09-03 12:14:42.488923	2018-09-03 12:14:42.590574	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	67	2272	macro_node_2_2018-09-03T14-14-40/root/source_3	2
13426	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_1___id_0__id_0			2018-09-03 12:14:43.39872	2018-09-03 12:14:43.520464	{"sample_id": ["id_0", "id_0"], "sample_index": [0, 0], "errors": []}	67	2276	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1	2
13420	macro_node_2_2018-09-03T14-14-40/root/addint/macro_node_2___addint___id_0__id_0			2018-09-03 12:14:42.702049	2018-09-03 12:14:51.840347	{"sample_id": ["id_0", "id_0"], "sample_index": [0, 0], "errors": []}	67	2269	macro_node_2_2018-09-03T14-14-40/root/addint	2
13425	macro_node_2_2018-09-03T14-14-40/root/addint/macro_node_2___addint___id_1__id_2			2018-09-03 12:14:43.268803	2018-09-03 12:15:09.789245	{"sample_id": ["id_1", "id_2"], "sample_index": [1, 2], "errors": []}	67	2269	macro_node_2_2018-09-03T14-14-40/root/addint	2
13421	macro_node_2_2018-09-03T14-14-40/root/addint/macro_node_2___addint___id_0__id_1			2018-09-03 12:14:42.816436	2018-09-03 12:14:55.380155	{"sample_id": ["id_0", "id_1"], "sample_index": [0, 1], "errors": []}	67	2269	macro_node_2_2018-09-03T14-14-40/root/addint	2
13429	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_1___id_1__id_0			2018-09-03 12:14:44.078943	2018-09-03 12:14:44.183199	{"sample_id": ["id_1", "id_0"], "sample_index": [1, 0], "errors": []}	67	2276	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1	2
13423	macro_node_2_2018-09-03T14-14-40/root/addint/macro_node_2___addint___id_1__id_0			2018-09-03 12:14:43.036707	2018-09-03 12:15:02.601691	{"sample_id": ["id_1", "id_0"], "sample_index": [1, 0], "errors": []}	67	2269	macro_node_2_2018-09-03T14-14-40/root/addint	2
13427	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_1___id_0__id_1			2018-09-03 12:14:43.635536	2018-09-03 12:14:43.748696	{"sample_id": ["id_0", "id_1"], "sample_index": [0, 1], "errors": []}	67	2276	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1	2
13438	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_0__id_2			2018-09-03 12:14:45.880119	2018-09-03 12:15:20.361384	{"sample_id": ["id_0+id_0", "id_2"], "sample_index": [0, 2], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13431	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_1___id_1__id_2			2018-09-03 12:14:44.511419	2018-09-03 12:14:44.620863	{"sample_id": ["id_1", "id_2"], "sample_index": [1, 2], "errors": []}	67	2276	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1	2
13434	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_2/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_2___id_2			2018-09-03 12:14:45.159406	2018-09-03 12:14:45.269325	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	67	2277	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_2	2
13430	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_1___id_1__id_1			2018-09-03 12:14:44.289458	2018-09-03 12:14:44.402541	{"sample_id": ["id_1", "id_1"], "sample_index": [1, 1], "errors": []}	67	2276	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1	2
13433	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_2/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_2___id_1			2018-09-03 12:14:44.945754	2018-09-03 12:14:45.054925	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	67	2277	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_2	2
13432	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_2/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_2___id_0			2018-09-03 12:14:44.725357	2018-09-03 12:14:44.835303	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	67	2277	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_2	2
13435	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_2/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_input_2___id_3			2018-09-03 12:14:45.377809	2018-09-03 12:14:45.506738	{"sample_id": ["id_3"], "sample_index": [3], "errors": []}	67	2277	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_2	2
13436	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_0__id_0			2018-09-03 12:14:45.637785	2018-09-03 12:15:13.291551	{"sample_id": ["id_0+id_0", "id_0"], "sample_index": [0, 0], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13437	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_0__id_1			2018-09-03 12:14:45.753587	2018-09-03 12:15:16.891582	{"sample_id": ["id_0+id_0", "id_1"], "sample_index": [0, 1], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13422	macro_node_2_2018-09-03T14-14-40/root/addint/macro_node_2___addint___id_0__id_2			2018-09-03 12:14:42.920979	2018-09-03 12:14:58.769573	{"sample_id": ["id_0", "id_2"], "sample_index": [0, 2], "errors": []}	67	2269	macro_node_2_2018-09-03T14-14-40/root/addint	2
13505	add_ints_2018-09-03T14-18-44/root/add/add_ints___add___id_1_1			2018-09-03 12:18:50.635644	2018-09-03 12:19:02.933074	{"sample_id": ["id_1_1"], "sample_index": [2], "errors": []}	68	2282	add_ints_2018-09-03T14-18-44/root/add	2
13506	add_ints_2018-09-03T14-18-44/root/add/add_ints___add___id_1_2			2018-09-03 12:18:50.748431	2018-09-03 12:19:06.586716	{"sample_id": ["id_1_2"], "sample_index": [3], "errors": []}	68	2282	add_ints_2018-09-03T14-18-44/root/add	2
13439	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_0__id_3			2018-09-03 12:14:45.9895	2018-09-03 12:15:23.853149	{"sample_id": ["id_0+id_0", "id_3"], "sample_index": [0, 3], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13440	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_1__id_0			2018-09-03 12:14:46.119483	2018-09-03 12:15:27.389492	{"sample_id": ["id_0+id_1", "id_0"], "sample_index": [1, 0], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13447	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_2__id_3			2018-09-03 12:14:46.925629	2018-09-03 12:15:52.959976	{"sample_id": ["id_0+id_2", "id_3"], "sample_index": [2, 3], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13453	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_1__id_1			2018-09-03 12:14:47.597271	2018-09-03 12:16:25.778315	{"sample_id": ["id_1+id_1", "id_1"], "sample_index": [4, 1], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13441	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_1__id_1			2018-09-03 12:14:46.242005	2018-09-03 12:15:31.233371	{"sample_id": ["id_0+id_1", "id_1"], "sample_index": [1, 1], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13442	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_1__id_2			2018-09-03 12:14:46.370465	2018-09-03 12:15:35.3023	{"sample_id": ["id_0+id_1", "id_2"], "sample_index": [1, 2], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13445	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_2__id_1			2018-09-03 12:14:46.705227	2018-09-03 12:15:45.860917	{"sample_id": ["id_0+id_2", "id_1"], "sample_index": [2, 1], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13443	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_1__id_3			2018-09-03 12:14:46.481349	2018-09-03 12:15:38.871502	{"sample_id": ["id_0+id_1", "id_3"], "sample_index": [1, 3], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13448	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_0__id_0			2018-09-03 12:14:47.036379	2018-09-03 12:15:56.664042	{"sample_id": ["id_1+id_0", "id_0"], "sample_index": [3, 0], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13446	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_2__id_2			2018-09-03 12:14:46.814063	2018-09-03 12:15:49.385934	{"sample_id": ["id_0+id_2", "id_2"], "sample_index": [2, 2], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13450	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_0__id_2			2018-09-03 12:14:47.260781	2018-09-03 12:16:07.576721	{"sample_id": ["id_1+id_0", "id_2"], "sample_index": [3, 2], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13449	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_0__id_1			2018-09-03 12:14:47.147323	2018-09-03 12:16:01.169557	{"sample_id": ["id_1+id_0", "id_1"], "sample_index": [3, 1], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13455	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_1__id_3			2018-09-03 12:14:47.838137	2018-09-03 12:16:34.047096	{"sample_id": ["id_1+id_1", "id_3"], "sample_index": [4, 3], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13451	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_0__id_3			2018-09-03 12:14:47.373962	2018-09-03 12:16:16.505609	{"sample_id": ["id_1+id_0", "id_3"], "sample_index": [3, 3], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13452	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_1__id_0			2018-09-03 12:14:47.483277	2018-09-03 12:16:21.230845	{"sample_id": ["id_1+id_1", "id_0"], "sample_index": [4, 0], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13457	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_2__id_1			2018-09-03 12:14:48.062018	2018-09-03 12:16:42.319348	{"sample_id": ["id_1+id_2", "id_1"], "sample_index": [5, 1], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13454	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_1__id_2			2018-09-03 12:14:47.721732	2018-09-03 12:16:29.767954	{"sample_id": ["id_1+id_1", "id_2"], "sample_index": [4, 2], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13456	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_2__id_0			2018-09-03 12:14:47.947956	2018-09-03 12:16:38.708821	{"sample_id": ["id_1+id_2", "id_0"], "sample_index": [5, 0], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13458	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_2__id_2			2018-09-03 12:14:48.181932	2018-09-03 12:16:45.926114	{"sample_id": ["id_1+id_2", "id_2"], "sample_index": [5, 2], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13444	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_0+id_2__id_0			2018-09-03 12:14:46.590061	2018-09-03 12:15:42.324485	{"sample_id": ["id_0+id_2", "id_0"], "sample_index": [2, 0], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13466	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_1__id_2			2018-09-03 12:16:51.026321	2018-09-03 12:16:51.138418	{"sample_id": ["id_0+id_1", "id_2"], "sample_index": [1, 2], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13463	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_0__id_3			2018-09-03 12:16:50.343499	2018-09-03 12:16:50.449237	{"sample_id": ["id_0+id_0", "id_3"], "sample_index": [0, 3], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13459	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/macro_node_2_node_add_multiple_ints_1_add_ints_macro___addint1___id_1+id_2__id_3			2018-09-03 12:14:48.288939	2018-09-03 12:16:49.263518	{"sample_id": ["id_1+id_2", "id_3"], "sample_index": [5, 3], "errors": []}	67	2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	2
13460	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_0__id_0			2018-09-03 12:16:49.63866	2018-09-03 12:16:49.758294	{"sample_id": ["id_0+id_0", "id_0"], "sample_index": [0, 0], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13464	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_1__id_0			2018-09-03 12:16:50.562225	2018-09-03 12:16:50.681629	{"sample_id": ["id_0+id_1", "id_0"], "sample_index": [1, 0], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13461	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_0__id_1			2018-09-03 12:16:49.886165	2018-09-03 12:16:49.993309	{"sample_id": ["id_0+id_0", "id_1"], "sample_index": [0, 1], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13469	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_2__id_1			2018-09-03 12:16:51.781813	2018-09-03 12:16:51.891179	{"sample_id": ["id_0+id_2", "id_1"], "sample_index": [2, 1], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13462	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_0__id_2			2018-09-03 12:16:50.102538	2018-09-03 12:16:50.219419	{"sample_id": ["id_0+id_0", "id_2"], "sample_index": [0, 2], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13467	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_1__id_3			2018-09-03 12:16:51.288464	2018-09-03 12:16:51.4157	{"sample_id": ["id_0+id_1", "id_3"], "sample_index": [1, 3], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13465	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_1__id_1			2018-09-03 12:16:50.799773	2018-09-03 12:16:50.907908	{"sample_id": ["id_0+id_1", "id_1"], "sample_index": [1, 1], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13471	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_2__id_3			2018-09-03 12:16:52.248952	2018-09-03 12:16:52.361958	{"sample_id": ["id_0+id_2", "id_3"], "sample_index": [2, 3], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13468	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_2__id_0			2018-09-03 12:16:51.536115	2018-09-03 12:16:51.665053	{"sample_id": ["id_0+id_2", "id_0"], "sample_index": [2, 0], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13470	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_0+id_2__id_2			2018-09-03 12:16:52.018003	2018-09-03 12:16:52.135191	{"sample_id": ["id_0+id_2", "id_2"], "sample_index": [2, 2], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13475	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_0__id_3			2018-09-03 12:16:53.159107	2018-09-03 12:16:53.280103	{"sample_id": ["id_1+id_0", "id_3"], "sample_index": [3, 3], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13473	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_0__id_1			2018-09-03 12:16:52.700282	2018-09-03 12:16:52.807087	{"sample_id": ["id_1+id_0", "id_1"], "sample_index": [3, 1], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13472	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_0__id_0			2018-09-03 12:16:52.484043	2018-09-03 12:16:52.59917	{"sample_id": ["id_1+id_0", "id_0"], "sample_index": [3, 0], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13474	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_0__id_2			2018-09-03 12:16:52.926563	2018-09-03 12:16:53.039523	{"sample_id": ["id_1+id_0", "id_2"], "sample_index": [3, 2], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13476	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_1__id_0			2018-09-03 12:16:53.403957	2018-09-03 12:16:53.513666	{"sample_id": ["id_1+id_1", "id_0"], "sample_index": [4, 0], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13477	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_1__id_1			2018-09-03 12:16:53.638575	2018-09-03 12:16:53.770264	{"sample_id": ["id_1+id_1", "id_1"], "sample_index": [4, 1], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13507	add_ints_2018-09-03T14-18-44/root/add/add_ints___add___id_1_3			2018-09-03 12:18:50.861394	2018-09-03 12:19:10.135501	{"sample_id": ["id_1_3"], "sample_index": [4], "errors": []}	68	2282	add_ints_2018-09-03T14-18-44/root/add	2
13508	add_ints_2018-09-03T14-18-44/root/sink/add_ints___sink___id_0___0			2018-09-03 12:18:50.982866	2018-09-03 12:19:14.526117	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	68	2284	add_ints_2018-09-03T14-18-44/root/sink	2
13482	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_2__id_2			2018-09-03 12:16:54.78199	2018-09-03 12:16:54.904748	{"sample_id": ["id_1+id_2", "id_2"], "sample_index": [5, 2], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13478	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_1__id_2			2018-09-03 12:16:53.88371	2018-09-03 12:16:53.999609	{"sample_id": ["id_1+id_1", "id_2"], "sample_index": [4, 2], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13479	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_1__id_3			2018-09-03 12:16:54.111952	2018-09-03 12:16:54.228521	{"sample_id": ["id_1+id_1", "id_3"], "sample_index": [4, 3], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13493	macro_node_2_2018-09-03T14-14-40/root/sink/macro_node_2___sink___id_0__id_1___0			2018-09-03 12:16:56.515687	2018-09-03 12:17:42.717273	{"sample_id": ["id_0", "id_1"], "sample_index": [0, 1], "errors": []}	67	2275	macro_node_2_2018-09-03T14-14-40/root/sink	2
13483	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_2__id_3			2018-09-03 12:16:55.022188	2018-09-03 12:16:55.12868	{"sample_id": ["id_1+id_2", "id_3"], "sample_index": [5, 3], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13480	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_2__id_0			2018-09-03 12:16:54.338149	2018-09-03 12:16:54.446325	{"sample_id": ["id_1+id_2", "id_0"], "sample_index": [5, 0], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13481	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/macro_node_2_node_add_multiple_ints_1_add_ints_macro___macro_sink___id_1+id_2__id_1			2018-09-03 12:16:54.557952	2018-09-03 12:16:54.670247	{"sample_id": ["id_1+id_2", "id_1"], "sample_index": [5, 1], "errors": []}	67	2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	2
13489	macro_node_2_2018-09-03T14-14-40/root/sum/macro_node_2___sum___id_1__id_1			2018-09-03 12:16:56.048223	2018-09-03 12:17:19.354561	{"sample_id": ["id_1", "id_1"], "sample_index": [1, 1], "errors": []}	67	2274	macro_node_2_2018-09-03T14-14-40/root/sum	2
13499	macro_node_2_2018-09-03T14-14-40/root/sink/macro_node_2___sink___id_1__id_3___0			2018-09-03 12:16:57.194706	2018-09-03 12:18:16.734664	{"sample_id": ["id_1", "id_3"], "sample_index": [1, 3], "errors": []}	67	2275	macro_node_2_2018-09-03T14-14-40/root/sink	2
13495	macro_node_2_2018-09-03T14-14-40/root/sink/macro_node_2___sink___id_0__id_3___0			2018-09-03 12:16:56.756228	2018-09-03 12:17:55.777732	{"sample_id": ["id_0", "id_3"], "sample_index": [0, 3], "errors": []}	67	2275	macro_node_2_2018-09-03T14-14-40/root/sink	2
13485	macro_node_2_2018-09-03T14-14-40/root/sum/macro_node_2___sum___id_0__id_1			2018-09-03 12:16:55.54663	2018-09-03 12:17:04.144374	{"sample_id": ["id_0", "id_1"], "sample_index": [0, 1], "errors": []}	67	2274	macro_node_2_2018-09-03T14-14-40/root/sum	2
13487	macro_node_2_2018-09-03T14-14-40/root/sum/macro_node_2___sum___id_0__id_3			2018-09-03 12:16:55.801403	2018-09-03 12:17:10.996384	{"sample_id": ["id_0", "id_3"], "sample_index": [0, 3], "errors": []}	67	2274	macro_node_2_2018-09-03T14-14-40/root/sum	2
13494	macro_node_2_2018-09-03T14-14-40/root/sink/macro_node_2___sink___id_0__id_2___0			2018-09-03 12:16:56.63894	2018-09-03 12:17:48.750519	{"sample_id": ["id_0", "id_2"], "sample_index": [0, 2], "errors": []}	67	2275	macro_node_2_2018-09-03T14-14-40/root/sink	2
13488	macro_node_2_2018-09-03T14-14-40/root/sum/macro_node_2___sum___id_1__id_0			2018-09-03 12:16:55.921969	2018-09-03 12:17:14.31929	{"sample_id": ["id_1", "id_0"], "sample_index": [1, 0], "errors": []}	67	2274	macro_node_2_2018-09-03T14-14-40/root/sum	2
13491	macro_node_2_2018-09-03T14-14-40/root/sum/macro_node_2___sum___id_1__id_3			2018-09-03 12:16:56.281171	2018-09-03 12:17:29.542487	{"sample_id": ["id_1", "id_3"], "sample_index": [1, 3], "errors": []}	67	2274	macro_node_2_2018-09-03T14-14-40/root/sum	2
13497	macro_node_2_2018-09-03T14-14-40/root/sink/macro_node_2___sink___id_1__id_1___0			2018-09-03 12:16:56.976463	2018-09-03 12:18:06.710953	{"sample_id": ["id_1", "id_1"], "sample_index": [1, 1], "errors": []}	67	2275	macro_node_2_2018-09-03T14-14-40/root/sink	2
13500	add_ints_2018-09-03T14-18-44/root/source/add_ints___source___id_0			2018-09-03 12:18:44.762683	2018-09-03 12:18:44.886393	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	68	2281	add_ints_2018-09-03T14-18-44/root/source	2
13496	macro_node_2_2018-09-03T14-14-40/root/sink/macro_node_2___sink___id_1__id_0___0			2018-09-03 12:16:56.868725	2018-09-03 12:18:02.280533	{"sample_id": ["id_1", "id_0"], "sample_index": [1, 0], "errors": []}	67	2275	macro_node_2_2018-09-03T14-14-40/root/sink	2
13501	add_ints_2018-09-03T14-18-44/root/source/add_ints___source___id_1			2018-09-03 12:18:44.99999	2018-09-03 12:18:49.773655	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	68	2281	add_ints_2018-09-03T14-18-44/root/source	2
13498	macro_node_2_2018-09-03T14-14-40/root/sink/macro_node_2___sink___id_1__id_2___0			2018-09-03 12:16:57.079355	2018-09-03 12:18:11.183847	{"sample_id": ["id_1", "id_2"], "sample_index": [1, 2], "errors": []}	67	2275	macro_node_2_2018-09-03T14-14-40/root/sink	2
13490	macro_node_2_2018-09-03T14-14-40/root/sum/macro_node_2___sum___id_1__id_2			2018-09-03 12:16:56.165567	2018-09-03 12:17:23.83402	{"sample_id": ["id_1", "id_2"], "sample_index": [1, 2], "errors": []}	67	2274	macro_node_2_2018-09-03T14-14-40/root/sum	2
13486	macro_node_2_2018-09-03T14-14-40/root/sum/macro_node_2___sum___id_0__id_2			2018-09-03 12:16:55.674363	2018-09-03 12:17:07.624494	{"sample_id": ["id_0", "id_2"], "sample_index": [0, 2], "errors": []}	67	2274	macro_node_2_2018-09-03T14-14-40/root/sum	2
13492	macro_node_2_2018-09-03T14-14-40/root/sink/macro_node_2___sink___id_0__id_0___0			2018-09-03 12:16:56.399502	2018-09-03 12:17:36.50353	{"sample_id": ["id_0", "id_0"], "sample_index": [0, 0], "errors": []}	67	2275	macro_node_2_2018-09-03T14-14-40/root/sink	2
13484	macro_node_2_2018-09-03T14-14-40/root/sum/macro_node_2___sum___id_0__id_0			2018-09-03 12:16:55.43788	2018-09-03 12:17:00.735472	{"sample_id": ["id_0", "id_0"], "sample_index": [0, 0], "errors": []}	67	2274	macro_node_2_2018-09-03T14-14-40/root/sum	2
13502	add_ints_2018-09-03T14-18-44/root/const_add_right_hand_0/add_ints___const_add_right_hand_0___id_0			2018-09-03 12:18:45.14104	2018-09-03 12:18:45.265155	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	68	2283	add_ints_2018-09-03T14-18-44/root/const_add_right_hand_0	2
13503	add_ints_2018-09-03T14-18-44/root/add/add_ints___add___id_0			2018-09-03 12:18:50.403242	2018-09-03 12:18:55.084319	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	68	2282	add_ints_2018-09-03T14-18-44/root/add	2
13504	add_ints_2018-09-03T14-18-44/root/add/add_ints___add___id_1_0			2018-09-03 12:18:50.513469	2018-09-03 12:18:59.19086	{"sample_id": ["id_1_0"], "sample_index": [1], "errors": []}	68	2282	add_ints_2018-09-03T14-18-44/root/add	2
13687	iris/root/0_statistics/18_const_get_wm_volume_flatten_0/job___002			2018-09-08 13:18:14.68438	2018-09-08 13:18:17.38027	{}	75	2391	iris/root/0_statistics/18_const_get_wm_volume_flatten_0	2
13515	auto_prefix_test_2018-09-03T14-30-01/root/const/auto_prefix_test___const___id_1			2018-09-03 12:30:02.826264	2018-09-03 12:30:02.953849	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	69	2286	auto_prefix_test_2018-09-03T14-30-01/root/const	2
13521	auto_prefix_test_2018-09-03T14-30-01/root/a_p/auto_prefix_test___a_p___id_1			2018-09-03 12:30:03.75816	2018-09-03 12:30:24.585297	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	69	2289	auto_prefix_test_2018-09-03T14-30-01/root/a_p	2
13516	auto_prefix_test_2018-09-03T14-30-01/root/const/auto_prefix_test___const___id_2			2018-09-03 12:30:03.077779	2018-09-03 12:30:03.197805	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	69	2286	auto_prefix_test_2018-09-03T14-30-01/root/const	2
13519	auto_prefix_test_2018-09-03T14-30-01/root/m_p/auto_prefix_test___m_p___id_2			2018-09-03 12:30:03.534346	2018-09-03 12:30:16.574486	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	69	2288	auto_prefix_test_2018-09-03T14-30-01/root/m_p	2
13509	add_ints_2018-09-03T14-18-44/root/sink/add_ints___sink___id_1_0___0			2018-09-03 12:18:51.08948	2018-09-03 12:19:20.030207	{"sample_id": ["id_1_0"], "sample_index": [1], "errors": []}	68	2284	add_ints_2018-09-03T14-18-44/root/sink	2
13510	add_ints_2018-09-03T14-18-44/root/sink/add_ints___sink___id_1_1___0			2018-09-03 12:18:51.20835	2018-09-03 12:19:25.592946	{"sample_id": ["id_1_1"], "sample_index": [2], "errors": []}	68	2284	add_ints_2018-09-03T14-18-44/root/sink	2
13532	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_p/auto_prefix_test___sink_a_p___id_0___0			2018-09-03 12:30:05.037784	2018-09-03 12:31:11.067362	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	69	2293	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_p	2
13511	add_ints_2018-09-03T14-18-44/root/sink/add_ints___sink___id_1_2___0			2018-09-03 12:18:51.325404	2018-09-03 12:19:31.457867	{"sample_id": ["id_1_2"], "sample_index": [3], "errors": []}	68	2284	add_ints_2018-09-03T14-18-44/root/sink	2
13512	add_ints_2018-09-03T14-18-44/root/sink/add_ints___sink___id_1_3___0			2018-09-03 12:18:51.435117	2018-09-03 12:19:37.348492	{"sample_id": ["id_1_3"], "sample_index": [4], "errors": []}	68	2284	add_ints_2018-09-03T14-18-44/root/sink	2
13517	auto_prefix_test_2018-09-03T14-30-01/root/m_p/auto_prefix_test___m_p___id_0			2018-09-03 12:30:03.307967	2018-09-03 12:30:09.666357	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	69	2288	auto_prefix_test_2018-09-03T14-30-01/root/m_p	2
13513	auto_prefix_test_2018-09-03T14-30-01/root/source/auto_prefix_test___source___id_0			2018-09-03 12:30:02.351652	2018-09-03 12:30:02.465641	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	69	2287	auto_prefix_test_2018-09-03T14-30-01/root/source	2
13522	auto_prefix_test_2018-09-03T14-30-01/root/a_p/auto_prefix_test___a_p___id_2			2018-09-03 12:30:03.878303	2018-09-03 12:30:28.383644	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	69	2289	auto_prefix_test_2018-09-03T14-30-01/root/a_p	2
13518	auto_prefix_test_2018-09-03T14-30-01/root/m_p/auto_prefix_test___m_p___id_1			2018-09-03 12:30:03.423852	2018-09-03 12:30:13.130607	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	69	2288	auto_prefix_test_2018-09-03T14-30-01/root/m_p	2
13514	auto_prefix_test_2018-09-03T14-30-01/root/const/auto_prefix_test___const___id_0			2018-09-03 12:30:02.57976	2018-09-03 12:30:02.690121	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	69	2286	auto_prefix_test_2018-09-03T14-30-01/root/const	2
13526	auto_prefix_test_2018-09-03T14-30-01/root/a_n/auto_prefix_test___a_n___id_0			2018-09-03 12:30:04.35373	2018-09-03 12:30:43.209074	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	69	2291	auto_prefix_test_2018-09-03T14-30-01/root/a_n	2
13520	auto_prefix_test_2018-09-03T14-30-01/root/a_p/auto_prefix_test___a_p___id_0			2018-09-03 12:30:03.642086	2018-09-03 12:30:20.317349	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	69	2289	auto_prefix_test_2018-09-03T14-30-01/root/a_p	2
13523	auto_prefix_test_2018-09-03T14-30-01/root/m_n/auto_prefix_test___m_n___id_0			2018-09-03 12:30:03.998082	2018-09-03 12:30:32.024712	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	69	2290	auto_prefix_test_2018-09-03T14-30-01/root/m_n	2
13527	auto_prefix_test_2018-09-03T14-30-01/root/a_n/auto_prefix_test___a_n___id_1			2018-09-03 12:30:04.46397	2018-09-03 12:30:46.717309	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	69	2291	auto_prefix_test_2018-09-03T14-30-01/root/a_n	2
13524	auto_prefix_test_2018-09-03T14-30-01/root/m_n/auto_prefix_test___m_n___id_1			2018-09-03 12:30:04.115991	2018-09-03 12:30:35.769733	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	69	2290	auto_prefix_test_2018-09-03T14-30-01/root/m_n	2
13529	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_p/auto_prefix_test___sink_m_p___id_0___0			2018-09-03 12:30:04.68773	2018-09-03 12:30:54.956024	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	69	2292	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_p	2
13525	auto_prefix_test_2018-09-03T14-30-01/root/m_n/auto_prefix_test___m_n___id_2			2018-09-03 12:30:04.235015	2018-09-03 12:30:39.736382	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	69	2290	auto_prefix_test_2018-09-03T14-30-01/root/m_n	2
13530	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_p/auto_prefix_test___sink_m_p___id_1___0			2018-09-03 12:30:04.808031	2018-09-03 12:31:00.280623	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	69	2292	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_p	2
13528	auto_prefix_test_2018-09-03T14-30-01/root/a_n/auto_prefix_test___a_n___id_2			2018-09-03 12:30:04.575972	2018-09-03 12:30:50.207583	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	69	2291	auto_prefix_test_2018-09-03T14-30-01/root/a_n	2
13536	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_n/auto_prefix_test___sink_m_n___id_1___0			2018-09-03 12:30:05.494356	2018-09-03 12:31:30.690689	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	69	2294	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_n	2
13531	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_p/auto_prefix_test___sink_m_p___id_2___0			2018-09-03 12:30:04.917372	2018-09-03 12:31:06.468501	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	69	2292	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_p	2
13533	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_p/auto_prefix_test___sink_a_p___id_1___0			2018-09-03 12:30:05.146097	2018-09-03 12:31:16.591336	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	69	2293	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_p	2
13534	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_p/auto_prefix_test___sink_a_p___id_2___0			2018-09-03 12:30:05.266382	2018-09-03 12:31:21.849871	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	69	2293	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_p	2
13535	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_n/auto_prefix_test___sink_m_n___id_0___0			2018-09-03 12:30:05.379271	2018-09-03 12:31:26.332773	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	69	2294	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_n	2
13537	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_n/auto_prefix_test___sink_m_n___id_2___0			2018-09-03 12:30:05.608532	2018-09-03 12:31:35.1273	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	69	2294	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_n	2
13538	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_n/auto_prefix_test___sink_a_n___id_0___0			2018-09-03 12:30:05.728096	2018-09-03 12:31:39.526233	{"sample_id": ["id_0"], "sample_index": [0], "errors": []}	69	2295	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_n	2
13539	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_n/auto_prefix_test___sink_a_n___id_1___0			2018-09-03 12:30:05.853748	2018-09-03 12:31:43.934571	{"sample_id": ["id_1"], "sample_index": [1], "errors": []}	69	2295	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_n	2
13540	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_n/auto_prefix_test___sink_a_n___id_2___0			2018-09-03 12:30:05.969702	2018-09-03 12:31:48.325408	{"sample_id": ["id_2"], "sample_index": [2], "errors": []}	69	2295	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_n	2
13548	add_ints_2018-09-06T13-57-24/root/add/add_ints___add___id_1_3			2018-09-06 11:58:09.817099	2018-09-06 11:58:52.444785	{"errors": [], "sample_index": [4], "sample_id": ["id_1_3"]}	70	2300	add_ints_2018-09-06T13-57-24/root/add	4
13541	add_ints_2018-09-06T13-57-24/root/source/add_ints___source___id_0			2018-09-06 11:57:25.021124	2018-09-06 11:57:25.096214	{"errors": [], "sample_index": [0], "sample_id": ["id_0"]}	70	2297	add_ints_2018-09-06T13-57-24/root/source	2
13563	failing_network_2018-09-06T14-00-46/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_2			2018-09-06 12:00:49.439695	2018-09-06 12:00:49.51198	{"sample_index": [2], "sample_id": ["id_2"], "errors": []}	71	2303	failing_network_2018-09-06T14-00-46/root/const_step_2_fail_1_0	2
13553	add_ints_2018-09-06T13-57-24/root/sink/add_ints___sink___id_1_3___0			2018-09-06 11:58:10.213662	2018-09-06 11:58:59.812243	{"errors": [], "sample_index": [4], "sample_id": ["id_1_3"]}	70	2299	add_ints_2018-09-06T13-57-24/root/sink	4
13555	failing_network_2018-09-06T14-00-46/root/source_3/failing_network___source_3___sample_1			2018-09-06 12:00:48.282204	2018-09-06 12:02:08.063229	{"sample_index": [0], "sample_id": ["sample_1"], "errors": []}	71	2305	failing_network_2018-09-06T14-00-46/root/source_3	2
13543	add_ints_2018-09-06T13-57-24/root/const_add_right_hand_0/add_ints___const_add_right_hand_0___id_0			2018-09-06 11:57:25.2874	2018-09-06 11:57:25.360917	{"errors": [], "sample_index": [0], "sample_id": ["id_0"]}	70	2298	add_ints_2018-09-06T13-57-24/root/const_add_right_hand_0	2
13542	add_ints_2018-09-06T13-57-24/root/source/add_ints___source___id_1			2018-09-06 11:57:25.187436	2018-09-06 11:58:09.225572	{"errors": [], "sample_index": [1], "sample_id": ["id_1"]}	70	2297	add_ints_2018-09-06T13-57-24/root/source	2
13551	add_ints_2018-09-06T13-57-24/root/sink/add_ints___sink___id_1_1___0			2018-09-06 11:58:10.060582	2018-09-06 11:59:02.883671	{"errors": [], "sample_index": [2], "sample_id": ["id_1_1"]}	70	2299	add_ints_2018-09-06T13-57-24/root/sink	4
13558	failing_network_2018-09-06T14-00-46/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_1			2018-09-06 12:00:48.644911	2018-09-06 12:00:48.719417	{"sample_index": [1], "sample_id": ["id_1"], "errors": []}	71	2307	failing_network_2018-09-06T14-00-46/root/const_step_1_fail_2_0	2
13545	add_ints_2018-09-06T13-57-24/root/add/add_ints___add___id_1_0			2018-09-06 11:58:09.596479	2018-09-06 11:59:03.265119	{"errors": [], "sample_index": [1], "sample_id": ["id_1_0"]}	70	2300	add_ints_2018-09-06T13-57-24/root/add	4
13557	failing_network_2018-09-06T14-00-46/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_0			2018-09-06 12:00:48.489373	2018-09-06 12:00:48.561307	{"sample_index": [0], "sample_id": ["id_0"], "errors": []}	71	2307	failing_network_2018-09-06T14-00-46/root/const_step_1_fail_2_0	2
13550	add_ints_2018-09-06T13-57-24/root/sink/add_ints___sink___id_1_0___0			2018-09-06 11:58:09.985094	2018-09-06 11:59:03.429358	{"errors": [], "sample_index": [1], "sample_id": ["id_1_0"]}	70	2299	add_ints_2018-09-06T13-57-24/root/sink	4
13549	add_ints_2018-09-06T13-57-24/root/sink/add_ints___sink___id_0___0			2018-09-06 11:58:09.908798	2018-09-06 11:59:03.564095	{"errors": [], "sample_index": [0], "sample_id": ["id_0"]}	70	2299	add_ints_2018-09-06T13-57-24/root/sink	4
13544	add_ints_2018-09-06T13-57-24/root/add/add_ints___add___id_0			2018-09-06 11:58:09.524361	2018-09-06 11:59:03.69421	{"errors": [], "sample_index": [0], "sample_id": ["id_0"]}	70	2300	add_ints_2018-09-06T13-57-24/root/add	4
13546	add_ints_2018-09-06T13-57-24/root/add/add_ints___add___id_1_1			2018-09-06 11:58:09.669534	2018-09-06 11:59:03.856124	{"errors": [], "sample_index": [2], "sample_id": ["id_1_1"]}	70	2300	add_ints_2018-09-06T13-57-24/root/add	4
13547	add_ints_2018-09-06T13-57-24/root/add/add_ints___add___id_1_2			2018-09-06 11:58:09.743	2018-09-06 11:59:04.010905	{"errors": [], "sample_index": [3], "sample_id": ["id_1_2"]}	70	2300	add_ints_2018-09-06T13-57-24/root/add	4
13552	add_ints_2018-09-06T13-57-24/root/sink/add_ints___sink___id_1_2___0			2018-09-06 11:58:10.138551	2018-09-06 11:59:04.169014	{"errors": [], "sample_index": [3], "sample_id": ["id_1_2"]}	70	2299	add_ints_2018-09-06T13-57-24/root/sink	4
13561	failing_network_2018-09-06T14-00-46/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_0			2018-09-06 12:00:49.126857	2018-09-06 12:00:49.19978	{"sample_index": [0], "sample_id": ["id_0"], "errors": []}	71	2303	failing_network_2018-09-06T14-00-46/root/const_step_2_fail_1_0	2
13560	failing_network_2018-09-06T14-00-46/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_3			2018-09-06 12:00:48.949774	2018-09-06 12:00:49.022534	{"sample_index": [3], "sample_id": ["id_3"], "errors": []}	71	2307	failing_network_2018-09-06T14-00-46/root/const_step_1_fail_2_0	2
13559	failing_network_2018-09-06T14-00-46/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_2			2018-09-06 12:00:48.801822	2018-09-06 12:00:48.868231	{"sample_index": [2], "sample_id": ["id_2"], "errors": []}	71	2307	failing_network_2018-09-06T14-00-46/root/const_step_1_fail_2_0	2
13562	failing_network_2018-09-06T14-00-46/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_1			2018-09-06 12:00:49.284228	2018-09-06 12:00:49.355375	{"sample_index": [1], "sample_id": ["id_1"], "errors": []}	71	2303	failing_network_2018-09-06T14-00-46/root/const_step_2_fail_1_0	2
13556	failing_network_2018-09-06T14-00-46/root/source_1/failing_network___source_1___sample_1			2018-09-06 12:00:48.383714	2018-09-06 12:01:59.15002	{"sample_index": [0], "sample_id": ["sample_1"], "errors": []}	71	2316	failing_network_2018-09-06T14-00-46/root/source_1	2
13554	failing_network_2018-09-06T14-00-46/root/source_2/failing_network___source_2___sample_1			2018-09-06 12:00:48.186056	2018-09-06 12:02:03.074261	{"sample_index": [0], "sample_id": ["sample_1"], "errors": []}	71	2313	failing_network_2018-09-06T14-00-46/root/source_2	2
13564	failing_network_2018-09-06T14-00-46/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_3			2018-09-06 12:00:49.596565	2018-09-06 12:00:49.666342	{"sample_index": [3], "sample_id": ["id_3"], "errors": []}	71	2303	failing_network_2018-09-06T14-00-46/root/const_step_2_fail_1_0	2
13567	failing_network_2018-09-06T14-00-46/root/step_1/failing_network___step_1___sample_1_2			2018-09-06 12:02:09.074059	2018-09-06 12:03:01.688068	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	71	2306	failing_network_2018-09-06T14-00-46/root/step_1	2
13565	failing_network_2018-09-06T14-00-46/root/step_1/failing_network___step_1___sample_1_0			2018-09-06 12:02:08.905861	2018-09-06 12:03:01.61097	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2306	failing_network_2018-09-06T14-00-46/root/step_1	2
13566	failing_network_2018-09-06T14-00-46/root/step_1/failing_network___step_1___sample_1_1			2018-09-06 12:02:08.994182	2018-09-06 12:03:00.992106	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	71	2306	failing_network_2018-09-06T14-00-46/root/step_1	3
13580	failing_network_2018-09-06T14-00-46/root/step_3/failing_network___step_3___sample_1_3			2018-09-06 12:02:10.155942	2018-09-06 12:03:02.382252	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	71	2302	failing_network_2018-09-06T14-00-46/root/step_3	4
13588	failing_network_2018-09-06T14-00-46/root/range/failing_network___range___sample_1_3			2018-09-06 12:02:10.807052	2018-09-06 12:03:02.514826	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	71	2310	failing_network_2018-09-06T14-00-46/root/range	4
13570	failing_network_2018-09-06T14-00-46/root/step_2/failing_network___step_2___sample_1_1			2018-09-06 12:02:09.32952	2018-09-06 12:03:00.628019	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	71	2308	failing_network_2018-09-06T14-00-46/root/step_2	2
13574	failing_network_2018-09-06T14-00-46/root/sink_1/failing_network___sink_1___sample_1_1___0			2018-09-06 12:02:09.655893	2018-09-06 12:03:01.063096	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	71	2312	failing_network_2018-09-06T14-00-46/root/sink_1	4
13572	failing_network_2018-09-06T14-00-46/root/step_2/failing_network___step_2___sample_1_3			2018-09-06 12:02:09.482976	2018-09-06 12:03:00.736725	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	71	2308	failing_network_2018-09-06T14-00-46/root/step_2	3
13573	failing_network_2018-09-06T14-00-46/root/sink_1/failing_network___sink_1___sample_1_0___0			2018-09-06 12:02:09.575071	2018-09-06 12:04:00.935969	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2312	failing_network_2018-09-06T14-00-46/root/sink_1	2
13578	failing_network_2018-09-06T14-00-46/root/step_3/failing_network___step_3___sample_1_1			2018-09-06 12:02:09.995149	2018-09-06 12:03:01.196818	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	71	2302	failing_network_2018-09-06T14-00-46/root/step_3	4
13589	failing_network_2018-09-06T14-00-46/root/sink_3/failing_network___sink_3___sample_1_0___0			2018-09-06 12:02:10.897417	2018-09-06 12:05:00.200916	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2309	failing_network_2018-09-06T14-00-46/root/sink_3	2
13575	failing_network_2018-09-06T14-00-46/root/sink_1/failing_network___sink_1___sample_1_2___0			2018-09-06 12:02:09.735259	2018-09-06 12:04:05.062232	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	71	2312	failing_network_2018-09-06T14-00-46/root/sink_1	2
13590	failing_network_2018-09-06T14-00-46/root/sink_3/failing_network___sink_3___sample_1_1___0			2018-09-06 12:02:10.97384	2018-09-06 12:03:01.4617	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	71	2309	failing_network_2018-09-06T14-00-46/root/sink_3	4
13579	failing_network_2018-09-06T14-00-46/root/step_3/failing_network___step_3___sample_1_2			2018-09-06 12:02:10.078758	2018-09-06 12:03:01.7568	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	71	2302	failing_network_2018-09-06T14-00-46/root/step_3	4
13591	failing_network_2018-09-06T14-00-46/root/sink_3/failing_network___sink_3___sample_1_2___0			2018-09-06 12:02:11.051012	2018-09-06 12:03:02.027413	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	71	2309	failing_network_2018-09-06T14-00-46/root/sink_3	4
13584	failing_network_2018-09-06T14-00-46/root/sink_2/failing_network___sink_2___sample_1_3___0			2018-09-06 12:02:10.48805	2018-09-06 12:03:00.823997	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	71	2314	failing_network_2018-09-06T14-00-46/root/sink_2	4
13577	failing_network_2018-09-06T14-00-46/root/step_3/failing_network___step_3___sample_1_0			2018-09-06 12:02:09.916032	2018-09-06 12:03:56.736815	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2302	failing_network_2018-09-06T14-00-46/root/step_3	2
13585	failing_network_2018-09-06T14-00-46/root/range/failing_network___range___sample_1_0			2018-09-06 12:02:10.578244	2018-09-06 12:04:56.132247	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2310	failing_network_2018-09-06T14-00-46/root/range	2
13587	failing_network_2018-09-06T14-00-46/root/range/failing_network___range___sample_1_2			2018-09-06 12:02:10.735599	2018-09-06 12:03:01.895487	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	71	2310	failing_network_2018-09-06T14-00-46/root/range	4
13586	failing_network_2018-09-06T14-00-46/root/range/failing_network___range___sample_1_1			2018-09-06 12:02:10.655617	2018-09-06 12:03:01.329607	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	71	2310	failing_network_2018-09-06T14-00-46/root/range	4
13568	failing_network_2018-09-06T14-00-46/root/step_1/failing_network___step_1___sample_1_3			2018-09-06 12:02:09.149374	2018-09-06 12:03:02.180434	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	71	2306	failing_network_2018-09-06T14-00-46/root/step_1	3
13582	failing_network_2018-09-06T14-00-46/root/sink_2/failing_network___sink_2___sample_1_1___0			2018-09-06 12:02:10.334204	2018-09-06 12:03:56.854235	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	71	2314	failing_network_2018-09-06T14-00-46/root/sink_2	2
13571	failing_network_2018-09-06T14-00-46/root/step_2/failing_network___step_2___sample_1_2			2018-09-06 12:02:09.405381	2018-09-06 12:02:59.373571	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	71	2308	failing_network_2018-09-06T14-00-46/root/step_2	3
13576	failing_network_2018-09-06T14-00-46/root/sink_1/failing_network___sink_1___sample_1_3___0			2018-09-06 12:02:09.811152	2018-09-06 12:03:02.250427	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	71	2312	failing_network_2018-09-06T14-00-46/root/sink_1	4
13592	failing_network_2018-09-06T14-00-46/root/sink_3/failing_network___sink_3___sample_1_3___0			2018-09-06 12:02:11.128706	2018-09-06 12:03:02.643697	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	71	2309	failing_network_2018-09-06T14-00-46/root/sink_3	4
13581	failing_network_2018-09-06T14-00-46/root/sink_2/failing_network___sink_2___sample_1_0___0			2018-09-06 12:02:10.250815	2018-09-06 12:03:56.941359	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2314	failing_network_2018-09-06T14-00-46/root/sink_2	2
13583	failing_network_2018-09-06T14-00-46/root/sink_2/failing_network___sink_2___sample_1_2___0			2018-09-06 12:02:10.412807	2018-09-06 12:02:59.452079	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	71	2314	failing_network_2018-09-06T14-00-46/root/sink_2	4
13569	failing_network_2018-09-06T14-00-46/root/step_2/failing_network___step_2___sample_1_0			2018-09-06 12:02:09.252859	2018-09-06 12:03:00.535307	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2308	failing_network_2018-09-06T14-00-46/root/step_2	2
13595	failing_network_2018-09-06T14-00-46/root/sum/failing_network___sum___sample_1_2			2018-09-06 12:05:01.291068	2018-09-06 12:05:02.85149	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	71	2311	failing_network_2018-09-06T14-00-46/root/sum	4
13593	failing_network_2018-09-06T14-00-46/root/sum/failing_network___sum___sample_1_0			2018-09-06 12:05:01.14875	2018-09-06 12:05:55.687881	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2311	failing_network_2018-09-06T14-00-46/root/sum	2
13594	failing_network_2018-09-06T14-00-46/root/sum/failing_network___sum___sample_1_1			2018-09-06 12:05:01.221941	2018-09-06 12:05:02.700823	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	71	2311	failing_network_2018-09-06T14-00-46/root/sum	4
13610	failing_network_2018-09-06T14-00-46/root/sink_5/failing_network___sink_5___sample_1_1___0			2018-09-06 12:05:02.436656	2018-09-06 12:05:03.842282	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	71	2315	failing_network_2018-09-06T14-00-46/root/sink_5	4
13597	failing_network_2018-09-06T14-00-46/root/sink_4/failing_network___sink_4___sample_1_0___0			2018-09-06 12:05:01.453622	2018-09-06 12:06:01.685784	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2304	failing_network_2018-09-06T14-00-46/root/sink_4	2
13609	failing_network_2018-09-06T14-00-46/root/sink_5/failing_network___sink_5___sample_1_0___0			2018-09-06 12:05:02.362295	2018-09-06 12:06:58.981057	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2315	failing_network_2018-09-06T14-00-46/root/sink_5	2
13611	failing_network_2018-09-06T14-00-46/root/sink_5/failing_network___sink_5___sample_1_2___0			2018-09-06 12:05:02.508455	2018-09-06 12:05:03.984429	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	71	2315	failing_network_2018-09-06T14-00-46/root/sink_5	4
13615	failing_network_2018-09-06T15-03-16/root/source_1/failing_network___source_1___sample_1			2018-09-06 13:03:17.517383	2018-09-06 13:05:07.132151	{"sample_index": [0], "sample_id": ["sample_1"], "errors": []}	73	2364	failing_network_2018-09-06T15-03-16/root/source_1	2
13619	failing_network_2018-09-06T15-03-16/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_3			2018-09-06 13:03:18.144341	2018-09-06 13:03:18.222026	{"sample_index": [3], "sample_id": ["id_3"], "errors": []}	73	2362	failing_network_2018-09-06T15-03-16/root/const_step_1_fail_2_0	2
13613	failing_network_2018-09-06T15-03-16/root/source_3/failing_network___source_3___sample_1			2018-09-06 13:03:17.304531	2018-09-06 13:04:40.802351	{"sample_index": [0], "sample_id": ["sample_1"], "errors": []}	73	2367	failing_network_2018-09-06T15-03-16/root/source_3	2
13612	failing_network_2018-09-06T14-00-46/root/sink_5/failing_network___sink_5___sample_1_3___0			2018-09-06 12:05:02.579922	2018-09-06 12:05:04.165218	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	71	2315	failing_network_2018-09-06T14-00-46/root/sink_5	4
13599	failing_network_2018-09-06T14-00-46/root/sink_4/failing_network___sink_4___sample_1_0___2			2018-09-06 12:05:01.590226	2018-09-06 12:05:55.247023	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2304	failing_network_2018-09-06T14-00-46/root/sink_4	2
13601	failing_network_2018-09-06T14-00-46/root/sink_4/failing_network___sink_4___sample_1_0___4			2018-09-06 12:05:01.733345	2018-09-06 12:05:55.334654	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2304	failing_network_2018-09-06T14-00-46/root/sink_4	2
13604	failing_network_2018-09-06T14-00-46/root/sink_4/failing_network___sink_4___sample_1_0___7			2018-09-06 12:05:01.953335	2018-09-06 12:05:55.41382	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2304	failing_network_2018-09-06T14-00-46/root/sink_4	2
13617	failing_network_2018-09-06T15-03-16/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_1			2018-09-06 13:03:17.799986	2018-09-06 13:03:17.886239	{"sample_index": [1], "sample_id": ["id_1"], "errors": []}	73	2362	failing_network_2018-09-06T15-03-16/root/const_step_1_fail_2_0	2
13602	failing_network_2018-09-06T14-00-46/root/sink_4/failing_network___sink_4___sample_1_0___5			2018-09-06 12:05:01.800552	2018-09-06 12:05:55.774568	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2304	failing_network_2018-09-06T14-00-46/root/sink_4	2
13616	failing_network_2018-09-06T15-03-16/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_0			2018-09-06 13:03:17.62652	2018-09-06 13:03:17.700036	{"sample_index": [0], "sample_id": ["id_0"], "errors": []}	73	2362	failing_network_2018-09-06T15-03-16/root/const_step_1_fail_2_0	2
13596	failing_network_2018-09-06T14-00-46/root/sum/failing_network___sum___sample_1_3			2018-09-06 12:05:01.358664	2018-09-06 12:05:02.995602	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	71	2311	failing_network_2018-09-06T14-00-46/root/sum	4
13606	failing_network_2018-09-06T14-00-46/root/sink_4/failing_network___sink_4___sample_1_1___0			2018-09-06 12:05:02.118973	2018-09-06 12:05:03.36901	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	71	2304	failing_network_2018-09-06T14-00-46/root/sink_4	4
13607	failing_network_2018-09-06T14-00-46/root/sink_4/failing_network___sink_4___sample_1_2___0			2018-09-06 12:05:02.198018	2018-09-06 12:05:03.511129	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	71	2304	failing_network_2018-09-06T14-00-46/root/sink_4	4
13608	failing_network_2018-09-06T14-00-46/root/sink_4/failing_network___sink_4___sample_1_3___0			2018-09-06 12:05:02.274984	2018-09-06 12:05:03.651109	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	71	2304	failing_network_2018-09-06T14-00-46/root/sink_4	4
13600	failing_network_2018-09-06T14-00-46/root/sink_4/failing_network___sink_4___sample_1_0___3			2018-09-06 12:05:01.663989	2018-09-06 12:05:56.134394	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2304	failing_network_2018-09-06T14-00-46/root/sink_4	2
13598	failing_network_2018-09-06T14-00-46/root/sink_4/failing_network___sink_4___sample_1_0___1			2018-09-06 12:05:01.520865	2018-09-06 12:05:56.655596	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2304	failing_network_2018-09-06T14-00-46/root/sink_4	2
13603	failing_network_2018-09-06T14-00-46/root/sink_4/failing_network___sink_4___sample_1_0___6			2018-09-06 12:05:01.878221	2018-09-06 12:05:56.734419	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2304	failing_network_2018-09-06T14-00-46/root/sink_4	2
13605	failing_network_2018-09-06T14-00-46/root/sink_4/failing_network___sink_4___sample_1_0___8			2018-09-06 12:05:02.03094	2018-09-06 12:06:01.067051	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	71	2304	failing_network_2018-09-06T14-00-46/root/sink_4	2
13618	failing_network_2018-09-06T15-03-16/root/const_step_1_fail_2_0/failing_network___const_step_1_fail_2_0___id_2			2018-09-06 13:03:17.977031	2018-09-06 13:03:18.052698	{"sample_index": [2], "sample_id": ["id_2"], "errors": []}	73	2362	failing_network_2018-09-06T15-03-16/root/const_step_1_fail_2_0	2
13621	failing_network_2018-09-06T15-03-16/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_1			2018-09-06 13:03:18.693332	2018-09-06 13:03:18.771409	{"sample_index": [1], "sample_id": ["id_1"], "errors": []}	73	2365	failing_network_2018-09-06T15-03-16/root/const_step_2_fail_1_0	2
13620	failing_network_2018-09-06T15-03-16/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_0			2018-09-06 13:03:18.332453	2018-09-06 13:03:18.402698	{"sample_index": [0], "sample_id": ["id_0"], "errors": []}	73	2365	failing_network_2018-09-06T15-03-16/root/const_step_2_fail_1_0	2
13622	failing_network_2018-09-06T15-03-16/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_2			2018-09-06 13:03:18.861782	2018-09-06 13:03:18.932065	{"sample_index": [2], "sample_id": ["id_2"], "errors": []}	73	2365	failing_network_2018-09-06T15-03-16/root/const_step_2_fail_1_0	2
13614	failing_network_2018-09-06T15-03-16/root/source_2/failing_network___source_2___sample_1			2018-09-06 13:03:17.410597	2018-09-06 13:04:40.679102	{"sample_index": [0], "sample_id": ["sample_1"], "errors": []}	73	2372	failing_network_2018-09-06T15-03-16/root/source_2	2
13623	failing_network_2018-09-06T15-03-16/root/const_step_2_fail_1_0/failing_network___const_step_2_fail_1_0___id_3			2018-09-06 13:03:19.021066	2018-09-06 13:03:19.094665	{"sample_index": [3], "sample_id": ["id_3"], "errors": []}	73	2365	failing_network_2018-09-06T15-03-16/root/const_step_2_fail_1_0	2
13635	failing_network_2018-09-06T15-03-16/root/step_3/failing_network___step_3___sample_1_3			2018-09-06 13:05:08.376715	2018-09-06 13:05:44.856255	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	73	2371	failing_network_2018-09-06T15-03-16/root/step_3	4
13625	failing_network_2018-09-06T15-03-16/root/step_2/failing_network___step_2___sample_1_1			2018-09-06 13:05:07.50109	2018-09-06 13:05:39.46479	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	73	2366	failing_network_2018-09-06T15-03-16/root/step_2	2
13627	failing_network_2018-09-06T15-03-16/root/step_2/failing_network___step_2___sample_1_3			2018-09-06 13:05:07.670202	2018-09-06 13:05:39.562967	{"sample_id": ["sample_1_3"], "hostinfo": {"username": "tkroes", "cwd": "/home/tkroes/python_356", "os": {"version": {"libc": {"version": "2.9", "libname": "glibc"}, "distname": "debian", "id": "", "version": "jessie/sid"}, "system": "Linux", "maxsize": 9223372036854775807, "is64bits": 1}, "drmaa": {"taskid": "undefined", "jobname": "fastr_failing_network___step_2___sample_1_3", "jobid": "2816426"}, "uname": ["Linux", "res-hpc-gpu02", "4.4.0-119-generic", "#143~14.04.1-Ubuntu SMP Mon Apr 2 18:04:36 UTC 2018", "x86_64", "x86_64"], "envvar": {"JOB_ID": "2816426", "SGE_O_HOME": "/home/tkroes", "LC_NUMERIC": "nl_NL.UTF-8", "_": "/home/tkroes/python_356/venv/bin/python", "HISTTIMEFORMAT": "'%a, %d %b %Y %l:%M:%S%p %z '", "LC_ADDRESS": "nl_NL.UTF-8", "LOADEDMODULES": "OpenGridScheduler:History", "PS1": "(venv) \\\\[\\\\e]0;\\\\u@\\\\h: \\\\w\\\\a\\\\]${debian_chroot:+($debian_chroot)}\\\\u@\\\\h:\\\\w\\\\$ ", "MODULE_VERSION_STACK": "3.2.10", "REQNAME": "fastr_failing_network___step_2___sample_1_3", "LC_TIME": "nl_NL.UTF-8", "RESTARTED": "0", "COMP_WORDBREAKS": " \\t\\n\\"'><;|&(:", "SGE_O_MAIL": "/var/mail/tkroes", "J2SDKDIR": "/usr/lib/jvm/java-8-oracle", "SGE_CWD_PATH": "/home/tkroes/python_356", "SHLVL": "1", "LS_COLORS": "rs", "_LMFILES_": "/usr/local/Modules/3.2.10/modulefiles/OpenGridScheduler:/usr/local/Modules/3.2.10/modulefiles/History", "JAVA_HOME": "/usr/lib/jvm/java-8-oracle", "HISTSIZE": "1000", "SGE_TASK_ID": "undefined", "DRMAA_LIBRARY_PATH": "/usr/local/OpenGridScheduler/gridengine/lib/linux-x64/libdrmaa.so.1.0", "SGE_STDOUT_PATH": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_failing_network_2018-09-06T15-03-16_lgalkkq4/step_2/sample_1_3/__fastr_stdout__.txt", "HOME": "/home/tkroes", "SSH_CLIENT": "145.88.35.10 55252 22", "DISPLAY": "localhost:18.0", "PERL5LIB": "/usr/share/perl5/:/usr/share/perl/:/usr/local/ensembl-api-56/ensembl/modules:/usr/local/ensembl-api-56/ensembl-compara/modules:/usr/local/ensembl-api-56/ensembl-variation/modules:/usr/local/ensembl-api-56/ensembl-functgenomics/modules:/usr/local/MedStat/privatePerl:/usr/local/bcl2fastq/bcl2fastq_v1.8.4/bcl2fastq_v1.8.4-build/lib/bcl2fastq-1.8.4/perl:/usr/local/vcftools/current/share/perl/5.18.2:/usr/lib/perl5/", "VIRTUAL_ENV": "/home/tkroes/python_356/venv", "SGE_EXECD_PORT": "6445", "LD_LIBRARY_PATH": "/usr/local/OpenGridScheduler/gridengine/lib/linux-x64/:/usr/local/jemalloc/current/lib/:/usr/local/lib/:/usr/local/ScaLAPACK/scalapack-2.0.2/lib/", "HISTFILESIZE": "2000", "NQUEUES": "1", "PBS_O_INITDIR": "/home/tkroes/python_356", "LESSCLOSE": "/usr/bin/lesspipe %s %s", "LANGUAGE": "en_US:", "ARC": "linux-x64", "JOB_SCRIPT": "/home/tkroes/python_356/venv/bin/python", "LC_MONETARY": "nl_NL.UTF-8", "MANPATH": "/usr/local/OpenGridScheduler/gridengine/man:/usr/share/man:/usr/local/share/man", "NHOSTS": "1", "TERM": "xterm-256color", "TMP": "/tmp/2816426.1.all.q", "LESSOPEN": "| /usr/bin/lesspipe %s", "SGE_O_WORKDIR": "/home/tkroes/python_356", "TMPDIR": "/tmp/2816426.1.all.q", "MODULE_VERSION": "3.2.10", "JOB_NAME": "fastr_failing_network___step_2___sample_1_3", "MODULESHOME": "/usr/local/Modules/3.2.10", "SGE_O_PATH": "/home/tkroes/python_356/venv/bin:/usr/local/OpenGridScheduler/gridengine/bin/linux-x64:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/jvm/java-8-oracle/bin:/usr/lib/jvm/java-8-oracle/db/bin:/usr/lib/jvm/java-8-oracle/jre/bin:/opt/dell/srvadmin/bin", "HISTCONTROL": "ignoreboth", "SGE_ACCOUNT": "sge", "J2REDIR": "/usr/lib/jvm/java-8-oracle/jre", "SGE_QMASTER_PORT": "6444", "LC_PAPER": "nl_NL.UTF-8", "LOGNAME": "tkroes", "SGE_ROOT": "/usr/local/OpenGridScheduler/gridengine", "SGE_BINARY_PATH": "/usr/local/OpenGridScheduler/gridengine/bin/linux-x64", "SGE_TASK_FIRST": "undefined", "SGE_JOB_SPOOL_DIR": "/var/spool/sge/res-hpc-gpu02/active_jobs/2816426.1", "LANG": "en_US.UTF-8", "USER": "tkroes", "SGE_ARCH": "linux-x64", "SHELL": "/bin/bash", "SGE_CELL": "default", "NSLOTS": "1", "LC_MEASUREMENT": "nl_NL.UTF-8", "LC_IDENTIFICATION": "nl_NL.UTF-8", "DERBY_HOME": "/usr/lib/jvm/java-8-oracle/db", "SGE_O_SHELL": "/bin/bash", "HOSTNAME": "res-hpc-gpu02.researchlumc.nl", "SGE_TASK_STEPSIZE": "undefined", "MODULEPATH": "/usr/local/Modules/versions:/usr/local/Modules/$MODULE_VERSION/modulefiles:/usr/local/Modules/modulefiles:/home/tkroes/tkroes/BBMRI/.local/bbmri/easybuild/modules/all", "SGE_STDERR_PATH": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_failing_network_2018-09-06T15-03-16_lgalkkq4/step_2/sample_1_3/__fastr_stderr__.txt", "PATH": "/tmp/2816426.1.all.q:/home/tkroes/python_356/venv/bin:/usr/local/OpenGridScheduler/gridengine/bin/linux-x64:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/jvm/java-8-oracle/bin:/usr/lib/jvm/java-8-oracle/db/bin:/usr/lib/jvm/java-8-oracle/jre/bin:/opt/dell/srvadmin/bin", "SSH_CONNECTION": "145.88.35.10 55252 145.88.66.103 22", "SGE_STDIN_PATH": "/dev/null", "QUEUE": "all.q", "SGE_O_HOST": "wingheadshark", "OLDPWD": "/home/tkroes", "SGE_O_LOGNAME": "tkroes", "ENVIRONMENT": "BATCH", "BASH_FUNC_module%%": "() {  eval `/usr/local/Modules/$MODULE_VERSION/bin/modulecmd bash $*`\\n}", "REQUEST": "fastr_failing_network___step_2___sample_1_3", "SGE_TASK_LAST": "undefined", "SGE_CLUSTER_NAME": "p6444", "LC_TELEPHONE": "nl_NL.UTF-8", "SSH_TTY": "/dev/pts/37", "LC_ALL": "en_US.UTF-8", "MAIL": "/var/mail/tkroes", "LC_NAME": "nl_NL.UTF-8", "PWD": "/home/tkroes/python_356", "KRB5CCNAME": "FILE:/tmp/krb5cc_144904_tLvS7Y"}, "python": {"version": "3.5.6", "implementation": "CPython", "branch": "", "revision": "", "compiler": "GCC 4.8.5"}}, "input_hash": null, "returncode": 0, "sample_index": [3], "tool_version": null, "time_elapsed": 0.05689096450805664, "stdout": "Namespace(fail_1=True, fail_2=False, in_1=9, in_2=9)\\nin 1  : 9\\nin 2  : 9\\nfail_1: True\\nfail_2: False\\nRESULT_2=[10]\\n", "tool_name": null, "command": ["python", "/home/tkroes/python_356/venv/lib/python3.5/site-packages/fastr/resources/tools/fastr/util/0.1/bin/fail.py", "--in_1", "9", "--in_2", "9", "--fail_1"], "output_hash": null, "stderr": "", "errors": [["FastrOutputValidationError", "Could not find result for output out_1", "/home/tkroes/python_356/venv/lib/python3.5/site-packages/fastr/execution/job.py", 990], ["FastrValueError", "Output values are not valid!", "/home/tkroes/python_356/venv/lib/python3.5/site-packages/fastr/execution/job.py", 749]]}	73	2366	failing_network_2018-09-06T15-03-16/root/step_2	3
13634	failing_network_2018-09-06T15-03-16/root/step_3/failing_network___step_3___sample_1_2			2018-09-06 13:05:08.291138	2018-09-06 13:05:44.182341	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	73	2371	failing_network_2018-09-06T15-03-16/root/step_3	4
13637	failing_network_2018-09-06T15-03-16/root/sink_2/failing_network___sink_2___sample_1_1___0			2018-09-06 13:05:08.537369	2018-09-06 13:06:48.74706	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	73	2360	failing_network_2018-09-06T15-03-16/root/sink_2	2
13639	failing_network_2018-09-06T15-03-16/root/sink_2/failing_network___sink_2___sample_1_3___0			2018-09-06 13:05:08.704071	2018-09-06 13:05:39.648344	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	73	2360	failing_network_2018-09-06T15-03-16/root/sink_2	4
13629	failing_network_2018-09-06T15-03-16/root/step_1/failing_network___step_1___sample_1_1			2018-09-06 13:05:07.859017	2018-09-06 13:05:43.355946	{"sample_id": ["sample_1_1"], "hostinfo": {"drmaa": {"taskid": "undefined", "jobname": "fastr_failing_network___step_1___sample_1_1", "jobid": "2816428"}, "cwd": "/home/tkroes/python_356", "os": {"version": {"version": "jessie/sid", "libc": {"version": "2.9", "libname": "glibc"}, "distname": "debian", "id": ""}, "system": "Linux", "is64bits": 1, "maxsize": 9223372036854775807}, "uname": ["Linux", "iridescentshark", "3.13.0-153-generic", "#203-Ubuntu SMP Thu Jun 14 08:52:28 UTC 2018", "x86_64", "x86_64"], "envvar": {"LC_NUMERIC": "nl_NL.UTF-8", "SGE_O_HOME": "/home/tkroes", "JOB_ID": "2816428", "_": "/home/tkroes/python_356/venv/bin/python", "HISTTIMEFORMAT": "'%a, %d %b %Y %l:%M:%S%p %z '", "NQUEUES": "1", "UPSTART_INSTANCE": "", "LOADEDMODULES": "OpenGridScheduler:History", "PS1": "(venv) \\\\[\\\\e]0;\\\\u@\\\\h: \\\\w\\\\a\\\\]${debian_chroot:+($debian_chroot)}\\\\u@\\\\h:\\\\w\\\\$ ", "MODULE_VERSION_STACK": "3.2.10", "REQNAME": "fastr_failing_network___step_1___sample_1_1", "LC_TIME": "nl_NL.UTF-8", "previous": "N", "J2REDIR": "/usr/lib/jvm/java-8-oracle/jre", "RESTARTED": "0", "COMP_WORDBREAKS": " \\t\\n\\"'><;|&(:", "SGE_O_MAIL": "/var/mail/tkroes", "J2SDKDIR": "/usr/lib/jvm/java-8-oracle", "SGE_CWD_PATH": "/home/tkroes/python_356", "SHLVL": "1", "_LMFILES_": "/usr/local/Modules/3.2.10/modulefiles/OpenGridScheduler:/usr/local/Modules/3.2.10/modulefiles/History", "JAVA_HOME": "/usr/lib/jvm/java-8-oracle", "HISTSIZE": "1000", "SGE_TASK_ID": "undefined", "DRMAA_LIBRARY_PATH": "/usr/local/OpenGridScheduler/gridengine/lib/linux-x64/libdrmaa.so.1.0", "SGE_STDOUT_PATH": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_failing_network_2018-09-06T15-03-16_lgalkkq4/step_1/sample_1_1/__fastr_stdout__.txt", "SGE_O_LOGNAME": "tkroes", "MAIL": "/var/mail/tkroes", "DISPLAY": "localhost:18.0", "PERL5LIB": "/usr/share/perl5/:/usr/share/perl/:/usr/local/ensembl-api-56/ensembl/modules:/usr/local/ensembl-api-56/ensembl-compara/modules:/usr/local/ensembl-api-56/ensembl-variation/modules:/usr/local/ensembl-api-56/ensembl-functgenomics/modules:/usr/local/MedStat/privatePerl:/usr/local/bcl2fastq/bcl2fastq_v1.8.4/bcl2fastq_v1.8.4-build/lib/bcl2fastq-1.8.4/perl:/usr/local/vcftools/current/share/perl/5.18.2:/usr/lib/perl5/", "VIRTUAL_ENV": "/home/tkroes/python_356/venv", "SGE_EXECD_PORT": "6445", "LD_LIBRARY_PATH": "/usr/local/OpenGridScheduler/gridengine/lib/linux-x64/:/usr/local/jemalloc/current/lib/:/usr/local/lib/:/usr/local/ScaLAPACK/scalapack-2.0.2/lib/", "HISTFILESIZE": "2000", "LC_ADDRESS": "nl_NL.UTF-8", "PBS_O_INITDIR": "/home/tkroes/python_356", "LESSCLOSE": "/usr/bin/lesspipe %s %s", "LANGUAGE": "en_US:", "ARC": "linux-x64", "UPSTART_EVENTS": "runlevel", "JOB_SCRIPT": "/home/tkroes/python_356/venv/bin/python", "LC_MONETARY": "nl_NL.UTF-8", "MANPATH": "/usr/local/OpenGridScheduler/gridengine/man:/usr/share/man:/usr/local/share/man", "TMP": "/tmp/2816428.1.all.q", "NHOSTS": "1", "TERM": "xterm-256color", "SGE_ARCH": "linux-x64", "LESSOPEN": "| /usr/bin/lesspipe %s", "SGE_O_WORKDIR": "/home/tkroes/python_356", "TMPDIR": "/tmp/2816428.1.all.q", "MODULE_VERSION": "3.2.10", "JOB_NAME": "fastr_failing_network___step_1___sample_1_1", "MODULESHOME": "/usr/local/Modules/3.2.10", "SGE_O_PATH": "/home/tkroes/python_356/venv/bin:/usr/local/OpenGridScheduler/gridengine/bin/linux-x64:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/jvm/java-8-oracle/bin:/usr/lib/jvm/java-8-oracle/db/bin:/usr/lib/jvm/java-8-oracle/jre/bin:/opt/dell/srvadmin/bin", "HISTCONTROL": "ignoreboth", "SGE_ACCOUNT": "sge", "SGE_TASK_LAST": "undefined", "SGE_QMASTER_PORT": "6444", "LC_PAPER": "nl_NL.UTF-8", "LOGNAME": "tkroes", "SGE_ROOT": "/usr/local/OpenGridScheduler/gridengine", "SGE_BINARY_PATH": "/usr/local/OpenGridScheduler/gridengine/bin/linux-x64", "SGE_JOB_SPOOL_DIR": "/var/spool/sge/iridescentshark/active_jobs/2816428.1", "SGE_TASK_FIRST": "undefined", "LS_COLORS": "rs", "SGE_CELL": "default", "QUEUE": "all.q", "runlevel": "2", "RUNLEVEL": "2", "SHELL": "/bin/bash", "USER": "tkroes", "SSH_CLIENT": "145.88.35.10 55252 22", "LC_MEASUREMENT": "nl_NL.UTF-8", "LC_IDENTIFICATION": "nl_NL.UTF-8", "DERBY_HOME": "/usr/lib/jvm/java-8-oracle/db", "SGE_O_SHELL": "/bin/bash", "HOSTNAME": "iridescentshark.researchlumc.nl", "SGE_TASK_STEPSIZE": "undefined", "LANG": "en_US.UTF-8", "MODULEPATH": "/usr/local/Modules/versions:/usr/local/Modules/$MODULE_VERSION/modulefiles:/usr/local/Modules/modulefiles:/home/tkroes/tkroes/BBMRI/.local/bbmri/easybuild/modules/all", "SGE_STDERR_PATH": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_failing_network_2018-09-06T15-03-16_lgalkkq4/step_1/sample_1_1/__fastr_stderr__.txt", "PATH": "/tmp/2816428.1.all.q:/home/tkroes/python_356/venv/bin:/usr/local/OpenGridScheduler/gridengine/bin/linux-x64:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/jvm/java-8-oracle/bin:/usr/lib/jvm/java-8-oracle/db/bin:/usr/lib/jvm/java-8-oracle/jre/bin:/opt/dell/srvadmin/bin", "SSH_CONNECTION": "145.88.35.10 55252 145.88.66.103 22", "SGE_STDIN_PATH": "/dev/null", "PREVLEVEL": "N", "SGE_O_HOST": "wingheadshark", "OLDPWD": "/home/tkroes", "HOME": "/home/tkroes", "ENVIRONMENT": "BATCH", "NSLOTS": "1", "BASH_FUNC_module%%": "() {  eval `/usr/local/Modules/$MODULE_VERSION/bin/modulecmd bash $*`\\n}", "REQUEST": "fastr_failing_network___step_1___sample_1_1", "UPSTART_JOB": "rc", "SGE_CLUSTER_NAME": "p6444", "LC_TELEPHONE": "nl_NL.UTF-8", "SSH_TTY": "/dev/pts/37", "LC_ALL": "en_US.UTF-8", "LC_NAME": "nl_NL.UTF-8", "PWD": "/home/tkroes/python_356", "KRB5CCNAME": "FILE:/tmp/krb5cc_144904_tLvS7Y"}, "username": "tkroes", "python": {"version": "3.5.6", "implementation": "CPython", "branch": "", "compiler": "GCC 4.8.5", "revision": ""}}, "input_hash": null, "returncode": 0, "sample_index": [1], "tool_version": null, "time_elapsed": 0.0649724006652832, "stdout": "Namespace(fail_1=False, fail_2=True, in_1=1, in_2=1)\\nin 1  : 1\\nin 2  : 1\\nfail_1: False\\nfail_2: True\\nRESULT_1=[2]\\n", "tool_name": null, "command": ["python", "/home/tkroes/python_356/venv/lib/python3.5/site-packages/fastr/resources/tools/fastr/util/0.1/bin/fail.py", "--in_1", "1", "--in_2", "1", "--fail_2"], "output_hash": null, "stderr": "", "errors": [["FastrOutputValidationError", "Could not find result for output out_2", "/home/tkroes/python_356/venv/lib/python3.5/site-packages/fastr/execution/job.py", 990], ["FastrValueError", "Output values are not valid!", "/home/tkroes/python_356/venv/lib/python3.5/site-packages/fastr/execution/job.py", 749]]}	73	2370	failing_network_2018-09-06T15-03-16/root/step_1	3
13642	failing_network_2018-09-06T15-03-16/root/sink_1/failing_network___sink_1___sample_1_2___0			2018-09-06 13:05:08.951946	2018-09-06 13:06:53.056657	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	73	2373	failing_network_2018-09-06T15-03-16/root/sink_1	2
13633	failing_network_2018-09-06T15-03-16/root/step_3/failing_network___step_3___sample_1_1			2018-09-06 13:05:08.192181	2018-09-06 13:05:43.443559	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	73	2371	failing_network_2018-09-06T15-03-16/root/step_3	4
13645	failing_network_2018-09-06T15-03-16/root/range/failing_network___range___sample_1_1			2018-09-06 13:05:09.244269	2018-09-06 13:05:43.588264	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	73	2363	failing_network_2018-09-06T15-03-16/root/range	4
13626	failing_network_2018-09-06T15-03-16/root/step_2/failing_network___step_2___sample_1_2			2018-09-06 13:05:07.588536	2018-09-06 13:05:44.113768	{"sample_id": ["sample_1_2"], "hostinfo": {"drmaa": {"taskid": "undefined", "jobname": "fastr_failing_network___step_2___sample_1_2", "jobid": "2816425"}, "cwd": "/home/tkroes/python_356", "os": {"version": {"libc": {"version": "2.9", "libname": "glibc"}, "distname": "debian", "id": "", "version": "jessie/sid"}, "system": "Linux", "maxsize": 9223372036854775807, "is64bits": 1}, "username": "tkroes", "uname": ["Linux", "chimerashark", "3.13.0-153-generic", "#203-Ubuntu SMP Thu Jun 14 08:52:28 UTC 2018", "x86_64", "x86_64"], "envvar": {"LC_NUMERIC": "nl_NL.UTF-8", "previous": "N", "PREVLEVEL": "N", "JOB_ID": "2816425", "_": "/home/tkroes/python_356/venv/bin/python", "HISTTIMEFORMAT": "'%a, %d %b %Y %l:%M:%S%p %z '", "LC_ADDRESS": "nl_NL.UTF-8", "RUNLEVEL": "2", "SGE_BINARY_PATH": "/usr/local/OpenGridScheduler/gridengine/bin/linux-x64", "PS1": "(venv) \\\\[\\\\e]0;\\\\u@\\\\h: \\\\w\\\\a\\\\]${debian_chroot:+($debian_chroot)}\\\\u@\\\\h:\\\\w\\\\$ ", "MODULE_VERSION_STACK": "3.2.10", "REQNAME": "fastr_failing_network___step_2___sample_1_2", "LC_TIME": "nl_NL.UTF-8", "SGE_O_HOME": "/home/tkroes", "RESTARTED": "0", "COMP_WORDBREAKS": " \\t\\n\\"'><;|&(:", "SGE_O_MAIL": "/var/mail/tkroes", "J2SDKDIR": "/usr/lib/jvm/java-8-oracle", "SGE_CWD_PATH": "/home/tkroes/python_356", "SHLVL": "1", "_LMFILES_": "/usr/local/Modules/3.2.10/modulefiles/OpenGridScheduler:/usr/local/Modules/3.2.10/modulefiles/History", "JAVA_HOME": "/usr/lib/jvm/java-8-oracle", "PBS_O_INITDIR": "/home/tkroes/python_356", "SGE_TASK_ID": "undefined", "DRMAA_LIBRARY_PATH": "/usr/local/OpenGridScheduler/gridengine/lib/linux-x64/libdrmaa.so.1.0", "SGE_STDOUT_PATH": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_failing_network_2018-09-06T15-03-16_lgalkkq4/step_2/sample_1_2/__fastr_stdout__.txt", "SGE_O_LOGNAME": "tkroes", "SSH_CLIENT": "145.88.35.10 55252 22", "DISPLAY": "localhost:18.0", "PERL5LIB": "/usr/share/perl5/:/usr/share/perl/:/usr/local/ensembl-api-56/ensembl/modules:/usr/local/ensembl-api-56/ensembl-compara/modules:/usr/local/ensembl-api-56/ensembl-variation/modules:/usr/local/ensembl-api-56/ensembl-functgenomics/modules:/usr/local/MedStat/privatePerl:/usr/local/bcl2fastq/bcl2fastq_v1.8.4/bcl2fastq_v1.8.4-build/lib/bcl2fastq-1.8.4/perl:/usr/local/vcftools/current/share/perl/5.18.2:/usr/lib/perl5/", "VIRTUAL_ENV": "/home/tkroes/python_356/venv", "SGE_EXECD_PORT": "6445", "LD_LIBRARY_PATH": "/usr/local/OpenGridScheduler/gridengine/lib/linux-x64/:/usr/local/jemalloc/current/lib/:/usr/local/lib/:/usr/local/ScaLAPACK/scalapack-2.0.2/lib/", "HISTFILESIZE": "2000", "NQUEUES": "1", "LOADEDMODULES": "OpenGridScheduler:History", "HISTSIZE": "1000", "LESSCLOSE": "/usr/bin/lesspipe %s %s", "LANGUAGE": "en_US:", "ARC": "linux-x64", "UPSTART_EVENTS": "runlevel", "JOB_SCRIPT": "/home/tkroes/python_356/venv/bin/python", "LC_MONETARY": "nl_NL.UTF-8", "MANPATH": "/usr/local/OpenGridScheduler/gridengine/man:/usr/share/man:/usr/local/share/man", "TMP": "/tmp/2816425.1.all.q", "LS_COLORS": "rs", "TERM": "xterm-256color", "SGE_ARCH": "linux-x64", "LESSOPEN": "| /usr/bin/lesspipe %s", "SGE_O_WORKDIR": "/home/tkroes/python_356", "TMPDIR": "/tmp/2816425.1.all.q", "MODULE_VERSION": "3.2.10", "JOB_NAME": "fastr_failing_network___step_2___sample_1_2", "MODULESHOME": "/usr/local/Modules/3.2.10", "SGE_O_PATH": "/home/tkroes/python_356/venv/bin:/usr/local/OpenGridScheduler/gridengine/bin/linux-x64:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/jvm/java-8-oracle/bin:/usr/lib/jvm/java-8-oracle/db/bin:/usr/lib/jvm/java-8-oracle/jre/bin:/opt/dell/srvadmin/bin", "HISTCONTROL": "ignoreboth", "SGE_ACCOUNT": "sge", "J2REDIR": "/usr/lib/jvm/java-8-oracle/jre", "SGE_QMASTER_PORT": "6444", "LC_PAPER": "nl_NL.UTF-8", "LOGNAME": "tkroes", "SGE_ROOT": "/usr/local/OpenGridScheduler/gridengine", "SGE_TASK_FIRST": "undefined", "SGE_JOB_SPOOL_DIR": "/var/spool/sge/chimerashark/active_jobs/2816425.1", "LANG": "en_US.UTF-8", "SGE_CELL": "default", "runlevel": "2", "UPSTART_INSTANCE": "", "SHELL": "/bin/bash", "USER": "tkroes", "NSLOTS": "1", "LC_MEASUREMENT": "nl_NL.UTF-8", "LC_IDENTIFICATION": "nl_NL.UTF-8", "DERBY_HOME": "/usr/lib/jvm/java-8-oracle/db", "UPSTART_JOB": "rc", "SGE_O_SHELL": "/bin/bash", "HOSTNAME": "chimerashark.researchlumc.nl", "SGE_TASK_STEPSIZE": "undefined", "NHOSTS": "1", "MODULEPATH": "/usr/local/Modules/versions:/usr/local/Modules/$MODULE_VERSION/modulefiles:/usr/local/Modules/modulefiles:/home/tkroes/tkroes/BBMRI/.local/bbmri/easybuild/modules/all", "SGE_STDERR_PATH": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_failing_network_2018-09-06T15-03-16_lgalkkq4/step_2/sample_1_2/__fastr_stderr__.txt", "PATH": "/tmp/2816425.1.all.q:/home/tkroes/python_356/venv/bin:/usr/local/OpenGridScheduler/gridengine/bin/linux-x64:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/jvm/java-8-oracle/bin:/usr/lib/jvm/java-8-oracle/db/bin:/usr/lib/jvm/java-8-oracle/jre/bin:/opt/dell/srvadmin/bin", "SSH_CONNECTION": "145.88.35.10 55252 145.88.66.103 22", "SGE_STDIN_PATH": "/dev/null", "QUEUE": "all.q", "SGE_O_HOST": "wingheadshark", "OLDPWD": "/home/tkroes", "HOME": "/home/tkroes", "ENVIRONMENT": "BATCH", "BASH_FUNC_module%%": "() {  eval `/usr/local/Modules/$MODULE_VERSION/bin/modulecmd bash $*`\\n}", "REQUEST": "fastr_failing_network___step_2___sample_1_2", "SGE_TASK_LAST": "undefined", "SGE_CLUSTER_NAME": "p6444", "LC_TELEPHONE": "nl_NL.UTF-8", "SSH_TTY": "/dev/pts/37", "LC_ALL": "en_US.UTF-8", "MAIL": "/var/mail/tkroes", "LC_NAME": "nl_NL.UTF-8", "PWD": "/home/tkroes/python_356", "KRB5CCNAME": "FILE:/tmp/krb5cc_144904_tLvS7Y"}, "python": {"version": "3.5.6", "implementation": "CPython", "branch": "", "revision": "", "compiler": "GCC 4.8.5"}}, "input_hash": null, "returncode": 0, "sample_index": [2], "tool_version": null, "time_elapsed": 0.1808795928955078, "stdout": "Namespace(fail_1=True, fail_2=False, in_1=12, in_2=12)\\nin 1  : 12\\nin 2  : 12\\nfail_1: True\\nfail_2: False\\nRESULT_2=[13]\\n", "tool_name": null, "command": ["python", "/home/tkroes/python_356/venv/lib/python3.5/site-packages/fastr/resources/tools/fastr/util/0.1/bin/fail.py", "--in_1", "12", "--in_2", "12", "--fail_1"], "output_hash": null, "stderr": "", "errors": [["FastrOutputValidationError", "Could not find result for output out_1", "/home/tkroes/python_356/venv/lib/python3.5/site-packages/fastr/execution/job.py", 990], ["FastrValueError", "Output values are not valid!", "/home/tkroes/python_356/venv/lib/python3.5/site-packages/fastr/execution/job.py", 749]]}	73	2366	failing_network_2018-09-06T15-03-16/root/step_2	3
13646	failing_network_2018-09-06T15-03-16/root/range/failing_network___range___sample_1_2			2018-09-06 13:05:09.322631	2018-09-06 13:05:44.317385	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	73	2363	failing_network_2018-09-06T15-03-16/root/range	4
13640	failing_network_2018-09-06T15-03-16/root/sink_1/failing_network___sink_1___sample_1_0___0			2018-09-06 13:05:08.794298	2018-09-06 13:06:37.584458	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2373	failing_network_2018-09-06T15-03-16/root/sink_1	2
13647	failing_network_2018-09-06T15-03-16/root/range/failing_network___range___sample_1_3			2018-09-06 13:05:09.404911	2018-09-06 13:05:45.002801	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	73	2363	failing_network_2018-09-06T15-03-16/root/range	4
13631	failing_network_2018-09-06T15-03-16/root/step_1/failing_network___step_1___sample_1_3			2018-09-06 13:05:08.009546	2018-09-06 13:05:44.781959	{"sample_id": ["sample_1_3"], "hostinfo": {"username": "tkroes", "cwd": "/home/tkroes/python_356", "os": {"version": {"version": "jessie/sid", "libc": {"version": "2.9", "libname": "glibc"}, "distname": "debian", "id": ""}, "system": "Linux", "is64bits": 1, "maxsize": 9223372036854775807}, "drmaa": {"taskid": "undefined", "jobname": "fastr_failing_network___step_1___sample_1_3", "jobid": "2816430"}, "uname": ["Linux", "cowshark", "3.13.0-153-generic", "#203-Ubuntu SMP Thu Jun 14 08:52:28 UTC 2018", "x86_64", "x86_64"], "envvar": {"JOB_ID": "2816430", "previous": "N", "PREVLEVEL": "N", "LC_NUMERIC": "nl_NL.UTF-8", "HISTTIMEFORMAT": "'%a, %d %b %Y %l:%M:%S%p %z '", "LC_ADDRESS": "nl_NL.UTF-8", "RUNLEVEL": "2", "LOADEDMODULES": "OpenGridScheduler:History", "PS1": "(venv) \\\\[\\\\e]0;\\\\u@\\\\h: \\\\w\\\\a\\\\]${debian_chroot:+($debian_chroot)}\\\\u@\\\\h:\\\\w\\\\$ ", "MODULE_VERSION_STACK": "3.2.10", "REQNAME": "fastr_failing_network___step_1___sample_1_3", "LC_TIME": "nl_NL.UTF-8", "SGE_O_HOME": "/home/tkroes", "RESTARTED": "0", "COMP_WORDBREAKS": " \\t\\n\\"'><;|&(:", "SGE_O_MAIL": "/var/mail/tkroes", "SGE_TASK_LAST": "undefined", "J2SDKDIR": "/usr/lib/jvm/java-8-oracle", "SGE_CWD_PATH": "/home/tkroes/python_356", "SHLVL": "1", "LS_COLORS": "rs", "_LMFILES_": "/usr/local/Modules/3.2.10/modulefiles/OpenGridScheduler:/usr/local/Modules/3.2.10/modulefiles/History", "JAVA_HOME": "/usr/lib/jvm/java-8-oracle", "HISTSIZE": "1000", "SGE_TASK_ID": "undefined", "DRMAA_LIBRARY_PATH": "/usr/local/OpenGridScheduler/gridengine/lib/linux-x64/libdrmaa.so.1.0", "SGE_STDOUT_PATH": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_failing_network_2018-09-06T15-03-16_lgalkkq4/step_1/sample_1_3/__fastr_stdout__.txt", "HOME": "/home/tkroes", "SSH_CLIENT": "145.88.35.10 55252 22", "DISPLAY": "localhost:18.0", "PERL5LIB": "/usr/share/perl5/:/usr/share/perl/:/usr/local/ensembl-api-56/ensembl/modules:/usr/local/ensembl-api-56/ensembl-compara/modules:/usr/local/ensembl-api-56/ensembl-variation/modules:/usr/local/ensembl-api-56/ensembl-functgenomics/modules:/usr/local/MedStat/privatePerl:/usr/local/bcl2fastq/bcl2fastq_v1.8.4/bcl2fastq_v1.8.4-build/lib/bcl2fastq-1.8.4/perl:/usr/local/vcftools/current/share/perl/5.18.2:/usr/lib/perl5/", "VIRTUAL_ENV": "/home/tkroes/python_356/venv", "SGE_EXECD_PORT": "6445", "LD_LIBRARY_PATH": "/usr/local/OpenGridScheduler/gridengine/lib/linux-x64/:/usr/local/jemalloc/current/lib/:/usr/local/lib/:/usr/local/ScaLAPACK/scalapack-2.0.2/lib/", "HISTFILESIZE": "2000", "NQUEUES": "1", "_": "/home/tkroes/python_356/venv/bin/python", "LESSCLOSE": "/usr/bin/lesspipe %s %s", "LANGUAGE": "en_US:", "ARC": "linux-x64", "UPSTART_EVENTS": "runlevel", "JOB_SCRIPT": "/home/tkroes/python_356/venv/bin/python", "PBS_O_INITDIR": "/home/tkroes/python_356", "LC_MONETARY": "nl_NL.UTF-8", "MANPATH": "/usr/local/OpenGridScheduler/gridengine/man:/usr/share/man:/usr/local/share/man", "TMP": "/tmp/2816430.1.all.q", "NHOSTS": "1", "TERM": "xterm-256color", "SGE_ARCH": "linux-x64", "LESSOPEN": "| /usr/bin/lesspipe %s", "SGE_O_WORKDIR": "/home/tkroes/python_356", "TMPDIR": "/tmp/2816430.1.all.q", "SGE_BINARY_PATH": "/usr/local/OpenGridScheduler/gridengine/bin/linux-x64", "JOB_NAME": "fastr_failing_network___step_1___sample_1_3", "MODULESHOME": "/usr/local/Modules/3.2.10", "SGE_O_PATH": "/home/tkroes/python_356/venv/bin:/usr/local/OpenGridScheduler/gridengine/bin/linux-x64:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/jvm/java-8-oracle/bin:/usr/lib/jvm/java-8-oracle/db/bin:/usr/lib/jvm/java-8-oracle/jre/bin:/opt/dell/srvadmin/bin", "HISTCONTROL": "ignoreboth", "SGE_ACCOUNT": "sge", "J2REDIR": "/usr/lib/jvm/java-8-oracle/jre", "SGE_QMASTER_PORT": "6444", "LC_PAPER": "nl_NL.UTF-8", "LOGNAME": "tkroes", "SGE_ROOT": "/usr/local/OpenGridScheduler/gridengine", "SGE_TASK_FIRST": "undefined", "SGE_JOB_SPOOL_DIR": "/var/spool/sge/cowshark/active_jobs/2816430.1", "LANG": "en_US.UTF-8", "USER": "tkroes", "runlevel": "2", "UPSTART_INSTANCE": "", "SHELL": "/bin/bash", "SGE_CELL": "default", "MAIL": "/var/mail/tkroes", "LC_MEASUREMENT": "nl_NL.UTF-8", "LC_IDENTIFICATION": "nl_NL.UTF-8", "DERBY_HOME": "/usr/lib/jvm/java-8-oracle/db", "SGE_O_SHELL": "/bin/bash", "HOSTNAME": "cowshark.researchlumc.nl", "SGE_TASK_STEPSIZE": "undefined", "MODULEPATH": "/usr/local/Modules/versions:/usr/local/Modules/$MODULE_VERSION/modulefiles:/usr/local/Modules/modulefiles:/home/tkroes/tkroes/BBMRI/.local/bbmri/easybuild/modules/all", "SGE_STDERR_PATH": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_failing_network_2018-09-06T15-03-16_lgalkkq4/step_1/sample_1_3/__fastr_stderr__.txt", "PATH": "/tmp/2816430.1.all.q:/home/tkroes/python_356/venv/bin:/usr/local/OpenGridScheduler/gridengine/bin/linux-x64:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/jvm/java-8-oracle/bin:/usr/lib/jvm/java-8-oracle/db/bin:/usr/lib/jvm/java-8-oracle/jre/bin:/opt/dell/srvadmin/bin", "SSH_CONNECTION": "145.88.35.10 55252 145.88.66.103 22", "SGE_STDIN_PATH": "/dev/null", "MODULE_VERSION": "3.2.10", "QUEUE": "all.q", "SGE_O_HOST": "wingheadshark", "OLDPWD": "/home/tkroes", "SGE_O_LOGNAME": "tkroes", "ENVIRONMENT": "BATCH", "NSLOTS": "1", "BASH_FUNC_module%%": "() {  eval `/usr/local/Modules/$MODULE_VERSION/bin/modulecmd bash $*`\\n}", "REQUEST": "fastr_failing_network___step_1___sample_1_3", "UPSTART_JOB": "rc", "SGE_CLUSTER_NAME": "p6444", "LC_TELEPHONE": "nl_NL.UTF-8", "SSH_TTY": "/dev/pts/37", "LC_ALL": "en_US.UTF-8", "LC_NAME": "nl_NL.UTF-8", "PWD": "/home/tkroes/python_356", "KRB5CCNAME": "FILE:/tmp/krb5cc_144904_tLvS7Y"}, "python": {"version": "3.5.6", "implementation": "CPython", "branch": "", "revision": "", "compiler": "GCC 4.8.5"}}, "input_hash": null, "returncode": 0, "sample_index": [3], "tool_version": null, "time_elapsed": 0.12555909156799316, "stdout": "Namespace(fail_1=False, fail_2=True, in_1=9, in_2=9)\\nin 1  : 9\\nin 2  : 9\\nfail_1: False\\nfail_2: True\\nRESULT_1=[10]\\n", "tool_name": null, "command": ["python", "/home/tkroes/python_356/venv/lib/python3.5/site-packages/fastr/resources/tools/fastr/util/0.1/bin/fail.py", "--in_1", "9", "--in_2", "9", "--fail_2"], "output_hash": null, "stderr": "", "errors": [["FastrOutputValidationError", "Could not find result for output out_2", "/home/tkroes/python_356/venv/lib/python3.5/site-packages/fastr/execution/job.py", 990], ["FastrValueError", "Output values are not valid!", "/home/tkroes/python_356/venv/lib/python3.5/site-packages/fastr/execution/job.py", 749]]}	73	2370	failing_network_2018-09-06T15-03-16/root/step_1	3
13632	failing_network_2018-09-06T15-03-16/root/step_3/failing_network___step_3___sample_1_0			2018-09-06 13:05:08.1115	2018-09-06 13:06:42.657611	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2371	failing_network_2018-09-06T15-03-16/root/step_3	2
13630	failing_network_2018-09-06T15-03-16/root/step_1/failing_network___step_1___sample_1_2			2018-09-06 13:05:07.934999	2018-09-06 13:05:44.025271	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	73	2370	failing_network_2018-09-06T15-03-16/root/step_1	2
13628	failing_network_2018-09-06T15-03-16/root/step_1/failing_network___step_1___sample_1_0			2018-09-06 13:05:07.780696	2018-09-06 13:05:43.258272	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2370	failing_network_2018-09-06T15-03-16/root/step_1	2
13636	failing_network_2018-09-06T15-03-16/root/sink_2/failing_network___sink_2___sample_1_0___0			2018-09-06 13:05:08.460288	2018-09-06 13:06:41.66346	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2360	failing_network_2018-09-06T15-03-16/root/sink_2	2
13644	failing_network_2018-09-06T15-03-16/root/range/failing_network___range___sample_1_0			2018-09-06 13:05:09.156332	2018-09-06 13:07:31.961331	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2363	failing_network_2018-09-06T15-03-16/root/range	2
13649	failing_network_2018-09-06T15-03-16/root/sink_3/failing_network___sink_3___sample_1_1___0			2018-09-06 13:05:09.577578	2018-09-06 13:05:43.734595	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	73	2368	failing_network_2018-09-06T15-03-16/root/sink_3	4
13648	failing_network_2018-09-06T15-03-16/root/sink_3/failing_network___sink_3___sample_1_0___0			2018-09-06 13:05:09.50034	2018-09-06 13:07:36.047602	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2368	failing_network_2018-09-06T15-03-16/root/sink_3	2
13650	failing_network_2018-09-06T15-03-16/root/sink_3/failing_network___sink_3___sample_1_2___0			2018-09-06 13:05:09.650847	2018-09-06 13:05:44.462261	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	73	2368	failing_network_2018-09-06T15-03-16/root/sink_3	4
13643	failing_network_2018-09-06T15-03-16/root/sink_1/failing_network___sink_1___sample_1_3___0			2018-09-06 13:05:09.064685	2018-09-06 13:05:45.304226	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	73	2373	failing_network_2018-09-06T15-03-16/root/sink_1	4
13641	failing_network_2018-09-06T15-03-16/root/sink_1/failing_network___sink_1___sample_1_1___0			2018-09-06 13:05:08.870459	2018-09-06 13:05:43.876602	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	73	2373	failing_network_2018-09-06T15-03-16/root/sink_1	4
13638	failing_network_2018-09-06T15-03-16/root/sink_2/failing_network___sink_2___sample_1_2___0			2018-09-06 13:05:08.621679	2018-09-06 13:05:44.605585	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	73	2360	failing_network_2018-09-06T15-03-16/root/sink_2	4
13624	failing_network_2018-09-06T15-03-16/root/step_2/failing_network___step_2___sample_1_0			2018-09-06 13:05:07.392241	2018-09-06 13:05:39.226617	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2366	failing_network_2018-09-06T15-03-16/root/step_2	2
13722	iris/root/6_qc_overview/46_t1_qc/job___001			2018-09-08 13:18:24.330537	2018-09-08 13:18:27.043591	{}	75	2451	iris/root/6_qc_overview/46_t1_qc	2
13651	failing_network_2018-09-06T15-03-16/root/sink_3/failing_network___sink_3___sample_1_3___0			2018-09-06 13:05:09.724612	2018-09-06 13:05:45.153064	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	73	2368	failing_network_2018-09-06T15-03-16/root/sink_3	4
13663	failing_network_2018-09-06T15-03-16/root/sink_4/failing_network___sink_4___sample_1_0___7			2018-09-06 13:07:37.88702	2018-09-06 13:08:33.900423	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2359	failing_network_2018-09-06T15-03-16/root/sink_4	2
13670	failing_network_2018-09-06T15-03-16/root/sink_5/failing_network___sink_5___sample_1_2___0			2018-09-06 13:07:38.517028	2018-09-06 13:07:40.229518	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	73	2369	failing_network_2018-09-06T15-03-16/root/sink_5	4
13661	failing_network_2018-09-06T15-03-16/root/sink_4/failing_network___sink_4___sample_1_0___5			2018-09-06 13:07:37.722796	2018-09-06 13:08:38.046863	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2359	failing_network_2018-09-06T15-03-16/root/sink_4	2
13671	failing_network_2018-09-06T15-03-16/root/sink_5/failing_network___sink_5___sample_1_3___0			2018-09-06 13:07:38.621948	2018-09-06 13:07:40.409113	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	73	2369	failing_network_2018-09-06T15-03-16/root/sink_5	4
13664	failing_network_2018-09-06T15-03-16/root/sink_4/failing_network___sink_4___sample_1_0___8			2018-09-06 13:07:37.969395	2018-09-06 13:08:32.875638	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2359	failing_network_2018-09-06T15-03-16/root/sink_4	2
13652	failing_network_2018-09-06T15-03-16/root/sum/failing_network___sum___sample_1_0			2018-09-06 13:07:36.968881	2018-09-06 13:08:33.233773	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2361	failing_network_2018-09-06T15-03-16/root/sum	2
13662	failing_network_2018-09-06T15-03-16/root/sink_4/failing_network___sink_4___sample_1_0___6			2018-09-06 13:07:37.80629	2018-09-06 13:08:38.143662	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2359	failing_network_2018-09-06T15-03-16/root/sink_4	2
13673	add_ints_2018-09-07T12-00-02/root/source/add_ints___source___id_1			2018-09-07 10:00:02.940364	2018-09-07 10:00:47.806375	{"errors": [], "sample_index": [1], "sample_id": ["id_1"]}	74	2376	add_ints_2018-09-07T12-00-02/root/source	4
13658	failing_network_2018-09-06T15-03-16/root/sink_4/failing_network___sink_4___sample_1_0___2			2018-09-06 13:07:37.479467	2018-09-06 13:08:42.85729	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2359	failing_network_2018-09-06T15-03-16/root/sink_4	2
13672	add_ints_2018-09-07T12-00-02/root/source/add_ints___source___id_0			2018-09-07 10:00:02.76986	2018-09-07 10:00:02.837384	{"errors": [], "sample_index": [0], "sample_id": ["id_0"]}	74	2376	add_ints_2018-09-07T12-00-02/root/source	2
13653	failing_network_2018-09-06T15-03-16/root/sum/failing_network___sum___sample_1_1			2018-09-06 13:07:37.06149	2018-09-06 13:07:38.757557	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	73	2361	failing_network_2018-09-06T15-03-16/root/sum	4
13656	failing_network_2018-09-06T15-03-16/root/sink_4/failing_network___sink_4___sample_1_0___0			2018-09-06 13:07:37.317044	2018-09-06 13:08:33.587377	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2359	failing_network_2018-09-06T15-03-16/root/sink_4	2
13659	failing_network_2018-09-06T15-03-16/root/sink_4/failing_network___sink_4___sample_1_0___3			2018-09-06 13:07:37.561797	2018-09-06 13:08:44.91066	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2359	failing_network_2018-09-06T15-03-16/root/sink_4	2
13657	failing_network_2018-09-06T15-03-16/root/sink_4/failing_network___sink_4___sample_1_0___1			2018-09-06 13:07:37.396855	2018-09-06 13:08:33.718573	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2359	failing_network_2018-09-06T15-03-16/root/sink_4	2
13660	failing_network_2018-09-06T15-03-16/root/sink_4/failing_network___sink_4___sample_1_0___4			2018-09-06 13:07:37.640055	2018-09-06 13:08:33.814035	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2359	failing_network_2018-09-06T15-03-16/root/sink_4	2
13654	failing_network_2018-09-06T15-03-16/root/sum/failing_network___sum___sample_1_2			2018-09-06 13:07:37.147714	2018-09-06 13:07:38.93508	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	73	2361	failing_network_2018-09-06T15-03-16/root/sum	4
13668	failing_network_2018-09-06T15-03-16/root/sink_5/failing_network___sink_5___sample_1_0___0			2018-09-06 13:07:38.338747	2018-09-06 13:09:42.16228	{"sample_index": [0], "sample_id": ["sample_1_0"], "errors": []}	73	2369	failing_network_2018-09-06T15-03-16/root/sink_5	2
13655	failing_network_2018-09-06T15-03-16/root/sum/failing_network___sum___sample_1_3			2018-09-06 13:07:37.227154	2018-09-06 13:07:39.093825	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	73	2361	failing_network_2018-09-06T15-03-16/root/sum	4
13665	failing_network_2018-09-06T15-03-16/root/sink_4/failing_network___sink_4___sample_1_1___0			2018-09-06 13:07:38.060968	2018-09-06 13:07:39.557834	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	73	2359	failing_network_2018-09-06T15-03-16/root/sink_4	4
13666	failing_network_2018-09-06T15-03-16/root/sink_4/failing_network___sink_4___sample_1_2___0			2018-09-06 13:07:38.161315	2018-09-06 13:07:39.729429	{"sample_index": [2], "sample_id": ["sample_1_2"], "errors": []}	73	2359	failing_network_2018-09-06T15-03-16/root/sink_4	4
13667	failing_network_2018-09-06T15-03-16/root/sink_4/failing_network___sink_4___sample_1_3___0			2018-09-06 13:07:38.248402	2018-09-06 13:07:39.885005	{"sample_index": [3], "sample_id": ["sample_1_3"], "errors": []}	73	2359	failing_network_2018-09-06T15-03-16/root/sink_4	4
13669	failing_network_2018-09-06T15-03-16/root/sink_5/failing_network___sink_5___sample_1_1___0			2018-09-06 13:07:38.425164	2018-09-06 13:07:40.06451	{"sample_index": [1], "sample_id": ["sample_1_1"], "errors": []}	73	2369	failing_network_2018-09-06T15-03-16/root/sink_5	4
13674	add_ints_2018-09-07T12-00-02/root/const_add_right_hand_0/add_ints___const_add_right_hand_0___id_0			2018-09-07 10:00:03.039972	2018-09-07 10:00:03.114999	{"errors": [], "sample_index": [0], "sample_id": ["id_0"]}	74	2375	add_ints_2018-09-07T12-00-02/root/const_add_right_hand_0	2
13677	iris/root/5_BET_and_REG/58_const_combine_brains_method_0/job___002			2018-09-08 13:18:13.329232	2018-09-08 13:19:06.473781	{}	75	2439	iris/root/5_BET_and_REG/58_const_combine_brains_method_0	2
13678	iris/root/5_BET_and_REG/13_const_threshold_labels_upper_threshold_0/job___002			2018-09-08 13:18:13.329232	2018-09-08 13:18:17.38027	{}	75	2421	iris/root/5_BET_and_REG/13_const_threshold_labels_upper_threshold_0	2
13676	iris/root/5_BET_and_REG/27_const_hammers_brain_transform_threads_0/job___001			2018-09-08 13:18:13.329232	2018-09-08 13:18:17.38027	{}	75	2426	iris/root/5_BET_and_REG/27_const_hammers_brain_transform_threads_0	2
13679	iris/root/5_BET_and_REG/47_const_convert_brain_component_type_0/job___002			2018-09-08 13:18:13.329232	2018-09-08 13:19:12.533929	{}	75	2434	iris/root/5_BET_and_REG/47_const_convert_brain_component_type_0	2
13682	iris/root/2_hammers_atlas/8_hammers_t1w/job___001			2018-09-08 13:18:13.329232	2018-09-08 13:19:12.533929	{}	75	2408	iris/root/2_hammers_atlas/8_hammers_t1w	2
13681	iris/root/5_BET_and_REG/10_const_bet_fraction_threshold_0/job___001			2018-09-08 13:18:13.329232	2018-09-08 13:18:17.38027	{}	75	2419	iris/root/5_BET_and_REG/10_const_bet_fraction_threshold_0	2
13680	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/74_const_invert_gm_parameters_0/job___002			2018-09-08 13:18:13.329232	2018-09-08 13:18:17.38027	{}	75	2466	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/74_const_invert_gm_parameters_0	2
13684	iris/root/2_hammers_atlas/64_hammers_brains_r5/job___001			2018-09-08 13:18:13.329232	2018-09-08 13:18:17.38027	{}	75	2411	iris/root/2_hammers_atlas/64_hammers_brains_r5	2
13685	iris/root/5_BET_and_REG/32_const_dilate_brainmask_operation_type_0/job___001			2018-09-08 13:18:14.68438	2018-09-08 13:18:17.38027	{}	75	2429	iris/root/5_BET_and_REG/32_const_dilate_brainmask_operation_type_0	2
13675	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/71_const_edit_transform_file_set_0/job___002			2018-09-08 13:18:13.329232	2018-09-08 13:18:18.767573	{}	75	2465	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/71_const_edit_transform_file_set_0	2
13683	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/78_const_reg_t1_to_spmtemplate_parameters_0/job___001			2018-09-08 13:18:13.329232	2018-09-08 13:19:13.89146	{}	75	2468	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/78_const_reg_t1_to_spmtemplate_parameters_0	2
13686	iris/root/0_statistics/26_const_get_wm_volume_volume_0/job___001			2018-09-08 13:18:14.68438	2018-09-08 13:19:16.544676	{}	75	2394	iris/root/0_statistics/26_const_get_wm_volume_volume_0	2
13728	iris/root/65_spm_tissue_quantification/69_outputs/84_csf_spm/job___001			2018-09-08 13:18:25.697602	2018-09-08 13:18:57.06775	{}	75	2480	iris/root/65_spm_tissue_quantification/69_outputs/84_csf_spm	2
13718	iris/root/4_output/57_hammer_brain_mask/job___002			2018-09-08 13:18:21.600793	2018-09-08 13:18:54.346661	{}	75	2417	iris/root/4_output/57_hammer_brain_mask	2
13695	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/86_edit_transform_file/job___002			2018-09-08 13:18:17.38027	2018-09-08 13:19:02.425715	{}	75	2473	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/86_edit_transform_file	2
13694	iris/root/2_hammers_atlas/50_hammers_brains/job___002			2018-09-08 13:18:17.38027	2018-09-08 13:18:17.38027	{}	75	2410	iris/root/2_hammers_atlas/50_hammers_brains	2
13696	iris/root/5_BET_and_REG/32_const_dilate_brainmask_operation_type_0/job___002			2018-09-08 13:18:17.38027	2018-09-08 13:18:17.38027	{}	75	2429	iris/root/5_BET_and_REG/32_const_dilate_brainmask_operation_type_0	2
13721	iris/root/6_qc_overview/21_t1_with_hammers/job___002			2018-09-08 13:18:24.330537	2018-09-08 13:18:50.24557	{}	75	2447	iris/root/6_qc_overview/21_t1_with_hammers	2
13693	iris/root/5_BET_and_REG/24_const_combine_labels_method_0/job___002			2018-09-08 13:18:14.68438	2018-09-08 13:19:16.544676	{}	75	2425	iris/root/5_BET_and_REG/24_const_combine_labels_method_0	2
13731	iris/root/6_qc_overview/62_t1_with_gm_outline_sink/job___002			2018-09-08 13:18:28.395929	2018-09-08 13:19:07.842096	{}	75	2453	iris/root/6_qc_overview/62_t1_with_gm_outline_sink	2
13719	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/job___002			2018-09-08 13:18:21.600793	2018-09-08 13:18:53.010202	{}	75	2469	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf	2
13732	iris/root/65_spm_tissue_quantification/69_outputs/73_wm_map/job___002			2018-09-08 13:18:28.395929	2018-09-08 13:19:01.053432	{}	75	2478	iris/root/65_spm_tissue_quantification/69_outputs/73_wm_map	2
13730	iris/root/6_qc_overview/54_t1_qc_sink/job___001			2018-09-08 13:18:27.043591	2018-09-08 13:18:31.123136	{}	75	2452	iris/root/6_qc_overview/54_t1_qc_sink	2
13698	iris/root/0_statistics/60_const_get_brain_volume_volume_0/job___001			2018-09-08 13:18:18.767573	2018-09-08 13:18:18.767573	{}	75	2401	iris/root/0_statistics/60_const_get_brain_volume_volume_0	2
13699	iris/root/0_statistics/41_const_mask_hammers_labels_operator_0/job___002			2018-09-08 13:18:18.767573	2018-09-08 13:18:18.767573	{}	75	2396	iris/root/0_statistics/41_const_mask_hammers_labels_operator_0	2
13702	iris/root/5_BET_and_REG/49_const_t1w_registration_parameters_0/job___002			2018-09-08 13:18:18.767573	2018-09-08 13:18:18.767573	{}	75	2435	iris/root/5_BET_and_REG/49_const_t1w_registration_parameters_0	2
13692	iris/root/5_BET_and_REG/53_const_label_registration_threads_0/job___002			2018-09-08 13:18:14.68438	2018-09-08 13:19:17.865501	{}	75	2437	iris/root/5_BET_and_REG/53_const_label_registration_threads_0	2
13723	iris/root/6_qc_overview/46_t1_qc/job___002			2018-09-08 13:18:24.330537	2018-09-08 13:18:27.043591	{}	75	2451	iris/root/6_qc_overview/46_t1_qc	2
13729	iris/root/6_qc_overview/37_t1_with_wm_outline_sink/job___001			2018-09-08 13:18:27.043591	2018-09-08 13:19:09.40607	{}	75	2449	iris/root/6_qc_overview/37_t1_with_wm_outline_sink	2
13724	iris/root/65_spm_tissue_quantification/69_outputs/90_wm_spm/job___002			2018-09-08 13:18:24.330537	2018-09-08 13:18:57.06775	{}	75	2482	iris/root/65_spm_tissue_quantification/69_outputs/90_wm_spm	2
13697	iris/root/5_BET_and_REG/7_threshold_labels/job___002			2018-09-08 13:18:18.767573	2018-09-08 13:18:50.24557	{}	75	2418	iris/root/5_BET_and_REG/7_threshold_labels	2
13725	iris/root/65_spm_tissue_quantification/69_outputs/84_csf_spm/job___002			2018-09-08 13:18:24.330537	2018-09-08 13:18:57.06775	{}	75	2480	iris/root/65_spm_tissue_quantification/69_outputs/84_csf_spm	2
13726	iris/root/65_spm_tissue_quantification/69_outputs/90_wm_spm/job___001			2018-09-08 13:18:24.330537	2018-09-08 13:18:57.06775	{}	75	2482	iris/root/65_spm_tissue_quantification/69_outputs/90_wm_spm	2
13727	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/job___001			2018-09-08 13:18:25.697602	2018-09-08 13:18:57.06775	{}	75	2471	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment	2
13700	iris/root/5_BET_and_REG/22_const_brain_mask_registation_parameters_0/job___002			2018-09-08 13:18:18.767573	2018-09-08 13:18:18.767573	{}	75	2424	iris/root/5_BET_and_REG/22_const_brain_mask_registation_parameters_0	2
13701	iris/root/1_dicom_to_nifti/61_const_t1_dicom_to_nifti_file_order_0/job___001			2018-09-08 13:18:18.767573	2018-09-08 13:18:18.767573	{}	75	2407	iris/root/1_dicom_to_nifti/61_const_t1_dicom_to_nifti_file_order_0	2
13703	iris/root/5_BET_and_REG/47_const_convert_brain_component_type_0/job___001			2018-09-08 13:18:18.767573	2018-09-08 13:18:18.767573	{}	75	2434	iris/root/5_BET_and_REG/47_const_convert_brain_component_type_0	2
13713	iris/root/5_BET_and_REG/35_const_dilate_brainmask_radius_0/job___001			2018-09-08 13:18:20.161172	2018-09-08 13:18:20.161172	{}	75	2431	iris/root/5_BET_and_REG/35_const_dilate_brainmask_radius_0	2
13717	iris/root/4_output/57_hammer_brain_mask/job___001			2018-09-08 13:18:21.600793	2018-09-08 13:19:17.865501	{}	75	2417	iris/root/4_output/57_hammer_brain_mask	2
13709	iris/root/0_statistics/11_get_gm_volume/job___002			2018-09-08 13:18:20.161172	2018-09-08 13:19:01.053432	{}	75	2389	iris/root/0_statistics/11_get_gm_volume	2
13708	iris/root/5_BET_and_REG/33_label_registration/job___002			2018-09-08 13:18:20.161172	2018-09-08 13:18:43.374024	{}	75	2430	iris/root/5_BET_and_REG/33_label_registration	2
13716	iris/root/5_BET_and_REG/28_n4_bias_correction/job___001			2018-09-08 13:18:21.600793	2018-09-08 13:18:32.479942	{}	75	2427	iris/root/5_BET_and_REG/28_n4_bias_correction	2
13715	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/job___001			2018-09-08 13:18:21.600793	2018-09-08 13:18:53.010202	{}	75	2475	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm	2
13714	iris/root/1_dicom_to_nifti/44_reformat_t1/job___002			2018-09-08 13:18:21.600793	2018-09-08 13:18:35.232791	{}	75	2406	iris/root/1_dicom_to_nifti/44_reformat_t1	2
13706	iris/root/3_subjects/34_t1w_dicom/job___002			2018-09-08 13:18:20.161172	2018-09-08 13:18:20.161172	{}	75	2412	iris/root/3_subjects/34_t1w_dicom	2
13707	iris/root/65_spm_tissue_quantification/67_InputImage/75_t1_passthrough/job___002			2018-09-08 13:18:20.161172	2018-09-08 13:19:03.799795	{}	75	2462	iris/root/65_spm_tissue_quantification/67_InputImage/75_t1_passthrough	2
13704	iris/root/0_statistics/25_wm_volume/job___001			2018-09-08 13:18:20.161172	2018-09-08 13:19:05.143426	{}	75	2393	iris/root/0_statistics/25_wm_volume	2
13711	iris/root/5_BET_and_REG/59_const_t1w_registration_threads_0/job___001			2018-09-08 13:18:20.161172	2018-09-08 13:18:20.161172	{}	75	2440	iris/root/5_BET_and_REG/59_const_t1w_registration_threads_0	2
13712	iris/root/0_statistics/52_const_get_brain_volume_selection_0/job___002			2018-09-08 13:18:20.161172	2018-09-08 13:18:20.161172	{}	75	2399	iris/root/0_statistics/52_const_get_brain_volume_selection_0	2
13710	iris/root/5_BET_and_REG/38_threshold_brain/job___001			2018-09-08 13:18:20.161172	2018-09-08 13:18:42.004717	{}	75	2432	iris/root/5_BET_and_REG/38_threshold_brain	2
13733	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation/job___002			2018-09-08 13:18:28.395929	2018-09-08 13:18:42.004717	{}	75	2464	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation	2
13734	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation/job___001			2018-09-08 13:18:29.768818	2018-09-08 13:18:43.374024	{}	75	2464	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation	2
\.


--
-- Name: jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.jobs_id_seq', 13873, true);


--
-- Data for Name: link_node_refs; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.link_node_refs (id, node_id, node_path, created) FROM stdin;
\.


--
-- Name: link_node_refs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.link_node_refs_id_seq', 58053, true);


--
-- Data for Name: links; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.links (id, title, description, created, modified, custom_data, run_id, from_port_id, to_port_id, data_type, common_ancestor) FROM stdin;
2241	link_0		2018-09-13 13:55:07.022216	2018-09-13 13:55:07.022222	{"expand": false, "collapse": []}	76	add_ints_2018-09-13T15-55-06/root/source/output	add_ints_2018-09-13T15-55-06/root/add/left_hand	\N	add_ints_2018-09-13T15-55-06/root
2242	link_1		2018-09-13 13:55:07.023838	2018-09-13 13:55:07.023842	{"expand": false, "collapse": []}	76	add_ints_2018-09-13T15-55-06/root/const_add_right_hand_0/output	add_ints_2018-09-13T15-55-06/root/add/right_hand	\N	add_ints_2018-09-13T15-55-06/root
2243	link_2		2018-09-13 13:55:07.024625	2018-09-13 13:55:07.024629	{"expand": false, "collapse": []}	76	add_ints_2018-09-13T15-55-06/root/add/result	add_ints_2018-09-13T15-55-06/root/sink/input	\N	add_ints_2018-09-13T15-55-06/root
2193	dress		2018-09-08 13:18:13.197919	2018-09-08 13:18:13.197924	{}	75	iris/root/65_spm_tissue_quantification/69_outputs/80_gm_map/out_sink	iris/root/4_output/40_gm_map_output/in_input	NiftiImageFileCompressed	iris/root
2194	cells		2018-09-08 13:18:13.198569	2018-09-08 13:18:13.198573	{}	75	iris/root/0_statistics/129_const_get_gm_volume_selection_0/out_output	iris/root/0_statistics/11_get_gm_volume/in_image	TxtFile	iris/root/0_statistics
2195	evidence		2018-09-08 13:18:13.199161	2018-09-08 13:18:13.199177	{}	75	iris/root/0_statistics/39_const_get_gm_volume_volume_0/out_output	iris/root/0_statistics/11_get_gm_volume/in_stddev	String	iris/root/0_statistics
2196	forty		2018-09-08 13:18:13.199902	2018-09-08 13:18:13.199906	{}	75	iris/root/6_qc_overview/42_t1_with_gm_outline/out_png_image	iris/root/6_qc_overview/62_t1_with_gm_outline_sink/in_input	PngImageFile	iris/root/6_qc_overview
2197	guidelines		2018-09-08 13:18:13.200536	2018-09-08 13:18:13.20054	{}	75	iris/root/2_hammers_atlas/8_hammers_t1w/out_output	iris/root/5_BET_and_REG/12_t1w_registration/in_moving_image	NiftiImageFileCompressed	iris/root
2198	seamanship		2018-09-08 13:18:13.20113	2018-09-08 13:18:13.201134	{}	75	iris/root/1_dicom_to_nifti/44_reformat_t1/out_reformatted_image	iris/root/5_BET_and_REG/28_n4_bias_correction/in_n4_parameters	NiftiImageFileCompressed	iris/root
2199	securities		2018-09-08 13:18:13.201925	2018-09-08 13:18:13.201929	{}	75	iris/root/5_BET_and_REG/35_const_dilate_brainmask_radius_0/out_output	iris/root/5_BET_and_REG/125_dilate_brainmask/in_boundary_condition	Float	iris/root/5_BET_and_REG
2234	currency		2018-09-08 13:18:13.222891	2018-09-08 13:18:13.222895	{}	75	iris/root/1_dicom_to_nifti/20_t1_dicom_to_nifti/out_image	iris/root/1_dicom_to_nifti/44_reformat_t1/in_image	NiftiImageFileCompressed	iris/root/1_dicom_to_nifti
1812	link_13		2018-08-22 09:29:02.294757	2018-08-22 09:29:02.294764	{"collapse": [], "expand": false}	32	failing_network_2018-08-22T11-29-01/root/range/result	failing_network_2018-08-22T11-29-01/root/sink_4/input	\N	failing_network_2018-08-22T11-29-01/root
1813	link_12		2018-08-22 09:29:02.296775	2018-08-22 09:29:02.29678	{"collapse": [], "expand": false}	32	failing_network_2018-08-22T11-29-01/root/step_3/out_1	failing_network_2018-08-22T11-29-01/root/sink_3/input	\N	failing_network_2018-08-22T11-29-01/root
1814	link_11		2018-08-22 09:29:02.297715	2018-08-22 09:29:02.29772	{"collapse": [], "expand": false}	32	failing_network_2018-08-22T11-29-01/root/step_2/out_2	failing_network_2018-08-22T11-29-01/root/sink_2/input	\N	failing_network_2018-08-22T11-29-01/root
1815	link_10		2018-08-22 09:29:02.298565	2018-08-22 09:29:02.298569	{"collapse": [], "expand": false}	32	failing_network_2018-08-22T11-29-01/root/step_1/out_1	failing_network_2018-08-22T11-29-01/root/sink_1/input	\N	failing_network_2018-08-22T11-29-01/root
1816	link_14		2018-08-22 09:29:02.299387	2018-08-22 09:29:02.299393	{"collapse": [], "expand": false}	32	failing_network_2018-08-22T11-29-01/root/sum/result	failing_network_2018-08-22T11-29-01/root/sink_5/input	\N	failing_network_2018-08-22T11-29-01/root
1817	link_9		2018-08-22 09:29:02.300211	2018-08-22 09:29:02.300215	{"collapse": [], "expand": false}	32	failing_network_2018-08-22T11-29-01/root/range/result	failing_network_2018-08-22T11-29-01/root/sum/values	\N	failing_network_2018-08-22T11-29-01/root
1818	link_8		2018-08-22 09:29:02.301015	2018-08-22 09:29:02.301019	{"collapse": [], "expand": false}	32	failing_network_2018-08-22T11-29-01/root/step_3/out_1	failing_network_2018-08-22T11-29-01/root/range/value	\N	failing_network_2018-08-22T11-29-01/root
1819	link_3		2018-08-22 09:29:02.301806	2018-08-22 09:29:02.30181	{"collapse": [], "expand": false}	32	failing_network_2018-08-22T11-29-01/root/source_3/output	failing_network_2018-08-22T11-29-01/root/step_2/in_1	\N	failing_network_2018-08-22T11-29-01/root
1820	link_2		2018-08-22 09:29:02.302538	2018-08-22 09:29:02.302555	{"collapse": [], "expand": false}	32	failing_network_2018-08-22T11-29-01/root/const_step_1_fail_2_0/output	failing_network_2018-08-22T11-29-01/root/step_1/fail_2	\N	failing_network_2018-08-22T11-29-01/root
1821	link_1		2018-08-22 09:29:02.303137	2018-08-22 09:29:02.303141	{"collapse": [], "expand": false}	32	failing_network_2018-08-22T11-29-01/root/source_2/output	failing_network_2018-08-22T11-29-01/root/step_1/in_2	\N	failing_network_2018-08-22T11-29-01/root
1822	link_0		2018-08-22 09:29:02.303748	2018-08-22 09:29:02.303752	{"collapse": [], "expand": false}	32	failing_network_2018-08-22T11-29-01/root/source_1/output	failing_network_2018-08-22T11-29-01/root/step_1/in_1	\N	failing_network_2018-08-22T11-29-01/root
1823	link_7		2018-08-22 09:29:02.30452	2018-08-22 09:29:02.304524	{"collapse": [], "expand": false}	32	failing_network_2018-08-22T11-29-01/root/step_2/out_1	failing_network_2018-08-22T11-29-01/root/step_3/in_2	\N	failing_network_2018-08-22T11-29-01/root
1824	link_6		2018-08-22 09:29:02.305093	2018-08-22 09:29:02.305097	{"collapse": [], "expand": false}	32	failing_network_2018-08-22T11-29-01/root/step_1/out_2	failing_network_2018-08-22T11-29-01/root/step_3/in_1	\N	failing_network_2018-08-22T11-29-01/root
1825	link_5		2018-08-22 09:29:02.305732	2018-08-22 09:29:02.305736	{"collapse": [], "expand": false}	32	failing_network_2018-08-22T11-29-01/root/const_step_2_fail_1_0/output	failing_network_2018-08-22T11-29-01/root/step_2/fail_1	\N	failing_network_2018-08-22T11-29-01/root
1826	link_4		2018-08-22 09:29:02.306331	2018-08-22 09:29:02.306335	{"collapse": [], "expand": false}	32	failing_network_2018-08-22T11-29-01/root/source_1/output	failing_network_2018-08-22T11-29-01/root/step_2/in_2	\N	failing_network_2018-08-22T11-29-01/root
1827	link_13		2018-08-22 10:10:17.552082	2018-08-22 10:10:17.552088	{"collapse": [], "expand": false}	33	failing_network_2018-08-22T12-10-16/root/range/result	failing_network_2018-08-22T12-10-16/root/sink_4/input	\N	failing_network_2018-08-22T12-10-16/root
1828	link_12		2018-08-22 10:10:17.553994	2018-08-22 10:10:17.553998	{"collapse": [], "expand": false}	33	failing_network_2018-08-22T12-10-16/root/step_3/out_1	failing_network_2018-08-22T12-10-16/root/sink_3/input	\N	failing_network_2018-08-22T12-10-16/root
1829	link_11		2018-08-22 10:10:17.554756	2018-08-22 10:10:17.55476	{"collapse": [], "expand": false}	33	failing_network_2018-08-22T12-10-16/root/step_2/out_2	failing_network_2018-08-22T12-10-16/root/sink_2/input	\N	failing_network_2018-08-22T12-10-16/root
1830	link_10		2018-08-22 10:10:17.5555	2018-08-22 10:10:17.555503	{"collapse": [], "expand": false}	33	failing_network_2018-08-22T12-10-16/root/step_1/out_1	failing_network_2018-08-22T12-10-16/root/sink_1/input	\N	failing_network_2018-08-22T12-10-16/root
1831	link_14		2018-08-22 10:10:17.556289	2018-08-22 10:10:17.556293	{"collapse": [], "expand": false}	33	failing_network_2018-08-22T12-10-16/root/sum/result	failing_network_2018-08-22T12-10-16/root/sink_5/input	\N	failing_network_2018-08-22T12-10-16/root
1832	link_9		2018-08-22 10:10:17.557022	2018-08-22 10:10:17.557026	{"collapse": [], "expand": false}	33	failing_network_2018-08-22T12-10-16/root/range/result	failing_network_2018-08-22T12-10-16/root/sum/values	\N	failing_network_2018-08-22T12-10-16/root
1833	link_8		2018-08-22 10:10:17.557969	2018-08-22 10:10:17.557973	{"collapse": [], "expand": false}	33	failing_network_2018-08-22T12-10-16/root/step_3/out_1	failing_network_2018-08-22T12-10-16/root/range/value	\N	failing_network_2018-08-22T12-10-16/root
1834	link_3		2018-08-22 10:10:17.558658	2018-08-22 10:10:17.558662	{"collapse": [], "expand": false}	33	failing_network_2018-08-22T12-10-16/root/source_3/output	failing_network_2018-08-22T12-10-16/root/step_2/in_1	\N	failing_network_2018-08-22T12-10-16/root
1835	link_2		2018-08-22 10:10:17.5592	2018-08-22 10:10:17.559204	{"collapse": [], "expand": false}	33	failing_network_2018-08-22T12-10-16/root/const_step_1_fail_2_0/output	failing_network_2018-08-22T12-10-16/root/step_1/fail_2	\N	failing_network_2018-08-22T12-10-16/root
1836	link_1		2018-08-22 10:10:17.559775	2018-08-22 10:10:17.559779	{"collapse": [], "expand": false}	33	failing_network_2018-08-22T12-10-16/root/source_2/output	failing_network_2018-08-22T12-10-16/root/step_1/in_2	\N	failing_network_2018-08-22T12-10-16/root
1837	link_0		2018-08-22 10:10:17.560521	2018-08-22 10:10:17.560524	{"collapse": [], "expand": false}	33	failing_network_2018-08-22T12-10-16/root/source_1/output	failing_network_2018-08-22T12-10-16/root/step_1/in_1	\N	failing_network_2018-08-22T12-10-16/root
1838	link_7		2018-08-22 10:10:17.561102	2018-08-22 10:10:17.561106	{"collapse": [], "expand": false}	33	failing_network_2018-08-22T12-10-16/root/step_2/out_1	failing_network_2018-08-22T12-10-16/root/step_3/in_2	\N	failing_network_2018-08-22T12-10-16/root
1839	link_6		2018-08-22 10:10:17.561699	2018-08-22 10:10:17.561703	{"collapse": [], "expand": false}	33	failing_network_2018-08-22T12-10-16/root/step_1/out_2	failing_network_2018-08-22T12-10-16/root/step_3/in_1	\N	failing_network_2018-08-22T12-10-16/root
1840	link_5		2018-08-22 10:10:17.562243	2018-08-22 10:10:17.562247	{"collapse": [], "expand": false}	33	failing_network_2018-08-22T12-10-16/root/const_step_2_fail_1_0/output	failing_network_2018-08-22T12-10-16/root/step_2/fail_1	\N	failing_network_2018-08-22T12-10-16/root
1841	link_4		2018-08-22 10:10:17.562813	2018-08-22 10:10:17.562817	{"collapse": [], "expand": false}	33	failing_network_2018-08-22T12-10-16/root/source_1/output	failing_network_2018-08-22T12-10-16/root/step_2/in_2	\N	failing_network_2018-08-22T12-10-16/root
1842	link_13		2018-08-22 10:15:04.89872	2018-08-22 10:15:04.898725	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range/result	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_4/input	\N	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro
1843	link_12		2018-08-22 10:15:04.899444	2018-08-22 10:15:04.899448	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/out_1	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_3/input	\N	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro
1844	link_11		2018-08-22 10:15:04.900157	2018-08-22 10:15:04.900161	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/out_2	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_2/input	\N	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro
1845	link_10		2018-08-22 10:15:04.900773	2018-08-22 10:15:04.900777	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/out_1	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_1/input	\N	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro
1846	link_14		2018-08-22 10:15:04.901488	2018-08-22 10:15:04.901492	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sum/result	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_5/input	\N	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro
2189	adviser		2018-09-08 13:18:13.195209	2018-09-08 13:18:13.195213	{}	75	iris/root/5_BET_and_REG/121_combine_brains/out_hard_segment	iris/root/0_statistics/14_get_brain_volume/in_mask	NiftiImageFileCompressed	iris/root
1847	link_9		2018-08-22 10:15:04.902191	2018-08-22 10:15:04.902196	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range/result	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sum/values	\N	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro
1848	link_8		2018-08-22 10:15:04.902908	2018-08-22 10:15:04.902912	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/out_1	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range/value	\N	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro
1849	link_3		2018-08-22 10:15:04.90355	2018-08-22 10:15:04.903555	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_3/output	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/in_1	\N	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro
1850	link_2		2018-08-22 10:15:04.904109	2018-08-22 10:15:04.904113	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_1_fail_2_0/output	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/fail_2	\N	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro
1851	link_1		2018-08-22 10:15:04.904736	2018-08-22 10:15:04.90474	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_2/output	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/in_2	\N	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro
1852	link_0		2018-08-22 10:15:04.905304	2018-08-22 10:15:04.905308	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1/output	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/in_1	\N	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro
1853	link_7		2018-08-22 10:15:04.905919	2018-08-22 10:15:04.905923	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/out_1	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/in_2	\N	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro
1854	link_6		2018-08-22 10:15:04.906558	2018-08-22 10:15:04.906562	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/out_2	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/in_1	\N	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro
1855	link_5		2018-08-22 10:15:04.907072	2018-08-22 10:15:04.907076	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_2_fail_1_0/output	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/fail_1	\N	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro
1856	link_4		2018-08-22 10:15:04.907635	2018-08-22 10:15:04.907639	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1/output	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/in_2	\N	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro
1857	link_3		2018-08-22 10:15:04.908197	2018-08-22 10:15:04.9082	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_5/output	failing_macro_top_level_2018-08-22T12-15-04/root/add/left_hand	\N	failing_macro_top_level_2018-08-22T12-15-04/root
1858	link_2		2018-08-22 10:15:04.908908	2018-08-22 10:15:04.908911	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/source_c/output	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_3/input	\N	failing_macro_top_level_2018-08-22T12-15-04/root
1859	link_1		2018-08-22 10:15:04.909548	2018-08-22 10:15:04.909552	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/source_b/output	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_2/input	\N	failing_macro_top_level_2018-08-22T12-15-04/root
1860	link_0		2018-08-22 10:15:04.910142	2018-08-22 10:15:04.910146	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/source_a/output	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1/input	\N	failing_macro_top_level_2018-08-22T12-15-04/root
1861	link_5		2018-08-22 10:15:04.910734	2018-08-22 10:15:04.910738	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/add/result	failing_macro_top_level_2018-08-22T12-15-04/root/sink/input	\N	failing_macro_top_level_2018-08-22T12-15-04/root
1862	link_4		2018-08-22 10:15:04.911278	2018-08-22 10:15:04.911282	{"collapse": [], "expand": false}	34	failing_macro_top_level_2018-08-22T12-15-04/root/const_add_right_hand_0/output	failing_macro_top_level_2018-08-22T12-15-04/root/add/right_hand	\N	failing_macro_top_level_2018-08-22T12-15-04/root
1863	link_3		2018-08-22 10:36:33.165905	2018-08-22 10:36:33.165911	{"collapse": [], "expand": false}	35	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/const_addint2_right_hand_0/output	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2/right_hand	\N	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add
1864	link_2		2018-08-22 10:36:33.167404	2018-08-22 10:36:33.167408	{"collapse": [], "expand": false}	35	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1/result	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2/left_hand	\N	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add
1865	link_1		2018-08-22 10:36:33.16851	2018-08-22 10:36:33.168517	{"collapse": [], "expand": false}	35	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/const_addint1_right_hand_0/output	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1/right_hand	\N	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add
1866	link_0		2018-08-22 10:36:33.169384	2018-08-22 10:36:33.169388	{"collapse": [], "expand": false}	35	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input/output	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1/left_hand	\N	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1
1867	link_4		2018-08-22 10:36:33.170196	2018-08-22 10:36:33.1702	{"collapse": [], "expand": false}	35	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2/result	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink/input	\N	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1
1868	link_3		2018-08-22 10:36:33.170876	2018-08-22 10:36:33.170879	{"collapse": [], "expand": false}	35	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/const_addint2_right_hand_0/output	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2/right_hand	\N	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add
1869	link_2		2018-08-22 10:36:33.171525	2018-08-22 10:36:33.171528	{"collapse": [], "expand": false}	35	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1/result	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2/left_hand	\N	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add
2190	moistures		2018-09-08 13:18:13.195796	2018-09-08 13:18:13.1958	{}	75	iris/root/3_subjects/34_t1w_dicom/out_output	iris/root/1_dicom_to_nifti/20_t1_dicom_to_nifti/in_threshold	DicomImageFile	iris/root
1870	link_1		2018-08-22 10:36:33.172105	2018-08-22 10:36:33.172108	{"collapse": [], "expand": false}	35	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/const_addint1_right_hand_0/output	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1/right_hand	\N	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add
1871	link_0		2018-08-22 10:36:33.173005	2018-08-22 10:36:33.173009	{"collapse": [], "expand": false}	35	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input/output	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1/left_hand	\N	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2
1872	link_4		2018-08-22 10:36:33.173608	2018-08-22 10:36:33.173611	{"collapse": [], "expand": false}	35	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2/result	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/macro_sink/input	\N	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2
1873	link_2		2018-08-22 10:36:33.174164	2018-08-22 10:36:33.174167	{"collapse": [], "expand": false}	35	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/macro_sink/output	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/output_value/input	\N	macro_top_level_2018-08-22T12-36-32/root/node_add_ints
1874	link_1		2018-08-22 10:36:33.174609	2018-08-22 10:36:33.174612	{"collapse": [], "expand": false}	35	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink/output	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input/input	\N	macro_top_level_2018-08-22T12-36-32/root/node_add_ints
1875	link_0		2018-08-22 10:36:33.175114	2018-08-22 10:36:33.175117	{"collapse": [], "expand": false}	35	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value/output	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input/input	\N	macro_top_level_2018-08-22T12-36-32/root/node_add_ints
1876	link_1		2018-08-22 10:36:33.175563	2018-08-22 10:36:33.175567	{"collapse": [], "expand": false}	35	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/output_value/output	macro_top_level_2018-08-22T12-36-32/root/sink/input	\N	macro_top_level_2018-08-22T12-36-32/root
1877	link_0		2018-08-22 10:36:33.176071	2018-08-22 10:36:33.176074	{"collapse": [], "expand": false}	35	macro_top_level_2018-08-22T12-36-32/root/source/source/output	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value/input	\N	macro_top_level_2018-08-22T12-36-32/root
1878	link_2		2018-08-22 10:40:46.890392	2018-08-22 10:40:46.890398	{"collapse": [], "expand": false}	36	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/result	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/input	\N	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1
1879	link_1		2018-08-22 10:40:46.891372	2018-08-22 10:40:46.891376	{"collapse": [], "expand": false}	36	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_2/output	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/right_hand	\N	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1
1880	link_0		2018-08-22 10:40:46.892077	2018-08-22 10:40:46.892096	{"collapse": [], "expand": false}	36	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1/output	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/left_hand	\N	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1
1881	link_3		2018-08-22 10:40:46.892707	2018-08-22 10:40:46.892711	{"collapse": [], "expand": false}	36	macro_node_2_2018-08-22T12-40-46/root/source_3/output	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_2/input	\N	macro_node_2_2018-08-22T12-40-46/root
1882	link_2		2018-08-22 10:40:46.893266	2018-08-22 10:40:46.89327	{"collapse": [], "expand": false}	36	macro_node_2_2018-08-22T12-40-46/root/addint/result	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1/input	\N	macro_node_2_2018-08-22T12-40-46/root
1883	link_1		2018-08-22 10:40:46.893949	2018-08-22 10:40:46.893953	{"collapse": [], "expand": false}	36	macro_node_2_2018-08-22T12-40-46/root/source_2/output	macro_node_2_2018-08-22T12-40-46/root/addint/right_hand	\N	macro_node_2_2018-08-22T12-40-46/root
1884	link_0		2018-08-22 10:40:46.894629	2018-08-22 10:40:46.894633	{"collapse": [], "expand": false}	36	macro_node_2_2018-08-22T12-40-46/root/source_1/output	macro_node_2_2018-08-22T12-40-46/root/addint/left_hand	\N	macro_node_2_2018-08-22T12-40-46/root
1885	link_5		2018-08-22 10:40:46.895179	2018-08-22 10:40:46.895183	{"collapse": [], "expand": false}	36	macro_node_2_2018-08-22T12-40-46/root/sum/result	macro_node_2_2018-08-22T12-40-46/root/sink/input	\N	macro_node_2_2018-08-22T12-40-46/root
1886	link_4		2018-08-22 10:40:46.895849	2018-08-22 10:40:46.895854	{"collapse": [1], "expand": false}	36	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/output	macro_node_2_2018-08-22T12-40-46/root/sum/values	\N	macro_node_2_2018-08-22T12-40-46/root
1887	link_3		2018-08-22 13:29:33.805487	2018-08-22 13:29:33.805492	{"collapse": [], "expand": false}	37	cross_validation_2018-08-22T15-29-33/root/crossvalidtion/train	cross_validation_2018-08-22T15-29-33/root/multiply/left_hand	\N	cross_validation_2018-08-22T15-29-33/root
1888	link_2		2018-08-22 13:29:33.806502	2018-08-22 13:29:33.806506	{"collapse": [], "expand": false}	37	cross_validation_2018-08-22T15-29-33/root/const_crossvalidtion_number_of_folds_0/output	cross_validation_2018-08-22T15-29-33/root/crossvalidtion/number_of_folds	\N	cross_validation_2018-08-22T15-29-33/root
1889	link_1		2018-08-22 13:29:33.807131	2018-08-22 13:29:33.807135	{"collapse": [], "expand": false}	37	cross_validation_2018-08-22T15-29-33/root/const_crossvalidtion_method_0/output	cross_validation_2018-08-22T15-29-33/root/crossvalidtion/method	\N	cross_validation_2018-08-22T15-29-33/root
1890	link_0		2018-08-22 13:29:33.807852	2018-08-22 13:29:33.807856	{"collapse": [], "expand": false}	37	cross_validation_2018-08-22T15-29-33/root/numbers/output	cross_validation_2018-08-22T15-29-33/root/crossvalidtion/items	\N	cross_validation_2018-08-22T15-29-33/root
1891	link_6		2018-08-22 13:29:33.808548	2018-08-22 13:29:33.808552	{"collapse": [], "expand": false}	37	cross_validation_2018-08-22T15-29-33/root/sum/result	cross_validation_2018-08-22T15-29-33/root/sink/input	\N	cross_validation_2018-08-22T15-29-33/root
1892	link_5		2018-08-22 13:29:33.809263	2018-08-22 13:29:33.80927	{"collapse": ["numbers_train"], "expand": false}	37	cross_validation_2018-08-22T15-29-33/root/multiply/result	cross_validation_2018-08-22T15-29-33/root/sum/values	\N	cross_validation_2018-08-22T15-29-33/root
1893	link_4		2018-08-22 13:29:33.809918	2018-08-22 13:29:33.809921	{"collapse": [], "expand": false}	37	cross_validation_2018-08-22T15-29-33/root/crossvalidtion/test	cross_validation_2018-08-22T15-29-33/root/multiply/right_hand	\N	cross_validation_2018-08-22T15-29-33/root
1894	link_2		2018-08-22 13:39:07.344519	2018-08-22 13:39:07.344524	{"collapse": [], "expand": false}	38	input_groups_2018-08-22T15-39-07/root/add/result	input_groups_2018-08-22T15-39-07/root/sink/input	\N	input_groups_2018-08-22T15-39-07/root
1895	link_1		2018-08-22 13:39:07.345839	2018-08-22 13:39:07.345843	{"collapse": [], "expand": false}	38	input_groups_2018-08-22T15-39-07/root/const_add_right_hand_0/output	input_groups_2018-08-22T15-39-07/root/add/right_hand	\N	input_groups_2018-08-22T15-39-07/root
1896	link_0		2018-08-22 13:39:07.346533	2018-08-22 13:39:07.346537	{"collapse": [], "expand": false}	38	input_groups_2018-08-22T15-39-07/root/source/output	input_groups_2018-08-22T15-39-07/root/add/left_hand	\N	input_groups_2018-08-22T15-39-07/root
1930	link_0		2018-08-24 13:34:44.974846	2018-08-24 13:34:44.974851	{"expand": false, "collapse": []}	42	failing_network_2018-08-24T15-34-44/root/source_1/output	failing_network_2018-08-24T15-34-44/root/step_1/in_1	\N	failing_network_2018-08-24T15-34-44/root
1931	link_1		2018-08-24 13:34:44.975759	2018-08-24 13:34:44.975764	{"expand": false, "collapse": []}	42	failing_network_2018-08-24T15-34-44/root/source_2/output	failing_network_2018-08-24T15-34-44/root/step_1/in_2	\N	failing_network_2018-08-24T15-34-44/root
1932	link_2		2018-08-24 13:34:44.976469	2018-08-24 13:34:44.976473	{"expand": false, "collapse": []}	42	failing_network_2018-08-24T15-34-44/root/const_step_1_fail_2_0/output	failing_network_2018-08-24T15-34-44/root/step_1/fail_2	\N	failing_network_2018-08-24T15-34-44/root
1933	link_3		2018-08-24 13:34:44.977144	2018-08-24 13:34:44.977149	{"expand": false, "collapse": []}	42	failing_network_2018-08-24T15-34-44/root/source_3/output	failing_network_2018-08-24T15-34-44/root/step_2/in_1	\N	failing_network_2018-08-24T15-34-44/root
1934	link_4		2018-08-24 13:34:44.977891	2018-08-24 13:34:44.977895	{"expand": false, "collapse": []}	42	failing_network_2018-08-24T15-34-44/root/source_1/output	failing_network_2018-08-24T15-34-44/root/step_2/in_2	\N	failing_network_2018-08-24T15-34-44/root
1935	link_5		2018-08-24 13:34:44.978628	2018-08-24 13:34:44.978632	{"expand": false, "collapse": []}	42	failing_network_2018-08-24T15-34-44/root/const_step_2_fail_1_0/output	failing_network_2018-08-24T15-34-44/root/step_2/fail_1	\N	failing_network_2018-08-24T15-34-44/root
1936	link_6		2018-08-24 13:34:44.979415	2018-08-24 13:34:44.979419	{"expand": false, "collapse": []}	42	failing_network_2018-08-24T15-34-44/root/step_1/out_2	failing_network_2018-08-24T15-34-44/root/step_3/in_1	\N	failing_network_2018-08-24T15-34-44/root
1937	link_7		2018-08-24 13:34:44.980096	2018-08-24 13:34:44.980101	{"expand": false, "collapse": []}	42	failing_network_2018-08-24T15-34-44/root/step_2/out_1	failing_network_2018-08-24T15-34-44/root/step_3/in_2	\N	failing_network_2018-08-24T15-34-44/root
1938	link_8		2018-08-24 13:34:44.980678	2018-08-24 13:34:44.980682	{"expand": false, "collapse": []}	42	failing_network_2018-08-24T15-34-44/root/step_3/out_1	failing_network_2018-08-24T15-34-44/root/range/value	\N	failing_network_2018-08-24T15-34-44/root
1939	link_9		2018-08-24 13:34:44.98126	2018-08-24 13:34:44.981264	{"expand": false, "collapse": []}	42	failing_network_2018-08-24T15-34-44/root/range/result	failing_network_2018-08-24T15-34-44/root/sum/values	\N	failing_network_2018-08-24T15-34-44/root
1940	link_10		2018-08-24 13:34:44.981828	2018-08-24 13:34:44.981832	{"expand": false, "collapse": []}	42	failing_network_2018-08-24T15-34-44/root/step_1/out_1	failing_network_2018-08-24T15-34-44/root/sink_1/input	\N	failing_network_2018-08-24T15-34-44/root
1941	link_11		2018-08-24 13:34:44.982354	2018-08-24 13:34:44.982358	{"expand": false, "collapse": []}	42	failing_network_2018-08-24T15-34-44/root/step_2/out_2	failing_network_2018-08-24T15-34-44/root/sink_2/input	\N	failing_network_2018-08-24T15-34-44/root
1942	link_12		2018-08-24 13:34:44.98289	2018-08-24 13:34:44.982894	{"expand": false, "collapse": []}	42	failing_network_2018-08-24T15-34-44/root/step_3/out_1	failing_network_2018-08-24T15-34-44/root/sink_3/input	\N	failing_network_2018-08-24T15-34-44/root
1943	link_13		2018-08-24 13:34:44.9835	2018-08-24 13:34:44.983504	{"expand": false, "collapse": []}	42	failing_network_2018-08-24T15-34-44/root/range/result	failing_network_2018-08-24T15-34-44/root/sink_4/input	\N	failing_network_2018-08-24T15-34-44/root
1944	link_14		2018-08-24 13:34:44.98406	2018-08-24 13:34:44.984064	{"expand": false, "collapse": []}	42	failing_network_2018-08-24T15-34-44/root/sum/result	failing_network_2018-08-24T15-34-44/root/sink_5/input	\N	failing_network_2018-08-24T15-34-44/root
1945	link_0		2018-09-03 08:32:43.890284	2018-09-03 08:32:43.89029	{"expand": false, "collapse": []}	43	example_2018-09-03T10-32-43/root/const_addint_right_hand_0/output	example_2018-09-03T10-32-43/root/addint/right_hand	\N	example_2018-09-03T10-32-43/root
1946	link_1		2018-09-03 08:32:43.891733	2018-09-03 08:32:43.891737	{"expand": false, "collapse": []}	43	example_2018-09-03T10-32-43/root/source1/output	example_2018-09-03T10-32-43/root/addint/left_hand	\N	example_2018-09-03T10-32-43/root
1947	link_2		2018-09-03 08:32:43.892488	2018-09-03 08:32:43.892492	{"expand": false, "collapse": []}	43	example_2018-09-03T10-32-43/root/addint/result	example_2018-09-03T10-32-43/root/sink1/input	\N	example_2018-09-03T10-32-43/root
1948	link_0		2018-09-03 08:36:43.191431	2018-09-03 08:36:43.191436	{"expand": false, "collapse": []}	44	example_2018-09-03T10-36-42/root/const_addint_right_hand_0/output	example_2018-09-03T10-36-42/root/addint/right_hand	\N	example_2018-09-03T10-36-42/root
1949	link_1		2018-09-03 08:36:43.192225	2018-09-03 08:36:43.192229	{"expand": false, "collapse": []}	44	example_2018-09-03T10-36-42/root/source1/output	example_2018-09-03T10-36-42/root/addint/left_hand	\N	example_2018-09-03T10-36-42/root
1950	link_2		2018-09-03 08:36:43.192941	2018-09-03 08:36:43.192944	{"expand": false, "collapse": []}	44	example_2018-09-03T10-36-42/root/addint/result	example_2018-09-03T10-36-42/root/sink1/input	\N	example_2018-09-03T10-36-42/root
1951	link_0		2018-09-03 08:37:59.169289	2018-09-03 08:37:59.169294	{"expand": false, "collapse": []}	45	example_2018-09-03T10-37-58/root/const_addint_right_hand_0/output	example_2018-09-03T10-37-58/root/addint/right_hand	\N	example_2018-09-03T10-37-58/root
1952	link_1		2018-09-03 08:37:59.169953	2018-09-03 08:37:59.169957	{"expand": false, "collapse": []}	45	example_2018-09-03T10-37-58/root/source1/output	example_2018-09-03T10-37-58/root/addint/left_hand	\N	example_2018-09-03T10-37-58/root
1953	link_2		2018-09-03 08:37:59.170611	2018-09-03 08:37:59.170615	{"expand": false, "collapse": []}	45	example_2018-09-03T10-37-58/root/addint/result	example_2018-09-03T10-37-58/root/sink1/input	\N	example_2018-09-03T10-37-58/root
1954	link_0		2018-09-03 08:41:42.775473	2018-09-03 08:41:42.775479	{"expand": false, "collapse": []}	46	example_2018-09-03T10-41-42/root/const_addint_right_hand_0/output	example_2018-09-03T10-41-42/root/addint/right_hand	\N	example_2018-09-03T10-41-42/root
1955	link_1		2018-09-03 08:41:42.776313	2018-09-03 08:41:42.776317	{"expand": false, "collapse": []}	46	example_2018-09-03T10-41-42/root/source1/output	example_2018-09-03T10-41-42/root/addint/left_hand	\N	example_2018-09-03T10-41-42/root
1956	link_2		2018-09-03 08:41:42.776923	2018-09-03 08:41:42.776927	{"expand": false, "collapse": []}	46	example_2018-09-03T10-41-42/root/addint/result	example_2018-09-03T10-41-42/root/sink1/input	\N	example_2018-09-03T10-41-42/root
1957	link_0		2018-09-03 08:47:39.892617	2018-09-03 08:47:39.892622	{"expand": false, "collapse": []}	47	example_2018-09-03T10-47-39/root/const_addint_right_hand_0/output	example_2018-09-03T10-47-39/root/addint/right_hand	\N	example_2018-09-03T10-47-39/root
1958	link_1		2018-09-03 08:47:39.893323	2018-09-03 08:47:39.893327	{"expand": false, "collapse": []}	47	example_2018-09-03T10-47-39/root/source1/output	example_2018-09-03T10-47-39/root/addint/left_hand	\N	example_2018-09-03T10-47-39/root
1959	link_2		2018-09-03 08:47:39.893958	2018-09-03 08:47:39.893962	{"expand": false, "collapse": []}	47	example_2018-09-03T10-47-39/root/addint/result	example_2018-09-03T10-47-39/root/sink1/input	\N	example_2018-09-03T10-47-39/root
1960	link_0		2018-09-03 09:01:12.311494	2018-09-03 09:01:12.311499	{"expand": false, "collapse": []}	48	example_2018-09-03T11-01-12/root/const_addint_right_hand_0/output	example_2018-09-03T11-01-12/root/addint/right_hand	\N	example_2018-09-03T11-01-12/root
1961	link_1		2018-09-03 09:01:12.312285	2018-09-03 09:01:12.31229	{"expand": false, "collapse": []}	48	example_2018-09-03T11-01-12/root/source1/output	example_2018-09-03T11-01-12/root/addint/left_hand	\N	example_2018-09-03T11-01-12/root
1962	link_2		2018-09-03 09:01:12.312891	2018-09-03 09:01:12.312895	{"expand": false, "collapse": []}	48	example_2018-09-03T11-01-12/root/addint/result	example_2018-09-03T11-01-12/root/sink1/input	\N	example_2018-09-03T11-01-12/root
1963	link_0		2018-09-03 09:07:54.396698	2018-09-03 09:07:54.396703	{"expand": false, "collapse": []}	49	example_2018-09-03T11-07-53/root/const_addint_right_hand_0/output	example_2018-09-03T11-07-53/root/addint/right_hand	\N	example_2018-09-03T11-07-53/root
1964	link_1		2018-09-03 09:07:54.397592	2018-09-03 09:07:54.397596	{"expand": false, "collapse": []}	49	example_2018-09-03T11-07-53/root/source1/output	example_2018-09-03T11-07-53/root/addint/left_hand	\N	example_2018-09-03T11-07-53/root
1965	link_2		2018-09-03 09:07:54.398316	2018-09-03 09:07:54.398321	{"expand": false, "collapse": []}	49	example_2018-09-03T11-07-53/root/addint/result	example_2018-09-03T11-07-53/root/sink1/input	\N	example_2018-09-03T11-07-53/root
1966	link_0		2018-09-03 09:12:21.590701	2018-09-03 09:12:21.590706	{"expand": false, "collapse": []}	50	example_2018-09-03T11-12-21/root/const_addint_right_hand_0/output	example_2018-09-03T11-12-21/root/addint/right_hand	\N	example_2018-09-03T11-12-21/root
1967	link_1		2018-09-03 09:12:21.591488	2018-09-03 09:12:21.591492	{"expand": false, "collapse": []}	50	example_2018-09-03T11-12-21/root/source1/output	example_2018-09-03T11-12-21/root/addint/left_hand	\N	example_2018-09-03T11-12-21/root
1968	link_2		2018-09-03 09:12:21.592073	2018-09-03 09:12:21.592077	{"expand": false, "collapse": []}	50	example_2018-09-03T11-12-21/root/addint/result	example_2018-09-03T11-12-21/root/sink1/input	\N	example_2018-09-03T11-12-21/root
1969	link_0		2018-09-03 09:13:16.486741	2018-09-03 09:13:16.486746	{"expand": false, "collapse": []}	51	example_2018-09-03T11-13-16/root/const_addint_right_hand_0/output	example_2018-09-03T11-13-16/root/addint/right_hand	\N	example_2018-09-03T11-13-16/root
1970	link_1		2018-09-03 09:13:16.487464	2018-09-03 09:13:16.487468	{"expand": false, "collapse": []}	51	example_2018-09-03T11-13-16/root/source1/output	example_2018-09-03T11-13-16/root/addint/left_hand	\N	example_2018-09-03T11-13-16/root
1971	link_2		2018-09-03 09:13:16.488021	2018-09-03 09:13:16.488028	{"expand": false, "collapse": []}	51	example_2018-09-03T11-13-16/root/addint/result	example_2018-09-03T11-13-16/root/sink1/input	\N	example_2018-09-03T11-13-16/root
1972	link_0		2018-09-03 09:14:49.308057	2018-09-03 09:14:49.308062	{"expand": false, "collapse": []}	52	example_2018-09-03T11-14-49/root/const_addint_right_hand_0/output	example_2018-09-03T11-14-49/root/addint/right_hand	\N	example_2018-09-03T11-14-49/root
1973	link_1		2018-09-03 09:14:49.30873	2018-09-03 09:14:49.308734	{"expand": false, "collapse": []}	52	example_2018-09-03T11-14-49/root/source1/output	example_2018-09-03T11-14-49/root/addint/left_hand	\N	example_2018-09-03T11-14-49/root
1974	link_2		2018-09-03 09:14:49.309374	2018-09-03 09:14:49.309378	{"expand": false, "collapse": []}	52	example_2018-09-03T11-14-49/root/addint/result	example_2018-09-03T11-14-49/root/sink1/input	\N	example_2018-09-03T11-14-49/root
1975	link_0		2018-09-03 09:17:35.588613	2018-09-03 09:17:35.588618	{"expand": false, "collapse": []}	53	example_2018-09-03T11-17-35/root/const_addint_right_hand_0/output	example_2018-09-03T11-17-35/root/addint/right_hand	\N	example_2018-09-03T11-17-35/root
1976	link_1		2018-09-03 09:17:35.589336	2018-09-03 09:17:35.58934	{"expand": false, "collapse": []}	53	example_2018-09-03T11-17-35/root/source1/output	example_2018-09-03T11-17-35/root/addint/left_hand	\N	example_2018-09-03T11-17-35/root
1977	link_2		2018-09-03 09:17:35.590029	2018-09-03 09:17:35.590032	{"expand": false, "collapse": []}	53	example_2018-09-03T11-17-35/root/addint/result	example_2018-09-03T11-17-35/root/sink1/input	\N	example_2018-09-03T11-17-35/root
1978	link_0		2018-09-03 09:31:39.081206	2018-09-03 09:31:39.081212	{"expand": false, "collapse": []}	54	example_2018-09-03T11-31-38/root/const_addint_right_hand_0/output	example_2018-09-03T11-31-38/root/addint/right_hand	\N	example_2018-09-03T11-31-38/root
1979	link_1		2018-09-03 09:31:39.082052	2018-09-03 09:31:39.082056	{"expand": false, "collapse": []}	54	example_2018-09-03T11-31-38/root/source1/output	example_2018-09-03T11-31-38/root/addint/left_hand	\N	example_2018-09-03T11-31-38/root
1980	link_2		2018-09-03 09:31:39.082745	2018-09-03 09:31:39.082752	{"expand": false, "collapse": []}	54	example_2018-09-03T11-31-38/root/addint/result	example_2018-09-03T11-31-38/root/sink1/input	\N	example_2018-09-03T11-31-38/root
1981	link_0		2018-09-03 09:34:06.669822	2018-09-03 09:34:06.669827	{"expand": false, "collapse": []}	55	auto_prefix_test_2018-09-03T11-34-05/root/source/output	auto_prefix_test_2018-09-03T11-34-05/root/m_p/left_hand	\N	auto_prefix_test_2018-09-03T11-34-05/root
1982	link_1		2018-09-03 09:34:06.670773	2018-09-03 09:34:06.670777	{"expand": false, "collapse": []}	55	auto_prefix_test_2018-09-03T11-34-05/root/const/output	auto_prefix_test_2018-09-03T11-34-05/root/m_p/right_hand	\N	auto_prefix_test_2018-09-03T11-34-05/root
1983	link_2		2018-09-03 09:34:06.671517	2018-09-03 09:34:06.671569	{"expand": false, "collapse": []}	55	auto_prefix_test_2018-09-03T11-34-05/root/source/output	auto_prefix_test_2018-09-03T11-34-05/root/a_p/left_hand	\N	auto_prefix_test_2018-09-03T11-34-05/root
1984	link_3		2018-09-03 09:34:06.672269	2018-09-03 09:34:06.672273	{"expand": false, "collapse": []}	55	auto_prefix_test_2018-09-03T11-34-05/root/const/output	auto_prefix_test_2018-09-03T11-34-05/root/a_p/right_hand	\N	auto_prefix_test_2018-09-03T11-34-05/root
1985	link_4		2018-09-03 09:34:06.67295	2018-09-03 09:34:06.672954	{"expand": false, "collapse": []}	55	auto_prefix_test_2018-09-03T11-34-05/root/source/output	auto_prefix_test_2018-09-03T11-34-05/root/m_n/left_hand	\N	auto_prefix_test_2018-09-03T11-34-05/root
1986	link_5		2018-09-03 09:34:06.67368	2018-09-03 09:34:06.673684	{"expand": false, "collapse": []}	55	auto_prefix_test_2018-09-03T11-34-05/root/const/output	auto_prefix_test_2018-09-03T11-34-05/root/m_n/right_hand	\N	auto_prefix_test_2018-09-03T11-34-05/root
1987	link_6		2018-09-03 09:34:06.674387	2018-09-03 09:34:06.674391	{"expand": false, "collapse": []}	55	auto_prefix_test_2018-09-03T11-34-05/root/source/output	auto_prefix_test_2018-09-03T11-34-05/root/a_n/left_hand	\N	auto_prefix_test_2018-09-03T11-34-05/root
1988	link_7		2018-09-03 09:34:06.674984	2018-09-03 09:34:06.674988	{"expand": false, "collapse": []}	55	auto_prefix_test_2018-09-03T11-34-05/root/const/output	auto_prefix_test_2018-09-03T11-34-05/root/a_n/right_hand	\N	auto_prefix_test_2018-09-03T11-34-05/root
1989	link_8		2018-09-03 09:34:06.675544	2018-09-03 09:34:06.675548	{"expand": false, "collapse": []}	55	auto_prefix_test_2018-09-03T11-34-05/root/m_p/multiplied	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_p/input	\N	auto_prefix_test_2018-09-03T11-34-05/root
1990	link_9		2018-09-03 09:34:06.67608	2018-09-03 09:34:06.676084	{"expand": false, "collapse": []}	55	auto_prefix_test_2018-09-03T11-34-05/root/m_n/multiplied	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_n/input	\N	auto_prefix_test_2018-09-03T11-34-05/root
1991	link_10		2018-09-03 09:34:06.676672	2018-09-03 09:34:06.676676	{"expand": false, "collapse": []}	55	auto_prefix_test_2018-09-03T11-34-05/root/a_n/added	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_n/input	\N	auto_prefix_test_2018-09-03T11-34-05/root
1992	link_11		2018-09-03 09:34:06.67721	2018-09-03 09:34:06.677213	{"expand": false, "collapse": []}	55	auto_prefix_test_2018-09-03T11-34-05/root/a_p/added	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_p/input	\N	auto_prefix_test_2018-09-03T11-34-05/root
1993	link_0		2018-09-03 10:17:58.537198	2018-09-03 10:17:58.537213	{"expand": false, "collapse": []}	56	example_2018-09-03T12-17-58/root/const_addint_right_hand_0/output	example_2018-09-03T12-17-58/root/addint/right_hand	\N	example_2018-09-03T12-17-58/root
1994	link_1		2018-09-03 10:17:58.538268	2018-09-03 10:17:58.538275	{"expand": false, "collapse": []}	56	example_2018-09-03T12-17-58/root/source1/output	example_2018-09-03T12-17-58/root/addint/left_hand	\N	example_2018-09-03T12-17-58/root
1995	link_2		2018-09-03 10:17:58.539025	2018-09-03 10:17:58.539029	{"expand": false, "collapse": []}	56	example_2018-09-03T12-17-58/root/addint/result	example_2018-09-03T12-17-58/root/sink1/input	\N	example_2018-09-03T12-17-58/root
1996	link_0		2018-09-03 10:31:12.135669	2018-09-03 10:31:12.135675	{"expand": false, "collapse": []}	57	example_2018-09-03T12-31-11/root/const_addint_right_hand_0/output	example_2018-09-03T12-31-11/root/addint/right_hand	\N	example_2018-09-03T12-31-11/root
1997	link_1		2018-09-03 10:31:12.136738	2018-09-03 10:31:12.136742	{"expand": false, "collapse": []}	57	example_2018-09-03T12-31-11/root/source1/output	example_2018-09-03T12-31-11/root/addint/left_hand	\N	example_2018-09-03T12-31-11/root
1998	link_2		2018-09-03 10:31:12.137499	2018-09-03 10:31:12.137504	{"expand": false, "collapse": []}	57	example_2018-09-03T12-31-11/root/addint/result	example_2018-09-03T12-31-11/root/sink1/input	\N	example_2018-09-03T12-31-11/root
1999	link_0		2018-09-03 10:47:06.285602	2018-09-03 10:47:06.285608	{"expand": false, "collapse": []}	58	example_2018-09-03T12-47-05/root/const_addint_right_hand_0/output	example_2018-09-03T12-47-05/root/addint/right_hand	\N	example_2018-09-03T12-47-05/root
2000	link_1		2018-09-03 10:47:06.286277	2018-09-03 10:47:06.286281	{"expand": false, "collapse": []}	58	example_2018-09-03T12-47-05/root/source1/output	example_2018-09-03T12-47-05/root/addint/left_hand	\N	example_2018-09-03T12-47-05/root
2001	link_2		2018-09-03 10:47:06.28682	2018-09-03 10:47:06.286824	{"expand": false, "collapse": []}	58	example_2018-09-03T12-47-05/root/addint/result	example_2018-09-03T12-47-05/root/sink1/input	\N	example_2018-09-03T12-47-05/root
2002	link_0		2018-09-03 10:54:57.338597	2018-09-03 10:54:57.338602	{"expand": false, "collapse": []}	59	example_2018-09-03T12-54-57/root/const_addint_right_hand_0/output	example_2018-09-03T12-54-57/root/addint/right_hand	\N	example_2018-09-03T12-54-57/root
2003	link_1		2018-09-03 10:54:57.339379	2018-09-03 10:54:57.339383	{"expand": false, "collapse": []}	59	example_2018-09-03T12-54-57/root/source1/output	example_2018-09-03T12-54-57/root/addint/left_hand	\N	example_2018-09-03T12-54-57/root
2004	link_2		2018-09-03 10:54:57.340044	2018-09-03 10:54:57.340048	{"expand": false, "collapse": []}	59	example_2018-09-03T12-54-57/root/addint/result	example_2018-09-03T12-54-57/root/sink1/input	\N	example_2018-09-03T12-54-57/root
2005	link_0		2018-09-03 10:55:47.00551	2018-09-03 10:55:47.005515	{"expand": false, "collapse": []}	60	example_2018-09-03T12-55-46/root/const_addint_right_hand_0/output	example_2018-09-03T12-55-46/root/addint/right_hand	\N	example_2018-09-03T12-55-46/root
2006	link_1		2018-09-03 10:55:47.006159	2018-09-03 10:55:47.006163	{"expand": false, "collapse": []}	60	example_2018-09-03T12-55-46/root/source1/output	example_2018-09-03T12-55-46/root/addint/left_hand	\N	example_2018-09-03T12-55-46/root
2007	link_2		2018-09-03 10:55:47.006805	2018-09-03 10:55:47.006808	{"expand": false, "collapse": []}	60	example_2018-09-03T12-55-46/root/addint/result	example_2018-09-03T12-55-46/root/sink1/input	\N	example_2018-09-03T12-55-46/root
2008	link_0		2018-09-03 11:09:36.245801	2018-09-03 11:09:36.245806	{"expand": false, "collapse": []}	61	example_2018-09-03T13-09-35/root/const_addint_right_hand_0/output	example_2018-09-03T13-09-35/root/addint/right_hand	\N	example_2018-09-03T13-09-35/root
2009	link_1		2018-09-03 11:09:36.246537	2018-09-03 11:09:36.246541	{"expand": false, "collapse": []}	61	example_2018-09-03T13-09-35/root/source1/output	example_2018-09-03T13-09-35/root/addint/left_hand	\N	example_2018-09-03T13-09-35/root
2010	link_2		2018-09-03 11:09:36.24724	2018-09-03 11:09:36.247244	{"expand": false, "collapse": []}	61	example_2018-09-03T13-09-35/root/addint/result	example_2018-09-03T13-09-35/root/sink1/input	\N	example_2018-09-03T13-09-35/root
2011	link_0		2018-09-03 11:11:45.562405	2018-09-03 11:11:45.56241	{"expand": false, "collapse": []}	62	example_2018-09-03T13-11-45/root/const_addint_right_hand_0/output	example_2018-09-03T13-11-45/root/addint/right_hand	\N	example_2018-09-03T13-11-45/root
2012	link_1		2018-09-03 11:11:45.563108	2018-09-03 11:11:45.563112	{"expand": false, "collapse": []}	62	example_2018-09-03T13-11-45/root/source1/output	example_2018-09-03T13-11-45/root/addint/left_hand	\N	example_2018-09-03T13-11-45/root
2013	link_2		2018-09-03 11:11:45.563723	2018-09-03 11:11:45.563727	{"expand": false, "collapse": []}	62	example_2018-09-03T13-11-45/root/addint/result	example_2018-09-03T13-11-45/root/sink1/input	\N	example_2018-09-03T13-11-45/root
2014	link_0		2018-09-03 11:12:40.069579	2018-09-03 11:12:40.069584	{"expand": false, "collapse": []}	63	example_2018-09-03T13-12-39/root/const_addint_right_hand_0/output	example_2018-09-03T13-12-39/root/addint/right_hand	\N	example_2018-09-03T13-12-39/root
2015	link_1		2018-09-03 11:12:40.0703	2018-09-03 11:12:40.070307	{"expand": false, "collapse": []}	63	example_2018-09-03T13-12-39/root/source1/output	example_2018-09-03T13-12-39/root/addint/left_hand	\N	example_2018-09-03T13-12-39/root
2016	link_2		2018-09-03 11:12:40.070968	2018-09-03 11:12:40.070972	{"expand": false, "collapse": []}	63	example_2018-09-03T13-12-39/root/addint/result	example_2018-09-03T13-12-39/root/sink1/input	\N	example_2018-09-03T13-12-39/root
2017	link_0		2018-09-03 11:13:28.428598	2018-09-03 11:13:28.428603	{"expand": false, "collapse": []}	64	example_2018-09-03T13-13-28/root/const_addint_right_hand_0/output	example_2018-09-03T13-13-28/root/addint/right_hand	\N	example_2018-09-03T13-13-28/root
2018	link_1		2018-09-03 11:13:28.429529	2018-09-03 11:13:28.429533	{"expand": false, "collapse": []}	64	example_2018-09-03T13-13-28/root/source1/output	example_2018-09-03T13-13-28/root/addint/left_hand	\N	example_2018-09-03T13-13-28/root
2019	link_2		2018-09-03 11:13:28.430163	2018-09-03 11:13:28.430167	{"expand": false, "collapse": []}	64	example_2018-09-03T13-13-28/root/addint/result	example_2018-09-03T13-13-28/root/sink1/input	\N	example_2018-09-03T13-13-28/root
2020	link_0		2018-09-03 12:01:00.81434	2018-09-03 12:01:00.814346	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1/output	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/in_1	\N	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro
2021	link_1		2018-09-03 12:01:00.815103	2018-09-03 12:01:00.815107	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_2/output	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/in_2	\N	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro
2022	link_2		2018-09-03 12:01:00.815757	2018-09-03 12:01:00.815761	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_1_fail_2_0/output	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/fail_2	\N	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro
2191	crafts		2018-09-08 13:18:13.196392	2018-09-08 13:18:13.196397	{}	75	iris/root/6_qc_overview/122_t1_with_brain/out_png_image	iris/root/6_qc_overview/127_t1_with_brain_sink/in_input	PngImageFile	iris/root/6_qc_overview
2023	link_3		2018-09-03 12:01:00.816499	2018-09-03 12:01:00.816503	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_3/output	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/in_1	\N	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro
2024	link_4		2018-09-03 12:01:00.817219	2018-09-03 12:01:00.817223	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1/output	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/in_2	\N	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro
2025	link_5		2018-09-03 12:01:00.817828	2018-09-03 12:01:00.817832	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_2_fail_1_0/output	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/fail_1	\N	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro
2026	link_6		2018-09-03 12:01:00.818664	2018-09-03 12:01:00.818668	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/out_2	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/in_1	\N	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro
2027	link_7		2018-09-03 12:01:00.819217	2018-09-03 12:01:00.819221	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/out_1	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/in_2	\N	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro
2028	link_8		2018-09-03 12:01:00.819849	2018-09-03 12:01:00.819854	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/out_1	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range/value	\N	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro
2029	link_9		2018-09-03 12:01:00.820573	2018-09-03 12:01:00.820576	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range/result	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sum/values	\N	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro
2030	link_10		2018-09-03 12:01:00.82114	2018-09-03 12:01:00.821144	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/out_1	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_1/input	\N	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro
2031	link_11		2018-09-03 12:01:00.821699	2018-09-03 12:01:00.821703	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/out_2	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_2/input	\N	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro
2032	link_12		2018-09-03 12:01:00.822211	2018-09-03 12:01:00.822216	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/out_1	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_3/input	\N	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro
2033	link_13		2018-09-03 12:01:00.822899	2018-09-03 12:01:00.822903	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range/result	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_4/input	\N	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro
2034	link_14		2018-09-03 12:01:00.823457	2018-09-03 12:01:00.823462	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sum/result	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_5/input	\N	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro
2035	link_0		2018-09-03 12:01:00.824035	2018-09-03 12:01:00.824039	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/source_a/output	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1/input	\N	failing_macro_top_level_2018-09-03T14-01-00/root
2036	link_1		2018-09-03 12:01:00.82465	2018-09-03 12:01:00.824653	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/source_b/output	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_2/input	\N	failing_macro_top_level_2018-09-03T14-01-00/root
2037	link_2		2018-09-03 12:01:00.825149	2018-09-03 12:01:00.825153	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/source_c/output	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_3/input	\N	failing_macro_top_level_2018-09-03T14-01-00/root
2038	link_3		2018-09-03 12:01:00.825784	2018-09-03 12:01:00.825788	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_5/output	failing_macro_top_level_2018-09-03T14-01-00/root/add/left_hand	\N	failing_macro_top_level_2018-09-03T14-01-00/root
2039	link_4		2018-09-03 12:01:00.826593	2018-09-03 12:01:00.826597	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/const_add_right_hand_0/output	failing_macro_top_level_2018-09-03T14-01-00/root/add/right_hand	\N	failing_macro_top_level_2018-09-03T14-01-00/root
2040	link_5		2018-09-03 12:01:00.827185	2018-09-03 12:01:00.827192	{"expand": false, "collapse": []}	65	failing_macro_top_level_2018-09-03T14-01-00/root/add/result	failing_macro_top_level_2018-09-03T14-01-00/root/sink/input	\N	failing_macro_top_level_2018-09-03T14-01-00/root
2041	link_0		2018-09-03 12:04:33.336939	2018-09-03 12:04:33.336944	{"expand": false, "collapse": []}	66	add_ints_2018-09-03T14-04-33/root/source/output	add_ints_2018-09-03T14-04-33/root/add/left_hand	\N	add_ints_2018-09-03T14-04-33/root
2042	link_1		2018-09-03 12:04:33.337915	2018-09-03 12:04:33.337919	{"expand": false, "collapse": []}	66	add_ints_2018-09-03T14-04-33/root/const_add_right_hand_0/output	add_ints_2018-09-03T14-04-33/root/add/right_hand	\N	add_ints_2018-09-03T14-04-33/root
2043	link_2		2018-09-03 12:04:33.338568	2018-09-03 12:04:33.338572	{"expand": false, "collapse": []}	66	add_ints_2018-09-03T14-04-33/root/add/result	add_ints_2018-09-03T14-04-33/root/sink/input	\N	add_ints_2018-09-03T14-04-33/root
2044	link_0		2018-09-03 12:14:40.530447	2018-09-03 12:14:40.530452	{"expand": false, "collapse": []}	67	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1/output	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/left_hand	\N	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1
2045	link_1		2018-09-03 12:14:40.531665	2018-09-03 12:14:40.53167	{"expand": false, "collapse": []}	67	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_2/output	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/right_hand	\N	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1
2046	link_2		2018-09-03 12:14:40.53239	2018-09-03 12:14:40.532394	{"expand": false, "collapse": []}	67	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/result	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/input	\N	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1
2047	link_0		2018-09-03 12:14:40.533163	2018-09-03 12:14:40.533167	{"expand": false, "collapse": []}	67	macro_node_2_2018-09-03T14-14-40/root/source_1/output	macro_node_2_2018-09-03T14-14-40/root/addint/left_hand	\N	macro_node_2_2018-09-03T14-14-40/root
2048	link_1		2018-09-03 12:14:40.533814	2018-09-03 12:14:40.533818	{"expand": false, "collapse": []}	67	macro_node_2_2018-09-03T14-14-40/root/source_2/output	macro_node_2_2018-09-03T14-14-40/root/addint/right_hand	\N	macro_node_2_2018-09-03T14-14-40/root
2049	link_2		2018-09-03 12:14:40.534521	2018-09-03 12:14:40.534541	{"expand": false, "collapse": []}	67	macro_node_2_2018-09-03T14-14-40/root/addint/result	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1/input	\N	macro_node_2_2018-09-03T14-14-40/root
2050	link_3		2018-09-03 12:14:40.535302	2018-09-03 12:14:40.535306	{"expand": false, "collapse": []}	67	macro_node_2_2018-09-03T14-14-40/root/source_3/output	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_2/input	\N	macro_node_2_2018-09-03T14-14-40/root
2051	link_4		2018-09-03 12:14:40.535925	2018-09-03 12:14:40.535929	{"expand": false, "collapse": [1]}	67	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/output	macro_node_2_2018-09-03T14-14-40/root/sum/values	\N	macro_node_2_2018-09-03T14-14-40/root
2052	link_5		2018-09-03 12:14:40.536568	2018-09-03 12:14:40.536571	{"expand": false, "collapse": []}	67	macro_node_2_2018-09-03T14-14-40/root/sum/result	macro_node_2_2018-09-03T14-14-40/root/sink/input	\N	macro_node_2_2018-09-03T14-14-40/root
2053	link_0		2018-09-03 12:18:44.670191	2018-09-03 12:18:44.670197	{"expand": false, "collapse": []}	68	add_ints_2018-09-03T14-18-44/root/source/output	add_ints_2018-09-03T14-18-44/root/add/left_hand	\N	add_ints_2018-09-03T14-18-44/root
2054	link_1		2018-09-03 12:18:44.671008	2018-09-03 12:18:44.671012	{"expand": false, "collapse": []}	68	add_ints_2018-09-03T14-18-44/root/const_add_right_hand_0/output	add_ints_2018-09-03T14-18-44/root/add/right_hand	\N	add_ints_2018-09-03T14-18-44/root
2055	link_2		2018-09-03 12:18:44.671708	2018-09-03 12:18:44.671712	{"expand": false, "collapse": []}	68	add_ints_2018-09-03T14-18-44/root/add/result	add_ints_2018-09-03T14-18-44/root/sink/input	\N	add_ints_2018-09-03T14-18-44/root
2056	link_0		2018-09-03 12:30:02.243071	2018-09-03 12:30:02.243076	{"expand": false, "collapse": []}	69	auto_prefix_test_2018-09-03T14-30-01/root/source/output	auto_prefix_test_2018-09-03T14-30-01/root/m_p/left_hand	\N	auto_prefix_test_2018-09-03T14-30-01/root
2057	link_1		2018-09-03 12:30:02.243964	2018-09-03 12:30:02.243978	{"expand": false, "collapse": []}	69	auto_prefix_test_2018-09-03T14-30-01/root/const/output	auto_prefix_test_2018-09-03T14-30-01/root/m_p/right_hand	\N	auto_prefix_test_2018-09-03T14-30-01/root
2058	link_2		2018-09-03 12:30:02.244692	2018-09-03 12:30:02.244696	{"expand": false, "collapse": []}	69	auto_prefix_test_2018-09-03T14-30-01/root/source/output	auto_prefix_test_2018-09-03T14-30-01/root/a_p/left_hand	\N	auto_prefix_test_2018-09-03T14-30-01/root
2059	link_3		2018-09-03 12:30:02.245374	2018-09-03 12:30:02.245383	{"expand": false, "collapse": []}	69	auto_prefix_test_2018-09-03T14-30-01/root/const/output	auto_prefix_test_2018-09-03T14-30-01/root/a_p/right_hand	\N	auto_prefix_test_2018-09-03T14-30-01/root
2060	link_4		2018-09-03 12:30:02.246028	2018-09-03 12:30:02.246032	{"expand": false, "collapse": []}	69	auto_prefix_test_2018-09-03T14-30-01/root/source/output	auto_prefix_test_2018-09-03T14-30-01/root/m_n/left_hand	\N	auto_prefix_test_2018-09-03T14-30-01/root
2061	link_5		2018-09-03 12:30:02.246612	2018-09-03 12:30:02.246616	{"expand": false, "collapse": []}	69	auto_prefix_test_2018-09-03T14-30-01/root/const/output	auto_prefix_test_2018-09-03T14-30-01/root/m_n/right_hand	\N	auto_prefix_test_2018-09-03T14-30-01/root
2062	link_6		2018-09-03 12:30:02.24725	2018-09-03 12:30:02.247254	{"expand": false, "collapse": []}	69	auto_prefix_test_2018-09-03T14-30-01/root/source/output	auto_prefix_test_2018-09-03T14-30-01/root/a_n/left_hand	\N	auto_prefix_test_2018-09-03T14-30-01/root
2063	link_7		2018-09-03 12:30:02.247902	2018-09-03 12:30:02.247906	{"expand": false, "collapse": []}	69	auto_prefix_test_2018-09-03T14-30-01/root/const/output	auto_prefix_test_2018-09-03T14-30-01/root/a_n/right_hand	\N	auto_prefix_test_2018-09-03T14-30-01/root
2064	link_8		2018-09-03 12:30:02.248465	2018-09-03 12:30:02.248469	{"expand": false, "collapse": []}	69	auto_prefix_test_2018-09-03T14-30-01/root/m_p/multiplied	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_p/input	\N	auto_prefix_test_2018-09-03T14-30-01/root
2065	link_9		2018-09-03 12:30:02.249103	2018-09-03 12:30:02.249107	{"expand": false, "collapse": []}	69	auto_prefix_test_2018-09-03T14-30-01/root/m_n/multiplied	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_n/input	\N	auto_prefix_test_2018-09-03T14-30-01/root
2066	link_10		2018-09-03 12:30:02.249689	2018-09-03 12:30:02.249693	{"expand": false, "collapse": []}	69	auto_prefix_test_2018-09-03T14-30-01/root/a_n/added	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_n/input	\N	auto_prefix_test_2018-09-03T14-30-01/root
2067	link_11		2018-09-03 12:30:02.250256	2018-09-03 12:30:02.25026	{"expand": false, "collapse": []}	69	auto_prefix_test_2018-09-03T14-30-01/root/a_p/added	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_p/input	\N	auto_prefix_test_2018-09-03T14-30-01/root
2068	link_2		2018-09-06 11:57:24.899639	2018-09-06 11:57:24.899645	{"collapse": [], "expand": false}	70	add_ints_2018-09-06T13-57-24/root/add/result	add_ints_2018-09-06T13-57-24/root/sink/input	\N	add_ints_2018-09-06T13-57-24/root
2069	link_1		2018-09-06 11:57:24.901267	2018-09-06 11:57:24.901272	{"collapse": [], "expand": false}	70	add_ints_2018-09-06T13-57-24/root/const_add_right_hand_0/output	add_ints_2018-09-06T13-57-24/root/add/right_hand	\N	add_ints_2018-09-06T13-57-24/root
2070	link_0		2018-09-06 11:57:24.90199	2018-09-06 11:57:24.901994	{"collapse": [], "expand": false}	70	add_ints_2018-09-06T13-57-24/root/source/output	add_ints_2018-09-06T13-57-24/root/add/left_hand	\N	add_ints_2018-09-06T13-57-24/root
2071	link_9		2018-09-06 12:00:46.588632	2018-09-06 12:00:46.588638	{"collapse": [], "expand": false}	71	failing_network_2018-09-06T14-00-46/root/range/result	failing_network_2018-09-06T14-00-46/root/sum/values	\N	failing_network_2018-09-06T14-00-46/root
2072	link_2		2018-09-06 12:00:46.589893	2018-09-06 12:00:46.589897	{"collapse": [], "expand": false}	71	failing_network_2018-09-06T14-00-46/root/const_step_1_fail_2_0/output	failing_network_2018-09-06T14-00-46/root/step_1/fail_2	\N	failing_network_2018-09-06T14-00-46/root
2073	link_1		2018-09-06 12:00:46.590634	2018-09-06 12:00:46.590639	{"collapse": [], "expand": false}	71	failing_network_2018-09-06T14-00-46/root/source_2/output	failing_network_2018-09-06T14-00-46/root/step_1/in_2	\N	failing_network_2018-09-06T14-00-46/root
2074	link_8		2018-09-06 12:00:46.591391	2018-09-06 12:00:46.591395	{"collapse": [], "expand": false}	71	failing_network_2018-09-06T14-00-46/root/step_3/out_1	failing_network_2018-09-06T14-00-46/root/range/value	\N	failing_network_2018-09-06T14-00-46/root
2075	link_4		2018-09-06 12:00:46.59215	2018-09-06 12:00:46.592154	{"collapse": [], "expand": false}	71	failing_network_2018-09-06T14-00-46/root/source_1/output	failing_network_2018-09-06T14-00-46/root/step_2/in_2	\N	failing_network_2018-09-06T14-00-46/root
2076	link_11		2018-09-06 12:00:46.592735	2018-09-06 12:00:46.592739	{"collapse": [], "expand": false}	71	failing_network_2018-09-06T14-00-46/root/step_2/out_2	failing_network_2018-09-06T14-00-46/root/sink_2/input	\N	failing_network_2018-09-06T14-00-46/root
2077	link_3		2018-09-06 12:00:46.593358	2018-09-06 12:00:46.593362	{"collapse": [], "expand": false}	71	failing_network_2018-09-06T14-00-46/root/source_3/output	failing_network_2018-09-06T14-00-46/root/step_2/in_1	\N	failing_network_2018-09-06T14-00-46/root
2078	link_13		2018-09-06 12:00:46.594022	2018-09-06 12:00:46.594026	{"collapse": [], "expand": false}	71	failing_network_2018-09-06T14-00-46/root/range/result	failing_network_2018-09-06T14-00-46/root/sink_4/input	\N	failing_network_2018-09-06T14-00-46/root
2079	link_0		2018-09-06 12:00:46.59463	2018-09-06 12:00:46.594633	{"collapse": [], "expand": false}	71	failing_network_2018-09-06T14-00-46/root/source_1/output	failing_network_2018-09-06T14-00-46/root/step_1/in_1	\N	failing_network_2018-09-06T14-00-46/root
2080	link_14		2018-09-06 12:00:46.595146	2018-09-06 12:00:46.59515	{"collapse": [], "expand": false}	71	failing_network_2018-09-06T14-00-46/root/sum/result	failing_network_2018-09-06T14-00-46/root/sink_5/input	\N	failing_network_2018-09-06T14-00-46/root
2081	link_5		2018-09-06 12:00:46.595883	2018-09-06 12:00:46.595887	{"collapse": [], "expand": false}	71	failing_network_2018-09-06T14-00-46/root/const_step_2_fail_1_0/output	failing_network_2018-09-06T14-00-46/root/step_2/fail_1	\N	failing_network_2018-09-06T14-00-46/root
2082	link_12		2018-09-06 12:00:46.596602	2018-09-06 12:00:46.596606	{"collapse": [], "expand": false}	71	failing_network_2018-09-06T14-00-46/root/step_3/out_1	failing_network_2018-09-06T14-00-46/root/sink_3/input	\N	failing_network_2018-09-06T14-00-46/root
2083	link_10		2018-09-06 12:00:46.597275	2018-09-06 12:00:46.597279	{"collapse": [], "expand": false}	71	failing_network_2018-09-06T14-00-46/root/step_1/out_1	failing_network_2018-09-06T14-00-46/root/sink_1/input	\N	failing_network_2018-09-06T14-00-46/root
2084	link_6		2018-09-06 12:00:46.597914	2018-09-06 12:00:46.597918	{"collapse": [], "expand": false}	71	failing_network_2018-09-06T14-00-46/root/step_1/out_2	failing_network_2018-09-06T14-00-46/root/step_3/in_1	\N	failing_network_2018-09-06T14-00-46/root
2085	link_7		2018-09-06 12:00:46.598511	2018-09-06 12:00:46.598515	{"collapse": [], "expand": false}	71	failing_network_2018-09-06T14-00-46/root/step_2/out_1	failing_network_2018-09-06T14-00-46/root/step_3/in_2	\N	failing_network_2018-09-06T14-00-46/root
2086	link_22		2018-09-06 13:00:59.217694	2018-09-06 13:00:59.2177	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/icv_parameters_used	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Parameters/input	\N	IntracranialVolume_2018-09-06T15-00-57/root
2087	link_23		2018-09-06 13:00:59.220122	2018-09-06 13:00:59.220128	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/icv_volume	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Volume/input	\N	IntracranialVolume_2018-09-06T15-00-57/root
2088	link_20		2018-09-06 13:00:59.220995	2018-09-06 13:00:59.220999	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Subject_Name/ICV_Subject_Name/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Subject_Name	\N	IntracranialVolume_2018-09-06T15-00-57/root
2089	link_21		2018-09-06 13:00:59.221795	2018-09-06 13:00:59.221799	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/icv_output_nii_mask	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Mask/input	\N	IntracranialVolume_2018-09-06T15-00-57/root
2090	link_26		2018-09-06 13:00:59.222497	2018-09-06 13:00:59.222501	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/icv_original_file_In_nii	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Original_Subject/input	\N	IntracranialVolume_2018-09-06T15-00-57/root
2091	link_24		2018-09-06 13:00:59.22326	2018-09-06 13:00:59.223264	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/icv_log	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_Log/input	\N	IntracranialVolume_2018-09-06T15-00-57/root
2092	link_25		2018-09-06 13:00:59.223986	2018-09-06 13:00:59.22399	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/icv_jpg	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_JPG/input	\N	IntracranialVolume_2018-09-06T15-00-57/root
2093	link_13		2018-09-06 13:00:59.224656	2018-09-06 13:00:59.22466	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Sub_lattice/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Sub_lattice	\N	IntracranialVolume_2018-09-06T15-00-57/root
2094	link_12		2018-09-06 13:00:59.22523	2018-09-06 13:00:59.225234	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Similarity/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Similarity	\N	IntracranialVolume_2018-09-06T15-00-57/root
2095	link_11		2018-09-06 13:00:59.225777	2018-09-06 13:00:59.225781	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Stiffness/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Stiffness	\N	IntracranialVolume_2018-09-06T15-00-57/root
2096	link_10		2018-09-06 13:00:59.226327	2018-09-06 13:00:59.226331	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Weight/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Weight	\N	IntracranialVolume_2018-09-06T15-00-57/root
2097	link_17		2018-09-06 13:00:59.226821	2018-09-06 13:00:59.226824	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Extended_Arguments/NonLinear_Normalization/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/NonLinear_Normalization	\N	IntracranialVolume_2018-09-06T15-00-57/root
2098	link_16		2018-09-06 13:00:59.227355	2018-09-06 13:00:59.227359	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Non_linear/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Non_linear	\N	IntracranialVolume_2018-09-06T15-00-57/root
2099	link_15		2018-09-06 13:00:59.227929	2018-09-06 13:00:59.227933	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Clobber/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Clobber	\N	IntracranialVolume_2018-09-06T15-00-57/root
2100	link_14		2018-09-06 13:00:59.228608	2018-09-06 13:00:59.228612	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Debug/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Debug	\N	IntracranialVolume_2018-09-06T15-00-57/root
2101	link_19		2018-09-06 13:00:59.22914	2018-09-06 13:00:59.229144	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Operation_Type/Calc_Crude/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Calc_Crude	\N	IntracranialVolume_2018-09-06T15-00-57/root
2102	link_18		2018-09-06 13:00:59.229667	2018-09-06 13:00:59.229671	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Mask_To_Binary_Min/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Mask_To_Binary_Min	\N	IntracranialVolume_2018-09-06T15-00-57/root
2103	link_9		2018-09-06 13:00:59.230251	2018-09-06 13:00:59.230255	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Blur_FWHM/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Blur_FWHM	\N	IntracranialVolume_2018-09-06T15-00-57/root
2104	link_8		2018-09-06 13:00:59.230768	2018-09-06 13:00:59.230772	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Step_Size/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Step_Size	\N	IntracranialVolume_2018-09-06T15-00-57/root
2105	link_3		2018-09-06 13:00:59.231311	2018-09-06 13:00:59.231314	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Linear_Estimate/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Linear_Estimate	\N	IntracranialVolume_2018-09-06T15-00-57/root
2106	link_2		2018-09-06 13:00:59.23195	2018-09-06 13:00:59.231954	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Program_File_Arguments/Input_To_Process/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Processed_XNAT_DICOM	\N	IntracranialVolume_2018-09-06T15-00-57/root
2107	link_1		2018-09-06 13:00:59.232507	2018-09-06 13:00:59.232511	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Model_Name/Model/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Model	\N	IntracranialVolume_2018-09-06T15-00-57/root
2108	link_0		2018-09-06 13:00:59.233066	2018-09-06 13:00:59.23307	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Delete_Files/Delete_Files/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Delete_Files	\N	IntracranialVolume_2018-09-06T15-00-57/root
2109	link_7		2018-09-06 13:00:59.233617	2018-09-06 13:00:59.23362	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Num_Of_Iterations/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Num_Of_Iterations	\N	IntracranialVolume_2018-09-06T15-00-57/root
2110	link_6		2018-09-06 13:00:59.234131	2018-09-06 13:00:59.234135	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Linear_Registration_Arguments/Linear_Normalization/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Linear_Normalization	\N	IntracranialVolume_2018-09-06T15-00-57/root
2111	link_5		2018-09-06 13:00:59.234684	2018-09-06 13:00:59.234688	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Registration_Blur_FWHM/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Registration_Blur_FWHM	\N	IntracranialVolume_2018-09-06T15-00-57/root
2112	link_4		2018-09-06 13:00:59.235284	2018-09-06 13:00:59.235288	{"collapse": [], "expand": false}	72	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Num_Of_Registrations/output	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/Num_Of_Registrations	\N	IntracranialVolume_2018-09-06T15-00-57/root
2113	link_2		2018-09-06 13:03:16.766581	2018-09-06 13:03:16.766586	{"expand": false, "collapse": []}	73	failing_network_2018-09-06T15-03-16/root/const_step_1_fail_2_0/output	failing_network_2018-09-06T15-03-16/root/step_1/fail_2	\N	failing_network_2018-09-06T15-03-16/root
2114	link_6		2018-09-06 13:03:16.767944	2018-09-06 13:03:16.767948	{"expand": false, "collapse": []}	73	failing_network_2018-09-06T15-03-16/root/step_1/out_2	failing_network_2018-09-06T15-03-16/root/step_3/in_1	\N	failing_network_2018-09-06T15-03-16/root
2115	link_11		2018-09-06 13:03:16.768774	2018-09-06 13:03:16.768777	{"expand": false, "collapse": []}	73	failing_network_2018-09-06T15-03-16/root/step_2/out_2	failing_network_2018-09-06T15-03-16/root/sink_2/input	\N	failing_network_2018-09-06T15-03-16/root
2116	link_9		2018-09-06 13:03:16.769527	2018-09-06 13:03:16.769531	{"expand": false, "collapse": []}	73	failing_network_2018-09-06T15-03-16/root/range/result	failing_network_2018-09-06T15-03-16/root/sum/values	\N	failing_network_2018-09-06T15-03-16/root
2117	link_7		2018-09-06 13:03:16.770232	2018-09-06 13:03:16.770236	{"expand": false, "collapse": []}	73	failing_network_2018-09-06T15-03-16/root/step_2/out_1	failing_network_2018-09-06T15-03-16/root/step_3/in_2	\N	failing_network_2018-09-06T15-03-16/root
2118	link_8		2018-09-06 13:03:16.770845	2018-09-06 13:03:16.770849	{"expand": false, "collapse": []}	73	failing_network_2018-09-06T15-03-16/root/step_3/out_1	failing_network_2018-09-06T15-03-16/root/range/value	\N	failing_network_2018-09-06T15-03-16/root
2119	link_10		2018-09-06 13:03:16.771518	2018-09-06 13:03:16.771522	{"expand": false, "collapse": []}	73	failing_network_2018-09-06T15-03-16/root/step_1/out_1	failing_network_2018-09-06T15-03-16/root/sink_1/input	\N	failing_network_2018-09-06T15-03-16/root
2120	link_14		2018-09-06 13:03:16.77236	2018-09-06 13:03:16.772364	{"expand": false, "collapse": []}	73	failing_network_2018-09-06T15-03-16/root/sum/result	failing_network_2018-09-06T15-03-16/root/sink_5/input	\N	failing_network_2018-09-06T15-03-16/root
2121	link_4		2018-09-06 13:03:16.773043	2018-09-06 13:03:16.773053	{"expand": false, "collapse": []}	73	failing_network_2018-09-06T15-03-16/root/source_1/output	failing_network_2018-09-06T15-03-16/root/step_2/in_2	\N	failing_network_2018-09-06T15-03-16/root
2122	link_0		2018-09-06 13:03:16.773668	2018-09-06 13:03:16.773672	{"expand": false, "collapse": []}	73	failing_network_2018-09-06T15-03-16/root/source_1/output	failing_network_2018-09-06T15-03-16/root/step_1/in_1	\N	failing_network_2018-09-06T15-03-16/root
2123	link_1		2018-09-06 13:03:16.77419	2018-09-06 13:03:16.774206	{"expand": false, "collapse": []}	73	failing_network_2018-09-06T15-03-16/root/source_2/output	failing_network_2018-09-06T15-03-16/root/step_1/in_2	\N	failing_network_2018-09-06T15-03-16/root
2124	link_3		2018-09-06 13:03:16.774789	2018-09-06 13:03:16.774793	{"expand": false, "collapse": []}	73	failing_network_2018-09-06T15-03-16/root/source_3/output	failing_network_2018-09-06T15-03-16/root/step_2/in_1	\N	failing_network_2018-09-06T15-03-16/root
2125	link_13		2018-09-06 13:03:16.775454	2018-09-06 13:03:16.775458	{"expand": false, "collapse": []}	73	failing_network_2018-09-06T15-03-16/root/range/result	failing_network_2018-09-06T15-03-16/root/sink_4/input	\N	failing_network_2018-09-06T15-03-16/root
2126	link_12		2018-09-06 13:03:16.776255	2018-09-06 13:03:16.776259	{"expand": false, "collapse": []}	73	failing_network_2018-09-06T15-03-16/root/step_3/out_1	failing_network_2018-09-06T15-03-16/root/sink_3/input	\N	failing_network_2018-09-06T15-03-16/root
2127	link_5		2018-09-06 13:03:16.776836	2018-09-06 13:03:16.77684	{"expand": false, "collapse": []}	73	failing_network_2018-09-06T15-03-16/root/const_step_2_fail_1_0/output	failing_network_2018-09-06T15-03-16/root/step_2/fail_1	\N	failing_network_2018-09-06T15-03-16/root
2128	link_0		2018-09-07 10:00:02.680388	2018-09-07 10:00:02.680393	{"collapse": [], "expand": false}	74	add_ints_2018-09-07T12-00-02/root/source/output	add_ints_2018-09-07T12-00-02/root/add/left_hand	\N	add_ints_2018-09-07T12-00-02/root
2129	link_1		2018-09-07 10:00:02.681373	2018-09-07 10:00:02.681377	{"collapse": [], "expand": false}	74	add_ints_2018-09-07T12-00-02/root/const_add_right_hand_0/output	add_ints_2018-09-07T12-00-02/root/add/right_hand	\N	add_ints_2018-09-07T12-00-02/root
2130	link_2		2018-09-07 10:00:02.682169	2018-09-07 10:00:02.682173	{"collapse": [], "expand": false}	74	add_ints_2018-09-07T12-00-02/root/add/result	add_ints_2018-09-07T12-00-02/root/sink/input	\N	add_ints_2018-09-07T12-00-02/root
2131	replacements		2018-09-08 13:18:13.159619	2018-09-08 13:18:13.159626	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/out_log_file	iris/root/65_spm_tissue_quantification/69_outputs/84_csf_spm/in_input	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification
2132	remainders		2018-09-08 13:18:13.161681	2018-09-08 13:18:13.161685	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/out_directory	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/in_gray_matter	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2133	chamber		2018-09-08 13:18:13.162533	2018-09-08 13:18:13.162537	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/out_csf_image	iris/root/65_spm_tissue_quantification/69_outputs/80_gm_map/in_input	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification
2134	officer		2018-09-08 13:18:13.163392	2018-09-08 13:18:13.163396	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/out_determinant_of_jacobian	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/in_cerebral_spinal_fluid	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2135	dependents		2018-09-08 13:18:13.164095	2018-09-08 13:18:13.164099	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/out_directory	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/in_gray_matter	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2136	addressees		2018-09-08 13:18:13.164939	2018-09-08 13:18:13.164943	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/out_log_file	iris/root/65_spm_tissue_quantification/69_outputs/88_gm_spm/in_input	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification
2137	blade		2018-09-08 13:18:13.165774	2018-09-08 13:18:13.165778	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/out_jacobian_matrix	iris/root/65_spm_tissue_quantification/69_outputs/90_wm_spm/in_input	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification
2138	compressions		2018-09-08 13:18:13.166331	2018-09-08 13:18:13.166335	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/out_wm_image	iris/root/65_spm_tissue_quantification/69_outputs/73_wm_map/in_input	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification
2139	orange		2018-09-08 13:18:13.166897	2018-09-08 13:18:13.166901	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/out_gm_image	iris/root/65_spm_tissue_quantification/69_outputs/72_csf_map/in_input	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification
2140	slopes		2018-09-08 13:18:13.167421	2018-09-08 13:18:13.167424	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/71_const_edit_transform_file_set_0/out_output	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/86_edit_transform_file/in_set	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2141	streams		2018-09-08 13:18:13.168159	2018-09-08 13:18:13.168163	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/out_log_file	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/86_edit_transform_file/in_transform	ElastixTransformFile	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2142	trains		2018-09-08 13:18:13.168684	2018-09-08 13:18:13.168688	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/74_const_invert_gm_parameters_0/out_output	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/in_fixed_mask	ElastixParameterFile	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2143	pieces		2018-09-08 13:18:13.169236	2018-09-08 13:18:13.16924	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/out_directory	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/in_moving_image	ElastixTransformFile	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2144	deck		2018-09-08 13:18:13.169895	2018-09-08 13:18:13.169898	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/86_edit_transform_file/out_transform	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/in_determinant_of_jacobian_flag	ElastixTransformFile	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2145	bases		2018-09-08 13:18:13.170451	2018-09-08 13:18:13.170455	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation/out_gm_image	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/in_determinant_of_jacobian_flag	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2146	lining		2018-09-08 13:18:13.171025	2018-09-08 13:18:13.171029	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/86_edit_transform_file/out_transform	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/in_image	ElastixTransformFile	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2147	measure		2018-09-08 13:18:13.171783	2018-09-08 13:18:13.171786	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation/out_csf_image	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/in_points	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2148	teachings		2018-09-08 13:18:13.172476	2018-09-08 13:18:13.172482	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/86_edit_transform_file/out_transform	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/in_image	ElastixTransformFile	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2149	kites		2018-09-08 13:18:13.173058	2018-09-08 13:18:13.173061	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation/out_directory	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/in_priority	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2150	claim		2018-09-08 13:18:13.173637	2018-09-08 13:18:13.173641	{}	75	iris/root/65_spm_tissue_quantification/67_InputImage/75_t1_passthrough/out_result	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/in_moving_image	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification
2151	generations		2018-09-08 13:18:13.174248	2018-09-08 13:18:13.174251	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation/out_directory	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/in_priority	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2152	organization		2018-09-08 13:18:13.17479	2018-09-08 13:18:13.174794	{}	75	iris/root/65_spm_tissue_quantification/67_InputImage/75_t1_passthrough/out_result	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/in_moving_image	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification
2153	inquiries		2018-09-08 13:18:13.175299	2018-09-08 13:18:13.175303	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/85_const_reg_t1_to_spmtemplate_fixed_image_0/out_output	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/in_initial_transform	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2154	hope		2018-09-08 13:18:13.175913	2018-09-08 13:18:13.175916	{}	75	iris/root/65_spm_tissue_quantification/67_InputImage/77_const_t1_passthrough_operator_0/out_output	iris/root/65_spm_tissue_quantification/67_InputImage/75_t1_passthrough/in_operator	String	iris/root/65_spm_tissue_quantification/67_InputImage
2155	science		2018-09-08 13:18:13.176444	2018-09-08 13:18:13.176447	{}	75	iris/root/65_spm_tissue_quantification/66_InputImages/81_T1_images/out_output	iris/root/65_spm_tissue_quantification/67_InputImage/75_t1_passthrough/in_left_hand	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification
2192	berry		2018-09-08 13:18:13.197048	2018-09-08 13:18:13.197052	{}	75	iris/root/1_dicom_to_nifti/44_reformat_t1/out_reformatted_image	iris/root/65_spm_tissue_quantification/66_InputImages/81_T1_images/in_source	NiftiImageFileCompressed	iris/root
2156	secret		2018-09-08 13:18:13.177001	2018-09-08 13:18:13.177005	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/out_log_file	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation/in_input_image	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2157	prevention		2018-09-08 13:18:13.177705	2018-09-08 13:18:13.177708	{}	75	iris/root/65_spm_tissue_quantification/67_InputImage/75_t1_passthrough/out_result	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/in_determinant_of_jacobian_flag	NiftiImageFileCompressed	iris/root/65_spm_tissue_quantification
2158	characteristics		2018-09-08 13:18:13.178326	2018-09-08 13:18:13.178329	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/out_log_file	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/in_transform	ElastixTransformFile	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2159	dusts		2018-09-08 13:18:13.178931	2018-09-08 13:18:13.178935	{}	75	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/78_const_reg_t1_to_spmtemplate_parameters_0/out_output	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/in_moving_image	ElastixParameterFile	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation
2160	binders		2018-09-08 13:18:13.17942	2018-09-08 13:18:13.179424	{}	75	iris/root/1_dicom_to_nifti/44_reformat_t1/out_reformatted_image	iris/root/6_qc_overview/122_t1_with_brain/in_image	NiftiImageFileCompressed	iris/root
2161	hilltops		2018-09-08 13:18:13.17993	2018-09-08 13:18:13.179934	{}	75	iris/root/5_BET_and_REG/7_threshold_labels/out_image	iris/root/6_qc_overview/122_t1_with_brain/in_segmentation	NiftiImageFileCompressed	iris/root
2162	calculations		2018-09-08 13:18:13.180459	2018-09-08 13:18:13.180463	{}	75	iris/root/5_BET_and_REG/27_const_hammers_brain_transform_threads_0/out_output	iris/root/5_BET_and_REG/17_hammers_brain_transform/in_transform	Int	iris/root/5_BET_and_REG
2163	traces		2018-09-08 13:18:13.181009	2018-09-08 13:18:13.181012	{}	75	iris/root/5_BET_and_REG/17_hammers_brain_transform/out_points	iris/root/5_BET_and_REG/121_combine_brains/in_method	NiftiImageFileCompressed	iris/root/5_BET_and_REG
2164	downgrades		2018-09-08 13:18:13.181676	2018-09-08 13:18:13.18168	{}	75	iris/root/1_dicom_to_nifti/44_reformat_t1/out_reformatted_image	iris/root/6_qc_overview/123_t1_with_wm_outline/in_segmentation	NiftiImageFileCompressed	iris/root
2165	drill		2018-09-08 13:18:13.18222	2018-09-08 13:18:13.182224	{}	75	iris/root/65_spm_tissue_quantification/69_outputs/73_wm_map/out_sink	iris/root/6_qc_overview/123_t1_with_wm_outline/in_segmentation	NiftiImageFileCompressed	iris/root
2166	coal		2018-09-08 13:18:13.182905	2018-09-08 13:18:13.18291	{}	75	iris/root/1_dicom_to_nifti/44_reformat_t1/out_reformatted_image	iris/root/6_qc_overview/42_t1_with_gm_outline/in_image	NiftiImageFileCompressed	iris/root
2167	horsepower		2018-09-08 13:18:13.183438	2018-09-08 13:18:13.183442	{}	75	iris/root/65_spm_tissue_quantification/69_outputs/80_gm_map/out_sink	iris/root/6_qc_overview/42_t1_with_gm_outline/in_segmentation	NiftiImageFileCompressed	iris/root
2168	cruises		2018-09-08 13:18:13.183983	2018-09-08 13:18:13.183987	{}	75	iris/root/5_BET_and_REG/125_dilate_brainmask/out_image	iris/root/5_BET_and_REG/12_t1w_registration/in_fixed_image	NiftiImageFileCompressed	iris/root/5_BET_and_REG
2169	hooks		2018-09-08 13:18:13.184753	2018-09-08 13:18:13.184758	{}	75	iris/root/5_BET_and_REG/56_brain_mask_registation/out_directory	iris/root/5_BET_and_REG/12_t1w_registration/in_parameters	ElastixTransformFile	iris/root/5_BET_and_REG
2170	point		2018-09-08 13:18:13.185316	2018-09-08 13:18:13.18532	{}	75	iris/root/2_hammers_atlas/64_hammers_brains_r5/out_output	iris/root/5_BET_and_REG/12_t1w_registration/in_threads	NiftiImageFileCompressed	iris/root
2171	increment		2018-09-08 13:18:13.185872	2018-09-08 13:18:13.185876	{}	75	iris/root/5_BET_and_REG/28_n4_bias_correction/out_corrected_image	iris/root/5_BET_and_REG/12_t1w_registration/in_fixed_image	NiftiImageFileCompressed	iris/root/5_BET_and_REG
2172	serials		2018-09-08 13:18:13.186399	2018-09-08 13:18:13.186403	{}	75	iris/root/2_hammers_atlas/50_hammers_brains/out_output	iris/root/5_BET_and_REG/17_hammers_brain_transform/in_jacobian_matrix_flag	NiftiImageFileCompressed	iris/root
2173	desertion		2018-09-08 13:18:13.186948	2018-09-08 13:18:13.186952	{}	75	iris/root/5_BET_and_REG/12_t1w_registration/out_transform	iris/root/5_BET_and_REG/17_hammers_brain_transform/in_image	ElastixTransformFile	iris/root/5_BET_and_REG
2174	dusts		2018-09-08 13:18:13.187498	2018-09-08 13:18:13.187502	{}	75	iris/root/5_BET_and_REG/49_const_t1w_registration_parameters_0/out_output	iris/root/5_BET_and_REG/12_t1w_registration/in_fixed_image	ElastixParameterFile	iris/root/5_BET_and_REG
2175	chattels		2018-09-08 13:18:13.188077	2018-09-08 13:18:13.188084	{}	75	iris/root/5_BET_and_REG/59_const_t1w_registration_threads_0/out_output	iris/root/5_BET_and_REG/12_t1w_registration/in_initial_transform	Int	iris/root/5_BET_and_REG
2176	trackers		2018-09-08 13:18:13.188669	2018-09-08 13:18:13.188673	{}	75	iris/root/0_statistics/52_const_get_brain_volume_selection_0/out_output	iris/root/0_statistics/14_get_brain_volume/in_selection	TxtFile	iris/root/0_statistics
2177	eighths		2018-09-08 13:18:13.189189	2018-09-08 13:18:13.189193	{}	75	iris/root/5_BET_and_REG/30_combine_labels/out_hard_segment	iris/root/6_qc_overview/21_t1_with_hammers/in_image	NiftiImageFileCompressed	iris/root
2178	sir		2018-09-08 13:18:13.189711	2018-09-08 13:18:13.189715	{}	75	iris/root/0_statistics/18_const_get_wm_volume_flatten_0/out_output	iris/root/0_statistics/126_get_wm_volume/in_volume	Boolean	iris/root/0_statistics
2179	traffic		2018-09-08 13:18:13.190234	2018-09-08 13:18:13.190238	{}	75	iris/root/5_BET_and_REG/30_combine_labels/out_hard_segment	iris/root/0_statistics/11_get_gm_volume/in_flatten	NiftiImageFileCompressed	iris/root
2180	equivalent		2018-09-08 13:18:13.190736	2018-09-08 13:18:13.19074	{}	75	iris/root/0_statistics/60_const_get_brain_volume_volume_0/out_output	iris/root/0_statistics/14_get_brain_volume/in_image	String	iris/root/0_statistics
2181	passengers		2018-09-08 13:18:13.191287	2018-09-08 13:18:13.191292	{}	75	iris/root/5_BET_and_REG/30_combine_labels/out_hard_segment	iris/root/0_statistics/126_get_wm_volume/in_image	NiftiImageFileCompressed	iris/root
2182	looks		2018-09-08 13:18:13.191773	2018-09-08 13:18:13.191777	{}	75	iris/root/65_spm_tissue_quantification/69_outputs/90_wm_spm/out_sink	iris/root/0_statistics/126_get_wm_volume/in_stddev	NiftiImageFileCompressed	iris/root
2183	seals		2018-09-08 13:18:13.192306	2018-09-08 13:18:13.19231	{}	75	iris/root/0_statistics/26_const_get_wm_volume_volume_0/out_output	iris/root/0_statistics/126_get_wm_volume/in_volume	String	iris/root/0_statistics
2184	firer		2018-09-08 13:18:13.19279	2018-09-08 13:18:13.192794	{}	75	iris/root/0_statistics/45_const_get_wm_volume_selection_0/out_output	iris/root/0_statistics/126_get_wm_volume/in_flatten	TxtFile	iris/root/0_statistics
2185	eliminators		2018-09-08 13:18:13.19329	2018-09-08 13:18:13.193294	{}	75	iris/root/1_dicom_to_nifti/44_reformat_t1/out_reformatted_image	iris/root/6_qc_overview/21_t1_with_hammers/in_image	NiftiImageFileCompressed	iris/root
2186	message		2018-09-08 13:18:13.193762	2018-09-08 13:18:13.193766	{}	75	iris/root/5_BET_and_REG/30_combine_labels/out_hard_segment	iris/root/0_statistics/19_mask_hammers_labels/in_operator	NiftiImageFileCompressed	iris/root
2187	kettles		2018-09-08 13:18:13.194241	2018-09-08 13:18:13.194246	{}	75	iris/root/65_spm_tissue_quantification/69_outputs/80_gm_map/out_sink	iris/root/0_statistics/19_mask_hammers_labels/in_operator	NiftiImageFileCompressed	iris/root
2188	bristle		2018-09-08 13:18:13.194718	2018-09-08 13:18:13.194722	{}	75	iris/root/0_statistics/41_const_mask_hammers_labels_operator_0/out_output	iris/root/0_statistics/19_mask_hammers_labels/in_left_hand	String	iris/root/0_statistics
2200	plexiglass		2018-09-08 13:18:13.202472	2018-09-08 13:18:13.202476	{}	75	iris/root/5_BET_and_REG/32_const_dilate_brainmask_operation_type_0/out_output	iris/root/5_BET_and_REG/125_dilate_brainmask/in_operation	__PxMorphology_0.3.0_interface__operation_type__Enum__	iris/root/5_BET_and_REG
2201	tin		2018-09-08 13:18:13.203168	2018-09-08 13:18:13.203172	{}	75	iris/root/5_BET_and_REG/131_const_dilate_brainmask_operation_0/out_output	iris/root/5_BET_and_REG/125_dilate_brainmask/in_image	__PxMorphology_0.3.0_interface__operation__Enum__	iris/root/5_BET_and_REG
2202	oils		2018-09-08 13:18:13.20395	2018-09-08 13:18:13.203954	{}	75	iris/root/5_BET_and_REG/13_const_threshold_labels_upper_threshold_0/out_output	iris/root/5_BET_and_REG/7_threshold_labels/in_number_of_histbins	Float	iris/root/5_BET_and_REG
2203	assemblies		2018-09-08 13:18:13.204479	2018-09-08 13:18:13.204483	{}	75	iris/root/5_BET_and_REG/121_combine_brains/out_hard_segment	iris/root/5_BET_and_REG/7_threshold_labels/in_number_of_levels	NiftiImageFileCompressed	iris/root/5_BET_and_REG
2204	clangs		2018-09-08 13:18:13.205073	2018-09-08 13:18:13.205078	{}	75	iris/root/2_hammers_atlas/64_hammers_brains_r5/out_output	iris/root/5_BET_and_REG/56_brain_mask_registation/in_moving_image	NiftiImageFileCompressed	iris/root
2205	mirrors		2018-09-08 13:18:13.205975	2018-09-08 13:18:13.205979	{}	75	iris/root/5_BET_and_REG/130_bet/out_brain_image	iris/root/5_BET_and_REG/28_n4_bias_correction/in_n4_parameters	NiftiImageFileCompressed	iris/root/5_BET_and_REG
2206	secrets		2018-09-08 13:18:13.206601	2018-09-08 13:18:13.206605	{}	75	iris/root/5_BET_and_REG/33_label_registration/out_determinant_of_jacobian	iris/root/5_BET_and_REG/30_combine_labels/in_number_of_classes	NiftiImageFileCompressed	iris/root/5_BET_and_REG
2207	recruit		2018-09-08 13:18:13.207259	2018-09-08 13:18:13.207263	{}	75	iris/root/5_BET_and_REG/53_const_label_registration_threads_0/out_output	iris/root/5_BET_and_REG/33_label_registration/in_priority	Int	iris/root/5_BET_and_REG
2208	highline		2018-09-08 13:18:13.207935	2018-09-08 13:18:13.207939	{}	75	iris/root/5_BET_and_REG/124_const_combine_labels_number_of_classes_0/out_output	iris/root/5_BET_and_REG/30_combine_labels/in_method	Int	iris/root/5_BET_and_REG
2209	torques		2018-09-08 13:18:13.208509	2018-09-08 13:18:13.208513	{}	75	iris/root/5_BET_and_REG/63_const_brain_mask_registation_threads_0/out_output	iris/root/5_BET_and_REG/56_brain_mask_registation/in_initial_transform	Int	iris/root/5_BET_and_REG
2210	sill		2018-09-08 13:18:13.209065	2018-09-08 13:18:13.209069	{}	75	iris/root/5_BET_and_REG/51_const_combine_brains_number_of_classes_0/out_output	iris/root/5_BET_and_REG/121_combine_brains/in_substitute_labels	Int	iris/root/5_BET_and_REG
2211	caves		2018-09-08 13:18:13.20976	2018-09-08 13:18:13.209764	{}	75	iris/root/5_BET_and_REG/58_const_combine_brains_method_0/out_output	iris/root/5_BET_and_REG/121_combine_brains/in_images	__PxCombineSegmentations_0.3.2_interface__method__Enum__	iris/root/5_BET_and_REG
2212	wishes		2018-09-08 13:18:13.210344	2018-09-08 13:18:13.210348	{}	75	iris/root/5_BET_and_REG/12_t1w_registration/out_log_file	iris/root/5_BET_and_REG/33_label_registration/in_priority	ElastixTransformFile	iris/root/5_BET_and_REG
2213	rust		2018-09-08 13:18:13.21097	2018-09-08 13:18:13.210973	{}	75	iris/root/2_hammers_atlas/36_hammers_labels/out_output	iris/root/5_BET_and_REG/33_label_registration/in_determinant_of_jacobian_flag	NiftiImageFileCompressed	iris/root
2214	demonstrations		2018-09-08 13:18:13.211569	2018-09-08 13:18:13.211573	{}	75	iris/root/6_qc_overview/46_t1_qc/out_png_image	iris/root/6_qc_overview/54_t1_qc_sink/in_input	PngImageFile	iris/root/6_qc_overview
2215	workmen		2018-09-08 13:18:13.212168	2018-09-08 13:18:13.212172	{}	75	iris/root/6_qc_overview/21_t1_with_hammers/out_png_image	iris/root/6_qc_overview/31_t1_with_hammers_sink/in_input	PngImageFile	iris/root/6_qc_overview
2216	disciplines		2018-09-08 13:18:13.212773	2018-09-08 13:18:13.212777	{}	75	iris/root/5_BET_and_REG/24_const_combine_labels_method_0/out_output	iris/root/5_BET_and_REG/30_combine_labels/in_original_labels	__PxCombineSegmentations_0.3.2_interface__method__Enum__	iris/root/5_BET_and_REG
2217	zips		2018-09-08 13:18:13.213322	2018-09-08 13:18:13.213326	{}	75	iris/root/1_dicom_to_nifti/44_reformat_t1/out_reformatted_image	iris/root/4_output/29_t1_nifti_images/in_input	NiftiImageFileCompressed	iris/root
2218	needle		2018-09-08 13:18:13.213985	2018-09-08 13:18:13.213989	{}	75	iris/root/1_dicom_to_nifti/44_reformat_t1/out_reformatted_image	iris/root/6_qc_overview/46_t1_qc/in_image	NiftiImageFileCompressed	iris/root
2219	fold		2018-09-08 13:18:13.214531	2018-09-08 13:18:13.214534	{}	75	iris/root/0_statistics/48_const_get_brain_volume_flatten_0/out_output	iris/root/0_statistics/14_get_brain_volume/in_flatten	Boolean	iris/root/0_statistics
2220	evenings		2018-09-08 13:18:13.215118	2018-09-08 13:18:13.215122	{}	75	iris/root/5_BET_and_REG/30_combine_labels/out_hard_segment	iris/root/4_output/23_multi_atlas_segm/in_input	NiftiImageFileCompressed	iris/root
2221	details		2018-09-08 13:18:13.215845	2018-09-08 13:18:13.215865	{}	75	iris/root/65_spm_tissue_quantification/69_outputs/73_wm_map/out_sink	iris/root/4_output/16_wm_map_output/in_input	NiftiImageFileCompressed	iris/root
2222	stresses		2018-09-08 13:18:13.216402	2018-09-08 13:18:13.216406	{}	75	iris/root/5_BET_and_REG/121_combine_brains/out_hard_segment	iris/root/0_statistics/14_get_brain_volume/in_image	NiftiImageFileCompressed	iris/root
2223	ships		2018-09-08 13:18:13.216959	2018-09-08 13:18:13.216963	{}	75	iris/root/5_BET_and_REG/7_threshold_labels/out_image	iris/root/4_output/57_hammer_brain_mask/in_input	NiftiImageFileCompressed	iris/root
2224	house		2018-09-08 13:18:13.217509	2018-09-08 13:18:13.217514	{}	75	iris/root/0_statistics/14_get_brain_volume/out_statistics	iris/root/0_statistics/55_brain_volume/in_input	CsvFile	iris/root/0_statistics
2225	candidates		2018-09-08 13:18:13.218061	2018-09-08 13:18:13.218064	{}	75	iris/root/0_statistics/11_get_gm_volume/out_statistics	iris/root/0_statistics/128_gm_volume/in_input	CsvFile	iris/root/0_statistics
2226	wound		2018-09-08 13:18:13.218594	2018-09-08 13:18:13.218597	{}	75	iris/root/6_qc_overview/123_t1_with_wm_outline/out_png_image	iris/root/6_qc_overview/37_t1_with_wm_outline_sink/in_input	PngImageFile	iris/root/6_qc_overview
2227	superintendent		2018-09-08 13:18:13.219151	2018-09-08 13:18:13.219155	{}	75	iris/root/65_spm_tissue_quantification/69_outputs/88_gm_spm/out_sink	iris/root/0_statistics/11_get_gm_volume/in_stddev	NiftiImageFileCompressed	iris/root
2228	disks		2018-09-08 13:18:13.21964	2018-09-08 13:18:13.219644	{}	75	iris/root/5_BET_and_REG/22_const_brain_mask_registation_parameters_0/out_output	iris/root/5_BET_and_REG/56_brain_mask_registation/in_priority	ElastixParameterFile	iris/root/5_BET_and_REG
2229	welders		2018-09-08 13:18:13.22018	2018-09-08 13:18:13.220184	{}	75	iris/root/0_statistics/9_const_get_gm_volume_flatten_0/out_output	iris/root/0_statistics/11_get_gm_volume/in_mean	Boolean	iris/root/0_statistics
2230	savings		2018-09-08 13:18:13.220703	2018-09-08 13:18:13.220707	{}	75	iris/root/5_BET_and_REG/130_bet/out_mesh_image	iris/root/5_BET_and_REG/125_dilate_brainmask/in_image	NiftiImageFileCompressed	iris/root/5_BET_and_REG
2231	aggravations		2018-09-08 13:18:13.221203	2018-09-08 13:18:13.221206	{}	75	iris/root/5_BET_and_REG/43_convert_brain/out_image	iris/root/5_BET_and_REG/38_threshold_brain/in_outside_value	NiftiImageFileCompressed	iris/root/5_BET_and_REG
2232	bus		2018-09-08 13:18:13.221821	2018-09-08 13:18:13.221825	{}	75	iris/root/0_statistics/126_get_wm_volume/out_statistics	iris/root/0_statistics/25_wm_volume/in_input	CsvFile	iris/root/0_statistics
2233	defense		2018-09-08 13:18:13.222358	2018-09-08 13:18:13.222362	{}	75	iris/root/1_dicom_to_nifti/44_reformat_t1/out_reformatted_image	iris/root/5_BET_and_REG/130_bet/in_head_radius	NiftiImageFileCompressed	iris/root
2235	lists		2018-09-08 13:18:13.223419	2018-09-08 13:18:13.223423	{}	75	iris/root/5_BET_and_REG/125_dilate_brainmask/out_image	iris/root/5_BET_and_REG/56_brain_mask_registation/in_priority	NiftiImageFileCompressed	iris/root/5_BET_and_REG
2236	microphone		2018-09-08 13:18:13.224062	2018-09-08 13:18:13.224066	{}	75	iris/root/1_dicom_to_nifti/61_const_t1_dicom_to_nifti_file_order_0/out_output	iris/root/1_dicom_to_nifti/20_t1_dicom_to_nifti/in_threshold	String	iris/root/1_dicom_to_nifti
2237	looks		2018-09-08 13:18:13.224716	2018-09-08 13:18:13.224719	{}	75	iris/root/5_BET_and_REG/47_const_convert_brain_component_type_0/out_output	iris/root/5_BET_and_REG/43_convert_brain/in_image	__PxCastConvert_0.3.2_interface__component_type__Enum__	iris/root/5_BET_and_REG
2238	meanings		2018-09-08 13:18:13.225307	2018-09-08 13:18:13.225327	{}	75	iris/root/5_BET_and_REG/130_bet/out_skull_image	iris/root/5_BET_and_REG/43_convert_brain/in_component_type	NiftiImageFileCompressed	iris/root/5_BET_and_REG
2239	compilers		2018-09-08 13:18:13.225961	2018-09-08 13:18:13.225965	{}	75	iris/root/5_BET_and_REG/10_const_bet_fraction_threshold_0/out_output	iris/root/5_BET_and_REG/130_bet/in_robust_estimation	Float	iris/root/5_BET_and_REG
2240	trip		2018-09-08 13:18:13.226524	2018-09-08 13:18:13.226528	{}	75	iris/root/5_BET_and_REG/15_const_bet_bias_cleanup_0/out_output	iris/root/5_BET_and_REG/130_bet/in_small_fov	Boolean	iris/root/5_BET_and_REG
\.


--
-- Name: links_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.links_id_seq', 2243, true);


--
-- Data for Name: nodes; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.nodes (id, path, title, description, created, modified, custom_data, type, parent_id, run_id, status, level) FROM stdin;
2124	example_2018-09-03T10-32-43/root	example		2018-09-03 08:32:43.870525	2018-09-03 08:32:43.870533	{}	NetworkRun	\N	43	\N	0
2125	example_2018-09-03T10-32-43/root/source1	source1	\N	2018-09-03 08:32:43.872016	2018-09-03 08:32:43.87202	{}	SourceNodeRun	2124	43	\N	1
2126	example_2018-09-03T10-32-43/root/sink1	sink1	\N	2018-09-03 08:32:43.872746	2018-09-03 08:32:43.87275	{}	SinkNodeRun	2124	43	\N	1
2127	example_2018-09-03T10-32-43/root/addint	addint	\N	2018-09-03 08:32:43.873391	2018-09-03 08:32:43.873395	{}	NodeRun	2124	43	\N	1
2128	example_2018-09-03T10-32-43/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 08:32:43.873971	2018-09-03 08:32:43.873974	{}	ConstantNodeRun	2124	43	\N	1
2129	example_2018-09-03T10-36-42/root	example		2018-09-03 08:36:43.176183	2018-09-03 08:36:43.17619	{}	NetworkRun	\N	44	\N	0
2130	example_2018-09-03T10-36-42/root/source1	source1	\N	2018-09-03 08:36:43.177337	2018-09-03 08:36:43.177342	{}	SourceNodeRun	2129	44	\N	1
2131	example_2018-09-03T10-36-42/root/sink1	sink1	\N	2018-09-03 08:36:43.178031	2018-09-03 08:36:43.178036	{}	SinkNodeRun	2129	44	\N	1
2132	example_2018-09-03T10-36-42/root/addint	addint	\N	2018-09-03 08:36:43.178695	2018-09-03 08:36:43.178699	{}	NodeRun	2129	44	\N	1
2133	example_2018-09-03T10-36-42/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 08:36:43.17941	2018-09-03 08:36:43.179414	{}	ConstantNodeRun	2129	44	\N	1
2134	example_2018-09-03T10-37-58/root	example		2018-09-03 08:37:59.155461	2018-09-03 08:37:59.155467	{}	NetworkRun	\N	45	\N	0
2135	example_2018-09-03T10-37-58/root/source1	source1	\N	2018-09-03 08:37:59.15647	2018-09-03 08:37:59.156475	{}	SourceNodeRun	2134	45	\N	1
2136	example_2018-09-03T10-37-58/root/sink1	sink1	\N	2018-09-03 08:37:59.156975	2018-09-03 08:37:59.156979	{}	SinkNodeRun	2134	45	\N	1
2137	example_2018-09-03T10-37-58/root/addint	addint	\N	2018-09-03 08:37:59.157473	2018-09-03 08:37:59.157477	{}	NodeRun	2134	45	\N	1
2138	example_2018-09-03T10-37-58/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 08:37:59.157915	2018-09-03 08:37:59.157919	{}	ConstantNodeRun	2134	45	\N	1
2139	example_2018-09-03T10-41-42/root	example		2018-09-03 08:41:42.761002	2018-09-03 08:41:42.761008	{}	NetworkRun	\N	46	\N	0
2140	example_2018-09-03T10-41-42/root/source1	source1	\N	2018-09-03 08:41:42.762012	2018-09-03 08:41:42.762017	{}	SourceNodeRun	2139	46	\N	1
2141	example_2018-09-03T10-41-42/root/sink1	sink1	\N	2018-09-03 08:41:42.762704	2018-09-03 08:41:42.762707	{}	SinkNodeRun	2139	46	\N	1
2142	example_2018-09-03T10-41-42/root/addint	addint	\N	2018-09-03 08:41:42.763299	2018-09-03 08:41:42.763303	{}	NodeRun	2139	46	\N	1
2143	example_2018-09-03T10-41-42/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 08:41:42.764036	2018-09-03 08:41:42.764041	{}	ConstantNodeRun	2139	46	\N	1
2144	example_2018-09-03T10-47-39/root	example		2018-09-03 08:47:39.877457	2018-09-03 08:47:39.877463	{}	NetworkRun	\N	47	\N	0
2145	example_2018-09-03T10-47-39/root/source1	source1	\N	2018-09-03 08:47:39.878495	2018-09-03 08:47:39.878499	{}	SourceNodeRun	2144	47	\N	1
2146	example_2018-09-03T10-47-39/root/sink1	sink1	\N	2018-09-03 08:47:39.87901	2018-09-03 08:47:39.879014	{}	SinkNodeRun	2144	47	\N	1
2147	example_2018-09-03T10-47-39/root/addint	addint	\N	2018-09-03 08:47:39.87958	2018-09-03 08:47:39.879584	{}	NodeRun	2144	47	\N	1
2148	example_2018-09-03T10-47-39/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 08:47:39.880358	2018-09-03 08:47:39.880362	{}	ConstantNodeRun	2144	47	\N	1
2149	example_2018-09-03T11-01-12/root	example		2018-09-03 09:01:12.296301	2018-09-03 09:01:12.296307	{}	NetworkRun	\N	48	\N	0
2150	example_2018-09-03T11-01-12/root/source1	source1	\N	2018-09-03 09:01:12.29752	2018-09-03 09:01:12.297526	{}	SourceNodeRun	2149	48	\N	1
2151	example_2018-09-03T11-01-12/root/sink1	sink1	\N	2018-09-03 09:01:12.298116	2018-09-03 09:01:12.29812	{}	SinkNodeRun	2149	48	\N	1
2152	example_2018-09-03T11-01-12/root/addint	addint	\N	2018-09-03 09:01:12.298813	2018-09-03 09:01:12.298817	{}	NodeRun	2149	48	\N	1
2153	example_2018-09-03T11-01-12/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 09:01:12.299471	2018-09-03 09:01:12.299478	{}	ConstantNodeRun	2149	48	\N	1
2154	example_2018-09-03T11-07-53/root	example		2018-09-03 09:07:54.382375	2018-09-03 09:07:54.382382	{}	NetworkRun	\N	49	\N	0
2155	example_2018-09-03T11-07-53/root/source1	source1	\N	2018-09-03 09:07:54.383375	2018-09-03 09:07:54.383379	{}	SourceNodeRun	2154	49	\N	1
2156	example_2018-09-03T11-07-53/root/sink1	sink1	\N	2018-09-03 09:07:54.383968	2018-09-03 09:07:54.383972	{}	SinkNodeRun	2154	49	\N	1
2157	example_2018-09-03T11-07-53/root/addint	addint	\N	2018-09-03 09:07:54.384572	2018-09-03 09:07:54.384576	{}	NodeRun	2154	49	\N	1
2158	example_2018-09-03T11-07-53/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 09:07:54.385251	2018-09-03 09:07:54.385256	{}	ConstantNodeRun	2154	49	\N	1
2159	example_2018-09-03T11-12-21/root	example		2018-09-03 09:12:21.575998	2018-09-03 09:12:21.576004	{}	NetworkRun	\N	50	\N	0
2160	example_2018-09-03T11-12-21/root/source1	source1	\N	2018-09-03 09:12:21.577084	2018-09-03 09:12:21.577088	{}	SourceNodeRun	2159	50	\N	1
2161	example_2018-09-03T11-12-21/root/sink1	sink1	\N	2018-09-03 09:12:21.577697	2018-09-03 09:12:21.577701	{}	SinkNodeRun	2159	50	\N	1
2162	example_2018-09-03T11-12-21/root/addint	addint	\N	2018-09-03 09:12:21.578263	2018-09-03 09:12:21.578267	{}	NodeRun	2159	50	\N	1
2163	example_2018-09-03T11-12-21/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 09:12:21.578824	2018-09-03 09:12:21.578828	{}	ConstantNodeRun	2159	50	\N	1
2164	example_2018-09-03T11-13-16/root	example		2018-09-03 09:13:16.472058	2018-09-03 09:13:16.472067	{}	NetworkRun	\N	51	\N	0
2165	example_2018-09-03T11-13-16/root/source1	source1	\N	2018-09-03 09:13:16.473029	2018-09-03 09:13:16.473034	{}	SourceNodeRun	2164	51	\N	1
2166	example_2018-09-03T11-13-16/root/sink1	sink1	\N	2018-09-03 09:13:16.473607	2018-09-03 09:13:16.473612	{}	SinkNodeRun	2164	51	\N	1
2167	example_2018-09-03T11-13-16/root/addint	addint	\N	2018-09-03 09:13:16.474141	2018-09-03 09:13:16.474145	{}	NodeRun	2164	51	\N	1
2168	example_2018-09-03T11-13-16/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 09:13:16.474681	2018-09-03 09:13:16.474685	{}	ConstantNodeRun	2164	51	\N	1
2169	example_2018-09-03T11-14-49/root	example		2018-09-03 09:14:49.294296	2018-09-03 09:14:49.294302	{}	NetworkRun	\N	52	\N	0
2170	example_2018-09-03T11-14-49/root/source1	source1	\N	2018-09-03 09:14:49.295294	2018-09-03 09:14:49.295298	{}	SourceNodeRun	2169	52	\N	1
2171	example_2018-09-03T11-14-49/root/sink1	sink1	\N	2018-09-03 09:14:49.296043	2018-09-03 09:14:49.296047	{}	SinkNodeRun	2169	52	\N	1
2172	example_2018-09-03T11-14-49/root/addint	addint	\N	2018-09-03 09:14:49.296651	2018-09-03 09:14:49.296655	{}	NodeRun	2169	52	\N	1
2173	example_2018-09-03T11-14-49/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 09:14:49.297206	2018-09-03 09:14:49.29721	{}	ConstantNodeRun	2169	52	\N	1
2174	example_2018-09-03T11-17-35/root	example		2018-09-03 09:17:35.575484	2018-09-03 09:17:35.575491	{}	NetworkRun	\N	53	\N	0
2175	example_2018-09-03T11-17-35/root/source1	source1	\N	2018-09-03 09:17:35.576482	2018-09-03 09:17:35.576487	{}	SourceNodeRun	2174	53	\N	1
2176	example_2018-09-03T11-17-35/root/sink1	sink1	\N	2018-09-03 09:17:35.577023	2018-09-03 09:17:35.577027	{}	SinkNodeRun	2174	53	\N	1
2177	example_2018-09-03T11-17-35/root/addint	addint	\N	2018-09-03 09:17:35.577585	2018-09-03 09:17:35.577589	{}	NodeRun	2174	53	\N	1
2178	example_2018-09-03T11-17-35/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 09:17:35.578065	2018-09-03 09:17:35.578068	{}	ConstantNodeRun	2174	53	\N	1
1968	failing_network_2018-08-22T11-29-01/root	failing_network		2018-08-22 09:29:02.226428	2018-08-22 09:29:02.226437	{}	NetworkRun	\N	32	\N	0
1969	failing_network_2018-08-22T11-29-01/root/step_1	step_1	\N	2018-08-22 09:29:02.228786	2018-08-22 09:29:02.228792	{}	NodeRun	1968	32	\N	1
1970	failing_network_2018-08-22T11-29-01/root/step_3	step_3	\N	2018-08-22 09:29:02.229702	2018-08-22 09:29:02.229706	{}	NodeRun	1968	32	\N	1
1971	failing_network_2018-08-22T11-29-01/root/step_2	step_2	\N	2018-08-22 09:29:02.230502	2018-08-22 09:29:02.230507	{}	NodeRun	1968	32	\N	1
1972	failing_network_2018-08-22T11-29-01/root/sum	sum	\N	2018-08-22 09:29:02.231143	2018-08-22 09:29:02.231147	{}	NodeRun	1968	32	\N	1
1973	failing_network_2018-08-22T11-29-01/root/source_1	source_1	\N	2018-08-22 09:29:02.231797	2018-08-22 09:29:02.231802	{}	SourceNodeRun	1968	32	\N	1
1974	failing_network_2018-08-22T11-29-01/root/sink_2	sink_2	\N	2018-08-22 09:29:02.23276	2018-08-22 09:29:02.232765	{}	SinkNodeRun	1968	32	\N	1
1975	failing_network_2018-08-22T11-29-01/root/sink_3	sink_3	\N	2018-08-22 09:29:02.233498	2018-08-22 09:29:02.233502	{}	SinkNodeRun	1968	32	\N	1
1976	failing_network_2018-08-22T11-29-01/root/range	range	\N	2018-08-22 09:29:02.234015	2018-08-22 09:29:02.234019	{}	NodeRun	1968	32	\N	1
1977	failing_network_2018-08-22T11-29-01/root/sink_1	sink_1	\N	2018-08-22 09:29:02.23473	2018-08-22 09:29:02.234735	{}	SinkNodeRun	1968	32	\N	1
1978	failing_network_2018-08-22T11-29-01/root/source_2	source_2	\N	2018-08-22 09:29:02.235368	2018-08-22 09:29:02.235375	{}	SourceNodeRun	1968	32	\N	1
1979	failing_network_2018-08-22T11-29-01/root/source_3	source_3	\N	2018-08-22 09:29:02.235962	2018-08-22 09:29:02.235966	{}	SourceNodeRun	1968	32	\N	1
1980	failing_network_2018-08-22T11-29-01/root/sink_4	sink_4	\N	2018-08-22 09:29:02.236524	2018-08-22 09:29:02.236528	{}	SinkNodeRun	1968	32	\N	1
1981	failing_network_2018-08-22T11-29-01/root/sink_5	sink_5	\N	2018-08-22 09:29:02.237024	2018-08-22 09:29:02.237028	{}	SinkNodeRun	1968	32	\N	1
1982	failing_network_2018-08-22T11-29-01/root/const_step_2_fail_1_0	const_step_2_fail_1_0	\N	2018-08-22 09:29:02.237703	2018-08-22 09:29:02.237707	{}	ConstantNodeRun	1968	32	\N	1
1983	failing_network_2018-08-22T11-29-01/root/const_step_1_fail_2_0	const_step_1_fail_2_0	\N	2018-08-22 09:29:02.238261	2018-08-22 09:29:02.238265	{}	ConstantNodeRun	1968	32	\N	1
1984	failing_network_2018-08-22T12-10-16/root	failing_network		2018-08-22 10:10:17.490134	2018-08-22 10:10:17.490142	{}	NetworkRun	\N	33	\N	0
1985	failing_network_2018-08-22T12-10-16/root/step_1	step_1	\N	2018-08-22 10:10:17.492473	2018-08-22 10:10:17.492478	{}	NodeRun	1984	33	\N	1
1986	failing_network_2018-08-22T12-10-16/root/step_3	step_3	\N	2018-08-22 10:10:17.493258	2018-08-22 10:10:17.493262	{}	NodeRun	1984	33	\N	1
1987	failing_network_2018-08-22T12-10-16/root/step_2	step_2	\N	2018-08-22 10:10:17.493978	2018-08-22 10:10:17.493982	{}	NodeRun	1984	33	\N	1
1988	failing_network_2018-08-22T12-10-16/root/sum	sum	\N	2018-08-22 10:10:17.49472	2018-08-22 10:10:17.494726	{}	NodeRun	1984	33	\N	1
1989	failing_network_2018-08-22T12-10-16/root/source_1	source_1	\N	2018-08-22 10:10:17.495446	2018-08-22 10:10:17.49545	{}	SourceNodeRun	1984	33	\N	1
1990	failing_network_2018-08-22T12-10-16/root/sink_2	sink_2	\N	2018-08-22 10:10:17.496187	2018-08-22 10:10:17.496191	{}	SinkNodeRun	1984	33	\N	1
1991	failing_network_2018-08-22T12-10-16/root/sink_3	sink_3	\N	2018-08-22 10:10:17.496759	2018-08-22 10:10:17.496764	{}	SinkNodeRun	1984	33	\N	1
1992	failing_network_2018-08-22T12-10-16/root/range	range	\N	2018-08-22 10:10:17.49733	2018-08-22 10:10:17.497334	{}	NodeRun	1984	33	\N	1
1993	failing_network_2018-08-22T12-10-16/root/sink_1	sink_1	\N	2018-08-22 10:10:17.4979	2018-08-22 10:10:17.497904	{}	SinkNodeRun	1984	33	\N	1
1994	failing_network_2018-08-22T12-10-16/root/source_2	source_2	\N	2018-08-22 10:10:17.498449	2018-08-22 10:10:17.498453	{}	SourceNodeRun	1984	33	\N	1
1995	failing_network_2018-08-22T12-10-16/root/source_3	source_3	\N	2018-08-22 10:10:17.499049	2018-08-22 10:10:17.499053	{}	SourceNodeRun	1984	33	\N	1
1996	failing_network_2018-08-22T12-10-16/root/sink_4	sink_4	\N	2018-08-22 10:10:17.499584	2018-08-22 10:10:17.499605	{}	SinkNodeRun	1984	33	\N	1
1997	failing_network_2018-08-22T12-10-16/root/sink_5	sink_5	\N	2018-08-22 10:10:17.500136	2018-08-22 10:10:17.50014	{}	SinkNodeRun	1984	33	\N	1
1998	failing_network_2018-08-22T12-10-16/root/const_step_2_fail_1_0	const_step_2_fail_1_0	\N	2018-08-22 10:10:17.500708	2018-08-22 10:10:17.500712	{}	ConstantNodeRun	1984	33	\N	1
1999	failing_network_2018-08-22T12-10-16/root/const_step_1_fail_2_0	const_step_1_fail_2_0	\N	2018-08-22 10:10:17.501392	2018-08-22 10:10:17.501397	{}	ConstantNodeRun	1984	33	\N	1
2000	failing_macro_top_level_2018-08-22T12-15-04/root	failing_macro_top_level		2018-08-22 10:15:04.821312	2018-08-22 10:15:04.821319	{}	NetworkRun	\N	34	\N	0
2001	failing_macro_top_level_2018-08-22T12-15-04/root/source_b	source_b	\N	2018-08-22 10:15:04.82258	2018-08-22 10:15:04.822603	{}	SourceNodeRun	2000	34	\N	1
2002	failing_macro_top_level_2018-08-22T12-15-04/root/source_c	source_c	\N	2018-08-22 10:15:04.823221	2018-08-22 10:15:04.823225	{}	SourceNodeRun	2000	34	\N	1
2003	failing_macro_top_level_2018-08-22T12-15-04/root/source_a	source_a	\N	2018-08-22 10:15:04.823866	2018-08-22 10:15:04.82387	{}	SourceNodeRun	2000	34	\N	1
2004	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro	failing_macro	\N	2018-08-22 10:15:04.824528	2018-08-22 10:15:04.824535	{}	MacroNodeRun	2000	34	\N	1
2005	failing_macro_top_level_2018-08-22T12-15-04/root/add	add	\N	2018-08-22 10:15:04.825236	2018-08-22 10:15:04.82524	{}	NodeRun	2000	34	\N	1
2006	failing_macro_top_level_2018-08-22T12-15-04/root/sink	sink	\N	2018-08-22 10:15:04.825833	2018-08-22 10:15:04.825837	{}	SinkNodeRun	2000	34	\N	1
2007	failing_macro_top_level_2018-08-22T12-15-04/root/const_add_right_hand_0	const_add_right_hand_0	\N	2018-08-22 10:15:04.826391	2018-08-22 10:15:04.826395	{}	ConstantNodeRun	2000	34	\N	1
2008	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1	step_1	\N	2018-08-22 10:15:04.827959	2018-08-22 10:15:04.827964	{}	NodeRun	2004	34	\N	2
2009	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3	step_3	\N	2018-08-22 10:15:04.828635	2018-08-22 10:15:04.828639	{}	NodeRun	2004	34	\N	2
2010	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2	step_2	\N	2018-08-22 10:15:04.829406	2018-08-22 10:15:04.82941	{}	NodeRun	2004	34	\N	2
2011	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sum	sum	\N	2018-08-22 10:15:04.830007	2018-08-22 10:15:04.83001	{}	NodeRun	2004	34	\N	2
2012	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_5	sink_5	\N	2018-08-22 10:15:04.830541	2018-08-22 10:15:04.830545	{}	SinkNodeRun	2004	34	\N	2
2013	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_2	sink_2	\N	2018-08-22 10:15:04.8311	2018-08-22 10:15:04.831104	{}	SinkNodeRun	2004	34	\N	2
2014	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_3	sink_3	\N	2018-08-22 10:15:04.831615	2018-08-22 10:15:04.831619	{}	SinkNodeRun	2004	34	\N	2
2015	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range	range	\N	2018-08-22 10:15:04.832105	2018-08-22 10:15:04.832108	{}	NodeRun	2004	34	\N	2
2016	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_1	sink_1	\N	2018-08-22 10:15:04.832806	2018-08-22 10:15:04.83281	{}	SinkNodeRun	2004	34	\N	2
2017	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_2	source_2	\N	2018-08-22 10:15:04.833481	2018-08-22 10:15:04.833485	{}	SourceNodeRun	2004	34	\N	2
2018	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_3	source_3	\N	2018-08-22 10:15:04.834034	2018-08-22 10:15:04.834038	{}	SourceNodeRun	2004	34	\N	2
2019	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_4	sink_4	\N	2018-08-22 10:15:04.834559	2018-08-22 10:15:04.834578	{}	SinkNodeRun	2004	34	\N	2
2020	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1	source_1	\N	2018-08-22 10:15:04.835063	2018-08-22 10:15:04.835066	{}	SourceNodeRun	2004	34	\N	2
2021	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_2_fail_1_0	const_step_2_fail_1_0	\N	2018-08-22 10:15:04.835496	2018-08-22 10:15:04.8355	{}	ConstantNodeRun	2004	34	\N	2
2022	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_1_fail_2_0	const_step_1_fail_2_0	\N	2018-08-22 10:15:04.836037	2018-08-22 10:15:04.836041	{}	ConstantNodeRun	2004	34	\N	2
2023	macro_top_level_2018-08-22T12-36-32/root	macro_top_level		2018-08-22 10:36:33.094612	2018-08-22 10:36:33.094619	{}	NetworkRun	\N	35	\N	0
2024	macro_top_level_2018-08-22T12-36-32/root/source	source	\N	2018-08-22 10:36:33.096176	2018-08-22 10:36:33.09618	{}	NetworkStep	2023	35	\N	1
2025	macro_top_level_2018-08-22T12-36-32/root/node_add_ints	node_add_ints	\N	2018-08-22 10:36:33.096901	2018-08-22 10:36:33.096905	{}	MacroNodeRun	2023	35	\N	1
2026	macro_top_level_2018-08-22T12-36-32/root/sink	sink	\N	2018-08-22 10:36:33.097585	2018-08-22 10:36:33.097588	{}	SinkNodeRun	2023	35	\N	1
2027	macro_top_level_2018-08-22T12-36-32/root/source/source	source	\N	2018-08-22 10:36:33.098698	2018-08-22 10:36:33.098703	{}	SourceNodeRun	2024	35	\N	2
2028	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value	input_value	\N	2018-08-22 10:36:33.099474	2018-08-22 10:36:33.099478	{}	SourceNodeRun	2025	35	\N	2
2029	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1	node_add_multiple_ints_1	\N	2018-08-22 10:36:33.100076	2018-08-22 10:36:33.10008	{}	MacroNodeRun	2025	35	\N	2
2030	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/output_value	output_value	\N	2018-08-22 10:36:33.100762	2018-08-22 10:36:33.100766	{}	SinkNodeRun	2025	35	\N	2
2031	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2	node_add_multiple_ints_2	\N	2018-08-22 10:36:33.101392	2018-08-22 10:36:33.101395	{}	MacroNodeRun	2025	35	\N	2
2032	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add	add	\N	2018-08-22 10:36:33.102608	2018-08-22 10:36:33.102612	{}	NetworkStep	2029	35	\N	3
2033	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink	macro_sink	\N	2018-08-22 10:36:33.10321	2018-08-22 10:36:33.103214	{}	SinkNodeRun	2029	35	\N	3
2034	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input	input	\N	2018-08-22 10:36:33.103793	2018-08-22 10:36:33.103797	{}	SourceNodeRun	2029	35	\N	3
2035	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add	add	\N	2018-08-22 10:36:33.104274	2018-08-22 10:36:33.104277	{}	NetworkStep	2031	35	\N	3
2036	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/macro_sink	macro_sink	\N	2018-08-22 10:36:33.104802	2018-08-22 10:36:33.104806	{}	SinkNodeRun	2031	35	\N	3
2037	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input	input	\N	2018-08-22 10:36:33.105512	2018-08-22 10:36:33.105516	{}	SourceNodeRun	2031	35	\N	3
2038	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1	addint1	\N	2018-08-22 10:36:33.10679	2018-08-22 10:36:33.106794	{}	NodeRun	2032	35	\N	4
2039	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2	addint2	\N	2018-08-22 10:36:33.107408	2018-08-22 10:36:33.107414	{}	NodeRun	2032	35	\N	4
2040	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/const_addint1_right_hand_0	const_addint1_right_hand_0	\N	2018-08-22 10:36:33.108	2018-08-22 10:36:33.108004	{}	ConstantNodeRun	2032	35	\N	4
2041	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/const_addint2_right_hand_0	const_addint2_right_hand_0	\N	2018-08-22 10:36:33.108601	2018-08-22 10:36:33.108605	{}	ConstantNodeRun	2032	35	\N	4
2042	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1	addint1	\N	2018-08-22 10:36:33.109208	2018-08-22 10:36:33.109211	{}	NodeRun	2035	35	\N	4
2043	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2	addint2	\N	2018-08-22 10:36:33.10969	2018-08-22 10:36:33.109694	{}	NodeRun	2035	35	\N	4
2044	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/const_addint1_right_hand_0	const_addint1_right_hand_0	\N	2018-08-22 10:36:33.110191	2018-08-22 10:36:33.110195	{}	ConstantNodeRun	2035	35	\N	4
2045	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/const_addint2_right_hand_0	const_addint2_right_hand_0	\N	2018-08-22 10:36:33.110624	2018-08-22 10:36:33.110627	{}	ConstantNodeRun	2035	35	\N	4
2046	macro_node_2_2018-08-22T12-40-46/root	macro_node_2		2018-08-22 10:40:46.850652	2018-08-22 10:40:46.85066	{}	NetworkRun	\N	36	\N	0
2047	macro_node_2_2018-08-22T12-40-46/root/sum	sum	\N	2018-08-22 10:40:46.852162	2018-08-22 10:40:46.852166	{}	NodeRun	2046	36	\N	1
2048	macro_node_2_2018-08-22T12-40-46/root/addint	addint	\N	2018-08-22 10:40:46.852917	2018-08-22 10:40:46.852921	{}	NodeRun	2046	36	\N	1
2049	macro_node_2_2018-08-22T12-40-46/root/source_2	source_2	\N	2018-08-22 10:40:46.853479	2018-08-22 10:40:46.853483	{}	SourceNodeRun	2046	36	\N	1
2050	macro_node_2_2018-08-22T12-40-46/root/source_3	source_3	\N	2018-08-22 10:40:46.854118	2018-08-22 10:40:46.854122	{}	SourceNodeRun	2046	36	\N	1
2051	macro_node_2_2018-08-22T12-40-46/root/source_1	source_1	\N	2018-08-22 10:40:46.854749	2018-08-22 10:40:46.854754	{}	SourceNodeRun	2046	36	\N	1
2052	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1	node_add_multiple_ints_1	\N	2018-08-22 10:40:46.855318	2018-08-22 10:40:46.855322	{}	MacroNodeRun	2046	36	\N	1
2053	macro_node_2_2018-08-22T12-40-46/root/sink	sink	\N	2018-08-22 10:40:46.855857	2018-08-22 10:40:46.855861	{}	SinkNodeRun	2046	36	\N	1
2054	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink	macro_sink	\N	2018-08-22 10:40:46.857251	2018-08-22 10:40:46.857256	{}	SinkNodeRun	2052	36	\N	2
2055	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1	addint1	\N	2018-08-22 10:40:46.857885	2018-08-22 10:40:46.85789	{}	NodeRun	2052	36	\N	2
2056	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1	macro_input_1	\N	2018-08-22 10:40:46.858411	2018-08-22 10:40:46.858415	{}	SourceNodeRun	2052	36	\N	2
2057	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_2	macro_input_2	\N	2018-08-22 10:40:46.85897	2018-08-22 10:40:46.858974	{}	SourceNodeRun	2052	36	\N	2
2058	cross_validation_2018-08-22T15-29-33/root	cross_validation		2018-08-22 13:29:33.777375	2018-08-22 13:29:33.777385	{}	NetworkRun	\N	37	\N	0
2059	cross_validation_2018-08-22T15-29-33/root/crossvalidtion	crossvalidtion	\N	2018-08-22 13:29:33.778842	2018-08-22 13:29:33.778889	{}	AdvancedFlowNodeRun	2058	37	\N	1
2060	cross_validation_2018-08-22T15-29-33/root/sum	sum	\N	2018-08-22 13:29:33.779711	2018-08-22 13:29:33.779716	{}	NodeRun	2058	37	\N	1
2061	cross_validation_2018-08-22T15-29-33/root/const_crossvalidtion_method_0	const_crossvalidtion_method_0	\N	2018-08-22 13:29:33.780481	2018-08-22 13:29:33.780486	{}	ConstantNodeRun	2058	37	\N	1
2062	cross_validation_2018-08-22T15-29-33/root/numbers	numbers	\N	2018-08-22 13:29:33.781203	2018-08-22 13:29:33.781207	{}	SourceNodeRun	2058	37	\N	1
2063	cross_validation_2018-08-22T15-29-33/root/multiply	multiply	\N	2018-08-22 13:29:33.781865	2018-08-22 13:29:33.781869	{}	NodeRun	2058	37	\N	1
2064	cross_validation_2018-08-22T15-29-33/root/const_crossvalidtion_number_of_folds_0	const_crossvalidtion_number_of_folds_0	\N	2018-08-22 13:29:33.782765	2018-08-22 13:29:33.782769	{}	ConstantNodeRun	2058	37	\N	1
2065	cross_validation_2018-08-22T15-29-33/root/sink	sink	\N	2018-08-22 13:29:33.78343	2018-08-22 13:29:33.783434	{}	SinkNodeRun	2058	37	\N	1
2066	input_groups_2018-08-22T15-39-07/root	input_groups		2018-08-22 13:39:07.328017	2018-08-22 13:39:07.328024	{}	NetworkRun	\N	38	\N	0
2067	input_groups_2018-08-22T15-39-07/root/source	source	\N	2018-08-22 13:39:07.329625	2018-08-22 13:39:07.329629	{}	SourceNodeRun	2066	38	\N	1
2068	input_groups_2018-08-22T15-39-07/root/add	add	\N	2018-08-22 13:39:07.330359	2018-08-22 13:39:07.330363	{}	NodeRun	2066	38	\N	1
2069	input_groups_2018-08-22T15-39-07/root/const_add_right_hand_0	const_add_right_hand_0	\N	2018-08-22 13:39:07.330888	2018-08-22 13:39:07.330892	{}	ConstantNodeRun	2066	38	\N	1
2070	input_groups_2018-08-22T15-39-07/root/sink	sink	\N	2018-08-22 13:39:07.331479	2018-08-22 13:39:07.331483	{}	SinkNodeRun	2066	38	\N	1
2179	example_2018-09-03T11-31-38/root	example		2018-09-03 09:31:39.065422	2018-09-03 09:31:39.06543	{}	NetworkRun	\N	54	\N	0
2180	example_2018-09-03T11-31-38/root/source1	source1	\N	2018-09-03 09:31:39.066621	2018-09-03 09:31:39.066626	{}	SourceNodeRun	2179	54	\N	1
2181	example_2018-09-03T11-31-38/root/sink1	sink1	\N	2018-09-03 09:31:39.06735	2018-09-03 09:31:39.067354	{}	SinkNodeRun	2179	54	\N	1
2182	example_2018-09-03T11-31-38/root/addint	addint	\N	2018-09-03 09:31:39.068068	2018-09-03 09:31:39.068071	{}	NodeRun	2179	54	\N	1
2183	example_2018-09-03T11-31-38/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 09:31:39.068759	2018-09-03 09:31:39.068763	{}	ConstantNodeRun	2179	54	\N	1
2184	auto_prefix_test_2018-09-03T11-34-05/root	auto_prefix_test		2018-09-03 09:34:06.63519	2018-09-03 09:34:06.635195	{}	NetworkRun	\N	55	\N	0
2185	auto_prefix_test_2018-09-03T11-34-05/root/const	const	\N	2018-09-03 09:34:06.636558	2018-09-03 09:34:06.636563	{}	ConstantNodeRun	2184	55	\N	1
2186	auto_prefix_test_2018-09-03T11-34-05/root/source	source	\N	2018-09-03 09:34:06.637195	2018-09-03 09:34:06.637199	{}	SourceNodeRun	2184	55	\N	1
2187	auto_prefix_test_2018-09-03T11-34-05/root/m_p	m_p	\N	2018-09-03 09:34:06.637759	2018-09-03 09:34:06.637763	{}	NodeRun	2184	55	\N	1
2188	auto_prefix_test_2018-09-03T11-34-05/root/a_p	a_p	\N	2018-09-03 09:34:06.638379	2018-09-03 09:34:06.638383	{}	NodeRun	2184	55	\N	1
2189	auto_prefix_test_2018-09-03T11-34-05/root/m_n	m_n	\N	2018-09-03 09:34:06.638916	2018-09-03 09:34:06.63892	{}	NodeRun	2184	55	\N	1
2190	auto_prefix_test_2018-09-03T11-34-05/root/a_n	a_n	\N	2018-09-03 09:34:06.639369	2018-09-03 09:34:06.639373	{}	NodeRun	2184	55	\N	1
2191	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_p	sink_m_p	\N	2018-09-03 09:34:06.639909	2018-09-03 09:34:06.639912	{}	SinkNodeRun	2184	55	\N	1
2192	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_p	sink_a_p	\N	2018-09-03 09:34:06.640614	2018-09-03 09:34:06.640618	{}	SinkNodeRun	2184	55	\N	1
2193	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_n	sink_m_n	\N	2018-09-03 09:34:06.64111	2018-09-03 09:34:06.641113	{}	SinkNodeRun	2184	55	\N	1
2194	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_n	sink_a_n	\N	2018-09-03 09:34:06.641575	2018-09-03 09:34:06.641579	{}	SinkNodeRun	2184	55	\N	1
2195	example_2018-09-03T12-17-58/root	example		2018-09-03 10:17:58.520026	2018-09-03 10:17:58.520034	{}	NetworkRun	\N	56	\N	0
2196	example_2018-09-03T12-17-58/root/source1	source1	\N	2018-09-03 10:17:58.521346	2018-09-03 10:17:58.52135	{}	SourceNodeRun	2195	56	\N	1
2197	example_2018-09-03T12-17-58/root/sink1	sink1	\N	2018-09-03 10:17:58.52198	2018-09-03 10:17:58.521984	{}	SinkNodeRun	2195	56	\N	1
2198	example_2018-09-03T12-17-58/root/addint	addint	\N	2018-09-03 10:17:58.522618	2018-09-03 10:17:58.522622	{}	NodeRun	2195	56	\N	1
2199	example_2018-09-03T12-17-58/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 10:17:58.523232	2018-09-03 10:17:58.523253	{}	ConstantNodeRun	2195	56	\N	1
2249	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_2	source_2	\N	2018-09-03 12:01:00.752264	2018-09-03 12:01:00.752268	{}	SourceNodeRun	2244	65	\N	2
2250	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_3	source_3	\N	2018-09-03 12:01:00.752843	2018-09-03 12:01:00.752848	{}	SourceNodeRun	2244	65	\N	2
2251	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_1	sink_1	\N	2018-09-03 12:01:00.753436	2018-09-03 12:01:00.75344	{}	SinkNodeRun	2244	65	\N	2
2252	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_2	sink_2	\N	2018-09-03 12:01:00.753996	2018-09-03 12:01:00.754	{}	SinkNodeRun	2244	65	\N	2
2253	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_3	sink_3	\N	2018-09-03 12:01:00.754633	2018-09-03 12:01:00.754637	{}	SinkNodeRun	2244	65	\N	2
2254	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_4	sink_4	\N	2018-09-03 12:01:00.755125	2018-09-03 12:01:00.755129	{}	SinkNodeRun	2244	65	\N	2
2255	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_5	sink_5	\N	2018-09-03 12:01:00.755576	2018-09-03 12:01:00.75558	{}	SinkNodeRun	2244	65	\N	2
2256	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1	step_1	\N	2018-09-03 12:01:00.756126	2018-09-03 12:01:00.756142	{}	NodeRun	2244	65	\N	2
2257	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2	step_2	\N	2018-09-03 12:01:00.756733	2018-09-03 12:01:00.756736	{}	NodeRun	2244	65	\N	2
2258	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3	step_3	\N	2018-09-03 12:01:00.757192	2018-09-03 12:01:00.757196	{}	NodeRun	2244	65	\N	2
2259	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range	range	\N	2018-09-03 12:01:00.757662	2018-09-03 12:01:00.757666	{}	NodeRun	2244	65	\N	2
2260	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sum	sum	\N	2018-09-03 12:01:00.758113	2018-09-03 12:01:00.758117	{}	NodeRun	2244	65	\N	2
2261	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_1_fail_2_0	const_step_1_fail_2_0	\N	2018-09-03 12:01:00.758536	2018-09-03 12:01:00.75854	{}	ConstantNodeRun	2244	65	\N	2
2262	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_2_fail_1_0	const_step_2_fail_1_0	\N	2018-09-03 12:01:00.759011	2018-09-03 12:01:00.759015	{}	ConstantNodeRun	2244	65	\N	2
2108	failing_network_2018-08-24T15-34-44/root	failing_network		2018-08-24 13:34:44.920415	2018-08-24 13:34:44.920423	{}	NetworkRun	\N	42	\N	0
2109	failing_network_2018-08-24T15-34-44/root/source_1	source_1	\N	2018-08-24 13:34:44.922178	2018-08-24 13:34:44.922182	{}	SourceNodeRun	2108	42	\N	1
2110	failing_network_2018-08-24T15-34-44/root/source_2	source_2	\N	2018-08-24 13:34:44.92301	2018-08-24 13:34:44.923015	{}	SourceNodeRun	2108	42	\N	1
2111	failing_network_2018-08-24T15-34-44/root/source_3	source_3	\N	2018-08-24 13:34:44.923799	2018-08-24 13:34:44.923803	{}	SourceNodeRun	2108	42	\N	1
2112	failing_network_2018-08-24T15-34-44/root/sink_1	sink_1	\N	2018-08-24 13:34:44.924435	2018-08-24 13:34:44.924439	{}	SinkNodeRun	2108	42	\N	1
2113	failing_network_2018-08-24T15-34-44/root/sink_2	sink_2	\N	2018-08-24 13:34:44.925082	2018-08-24 13:34:44.925087	{}	SinkNodeRun	2108	42	\N	1
2114	failing_network_2018-08-24T15-34-44/root/sink_3	sink_3	\N	2018-08-24 13:34:44.925819	2018-08-24 13:34:44.925823	{}	SinkNodeRun	2108	42	\N	1
2115	failing_network_2018-08-24T15-34-44/root/sink_4	sink_4	\N	2018-08-24 13:34:44.926344	2018-08-24 13:34:44.926348	{}	SinkNodeRun	2108	42	\N	1
2116	failing_network_2018-08-24T15-34-44/root/sink_5	sink_5	\N	2018-08-24 13:34:44.926904	2018-08-24 13:34:44.926908	{}	SinkNodeRun	2108	42	\N	1
2117	failing_network_2018-08-24T15-34-44/root/step_1	step_1	\N	2018-08-24 13:34:44.927533	2018-08-24 13:34:44.927537	{}	NodeRun	2108	42	\N	1
2118	failing_network_2018-08-24T15-34-44/root/step_2	step_2	\N	2018-08-24 13:34:44.928222	2018-08-24 13:34:44.928226	{}	NodeRun	2108	42	\N	1
2119	failing_network_2018-08-24T15-34-44/root/step_3	step_3	\N	2018-08-24 13:34:44.92894	2018-08-24 13:34:44.928944	{}	NodeRun	2108	42	\N	1
2120	failing_network_2018-08-24T15-34-44/root/range	range	\N	2018-08-24 13:34:44.929508	2018-08-24 13:34:44.929512	{}	NodeRun	2108	42	\N	1
2121	failing_network_2018-08-24T15-34-44/root/sum	sum	\N	2018-08-24 13:34:44.930063	2018-08-24 13:34:44.930066	{}	NodeRun	2108	42	\N	1
2122	failing_network_2018-08-24T15-34-44/root/const_step_1_fail_2_0	const_step_1_fail_2_0	\N	2018-08-24 13:34:44.930828	2018-08-24 13:34:44.930834	{}	ConstantNodeRun	2108	42	\N	1
2123	failing_network_2018-08-24T15-34-44/root/const_step_2_fail_1_0	const_step_2_fail_1_0	\N	2018-08-24 13:34:44.93135	2018-08-24 13:34:44.931354	{}	ConstantNodeRun	2108	42	\N	1
2200	example_2018-09-03T12-31-11/root	example		2018-09-03 10:31:12.117736	2018-09-03 10:31:12.117744	{}	NetworkRun	\N	57	\N	0
2201	example_2018-09-03T12-31-11/root/source1	source1	\N	2018-09-03 10:31:12.119033	2018-09-03 10:31:12.119037	{}	SourceNodeRun	2200	57	\N	1
2202	example_2018-09-03T12-31-11/root/sink1	sink1	\N	2018-09-03 10:31:12.119928	2018-09-03 10:31:12.119932	{}	SinkNodeRun	2200	57	\N	1
2203	example_2018-09-03T12-31-11/root/addint	addint	\N	2018-09-03 10:31:12.120749	2018-09-03 10:31:12.120753	{}	NodeRun	2200	57	\N	1
2204	example_2018-09-03T12-31-11/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 10:31:12.121456	2018-09-03 10:31:12.12146	{}	ConstantNodeRun	2200	57	\N	1
2205	example_2018-09-03T12-47-05/root	example		2018-09-03 10:47:06.271034	2018-09-03 10:47:06.27104	{}	NetworkRun	\N	58	\N	0
2206	example_2018-09-03T12-47-05/root/source1	source1	\N	2018-09-03 10:47:06.272028	2018-09-03 10:47:06.272032	{}	SourceNodeRun	2205	58	\N	1
2207	example_2018-09-03T12-47-05/root/sink1	sink1	\N	2018-09-03 10:47:06.272764	2018-09-03 10:47:06.272768	{}	SinkNodeRun	2205	58	\N	1
2208	example_2018-09-03T12-47-05/root/addint	addint	\N	2018-09-03 10:47:06.273353	2018-09-03 10:47:06.273357	{}	NodeRun	2205	58	\N	1
2209	example_2018-09-03T12-47-05/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 10:47:06.273866	2018-09-03 10:47:06.27387	{}	ConstantNodeRun	2205	58	\N	1
2210	example_2018-09-03T12-54-57/root	example		2018-09-03 10:54:57.323724	2018-09-03 10:54:57.32373	{}	NetworkRun	\N	59	\N	0
2211	example_2018-09-03T12-54-57/root/source1	source1	\N	2018-09-03 10:54:57.32481	2018-09-03 10:54:57.324814	{}	SourceNodeRun	2210	59	\N	1
2212	example_2018-09-03T12-54-57/root/sink1	sink1	\N	2018-09-03 10:54:57.325439	2018-09-03 10:54:57.325443	{}	SinkNodeRun	2210	59	\N	1
2213	example_2018-09-03T12-54-57/root/addint	addint	\N	2018-09-03 10:54:57.326101	2018-09-03 10:54:57.326105	{}	NodeRun	2210	59	\N	1
2214	example_2018-09-03T12-54-57/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 10:54:57.326685	2018-09-03 10:54:57.326688	{}	ConstantNodeRun	2210	59	\N	1
2215	example_2018-09-03T12-55-46/root	example		2018-09-03 10:55:46.991185	2018-09-03 10:55:46.991192	{}	NetworkRun	\N	60	\N	0
2216	example_2018-09-03T12-55-46/root/source1	source1	\N	2018-09-03 10:55:46.992225	2018-09-03 10:55:46.992229	{}	SourceNodeRun	2215	60	\N	1
2217	example_2018-09-03T12-55-46/root/sink1	sink1	\N	2018-09-03 10:55:46.992811	2018-09-03 10:55:46.992815	{}	SinkNodeRun	2215	60	\N	1
2218	example_2018-09-03T12-55-46/root/addint	addint	\N	2018-09-03 10:55:46.993335	2018-09-03 10:55:46.99334	{}	NodeRun	2215	60	\N	1
2219	example_2018-09-03T12-55-46/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 10:55:46.993913	2018-09-03 10:55:46.993917	{}	ConstantNodeRun	2215	60	\N	1
2220	example_2018-09-03T13-09-35/root	example		2018-09-03 11:09:36.231652	2018-09-03 11:09:36.231658	{}	NetworkRun	\N	61	\N	0
2221	example_2018-09-03T13-09-35/root/sink1	sink1	\N	2018-09-03 11:09:36.232784	2018-09-03 11:09:36.232788	{}	SinkNodeRun	2220	61	\N	1
2222	example_2018-09-03T13-09-35/root/addint	addint	\N	2018-09-03 11:09:36.233415	2018-09-03 11:09:36.233419	{}	NodeRun	2220	61	\N	1
2223	example_2018-09-03T13-09-35/root/source1	source1	\N	2018-09-03 11:09:36.234019	2018-09-03 11:09:36.234022	{}	SourceNodeRun	2220	61	\N	1
2224	example_2018-09-03T13-09-35/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 11:09:36.234607	2018-09-03 11:09:36.234611	{}	ConstantNodeRun	2220	61	\N	1
2225	example_2018-09-03T13-11-45/root	example		2018-09-03 11:11:45.547281	2018-09-03 11:11:45.547287	{}	NetworkRun	\N	62	\N	0
2226	example_2018-09-03T13-11-45/root/addint	addint	\N	2018-09-03 11:11:45.54852	2018-09-03 11:11:45.548524	{}	NodeRun	2225	62	\N	1
2227	example_2018-09-03T13-11-45/root/source1	source1	\N	2018-09-03 11:11:45.549272	2018-09-03 11:11:45.549276	{}	SourceNodeRun	2225	62	\N	1
2228	example_2018-09-03T13-11-45/root/sink1	sink1	\N	2018-09-03 11:11:45.549901	2018-09-03 11:11:45.549905	{}	SinkNodeRun	2225	62	\N	1
2229	example_2018-09-03T13-11-45/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 11:11:45.550585	2018-09-03 11:11:45.550589	{}	ConstantNodeRun	2225	62	\N	1
2230	example_2018-09-03T13-12-39/root	example		2018-09-03 11:12:40.054277	2018-09-03 11:12:40.054283	{}	NetworkRun	\N	63	\N	0
2231	example_2018-09-03T13-12-39/root/source1	source1	\N	2018-09-03 11:12:40.055343	2018-09-03 11:12:40.055348	{}	SourceNodeRun	2230	63	\N	1
2232	example_2018-09-03T13-12-39/root/sink1	sink1	\N	2018-09-03 11:12:40.055914	2018-09-03 11:12:40.055918	{}	SinkNodeRun	2230	63	\N	1
2233	example_2018-09-03T13-12-39/root/addint	addint	\N	2018-09-03 11:12:40.056618	2018-09-03 11:12:40.056622	{}	NodeRun	2230	63	\N	1
2234	example_2018-09-03T13-12-39/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 11:12:40.05721	2018-09-03 11:12:40.057214	{}	ConstantNodeRun	2230	63	\N	1
2235	example_2018-09-03T13-13-28/root	example		2018-09-03 11:13:28.413348	2018-09-03 11:13:28.413354	{}	NetworkRun	\N	64	\N	0
2236	example_2018-09-03T13-13-28/root/source1	source1	\N	2018-09-03 11:13:28.414592	2018-09-03 11:13:28.414597	{}	SourceNodeRun	2235	64	\N	1
2237	example_2018-09-03T13-13-28/root/sink1	sink1	\N	2018-09-03 11:13:28.415189	2018-09-03 11:13:28.415193	{}	SinkNodeRun	2235	64	\N	1
2238	example_2018-09-03T13-13-28/root/addint	addint	\N	2018-09-03 11:13:28.41591	2018-09-03 11:13:28.415915	{}	NodeRun	2235	64	\N	1
2239	example_2018-09-03T13-13-28/root/const_addint_right_hand_0	const_addint_right_hand_0	\N	2018-09-03 11:13:28.416632	2018-09-03 11:13:28.416637	{}	ConstantNodeRun	2235	64	\N	1
2240	failing_macro_top_level_2018-09-03T14-01-00/root	failing_macro_top_level		2018-09-03 12:01:00.744845	2018-09-03 12:01:00.744853	{}	NetworkRun	\N	65	\N	0
2241	failing_macro_top_level_2018-09-03T14-01-00/root/source_a	source_a	\N	2018-09-03 12:01:00.746177	2018-09-03 12:01:00.746181	{}	SourceNodeRun	2240	65	\N	1
2242	failing_macro_top_level_2018-09-03T14-01-00/root/source_b	source_b	\N	2018-09-03 12:01:00.746846	2018-09-03 12:01:00.746851	{}	SourceNodeRun	2240	65	\N	1
2243	failing_macro_top_level_2018-09-03T14-01-00/root/source_c	source_c	\N	2018-09-03 12:01:00.7475	2018-09-03 12:01:00.747504	{}	SourceNodeRun	2240	65	\N	1
2244	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro	failing_macro	\N	2018-09-03 12:01:00.748203	2018-09-03 12:01:00.748207	{}	MacroNodeRun	2240	65	\N	1
2245	failing_macro_top_level_2018-09-03T14-01-00/root/add	add	\N	2018-09-03 12:01:00.748904	2018-09-03 12:01:00.748908	{}	NodeRun	2240	65	\N	1
2246	failing_macro_top_level_2018-09-03T14-01-00/root/sink	sink	\N	2018-09-03 12:01:00.749551	2018-09-03 12:01:00.749555	{}	SinkNodeRun	2240	65	\N	1
2247	failing_macro_top_level_2018-09-03T14-01-00/root/const_add_right_hand_0	const_add_right_hand_0	\N	2018-09-03 12:01:00.75009	2018-09-03 12:01:00.750094	{}	ConstantNodeRun	2240	65	\N	1
2248	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1	source_1	\N	2018-09-03 12:01:00.751708	2018-09-03 12:01:00.751712	{}	SourceNodeRun	2244	65	\N	2
2263	add_ints_2018-09-03T14-04-33/root	add_ints		2018-09-03 12:04:33.320532	2018-09-03 12:04:33.320539	{}	NetworkRun	\N	66	\N	0
2264	add_ints_2018-09-03T14-04-33/root/source	source	\N	2018-09-03 12:04:33.321643	2018-09-03 12:04:33.321647	{}	SourceNodeRun	2263	66	\N	1
2265	add_ints_2018-09-03T14-04-33/root/add	add	\N	2018-09-03 12:04:33.322385	2018-09-03 12:04:33.322389	{}	NodeRun	2263	66	\N	1
2266	add_ints_2018-09-03T14-04-33/root/const_add_right_hand_0	const_add_right_hand_0	\N	2018-09-03 12:04:33.323029	2018-09-03 12:04:33.323033	{}	ConstantNodeRun	2263	66	\N	1
2267	add_ints_2018-09-03T14-04-33/root/sink	sink	\N	2018-09-03 12:04:33.323661	2018-09-03 12:04:33.323665	{}	SinkNodeRun	2263	66	\N	1
2268	macro_node_2_2018-09-03T14-14-40/root	macro_node_2		2018-09-03 12:14:40.492481	2018-09-03 12:14:40.492488	{}	NetworkRun	\N	67	\N	0
2269	macro_node_2_2018-09-03T14-14-40/root/addint	addint	\N	2018-09-03 12:14:40.493867	2018-09-03 12:14:40.493872	{}	NodeRun	2268	67	\N	1
2270	macro_node_2_2018-09-03T14-14-40/root/source_1	source_1	\N	2018-09-03 12:14:40.494477	2018-09-03 12:14:40.494481	{}	SourceNodeRun	2268	67	\N	1
2271	macro_node_2_2018-09-03T14-14-40/root/source_2	source_2	\N	2018-09-03 12:14:40.495047	2018-09-03 12:14:40.495051	{}	SourceNodeRun	2268	67	\N	1
2272	macro_node_2_2018-09-03T14-14-40/root/source_3	source_3	\N	2018-09-03 12:14:40.495599	2018-09-03 12:14:40.495603	{}	SourceNodeRun	2268	67	\N	1
2273	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1	node_add_multiple_ints_1	\N	2018-09-03 12:14:40.496162	2018-09-03 12:14:40.496166	{}	MacroNodeRun	2268	67	\N	1
2274	macro_node_2_2018-09-03T14-14-40/root/sum	sum	\N	2018-09-03 12:14:40.496741	2018-09-03 12:14:40.496745	{}	NodeRun	2268	67	\N	1
2275	macro_node_2_2018-09-03T14-14-40/root/sink	sink	\N	2018-09-03 12:14:40.497214	2018-09-03 12:14:40.497218	{}	SinkNodeRun	2268	67	\N	1
2276	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1	macro_input_1	\N	2018-09-03 12:14:40.498389	2018-09-03 12:14:40.498393	{}	SourceNodeRun	2273	67	\N	2
2277	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_2	macro_input_2	\N	2018-09-03 12:14:40.498961	2018-09-03 12:14:40.498964	{}	SourceNodeRun	2273	67	\N	2
2278	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1	addint1	\N	2018-09-03 12:14:40.499444	2018-09-03 12:14:40.499447	{}	NodeRun	2273	67	\N	2
2279	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink	macro_sink	\N	2018-09-03 12:14:40.500078	2018-09-03 12:14:40.500099	{}	SinkNodeRun	2273	67	\N	2
2280	add_ints_2018-09-03T14-18-44/root	add_ints		2018-09-03 12:18:44.653354	2018-09-03 12:18:44.65336	{}	NetworkRun	\N	68	\N	0
2281	add_ints_2018-09-03T14-18-44/root/source	source	\N	2018-09-03 12:18:44.654526	2018-09-03 12:18:44.654531	{}	SourceNodeRun	2280	68	\N	1
2282	add_ints_2018-09-03T14-18-44/root/add	add	\N	2018-09-03 12:18:44.655298	2018-09-03 12:18:44.655302	{}	NodeRun	2280	68	\N	1
2283	add_ints_2018-09-03T14-18-44/root/const_add_right_hand_0	const_add_right_hand_0	\N	2018-09-03 12:18:44.655984	2018-09-03 12:18:44.655988	{}	ConstantNodeRun	2280	68	\N	1
2284	add_ints_2018-09-03T14-18-44/root/sink	sink	\N	2018-09-03 12:18:44.656551	2018-09-03 12:18:44.656555	{}	SinkNodeRun	2280	68	\N	1
2285	auto_prefix_test_2018-09-03T14-30-01/root	auto_prefix_test		2018-09-03 12:30:02.201878	2018-09-03 12:30:02.201886	{}	NetworkRun	\N	69	\N	0
2286	auto_prefix_test_2018-09-03T14-30-01/root/const	const	\N	2018-09-03 12:30:02.203311	2018-09-03 12:30:02.203315	{}	ConstantNodeRun	2285	69	\N	1
2287	auto_prefix_test_2018-09-03T14-30-01/root/source	source	\N	2018-09-03 12:30:02.203971	2018-09-03 12:30:02.203975	{}	SourceNodeRun	2285	69	\N	1
2288	auto_prefix_test_2018-09-03T14-30-01/root/m_p	m_p	\N	2018-09-03 12:30:02.204604	2018-09-03 12:30:02.204608	{}	NodeRun	2285	69	\N	1
2289	auto_prefix_test_2018-09-03T14-30-01/root/a_p	a_p	\N	2018-09-03 12:30:02.205185	2018-09-03 12:30:02.205189	{}	NodeRun	2285	69	\N	1
2290	auto_prefix_test_2018-09-03T14-30-01/root/m_n	m_n	\N	2018-09-03 12:30:02.205739	2018-09-03 12:30:02.205743	{}	NodeRun	2285	69	\N	1
2291	auto_prefix_test_2018-09-03T14-30-01/root/a_n	a_n	\N	2018-09-03 12:30:02.206302	2018-09-03 12:30:02.206306	{}	NodeRun	2285	69	\N	1
2292	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_p	sink_m_p	\N	2018-09-03 12:30:02.206826	2018-09-03 12:30:02.20683	{}	SinkNodeRun	2285	69	\N	1
2293	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_p	sink_a_p	\N	2018-09-03 12:30:02.207346	2018-09-03 12:30:02.207351	{}	SinkNodeRun	2285	69	\N	1
2294	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_n	sink_m_n	\N	2018-09-03 12:30:02.208031	2018-09-03 12:30:02.208035	{}	SinkNodeRun	2285	69	\N	1
2295	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_n	sink_a_n	\N	2018-09-03 12:30:02.208623	2018-09-03 12:30:02.208627	{}	SinkNodeRun	2285	69	\N	1
2296	add_ints_2018-09-06T13-57-24/root	add_ints		2018-09-06 11:57:24.877638	2018-09-06 11:57:24.877647	{}	NetworkRun	\N	70	\N	0
2297	add_ints_2018-09-06T13-57-24/root/source	source	\N	2018-09-06 11:57:24.87951	2018-09-06 11:57:24.879515	{}	SourceNodeRun	2296	70	\N	1
2298	add_ints_2018-09-06T13-57-24/root/const_add_right_hand_0	const_add_right_hand_0	\N	2018-09-06 11:57:24.880424	2018-09-06 11:57:24.880428	{}	ConstantNodeRun	2296	70	\N	1
2299	add_ints_2018-09-06T13-57-24/root/sink	sink	\N	2018-09-06 11:57:24.881273	2018-09-06 11:57:24.881278	{}	SinkNodeRun	2296	70	\N	1
2300	add_ints_2018-09-06T13-57-24/root/add	add	\N	2018-09-06 11:57:24.881853	2018-09-06 11:57:24.881857	{}	NodeRun	2296	70	\N	1
2301	failing_network_2018-09-06T14-00-46/root	failing_network		2018-09-06 12:00:46.533264	2018-09-06 12:00:46.533271	{}	NetworkRun	\N	71	\N	0
2302	failing_network_2018-09-06T14-00-46/root/step_3	step_3	\N	2018-09-06 12:00:46.535077	2018-09-06 12:00:46.535081	{}	NodeRun	2301	71	\N	1
2303	failing_network_2018-09-06T14-00-46/root/const_step_2_fail_1_0	const_step_2_fail_1_0	\N	2018-09-06 12:00:46.535785	2018-09-06 12:00:46.535789	{}	ConstantNodeRun	2301	71	\N	1
2304	failing_network_2018-09-06T14-00-46/root/sink_4	sink_4	\N	2018-09-06 12:00:46.536429	2018-09-06 12:00:46.536434	{}	SinkNodeRun	2301	71	\N	1
2305	failing_network_2018-09-06T14-00-46/root/source_3	source_3	\N	2018-09-06 12:00:46.537039	2018-09-06 12:00:46.537044	{}	SourceNodeRun	2301	71	\N	1
2306	failing_network_2018-09-06T14-00-46/root/step_1	step_1	\N	2018-09-06 12:00:46.537576	2018-09-06 12:00:46.53758	{}	NodeRun	2301	71	\N	1
2307	failing_network_2018-09-06T14-00-46/root/const_step_1_fail_2_0	const_step_1_fail_2_0	\N	2018-09-06 12:00:46.538089	2018-09-06 12:00:46.538093	{}	ConstantNodeRun	2301	71	\N	1
2308	failing_network_2018-09-06T14-00-46/root/step_2	step_2	\N	2018-09-06 12:00:46.5386	2018-09-06 12:00:46.538604	{}	NodeRun	2301	71	\N	1
2309	failing_network_2018-09-06T14-00-46/root/sink_3	sink_3	\N	2018-09-06 12:00:46.539551	2018-09-06 12:00:46.539555	{}	SinkNodeRun	2301	71	\N	1
2310	failing_network_2018-09-06T14-00-46/root/range	range	\N	2018-09-06 12:00:46.540076	2018-09-06 12:00:46.54008	{}	NodeRun	2301	71	\N	1
2311	failing_network_2018-09-06T14-00-46/root/sum	sum	\N	2018-09-06 12:00:46.540628	2018-09-06 12:00:46.540632	{}	NodeRun	2301	71	\N	1
2312	failing_network_2018-09-06T14-00-46/root/sink_1	sink_1	\N	2018-09-06 12:00:46.541119	2018-09-06 12:00:46.541122	{}	SinkNodeRun	2301	71	\N	1
2313	failing_network_2018-09-06T14-00-46/root/source_2	source_2	\N	2018-09-06 12:00:46.541643	2018-09-06 12:00:46.541647	{}	SourceNodeRun	2301	71	\N	1
2314	failing_network_2018-09-06T14-00-46/root/sink_2	sink_2	\N	2018-09-06 12:00:46.54213	2018-09-06 12:00:46.542134	{}	SinkNodeRun	2301	71	\N	1
2315	failing_network_2018-09-06T14-00-46/root/sink_5	sink_5	\N	2018-09-06 12:00:46.542638	2018-09-06 12:00:46.542642	{}	SinkNodeRun	2301	71	\N	1
2316	failing_network_2018-09-06T14-00-46/root/source_1	source_1	\N	2018-09-06 12:00:46.543091	2018-09-06 12:00:46.543095	{}	SourceNodeRun	2301	71	\N	1
2317	IntracranialVolume_2018-09-06T15-00-57/root	IntracranialVolume		2018-09-06 13:00:59.077469	2018-09-06 13:00:59.077478	{}	NetworkRun	\N	72	\N	0
2318	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments	Program_Basic_Arguments	\N	2018-09-06 13:00:59.079661	2018-09-06 13:00:59.079666	{}	NetworkStep	2317	72	\N	1
2319	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Linear_Registration_Arguments	ICV_Program_Linear_Registration_Arguments	\N	2018-09-06 13:00:59.080528	2018-09-06 13:00:59.080532	{}	NetworkStep	2317	72	\N	1
2320	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments	Program_Linear_Registration_Arguments	\N	2018-09-06 13:00:59.081335	2018-09-06 13:00:59.081339	{}	NetworkStep	2317	72	\N	1
2321	IntracranialVolume_2018-09-06T15-00-57/root/Subject_Name	Subject_Name	\N	2018-09-06 13:00:59.082037	2018-09-06 13:00:59.082041	{}	NetworkStep	2317	72	\N	1
2322	IntracranialVolume_2018-09-06T15-00-57/root/Model_Name	Model_Name	\N	2018-09-06 13:00:59.082641	2018-09-06 13:00:59.082645	{}	NetworkStep	2317	72	\N	1
2323	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments	Program_Extended_Arguments	\N	2018-09-06 13:00:59.083207	2018-09-06 13:00:59.083211	{}	NetworkStep	2317	72	\N	1
2324	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs	ICV_Files_Outputs	\N	2018-09-06 13:00:59.083761	2018-09-06 13:00:59.083765	{}	NetworkStep	2317	72	\N	1
2325	IntracranialVolume_2018-09-06T15-00-57/root/Delete_Files	Delete_Files	\N	2018-09-06 13:00:59.084337	2018-09-06 13:00:59.084341	{}	NetworkStep	2317	72	\N	1
2326	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation	Main_Operation	\N	2018-09-06 13:00:59.08481	2018-09-06 13:00:59.084814	{}	NetworkStep	2317	72	\N	1
2327	IntracranialVolume_2018-09-06T15-00-57/root/Operation_Type	Operation_Type	\N	2018-09-06 13:00:59.085377	2018-09-06 13:00:59.085381	{}	NetworkStep	2317	72	\N	1
2328	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Extended_Arguments	ICV_Program_Extended_Arguments	\N	2018-09-06 13:00:59.086019	2018-09-06 13:00:59.086023	{}	NetworkStep	2317	72	\N	1
2329	IntracranialVolume_2018-09-06T15-00-57/root/Program_File_Arguments	Program_File_Arguments	\N	2018-09-06 13:00:59.086617	2018-09-06 13:00:59.086621	{}	NetworkStep	2317	72	\N	1
2330	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Num_Of_Iterations	Num_Of_Iterations	\N	2018-09-06 13:00:59.089266	2018-09-06 13:00:59.089271	{}	SourceNodeRun	2318	72	\N	2
2331	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Step_Size	Step_Size	\N	2018-09-06 13:00:59.089884	2018-09-06 13:00:59.089888	{}	SourceNodeRun	2318	72	\N	2
2332	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Blur_FWHM	Blur_FWHM	\N	2018-09-06 13:00:59.090537	2018-09-06 13:00:59.090541	{}	SourceNodeRun	2318	72	\N	2
2333	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Linear_Registration_Arguments/Linear_Normalization	Linear_Normalization	\N	2018-09-06 13:00:59.09112	2018-09-06 13:00:59.091124	{}	SourceNodeRun	2319	72	\N	2
2334	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Linear_Estimate	Linear_Estimate	\N	2018-09-06 13:00:59.091617	2018-09-06 13:00:59.091621	{}	SourceNodeRun	2320	72	\N	2
2335	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Num_Of_Registrations	Num_Of_Registrations	\N	2018-09-06 13:00:59.09218	2018-09-06 13:00:59.092184	{}	SourceNodeRun	2320	72	\N	2
2336	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Registration_Blur_FWHM	Registration_Blur_FWHM	\N	2018-09-06 13:00:59.092657	2018-09-06 13:00:59.092662	{}	SourceNodeRun	2320	72	\N	2
2337	IntracranialVolume_2018-09-06T15-00-57/root/Subject_Name/ICV_Subject_Name	ICV_Subject_Name	\N	2018-09-06 13:00:59.093399	2018-09-06 13:00:59.093403	{}	SourceNodeRun	2321	72	\N	2
2338	IntracranialVolume_2018-09-06T15-00-57/root/Model_Name/Model	Model	\N	2018-09-06 13:00:59.094165	2018-09-06 13:00:59.094169	{}	SourceNodeRun	2322	72	\N	2
2339	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Weight	Weight	\N	2018-09-06 13:00:59.094726	2018-09-06 13:00:59.09473	{}	SourceNodeRun	2323	72	\N	2
2340	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Stiffness	Stiffness	\N	2018-09-06 13:00:59.095277	2018-09-06 13:00:59.095281	{}	SourceNodeRun	2323	72	\N	2
2341	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Similarity	Similarity	\N	2018-09-06 13:00:59.095765	2018-09-06 13:00:59.095769	{}	SourceNodeRun	2323	72	\N	2
2342	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Sub_lattice	Sub_lattice	\N	2018-09-06 13:00:59.096335	2018-09-06 13:00:59.096339	{}	SourceNodeRun	2323	72	\N	2
2343	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Debug	Debug	\N	2018-09-06 13:00:59.09687	2018-09-06 13:00:59.096874	{}	SourceNodeRun	2323	72	\N	2
2344	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Clobber	Clobber	\N	2018-09-06 13:00:59.097421	2018-09-06 13:00:59.097425	{}	SourceNodeRun	2323	72	\N	2
2345	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Non_linear	Non_linear	\N	2018-09-06 13:00:59.098049	2018-09-06 13:00:59.098054	{}	SourceNodeRun	2323	72	\N	2
2346	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Mask_To_Binary_Min	Mask_To_Binary_Min	\N	2018-09-06 13:00:59.098638	2018-09-06 13:00:59.098642	{}	SourceNodeRun	2323	72	\N	2
2347	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Mask	ICV_Output_ICV_Mask	\N	2018-09-06 13:00:59.099201	2018-09-06 13:00:59.099216	{}	SinkNodeRun	2324	72	\N	2
2348	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Parameters	ICV_Output_ICV_Parameters	\N	2018-09-06 13:00:59.099721	2018-09-06 13:00:59.099724	{}	SinkNodeRun	2324	72	\N	2
2349	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Volume	ICV_Output_ICV_Volume	\N	2018-09-06 13:00:59.100355	2018-09-06 13:00:59.100359	{}	SinkNodeRun	2324	72	\N	2
2350	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_Log	ICV_Output_Log	\N	2018-09-06 13:00:59.100917	2018-09-06 13:00:59.100921	{}	SinkNodeRun	2324	72	\N	2
2351	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Original_Subject	ICV_Original_Subject	\N	2018-09-06 13:00:59.10151	2018-09-06 13:00:59.101513	{}	SinkNodeRun	2324	72	\N	2
2352	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_JPG	ICV_Output_ICV_JPG	\N	2018-09-06 13:00:59.102048	2018-09-06 13:00:59.102052	{}	SinkNodeRun	2324	72	\N	2
2353	IntracranialVolume_2018-09-06T15-00-57/root/Delete_Files/Delete_Files	Delete_Files	\N	2018-09-06 13:00:59.102549	2018-09-06 13:00:59.102552	{}	SourceNodeRun	2325	72	\N	2
2354	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume	IntracranialVolume	\N	2018-09-06 13:00:59.10318	2018-09-06 13:00:59.103185	{}	NodeRun	2326	72	\N	2
2355	IntracranialVolume_2018-09-06T15-00-57/root/Operation_Type/Calc_Crude	Calc_Crude	\N	2018-09-06 13:00:59.103732	2018-09-06 13:00:59.103736	{}	SourceNodeRun	2327	72	\N	2
2356	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Extended_Arguments/NonLinear_Normalization	NonLinear_Normalization	\N	2018-09-06 13:00:59.104316	2018-09-06 13:00:59.10432	{}	SourceNodeRun	2328	72	\N	2
2357	IntracranialVolume_2018-09-06T15-00-57/root/Program_File_Arguments/Input_To_Process	Input_To_Process	\N	2018-09-06 13:00:59.104784	2018-09-06 13:00:59.104788	{}	SourceNodeRun	2329	72	\N	2
2358	failing_network_2018-09-06T15-03-16/root	failing_network		2018-09-06 13:03:16.699903	2018-09-06 13:03:16.699915	{}	NetworkRun	\N	73	\N	0
2359	failing_network_2018-09-06T15-03-16/root/sink_4	sink_4	\N	2018-09-06 13:03:16.702112	2018-09-06 13:03:16.702118	{}	SinkNodeRun	2358	73	\N	1
2360	failing_network_2018-09-06T15-03-16/root/sink_2	sink_2	\N	2018-09-06 13:03:16.702877	2018-09-06 13:03:16.702883	{}	SinkNodeRun	2358	73	\N	1
2361	failing_network_2018-09-06T15-03-16/root/sum	sum	\N	2018-09-06 13:03:16.703617	2018-09-06 13:03:16.703636	{}	NodeRun	2358	73	\N	1
2362	failing_network_2018-09-06T15-03-16/root/const_step_1_fail_2_0	const_step_1_fail_2_0	\N	2018-09-06 13:03:16.704444	2018-09-06 13:03:16.70445	{}	ConstantNodeRun	2358	73	\N	1
2363	failing_network_2018-09-06T15-03-16/root/range	range	\N	2018-09-06 13:03:16.705446	2018-09-06 13:03:16.705454	{}	NodeRun	2358	73	\N	1
2364	failing_network_2018-09-06T15-03-16/root/source_1	source_1	\N	2018-09-06 13:03:16.706253	2018-09-06 13:03:16.706259	{}	SourceNodeRun	2358	73	\N	1
2365	failing_network_2018-09-06T15-03-16/root/const_step_2_fail_1_0	const_step_2_fail_1_0	\N	2018-09-06 13:03:16.706974	2018-09-06 13:03:16.706981	{}	ConstantNodeRun	2358	73	\N	1
2366	failing_network_2018-09-06T15-03-16/root/step_2	step_2	\N	2018-09-06 13:03:16.707689	2018-09-06 13:03:16.707696	{}	NodeRun	2358	73	\N	1
2367	failing_network_2018-09-06T15-03-16/root/source_3	source_3	\N	2018-09-06 13:03:16.708429	2018-09-06 13:03:16.708435	{}	SourceNodeRun	2358	73	\N	1
2368	failing_network_2018-09-06T15-03-16/root/sink_3	sink_3	\N	2018-09-06 13:03:16.709137	2018-09-06 13:03:16.709144	{}	SinkNodeRun	2358	73	\N	1
2369	failing_network_2018-09-06T15-03-16/root/sink_5	sink_5	\N	2018-09-06 13:03:16.709831	2018-09-06 13:03:16.709837	{}	SinkNodeRun	2358	73	\N	1
2370	failing_network_2018-09-06T15-03-16/root/step_1	step_1	\N	2018-09-06 13:03:16.710535	2018-09-06 13:03:16.710542	{}	NodeRun	2358	73	\N	1
2371	failing_network_2018-09-06T15-03-16/root/step_3	step_3	\N	2018-09-06 13:03:16.711265	2018-09-06 13:03:16.711272	{}	NodeRun	2358	73	\N	1
2372	failing_network_2018-09-06T15-03-16/root/source_2	source_2	\N	2018-09-06 13:03:16.711874	2018-09-06 13:03:16.711881	{}	SourceNodeRun	2358	73	\N	1
2373	failing_network_2018-09-06T15-03-16/root/sink_1	sink_1	\N	2018-09-06 13:03:16.712599	2018-09-06 13:03:16.712605	{}	SinkNodeRun	2358	73	\N	1
2374	add_ints_2018-09-07T12-00-02/root	add_ints		2018-09-07 10:00:02.661367	2018-09-07 10:00:02.661375	{}	NetworkRun	\N	74	\N	0
2375	add_ints_2018-09-07T12-00-02/root/const_add_right_hand_0	const_add_right_hand_0	\N	2018-09-07 10:00:02.662623	2018-09-07 10:00:02.662628	{}	ConstantNodeRun	2374	74	\N	1
2376	add_ints_2018-09-07T12-00-02/root/source	source	\N	2018-09-07 10:00:02.663422	2018-09-07 10:00:02.663426	{}	SourceNodeRun	2374	74	\N	1
2377	add_ints_2018-09-07T12-00-02/root/add	add	\N	2018-09-07 10:00:02.664224	2018-09-07 10:00:02.664229	{}	NodeRun	2374	74	\N	1
2378	add_ints_2018-09-07T12-00-02/root/sink	sink	\N	2018-09-07 10:00:02.664919	2018-09-07 10:00:02.664923	{}	SinkNodeRun	2374	74	\N	1
2379	iris/root	root		2018-09-08 13:18:12.738494	2018-09-08 13:18:12.738507	{}	analyses_screams	\N	75	\N	0
2380	iris/root/0_statistics	0_statistics	Leaf	2018-09-08 13:18:12.741029	2018-09-08 13:18:12.741033	{}	concerns_halls	2379	75	\N	1
2381	iris/root/1_dicom_to_nifti	1_dicom_to_nifti	Leaf	2018-09-08 13:18:12.741896	2018-09-08 13:18:12.741901	{}	glows_yell	2379	75	\N	1
2382	iris/root/2_hammers_atlas	2_hammers_atlas	Leaf	2018-09-08 13:18:12.742562	2018-09-08 13:18:12.742566	{}	waste_alibis	2379	75	\N	1
2383	iris/root/3_subjects	3_subjects	Leaf	2018-09-08 13:18:12.743227	2018-09-08 13:18:12.743233	{}	harness_alley	2379	75	\N	1
2384	iris/root/4_output	4_output	Leaf	2018-09-08 13:18:12.743953	2018-09-08 13:18:12.743957	{}	barges_records	2379	75	\N	1
2385	iris/root/5_BET_and_REG	5_BET_and_REG	Leaf	2018-09-08 13:18:12.744605	2018-09-08 13:18:12.74461	{}	rigs_insurance	2379	75	\N	1
2386	iris/root/6_qc_overview	6_qc_overview	Leaf	2018-09-08 13:18:12.745295	2018-09-08 13:18:12.745299	{}	rap_atmosphere	2379	75	\N	1
2387	iris/root/65_spm_tissue_quantification	65_spm_tissue_quantification	Leaf	2018-09-08 13:18:12.745986	2018-09-08 13:18:12.74599	{}	flame_catcher	2379	75	\N	1
2388	iris/root/0_statistics/9_const_get_gm_volume_flatten_0	9_const_get_gm_volume_flatten_0		2018-09-08 13:18:12.750698	2018-09-08 13:18:12.750704	{}	constant	2380	75	\N	2
2389	iris/root/0_statistics/11_get_gm_volume	11_get_gm_volume		2018-09-08 13:18:12.751521	2018-09-08 13:18:12.751526	{}	node	2380	75	\N	2
2390	iris/root/0_statistics/14_get_brain_volume	14_get_brain_volume		2018-09-08 13:18:12.752144	2018-09-08 13:18:12.752149	{}	node	2380	75	\N	2
2391	iris/root/0_statistics/18_const_get_wm_volume_flatten_0	18_const_get_wm_volume_flatten_0		2018-09-08 13:18:12.752838	2018-09-08 13:18:12.752842	{}	constant	2380	75	\N	2
2392	iris/root/0_statistics/19_mask_hammers_labels	19_mask_hammers_labels		2018-09-08 13:18:12.753428	2018-09-08 13:18:12.753432	{}	node	2380	75	\N	2
2393	iris/root/0_statistics/25_wm_volume	25_wm_volume		2018-09-08 13:18:12.754002	2018-09-08 13:18:12.754006	{}	sink	2380	75	\N	2
2394	iris/root/0_statistics/26_const_get_wm_volume_volume_0	26_const_get_wm_volume_volume_0		2018-09-08 13:18:12.754569	2018-09-08 13:18:12.754573	{}	constant	2380	75	\N	2
2395	iris/root/0_statistics/39_const_get_gm_volume_volume_0	39_const_get_gm_volume_volume_0		2018-09-08 13:18:12.755212	2018-09-08 13:18:12.755218	{}	constant	2380	75	\N	2
2396	iris/root/0_statistics/41_const_mask_hammers_labels_operator_0	41_const_mask_hammers_labels_operator_0		2018-09-08 13:18:12.755956	2018-09-08 13:18:12.75596	{}	constant	2380	75	\N	2
2397	iris/root/0_statistics/45_const_get_wm_volume_selection_0	45_const_get_wm_volume_selection_0		2018-09-08 13:18:12.756547	2018-09-08 13:18:12.756551	{}	constant	2380	75	\N	2
2398	iris/root/0_statistics/48_const_get_brain_volume_flatten_0	48_const_get_brain_volume_flatten_0		2018-09-08 13:18:12.757313	2018-09-08 13:18:12.757319	{}	constant	2380	75	\N	2
2399	iris/root/0_statistics/52_const_get_brain_volume_selection_0	52_const_get_brain_volume_selection_0		2018-09-08 13:18:12.7581	2018-09-08 13:18:12.758108	{}	constant	2380	75	\N	2
2400	iris/root/0_statistics/55_brain_volume	55_brain_volume		2018-09-08 13:18:12.758942	2018-09-08 13:18:12.758949	{}	sink	2380	75	\N	2
2401	iris/root/0_statistics/60_const_get_brain_volume_volume_0	60_const_get_brain_volume_volume_0		2018-09-08 13:18:12.759573	2018-09-08 13:18:12.759578	{}	constant	2380	75	\N	2
2402	iris/root/0_statistics/126_get_wm_volume	126_get_wm_volume		2018-09-08 13:18:12.760157	2018-09-08 13:18:12.760161	{}	node	2380	75	\N	2
2403	iris/root/0_statistics/128_gm_volume	128_gm_volume		2018-09-08 13:18:12.76088	2018-09-08 13:18:12.760887	{}	sink	2380	75	\N	2
2404	iris/root/0_statistics/129_const_get_gm_volume_selection_0	129_const_get_gm_volume_selection_0		2018-09-08 13:18:12.761388	2018-09-08 13:18:12.761393	{}	constant	2380	75	\N	2
2405	iris/root/1_dicom_to_nifti/20_t1_dicom_to_nifti	20_t1_dicom_to_nifti		2018-09-08 13:18:12.762016	2018-09-08 13:18:12.76202	{}	node	2381	75	\N	2
2406	iris/root/1_dicom_to_nifti/44_reformat_t1	44_reformat_t1		2018-09-08 13:18:12.762699	2018-09-08 13:18:12.762703	{}	node	2381	75	\N	2
2407	iris/root/1_dicom_to_nifti/61_const_t1_dicom_to_nifti_file_order_0	61_const_t1_dicom_to_nifti_file_order_0		2018-09-08 13:18:12.763267	2018-09-08 13:18:12.763271	{}	constant	2381	75	\N	2
2408	iris/root/2_hammers_atlas/8_hammers_t1w	8_hammers_t1w		2018-09-08 13:18:12.763868	2018-09-08 13:18:12.763874	{}	source	2382	75	\N	2
2409	iris/root/2_hammers_atlas/36_hammers_labels	36_hammers_labels		2018-09-08 13:18:12.764388	2018-09-08 13:18:12.764394	{}	source	2382	75	\N	2
2410	iris/root/2_hammers_atlas/50_hammers_brains	50_hammers_brains		2018-09-08 13:18:12.764996	2018-09-08 13:18:12.765	{}	source	2382	75	\N	2
2411	iris/root/2_hammers_atlas/64_hammers_brains_r5	64_hammers_brains_r5		2018-09-08 13:18:12.765525	2018-09-08 13:18:12.765529	{}	source	2382	75	\N	2
2412	iris/root/3_subjects/34_t1w_dicom	34_t1w_dicom		2018-09-08 13:18:12.766115	2018-09-08 13:18:12.766119	{}	source	2383	75	\N	2
2413	iris/root/4_output/16_wm_map_output	16_wm_map_output		2018-09-08 13:18:12.766691	2018-09-08 13:18:12.766705	{}	sink	2384	75	\N	2
2414	iris/root/4_output/23_multi_atlas_segm	23_multi_atlas_segm		2018-09-08 13:18:12.767284	2018-09-08 13:18:12.767288	{}	sink	2384	75	\N	2
2415	iris/root/4_output/29_t1_nifti_images	29_t1_nifti_images		2018-09-08 13:18:12.767838	2018-09-08 13:18:12.767842	{}	sink	2384	75	\N	2
2416	iris/root/4_output/40_gm_map_output	40_gm_map_output		2018-09-08 13:18:12.768373	2018-09-08 13:18:12.768392	{}	sink	2384	75	\N	2
2417	iris/root/4_output/57_hammer_brain_mask	57_hammer_brain_mask		2018-09-08 13:18:12.769197	2018-09-08 13:18:12.769201	{}	sink	2384	75	\N	2
2418	iris/root/5_BET_and_REG/7_threshold_labels	7_threshold_labels		2018-09-08 13:18:12.769922	2018-09-08 13:18:12.769926	{}	node	2385	75	\N	2
2419	iris/root/5_BET_and_REG/10_const_bet_fraction_threshold_0	10_const_bet_fraction_threshold_0		2018-09-08 13:18:12.770422	2018-09-08 13:18:12.770427	{}	constant	2385	75	\N	2
2420	iris/root/5_BET_and_REG/12_t1w_registration	12_t1w_registration		2018-09-08 13:18:12.771009	2018-09-08 13:18:12.771013	{}	node	2385	75	\N	2
2421	iris/root/5_BET_and_REG/13_const_threshold_labels_upper_threshold_0	13_const_threshold_labels_upper_threshold_0		2018-09-08 13:18:12.77172	2018-09-08 13:18:12.771724	{}	constant	2385	75	\N	2
2422	iris/root/5_BET_and_REG/15_const_bet_bias_cleanup_0	15_const_bet_bias_cleanup_0		2018-09-08 13:18:12.772269	2018-09-08 13:18:12.772274	{}	constant	2385	75	\N	2
2423	iris/root/5_BET_and_REG/17_hammers_brain_transform	17_hammers_brain_transform		2018-09-08 13:18:12.772897	2018-09-08 13:18:12.772901	{}	node	2385	75	\N	2
2424	iris/root/5_BET_and_REG/22_const_brain_mask_registation_parameters_0	22_const_brain_mask_registation_parameters_0		2018-09-08 13:18:12.773406	2018-09-08 13:18:12.77341	{}	constant	2385	75	\N	2
2425	iris/root/5_BET_and_REG/24_const_combine_labels_method_0	24_const_combine_labels_method_0		2018-09-08 13:18:12.77399	2018-09-08 13:18:12.773994	{}	constant	2385	75	\N	2
2426	iris/root/5_BET_and_REG/27_const_hammers_brain_transform_threads_0	27_const_hammers_brain_transform_threads_0		2018-09-08 13:18:12.774497	2018-09-08 13:18:12.774501	{}	constant	2385	75	\N	2
2427	iris/root/5_BET_and_REG/28_n4_bias_correction	28_n4_bias_correction		2018-09-08 13:18:12.775035	2018-09-08 13:18:12.775039	{}	node	2385	75	\N	2
2428	iris/root/5_BET_and_REG/30_combine_labels	30_combine_labels		2018-09-08 13:18:12.775661	2018-09-08 13:18:12.775665	{}	node	2385	75	\N	2
2429	iris/root/5_BET_and_REG/32_const_dilate_brainmask_operation_type_0	32_const_dilate_brainmask_operation_type_0		2018-09-08 13:18:12.77628	2018-09-08 13:18:12.776285	{}	constant	2385	75	\N	2
2430	iris/root/5_BET_and_REG/33_label_registration	33_label_registration		2018-09-08 13:18:12.776921	2018-09-08 13:18:12.776925	{}	node	2385	75	\N	2
2431	iris/root/5_BET_and_REG/35_const_dilate_brainmask_radius_0	35_const_dilate_brainmask_radius_0		2018-09-08 13:18:12.777485	2018-09-08 13:18:12.777489	{}	constant	2385	75	\N	2
2432	iris/root/5_BET_and_REG/38_threshold_brain	38_threshold_brain		2018-09-08 13:18:12.778012	2018-09-08 13:18:12.778016	{}	node	2385	75	\N	2
2433	iris/root/5_BET_and_REG/43_convert_brain	43_convert_brain		2018-09-08 13:18:12.778662	2018-09-08 13:18:12.778666	{}	node	2385	75	\N	2
2434	iris/root/5_BET_and_REG/47_const_convert_brain_component_type_0	47_const_convert_brain_component_type_0		2018-09-08 13:18:12.779325	2018-09-08 13:18:12.779343	{}	constant	2385	75	\N	2
2435	iris/root/5_BET_and_REG/49_const_t1w_registration_parameters_0	49_const_t1w_registration_parameters_0		2018-09-08 13:18:12.779938	2018-09-08 13:18:12.779942	{}	constant	2385	75	\N	2
2436	iris/root/5_BET_and_REG/51_const_combine_brains_number_of_classes_0	51_const_combine_brains_number_of_classes_0		2018-09-08 13:18:12.780499	2018-09-08 13:18:12.780503	{}	constant	2385	75	\N	2
2437	iris/root/5_BET_and_REG/53_const_label_registration_threads_0	53_const_label_registration_threads_0		2018-09-08 13:18:12.781021	2018-09-08 13:18:12.781025	{}	constant	2385	75	\N	2
2438	iris/root/5_BET_and_REG/56_brain_mask_registation	56_brain_mask_registation		2018-09-08 13:18:12.781763	2018-09-08 13:18:12.781767	{}	node	2385	75	\N	2
2439	iris/root/5_BET_and_REG/58_const_combine_brains_method_0	58_const_combine_brains_method_0		2018-09-08 13:18:12.782343	2018-09-08 13:18:12.782347	{}	constant	2385	75	\N	2
2440	iris/root/5_BET_and_REG/59_const_t1w_registration_threads_0	59_const_t1w_registration_threads_0		2018-09-08 13:18:12.783075	2018-09-08 13:18:12.783079	{}	constant	2385	75	\N	2
2441	iris/root/5_BET_and_REG/63_const_brain_mask_registation_threads_0	63_const_brain_mask_registation_threads_0		2018-09-08 13:18:12.783807	2018-09-08 13:18:12.783811	{}	constant	2385	75	\N	2
2442	iris/root/5_BET_and_REG/121_combine_brains	121_combine_brains		2018-09-08 13:18:12.784364	2018-09-08 13:18:12.784368	{}	node	2385	75	\N	2
2443	iris/root/5_BET_and_REG/124_const_combine_labels_number_of_classes_0	124_const_combine_labels_number_of_classes_0		2018-09-08 13:18:12.784948	2018-09-08 13:18:12.784952	{}	constant	2385	75	\N	2
2444	iris/root/5_BET_and_REG/125_dilate_brainmask	125_dilate_brainmask		2018-09-08 13:18:12.785439	2018-09-08 13:18:12.785443	{}	node	2385	75	\N	2
2445	iris/root/5_BET_and_REG/130_bet	130_bet		2018-09-08 13:18:12.786008	2018-09-08 13:18:12.786012	{}	node	2385	75	\N	2
2446	iris/root/5_BET_and_REG/131_const_dilate_brainmask_operation_0	131_const_dilate_brainmask_operation_0		2018-09-08 13:18:12.786655	2018-09-08 13:18:12.786659	{}	constant	2385	75	\N	2
2447	iris/root/6_qc_overview/21_t1_with_hammers	21_t1_with_hammers		2018-09-08 13:18:12.787232	2018-09-08 13:18:12.787236	{}	node	2386	75	\N	2
2448	iris/root/6_qc_overview/31_t1_with_hammers_sink	31_t1_with_hammers_sink		2018-09-08 13:18:12.787838	2018-09-08 13:18:12.787842	{}	sink	2386	75	\N	2
2449	iris/root/6_qc_overview/37_t1_with_wm_outline_sink	37_t1_with_wm_outline_sink		2018-09-08 13:18:12.788353	2018-09-08 13:18:12.788357	{}	sink	2386	75	\N	2
2450	iris/root/6_qc_overview/42_t1_with_gm_outline	42_t1_with_gm_outline		2018-09-08 13:18:12.788824	2018-09-08 13:18:12.788828	{}	node	2386	75	\N	2
2451	iris/root/6_qc_overview/46_t1_qc	46_t1_qc		2018-09-08 13:18:12.789327	2018-09-08 13:18:12.789331	{}	node	2386	75	\N	2
2452	iris/root/6_qc_overview/54_t1_qc_sink	54_t1_qc_sink		2018-09-08 13:18:12.789876	2018-09-08 13:18:12.78988	{}	sink	2386	75	\N	2
2453	iris/root/6_qc_overview/62_t1_with_gm_outline_sink	62_t1_with_gm_outline_sink		2018-09-08 13:18:12.790345	2018-09-08 13:18:12.790349	{}	sink	2386	75	\N	2
2454	iris/root/6_qc_overview/122_t1_with_brain	122_t1_with_brain		2018-09-08 13:18:12.790938	2018-09-08 13:18:12.790942	{}	node	2386	75	\N	2
2455	iris/root/6_qc_overview/123_t1_with_wm_outline	123_t1_with_wm_outline		2018-09-08 13:18:12.791395	2018-09-08 13:18:12.791399	{}	node	2386	75	\N	2
2456	iris/root/6_qc_overview/127_t1_with_brain_sink	127_t1_with_brain_sink		2018-09-08 13:18:12.791931	2018-09-08 13:18:12.791935	{}	sink	2386	75	\N	2
2457	iris/root/65_spm_tissue_quantification/66_InputImages	66_InputImages	Leaf	2018-09-08 13:18:12.792399	2018-09-08 13:18:12.792403	{}	blazes_rollers	2387	75	\N	2
2458	iris/root/65_spm_tissue_quantification/67_InputImage	67_InputImage	Leaf	2018-09-08 13:18:12.792957	2018-09-08 13:18:12.792962	{}	nonavailability_deletions	2387	75	\N	2
2459	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation	68_SPM_Tissue_Segmentation	Leaf	2018-09-08 13:18:12.793423	2018-09-08 13:18:12.793427	{}	armies_shirt	2387	75	\N	2
2460	iris/root/65_spm_tissue_quantification/69_outputs	69_outputs	Leaf	2018-09-08 13:18:12.793978	2018-09-08 13:18:12.793982	{}	chaplain_painter	2387	75	\N	2
2461	iris/root/65_spm_tissue_quantification/66_InputImages/81_T1_images	81_T1_images		2018-09-08 13:18:12.802193	2018-09-08 13:18:12.802211	{}	source	2457	75	\N	3
2462	iris/root/65_spm_tissue_quantification/67_InputImage/75_t1_passthrough	75_t1_passthrough		2018-09-08 13:18:12.803117	2018-09-08 13:18:12.803121	{}	node	2458	75	\N	3
2463	iris/root/65_spm_tissue_quantification/67_InputImage/77_const_t1_passthrough_operator_0	77_const_t1_passthrough_operator_0		2018-09-08 13:18:12.803723	2018-09-08 13:18:12.803727	{}	constant	2458	75	\N	3
2464	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation	70_spm_segmantation		2018-09-08 13:18:12.804269	2018-09-08 13:18:12.804273	{}	node	2459	75	\N	3
2465	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/71_const_edit_transform_file_set_0	71_const_edit_transform_file_set_0		2018-09-08 13:18:12.804968	2018-09-08 13:18:12.804972	{}	constant	2459	75	\N	3
2466	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/74_const_invert_gm_parameters_0	74_const_invert_gm_parameters_0		2018-09-08 13:18:12.805511	2018-09-08 13:18:12.80552	{}	constant	2459	75	\N	3
2467	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate	76_reg_t1_to_spmtemplate		2018-09-08 13:18:12.806058	2018-09-08 13:18:12.806062	{}	node	2459	75	\N	3
2468	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/78_const_reg_t1_to_spmtemplate_parameters_0	78_const_reg_t1_to_spmtemplate_parameters_0		2018-09-08 13:18:12.806622	2018-09-08 13:18:12.806626	{}	constant	2459	75	\N	3
2469	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf	79_transform_csf		2018-09-08 13:18:12.807308	2018-09-08 13:18:12.807312	{}	node	2459	75	\N	3
2470	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm	82_invert_gm		2018-09-08 13:18:12.807839	2018-09-08 13:18:12.807843	{}	node	2459	75	\N	3
2471	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment	83_spm_hard_segment		2018-09-08 13:18:12.808338	2018-09-08 13:18:12.808343	{}	node	2459	75	\N	3
2472	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/85_const_reg_t1_to_spmtemplate_fixed_image_0	85_const_reg_t1_to_spmtemplate_fixed_image_0		2018-09-08 13:18:12.808877	2018-09-08 13:18:12.808887	{}	constant	2459	75	\N	3
2473	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/86_edit_transform_file	86_edit_transform_file		2018-09-08 13:18:12.809426	2018-09-08 13:18:12.80943	{}	node	2459	75	\N	3
2474	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm	87_transform_gm		2018-09-08 13:18:12.809992	2018-09-08 13:18:12.809996	{}	node	2459	75	\N	3
2475	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm	89_transform_wm		2018-09-08 13:18:12.810507	2018-09-08 13:18:12.810511	{}	node	2459	75	\N	3
2476	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1	91_transform_t1		2018-09-08 13:18:12.811078	2018-09-08 13:18:12.811082	{}	node	2459	75	\N	3
2477	iris/root/65_spm_tissue_quantification/69_outputs/72_csf_map	72_csf_map		2018-09-08 13:18:12.811766	2018-09-08 13:18:12.81177	{}	sink	2460	75	\N	3
2478	iris/root/65_spm_tissue_quantification/69_outputs/73_wm_map	73_wm_map		2018-09-08 13:18:12.812309	2018-09-08 13:18:12.812313	{}	sink	2460	75	\N	3
2479	iris/root/65_spm_tissue_quantification/69_outputs/80_gm_map	80_gm_map		2018-09-08 13:18:12.812822	2018-09-08 13:18:12.812826	{}	sink	2460	75	\N	3
2480	iris/root/65_spm_tissue_quantification/69_outputs/84_csf_spm	84_csf_spm		2018-09-08 13:18:12.813336	2018-09-08 13:18:12.81334	{}	sink	2460	75	\N	3
2481	iris/root/65_spm_tissue_quantification/69_outputs/88_gm_spm	88_gm_spm		2018-09-08 13:18:12.813893	2018-09-08 13:18:12.813897	{}	sink	2460	75	\N	3
2482	iris/root/65_spm_tissue_quantification/69_outputs/90_wm_spm	90_wm_spm		2018-09-08 13:18:12.814386	2018-09-08 13:18:12.81439	{}	sink	2460	75	\N	3
2483	add_ints_2018-09-13T15-55-06/root	add_ints		2018-09-13 13:55:07.001014	2018-09-13 13:55:07.001022	{}	NetworkRun	\N	76	\N	0
2484	add_ints_2018-09-13T15-55-06/root/source	source	\N	2018-09-13 13:55:07.002675	2018-09-13 13:55:07.002679	{}	SourceNodeRun	2483	76	\N	1
2485	add_ints_2018-09-13T15-55-06/root/add	add	\N	2018-09-13 13:55:07.003437	2018-09-13 13:55:07.003441	{}	NodeRun	2483	76	\N	1
2486	add_ints_2018-09-13T15-55-06/root/const_add_right_hand_0	const_add_right_hand_0	\N	2018-09-13 13:55:07.004086	2018-09-13 13:55:07.00409	{}	ConstantNodeRun	2483	76	\N	1
2487	add_ints_2018-09-13T15-55-06/root/sink	sink	\N	2018-09-13 13:55:07.004795	2018-09-13 13:55:07.0048	{}	SinkNodeRun	2483	76	\N	1
\.


--
-- Name: nodes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.nodes_id_seq', 2487, true);


--
-- Data for Name: out_ports; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.out_ports (id, path, title, description, created, modified, custom_data, node_id) FROM stdin;
4855	add_ints_2018-09-13T15-55-06/root/source/output	output	\N	2018-09-13 13:55:07.01617	2018-09-13 13:55:07.016178	{"datatype": "Int", "dimension_names": ["source"]}	2484
4856	add_ints_2018-09-13T15-55-06/root/source/v_out_port	•••	Virtual output port	2018-09-13 13:55:07.017479	2018-09-13 13:55:07.017484	null	2484
4857	add_ints_2018-09-13T15-55-06/root/add/result	result	\N	2018-09-13 13:55:07.018053	2018-09-13 13:55:07.018057	{"datatype": "Int", "dimension_names": ["source"]}	2485
4858	add_ints_2018-09-13T15-55-06/root/add/v_out_port	•••	Virtual output port	2018-09-13 13:55:07.018628	2018-09-13 13:55:07.018632	null	2485
4859	add_ints_2018-09-13T15-55-06/root/const_add_right_hand_0/output	output	\N	2018-09-13 13:55:07.01912	2018-09-13 13:55:07.019124	{"datatype": "Int", "dimension_names": ["const_add_right_hand_0"]}	2486
4860	add_ints_2018-09-13T15-55-06/root/const_add_right_hand_0/v_out_port	•••	Virtual output port	2018-09-13 13:55:07.019646	2018-09-13 13:55:07.01965	null	2486
4861	add_ints_2018-09-13T15-55-06/root/sink/output	output	\N	2018-09-13 13:55:07.020183	2018-09-13 13:55:07.020187	{"datatype": "Int", "dimension_names": []}	2487
4862	add_ints_2018-09-13T15-55-06/root/sink/v_out_port	•••	Virtual output port	2018-09-13 13:55:07.020695	2018-09-13 13:55:07.020698	null	2487
4171	example_2018-09-03T10-32-43/root/source1/output	output	\N	2018-09-03 08:32:43.87907	2018-09-03 08:32:43.879076	{"datatype": "Int", "dimension_names": ["source1"]}	2125
4172	example_2018-09-03T10-32-43/root/source1/v_out_port	•••	Virtual output port	2018-09-03 08:32:43.880225	2018-09-03 08:32:43.880229	null	2125
4173	example_2018-09-03T10-32-43/root/sink1/output	output	\N	2018-09-03 08:32:43.880826	2018-09-03 08:32:43.880829	{"datatype": "Int", "dimension_names": []}	2126
4174	example_2018-09-03T10-32-43/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 08:32:43.881387	2018-09-03 08:32:43.881391	null	2126
4175	example_2018-09-03T10-32-43/root/addint/result	result	\N	2018-09-03 08:32:43.881848	2018-09-03 08:32:43.881852	{"datatype": "Int", "dimension_names": ["source1"]}	2127
4176	example_2018-09-03T10-32-43/root/addint/v_out_port	•••	Virtual output port	2018-09-03 08:32:43.882291	2018-09-03 08:32:43.882295	null	2127
4177	example_2018-09-03T10-32-43/root/const_addint_right_hand_0/output	output	\N	2018-09-03 08:32:43.882719	2018-09-03 08:32:43.882723	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2128
4178	example_2018-09-03T10-32-43/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 08:32:43.88312	2018-09-03 08:32:43.883123	null	2128
4179	example_2018-09-03T10-36-42/root/source1/output	output	\N	2018-09-03 08:36:43.183059	2018-09-03 08:36:43.183064	{"datatype": "Int", "dimension_names": ["source1"]}	2130
4180	example_2018-09-03T10-36-42/root/source1/v_out_port	•••	Virtual output port	2018-09-03 08:36:43.183614	2018-09-03 08:36:43.183618	null	2130
4181	example_2018-09-03T10-36-42/root/sink1/output	output	\N	2018-09-03 08:36:43.184185	2018-09-03 08:36:43.184189	{"datatype": "Int", "dimension_names": []}	2131
4182	example_2018-09-03T10-36-42/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 08:36:43.184686	2018-09-03 08:36:43.18469	null	2131
4183	example_2018-09-03T10-36-42/root/addint/result	result	\N	2018-09-03 08:36:43.18519	2018-09-03 08:36:43.185193	{"datatype": "Int", "dimension_names": ["source1"]}	2132
4184	example_2018-09-03T10-36-42/root/addint/v_out_port	•••	Virtual output port	2018-09-03 08:36:43.18569	2018-09-03 08:36:43.185694	null	2132
4185	example_2018-09-03T10-36-42/root/const_addint_right_hand_0/output	output	\N	2018-09-03 08:36:43.186202	2018-09-03 08:36:43.186206	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2133
4186	example_2018-09-03T10-36-42/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 08:36:43.186672	2018-09-03 08:36:43.186675	null	2133
4187	example_2018-09-03T10-37-58/root/source1/output	output	\N	2018-09-03 08:37:59.16124	2018-09-03 08:37:59.161245	{"datatype": "Int", "dimension_names": ["source1"]}	2135
4188	example_2018-09-03T10-37-58/root/source1/v_out_port	•••	Virtual output port	2018-09-03 08:37:59.161729	2018-09-03 08:37:59.161733	null	2135
4189	example_2018-09-03T10-37-58/root/sink1/output	output	\N	2018-09-03 08:37:59.162123	2018-09-03 08:37:59.162127	{"datatype": "Int", "dimension_names": []}	2136
4190	example_2018-09-03T10-37-58/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 08:37:59.162555	2018-09-03 08:37:59.162559	null	2136
4191	example_2018-09-03T10-37-58/root/addint/result	result	\N	2018-09-03 08:37:59.163193	2018-09-03 08:37:59.163196	{"datatype": "Int", "dimension_names": ["source1"]}	2137
4192	example_2018-09-03T10-37-58/root/addint/v_out_port	•••	Virtual output port	2018-09-03 08:37:59.163663	2018-09-03 08:37:59.163666	null	2137
4193	example_2018-09-03T10-37-58/root/const_addint_right_hand_0/output	output	\N	2018-09-03 08:37:59.164142	2018-09-03 08:37:59.164146	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2138
4194	example_2018-09-03T10-37-58/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 08:37:59.164592	2018-09-03 08:37:59.164596	null	2138
4195	example_2018-09-03T10-41-42/root/source1/output	output	\N	2018-09-03 08:41:42.771353	2018-09-03 08:41:42.771376	{"datatype": "Int", "dimension_names": ["source1"]}	2140
4196	example_2018-09-03T10-41-42/root/source1/v_out_port	•••	Virtual output port	2018-09-03 08:41:42.771907	2018-09-03 08:41:42.771911	null	2140
4197	example_2018-09-03T10-41-42/root/sink1/output	output	\N	2018-09-03 08:41:42.772354	2018-09-03 08:41:42.772375	{"datatype": "Int", "dimension_names": []}	2141
4198	example_2018-09-03T10-41-42/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 08:41:42.77281	2018-09-03 08:41:42.772813	null	2141
4199	example_2018-09-03T10-41-42/root/addint/result	result	\N	2018-09-03 08:41:42.773225	2018-09-03 08:41:42.773229	{"datatype": "Int", "dimension_names": ["source1"]}	2142
4200	example_2018-09-03T10-41-42/root/addint/v_out_port	•••	Virtual output port	2018-09-03 08:41:42.773684	2018-09-03 08:41:42.773687	null	2142
4201	example_2018-09-03T10-41-42/root/const_addint_right_hand_0/output	output	\N	2018-09-03 08:41:42.774205	2018-09-03 08:41:42.774209	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2143
4202	example_2018-09-03T10-41-42/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 08:41:42.774694	2018-09-03 08:41:42.774697	null	2143
4203	example_2018-09-03T10-47-39/root/source1/output	output	\N	2018-09-03 08:47:39.881695	2018-09-03 08:47:39.881699	{"datatype": "Int", "dimension_names": ["source1"]}	2145
4204	example_2018-09-03T10-47-39/root/source1/v_out_port	•••	Virtual output port	2018-09-03 08:47:39.882212	2018-09-03 08:47:39.882216	null	2145
4205	example_2018-09-03T10-47-39/root/sink1/output	output	\N	2018-09-03 08:47:39.88271	2018-09-03 08:47:39.882713	{"datatype": "Int", "dimension_names": []}	2146
4206	example_2018-09-03T10-47-39/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 08:47:39.883168	2018-09-03 08:47:39.883171	null	2146
4207	example_2018-09-03T10-47-39/root/addint/result	result	\N	2018-09-03 08:47:39.883611	2018-09-03 08:47:39.883615	{"datatype": "Int", "dimension_names": ["source1"]}	2147
4208	example_2018-09-03T10-47-39/root/addint/v_out_port	•••	Virtual output port	2018-09-03 08:47:39.884241	2018-09-03 08:47:39.884244	null	2147
4209	example_2018-09-03T10-47-39/root/const_addint_right_hand_0/output	output	\N	2018-09-03 08:47:39.884706	2018-09-03 08:47:39.88471	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2148
4210	example_2018-09-03T10-47-39/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 08:47:39.885169	2018-09-03 08:47:39.885172	null	2148
4211	example_2018-09-03T11-01-12/root/source1/output	output	\N	2018-09-03 09:01:12.307442	2018-09-03 09:01:12.307449	{"datatype": "Int", "dimension_names": ["source1"]}	2150
4212	example_2018-09-03T11-01-12/root/source1/v_out_port	•••	Virtual output port	2018-09-03 09:01:12.307913	2018-09-03 09:01:12.307916	null	2150
4213	example_2018-09-03T11-01-12/root/sink1/output	output	\N	2018-09-03 09:01:12.308315	2018-09-03 09:01:12.308319	{"datatype": "Int", "dimension_names": []}	2151
4214	example_2018-09-03T11-01-12/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 09:01:12.308745	2018-09-03 09:01:12.308749	null	2151
4215	example_2018-09-03T11-01-12/root/addint/result	result	\N	2018-09-03 09:01:12.309131	2018-09-03 09:01:12.309135	{"datatype": "Int", "dimension_names": ["source1"]}	2152
4216	example_2018-09-03T11-01-12/root/addint/v_out_port	•••	Virtual output port	2018-09-03 09:01:12.309619	2018-09-03 09:01:12.309623	null	2152
4217	example_2018-09-03T11-01-12/root/const_addint_right_hand_0/output	output	\N	2018-09-03 09:01:12.310168	2018-09-03 09:01:12.310172	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2153
4218	example_2018-09-03T11-01-12/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 09:01:12.310732	2018-09-03 09:01:12.310736	null	2153
4219	example_2018-09-03T11-07-53/root/source1/output	output	\N	2018-09-03 09:07:54.392758	2018-09-03 09:07:54.392764	{"datatype": "Int", "dimension_names": ["source1"]}	2155
4220	example_2018-09-03T11-07-53/root/source1/v_out_port	•••	Virtual output port	2018-09-03 09:07:54.39328	2018-09-03 09:07:54.393284	null	2155
4221	example_2018-09-03T11-07-53/root/sink1/output	output	\N	2018-09-03 09:07:54.393759	2018-09-03 09:07:54.393763	{"datatype": "Int", "dimension_names": []}	2156
4222	example_2018-09-03T11-07-53/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 09:07:54.394211	2018-09-03 09:07:54.394215	null	2156
4223	example_2018-09-03T11-07-53/root/addint/result	result	\N	2018-09-03 09:07:54.394649	2018-09-03 09:07:54.394653	{"datatype": "Int", "dimension_names": ["source1"]}	2157
4224	example_2018-09-03T11-07-53/root/addint/v_out_port	•••	Virtual output port	2018-09-03 09:07:54.395115	2018-09-03 09:07:54.395118	null	2157
4225	example_2018-09-03T11-07-53/root/const_addint_right_hand_0/output	output	\N	2018-09-03 09:07:54.395568	2018-09-03 09:07:54.395572	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2158
4226	example_2018-09-03T11-07-53/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 09:07:54.396037	2018-09-03 09:07:54.39604	null	2158
4227	example_2018-09-03T11-12-21/root/source1/output	output	\N	2018-09-03 09:12:21.580321	2018-09-03 09:12:21.580327	{"datatype": "Int", "dimension_names": ["source1"]}	2160
4228	example_2018-09-03T11-12-21/root/source1/v_out_port	•••	Virtual output port	2018-09-03 09:12:21.580858	2018-09-03 09:12:21.580862	null	2160
4229	example_2018-09-03T11-12-21/root/sink1/output	output	\N	2018-09-03 09:12:21.581409	2018-09-03 09:12:21.581413	{"datatype": "Int", "dimension_names": []}	2161
4230	example_2018-09-03T11-12-21/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 09:12:21.581848	2018-09-03 09:12:21.581852	null	2161
4231	example_2018-09-03T11-12-21/root/addint/result	result	\N	2018-09-03 09:12:21.582296	2018-09-03 09:12:21.5823	{"datatype": "Int", "dimension_names": ["source1"]}	2162
4232	example_2018-09-03T11-12-21/root/addint/v_out_port	•••	Virtual output port	2018-09-03 09:12:21.58283	2018-09-03 09:12:21.582834	null	2162
4233	example_2018-09-03T11-12-21/root/const_addint_right_hand_0/output	output	\N	2018-09-03 09:12:21.583274	2018-09-03 09:12:21.583278	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2163
4234	example_2018-09-03T11-12-21/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 09:12:21.583727	2018-09-03 09:12:21.583731	null	2163
4235	example_2018-09-03T11-13-16/root/source1/output	output	\N	2018-09-03 09:13:16.482366	2018-09-03 09:13:16.482371	{"datatype": "Int", "dimension_names": ["source1"]}	2165
4236	example_2018-09-03T11-13-16/root/source1/v_out_port	•••	Virtual output port	2018-09-03 09:13:16.482965	2018-09-03 09:13:16.482969	null	2165
4237	example_2018-09-03T11-13-16/root/sink1/output	output	\N	2018-09-03 09:13:16.483494	2018-09-03 09:13:16.483498	{"datatype": "Int", "dimension_names": []}	2166
4238	example_2018-09-03T11-13-16/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 09:13:16.483913	2018-09-03 09:13:16.483917	null	2166
4239	example_2018-09-03T11-13-16/root/addint/result	result	\N	2018-09-03 09:13:16.484341	2018-09-03 09:13:16.484345	{"datatype": "Int", "dimension_names": ["source1"]}	2167
4240	example_2018-09-03T11-13-16/root/addint/v_out_port	•••	Virtual output port	2018-09-03 09:13:16.484809	2018-09-03 09:13:16.484813	null	2167
4241	example_2018-09-03T11-13-16/root/const_addint_right_hand_0/output	output	\N	2018-09-03 09:13:16.485315	2018-09-03 09:13:16.485319	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2168
4242	example_2018-09-03T11-13-16/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 09:13:16.485952	2018-09-03 09:13:16.485956	null	2168
4243	example_2018-09-03T11-14-49/root/source1/output	output	\N	2018-09-03 09:14:49.300493	2018-09-03 09:14:49.300498	{"datatype": "Int", "dimension_names": ["source1"]}	2170
3870	failing_network_2018-08-22T11-29-01/root/step_1/out_1	out_1	\N	2018-08-22 09:29:02.243022	2018-08-22 09:29:02.243028	{"datatype": "Int", "dimension_names": ["source_1"]}	1969
3871	failing_network_2018-08-22T11-29-01/root/step_1/out_2	out_2	\N	2018-08-22 09:29:02.244714	2018-08-22 09:29:02.244718	{"datatype": "Int", "dimension_names": ["source_1"]}	1969
3872	failing_network_2018-08-22T11-29-01/root/step_1/v_out_port	•••	Virtual output port	2018-08-22 09:29:02.245396	2018-08-22 09:29:02.245401	null	1969
3873	failing_network_2018-08-22T11-29-01/root/step_3/out_1	out_1	\N	2018-08-22 09:29:02.246032	2018-08-22 09:29:02.246037	{"datatype": "Int", "dimension_names": ["source_1"]}	1970
3874	failing_network_2018-08-22T11-29-01/root/step_3/out_2	out_2	\N	2018-08-22 09:29:02.246572	2018-08-22 09:29:02.246577	{"datatype": "Int", "dimension_names": ["source_1"]}	1970
4244	example_2018-09-03T11-14-49/root/source1/v_out_port	•••	Virtual output port	2018-09-03 09:14:49.300975	2018-09-03 09:14:49.300979	null	2170
3875	failing_network_2018-08-22T11-29-01/root/step_3/v_out_port	•••	Virtual output port	2018-08-22 09:29:02.247082	2018-08-22 09:29:02.247086	null	1970
3876	failing_network_2018-08-22T11-29-01/root/step_2/out_1	out_1	\N	2018-08-22 09:29:02.247705	2018-08-22 09:29:02.247709	{"datatype": "Int", "dimension_names": ["source_3"]}	1971
3877	failing_network_2018-08-22T11-29-01/root/step_2/out_2	out_2	\N	2018-08-22 09:29:02.248345	2018-08-22 09:29:02.24835	{"datatype": "Int", "dimension_names": ["source_3"]}	1971
3878	failing_network_2018-08-22T11-29-01/root/step_2/v_out_port	•••	Virtual output port	2018-08-22 09:29:02.248896	2018-08-22 09:29:02.2489	null	1971
3879	failing_network_2018-08-22T11-29-01/root/sum/result	result	\N	2018-08-22 09:29:02.249454	2018-08-22 09:29:02.249458	{"datatype": "Int", "dimension_names": ["source_1"]}	1972
3880	failing_network_2018-08-22T11-29-01/root/sum/v_out_port	•••	Virtual output port	2018-08-22 09:29:02.249988	2018-08-22 09:29:02.249992	null	1972
3881	failing_network_2018-08-22T11-29-01/root/source_1/output	output	\N	2018-08-22 09:29:02.250524	2018-08-22 09:29:02.250528	{"datatype": "Int", "dimension_names": ["source_1"]}	1973
3882	failing_network_2018-08-22T11-29-01/root/source_1/v_out_port	•••	Virtual output port	2018-08-22 09:29:02.251061	2018-08-22 09:29:02.251065	null	1973
3883	failing_network_2018-08-22T11-29-01/root/sink_2/output	output	\N	2018-08-22 09:29:02.251685	2018-08-22 09:29:02.251689	null	1974
3884	failing_network_2018-08-22T11-29-01/root/sink_2/v_out_port	•••	Virtual output port	2018-08-22 09:29:02.252142	2018-08-22 09:29:02.252146	null	1974
3885	failing_network_2018-08-22T11-29-01/root/sink_3/output	output	\N	2018-08-22 09:29:02.252643	2018-08-22 09:29:02.252647	null	1975
3886	failing_network_2018-08-22T11-29-01/root/sink_3/v_out_port	•••	Virtual output port	2018-08-22 09:29:02.25311	2018-08-22 09:29:02.253114	null	1975
3887	failing_network_2018-08-22T11-29-01/root/range/result	result	\N	2018-08-22 09:29:02.25364	2018-08-22 09:29:02.253644	{"datatype": "Int", "dimension_names": ["source_1"]}	1976
3888	failing_network_2018-08-22T11-29-01/root/range/v_out_port	•••	Virtual output port	2018-08-22 09:29:02.254077	2018-08-22 09:29:02.254081	null	1976
3889	failing_network_2018-08-22T11-29-01/root/sink_1/output	output	\N	2018-08-22 09:29:02.254696	2018-08-22 09:29:02.254701	null	1977
3890	failing_network_2018-08-22T11-29-01/root/sink_1/v_out_port	•••	Virtual output port	2018-08-22 09:29:02.255187	2018-08-22 09:29:02.255191	null	1977
3891	failing_network_2018-08-22T11-29-01/root/source_2/output	output	\N	2018-08-22 09:29:02.255704	2018-08-22 09:29:02.255708	{"datatype": "Int", "dimension_names": ["source_2"]}	1978
3892	failing_network_2018-08-22T11-29-01/root/source_2/v_out_port	•••	Virtual output port	2018-08-22 09:29:02.256344	2018-08-22 09:29:02.25635	null	1978
3893	failing_network_2018-08-22T11-29-01/root/source_3/output	output	\N	2018-08-22 09:29:02.25689	2018-08-22 09:29:02.256894	{"datatype": "Int", "dimension_names": ["source_3"]}	1979
3894	failing_network_2018-08-22T11-29-01/root/source_3/v_out_port	•••	Virtual output port	2018-08-22 09:29:02.257419	2018-08-22 09:29:02.257423	null	1979
3895	failing_network_2018-08-22T11-29-01/root/sink_4/output	output	\N	2018-08-22 09:29:02.2579	2018-08-22 09:29:02.257903	null	1980
3896	failing_network_2018-08-22T11-29-01/root/sink_4/v_out_port	•••	Virtual output port	2018-08-22 09:29:02.258409	2018-08-22 09:29:02.258413	null	1980
3897	failing_network_2018-08-22T11-29-01/root/sink_5/output	output	\N	2018-08-22 09:29:02.258941	2018-08-22 09:29:02.258945	null	1981
3898	failing_network_2018-08-22T11-29-01/root/sink_5/v_out_port	•••	Virtual output port	2018-08-22 09:29:02.259534	2018-08-22 09:29:02.259538	null	1981
3899	failing_network_2018-08-22T11-29-01/root/const_step_2_fail_1_0/output	output	\N	2018-08-22 09:29:02.260012	2018-08-22 09:29:02.260016	{"datatype": "Boolean", "dimension_names": ["const_step_2_fail_1_0"]}	1982
3900	failing_network_2018-08-22T11-29-01/root/const_step_2_fail_1_0/v_out_port	•••	Virtual output port	2018-08-22 09:29:02.260557	2018-08-22 09:29:02.260562	null	1982
3901	failing_network_2018-08-22T11-29-01/root/const_step_1_fail_2_0/output	output	\N	2018-08-22 09:29:02.26102	2018-08-22 09:29:02.261039	{"datatype": "Boolean", "dimension_names": ["const_step_1_fail_2_0"]}	1983
3902	failing_network_2018-08-22T11-29-01/root/const_step_1_fail_2_0/v_out_port	•••	Virtual output port	2018-08-22 09:29:02.261499	2018-08-22 09:29:02.261503	null	1983
3903	failing_network_2018-08-22T12-10-16/root/step_1/out_1	out_1	\N	2018-08-22 10:10:17.513791	2018-08-22 10:10:17.513797	{"datatype": "Int", "dimension_names": ["source_1"]}	1985
3904	failing_network_2018-08-22T12-10-16/root/step_1/out_2	out_2	\N	2018-08-22 10:10:17.515249	2018-08-22 10:10:17.515253	{"datatype": "Int", "dimension_names": ["source_1"]}	1985
3905	failing_network_2018-08-22T12-10-16/root/step_1/v_out_port	•••	Virtual output port	2018-08-22 10:10:17.515822	2018-08-22 10:10:17.515826	null	1985
3906	failing_network_2018-08-22T12-10-16/root/step_3/out_1	out_1	\N	2018-08-22 10:10:17.516499	2018-08-22 10:10:17.516503	{"datatype": "Int", "dimension_names": ["source_1"]}	1986
3907	failing_network_2018-08-22T12-10-16/root/step_3/out_2	out_2	\N	2018-08-22 10:10:17.517034	2018-08-22 10:10:17.517037	{"datatype": "Int", "dimension_names": ["source_1"]}	1986
3908	failing_network_2018-08-22T12-10-16/root/step_3/v_out_port	•••	Virtual output port	2018-08-22 10:10:17.517586	2018-08-22 10:10:17.51759	null	1986
3909	failing_network_2018-08-22T12-10-16/root/step_2/out_1	out_1	\N	2018-08-22 10:10:17.5181	2018-08-22 10:10:17.518104	{"datatype": "Int", "dimension_names": ["source_3"]}	1987
3910	failing_network_2018-08-22T12-10-16/root/step_2/out_2	out_2	\N	2018-08-22 10:10:17.518676	2018-08-22 10:10:17.51868	{"datatype": "Int", "dimension_names": ["source_3"]}	1987
3911	failing_network_2018-08-22T12-10-16/root/step_2/v_out_port	•••	Virtual output port	2018-08-22 10:10:17.519135	2018-08-22 10:10:17.519139	null	1987
3912	failing_network_2018-08-22T12-10-16/root/sum/result	result	\N	2018-08-22 10:10:17.51961	2018-08-22 10:10:17.519614	{"datatype": "Int", "dimension_names": ["source_1"]}	1988
3913	failing_network_2018-08-22T12-10-16/root/sum/v_out_port	•••	Virtual output port	2018-08-22 10:10:17.52006	2018-08-22 10:10:17.520064	null	1988
3914	failing_network_2018-08-22T12-10-16/root/source_1/output	output	\N	2018-08-22 10:10:17.520598	2018-08-22 10:10:17.520602	{"datatype": "Int", "dimension_names": ["source_1"]}	1989
3915	failing_network_2018-08-22T12-10-16/root/source_1/v_out_port	•••	Virtual output port	2018-08-22 10:10:17.521033	2018-08-22 10:10:17.521037	null	1989
3916	failing_network_2018-08-22T12-10-16/root/sink_2/output	output	\N	2018-08-22 10:10:17.521444	2018-08-22 10:10:17.521448	null	1990
3917	failing_network_2018-08-22T12-10-16/root/sink_2/v_out_port	•••	Virtual output port	2018-08-22 10:10:17.521888	2018-08-22 10:10:17.521892	null	1990
3918	failing_network_2018-08-22T12-10-16/root/sink_3/output	output	\N	2018-08-22 10:10:17.522277	2018-08-22 10:10:17.522281	null	1991
3919	failing_network_2018-08-22T12-10-16/root/sink_3/v_out_port	•••	Virtual output port	2018-08-22 10:10:17.522701	2018-08-22 10:10:17.522705	null	1991
3920	failing_network_2018-08-22T12-10-16/root/range/result	result	\N	2018-08-22 10:10:17.523097	2018-08-22 10:10:17.523101	{"datatype": "Int", "dimension_names": ["source_1"]}	1992
3921	failing_network_2018-08-22T12-10-16/root/range/v_out_port	•••	Virtual output port	2018-08-22 10:10:17.523497	2018-08-22 10:10:17.523501	null	1992
3922	failing_network_2018-08-22T12-10-16/root/sink_1/output	output	\N	2018-08-22 10:10:17.523937	2018-08-22 10:10:17.523941	null	1993
3923	failing_network_2018-08-22T12-10-16/root/sink_1/v_out_port	•••	Virtual output port	2018-08-22 10:10:17.52434	2018-08-22 10:10:17.524344	null	1993
3924	failing_network_2018-08-22T12-10-16/root/source_2/output	output	\N	2018-08-22 10:10:17.524771	2018-08-22 10:10:17.524775	{"datatype": "Int", "dimension_names": ["source_2"]}	1994
3925	failing_network_2018-08-22T12-10-16/root/source_2/v_out_port	•••	Virtual output port	2018-08-22 10:10:17.525204	2018-08-22 10:10:17.525208	null	1994
3926	failing_network_2018-08-22T12-10-16/root/source_3/output	output	\N	2018-08-22 10:10:17.525731	2018-08-22 10:10:17.525735	{"datatype": "Int", "dimension_names": ["source_3"]}	1995
3927	failing_network_2018-08-22T12-10-16/root/source_3/v_out_port	•••	Virtual output port	2018-08-22 10:10:17.526188	2018-08-22 10:10:17.526191	null	1995
3928	failing_network_2018-08-22T12-10-16/root/sink_4/output	output	\N	2018-08-22 10:10:17.526578	2018-08-22 10:10:17.526601	null	1996
3929	failing_network_2018-08-22T12-10-16/root/sink_4/v_out_port	•••	Virtual output port	2018-08-22 10:10:17.527032	2018-08-22 10:10:17.527035	null	1996
3930	failing_network_2018-08-22T12-10-16/root/sink_5/output	output	\N	2018-08-22 10:10:17.527406	2018-08-22 10:10:17.527409	null	1997
3931	failing_network_2018-08-22T12-10-16/root/sink_5/v_out_port	•••	Virtual output port	2018-08-22 10:10:17.527908	2018-08-22 10:10:17.52793	null	1997
3932	failing_network_2018-08-22T12-10-16/root/const_step_2_fail_1_0/output	output	\N	2018-08-22 10:10:17.528532	2018-08-22 10:10:17.528536	{"datatype": "Boolean", "dimension_names": ["const_step_2_fail_1_0"]}	1998
3933	failing_network_2018-08-22T12-10-16/root/const_step_2_fail_1_0/v_out_port	•••	Virtual output port	2018-08-22 10:10:17.529037	2018-08-22 10:10:17.52904	null	1998
3934	failing_network_2018-08-22T12-10-16/root/const_step_1_fail_2_0/output	output	\N	2018-08-22 10:10:17.529487	2018-08-22 10:10:17.529491	{"datatype": "Boolean", "dimension_names": ["const_step_1_fail_2_0"]}	1999
3935	failing_network_2018-08-22T12-10-16/root/const_step_1_fail_2_0/v_out_port	•••	Virtual output port	2018-08-22 10:10:17.529968	2018-08-22 10:10:17.529972	null	1999
3936	failing_macro_top_level_2018-08-22T12-15-04/root/source_b/output	output	\N	2018-08-22 10:15:04.866534	2018-08-22 10:15:04.86654	{"datatype": "Int", "dimension_names": ["source_b"]}	2001
3937	failing_macro_top_level_2018-08-22T12-15-04/root/source_b/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.867146	2018-08-22 10:15:04.86715	null	2001
3938	failing_macro_top_level_2018-08-22T12-15-04/root/source_c/output	output	\N	2018-08-22 10:15:04.867674	2018-08-22 10:15:04.867678	{"datatype": "Int", "dimension_names": ["source_c"]}	2002
3939	failing_macro_top_level_2018-08-22T12-15-04/root/source_c/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.868121	2018-08-22 10:15:04.868125	null	2002
3940	failing_macro_top_level_2018-08-22T12-15-04/root/source_a/output	output	\N	2018-08-22 10:15:04.868646	2018-08-22 10:15:04.868651	{"datatype": "Int", "dimension_names": ["source_a"]}	2003
3941	failing_macro_top_level_2018-08-22T12-15-04/root/source_a/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.869127	2018-08-22 10:15:04.869131	null	2003
3942	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.869618	2018-08-22 10:15:04.869622	null	2004
3943	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/out_1	out_1	\N	2018-08-22 10:15:04.870055	2018-08-22 10:15:04.870059	{"datatype": "Int", "dimension_names": ["source_1"]}	2008
3944	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/out_2	out_2	\N	2018-08-22 10:15:04.870608	2018-08-22 10:15:04.870613	{"datatype": "Int", "dimension_names": ["source_1"]}	2008
3945	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_1/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.871075	2018-08-22 10:15:04.871079	null	2008
3946	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/out_1	out_1	\N	2018-08-22 10:15:04.871501	2018-08-22 10:15:04.871505	{"datatype": "Int", "dimension_names": ["source_1"]}	2009
3947	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/out_2	out_2	\N	2018-08-22 10:15:04.871984	2018-08-22 10:15:04.872004	{"datatype": "Int", "dimension_names": ["source_1"]}	2009
3948	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_3/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.872538	2018-08-22 10:15:04.872544	null	2009
3949	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/out_1	out_1	\N	2018-08-22 10:15:04.873076	2018-08-22 10:15:04.873079	{"datatype": "Int", "dimension_names": ["source_3"]}	2010
3950	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/out_2	out_2	\N	2018-08-22 10:15:04.873546	2018-08-22 10:15:04.87355	{"datatype": "Int", "dimension_names": ["source_3"]}	2010
3951	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/step_2/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.874021	2018-08-22 10:15:04.874025	null	2010
3952	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sum/result	result	\N	2018-08-22 10:15:04.874607	2018-08-22 10:15:04.87461	{"datatype": "Int", "dimension_names": ["source_1"]}	2011
3953	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sum/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.875051	2018-08-22 10:15:04.875055	null	2011
3954	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_5/output	output	\N	2018-08-22 10:15:04.875475	2018-08-22 10:15:04.875479	null	2012
3955	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_5/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.875943	2018-08-22 10:15:04.875947	null	2012
3956	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_2/output	output	\N	2018-08-22 10:15:04.876462	2018-08-22 10:15:04.876466	null	2013
3957	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_2/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.876973	2018-08-22 10:15:04.876977	null	2013
3958	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_3/output	output	\N	2018-08-22 10:15:04.877502	2018-08-22 10:15:04.877505	null	2014
3959	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_3/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.877988	2018-08-22 10:15:04.877992	null	2014
3960	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range/result	result	\N	2018-08-22 10:15:04.878502	2018-08-22 10:15:04.878506	{"datatype": "Int", "dimension_names": ["source_1"]}	2015
3961	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/range/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.878986	2018-08-22 10:15:04.87899	null	2015
3962	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_1/output	output	\N	2018-08-22 10:15:04.879498	2018-08-22 10:15:04.879502	null	2016
3963	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_1/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.879987	2018-08-22 10:15:04.879991	null	2016
3964	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_2/output	output	\N	2018-08-22 10:15:04.880514	2018-08-22 10:15:04.880518	{"datatype": "Int", "dimension_names": ["source_2"]}	2017
3965	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_2/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.881069	2018-08-22 10:15:04.881073	null	2017
3966	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_3/output	output	\N	2018-08-22 10:15:04.881504	2018-08-22 10:15:04.881508	{"datatype": "Int", "dimension_names": ["source_3"]}	2018
3967	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_3/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.881994	2018-08-22 10:15:04.881998	null	2018
3968	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_4/output	output	\N	2018-08-22 10:15:04.882512	2018-08-22 10:15:04.882515	null	2019
3969	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/sink_4/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.883021	2018-08-22 10:15:04.883025	null	2019
3970	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1/output	output	\N	2018-08-22 10:15:04.883604	2018-08-22 10:15:04.883608	{"datatype": "Int", "dimension_names": ["source_1"]}	2020
3971	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/source_1/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.884065	2018-08-22 10:15:04.884069	null	2020
3972	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_2_fail_1_0/output	output	\N	2018-08-22 10:15:04.884708	2018-08-22 10:15:04.884712	{"datatype": "Boolean", "dimension_names": ["const_step_2_fail_1_0"]}	2021
3973	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_2_fail_1_0/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.88516	2018-08-22 10:15:04.885164	null	2021
3974	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_1_fail_2_0/output	output	\N	2018-08-22 10:15:04.885641	2018-08-22 10:15:04.885645	{"datatype": "Boolean", "dimension_names": ["const_step_1_fail_2_0"]}	2022
3975	failing_macro_top_level_2018-08-22T12-15-04/root/failing_macro/const_step_1_fail_2_0/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.88608	2018-08-22 10:15:04.886084	null	2022
3976	failing_macro_top_level_2018-08-22T12-15-04/root/add/result	result	\N	2018-08-22 10:15:04.886558	2018-08-22 10:15:04.886562	{"datatype": "Int", "dimension_names": ["source_a"]}	2005
3977	failing_macro_top_level_2018-08-22T12-15-04/root/add/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.887007	2018-08-22 10:15:04.88701	null	2005
3978	failing_macro_top_level_2018-08-22T12-15-04/root/sink/output	output	\N	2018-08-22 10:15:04.887434	2018-08-22 10:15:04.887438	null	2006
3979	failing_macro_top_level_2018-08-22T12-15-04/root/sink/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.888032	2018-08-22 10:15:04.888036	null	2006
3980	failing_macro_top_level_2018-08-22T12-15-04/root/const_add_right_hand_0/output	output	\N	2018-08-22 10:15:04.888641	2018-08-22 10:15:04.888645	{"datatype": "Int", "dimension_names": ["const_add_right_hand_0"]}	2007
3981	failing_macro_top_level_2018-08-22T12-15-04/root/const_add_right_hand_0/v_out_port	•••	Virtual output port	2018-08-22 10:15:04.889117	2018-08-22 10:15:04.889121	null	2007
3982	macro_top_level_2018-08-22T12-36-32/root/source/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.115138	2018-08-22 10:36:33.115144	null	2024
3983	macro_top_level_2018-08-22T12-36-32/root/source/source/output	output	\N	2018-08-22 10:36:33.116246	2018-08-22 10:36:33.11625	{"datatype": "Int", "dimension_names": ["source"]}	2027
3984	macro_top_level_2018-08-22T12-36-32/root/source/source/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.116839	2018-08-22 10:36:33.116843	null	2027
3985	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.117621	2018-08-22 10:36:33.117625	null	2025
3986	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value/output	output	\N	2018-08-22 10:36:33.118162	2018-08-22 10:36:33.118166	{"datatype": "Int", "dimension_names": ["input_value"]}	2028
3987	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/input_value/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.118588	2018-08-22 10:36:33.118591	null	2028
3988	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.119055	2018-08-22 10:36:33.119059	null	2029
3989	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.119469	2018-08-22 10:36:33.119472	null	2032
3990	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1/result	result	\N	2018-08-22 10:36:33.119891	2018-08-22 10:36:33.119895	{"datatype": "Int", "dimension_names": ["input"]}	2038
3991	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint1/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.120438	2018-08-22 10:36:33.120442	null	2038
3992	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2/result	result	\N	2018-08-22 10:36:33.121026	2018-08-22 10:36:33.121029	{"datatype": "Int", "dimension_names": ["input"]}	2039
3993	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/addint2/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.121611	2018-08-22 10:36:33.121614	null	2039
3994	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/const_addint1_right_hand_0/output	output	\N	2018-08-22 10:36:33.122114	2018-08-22 10:36:33.122117	{"datatype": "Int", "dimension_names": ["const_addint1_right_hand_0"]}	2040
3995	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/const_addint1_right_hand_0/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.12252	2018-08-22 10:36:33.122523	null	2040
3996	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/const_addint2_right_hand_0/output	output	\N	2018-08-22 10:36:33.122977	2018-08-22 10:36:33.12298	{"datatype": "Int", "dimension_names": ["const_addint2_right_hand_0"]}	2041
3997	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/add/const_addint2_right_hand_0/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.123394	2018-08-22 10:36:33.123397	null	2041
3998	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink/output	output	\N	2018-08-22 10:36:33.123866	2018-08-22 10:36:33.12387	null	2033
3999	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/macro_sink/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.124448	2018-08-22 10:36:33.124452	null	2033
4000	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input/output	output	\N	2018-08-22 10:36:33.125019	2018-08-22 10:36:33.125023	{"datatype": "Int", "dimension_names": ["input"]}	2034
4001	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_1/input/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.125471	2018-08-22 10:36:33.125475	null	2034
4002	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/output_value/output	output	\N	2018-08-22 10:36:33.125916	2018-08-22 10:36:33.12592	null	2030
4003	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/output_value/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.126349	2018-08-22 10:36:33.126352	null	2030
4004	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.126753	2018-08-22 10:36:33.126757	null	2031
4005	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.127134	2018-08-22 10:36:33.127137	null	2035
4006	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1/result	result	\N	2018-08-22 10:36:33.127523	2018-08-22 10:36:33.127527	{"datatype": "Int", "dimension_names": ["input"]}	2042
4007	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint1/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.128013	2018-08-22 10:36:33.128016	null	2042
4008	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2/result	result	\N	2018-08-22 10:36:33.128559	2018-08-22 10:36:33.128562	{"datatype": "Int", "dimension_names": ["input"]}	2043
4009	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/addint2/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.129057	2018-08-22 10:36:33.12906	null	2043
4010	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/const_addint1_right_hand_0/output	output	\N	2018-08-22 10:36:33.1295	2018-08-22 10:36:33.129506	{"datatype": "Int", "dimension_names": ["const_addint1_right_hand_0"]}	2044
4011	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/const_addint1_right_hand_0/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.130023	2018-08-22 10:36:33.130026	null	2044
4012	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/const_addint2_right_hand_0/output	output	\N	2018-08-22 10:36:33.130437	2018-08-22 10:36:33.130441	{"datatype": "Int", "dimension_names": ["const_addint2_right_hand_0"]}	2045
4013	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/add/const_addint2_right_hand_0/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.130919	2018-08-22 10:36:33.130922	null	2045
4014	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/macro_sink/output	output	\N	2018-08-22 10:36:33.131567	2018-08-22 10:36:33.131571	null	2036
4015	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/macro_sink/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.132057	2018-08-22 10:36:33.132061	null	2036
4016	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input/output	output	\N	2018-08-22 10:36:33.13265	2018-08-22 10:36:33.132654	{"datatype": "Int", "dimension_names": ["input"]}	2037
4017	macro_top_level_2018-08-22T12-36-32/root/node_add_ints/node_add_multiple_ints_2/input/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.133186	2018-08-22 10:36:33.133189	null	2037
4018	macro_top_level_2018-08-22T12-36-32/root/sink/output	output	\N	2018-08-22 10:36:33.133611	2018-08-22 10:36:33.133615	null	2026
4019	macro_top_level_2018-08-22T12-36-32/root/sink/v_out_port	•••	Virtual output port	2018-08-22 10:36:33.134099	2018-08-22 10:36:33.134102	null	2026
4020	macro_node_2_2018-08-22T12-40-46/root/sum/result	result	\N	2018-08-22 10:40:46.861538	2018-08-22 10:40:46.861543	{"datatype": "Int", "dimension_names": ["source_1", "source_3"]}	2047
4021	macro_node_2_2018-08-22T12-40-46/root/sum/v_out_port	•••	Virtual output port	2018-08-22 10:40:46.862203	2018-08-22 10:40:46.862207	null	2047
4022	macro_node_2_2018-08-22T12-40-46/root/addint/result	result	\N	2018-08-22 10:40:46.862809	2018-08-22 10:40:46.862813	{"datatype": "Int", "dimension_names": ["source_1", "source_2"]}	2048
4023	macro_node_2_2018-08-22T12-40-46/root/addint/v_out_port	•••	Virtual output port	2018-08-22 10:40:46.86328	2018-08-22 10:40:46.863284	null	2048
4024	macro_node_2_2018-08-22T12-40-46/root/source_2/output	output	\N	2018-08-22 10:40:46.863761	2018-08-22 10:40:46.863764	{"datatype": "Int", "dimension_names": ["source_2"]}	2049
4025	macro_node_2_2018-08-22T12-40-46/root/source_2/v_out_port	•••	Virtual output port	2018-08-22 10:40:46.864276	2018-08-22 10:40:46.864279	null	2049
4026	macro_node_2_2018-08-22T12-40-46/root/source_3/output	output	\N	2018-08-22 10:40:46.864824	2018-08-22 10:40:46.864828	{"datatype": "Int", "dimension_names": ["source_3"]}	2050
4027	macro_node_2_2018-08-22T12-40-46/root/source_3/v_out_port	•••	Virtual output port	2018-08-22 10:40:46.86528	2018-08-22 10:40:46.865284	null	2050
4028	macro_node_2_2018-08-22T12-40-46/root/source_1/output	output	\N	2018-08-22 10:40:46.865759	2018-08-22 10:40:46.865763	{"datatype": "Int", "dimension_names": ["source_1"]}	2051
4029	macro_node_2_2018-08-22T12-40-46/root/source_1/v_out_port	•••	Virtual output port	2018-08-22 10:40:46.86627	2018-08-22 10:40:46.866274	null	2051
4030	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/v_out_port	•••	Virtual output port	2018-08-22 10:40:46.866761	2018-08-22 10:40:46.866765	null	2052
4031	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/output	output	\N	2018-08-22 10:40:46.867227	2018-08-22 10:40:46.867231	null	2054
4032	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_sink/v_out_port	•••	Virtual output port	2018-08-22 10:40:46.867833	2018-08-22 10:40:46.867837	null	2054
4033	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/result	result	\N	2018-08-22 10:40:46.868309	2018-08-22 10:40:46.868313	{"datatype": "Int", "dimension_names": ["macro_input_1", "macro_input_2"]}	2055
4034	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/addint1/v_out_port	•••	Virtual output port	2018-08-22 10:40:46.868765	2018-08-22 10:40:46.868768	null	2055
4035	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1/output	output	\N	2018-08-22 10:40:46.869217	2018-08-22 10:40:46.869221	{"datatype": "Int", "dimension_names": ["macro_input_1"]}	2056
4036	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_1/v_out_port	•••	Virtual output port	2018-08-22 10:40:46.869658	2018-08-22 10:40:46.869661	null	2056
4037	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_2/output	output	\N	2018-08-22 10:40:46.870104	2018-08-22 10:40:46.870107	{"datatype": "Int", "dimension_names": ["macro_input_2"]}	2057
4038	macro_node_2_2018-08-22T12-40-46/root/node_add_multiple_ints_1/macro_input_2/v_out_port	•••	Virtual output port	2018-08-22 10:40:46.870688	2018-08-22 10:40:46.870692	null	2057
4039	macro_node_2_2018-08-22T12-40-46/root/sink/output	output	\N	2018-08-22 10:40:46.871245	2018-08-22 10:40:46.871248	null	2053
4040	macro_node_2_2018-08-22T12-40-46/root/sink/v_out_port	•••	Virtual output port	2018-08-22 10:40:46.871838	2018-08-22 10:40:46.871842	null	2053
4041	cross_validation_2018-08-22T15-29-33/root/crossvalidtion/train	train	\N	2018-08-22 13:29:33.793817	2018-08-22 13:29:33.793822	{"datatype": "Int", "dimension_names": ["numbers_train", "crossvalidtion"]}	2059
4042	cross_validation_2018-08-22T15-29-33/root/crossvalidtion/test	test	\N	2018-08-22 13:29:33.794701	2018-08-22 13:29:33.794705	{"datatype": "Int", "dimension_names": ["numbers_test", "crossvalidtion"]}	2059
4043	cross_validation_2018-08-22T15-29-33/root/crossvalidtion/v_out_port	•••	Virtual output port	2018-08-22 13:29:33.795301	2018-08-22 13:29:33.795307	null	2059
4044	cross_validation_2018-08-22T15-29-33/root/sum/result	result	\N	2018-08-22 13:29:33.795813	2018-08-22 13:29:33.795817	{"datatype": "Int", "dimension_names": ["numbers_test", "crossvalidtion"]}	2060
4045	cross_validation_2018-08-22T15-29-33/root/sum/v_out_port	•••	Virtual output port	2018-08-22 13:29:33.796351	2018-08-22 13:29:33.796355	null	2060
4046	cross_validation_2018-08-22T15-29-33/root/const_crossvalidtion_method_0/output	output	\N	2018-08-22 13:29:33.796835	2018-08-22 13:29:33.796839	{"datatype": "__CrossValidation_0.1_interface__method__Enum__", "dimension_names": ["const_crossvalidtion_method_0"]}	2061
4047	cross_validation_2018-08-22T15-29-33/root/const_crossvalidtion_method_0/v_out_port	•••	Virtual output port	2018-08-22 13:29:33.797336	2018-08-22 13:29:33.79734	null	2061
4048	cross_validation_2018-08-22T15-29-33/root/numbers/output	output	\N	2018-08-22 13:29:33.79788	2018-08-22 13:29:33.797883	{"datatype": "Int", "dimension_names": ["numbers"]}	2062
4049	cross_validation_2018-08-22T15-29-33/root/numbers/v_out_port	•••	Virtual output port	2018-08-22 13:29:33.798332	2018-08-22 13:29:33.798336	null	2062
4050	cross_validation_2018-08-22T15-29-33/root/multiply/result	result	\N	2018-08-22 13:29:33.798804	2018-08-22 13:29:33.798807	{"datatype": "Float", "dimension_names": ["numbers_train", "numbers_test", "crossvalidtion"]}	2063
4051	cross_validation_2018-08-22T15-29-33/root/multiply/v_out_port	•••	Virtual output port	2018-08-22 13:29:33.799229	2018-08-22 13:29:33.799233	null	2063
4052	cross_validation_2018-08-22T15-29-33/root/const_crossvalidtion_number_of_folds_0/output	output	\N	2018-08-22 13:29:33.79964	2018-08-22 13:29:33.799643	{"datatype": "Int", "dimension_names": ["const_crossvalidtion_number_of_folds_0"]}	2064
4053	cross_validation_2018-08-22T15-29-33/root/const_crossvalidtion_number_of_folds_0/v_out_port	•••	Virtual output port	2018-08-22 13:29:33.800073	2018-08-22 13:29:33.800076	null	2064
4054	cross_validation_2018-08-22T15-29-33/root/sink/output	output	\N	2018-08-22 13:29:33.800555	2018-08-22 13:29:33.800559	null	2065
4055	cross_validation_2018-08-22T15-29-33/root/sink/v_out_port	•••	Virtual output port	2018-08-22 13:29:33.800996	2018-08-22 13:29:33.801	null	2065
4056	input_groups_2018-08-22T15-39-07/root/source/output	output	\N	2018-08-22 13:39:07.335235	2018-08-22 13:39:07.335245	{"datatype": "Int", "dimension_names": ["source"]}	2067
4057	input_groups_2018-08-22T15-39-07/root/source/v_out_port	•••	Virtual output port	2018-08-22 13:39:07.335838	2018-08-22 13:39:07.335841	null	2067
4058	input_groups_2018-08-22T15-39-07/root/add/result	result	\N	2018-08-22 13:39:07.336527	2018-08-22 13:39:07.336531	{"datatype": "Int", "dimension_names": ["source", "const_add_right_hand_0"]}	2068
4059	input_groups_2018-08-22T15-39-07/root/add/v_out_port	•••	Virtual output port	2018-08-22 13:39:07.337373	2018-08-22 13:39:07.337378	null	2068
4060	input_groups_2018-08-22T15-39-07/root/const_add_right_hand_0/output	output	\N	2018-08-22 13:39:07.337973	2018-08-22 13:39:07.337978	{"datatype": "Int", "dimension_names": ["const_add_right_hand_0"]}	2069
4061	input_groups_2018-08-22T15-39-07/root/const_add_right_hand_0/v_out_port	•••	Virtual output port	2018-08-22 13:39:07.338497	2018-08-22 13:39:07.338502	null	2069
4062	input_groups_2018-08-22T15-39-07/root/sink/output	output	\N	2018-08-22 13:39:07.338979	2018-08-22 13:39:07.338983	null	2070
4063	input_groups_2018-08-22T15-39-07/root/sink/v_out_port	•••	Virtual output port	2018-08-22 13:39:07.339469	2018-08-22 13:39:07.339473	null	2070
4245	example_2018-09-03T11-14-49/root/sink1/output	output	\N	2018-09-03 09:14:49.30136	2018-09-03 09:14:49.301364	{"datatype": "Int", "dimension_names": []}	2171
4246	example_2018-09-03T11-14-49/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 09:14:49.301828	2018-09-03 09:14:49.301831	null	2171
4247	example_2018-09-03T11-14-49/root/addint/result	result	\N	2018-09-03 09:14:49.302233	2018-09-03 09:14:49.302236	{"datatype": "Int", "dimension_names": ["source1"]}	2172
4248	example_2018-09-03T11-14-49/root/addint/v_out_port	•••	Virtual output port	2018-09-03 09:14:49.302627	2018-09-03 09:14:49.302631	null	2172
4249	example_2018-09-03T11-14-49/root/const_addint_right_hand_0/output	output	\N	2018-09-03 09:14:49.303001	2018-09-03 09:14:49.303004	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2173
4250	example_2018-09-03T11-14-49/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 09:14:49.30336	2018-09-03 09:14:49.303364	null	2173
4251	example_2018-09-03T11-17-35/root/source1/output	output	\N	2018-09-03 09:17:35.58502	2018-09-03 09:17:35.585025	{"datatype": "Int", "dimension_names": ["source1"]}	2175
4252	example_2018-09-03T11-17-35/root/source1/v_out_port	•••	Virtual output port	2018-09-03 09:17:35.585498	2018-09-03 09:17:35.585502	null	2175
4253	example_2018-09-03T11-17-35/root/sink1/output	output	\N	2018-09-03 09:17:35.585914	2018-09-03 09:17:35.585918	{"datatype": "Int", "dimension_names": []}	2176
4254	example_2018-09-03T11-17-35/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 09:17:35.586308	2018-09-03 09:17:35.586312	null	2176
4255	example_2018-09-03T11-17-35/root/addint/result	result	\N	2018-09-03 09:17:35.586754	2018-09-03 09:17:35.586758	{"datatype": "Int", "dimension_names": ["source1"]}	2177
4256	example_2018-09-03T11-17-35/root/addint/v_out_port	•••	Virtual output port	2018-09-03 09:17:35.587131	2018-09-03 09:17:35.587135	null	2177
4257	example_2018-09-03T11-17-35/root/const_addint_right_hand_0/output	output	\N	2018-09-03 09:17:35.587533	2018-09-03 09:17:35.587537	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2178
4258	example_2018-09-03T11-17-35/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 09:17:35.58797	2018-09-03 09:17:35.587973	null	2178
4259	example_2018-09-03T11-31-38/root/source1/output	output	\N	2018-09-03 09:31:39.070375	2018-09-03 09:31:39.07038	{"datatype": "Int", "dimension_names": ["source1"]}	2180
4260	example_2018-09-03T11-31-38/root/source1/v_out_port	•••	Virtual output port	2018-09-03 09:31:39.071067	2018-09-03 09:31:39.071071	null	2180
4261	example_2018-09-03T11-31-38/root/sink1/output	output	\N	2018-09-03 09:31:39.071654	2018-09-03 09:31:39.071658	{"datatype": "Int", "dimension_names": []}	2181
4262	example_2018-09-03T11-31-38/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 09:31:39.072134	2018-09-03 09:31:39.072138	null	2181
4263	example_2018-09-03T11-31-38/root/addint/result	result	\N	2018-09-03 09:31:39.072633	2018-09-03 09:31:39.072637	{"datatype": "Int", "dimension_names": ["source1"]}	2182
4264	example_2018-09-03T11-31-38/root/addint/v_out_port	•••	Virtual output port	2018-09-03 09:31:39.073067	2018-09-03 09:31:39.073071	null	2182
4265	example_2018-09-03T11-31-38/root/const_addint_right_hand_0/output	output	\N	2018-09-03 09:31:39.073446	2018-09-03 09:31:39.07345	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2183
4266	example_2018-09-03T11-31-38/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 09:31:39.074031	2018-09-03 09:31:39.074035	null	2183
4333	example_2018-09-03T13-09-35/root/addint/result	result	\N	2018-09-03 11:09:36.242877	2018-09-03 11:09:36.242881	{"dimension_names": ["source1"], "datatype": "Int"}	2222
4334	example_2018-09-03T13-09-35/root/addint/v_out_port	•••	Virtual output port	2018-09-03 11:09:36.243285	2018-09-03 11:09:36.243288	null	2222
4335	example_2018-09-03T13-09-35/root/source1/output	output	\N	2018-09-03 11:09:36.243693	2018-09-03 11:09:36.243696	{"dimension_names": ["source1"], "datatype": "Int"}	2223
4336	example_2018-09-03T13-09-35/root/source1/v_out_port	•••	Virtual output port	2018-09-03 11:09:36.244082	2018-09-03 11:09:36.244086	null	2223
4337	example_2018-09-03T13-09-35/root/const_addint_right_hand_0/output	output	\N	2018-09-03 11:09:36.244586	2018-09-03 11:09:36.24459	{"dimension_names": ["const_addint_right_hand_0"], "datatype": "Int"}	2224
4338	example_2018-09-03T13-09-35/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 11:09:36.245107	2018-09-03 11:09:36.245111	null	2224
4339	example_2018-09-03T13-11-45/root/addint/result	result	\N	2018-09-03 11:11:45.558486	2018-09-03 11:11:45.558491	{"dimension_names": ["source1"], "datatype": "Int"}	2226
4340	example_2018-09-03T13-11-45/root/addint/v_out_port	•••	Virtual output port	2018-09-03 11:11:45.558984	2018-09-03 11:11:45.558988	null	2226
4341	example_2018-09-03T13-11-45/root/source1/output	output	\N	2018-09-03 11:11:45.559474	2018-09-03 11:11:45.559478	{"dimension_names": ["source1"], "datatype": "Int"}	2227
4342	example_2018-09-03T13-11-45/root/source1/v_out_port	•••	Virtual output port	2018-09-03 11:11:45.559874	2018-09-03 11:11:45.559878	null	2227
4343	example_2018-09-03T13-11-45/root/sink1/output	output	\N	2018-09-03 11:11:45.560296	2018-09-03 11:11:45.5603	{"dimension_names": [], "datatype": "Int"}	2228
4344	example_2018-09-03T13-11-45/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 11:11:45.560765	2018-09-03 11:11:45.560768	null	2228
4345	example_2018-09-03T13-11-45/root/const_addint_right_hand_0/output	output	\N	2018-09-03 11:11:45.56119	2018-09-03 11:11:45.561193	{"dimension_names": ["const_addint_right_hand_0"], "datatype": "Int"}	2229
4346	example_2018-09-03T13-11-45/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 11:11:45.561652	2018-09-03 11:11:45.561656	null	2229
4347	example_2018-09-03T13-12-39/root/source1/output	output	\N	2018-09-03 11:12:40.05857	2018-09-03 11:12:40.058575	{"datatype": "Int", "dimension_names": ["source1"]}	2231
4348	example_2018-09-03T13-12-39/root/source1/v_out_port	•••	Virtual output port	2018-09-03 11:12:40.059302	2018-09-03 11:12:40.059306	null	2231
4349	example_2018-09-03T13-12-39/root/sink1/output	output	\N	2018-09-03 11:12:40.059816	2018-09-03 11:12:40.05982	{"datatype": "Int", "dimension_names": []}	2232
4350	example_2018-09-03T13-12-39/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 11:12:40.060359	2018-09-03 11:12:40.060363	null	2232
4351	example_2018-09-03T13-12-39/root/addint/result	result	\N	2018-09-03 11:12:40.060829	2018-09-03 11:12:40.060833	{"datatype": "Int", "dimension_names": ["source1"]}	2233
4352	example_2018-09-03T13-12-39/root/addint/v_out_port	•••	Virtual output port	2018-09-03 11:12:40.061438	2018-09-03 11:12:40.061442	null	2233
4353	example_2018-09-03T13-12-39/root/const_addint_right_hand_0/output	output	\N	2018-09-03 11:12:40.061899	2018-09-03 11:12:40.061903	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2234
4267	auto_prefix_test_2018-09-03T11-34-05/root/const/output	output	\N	2018-09-03 09:34:06.659098	2018-09-03 09:34:06.659102	{"datatype": "Int", "dimension_names": ["const"]}	2185
4268	auto_prefix_test_2018-09-03T11-34-05/root/const/v_out_port	•••	Virtual output port	2018-09-03 09:34:06.659826	2018-09-03 09:34:06.65983	null	2185
4269	auto_prefix_test_2018-09-03T11-34-05/root/source/output	output	\N	2018-09-03 09:34:06.660269	2018-09-03 09:34:06.660273	{"datatype": "Int", "dimension_names": ["source"]}	2186
4270	auto_prefix_test_2018-09-03T11-34-05/root/source/v_out_port	•••	Virtual output port	2018-09-03 09:34:06.660713	2018-09-03 09:34:06.660717	null	2186
4271	auto_prefix_test_2018-09-03T11-34-05/root/m_p/added	added	\N	2018-09-03 09:34:06.661152	2018-09-03 09:34:06.661156	{"datatype": "Int", "dimension_names": ["source"]}	2187
4272	auto_prefix_test_2018-09-03T11-34-05/root/m_p/multiplied	multiplied	\N	2018-09-03 09:34:06.66158	2018-09-03 09:34:06.661584	{"datatype": "Int", "dimension_names": ["source"]}	2187
4273	auto_prefix_test_2018-09-03T11-34-05/root/m_p/v_out_port	•••	Virtual output port	2018-09-03 09:34:06.661973	2018-09-03 09:34:06.661976	null	2187
4274	auto_prefix_test_2018-09-03T11-34-05/root/a_p/added	added	\N	2018-09-03 09:34:06.662347	2018-09-03 09:34:06.662351	{"datatype": "Int", "dimension_names": ["source"]}	2188
4275	auto_prefix_test_2018-09-03T11-34-05/root/a_p/multiplied	multiplied	\N	2018-09-03 09:34:06.662765	2018-09-03 09:34:06.662769	{"datatype": "Int", "dimension_names": ["source"]}	2188
4276	auto_prefix_test_2018-09-03T11-34-05/root/a_p/v_out_port	•••	Virtual output port	2018-09-03 09:34:06.663132	2018-09-03 09:34:06.663136	null	2188
4277	auto_prefix_test_2018-09-03T11-34-05/root/m_n/added	added	\N	2018-09-03 09:34:06.663582	2018-09-03 09:34:06.663586	{"datatype": "Int", "dimension_names": ["source"]}	2189
4278	auto_prefix_test_2018-09-03T11-34-05/root/m_n/multiplied	multiplied	\N	2018-09-03 09:34:06.663986	2018-09-03 09:34:06.663989	{"datatype": "Int", "dimension_names": ["source"]}	2189
4279	auto_prefix_test_2018-09-03T11-34-05/root/m_n/v_out_port	•••	Virtual output port	2018-09-03 09:34:06.664387	2018-09-03 09:34:06.664391	null	2189
4280	auto_prefix_test_2018-09-03T11-34-05/root/a_n/added	added	\N	2018-09-03 09:34:06.664802	2018-09-03 09:34:06.664806	{"datatype": "Int", "dimension_names": ["source"]}	2190
4281	auto_prefix_test_2018-09-03T11-34-05/root/a_n/multiplied	multiplied	\N	2018-09-03 09:34:06.66521	2018-09-03 09:34:06.665213	{"datatype": "Int", "dimension_names": ["source"]}	2190
4282	auto_prefix_test_2018-09-03T11-34-05/root/a_n/v_out_port	•••	Virtual output port	2018-09-03 09:34:06.665607	2018-09-03 09:34:06.66561	null	2190
4283	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_p/output	output	\N	2018-09-03 09:34:06.666041	2018-09-03 09:34:06.666044	{"datatype": "Int", "dimension_names": []}	2191
4284	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_p/v_out_port	•••	Virtual output port	2018-09-03 09:34:06.666448	2018-09-03 09:34:06.666452	null	2191
4285	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_p/output	output	\N	2018-09-03 09:34:06.666863	2018-09-03 09:34:06.666867	{"datatype": "Int", "dimension_names": []}	2192
4286	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_p/v_out_port	•••	Virtual output port	2018-09-03 09:34:06.667237	2018-09-03 09:34:06.667241	null	2192
4287	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_n/output	output	\N	2018-09-03 09:34:06.667698	2018-09-03 09:34:06.667702	{"datatype": "Int", "dimension_names": []}	2193
4288	auto_prefix_test_2018-09-03T11-34-05/root/sink_m_n/v_out_port	•••	Virtual output port	2018-09-03 09:34:06.668108	2018-09-03 09:34:06.668112	null	2193
4289	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_n/output	output	\N	2018-09-03 09:34:06.668547	2018-09-03 09:34:06.66855	{"datatype": "Int", "dimension_names": []}	2194
4290	auto_prefix_test_2018-09-03T11-34-05/root/sink_a_n/v_out_port	•••	Virtual output port	2018-09-03 09:34:06.668994	2018-09-03 09:34:06.668998	null	2194
4291	example_2018-09-03T12-17-58/root/source1/output	output	\N	2018-09-03 10:17:58.525078	2018-09-03 10:17:58.525084	{"datatype": "Int", "dimension_names": ["source1"]}	2196
4292	example_2018-09-03T12-17-58/root/source1/v_out_port	•••	Virtual output port	2018-09-03 10:17:58.525782	2018-09-03 10:17:58.525786	null	2196
4293	example_2018-09-03T12-17-58/root/sink1/output	output	\N	2018-09-03 10:17:58.526396	2018-09-03 10:17:58.5264	{"datatype": "Int", "dimension_names": []}	2197
4294	example_2018-09-03T12-17-58/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 10:17:58.526995	2018-09-03 10:17:58.527	null	2197
4295	example_2018-09-03T12-17-58/root/addint/result	result	\N	2018-09-03 10:17:58.527517	2018-09-03 10:17:58.527522	{"datatype": "Int", "dimension_names": ["source1"]}	2198
4296	example_2018-09-03T12-17-58/root/addint/v_out_port	•••	Virtual output port	2018-09-03 10:17:58.528046	2018-09-03 10:17:58.52805	null	2198
4297	example_2018-09-03T12-17-58/root/const_addint_right_hand_0/output	output	\N	2018-09-03 10:17:58.528589	2018-09-03 10:17:58.528594	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2199
4298	example_2018-09-03T12-17-58/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 10:17:58.529095	2018-09-03 10:17:58.529099	null	2199
4138	failing_network_2018-08-24T15-34-44/root/source_1/output	output	\N	2018-08-24 13:34:44.940799	2018-08-24 13:34:44.940805	{"datatype": "Int", "dimension_names": ["source_1"]}	2109
4139	failing_network_2018-08-24T15-34-44/root/source_1/v_out_port	•••	Virtual output port	2018-08-24 13:34:44.941318	2018-08-24 13:34:44.941322	null	2109
4140	failing_network_2018-08-24T15-34-44/root/source_2/output	output	\N	2018-08-24 13:34:44.941803	2018-08-24 13:34:44.941807	{"datatype": "Int", "dimension_names": ["source_2"]}	2110
4141	failing_network_2018-08-24T15-34-44/root/source_2/v_out_port	•••	Virtual output port	2018-08-24 13:34:44.942298	2018-08-24 13:34:44.942302	null	2110
4142	failing_network_2018-08-24T15-34-44/root/source_3/output	output	\N	2018-08-24 13:34:44.942752	2018-08-24 13:34:44.942756	{"datatype": "Int", "dimension_names": ["source_3"]}	2111
4143	failing_network_2018-08-24T15-34-44/root/source_3/v_out_port	•••	Virtual output port	2018-08-24 13:34:44.943279	2018-08-24 13:34:44.943283	null	2111
4144	failing_network_2018-08-24T15-34-44/root/sink_1/output	output	\N	2018-08-24 13:34:44.943797	2018-08-24 13:34:44.943801	{"datatype": "Int", "dimension_names": []}	2112
4145	failing_network_2018-08-24T15-34-44/root/sink_1/v_out_port	•••	Virtual output port	2018-08-24 13:34:44.944422	2018-08-24 13:34:44.944426	null	2112
4146	failing_network_2018-08-24T15-34-44/root/sink_2/output	output	\N	2018-08-24 13:34:44.944893	2018-08-24 13:34:44.944896	{"datatype": "Int", "dimension_names": []}	2113
4147	failing_network_2018-08-24T15-34-44/root/sink_2/v_out_port	•••	Virtual output port	2018-08-24 13:34:44.945544	2018-08-24 13:34:44.945548	null	2113
4148	failing_network_2018-08-24T15-34-44/root/sink_3/output	output	\N	2018-08-24 13:34:44.945986	2018-08-24 13:34:44.94599	{"datatype": "Int", "dimension_names": []}	2114
4149	failing_network_2018-08-24T15-34-44/root/sink_3/v_out_port	•••	Virtual output port	2018-08-24 13:34:44.946432	2018-08-24 13:34:44.946436	null	2114
4150	failing_network_2018-08-24T15-34-44/root/sink_4/output	output	\N	2018-08-24 13:34:44.946855	2018-08-24 13:34:44.946859	{"datatype": "Int", "dimension_names": []}	2115
4151	failing_network_2018-08-24T15-34-44/root/sink_4/v_out_port	•••	Virtual output port	2018-08-24 13:34:44.947257	2018-08-24 13:34:44.947261	null	2115
4152	failing_network_2018-08-24T15-34-44/root/sink_5/output	output	\N	2018-08-24 13:34:44.947653	2018-08-24 13:34:44.947657	{"datatype": "Int", "dimension_names": []}	2116
4153	failing_network_2018-08-24T15-34-44/root/sink_5/v_out_port	•••	Virtual output port	2018-08-24 13:34:44.948052	2018-08-24 13:34:44.948056	null	2116
4154	failing_network_2018-08-24T15-34-44/root/step_1/out_1	out_1	\N	2018-08-24 13:34:44.948459	2018-08-24 13:34:44.948463	{"datatype": "Int", "dimension_names": ["source_1"]}	2117
4299	example_2018-09-03T12-31-11/root/source1/output	output	\N	2018-09-03 10:31:12.130706	2018-09-03 10:31:12.130711	{"datatype": "Int", "dimension_names": ["source1"]}	2201
4155	failing_network_2018-08-24T15-34-44/root/step_1/out_2	out_2	\N	2018-08-24 13:34:44.94886	2018-08-24 13:34:44.948864	{"datatype": "Int", "dimension_names": ["source_1"]}	2117
4156	failing_network_2018-08-24T15-34-44/root/step_1/v_out_port	•••	Virtual output port	2018-08-24 13:34:44.949403	2018-08-24 13:34:44.949406	null	2117
4157	failing_network_2018-08-24T15-34-44/root/step_2/out_1	out_1	\N	2018-08-24 13:34:44.949871	2018-08-24 13:34:44.949874	{"datatype": "Int", "dimension_names": ["source_3"]}	2118
4158	failing_network_2018-08-24T15-34-44/root/step_2/out_2	out_2	\N	2018-08-24 13:34:44.950391	2018-08-24 13:34:44.950394	{"datatype": "Int", "dimension_names": ["source_3"]}	2118
4159	failing_network_2018-08-24T15-34-44/root/step_2/v_out_port	•••	Virtual output port	2018-08-24 13:34:44.950936	2018-08-24 13:34:44.95094	null	2118
4160	failing_network_2018-08-24T15-34-44/root/step_3/out_1	out_1	\N	2018-08-24 13:34:44.951412	2018-08-24 13:34:44.951415	{"datatype": "Int", "dimension_names": ["source_1"]}	2119
4161	failing_network_2018-08-24T15-34-44/root/step_3/out_2	out_2	\N	2018-08-24 13:34:44.951891	2018-08-24 13:34:44.951895	{"datatype": "Int", "dimension_names": ["source_1"]}	2119
4162	failing_network_2018-08-24T15-34-44/root/step_3/v_out_port	•••	Virtual output port	2018-08-24 13:34:44.9524	2018-08-24 13:34:44.952403	null	2119
4163	failing_network_2018-08-24T15-34-44/root/range/result	result	\N	2018-08-24 13:34:44.952918	2018-08-24 13:34:44.952922	{"datatype": "Int", "dimension_names": ["source_1"]}	2120
4164	failing_network_2018-08-24T15-34-44/root/range/v_out_port	•••	Virtual output port	2018-08-24 13:34:44.953494	2018-08-24 13:34:44.953498	null	2120
4165	failing_network_2018-08-24T15-34-44/root/sum/result	result	\N	2018-08-24 13:34:44.953992	2018-08-24 13:34:44.953996	{"datatype": "Int", "dimension_names": ["source_1"]}	2121
4166	failing_network_2018-08-24T15-34-44/root/sum/v_out_port	•••	Virtual output port	2018-08-24 13:34:44.954464	2018-08-24 13:34:44.954468	null	2121
4167	failing_network_2018-08-24T15-34-44/root/const_step_1_fail_2_0/output	output	\N	2018-08-24 13:34:44.954891	2018-08-24 13:34:44.954895	{"datatype": "Boolean", "dimension_names": ["const_step_1_fail_2_0"]}	2122
4168	failing_network_2018-08-24T15-34-44/root/const_step_1_fail_2_0/v_out_port	•••	Virtual output port	2018-08-24 13:34:44.955409	2018-08-24 13:34:44.955413	null	2122
4169	failing_network_2018-08-24T15-34-44/root/const_step_2_fail_1_0/output	output	\N	2018-08-24 13:34:44.955929	2018-08-24 13:34:44.955933	{"datatype": "Boolean", "dimension_names": ["const_step_2_fail_1_0"]}	2123
4170	failing_network_2018-08-24T15-34-44/root/const_step_2_fail_1_0/v_out_port	•••	Virtual output port	2018-08-24 13:34:44.956407	2018-08-24 13:34:44.956411	null	2123
4300	example_2018-09-03T12-31-11/root/source1/v_out_port	•••	Virtual output port	2018-09-03 10:31:12.131374	2018-09-03 10:31:12.131378	null	2201
4301	example_2018-09-03T12-31-11/root/sink1/output	output	\N	2018-09-03 10:31:12.1319	2018-09-03 10:31:12.131904	{"datatype": "Int", "dimension_names": []}	2202
4302	example_2018-09-03T12-31-11/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 10:31:12.132443	2018-09-03 10:31:12.132447	null	2202
4303	example_2018-09-03T12-31-11/root/addint/result	result	\N	2018-09-03 10:31:12.133134	2018-09-03 10:31:12.133138	{"datatype": "Int", "dimension_names": ["source1"]}	2203
4304	example_2018-09-03T12-31-11/root/addint/v_out_port	•••	Virtual output port	2018-09-03 10:31:12.133736	2018-09-03 10:31:12.13374	null	2203
4305	example_2018-09-03T12-31-11/root/const_addint_right_hand_0/output	output	\N	2018-09-03 10:31:12.134314	2018-09-03 10:31:12.134318	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2204
4306	example_2018-09-03T12-31-11/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 10:31:12.134809	2018-09-03 10:31:12.134812	null	2204
4307	example_2018-09-03T12-47-05/root/source1/output	output	\N	2018-09-03 10:47:06.277321	2018-09-03 10:47:06.277327	{"datatype": "Int", "dimension_names": ["source1"]}	2206
4308	example_2018-09-03T12-47-05/root/source1/v_out_port	•••	Virtual output port	2018-09-03 10:47:06.277895	2018-09-03 10:47:06.277899	null	2206
4309	example_2018-09-03T12-47-05/root/sink1/output	output	\N	2018-09-03 10:47:06.278501	2018-09-03 10:47:06.278505	{"datatype": "Int", "dimension_names": []}	2207
4310	example_2018-09-03T12-47-05/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 10:47:06.278962	2018-09-03 10:47:06.278965	null	2207
4311	example_2018-09-03T12-47-05/root/addint/result	result	\N	2018-09-03 10:47:06.279519	2018-09-03 10:47:06.279523	{"datatype": "Int", "dimension_names": ["source1"]}	2208
4312	example_2018-09-03T12-47-05/root/addint/v_out_port	•••	Virtual output port	2018-09-03 10:47:06.279976	2018-09-03 10:47:06.27998	null	2208
4313	example_2018-09-03T12-47-05/root/const_addint_right_hand_0/output	output	\N	2018-09-03 10:47:06.280424	2018-09-03 10:47:06.280427	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2209
4314	example_2018-09-03T12-47-05/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 10:47:06.280962	2018-09-03 10:47:06.280965	null	2209
4315	example_2018-09-03T12-54-57/root/source1/output	output	\N	2018-09-03 10:54:57.334656	2018-09-03 10:54:57.334661	{"datatype": "Int", "dimension_names": ["source1"]}	2211
4316	example_2018-09-03T12-54-57/root/source1/v_out_port	•••	Virtual output port	2018-09-03 10:54:57.335213	2018-09-03 10:54:57.335245	null	2211
4317	example_2018-09-03T12-54-57/root/sink1/output	output	\N	2018-09-03 10:54:57.33567	2018-09-03 10:54:57.335674	{"datatype": "Int", "dimension_names": []}	2212
4318	example_2018-09-03T12-54-57/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 10:54:57.336086	2018-09-03 10:54:57.33609	null	2212
4319	example_2018-09-03T12-54-57/root/addint/result	result	\N	2018-09-03 10:54:57.336609	2018-09-03 10:54:57.336613	{"datatype": "Int", "dimension_names": ["source1"]}	2213
4320	example_2018-09-03T12-54-57/root/addint/v_out_port	•••	Virtual output port	2018-09-03 10:54:57.337038	2018-09-03 10:54:57.337041	null	2213
4321	example_2018-09-03T12-54-57/root/const_addint_right_hand_0/output	output	\N	2018-09-03 10:54:57.337495	2018-09-03 10:54:57.337499	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2214
4322	example_2018-09-03T12-54-57/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 10:54:57.337974	2018-09-03 10:54:57.337978	null	2214
4323	example_2018-09-03T12-55-46/root/source1/output	output	\N	2018-09-03 10:55:47.001595	2018-09-03 10:55:47.0016	{"datatype": "Int", "dimension_names": ["source1"]}	2216
4324	example_2018-09-03T12-55-46/root/source1/v_out_port	•••	Virtual output port	2018-09-03 10:55:47.002081	2018-09-03 10:55:47.002085	null	2216
4325	example_2018-09-03T12-55-46/root/sink1/output	output	\N	2018-09-03 10:55:47.002568	2018-09-03 10:55:47.002572	{"datatype": "Int", "dimension_names": []}	2217
4326	example_2018-09-03T12-55-46/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 10:55:47.003002	2018-09-03 10:55:47.003006	null	2217
4327	example_2018-09-03T12-55-46/root/addint/result	result	\N	2018-09-03 10:55:47.003477	2018-09-03 10:55:47.003481	{"datatype": "Int", "dimension_names": ["source1"]}	2218
4328	example_2018-09-03T12-55-46/root/addint/v_out_port	•••	Virtual output port	2018-09-03 10:55:47.003922	2018-09-03 10:55:47.003926	null	2218
4329	example_2018-09-03T12-55-46/root/const_addint_right_hand_0/output	output	\N	2018-09-03 10:55:47.004376	2018-09-03 10:55:47.004379	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2219
4330	example_2018-09-03T12-55-46/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 10:55:47.004852	2018-09-03 10:55:47.004856	null	2219
4331	example_2018-09-03T13-09-35/root/sink1/output	output	\N	2018-09-03 11:09:36.241914	2018-09-03 11:09:36.241919	{"dimension_names": [], "datatype": "Int"}	2221
4332	example_2018-09-03T13-09-35/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 11:09:36.242456	2018-09-03 11:09:36.24246	null	2221
4354	example_2018-09-03T13-12-39/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 11:12:40.062341	2018-09-03 11:12:40.062347	null	2234
4355	example_2018-09-03T13-13-28/root/source1/output	output	\N	2018-09-03 11:13:28.418126	2018-09-03 11:13:28.418136	{"datatype": "Int", "dimension_names": ["source1"]}	2236
4356	example_2018-09-03T13-13-28/root/source1/v_out_port	•••	Virtual output port	2018-09-03 11:13:28.418745	2018-09-03 11:13:28.418749	null	2236
4357	example_2018-09-03T13-13-28/root/sink1/output	output	\N	2018-09-03 11:13:28.419304	2018-09-03 11:13:28.419308	{"datatype": "Int", "dimension_names": []}	2237
4358	example_2018-09-03T13-13-28/root/sink1/v_out_port	•••	Virtual output port	2018-09-03 11:13:28.419747	2018-09-03 11:13:28.419751	null	2237
4359	example_2018-09-03T13-13-28/root/addint/result	result	\N	2018-09-03 11:13:28.420232	2018-09-03 11:13:28.420236	{"datatype": "Int", "dimension_names": ["source1"]}	2238
4360	example_2018-09-03T13-13-28/root/addint/v_out_port	•••	Virtual output port	2018-09-03 11:13:28.420728	2018-09-03 11:13:28.420732	null	2238
4361	example_2018-09-03T13-13-28/root/const_addint_right_hand_0/output	output	\N	2018-09-03 11:13:28.421317	2018-09-03 11:13:28.42132	{"datatype": "Int", "dimension_names": ["const_addint_right_hand_0"]}	2239
4362	example_2018-09-03T13-13-28/root/const_addint_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 11:13:28.421794	2018-09-03 11:13:28.421798	null	2239
4363	failing_macro_top_level_2018-09-03T14-01-00/root/source_a/output	output	\N	2018-09-03 12:01:00.771847	2018-09-03 12:01:00.771853	{"datatype": "Int", "dimension_names": ["source_a"]}	2241
4364	failing_macro_top_level_2018-09-03T14-01-00/root/source_a/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.77243	2018-09-03 12:01:00.772451	null	2241
4365	failing_macro_top_level_2018-09-03T14-01-00/root/source_b/output	output	\N	2018-09-03 12:01:00.773005	2018-09-03 12:01:00.773008	{"datatype": "Int", "dimension_names": ["source_b"]}	2242
4366	failing_macro_top_level_2018-09-03T14-01-00/root/source_b/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.77345	2018-09-03 12:01:00.773453	null	2242
4367	failing_macro_top_level_2018-09-03T14-01-00/root/source_c/output	output	\N	2018-09-03 12:01:00.773888	2018-09-03 12:01:00.773892	{"datatype": "Int", "dimension_names": ["source_c"]}	2243
4368	failing_macro_top_level_2018-09-03T14-01-00/root/source_c/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.774342	2018-09-03 12:01:00.774346	null	2243
4369	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.774759	2018-09-03 12:01:00.774763	null	2244
4370	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1/output	output	\N	2018-09-03 12:01:00.775171	2018-09-03 12:01:00.775174	{"datatype": "Int", "dimension_names": ["source_1"]}	2248
4371	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_1/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.77555	2018-09-03 12:01:00.775553	null	2248
4372	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_2/output	output	\N	2018-09-03 12:01:00.775974	2018-09-03 12:01:00.775978	{"datatype": "Int", "dimension_names": ["source_2"]}	2249
4373	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_2/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.776459	2018-09-03 12:01:00.776463	null	2249
4374	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_3/output	output	\N	2018-09-03 12:01:00.776919	2018-09-03 12:01:00.776923	{"datatype": "Int", "dimension_names": ["source_3"]}	2250
4375	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/source_3/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.777318	2018-09-03 12:01:00.777322	null	2250
4376	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_1/output	output	\N	2018-09-03 12:01:00.777753	2018-09-03 12:01:00.777757	{"datatype": "Int", "dimension_names": []}	2251
4377	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_1/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.778156	2018-09-03 12:01:00.77816	null	2251
4378	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_2/output	output	\N	2018-09-03 12:01:00.778574	2018-09-03 12:01:00.778577	{"datatype": "Int", "dimension_names": []}	2252
4379	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_2/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.778984	2018-09-03 12:01:00.778987	null	2252
4380	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_3/output	output	\N	2018-09-03 12:01:00.779372	2018-09-03 12:01:00.779376	{"datatype": "Int", "dimension_names": []}	2253
4381	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_3/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.779811	2018-09-03 12:01:00.779815	null	2253
4382	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_4/output	output	\N	2018-09-03 12:01:00.780225	2018-09-03 12:01:00.780228	{"datatype": "Int", "dimension_names": []}	2254
4383	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_4/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.780667	2018-09-03 12:01:00.780671	null	2254
4384	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_5/output	output	\N	2018-09-03 12:01:00.781075	2018-09-03 12:01:00.781079	{"datatype": "Int", "dimension_names": []}	2255
4385	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sink_5/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.781456	2018-09-03 12:01:00.78146	null	2255
4386	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/out_1	out_1	\N	2018-09-03 12:01:00.781887	2018-09-03 12:01:00.781891	{"datatype": "Int", "dimension_names": ["source_1"]}	2256
4387	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/out_2	out_2	\N	2018-09-03 12:01:00.782285	2018-09-03 12:01:00.782289	{"datatype": "Int", "dimension_names": ["source_1"]}	2256
4388	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_1/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.782778	2018-09-03 12:01:00.782782	null	2256
4389	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/out_1	out_1	\N	2018-09-03 12:01:00.783187	2018-09-03 12:01:00.783191	{"datatype": "Int", "dimension_names": ["source_3"]}	2257
4390	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/out_2	out_2	\N	2018-09-03 12:01:00.783619	2018-09-03 12:01:00.783623	{"datatype": "Int", "dimension_names": ["source_3"]}	2257
4391	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_2/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.784033	2018-09-03 12:01:00.784037	null	2257
4392	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/out_1	out_1	\N	2018-09-03 12:01:00.784631	2018-09-03 12:01:00.784635	{"datatype": "Int", "dimension_names": ["source_1"]}	2258
4393	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/out_2	out_2	\N	2018-09-03 12:01:00.785053	2018-09-03 12:01:00.785057	{"datatype": "Int", "dimension_names": ["source_1"]}	2258
4394	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/step_3/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.785467	2018-09-03 12:01:00.78547	null	2258
4395	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range/result	result	\N	2018-09-03 12:01:00.785887	2018-09-03 12:01:00.785891	{"datatype": "Int", "dimension_names": ["source_1"]}	2259
4396	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/range/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.786317	2018-09-03 12:01:00.786321	null	2259
4397	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sum/result	result	\N	2018-09-03 12:01:00.786746	2018-09-03 12:01:00.78675	{"datatype": "Int", "dimension_names": ["source_1"]}	2260
4398	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/sum/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.78715	2018-09-03 12:01:00.787153	null	2260
4399	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_1_fail_2_0/output	output	\N	2018-09-03 12:01:00.78756	2018-09-03 12:01:00.787564	{"datatype": "Boolean", "dimension_names": ["const_step_1_fail_2_0"]}	2261
4400	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_1_fail_2_0/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.78805	2018-09-03 12:01:00.788054	null	2261
4401	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_2_fail_1_0/output	output	\N	2018-09-03 12:01:00.788433	2018-09-03 12:01:00.788437	{"datatype": "Boolean", "dimension_names": ["const_step_2_fail_1_0"]}	2262
4402	failing_macro_top_level_2018-09-03T14-01-00/root/failing_macro/const_step_2_fail_1_0/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.78902	2018-09-03 12:01:00.789025	null	2262
4403	failing_macro_top_level_2018-09-03T14-01-00/root/add/result	result	\N	2018-09-03 12:01:00.789453	2018-09-03 12:01:00.789457	{"datatype": "Int", "dimension_names": ["source_a"]}	2245
4404	failing_macro_top_level_2018-09-03T14-01-00/root/add/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.789911	2018-09-03 12:01:00.789915	null	2245
4405	failing_macro_top_level_2018-09-03T14-01-00/root/sink/output	output	\N	2018-09-03 12:01:00.790422	2018-09-03 12:01:00.790426	{"datatype": "Int", "dimension_names": []}	2246
4406	failing_macro_top_level_2018-09-03T14-01-00/root/sink/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.790863	2018-09-03 12:01:00.790867	null	2246
4407	failing_macro_top_level_2018-09-03T14-01-00/root/const_add_right_hand_0/output	output	\N	2018-09-03 12:01:00.79126	2018-09-03 12:01:00.791264	{"datatype": "Int", "dimension_names": ["const_add_right_hand_0"]}	2247
4408	failing_macro_top_level_2018-09-03T14-01-00/root/const_add_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 12:01:00.791701	2018-09-03 12:01:00.791705	null	2247
4409	add_ints_2018-09-03T14-04-33/root/source/output	output	\N	2018-09-03 12:04:33.332422	2018-09-03 12:04:33.332428	{"datatype": "Int", "dimension_names": ["source"]}	2264
4410	add_ints_2018-09-03T14-04-33/root/source/v_out_port	•••	Virtual output port	2018-09-03 12:04:33.33311	2018-09-03 12:04:33.333114	null	2264
4411	add_ints_2018-09-03T14-04-33/root/add/result	result	\N	2018-09-03 12:04:33.333589	2018-09-03 12:04:33.333593	{"datatype": "Int", "dimension_names": ["source"]}	2265
4412	add_ints_2018-09-03T14-04-33/root/add/v_out_port	•••	Virtual output port	2018-09-03 12:04:33.334112	2018-09-03 12:04:33.334117	null	2265
4413	add_ints_2018-09-03T14-04-33/root/const_add_right_hand_0/output	output	\N	2018-09-03 12:04:33.334604	2018-09-03 12:04:33.334618	{"datatype": "Int", "dimension_names": ["const_add_right_hand_0"]}	2266
4414	add_ints_2018-09-03T14-04-33/root/const_add_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 12:04:33.335117	2018-09-03 12:04:33.335121	null	2266
4415	add_ints_2018-09-03T14-04-33/root/sink/output	output	\N	2018-09-03 12:04:33.335617	2018-09-03 12:04:33.335622	{"datatype": "Int", "dimension_names": []}	2267
4416	add_ints_2018-09-03T14-04-33/root/sink/v_out_port	•••	Virtual output port	2018-09-03 12:04:33.336152	2018-09-03 12:04:33.336156	null	2267
4417	macro_node_2_2018-09-03T14-14-40/root/addint/result	result	\N	2018-09-03 12:14:40.507068	2018-09-03 12:14:40.507074	{"datatype": "Int", "dimension_names": ["source_1", "source_2"]}	2269
4418	macro_node_2_2018-09-03T14-14-40/root/addint/v_out_port	•••	Virtual output port	2018-09-03 12:14:40.507669	2018-09-03 12:14:40.507673	null	2269
4419	macro_node_2_2018-09-03T14-14-40/root/source_1/output	output	\N	2018-09-03 12:14:40.508132	2018-09-03 12:14:40.508136	{"datatype": "Int", "dimension_names": ["source_1"]}	2270
4420	macro_node_2_2018-09-03T14-14-40/root/source_1/v_out_port	•••	Virtual output port	2018-09-03 12:14:40.508529	2018-09-03 12:14:40.508533	null	2270
4421	macro_node_2_2018-09-03T14-14-40/root/source_2/output	output	\N	2018-09-03 12:14:40.508964	2018-09-03 12:14:40.508968	{"datatype": "Int", "dimension_names": ["source_2"]}	2271
4422	macro_node_2_2018-09-03T14-14-40/root/source_2/v_out_port	•••	Virtual output port	2018-09-03 12:14:40.509349	2018-09-03 12:14:40.509353	null	2271
4423	macro_node_2_2018-09-03T14-14-40/root/source_3/output	output	\N	2018-09-03 12:14:40.509799	2018-09-03 12:14:40.509803	{"datatype": "Int", "dimension_names": ["source_3"]}	2272
4424	macro_node_2_2018-09-03T14-14-40/root/source_3/v_out_port	•••	Virtual output port	2018-09-03 12:14:40.510354	2018-09-03 12:14:40.510357	null	2272
4425	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/v_out_port	•••	Virtual output port	2018-09-03 12:14:40.510809	2018-09-03 12:14:40.510813	null	2273
4426	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1/output	output	\N	2018-09-03 12:14:40.511241	2018-09-03 12:14:40.511245	{"datatype": "Int", "dimension_names": ["macro_input_1"]}	2276
4427	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_1/v_out_port	•••	Virtual output port	2018-09-03 12:14:40.511665	2018-09-03 12:14:40.511669	null	2276
4428	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_2/output	output	\N	2018-09-03 12:14:40.512055	2018-09-03 12:14:40.512059	{"datatype": "Int", "dimension_names": ["macro_input_2"]}	2277
4429	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_input_2/v_out_port	•••	Virtual output port	2018-09-03 12:14:40.512464	2018-09-03 12:14:40.512467	null	2277
4430	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/result	result	\N	2018-09-03 12:14:40.512878	2018-09-03 12:14:40.512899	{"datatype": "Int", "dimension_names": ["macro_input_1", "macro_input_2"]}	2278
4431	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/addint1/v_out_port	•••	Virtual output port	2018-09-03 12:14:40.513264	2018-09-03 12:14:40.513268	null	2278
4432	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/output	output	\N	2018-09-03 12:14:40.513673	2018-09-03 12:14:40.513677	{"datatype": "Int", "dimension_names": ["macro_input_1", "macro_input_2"]}	2279
4433	macro_node_2_2018-09-03T14-14-40/root/node_add_multiple_ints_1/macro_sink/v_out_port	•••	Virtual output port	2018-09-03 12:14:40.514247	2018-09-03 12:14:40.514251	null	2279
4434	macro_node_2_2018-09-03T14-14-40/root/sum/result	result	\N	2018-09-03 12:14:40.514733	2018-09-03 12:14:40.514737	{"datatype": "Int", "dimension_names": ["source_1", "source_3"]}	2274
4435	macro_node_2_2018-09-03T14-14-40/root/sum/v_out_port	•••	Virtual output port	2018-09-03 12:14:40.515323	2018-09-03 12:14:40.515327	null	2274
4436	macro_node_2_2018-09-03T14-14-40/root/sink/output	output	\N	2018-09-03 12:14:40.515917	2018-09-03 12:14:40.515921	{"datatype": "Int", "dimension_names": ["source_1", "source_3"]}	2275
4437	macro_node_2_2018-09-03T14-14-40/root/sink/v_out_port	•••	Virtual output port	2018-09-03 12:14:40.516571	2018-09-03 12:14:40.516574	null	2275
4438	add_ints_2018-09-03T14-18-44/root/source/output	output	\N	2018-09-03 12:18:44.65825	2018-09-03 12:18:44.658258	{"datatype": "Int", "dimension_names": ["source"]}	2281
4439	add_ints_2018-09-03T14-18-44/root/source/v_out_port	•••	Virtual output port	2018-09-03 12:18:44.658912	2018-09-03 12:18:44.658931	null	2281
4440	add_ints_2018-09-03T14-18-44/root/add/result	result	\N	2018-09-03 12:18:44.659653	2018-09-03 12:18:44.659657	{"datatype": "Int", "dimension_names": ["source"]}	2282
4441	add_ints_2018-09-03T14-18-44/root/add/v_out_port	•••	Virtual output port	2018-09-03 12:18:44.660225	2018-09-03 12:18:44.660229	null	2282
4442	add_ints_2018-09-03T14-18-44/root/const_add_right_hand_0/output	output	\N	2018-09-03 12:18:44.660803	2018-09-03 12:18:44.660807	{"datatype": "Int", "dimension_names": ["const_add_right_hand_0"]}	2283
4443	add_ints_2018-09-03T14-18-44/root/const_add_right_hand_0/v_out_port	•••	Virtual output port	2018-09-03 12:18:44.661497	2018-09-03 12:18:44.661501	null	2283
4444	add_ints_2018-09-03T14-18-44/root/sink/output	output	\N	2018-09-03 12:18:44.662055	2018-09-03 12:18:44.662058	{"datatype": "Int", "dimension_names": []}	2284
4445	add_ints_2018-09-03T14-18-44/root/sink/v_out_port	•••	Virtual output port	2018-09-03 12:18:44.662475	2018-09-03 12:18:44.662479	null	2284
4446	auto_prefix_test_2018-09-03T14-30-01/root/const/output	output	\N	2018-09-03 12:30:02.229927	2018-09-03 12:30:02.229932	{"datatype": "Int", "dimension_names": ["const"]}	2286
4447	auto_prefix_test_2018-09-03T14-30-01/root/const/v_out_port	•••	Virtual output port	2018-09-03 12:30:02.230456	2018-09-03 12:30:02.23046	null	2286
4448	auto_prefix_test_2018-09-03T14-30-01/root/source/output	output	\N	2018-09-03 12:30:02.231007	2018-09-03 12:30:02.231011	{"datatype": "Int", "dimension_names": ["source"]}	2287
4449	auto_prefix_test_2018-09-03T14-30-01/root/source/v_out_port	•••	Virtual output port	2018-09-03 12:30:02.231474	2018-09-03 12:30:02.231478	null	2287
4450	auto_prefix_test_2018-09-03T14-30-01/root/m_p/added	added	\N	2018-09-03 12:30:02.232092	2018-09-03 12:30:02.232096	{"datatype": "Int", "dimension_names": ["source"]}	2288
4451	auto_prefix_test_2018-09-03T14-30-01/root/m_p/multiplied	multiplied	\N	2018-09-03 12:30:02.232722	2018-09-03 12:30:02.232727	{"datatype": "Int", "dimension_names": ["source"]}	2288
4452	auto_prefix_test_2018-09-03T14-30-01/root/m_p/v_out_port	•••	Virtual output port	2018-09-03 12:30:02.23322	2018-09-03 12:30:02.233224	null	2288
4453	auto_prefix_test_2018-09-03T14-30-01/root/a_p/added	added	\N	2018-09-03 12:30:02.233717	2018-09-03 12:30:02.233721	{"datatype": "Int", "dimension_names": ["source"]}	2289
4454	auto_prefix_test_2018-09-03T14-30-01/root/a_p/multiplied	multiplied	\N	2018-09-03 12:30:02.234221	2018-09-03 12:30:02.234225	{"datatype": "Int", "dimension_names": ["source"]}	2289
4455	auto_prefix_test_2018-09-03T14-30-01/root/a_p/v_out_port	•••	Virtual output port	2018-09-03 12:30:02.234702	2018-09-03 12:30:02.234706	null	2289
4456	auto_prefix_test_2018-09-03T14-30-01/root/m_n/added	added	\N	2018-09-03 12:30:02.235182	2018-09-03 12:30:02.235186	{"datatype": "Int", "dimension_names": ["source"]}	2290
4457	auto_prefix_test_2018-09-03T14-30-01/root/m_n/multiplied	multiplied	\N	2018-09-03 12:30:02.235672	2018-09-03 12:30:02.235676	{"datatype": "Int", "dimension_names": ["source"]}	2290
4458	auto_prefix_test_2018-09-03T14-30-01/root/m_n/v_out_port	•••	Virtual output port	2018-09-03 12:30:02.236184	2018-09-03 12:30:02.236187	null	2290
4459	auto_prefix_test_2018-09-03T14-30-01/root/a_n/added	added	\N	2018-09-03 12:30:02.23664	2018-09-03 12:30:02.236644	{"datatype": "Int", "dimension_names": ["source"]}	2291
4460	auto_prefix_test_2018-09-03T14-30-01/root/a_n/multiplied	multiplied	\N	2018-09-03 12:30:02.237155	2018-09-03 12:30:02.237159	{"datatype": "Int", "dimension_names": ["source"]}	2291
4461	auto_prefix_test_2018-09-03T14-30-01/root/a_n/v_out_port	•••	Virtual output port	2018-09-03 12:30:02.237658	2018-09-03 12:30:02.237662	null	2291
4462	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_p/output	output	\N	2018-09-03 12:30:02.238136	2018-09-03 12:30:02.23814	{"datatype": "Int", "dimension_names": []}	2292
4463	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_p/v_out_port	•••	Virtual output port	2018-09-03 12:30:02.23863	2018-09-03 12:30:02.238634	null	2292
4464	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_p/output	output	\N	2018-09-03 12:30:02.239156	2018-09-03 12:30:02.239159	{"datatype": "Int", "dimension_names": []}	2293
4465	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_p/v_out_port	•••	Virtual output port	2018-09-03 12:30:02.239696	2018-09-03 12:30:02.2397	null	2293
4466	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_n/output	output	\N	2018-09-03 12:30:02.240311	2018-09-03 12:30:02.240315	{"datatype": "Int", "dimension_names": []}	2294
4467	auto_prefix_test_2018-09-03T14-30-01/root/sink_m_n/v_out_port	•••	Virtual output port	2018-09-03 12:30:02.240875	2018-09-03 12:30:02.240879	null	2294
4468	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_n/output	output	\N	2018-09-03 12:30:02.241416	2018-09-03 12:30:02.24142	{"datatype": "Int", "dimension_names": []}	2295
4469	auto_prefix_test_2018-09-03T14-30-01/root/sink_a_n/v_out_port	•••	Virtual output port	2018-09-03 12:30:02.241988	2018-09-03 12:30:02.242002	null	2295
4470	add_ints_2018-09-06T13-57-24/root/source/output	output	\N	2018-09-06 11:57:24.893567	2018-09-06 11:57:24.893573	{"dimension_names": ["source"], "datatype": "Int"}	2297
4471	add_ints_2018-09-06T13-57-24/root/source/v_out_port	•••	Virtual output port	2018-09-06 11:57:24.894804	2018-09-06 11:57:24.894808	null	2297
4472	add_ints_2018-09-06T13-57-24/root/const_add_right_hand_0/output	output	\N	2018-09-06 11:57:24.895381	2018-09-06 11:57:24.895385	{"dimension_names": ["const_add_right_hand_0"], "datatype": "Int"}	2298
4473	add_ints_2018-09-06T13-57-24/root/const_add_right_hand_0/v_out_port	•••	Virtual output port	2018-09-06 11:57:24.895905	2018-09-06 11:57:24.895908	null	2298
4474	add_ints_2018-09-06T13-57-24/root/sink/output	output	\N	2018-09-06 11:57:24.896479	2018-09-06 11:57:24.896483	{"dimension_names": [], "datatype": "Int"}	2299
4475	add_ints_2018-09-06T13-57-24/root/sink/v_out_port	•••	Virtual output port	2018-09-06 11:57:24.89714	2018-09-06 11:57:24.897144	null	2299
4476	add_ints_2018-09-06T13-57-24/root/add/result	result	\N	2018-09-06 11:57:24.897634	2018-09-06 11:57:24.897638	{"dimension_names": ["source"], "datatype": "Int"}	2300
4477	add_ints_2018-09-06T13-57-24/root/add/v_out_port	•••	Virtual output port	2018-09-06 11:57:24.898082	2018-09-06 11:57:24.898086	null	2300
4478	failing_network_2018-09-06T14-00-46/root/step_3/out_1	out_1	\N	2018-09-06 12:00:46.565447	2018-09-06 12:00:46.565453	{"dimension_names": ["source_1"], "datatype": "Int"}	2302
4479	failing_network_2018-09-06T14-00-46/root/step_3/out_2	out_2	\N	2018-09-06 12:00:46.566012	2018-09-06 12:00:46.566015	{"dimension_names": ["source_1"], "datatype": "Int"}	2302
4480	failing_network_2018-09-06T14-00-46/root/step_3/v_out_port	•••	Virtual output port	2018-09-06 12:00:46.566528	2018-09-06 12:00:46.566531	null	2302
4481	failing_network_2018-09-06T14-00-46/root/const_step_2_fail_1_0/output	output	\N	2018-09-06 12:00:46.566978	2018-09-06 12:00:46.566981	{"dimension_names": ["const_step_2_fail_1_0"], "datatype": "Boolean"}	2303
4482	failing_network_2018-09-06T14-00-46/root/const_step_2_fail_1_0/v_out_port	•••	Virtual output port	2018-09-06 12:00:46.567469	2018-09-06 12:00:46.567473	null	2303
4483	failing_network_2018-09-06T14-00-46/root/sink_4/output	output	\N	2018-09-06 12:00:46.567935	2018-09-06 12:00:46.567938	{"dimension_names": [], "datatype": "Int"}	2304
4484	failing_network_2018-09-06T14-00-46/root/sink_4/v_out_port	•••	Virtual output port	2018-09-06 12:00:46.568472	2018-09-06 12:00:46.568476	null	2304
4485	failing_network_2018-09-06T14-00-46/root/source_3/output	output	\N	2018-09-06 12:00:46.568929	2018-09-06 12:00:46.568933	{"dimension_names": ["source_3"], "datatype": "Int"}	2305
4486	failing_network_2018-09-06T14-00-46/root/source_3/v_out_port	•••	Virtual output port	2018-09-06 12:00:46.569388	2018-09-06 12:00:46.569391	null	2305
4487	failing_network_2018-09-06T14-00-46/root/step_1/out_1	out_1	\N	2018-09-06 12:00:46.569816	2018-09-06 12:00:46.569819	{"dimension_names": ["source_1"], "datatype": "Int"}	2306
4488	failing_network_2018-09-06T14-00-46/root/step_1/out_2	out_2	\N	2018-09-06 12:00:46.570226	2018-09-06 12:00:46.57023	{"dimension_names": ["source_1"], "datatype": "Int"}	2306
4489	failing_network_2018-09-06T14-00-46/root/step_1/v_out_port	•••	Virtual output port	2018-09-06 12:00:46.570652	2018-09-06 12:00:46.570655	null	2306
4490	failing_network_2018-09-06T14-00-46/root/const_step_1_fail_2_0/output	output	\N	2018-09-06 12:00:46.571104	2018-09-06 12:00:46.571108	{"dimension_names": ["const_step_1_fail_2_0"], "datatype": "Boolean"}	2307
4491	failing_network_2018-09-06T14-00-46/root/const_step_1_fail_2_0/v_out_port	•••	Virtual output port	2018-09-06 12:00:46.571527	2018-09-06 12:00:46.571531	null	2307
4492	failing_network_2018-09-06T14-00-46/root/step_2/out_1	out_1	\N	2018-09-06 12:00:46.572051	2018-09-06 12:00:46.572054	{"dimension_names": ["source_3"], "datatype": "Int"}	2308
4493	failing_network_2018-09-06T14-00-46/root/step_2/out_2	out_2	\N	2018-09-06 12:00:46.57256	2018-09-06 12:00:46.572564	{"dimension_names": ["source_3"], "datatype": "Int"}	2308
4494	failing_network_2018-09-06T14-00-46/root/step_2/v_out_port	•••	Virtual output port	2018-09-06 12:00:46.573081	2018-09-06 12:00:46.573085	null	2308
4495	failing_network_2018-09-06T14-00-46/root/sink_3/output	output	\N	2018-09-06 12:00:46.573696	2018-09-06 12:00:46.5737	{"dimension_names": [], "datatype": "Int"}	2309
4496	failing_network_2018-09-06T14-00-46/root/sink_3/v_out_port	•••	Virtual output port	2018-09-06 12:00:46.574157	2018-09-06 12:00:46.57416	null	2309
4497	failing_network_2018-09-06T14-00-46/root/range/result	result	\N	2018-09-06 12:00:46.574645	2018-09-06 12:00:46.574649	{"dimension_names": ["source_1"], "datatype": "Int"}	2310
4498	failing_network_2018-09-06T14-00-46/root/range/v_out_port	•••	Virtual output port	2018-09-06 12:00:46.575085	2018-09-06 12:00:46.575089	null	2310
4499	failing_network_2018-09-06T14-00-46/root/sum/result	result	\N	2018-09-06 12:00:46.575582	2018-09-06 12:00:46.575585	{"dimension_names": ["source_1"], "datatype": "Int"}	2311
4500	failing_network_2018-09-06T14-00-46/root/sum/v_out_port	•••	Virtual output port	2018-09-06 12:00:46.576007	2018-09-06 12:00:46.576011	null	2311
4501	failing_network_2018-09-06T14-00-46/root/sink_1/output	output	\N	2018-09-06 12:00:46.576448	2018-09-06 12:00:46.576452	{"dimension_names": [], "datatype": "Int"}	2312
4502	failing_network_2018-09-06T14-00-46/root/sink_1/v_out_port	•••	Virtual output port	2018-09-06 12:00:46.576983	2018-09-06 12:00:46.576986	null	2312
4503	failing_network_2018-09-06T14-00-46/root/source_2/output	output	\N	2018-09-06 12:00:46.577623	2018-09-06 12:00:46.577626	{"dimension_names": ["source_2"], "datatype": "Int"}	2313
4504	failing_network_2018-09-06T14-00-46/root/source_2/v_out_port	•••	Virtual output port	2018-09-06 12:00:46.578151	2018-09-06 12:00:46.578155	null	2313
4505	failing_network_2018-09-06T14-00-46/root/sink_2/output	output	\N	2018-09-06 12:00:46.578646	2018-09-06 12:00:46.578649	{"dimension_names": [], "datatype": "Int"}	2314
4506	failing_network_2018-09-06T14-00-46/root/sink_2/v_out_port	•••	Virtual output port	2018-09-06 12:00:46.579091	2018-09-06 12:00:46.579095	null	2314
4507	failing_network_2018-09-06T14-00-46/root/sink_5/output	output	\N	2018-09-06 12:00:46.579614	2018-09-06 12:00:46.579618	{"dimension_names": [], "datatype": "Int"}	2315
4508	failing_network_2018-09-06T14-00-46/root/sink_5/v_out_port	•••	Virtual output port	2018-09-06 12:00:46.580088	2018-09-06 12:00:46.580092	null	2315
4509	failing_network_2018-09-06T14-00-46/root/source_1/output	output	\N	2018-09-06 12:00:46.580552	2018-09-06 12:00:46.580556	{"dimension_names": ["source_1"], "datatype": "Int"}	2316
4510	failing_network_2018-09-06T14-00-46/root/source_1/v_out_port	•••	Virtual output port	2018-09-06 12:00:46.581022	2018-09-06 12:00:46.581026	null	2316
4511	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.113495	2018-09-06 13:00:59.113502	null	2318
4512	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Num_Of_Iterations/output	output	\N	2018-09-06 13:00:59.115387	2018-09-06 13:00:59.115391	{"datatype": "String", "dimension_names": ["Num_Of_Iterations"]}	2330
4513	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Num_Of_Iterations/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.116036	2018-09-06 13:00:59.116041	null	2330
4514	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Step_Size/output	output	\N	2018-09-06 13:00:59.1166	2018-09-06 13:00:59.116604	{"datatype": "String", "dimension_names": ["Step_Size"]}	2331
4515	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Step_Size/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.117113	2018-09-06 13:00:59.117117	null	2331
4516	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Blur_FWHM/output	output	\N	2018-09-06 13:00:59.117609	2018-09-06 13:00:59.117612	{"datatype": "String", "dimension_names": ["Blur_FWHM"]}	2332
4517	IntracranialVolume_2018-09-06T15-00-57/root/Program_Basic_Arguments/Blur_FWHM/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.118102	2018-09-06 13:00:59.118106	null	2332
4518	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Linear_Registration_Arguments/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.118559	2018-09-06 13:00:59.118563	null	2319
4519	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Linear_Registration_Arguments/Linear_Normalization/output	output	\N	2018-09-06 13:00:59.118999	2018-09-06 13:00:59.119002	{"datatype": "Boolean", "dimension_names": ["Linear_Normalization"]}	2333
4520	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Linear_Registration_Arguments/Linear_Normalization/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.119464	2018-09-06 13:00:59.119467	null	2333
4521	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.119906	2018-09-06 13:00:59.11991	null	2320
4522	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Linear_Estimate/output	output	\N	2018-09-06 13:00:59.120349	2018-09-06 13:00:59.120353	{"datatype": "String", "dimension_names": ["Linear_Estimate"]}	2334
4523	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Linear_Estimate/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.120915	2018-09-06 13:00:59.120919	null	2334
4524	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Num_Of_Registrations/output	output	\N	2018-09-06 13:00:59.121429	2018-09-06 13:00:59.121433	{"datatype": "Int", "dimension_names": ["Num_Of_Registrations"]}	2335
4525	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Num_Of_Registrations/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.121928	2018-09-06 13:00:59.121931	null	2335
4526	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Registration_Blur_FWHM/output	output	\N	2018-09-06 13:00:59.122434	2018-09-06 13:00:59.122438	{"datatype": "Int", "dimension_names": ["Registration_Blur_FWHM"]}	2336
4527	IntracranialVolume_2018-09-06T15-00-57/root/Program_Linear_Registration_Arguments/Registration_Blur_FWHM/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.122873	2018-09-06 13:00:59.122877	null	2336
4528	IntracranialVolume_2018-09-06T15-00-57/root/Subject_Name/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.123331	2018-09-06 13:00:59.123334	null	2321
4529	IntracranialVolume_2018-09-06T15-00-57/root/Subject_Name/ICV_Subject_Name/output	output	\N	2018-09-06 13:00:59.123779	2018-09-06 13:00:59.123783	{"datatype": "String", "dimension_names": ["ICV_Subject_Name"]}	2337
4530	IntracranialVolume_2018-09-06T15-00-57/root/Subject_Name/ICV_Subject_Name/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.124444	2018-09-06 13:00:59.124448	null	2337
4531	IntracranialVolume_2018-09-06T15-00-57/root/Model_Name/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.125056	2018-09-06 13:00:59.12506	null	2322
4532	IntracranialVolume_2018-09-06T15-00-57/root/Model_Name/Model/output	output	\N	2018-09-06 13:00:59.125646	2018-09-06 13:00:59.125665	{"datatype": "String", "dimension_names": ["Model"]}	2338
4533	IntracranialVolume_2018-09-06T15-00-57/root/Model_Name/Model/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.126253	2018-09-06 13:00:59.126257	null	2338
4534	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.126684	2018-09-06 13:00:59.126688	null	2323
4535	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Weight/output	output	\N	2018-09-06 13:00:59.127176	2018-09-06 13:00:59.12718	{"datatype": "Float", "dimension_names": ["Weight"]}	2339
4536	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Weight/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.127695	2018-09-06 13:00:59.127699	null	2339
4623	add_ints_2018-09-07T12-00-02/root/add/result	result	\N	2018-09-07 10:00:02.677531	2018-09-07 10:00:02.677535	{"datatype": "Int", "dimension_names": ["source"]}	2377
4537	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Stiffness/output	output	\N	2018-09-06 13:00:59.128408	2018-09-06 13:00:59.128412	{"datatype": "Float", "dimension_names": ["Stiffness"]}	2340
4538	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Stiffness/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.129167	2018-09-06 13:00:59.129188	null	2340
4539	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Similarity/output	output	\N	2018-09-06 13:00:59.129704	2018-09-06 13:00:59.129708	{"datatype": "Float", "dimension_names": ["Similarity"]}	2341
4540	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Similarity/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.130288	2018-09-06 13:00:59.130292	null	2341
4541	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Sub_lattice/output	output	\N	2018-09-06 13:00:59.130717	2018-09-06 13:00:59.130721	{"datatype": "Int", "dimension_names": ["Sub_lattice"]}	2342
4542	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Sub_lattice/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.13127	2018-09-06 13:00:59.131273	null	2342
4543	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Debug/output	output	\N	2018-09-06 13:00:59.131765	2018-09-06 13:00:59.131769	{"datatype": "Boolean", "dimension_names": ["Debug"]}	2343
4544	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Debug/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.132308	2018-09-06 13:00:59.132312	null	2343
4545	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Clobber/output	output	\N	2018-09-06 13:00:59.132801	2018-09-06 13:00:59.132805	{"datatype": "Boolean", "dimension_names": ["Clobber"]}	2344
4546	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Clobber/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.133379	2018-09-06 13:00:59.133383	null	2344
4547	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Non_linear/output	output	\N	2018-09-06 13:00:59.133936	2018-09-06 13:00:59.13394	{"datatype": "String", "dimension_names": ["Non_linear"]}	2345
4548	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Non_linear/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.134393	2018-09-06 13:00:59.134397	null	2345
4549	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Mask_To_Binary_Min/output	output	\N	2018-09-06 13:00:59.134878	2018-09-06 13:00:59.134882	{"datatype": "Float", "dimension_names": ["Mask_To_Binary_Min"]}	2346
4550	IntracranialVolume_2018-09-06T15-00-57/root/Program_Extended_Arguments/Mask_To_Binary_Min/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.135459	2018-09-06 13:00:59.135463	null	2346
4551	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.136051	2018-09-06 13:00:59.136055	null	2324
4552	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Mask/output	output	\N	2018-09-06 13:00:59.136533	2018-09-06 13:00:59.136537	{"datatype": "NiftiImageFileCompressed", "dimension_names": []}	2347
4553	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Mask/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.137018	2018-09-06 13:00:59.137021	null	2347
4554	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Parameters/output	output	\N	2018-09-06 13:00:59.137451	2018-09-06 13:00:59.137455	{"datatype": "TxtFile", "dimension_names": []}	2348
4555	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Parameters/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.137942	2018-09-06 13:00:59.137946	null	2348
4556	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Volume/output	output	\N	2018-09-06 13:00:59.138426	2018-09-06 13:00:59.13843	{"datatype": "TxtFile", "dimension_names": []}	2349
4557	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_Volume/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.138947	2018-09-06 13:00:59.13895	null	2349
4558	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_Log/output	output	\N	2018-09-06 13:00:59.139428	2018-09-06 13:00:59.139432	{"datatype": "TxtFile", "dimension_names": []}	2350
4559	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_Log/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.13991	2018-09-06 13:00:59.139914	null	2350
4560	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Original_Subject/output	output	\N	2018-09-06 13:00:59.140393	2018-09-06 13:00:59.140397	{"datatype": "NiftiImageFileCompressed", "dimension_names": []}	2351
4561	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Original_Subject/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.14082	2018-09-06 13:00:59.140824	null	2351
4562	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_JPG/output	output	\N	2018-09-06 13:00:59.141235	2018-09-06 13:00:59.141239	{"datatype": "JPGFile", "dimension_names": []}	2352
4563	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Files_Outputs/ICV_Output_ICV_JPG/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.14164	2018-09-06 13:00:59.141644	null	2352
4564	IntracranialVolume_2018-09-06T15-00-57/root/Delete_Files/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.142061	2018-09-06 13:00:59.142065	null	2325
4565	IntracranialVolume_2018-09-06T15-00-57/root/Delete_Files/Delete_Files/output	output	\N	2018-09-06 13:00:59.142498	2018-09-06 13:00:59.142501	{"datatype": "Boolean", "dimension_names": ["Delete_Files"]}	2353
4566	IntracranialVolume_2018-09-06T15-00-57/root/Delete_Files/Delete_Files/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.143018	2018-09-06 13:00:59.143021	null	2353
4567	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.143476	2018-09-06 13:00:59.14348	null	2326
4568	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/icv_directory	icv_directory	\N	2018-09-06 13:00:59.143989	2018-09-06 13:00:59.143996	{"datatype": "Directory", "dimension_names": ["Delete_Files"]}	2354
4569	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/icv_output_nii_mask	icv_output_nii_mask	\N	2018-09-06 13:00:59.14449	2018-09-06 13:00:59.144494	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["Delete_Files"]}	2354
4570	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/icv_output_mnc_mask	icv_output_mnc_mask	\N	2018-09-06 13:00:59.144962	2018-09-06 13:00:59.144966	{"datatype": "CompressedMincImageFile", "dimension_names": ["Delete_Files"]}	2354
4571	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/icv_volume	icv_volume	\N	2018-09-06 13:00:59.145419	2018-09-06 13:00:59.145423	{"datatype": "TxtFile", "dimension_names": ["Delete_Files"]}	2354
4572	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/icv_parameters_used	icv_parameters_used	\N	2018-09-06 13:00:59.145834	2018-09-06 13:00:59.145838	{"datatype": "TxtFile", "dimension_names": ["Delete_Files"]}	2354
4573	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/icv_log	icv_log	\N	2018-09-06 13:00:59.146317	2018-09-06 13:00:59.146321	{"datatype": "TxtFile", "dimension_names": ["Delete_Files"]}	2354
4574	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/icv_jpg	icv_jpg	\N	2018-09-06 13:00:59.146718	2018-09-06 13:00:59.146722	{"datatype": "JPGFile", "dimension_names": ["Delete_Files"]}	2354
4575	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/icv_original_file_In_nii	icv_original_file_In_nii	\N	2018-09-06 13:00:59.147115	2018-09-06 13:00:59.147119	{"datatype": "NiftiImageFileCompressed", "dimension_names": ["Delete_Files"]}	2354
4627	iris/root/0_statistics/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.04486	2018-09-08 13:18:13.044884	null	2380
4576	IntracranialVolume_2018-09-06T15-00-57/root/Main_Operation/IntracranialVolume/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.147563	2018-09-06 13:00:59.147569	null	2354
4577	IntracranialVolume_2018-09-06T15-00-57/root/Operation_Type/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.148231	2018-09-06 13:00:59.148235	null	2327
4578	IntracranialVolume_2018-09-06T15-00-57/root/Operation_Type/Calc_Crude/output	output	\N	2018-09-06 13:00:59.148666	2018-09-06 13:00:59.148669	{"datatype": "Boolean", "dimension_names": ["Calc_Crude"]}	2355
4579	IntracranialVolume_2018-09-06T15-00-57/root/Operation_Type/Calc_Crude/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.149083	2018-09-06 13:00:59.149087	null	2355
4580	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Extended_Arguments/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.149499	2018-09-06 13:00:59.149502	null	2328
4581	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Extended_Arguments/NonLinear_Normalization/output	output	\N	2018-09-06 13:00:59.149935	2018-09-06 13:00:59.149939	{"datatype": "Boolean", "dimension_names": ["NonLinear_Normalization"]}	2356
4582	IntracranialVolume_2018-09-06T15-00-57/root/ICV_Program_Extended_Arguments/NonLinear_Normalization/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.150425	2018-09-06 13:00:59.150429	null	2356
4583	IntracranialVolume_2018-09-06T15-00-57/root/Program_File_Arguments/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.150927	2018-09-06 13:00:59.150931	null	2329
4584	IntracranialVolume_2018-09-06T15-00-57/root/Program_File_Arguments/Input_To_Process/output	output	\N	2018-09-06 13:00:59.15137	2018-09-06 13:00:59.151374	{"datatype": "DicomImageFile", "dimension_names": ["Input_To_Process"]}	2357
4585	IntracranialVolume_2018-09-06T15-00-57/root/Program_File_Arguments/Input_To_Process/v_out_port	•••	Virtual output port	2018-09-06 13:00:59.151864	2018-09-06 13:00:59.151868	null	2357
4586	failing_network_2018-09-06T15-03-16/root/sink_4/output	output	\N	2018-09-06 13:03:16.719053	2018-09-06 13:03:16.71906	{"dimension_names": [], "datatype": "Int"}	2359
4587	failing_network_2018-09-06T15-03-16/root/sink_4/v_out_port	•••	Virtual output port	2018-09-06 13:03:16.720032	2018-09-06 13:03:16.720038	null	2359
4588	failing_network_2018-09-06T15-03-16/root/sink_2/output	output	\N	2018-09-06 13:03:16.720751	2018-09-06 13:03:16.720758	{"dimension_names": [], "datatype": "Int"}	2360
4589	failing_network_2018-09-06T15-03-16/root/sink_2/v_out_port	•••	Virtual output port	2018-09-06 13:03:16.721428	2018-09-06 13:03:16.721434	null	2360
4590	failing_network_2018-09-06T15-03-16/root/sum/result	result	\N	2018-09-06 13:03:16.721996	2018-09-06 13:03:16.722002	{"dimension_names": ["source_1"], "datatype": "Int"}	2361
4591	failing_network_2018-09-06T15-03-16/root/sum/v_out_port	•••	Virtual output port	2018-09-06 13:03:16.722608	2018-09-06 13:03:16.722614	null	2361
4592	failing_network_2018-09-06T15-03-16/root/const_step_1_fail_2_0/output	output	\N	2018-09-06 13:03:16.723187	2018-09-06 13:03:16.723204	{"dimension_names": ["const_step_1_fail_2_0"], "datatype": "Boolean"}	2362
4593	failing_network_2018-09-06T15-03-16/root/const_step_1_fail_2_0/v_out_port	•••	Virtual output port	2018-09-06 13:03:16.723807	2018-09-06 13:03:16.723813	null	2362
4594	failing_network_2018-09-06T15-03-16/root/range/result	result	\N	2018-09-06 13:03:16.724384	2018-09-06 13:03:16.72439	{"dimension_names": ["source_1"], "datatype": "Int"}	2363
4595	failing_network_2018-09-06T15-03-16/root/range/v_out_port	•••	Virtual output port	2018-09-06 13:03:16.725131	2018-09-06 13:03:16.725137	null	2363
4596	failing_network_2018-09-06T15-03-16/root/source_1/output	output	\N	2018-09-06 13:03:16.725769	2018-09-06 13:03:16.725775	{"dimension_names": ["source_1"], "datatype": "Int"}	2364
4597	failing_network_2018-09-06T15-03-16/root/source_1/v_out_port	•••	Virtual output port	2018-09-06 13:03:16.726444	2018-09-06 13:03:16.72645	null	2364
4598	failing_network_2018-09-06T15-03-16/root/const_step_2_fail_1_0/output	output	\N	2018-09-06 13:03:16.727106	2018-09-06 13:03:16.727112	{"dimension_names": ["const_step_2_fail_1_0"], "datatype": "Boolean"}	2365
4599	failing_network_2018-09-06T15-03-16/root/const_step_2_fail_1_0/v_out_port	•••	Virtual output port	2018-09-06 13:03:16.727723	2018-09-06 13:03:16.727729	null	2365
4600	failing_network_2018-09-06T15-03-16/root/step_2/out_1	out_1	\N	2018-09-06 13:03:16.728412	2018-09-06 13:03:16.728419	{"dimension_names": ["source_3"], "datatype": "Int"}	2366
4601	failing_network_2018-09-06T15-03-16/root/step_2/out_2	out_2	\N	2018-09-06 13:03:16.729013	2018-09-06 13:03:16.729018	{"dimension_names": ["source_3"], "datatype": "Int"}	2366
4602	failing_network_2018-09-06T15-03-16/root/step_2/v_out_port	•••	Virtual output port	2018-09-06 13:03:16.729631	2018-09-06 13:03:16.729638	null	2366
4603	failing_network_2018-09-06T15-03-16/root/source_3/output	output	\N	2018-09-06 13:03:16.730298	2018-09-06 13:03:16.730304	{"dimension_names": ["source_3"], "datatype": "Int"}	2367
4604	failing_network_2018-09-06T15-03-16/root/source_3/v_out_port	•••	Virtual output port	2018-09-06 13:03:16.730872	2018-09-06 13:03:16.730877	null	2367
4605	failing_network_2018-09-06T15-03-16/root/sink_3/output	output	\N	2018-09-06 13:03:16.731465	2018-09-06 13:03:16.731471	{"dimension_names": [], "datatype": "Int"}	2368
4606	failing_network_2018-09-06T15-03-16/root/sink_3/v_out_port	•••	Virtual output port	2018-09-06 13:03:16.732108	2018-09-06 13:03:16.732133	null	2368
4607	failing_network_2018-09-06T15-03-16/root/sink_5/output	output	\N	2018-09-06 13:03:16.732726	2018-09-06 13:03:16.732732	{"dimension_names": [], "datatype": "Int"}	2369
4608	failing_network_2018-09-06T15-03-16/root/sink_5/v_out_port	•••	Virtual output port	2018-09-06 13:03:16.733457	2018-09-06 13:03:16.733463	null	2369
4609	failing_network_2018-09-06T15-03-16/root/step_1/out_1	out_1	\N	2018-09-06 13:03:16.734189	2018-09-06 13:03:16.734212	{"dimension_names": ["source_1"], "datatype": "Int"}	2370
4610	failing_network_2018-09-06T15-03-16/root/step_1/out_2	out_2	\N	2018-09-06 13:03:16.734801	2018-09-06 13:03:16.734807	{"dimension_names": ["source_1"], "datatype": "Int"}	2370
4611	failing_network_2018-09-06T15-03-16/root/step_1/v_out_port	•••	Virtual output port	2018-09-06 13:03:16.735488	2018-09-06 13:03:16.735494	null	2370
4612	failing_network_2018-09-06T15-03-16/root/step_3/out_1	out_1	\N	2018-09-06 13:03:16.736215	2018-09-06 13:03:16.736224	{"dimension_names": ["source_1"], "datatype": "Int"}	2371
4613	failing_network_2018-09-06T15-03-16/root/step_3/out_2	out_2	\N	2018-09-06 13:03:16.736901	2018-09-06 13:03:16.736908	{"dimension_names": ["source_1"], "datatype": "Int"}	2371
4614	failing_network_2018-09-06T15-03-16/root/step_3/v_out_port	•••	Virtual output port	2018-09-06 13:03:16.737608	2018-09-06 13:03:16.737615	null	2371
4615	failing_network_2018-09-06T15-03-16/root/source_2/output	output	\N	2018-09-06 13:03:16.738311	2018-09-06 13:03:16.738318	{"dimension_names": ["source_2"], "datatype": "Int"}	2372
4616	failing_network_2018-09-06T15-03-16/root/source_2/v_out_port	•••	Virtual output port	2018-09-06 13:03:16.739054	2018-09-06 13:03:16.73906	null	2372
4617	failing_network_2018-09-06T15-03-16/root/sink_1/output	output	\N	2018-09-06 13:03:16.739761	2018-09-06 13:03:16.739768	{"dimension_names": [], "datatype": "Int"}	2373
4618	failing_network_2018-09-06T15-03-16/root/sink_1/v_out_port	•••	Virtual output port	2018-09-06 13:03:16.740432	2018-09-06 13:03:16.740436	null	2373
4619	add_ints_2018-09-07T12-00-02/root/const_add_right_hand_0/output	output	\N	2018-09-07 10:00:02.674913	2018-09-07 10:00:02.674919	{"datatype": "Int", "dimension_names": ["const_add_right_hand_0"]}	2375
4620	add_ints_2018-09-07T12-00-02/root/const_add_right_hand_0/v_out_port	•••	Virtual output port	2018-09-07 10:00:02.675742	2018-09-07 10:00:02.675747	null	2375
4621	add_ints_2018-09-07T12-00-02/root/source/output	output	\N	2018-09-07 10:00:02.676357	2018-09-07 10:00:02.676361	{"datatype": "Int", "dimension_names": ["source"]}	2376
4622	add_ints_2018-09-07T12-00-02/root/source/v_out_port	•••	Virtual output port	2018-09-07 10:00:02.676985	2018-09-07 10:00:02.676989	null	2376
4624	add_ints_2018-09-07T12-00-02/root/add/v_out_port	•••	Virtual output port	2018-09-07 10:00:02.678293	2018-09-07 10:00:02.678297	null	2377
4625	add_ints_2018-09-07T12-00-02/root/sink/output	output	\N	2018-09-07 10:00:02.678887	2018-09-07 10:00:02.678891	{"datatype": "Int", "dimension_names": []}	2378
4626	add_ints_2018-09-07T12-00-02/root/sink/v_out_port	•••	Virtual output port	2018-09-07 10:00:02.679413	2018-09-07 10:00:02.679417	null	2378
4628	iris/root/0_statistics/9_const_get_gm_volume_flatten_0/out_output	out_output		2018-09-08 13:18:13.046578	2018-09-08 13:18:13.046583	{}	2388
4629	iris/root/0_statistics/9_const_get_gm_volume_flatten_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.047218	2018-09-08 13:18:13.047222	null	2388
4630	iris/root/0_statistics/11_get_gm_volume/out_statistics	out_statistics		2018-09-08 13:18:13.04787	2018-09-08 13:18:13.047874	{}	2389
4631	iris/root/0_statistics/11_get_gm_volume/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.048427	2018-09-08 13:18:13.048431	null	2389
4632	iris/root/0_statistics/14_get_brain_volume/out_statistics	out_statistics		2018-09-08 13:18:13.049009	2018-09-08 13:18:13.049013	{}	2390
4633	iris/root/0_statistics/14_get_brain_volume/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.049499	2018-09-08 13:18:13.049503	null	2390
4634	iris/root/0_statistics/18_const_get_wm_volume_flatten_0/out_output	out_output		2018-09-08 13:18:13.049992	2018-09-08 13:18:13.049998	{}	2391
4635	iris/root/0_statistics/18_const_get_wm_volume_flatten_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.050449	2018-09-08 13:18:13.050453	null	2391
4636	iris/root/0_statistics/19_mask_hammers_labels/out_result	out_result		2018-09-08 13:18:13.050943	2018-09-08 13:18:13.050947	{}	2392
4637	iris/root/0_statistics/19_mask_hammers_labels/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.051393	2018-09-08 13:18:13.051397	null	2392
4638	iris/root/0_statistics/25_wm_volume/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.05192	2018-09-08 13:18:13.051924	null	2393
4639	iris/root/0_statistics/26_const_get_wm_volume_volume_0/out_output	out_output		2018-09-08 13:18:13.052377	2018-09-08 13:18:13.052381	{}	2394
4640	iris/root/0_statistics/26_const_get_wm_volume_volume_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.052823	2018-09-08 13:18:13.052826	null	2394
4641	iris/root/0_statistics/39_const_get_gm_volume_volume_0/out_output	out_output		2018-09-08 13:18:13.053253	2018-09-08 13:18:13.053257	{}	2395
4642	iris/root/0_statistics/39_const_get_gm_volume_volume_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.053732	2018-09-08 13:18:13.053736	null	2395
4643	iris/root/0_statistics/41_const_mask_hammers_labels_operator_0/out_output	out_output		2018-09-08 13:18:13.054144	2018-09-08 13:18:13.054148	{}	2396
4644	iris/root/0_statistics/41_const_mask_hammers_labels_operator_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.054614	2018-09-08 13:18:13.054618	null	2396
4645	iris/root/0_statistics/45_const_get_wm_volume_selection_0/out_output	out_output		2018-09-08 13:18:13.05506	2018-09-08 13:18:13.055063	{}	2397
4646	iris/root/0_statistics/45_const_get_wm_volume_selection_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.055508	2018-09-08 13:18:13.055512	null	2397
4647	iris/root/0_statistics/48_const_get_brain_volume_flatten_0/out_output	out_output		2018-09-08 13:18:13.055984	2018-09-08 13:18:13.055988	{}	2398
4648	iris/root/0_statistics/48_const_get_brain_volume_flatten_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.056415	2018-09-08 13:18:13.056418	null	2398
4649	iris/root/0_statistics/52_const_get_brain_volume_selection_0/out_output	out_output		2018-09-08 13:18:13.056819	2018-09-08 13:18:13.056823	{}	2399
4650	iris/root/0_statistics/52_const_get_brain_volume_selection_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.057251	2018-09-08 13:18:13.057255	null	2399
4651	iris/root/0_statistics/55_brain_volume/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.057799	2018-09-08 13:18:13.057803	null	2400
4652	iris/root/0_statistics/60_const_get_brain_volume_volume_0/out_output	out_output		2018-09-08 13:18:13.058237	2018-09-08 13:18:13.058241	{}	2401
4653	iris/root/0_statistics/60_const_get_brain_volume_volume_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.058702	2018-09-08 13:18:13.058706	null	2401
4654	iris/root/0_statistics/126_get_wm_volume/out_statistics	out_statistics		2018-09-08 13:18:13.05915	2018-09-08 13:18:13.059154	{}	2402
4655	iris/root/0_statistics/126_get_wm_volume/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.059805	2018-09-08 13:18:13.05981	null	2402
4656	iris/root/0_statistics/128_gm_volume/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.060325	2018-09-08 13:18:13.060329	null	2403
4657	iris/root/0_statistics/129_const_get_gm_volume_selection_0/out_output	out_output		2018-09-08 13:18:13.060809	2018-09-08 13:18:13.060812	{}	2404
4658	iris/root/0_statistics/129_const_get_gm_volume_selection_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.061285	2018-09-08 13:18:13.061289	null	2404
4659	iris/root/1_dicom_to_nifti/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.061816	2018-09-08 13:18:13.06182	null	2381
4660	iris/root/1_dicom_to_nifti/20_t1_dicom_to_nifti/out_image	out_image		2018-09-08 13:18:13.062256	2018-09-08 13:18:13.06226	{}	2405
4661	iris/root/1_dicom_to_nifti/20_t1_dicom_to_nifti/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.062655	2018-09-08 13:18:13.062658	null	2405
4662	iris/root/1_dicom_to_nifti/44_reformat_t1/out_reformatted_image	out_reformatted_image		2018-09-08 13:18:13.063132	2018-09-08 13:18:13.063136	{}	2406
4663	iris/root/1_dicom_to_nifti/44_reformat_t1/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.063698	2018-09-08 13:18:13.063702	null	2406
4664	iris/root/1_dicom_to_nifti/61_const_t1_dicom_to_nifti_file_order_0/out_output	out_output		2018-09-08 13:18:13.064172	2018-09-08 13:18:13.064175	{}	2407
4665	iris/root/1_dicom_to_nifti/61_const_t1_dicom_to_nifti_file_order_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.064738	2018-09-08 13:18:13.064742	null	2407
4666	iris/root/2_hammers_atlas/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.065213	2018-09-08 13:18:13.065216	null	2382
4667	iris/root/2_hammers_atlas/8_hammers_t1w/out_output	out_output		2018-09-08 13:18:13.065672	2018-09-08 13:18:13.065676	{}	2408
4668	iris/root/2_hammers_atlas/8_hammers_t1w/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.066128	2018-09-08 13:18:13.066131	null	2408
4669	iris/root/2_hammers_atlas/36_hammers_labels/out_output	out_output		2018-09-08 13:18:13.066586	2018-09-08 13:18:13.06659	{}	2409
4670	iris/root/2_hammers_atlas/36_hammers_labels/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.067059	2018-09-08 13:18:13.067063	null	2409
4671	iris/root/2_hammers_atlas/50_hammers_brains/out_output	out_output		2018-09-08 13:18:13.067498	2018-09-08 13:18:13.067502	{}	2410
4672	iris/root/2_hammers_atlas/50_hammers_brains/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.068027	2018-09-08 13:18:13.068031	null	2410
4673	iris/root/2_hammers_atlas/64_hammers_brains_r5/out_output	out_output		2018-09-08 13:18:13.068527	2018-09-08 13:18:13.068531	{}	2411
4674	iris/root/2_hammers_atlas/64_hammers_brains_r5/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.068993	2018-09-08 13:18:13.068997	null	2411
4675	iris/root/3_subjects/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.069457	2018-09-08 13:18:13.069461	null	2383
4676	iris/root/3_subjects/34_t1w_dicom/out_output	out_output		2018-09-08 13:18:13.069927	2018-09-08 13:18:13.069931	{}	2412
4677	iris/root/3_subjects/34_t1w_dicom/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.070317	2018-09-08 13:18:13.070321	null	2412
4678	iris/root/4_output/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.070821	2018-09-08 13:18:13.070825	null	2384
4679	iris/root/4_output/16_wm_map_output/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.071238	2018-09-08 13:18:13.071242	null	2413
4680	iris/root/4_output/23_multi_atlas_segm/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.071703	2018-09-08 13:18:13.071707	null	2414
4681	iris/root/4_output/29_t1_nifti_images/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.072114	2018-09-08 13:18:13.072132	null	2415
4682	iris/root/4_output/40_gm_map_output/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.072726	2018-09-08 13:18:13.07273	null	2416
4683	iris/root/4_output/57_hammer_brain_mask/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.073253	2018-09-08 13:18:13.073257	null	2417
4684	iris/root/5_BET_and_REG/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.073806	2018-09-08 13:18:13.07381	null	2385
4685	iris/root/5_BET_and_REG/7_threshold_labels/out_image	out_image		2018-09-08 13:18:13.074274	2018-09-08 13:18:13.074278	{}	2418
4686	iris/root/5_BET_and_REG/7_threshold_labels/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.07477	2018-09-08 13:18:13.074774	null	2418
4687	iris/root/5_BET_and_REG/10_const_bet_fraction_threshold_0/out_output	out_output		2018-09-08 13:18:13.075215	2018-09-08 13:18:13.075218	{}	2419
4688	iris/root/5_BET_and_REG/10_const_bet_fraction_threshold_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.075741	2018-09-08 13:18:13.075745	null	2419
4689	iris/root/5_BET_and_REG/12_t1w_registration/out_transform	out_transform		2018-09-08 13:18:13.076194	2018-09-08 13:18:13.076198	{}	2420
4690	iris/root/5_BET_and_REG/12_t1w_registration/out_directory	out_directory		2018-09-08 13:18:13.076738	2018-09-08 13:18:13.076742	{}	2420
4691	iris/root/5_BET_and_REG/12_t1w_registration/out_log_file	out_log_file		2018-09-08 13:18:13.077215	2018-09-08 13:18:13.077218	{}	2420
4692	iris/root/5_BET_and_REG/12_t1w_registration/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.077705	2018-09-08 13:18:13.077709	null	2420
4693	iris/root/5_BET_and_REG/13_const_threshold_labels_upper_threshold_0/out_output	out_output		2018-09-08 13:18:13.078167	2018-09-08 13:18:13.078171	{}	2421
4694	iris/root/5_BET_and_REG/13_const_threshold_labels_upper_threshold_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.078634	2018-09-08 13:18:13.078638	null	2421
4695	iris/root/5_BET_and_REG/15_const_bet_bias_cleanup_0/out_output	out_output		2018-09-08 13:18:13.079141	2018-09-08 13:18:13.079145	{}	2422
4696	iris/root/5_BET_and_REG/15_const_bet_bias_cleanup_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.079656	2018-09-08 13:18:13.07966	null	2422
4697	iris/root/5_BET_and_REG/17_hammers_brain_transform/out_determinant_of_jacobian	out_determinant_of_jacobian		2018-09-08 13:18:13.080171	2018-09-08 13:18:13.080175	{}	2423
4698	iris/root/5_BET_and_REG/17_hammers_brain_transform/out_points	out_points		2018-09-08 13:18:13.08062	2018-09-08 13:18:13.080624	{}	2423
4699	iris/root/5_BET_and_REG/17_hammers_brain_transform/out_log_file	out_log_file		2018-09-08 13:18:13.081091	2018-09-08 13:18:13.081094	{}	2423
4700	iris/root/5_BET_and_REG/17_hammers_brain_transform/out_image	out_image		2018-09-08 13:18:13.08151	2018-09-08 13:18:13.081513	{}	2423
4701	iris/root/5_BET_and_REG/17_hammers_brain_transform/out_directory	out_directory		2018-09-08 13:18:13.081956	2018-09-08 13:18:13.08196	{}	2423
4702	iris/root/5_BET_and_REG/17_hammers_brain_transform/out_jacobian_matrix	out_jacobian_matrix		2018-09-08 13:18:13.082364	2018-09-08 13:18:13.082367	{}	2423
4703	iris/root/5_BET_and_REG/17_hammers_brain_transform/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.082875	2018-09-08 13:18:13.082896	null	2423
4704	iris/root/5_BET_and_REG/22_const_brain_mask_registation_parameters_0/out_output	out_output		2018-09-08 13:18:13.083452	2018-09-08 13:18:13.083456	{}	2424
4705	iris/root/5_BET_and_REG/22_const_brain_mask_registation_parameters_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.08401	2018-09-08 13:18:13.084014	null	2424
4706	iris/root/5_BET_and_REG/24_const_combine_labels_method_0/out_output	out_output		2018-09-08 13:18:13.084477	2018-09-08 13:18:13.084481	{}	2425
4707	iris/root/5_BET_and_REG/24_const_combine_labels_method_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.084931	2018-09-08 13:18:13.084935	null	2425
4708	iris/root/5_BET_and_REG/27_const_hammers_brain_transform_threads_0/out_output	out_output		2018-09-08 13:18:13.085368	2018-09-08 13:18:13.085372	{}	2426
4709	iris/root/5_BET_and_REG/27_const_hammers_brain_transform_threads_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.085994	2018-09-08 13:18:13.085998	null	2426
4710	iris/root/5_BET_and_REG/28_n4_bias_correction/out_corrected_image	out_corrected_image		2018-09-08 13:18:13.086744	2018-09-08 13:18:13.086748	{}	2427
4711	iris/root/5_BET_and_REG/28_n4_bias_correction/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.087327	2018-09-08 13:18:13.087331	null	2427
4712	iris/root/5_BET_and_REG/30_combine_labels/out_hard_segment	out_hard_segment		2018-09-08 13:18:13.087879	2018-09-08 13:18:13.087892	{}	2428
4713	iris/root/5_BET_and_REG/30_combine_labels/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.088659	2018-09-08 13:18:13.088663	null	2428
4714	iris/root/5_BET_and_REG/32_const_dilate_brainmask_operation_type_0/out_output	out_output		2018-09-08 13:18:13.089227	2018-09-08 13:18:13.089231	{}	2429
4715	iris/root/5_BET_and_REG/32_const_dilate_brainmask_operation_type_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.089703	2018-09-08 13:18:13.089706	null	2429
4716	iris/root/5_BET_and_REG/33_label_registration/out_determinant_of_jacobian	out_determinant_of_jacobian		2018-09-08 13:18:13.090184	2018-09-08 13:18:13.090188	{}	2430
4717	iris/root/5_BET_and_REG/33_label_registration/out_points	out_points		2018-09-08 13:18:13.09071	2018-09-08 13:18:13.090713	{}	2430
4718	iris/root/5_BET_and_REG/33_label_registration/out_log_file	out_log_file		2018-09-08 13:18:13.091185	2018-09-08 13:18:13.091189	{}	2430
4719	iris/root/5_BET_and_REG/33_label_registration/out_image	out_image		2018-09-08 13:18:13.091628	2018-09-08 13:18:13.091632	{}	2430
4720	iris/root/5_BET_and_REG/33_label_registration/out_directory	out_directory		2018-09-08 13:18:13.09205	2018-09-08 13:18:13.092053	{}	2430
4721	iris/root/5_BET_and_REG/33_label_registration/out_jacobian_matrix	out_jacobian_matrix		2018-09-08 13:18:13.092518	2018-09-08 13:18:13.092521	{}	2430
4722	iris/root/5_BET_and_REG/33_label_registration/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.093023	2018-09-08 13:18:13.093026	null	2430
4723	iris/root/5_BET_and_REG/35_const_dilate_brainmask_radius_0/out_output	out_output		2018-09-08 13:18:13.093507	2018-09-08 13:18:13.093511	{}	2431
4724	iris/root/5_BET_and_REG/35_const_dilate_brainmask_radius_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.093917	2018-09-08 13:18:13.093921	null	2431
4725	iris/root/5_BET_and_REG/38_threshold_brain/out_image	out_image		2018-09-08 13:18:13.094324	2018-09-08 13:18:13.094328	{}	2432
4726	iris/root/5_BET_and_REG/38_threshold_brain/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.094843	2018-09-08 13:18:13.094846	null	2432
4727	iris/root/5_BET_and_REG/43_convert_brain/out_image	out_image		2018-09-08 13:18:13.095364	2018-09-08 13:18:13.095368	{}	2433
4728	iris/root/5_BET_and_REG/43_convert_brain/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.095844	2018-09-08 13:18:13.095859	null	2433
4729	iris/root/5_BET_and_REG/47_const_convert_brain_component_type_0/out_output	out_output		2018-09-08 13:18:13.096422	2018-09-08 13:18:13.096426	{}	2434
4730	iris/root/5_BET_and_REG/47_const_convert_brain_component_type_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.096988	2018-09-08 13:18:13.096992	null	2434
4731	iris/root/5_BET_and_REG/49_const_t1w_registration_parameters_0/out_output	out_output		2018-09-08 13:18:13.097449	2018-09-08 13:18:13.097453	{}	2435
4732	iris/root/5_BET_and_REG/49_const_t1w_registration_parameters_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.097948	2018-09-08 13:18:13.097951	null	2435
4733	iris/root/5_BET_and_REG/51_const_combine_brains_number_of_classes_0/out_output	out_output		2018-09-08 13:18:13.098419	2018-09-08 13:18:13.098423	{}	2436
4734	iris/root/5_BET_and_REG/51_const_combine_brains_number_of_classes_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.098923	2018-09-08 13:18:13.098927	null	2436
4735	iris/root/5_BET_and_REG/53_const_label_registration_threads_0/out_output	out_output		2018-09-08 13:18:13.099362	2018-09-08 13:18:13.099366	{}	2437
4736	iris/root/5_BET_and_REG/53_const_label_registration_threads_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.099958	2018-09-08 13:18:13.099962	null	2437
4737	iris/root/5_BET_and_REG/56_brain_mask_registation/out_transform	out_transform		2018-09-08 13:18:13.100444	2018-09-08 13:18:13.100448	{}	2438
4738	iris/root/5_BET_and_REG/56_brain_mask_registation/out_directory	out_directory		2018-09-08 13:18:13.101057	2018-09-08 13:18:13.101061	{}	2438
4739	iris/root/5_BET_and_REG/56_brain_mask_registation/out_log_file	out_log_file		2018-09-08 13:18:13.101546	2018-09-08 13:18:13.10155	{}	2438
4740	iris/root/5_BET_and_REG/56_brain_mask_registation/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.101992	2018-09-08 13:18:13.101995	null	2438
4741	iris/root/5_BET_and_REG/58_const_combine_brains_method_0/out_output	out_output		2018-09-08 13:18:13.102418	2018-09-08 13:18:13.102422	{}	2439
4742	iris/root/5_BET_and_REG/58_const_combine_brains_method_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.102909	2018-09-08 13:18:13.102913	null	2439
4743	iris/root/5_BET_and_REG/59_const_t1w_registration_threads_0/out_output	out_output		2018-09-08 13:18:13.10334	2018-09-08 13:18:13.103344	{}	2440
4744	iris/root/5_BET_and_REG/59_const_t1w_registration_threads_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.103964	2018-09-08 13:18:13.103968	null	2440
4745	iris/root/5_BET_and_REG/63_const_brain_mask_registation_threads_0/out_output	out_output		2018-09-08 13:18:13.104414	2018-09-08 13:18:13.104417	{}	2441
4746	iris/root/5_BET_and_REG/63_const_brain_mask_registation_threads_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.10495	2018-09-08 13:18:13.104954	null	2441
4747	iris/root/5_BET_and_REG/121_combine_brains/out_hard_segment	out_hard_segment		2018-09-08 13:18:13.105399	2018-09-08 13:18:13.105403	{}	2442
4748	iris/root/5_BET_and_REG/121_combine_brains/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.105831	2018-09-08 13:18:13.105835	null	2442
4749	iris/root/5_BET_and_REG/124_const_combine_labels_number_of_classes_0/out_output	out_output		2018-09-08 13:18:13.106321	2018-09-08 13:18:13.106325	{}	2443
4750	iris/root/5_BET_and_REG/124_const_combine_labels_number_of_classes_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.106891	2018-09-08 13:18:13.106895	null	2443
4751	iris/root/5_BET_and_REG/125_dilate_brainmask/out_image	out_image		2018-09-08 13:18:13.107366	2018-09-08 13:18:13.107369	{}	2444
4752	iris/root/5_BET_and_REG/125_dilate_brainmask/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.107832	2018-09-08 13:18:13.107835	null	2444
4753	iris/root/5_BET_and_REG/130_bet/out_overlay_image	out_overlay_image		2018-09-08 13:18:13.108297	2018-09-08 13:18:13.108301	{}	2445
4754	iris/root/5_BET_and_REG/130_bet/out_mesh_image	out_mesh_image		2018-09-08 13:18:13.108823	2018-09-08 13:18:13.108826	{}	2445
4755	iris/root/5_BET_and_REG/130_bet/out_mask_image	out_mask_image		2018-09-08 13:18:13.10929	2018-09-08 13:18:13.109294	{}	2445
4756	iris/root/5_BET_and_REG/130_bet/out_skull_image	out_skull_image		2018-09-08 13:18:13.109773	2018-09-08 13:18:13.109777	{}	2445
4757	iris/root/5_BET_and_REG/130_bet/out_brain_image	out_brain_image		2018-09-08 13:18:13.110223	2018-09-08 13:18:13.110227	{}	2445
4758	iris/root/5_BET_and_REG/130_bet/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.110969	2018-09-08 13:18:13.110973	null	2445
4759	iris/root/5_BET_and_REG/131_const_dilate_brainmask_operation_0/out_output	out_output		2018-09-08 13:18:13.111405	2018-09-08 13:18:13.111409	{}	2446
4760	iris/root/5_BET_and_REG/131_const_dilate_brainmask_operation_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.111985	2018-09-08 13:18:13.111989	null	2446
4761	iris/root/6_qc_overview/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.112453	2018-09-08 13:18:13.112456	null	2386
4762	iris/root/6_qc_overview/21_t1_with_hammers/out_png_image	out_png_image		2018-09-08 13:18:13.112922	2018-09-08 13:18:13.112926	{}	2447
4763	iris/root/6_qc_overview/21_t1_with_hammers/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.113374	2018-09-08 13:18:13.113378	null	2447
4764	iris/root/6_qc_overview/31_t1_with_hammers_sink/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.113996	2018-09-08 13:18:13.114	null	2448
4765	iris/root/6_qc_overview/37_t1_with_wm_outline_sink/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.114496	2018-09-08 13:18:13.1145	null	2449
4766	iris/root/6_qc_overview/42_t1_with_gm_outline/out_png_image	out_png_image		2018-09-08 13:18:13.115018	2018-09-08 13:18:13.115022	{}	2450
4767	iris/root/6_qc_overview/42_t1_with_gm_outline/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.11552	2018-09-08 13:18:13.115524	null	2450
4768	iris/root/6_qc_overview/46_t1_qc/out_png_image	out_png_image		2018-09-08 13:18:13.116008	2018-09-08 13:18:13.116012	{}	2451
4769	iris/root/6_qc_overview/46_t1_qc/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.116438	2018-09-08 13:18:13.116442	null	2451
4770	iris/root/6_qc_overview/54_t1_qc_sink/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.116833	2018-09-08 13:18:13.116837	null	2452
4771	iris/root/6_qc_overview/62_t1_with_gm_outline_sink/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.117237	2018-09-08 13:18:13.11724	null	2453
4772	iris/root/6_qc_overview/122_t1_with_brain/out_png_image	out_png_image		2018-09-08 13:18:13.117628	2018-09-08 13:18:13.117632	{}	2454
4773	iris/root/6_qc_overview/122_t1_with_brain/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.118072	2018-09-08 13:18:13.118076	null	2454
4774	iris/root/6_qc_overview/123_t1_with_wm_outline/out_png_image	out_png_image		2018-09-08 13:18:13.118632	2018-09-08 13:18:13.118636	{}	2455
4775	iris/root/6_qc_overview/123_t1_with_wm_outline/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.119088	2018-09-08 13:18:13.119092	null	2455
4776	iris/root/6_qc_overview/127_t1_with_brain_sink/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.119536	2018-09-08 13:18:13.11954	null	2456
4777	iris/root/65_spm_tissue_quantification/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.12009	2018-09-08 13:18:13.120094	null	2387
4778	iris/root/65_spm_tissue_quantification/66_InputImages/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.120571	2018-09-08 13:18:13.120575	null	2457
4779	iris/root/65_spm_tissue_quantification/66_InputImages/81_T1_images/out_output	out_output		2018-09-08 13:18:13.121023	2018-09-08 13:18:13.121027	{}	2461
4780	iris/root/65_spm_tissue_quantification/66_InputImages/81_T1_images/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.121456	2018-09-08 13:18:13.12146	null	2461
4781	iris/root/65_spm_tissue_quantification/67_InputImage/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.121893	2018-09-08 13:18:13.121897	null	2458
4782	iris/root/65_spm_tissue_quantification/67_InputImage/75_t1_passthrough/out_result	out_result		2018-09-08 13:18:13.122311	2018-09-08 13:18:13.122315	{}	2462
4783	iris/root/65_spm_tissue_quantification/67_InputImage/75_t1_passthrough/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.122972	2018-09-08 13:18:13.122976	null	2462
4784	iris/root/65_spm_tissue_quantification/67_InputImage/77_const_t1_passthrough_operator_0/out_output	out_output		2018-09-08 13:18:13.123417	2018-09-08 13:18:13.123421	{}	2463
4785	iris/root/65_spm_tissue_quantification/67_InputImage/77_const_t1_passthrough_operator_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.123973	2018-09-08 13:18:13.123977	null	2463
4786	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.124429	2018-09-08 13:18:13.124433	null	2459
4787	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation/out_directory	out_directory		2018-09-08 13:18:13.124899	2018-09-08 13:18:13.124903	{}	2464
4788	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation/out_gm_image	out_gm_image		2018-09-08 13:18:13.125323	2018-09-08 13:18:13.125327	{}	2464
4789	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation/out_csf_image	out_csf_image		2018-09-08 13:18:13.125861	2018-09-08 13:18:13.125867	{}	2464
4790	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation/out_wm_image	out_wm_image		2018-09-08 13:18:13.126291	2018-09-08 13:18:13.126294	{}	2464
4791	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/70_spm_segmantation/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.126773	2018-09-08 13:18:13.126777	null	2464
4792	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/71_const_edit_transform_file_set_0/out_output	out_output		2018-09-08 13:18:13.127343	2018-09-08 13:18:13.127347	{}	2465
4793	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/71_const_edit_transform_file_set_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.127829	2018-09-08 13:18:13.127833	null	2465
4794	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/74_const_invert_gm_parameters_0/out_output	out_output		2018-09-08 13:18:13.128276	2018-09-08 13:18:13.12828	{}	2466
4795	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/74_const_invert_gm_parameters_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.128736	2018-09-08 13:18:13.12874	null	2466
4796	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/out_transform	out_transform		2018-09-08 13:18:13.129185	2018-09-08 13:18:13.129188	{}	2467
4797	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/out_directory	out_directory		2018-09-08 13:18:13.129673	2018-09-08 13:18:13.129677	{}	2467
4798	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/out_log_file	out_log_file		2018-09-08 13:18:13.130186	2018-09-08 13:18:13.13019	{}	2467
4799	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/76_reg_t1_to_spmtemplate/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.130696	2018-09-08 13:18:13.1307	null	2467
4800	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/78_const_reg_t1_to_spmtemplate_parameters_0/out_output	out_output		2018-09-08 13:18:13.13128	2018-09-08 13:18:13.131284	{}	2468
4801	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/78_const_reg_t1_to_spmtemplate_parameters_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.131832	2018-09-08 13:18:13.131836	null	2468
4802	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/out_determinant_of_jacobian	out_determinant_of_jacobian		2018-09-08 13:18:13.132292	2018-09-08 13:18:13.132296	{}	2469
4803	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/out_points	out_points		2018-09-08 13:18:13.13276	2018-09-08 13:18:13.132764	{}	2469
4804	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/out_log_file	out_log_file		2018-09-08 13:18:13.133196	2018-09-08 13:18:13.1332	{}	2469
4805	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/out_image	out_image		2018-09-08 13:18:13.133613	2018-09-08 13:18:13.133617	{}	2469
4806	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/out_directory	out_directory		2018-09-08 13:18:13.13409	2018-09-08 13:18:13.134094	{}	2469
4807	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/out_jacobian_matrix	out_jacobian_matrix		2018-09-08 13:18:13.134514	2018-09-08 13:18:13.134518	{}	2469
4808	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/79_transform_csf/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.135014	2018-09-08 13:18:13.135018	null	2469
4809	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/out_transform	out_transform		2018-09-08 13:18:13.135522	2018-09-08 13:18:13.135526	{}	2470
4810	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/out_directory	out_directory		2018-09-08 13:18:13.135996	2018-09-08 13:18:13.136	{}	2470
4811	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/out_log_file	out_log_file		2018-09-08 13:18:13.136455	2018-09-08 13:18:13.136459	{}	2470
4812	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/82_invert_gm/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.136944	2018-09-08 13:18:13.136948	null	2470
4813	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/out_gm_image	out_gm_image		2018-09-08 13:18:13.137359	2018-09-08 13:18:13.137363	{}	2471
4814	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/out_csf_image	out_csf_image		2018-09-08 13:18:13.137843	2018-09-08 13:18:13.137857	{}	2471
4815	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/out_wm_image	out_wm_image		2018-09-08 13:18:13.138267	2018-09-08 13:18:13.138271	{}	2471
4816	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/83_spm_hard_segment/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.138669	2018-09-08 13:18:13.138673	null	2471
4817	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/85_const_reg_t1_to_spmtemplate_fixed_image_0/out_output	out_output		2018-09-08 13:18:13.139096	2018-09-08 13:18:13.1391	{}	2472
4818	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/85_const_reg_t1_to_spmtemplate_fixed_image_0/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.139526	2018-09-08 13:18:13.13953	null	2472
4819	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/86_edit_transform_file/out_transform	out_transform		2018-09-08 13:18:13.139966	2018-09-08 13:18:13.13997	{}	2473
4820	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/86_edit_transform_file/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.140393	2018-09-08 13:18:13.140397	null	2473
4821	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/out_determinant_of_jacobian	out_determinant_of_jacobian		2018-09-08 13:18:13.140909	2018-09-08 13:18:13.140913	{}	2474
4822	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/out_points	out_points		2018-09-08 13:18:13.141356	2018-09-08 13:18:13.14136	{}	2474
4823	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/out_log_file	out_log_file		2018-09-08 13:18:13.141892	2018-09-08 13:18:13.141896	{}	2474
4824	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/out_image	out_image		2018-09-08 13:18:13.1423	2018-09-08 13:18:13.142304	{}	2474
4825	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/out_directory	out_directory		2018-09-08 13:18:13.142713	2018-09-08 13:18:13.142717	{}	2474
4826	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/out_jacobian_matrix	out_jacobian_matrix		2018-09-08 13:18:13.143162	2018-09-08 13:18:13.143166	{}	2474
4827	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/87_transform_gm/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.143605	2018-09-08 13:18:13.143609	null	2474
4828	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/out_determinant_of_jacobian	out_determinant_of_jacobian		2018-09-08 13:18:13.144074	2018-09-08 13:18:13.144078	{}	2475
4829	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/out_points	out_points		2018-09-08 13:18:13.144538	2018-09-08 13:18:13.144542	{}	2475
4830	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/out_log_file	out_log_file		2018-09-08 13:18:13.144979	2018-09-08 13:18:13.144983	{}	2475
4831	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/out_image	out_image		2018-09-08 13:18:13.145395	2018-09-08 13:18:13.145399	{}	2475
4832	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/out_directory	out_directory		2018-09-08 13:18:13.145862	2018-09-08 13:18:13.145867	{}	2475
4833	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/out_jacobian_matrix	out_jacobian_matrix		2018-09-08 13:18:13.146278	2018-09-08 13:18:13.146281	{}	2475
4834	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/89_transform_wm/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.146664	2018-09-08 13:18:13.146668	null	2475
4835	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/out_determinant_of_jacobian	out_determinant_of_jacobian		2018-09-08 13:18:13.147131	2018-09-08 13:18:13.147135	{}	2476
4836	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/out_points	out_points		2018-09-08 13:18:13.147636	2018-09-08 13:18:13.147639	{}	2476
4837	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/out_log_file	out_log_file		2018-09-08 13:18:13.148146	2018-09-08 13:18:13.14815	{}	2476
4838	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/out_image	out_image		2018-09-08 13:18:13.148559	2018-09-08 13:18:13.148563	{}	2476
4839	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/out_directory	out_directory		2018-09-08 13:18:13.148983	2018-09-08 13:18:13.148987	{}	2476
4840	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/out_jacobian_matrix	out_jacobian_matrix		2018-09-08 13:18:13.149416	2018-09-08 13:18:13.14942	{}	2476
4841	iris/root/65_spm_tissue_quantification/68_SPM_Tissue_Segmentation/91_transform_t1/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.149876	2018-09-08 13:18:13.14988	null	2476
4842	iris/root/65_spm_tissue_quantification/69_outputs/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.150299	2018-09-08 13:18:13.150303	null	2460
4843	iris/root/65_spm_tissue_quantification/69_outputs/72_csf_map/out_sink	out_sink		2018-09-08 13:18:13.150732	2018-09-08 13:18:13.150736	{}	2477
4844	iris/root/65_spm_tissue_quantification/69_outputs/72_csf_map/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.151156	2018-09-08 13:18:13.15116	null	2477
4845	iris/root/65_spm_tissue_quantification/69_outputs/73_wm_map/out_sink	out_sink		2018-09-08 13:18:13.151614	2018-09-08 13:18:13.151618	{}	2478
4846	iris/root/65_spm_tissue_quantification/69_outputs/73_wm_map/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.152042	2018-09-08 13:18:13.152046	null	2478
4847	iris/root/65_spm_tissue_quantification/69_outputs/80_gm_map/out_sink	out_sink		2018-09-08 13:18:13.152492	2018-09-08 13:18:13.152495	{}	2479
4848	iris/root/65_spm_tissue_quantification/69_outputs/80_gm_map/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.152918	2018-09-08 13:18:13.152922	null	2479
4849	iris/root/65_spm_tissue_quantification/69_outputs/84_csf_spm/out_sink	out_sink		2018-09-08 13:18:13.153337	2018-09-08 13:18:13.15334	{}	2480
4850	iris/root/65_spm_tissue_quantification/69_outputs/84_csf_spm/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.153804	2018-09-08 13:18:13.153808	null	2480
4851	iris/root/65_spm_tissue_quantification/69_outputs/88_gm_spm/out_sink	out_sink		2018-09-08 13:18:13.154273	2018-09-08 13:18:13.154277	{}	2481
4852	iris/root/65_spm_tissue_quantification/69_outputs/88_gm_spm/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.154759	2018-09-08 13:18:13.154763	null	2481
4853	iris/root/65_spm_tissue_quantification/69_outputs/90_wm_spm/out_sink	out_sink		2018-09-08 13:18:13.155174	2018-09-08 13:18:13.155178	{}	2482
4854	iris/root/65_spm_tissue_quantification/69_outputs/90_wm_spm/v_out_port	•••	Virtual output port	2018-09-08 13:18:13.155583	2018-09-08 13:18:13.155587	null	2482
\.


--
-- Name: out_ports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.out_ports_id_seq', 4862, true);


--
-- Data for Name: runs; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.runs (id, name, title, description, created, modified, custom_data, "user") FROM stdin;
49	example_2018-09-03T11-07-53	example_2018-09-03T11-07-53	Run of example_2018-09-03T11-07-53 started at 2018-09-03 11:07:53.985774	2018-09-03 09:07:54.366849	2018-09-03 09:08:49.792501	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T11-07-53_r_r3dv_7"}	hachterberg
34	failing_macro_top_level_2018-08-22T12-15-04	failing_macro_top_level_2018-08-22T12-15-04	Run of failing_macro_top_level_2018-08-22T12-15-04 started at 2018-08-22 12:15:04.420671	2018-08-22 10:15:04.775452	2018-08-22 10:22:35.74179	{"workflow_engine": "fastr", "tmpdir": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_failing_macro_top_level_2018-08-22T12-15-04_fMMjNc"}	tkroes
48	example_2018-09-03T11-01-12	example_2018-09-03T11-01-12	Run of example_2018-09-03T11-01-12 started at 2018-09-03 11:01:12.021642	2018-09-03 09:01:12.278955	2018-09-03 09:01:52.023746	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T11-01-12_gs0fsf4u"}	hachterberg
37	cross_validation_2018-08-22T15-29-33	cross_validation_2018-08-22T15-29-33	Run of cross_validation_2018-08-22T15-29-33 started at 2018-08-22 15:29:33.539255	2018-08-22 13:29:33.754338	2018-08-22 13:32:36.580038	{"workflow_engine": "fastr", "tmpdir": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_cross_validation_2018-08-22T15-29-33_RxjZXf"}	tkroes
32	failing_network_2018-08-22T11-29-01	failing_network_2018-08-22T11-29-01	Run of failing_network_2018-08-22T11-29-01 started at 2018-08-22 11:29:01.902719	2018-08-22 09:29:02.182571	2018-08-22 09:32:09.673982	{"workflow_engine": "fastr", "tmpdir": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_failing_network_2018-08-22T11-29-01_UvFYON"}	tkroes
42	failing_network_2018-08-24T15-34-44	failing_network_2018-08-24T15-34-44	Run of failing_network_2018-08-24T15-34-44 started at 2018-08-24 15:34:44.684273	2018-08-24 13:34:44.867143	2018-08-24 13:36:53.015941	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_failing_network_2018-08-24T15-34-44_iq1x8o52"}	hachterberg
33	failing_network_2018-08-22T12-10-16	failing_network_2018-08-22T12-10-16	Run of failing_network_2018-08-22T12-10-16 started at 2018-08-22 12:10:16.363042	2018-08-22 10:10:17.451655	2018-08-22 10:14:28.16896	{"workflow_engine": "fastr", "tmpdir": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_failing_network_2018-08-22T12-10-16_8nJmQj"}	tkroes
35	macro_top_level_2018-08-22T12-36-32	macro_top_level_2018-08-22T12-36-32	Run of macro_top_level_2018-08-22T12-36-32 started at 2018-08-22 12:36:32.684357	2018-08-22 10:36:33.053481	2018-08-22 10:40:18.459728	{"workflow_engine": "fastr", "tmpdir": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_macro_top_level_2018-08-22T12-36-32_JyNwKZ"}	tkroes
43	example_2018-09-03T10-32-43	example_2018-09-03T10-32-43	Run of example_2018-09-03T10-32-43 started at 2018-09-03 10:32:43.154387	2018-09-03 08:32:43.850114	2018-09-03 08:33:24.567068	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T10-32-43_ppjaqsjf"}	hachterberg
36	macro_node_2_2018-08-22T12-40-46	macro_node_2_2018-08-22T12-40-46	Run of macro_node_2_2018-08-22T12-40-46 started at 2018-08-22 12:40:46.552599	2018-08-22 10:40:46.822292	2018-08-22 10:44:32.690269	{"workflow_engine": "fastr", "tmpdir": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_macro_node_2_2018-08-22T12-40-46_RZQAuh"}	tkroes
38	input_groups_2018-08-22T15-39-07	input_groups_2018-08-22T15-39-07	Run of input_groups_2018-08-22T15-39-07 started at 2018-08-22 15:39:07.123076	2018-08-22 13:39:07.310825	2018-08-22 13:41:44.717235	{"workflow_engine": "fastr", "tmpdir": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_input_groups_2018-08-22T15-39-07_GJzxrf"}	tkroes
45	example_2018-09-03T10-37-58	example_2018-09-03T10-37-58	Run of example_2018-09-03T10-37-58 started at 2018-09-03 10:37:58.871453	2018-09-03 08:37:59.140925	2018-09-03 08:38:34.551014	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T10-37-58_i5e36akm"}	hachterberg
46	example_2018-09-03T10-41-42	example_2018-09-03T10-41-42	Run of example_2018-09-03T10-41-42 started at 2018-09-03 10:41:42.495697	2018-09-03 08:41:42.744437	2018-09-03 08:42:17.332817	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T10-41-42__pdiapsd"}	hachterberg
44	example_2018-09-03T10-36-42	example_2018-09-03T10-36-42	Run of example_2018-09-03T10-36-42 started at 2018-09-03 10:36:42.904094	2018-09-03 08:36:43.158548	2018-09-03 08:37:17.363906	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T10-36-42_b41dblwy"}	hachterberg
47	example_2018-09-03T10-47-39	example_2018-09-03T10-47-39	Run of example_2018-09-03T10-47-39 started at 2018-09-03 10:47:39.606467	2018-09-03 08:47:39.86171	2018-09-03 08:48:19.776035	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T10-47-39__o9mws96"}	hachterberg
53	example_2018-09-03T11-17-35	example_2018-09-03T11-17-35	Run of example_2018-09-03T11-17-35 started at 2018-09-03 11:17:35.158355	2018-09-03 09:17:35.559357	2018-09-03 09:18:17.609802	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T11-17-35_6j9ek34b"}	hachterberg
50	example_2018-09-03T11-12-21	example_2018-09-03T11-12-21	Run of example_2018-09-03T11-12-21 started at 2018-09-03 11:12:21.302653	2018-09-03 09:12:21.560446	2018-09-03 09:12:59.274844	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T11-12-21_cbj9htfu"}	hachterberg
54	example_2018-09-03T11-31-38	example_2018-09-03T11-31-38	Run of example_2018-09-03T11-31-38 started at 2018-09-03 11:31:38.759927	2018-09-03 09:31:39.045837	2018-09-03 09:32:50.244913	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T11-31-38_kmvnnu9c"}	hachterberg
51	example_2018-09-03T11-13-16	example_2018-09-03T11-13-16	Run of example_2018-09-03T11-13-16 started at 2018-09-03 11:13:16.242156	2018-09-03 09:13:16.457105	2018-09-03 09:13:52.140369	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T11-13-16_wbvkq8wm"}	hachterberg
52	example_2018-09-03T11-14-49	example_2018-09-03T11-14-49	Run of example_2018-09-03T11-14-49 started at 2018-09-03 11:14:49.022458	2018-09-03 09:14:49.279953	2018-09-03 09:15:25.239752	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T11-14-49_ig4806hb"}	hachterberg
55	auto_prefix_test_2018-09-03T11-34-05	auto_prefix_test_2018-09-03T11-34-05	Run of auto_prefix_test_2018-09-03T11-34-05 started at 2018-09-03 11:34:05.681380	2018-09-03 09:34:06.607122	2018-09-03 09:36:55.276221	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_auto_prefix_test_2018-09-03T11-34-05_oq8sl5sq"}	hachterberg
56	example_2018-09-03T12-17-58	example_2018-09-03T12-17-58	Run of example_2018-09-03T12-17-58 started at 2018-09-03 12:17:58.199319	2018-09-03 10:17:58.502931	2018-09-03 10:18:34.196924	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T12-17-58_e7uajjzx"}	hachterberg
57	example_2018-09-03T12-31-11	example_2018-09-03T12-31-11	Run of example_2018-09-03T12-31-11 started at 2018-09-03 12:31:11.804669	2018-09-03 10:31:12.099146	2018-09-03 10:31:46.105318	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T12-31-11_4amd5mb2"}	hachterberg
75	iris	iris		2018-09-08 13:18:12.497268	2018-09-09 17:56:01.923144	{}	simulator
67	macro_node_2_2018-09-03T14-14-40	macro_node_2_2018-09-03T14-14-40	Run of macro_node_2_2018-09-03T14-14-40 started at 2018-09-03 14:14:40.236341	2018-09-03 12:14:40.462167	2018-09-03 12:18:16.734664	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_macro_node_2_2018-09-03T14-14-40_i4afu_16"}	hachterberg
72	IntracranialVolume_2018-09-06T15-00-57	IntracranialVolume_2018-09-06T15-00-57	Run of IntracranialVolume_2018-09-06T15-00-57 started at 2018-09-06 15:00:57.969967	2018-09-06 13:00:58.968131	2018-09-06 13:00:58.968131	{"workflow_engine": "fastr", "tmpdir": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/ICV"}	tkroes
65	failing_macro_top_level_2018-09-03T14-01-00	failing_macro_top_level_2018-09-03T14-01-00	Run of failing_macro_top_level_2018-09-03T14-01-00 started at 2018-09-03 14:01:00.348877	2018-09-03 12:01:00.696818	2018-09-03 12:02:08.510585	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_failing_macro_top_level_2018-09-03T14-01-00_0u8o6971"}	hachterberg
64	example_2018-09-03T13-13-28	example_2018-09-03T13-13-28	Run of example_2018-09-03T13-13-28 started at 2018-09-03 13:13:28.182654	2018-09-03 11:13:28.398638	2018-09-03 11:14:09.387866	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T13-13-28_eu_kqawk"}	hachterberg
69	auto_prefix_test_2018-09-03T14-30-01	auto_prefix_test_2018-09-03T14-30-01	Run of auto_prefix_test_2018-09-03T14-30-01 started at 2018-09-03 14:30:01.897003	2018-09-03 12:30:02.168929	2018-09-03 12:31:48.325408	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_auto_prefix_test_2018-09-03T14-30-01_lcu1yzwc"}	hachterberg
60	example_2018-09-03T12-55-46	example_2018-09-03T12-55-46	Run of example_2018-09-03T12-55-46 started at 2018-09-03 12:55:46.735829	2018-09-03 10:55:46.976463	2018-09-03 10:56:24.35088	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T12-55-46_lyi526_m"}	hachterberg
59	example_2018-09-03T12-54-57	example_2018-09-03T12-54-57	Run of example_2018-09-03T12-54-57 started at 2018-09-03 12:54:57.007115	2018-09-03 10:54:57.308008	2018-09-03 10:55:32.742661	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T12-54-57_u08j6ygx"}	hachterberg
61	example_2018-09-03T13-09-35	example_2018-09-03T13-09-35	Run of example_2018-09-03T13-09-35 started at 2018-09-03 13:09:35.892905	2018-09-03 11:09:36.215014	2018-09-03 11:10:14.342541	{"tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T13-09-35_uk9w8_dx", "workflow_engine": "fastr"}	hachterberg
58	example_2018-09-03T12-47-05	example_2018-09-03T12-47-05	Run of example_2018-09-03T12-47-05 started at 2018-09-03 12:47:05.948222	2018-09-03 10:47:06.253123	2018-09-03 10:47:49.195573	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T12-47-05_ipb_tivz"}	hachterberg
62	example_2018-09-03T13-11-45	example_2018-09-03T13-11-45	Run of example_2018-09-03T13-11-45 started at 2018-09-03 13:11:45.253646	2018-09-03 11:11:45.532127	2018-09-03 11:12:24.047807	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T13-11-45_8fzk_biq"}	hachterberg
76	add_ints_2018-09-13T15-55-06	add_ints_2018-09-13T15-55-06	Run of add_ints_2018-09-13T15-55-06 started at 2018-09-13 15:55:06.806116	2018-09-13 13:55:06.978199	2018-09-13 13:55:57.823684	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_add_ints_2018-09-13T15-55-06_87y6i5ca"}	hachterberg
63	example_2018-09-03T13-12-39	example_2018-09-03T13-12-39	Run of example_2018-09-03T13-12-39 started at 2018-09-03 13:12:39.760340	2018-09-03 11:12:40.03944	2018-09-03 11:13:14.709713	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_example_2018-09-03T13-12-39_k88dmxem"}	hachterberg
66	add_ints_2018-09-03T14-04-33	add_ints_2018-09-03T14-04-33	Run of add_ints_2018-09-03T14-04-33 started at 2018-09-03 14:04:33.055110	2018-09-03 12:04:33.303727	2018-09-03 12:05:26.466054	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_add_ints_2018-09-03T14-04-33_th_n6kd8"}	hachterberg
70	add_ints_2018-09-06T13-57-24	add_ints_2018-09-06T13-57-24	Run of add_ints_2018-09-06T13-57-24 started at 2018-09-06 13:57:24.631499	2018-09-06 11:57:24.858649	2018-09-06 11:59:04.169014	{"tmpdir": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_add_ints_2018-09-06T13-57-24_59u7mfl2", "workflow_engine": "fastr"}	tkroes
68	add_ints_2018-09-03T14-18-44	add_ints_2018-09-03T14-18-44	Run of add_ints_2018-09-03T14-18-44 started at 2018-09-03 14:18:44.439921	2018-09-03 12:18:44.638651	2018-09-03 12:19:37.348492	{"workflow_engine": "fastr", "tmpdir": "/home/hachterberg/FastrTemp/fastr_add_ints_2018-09-03T14-18-44_kojxqj8b"}	hachterberg
73	failing_network_2018-09-06T15-03-16	failing_network_2018-09-06T15-03-16	Run of failing_network_2018-09-06T15-03-16 started at 2018-09-06 15:03:16.296372	2018-09-06 13:03:16.645729	2018-09-06 13:09:42.16228	{"workflow_engine": "fastr", "tmpdir": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_failing_network_2018-09-06T15-03-16_lgalkkq4"}	tkroes
71	failing_network_2018-09-06T14-00-46	failing_network_2018-09-06T14-00-46	Run of failing_network_2018-09-06T14-00-46 started at 2018-09-06 14:00:46.103574	2018-09-06 12:00:46.468788	2018-09-06 12:06:58.981057	{"tmpdir": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_failing_network_2018-09-06T14-00-46_vtukg7wa", "workflow_engine": "fastr"}	tkroes
74	add_ints_2018-09-07T12-00-02	add_ints_2018-09-07T12-00-02	Run of add_ints_2018-09-07T12-00-02 started at 2018-09-07 12:00:02.359598	2018-09-07 10:00:02.644024	2018-09-07 10:00:47.806375	{"tmpdir": "/exports/lkeb-hpc/tkroes/BBMRI/scratch/fastr_add_ints_2018-09-07T12-00-02_6utv42e_", "workflow_engine": "fastr"}	tkroes
\.


--
-- Name: runs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.runs_id_seq', 76, true);


--
-- Data for Name: update_jobs; Type: TABLE DATA; Schema: public; Owner: pim
--

COPY public.update_jobs (id, path, modified, node_id, status) FROM stdin;
\.


--
-- Name: update_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pim
--

SELECT pg_catalog.setval('public.update_jobs_id_seq', 1, false);


--
-- Name: aggregates aggregates_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.aggregates
    ADD CONSTRAINT aggregates_pkey PRIMARY KEY (id);


--
-- Name: in_ports in_ports_path_key; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.in_ports
    ADD CONSTRAINT in_ports_path_key UNIQUE (path);


--
-- Name: in_ports in_ports_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.in_ports
    ADD CONSTRAINT in_ports_pkey PRIMARY KEY (id);


--
-- Name: jobs jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_pkey PRIMARY KEY (id);


--
-- Name: link_node_refs link_node_refs_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.link_node_refs
    ADD CONSTRAINT link_node_refs_pkey PRIMARY KEY (id);


--
-- Name: links links_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_pkey PRIMARY KEY (id);


--
-- Name: nodes nodes_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.nodes
    ADD CONSTRAINT nodes_pkey PRIMARY KEY (id);


--
-- Name: out_ports out_ports_path_key; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.out_ports
    ADD CONSTRAINT out_ports_path_key UNIQUE (path);


--
-- Name: out_ports out_ports_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.out_ports
    ADD CONSTRAINT out_ports_pkey PRIMARY KEY (id);


--
-- Name: runs runs_name_key; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.runs
    ADD CONSTRAINT runs_name_key UNIQUE (name);


--
-- Name: runs runs_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.runs
    ADD CONSTRAINT runs_pkey PRIMARY KEY (id);


--
-- Name: update_jobs update_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.update_jobs
    ADD CONSTRAINT update_jobs_pkey PRIMARY KEY (id);


--
-- Name: ix_aggregates_node_id; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_aggregates_node_id ON public.aggregates USING btree (node_id);


--
-- Name: ix_jobs_path; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_jobs_path ON public.jobs USING btree (path);


--
-- Name: ix_link_node_refs_node_id; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_link_node_refs_node_id ON public.link_node_refs USING btree (node_id);


--
-- Name: ix_link_node_refs_node_path; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_link_node_refs_node_path ON public.link_node_refs USING btree (node_path);


--
-- Name: ix_nodes_parent_id; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_nodes_parent_id ON public.nodes USING btree (parent_id);


--
-- Name: ix_nodes_path; Type: INDEX; Schema: public; Owner: pim
--

CREATE UNIQUE INDEX ix_nodes_path ON public.nodes USING btree (path);


--
-- Name: ix_update_jobs_path; Type: INDEX; Schema: public; Owner: pim
--

CREATE INDEX ix_update_jobs_path ON public.update_jobs USING btree (path);


--
-- Name: aggregates aggregates_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.aggregates
    ADD CONSTRAINT aggregates_node_id_fkey FOREIGN KEY (node_id) REFERENCES public.nodes(id) ON DELETE CASCADE;


--
-- Name: in_ports in_ports_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.in_ports
    ADD CONSTRAINT in_ports_node_id_fkey FOREIGN KEY (node_id) REFERENCES public.nodes(id) ON DELETE CASCADE;


--
-- Name: jobs jobs_node_path_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_node_path_fkey FOREIGN KEY (node_path) REFERENCES public.nodes(path);


--
-- Name: jobs jobs_run_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_run_id_fkey FOREIGN KEY (run_id) REFERENCES public.runs(id) ON DELETE CASCADE;


--
-- Name: links links_common_ancestor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_common_ancestor_fkey FOREIGN KEY (common_ancestor) REFERENCES public.nodes(path);


--
-- Name: links links_from_port_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_from_port_id_fkey FOREIGN KEY (from_port_id) REFERENCES public.out_ports(path);


--
-- Name: links links_run_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_run_id_fkey FOREIGN KEY (run_id) REFERENCES public.runs(id) ON DELETE CASCADE;


--
-- Name: links links_to_port_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_to_port_id_fkey FOREIGN KEY (to_port_id) REFERENCES public.in_ports(path);


--
-- Name: nodes nodes_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.nodes
    ADD CONSTRAINT nodes_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.nodes(id);


--
-- Name: nodes nodes_run_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.nodes
    ADD CONSTRAINT nodes_run_id_fkey FOREIGN KEY (run_id) REFERENCES public.runs(id) ON DELETE CASCADE;


--
-- Name: out_ports out_ports_node_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pim
--

ALTER TABLE ONLY public.out_ports
    ADD CONSTRAINT out_ports_node_id_fkey FOREIGN KEY (node_id) REFERENCES public.nodes(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

