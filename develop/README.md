###PIM development environment
Launch the environment with:
```
cd /pim/develop
sudo docker-compose up
```
PIM will be available at ```localhost:8080```

####Backend development
The files for backend development are located in ```/pim/server```. The backend server is based on a Flask application with debug turned on, so changing the content of *.py files will trigger a re-load of the Flask application. 

####Frontend development
The files for backend development are located in ```/pim/app```. Changing the content of JavaScript files triggers a re-load of the WebPack development server.
